
@extends("includes.email_container")
@section("content")
            <div>

                 <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Hello {{ ucfirst($invitation->user->name) }},</h3>

                    <p style="font-size: 14px;line-height: 17px;">This mail is notifying about  admin has sent invoice to you for the patient {{ ucfirst($invitation->patient->name) }}.</p>                    
                    
                       <div style="text-align: center; width:100%; margin:20px 0px;"> <a style="background: linear-gradient(254.35deg, #515C84 39.06%, rgba(81, 92, 132, 0.42) 60.46%);
                        border-radius: 20px;
                        padding: 9px 30px;
                        color: white;
                        text-decoration: none;
                        font-size: 17;
                        font-weight: bold;" 
                          href="{{url('/uploads/lawyer_custom_invoice')}}/{{$invitation->invoices->lawyer_invoice_filename}}" download target='_blank'>Download Invoice</a>
                    </div> 

                    <br/>

                    <br/>

                    <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
                    <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 

            </div>  

               </div>

            </div>

@endsection

