var express = require('express')();
var server = require('http').Server(express);
var io = require('socket.io')(server);

let users = [];
let socketId = null;

io.on('connection', function (socket) {
  console.log("new client connected");
  if(socket.id){
    socketId = socket.id;
    io.emit('user-join', socketId);
  }
  
  socket.on('chat-message', function (data) {
    var res = data;
    var isOnlineUser = users.filter(function(el) {
      return el.sender_id ==data.partner_id;
    });
    if(isOnlineUser && isOnlineUser.length > 0) {
      res['is_read'] = true;
    }else{
      res['is_read'] = false;
    }
   
   
    io.emit('chat-message', res);
  });


  socket.on('user-join', function(userData) {

    // console.log("new user join")
    var inList = [];
      inList = users.filter(function(el) {
      return el.socket_id == socket.id;
    });

    if(inList && inList.length > 0){
    }else{
      users.push(userData);
    }
  });

  socket.on('disconnect', function(data) {
    var updatedUsers = [];
    updatedUsers = users.filter(function(el) {
      return socket.id !=el.socket_id;
    });
    users = updatedUsers;
    console.log(updatedUsers)
    console.log('disconnect');
    
    io.emit('user-unjoin', 'one user');
  });

});

server.listen(3000, function () {
  console.log('socket.io server listen at 3000');
});

