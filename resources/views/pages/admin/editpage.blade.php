@extends('layouts.admin_app')



@section('content')

<style>
	.addpatientdata p{
		color:#000000;
	}
	.addpatientdata b{
		font-weight:500;
	}
	.cke_button__image_icon{
		display: none !important;
	}
</style>


<div class="container-fluid">

    <div class="row">

	    <div class="col-md-12">

			 <nav aria-label="breadcrumb">

			  <ol class="breadcrumb">

				<li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{url('admin/cms')}}">CMS</a></li>

				<li class="breadcrumb-item active"><a href="#">{{$page->page_title}}</a></li>

			  </ol>

			</nav>

		</div>

		<div class="col-md-12">  

			<!----search-------------->
				<p style="    color: #141a45;font-weight: 500;font-size: 28px;text-align: center;">{{$page->page_title}}</p>

				<form method="POST" action="{{ url('admin/editpage') }}" enctype="multipart/form-data" >
					@csrf
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							@if ($message = Session::get('success'))
								<div class="alert alert-success alert-block">
									<button type="button" class="close" data-dismiss="alert">×</button>    
									<strong>{{ $message }}</strong>
								</div>
							@endif
						</div>
                       
                        <div class="col-md-3"></div>
						<div class="col-md-1"></div>
						<div class="col-md-10">
							@if($page->page_slug=='about')
								<div class="form-group">
									<label><b>Image:</b></label><br>
									<input name="aboutus_image" class="fomr-control" type="file">
									@error('aboutus_image')
										<br><strong style="color:red;">{{ $message }}</strong>
									@enderror
							  </div> 
							  @if($setting_about_image->settings_value!=null) 
							  <p>Current Image:  <img src="{{URL::asset('/uploads/aboutus')}}/{{$setting_about_image->settings_value}}" style="width:85px;"></img></p>
							  @endif
							@endif
							<div class="form-group">
									<label><b>Content:</b></label>
									<textarea id="editor1" name="content" >{{$page->content}}</textarea>
									
									@error('terms_privacy_policy')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
									<input type="hidden" name="slug" value="{{$page->page_slug}}">
							</div>
							@if($page->page_slug=='about')
								<div class="form-group">
									<label><b>Team Picture 1 Name :</b></label>
									<input name="team_pic_1_name" class="fomr-control" type="text" placeholder="Enter Name" value="{{$team_pic_1->settings_display_name}}"><br>
									<label><b>Team Picture 1 :</b></label><br>
									<input name="team_pic_1" class="fomr-control" type="file">
									@error('team_pic_1')
										<br><strong style="color:red;">{{ $message }}</strong>
									@enderror
							  </div> 
							  @if(($team_pic_1!=null)   && ($team_pic_1->settings_value!=null)) 
							  <p>Current Image:  <img src="{{URL::asset('/uploads/aboutus')}}/{{$team_pic_1->settings_value}}" style="width:85px;"></img></p>
							  @endif

							  <div class="form-group">
							  	    <label><b>Team Picture 2 Name :</b></label>
									<input name="team_pic_2_name" class="fomr-control" type="text" placeholder="Enter Name" value="{{$team_pic_2->settings_display_name}}"><br>
									<label><b>Team Picture 2 :</b></label><br>
									<input name="team_pic_2" class="fomr-control" type="file">
									@error('team_pic_1')
										<br><strong style="color:red;">{{ $message }}</strong>
									@enderror
							  </div> 
							  @if(($team_pic_2!=null)   && ($team_pic_2->settings_value!=null)) 
							  <p>Current Image:  <img src="{{URL::asset('/uploads/aboutus')}}/{{$team_pic_2->settings_value}}" style="width:85px;"></img></p>
							  @endif

							  <div class="form-group">
							  	    <label><b>Team Picture 3 Name :</b></label>
									<input name="team_pic_3_name" class="fomr-control" type="text" placeholder="Enter Name" value="{{$team_pic_3->settings_display_name}}"><br>
									<label><b>Team Picture 3 :</b></label><br>
									<input name="team_pic_3" class="fomr-control" type="file">
									@error('team_pic_3')
										<br><strong style="color:red;">{{ $message }}</strong>
									@enderror
							  </div> 
							  @if(($team_pic_3!=null)   && ($team_pic_3->settings_value!=null)) 
							  <p>Current Image:  <img src="{{URL::asset('/uploads/aboutus')}}/{{$team_pic_3->settings_value}}" style="width:85px;"></img></p>
							  @endif

							  <div class="form-group">
							  	    <label><b>Team Picture 4 Name :</b></label>
									<input name="team_pic_4_name" class="fomr-control" type="text" placeholder="Enter Name" value="{{$team_pic_4->settings_display_name}}"><br>
									<label><b>Team Picture 4 :</b></label><br>
									<input name="team_pic_4" class="fomr-control" type="file">
									@error('team_pic_4')
										<br><strong style="color:red;">{{ $message }}</strong>
									@enderror
							  </div> 
							  @if(($team_pic_4!=null)   && ($team_pic_4->settings_value!=null)) 
							  <p>Current Image:  <img src="{{URL::asset('/uploads/aboutus')}}/{{$team_pic_4->settings_value}}" style="width:85px;"></img></p>
							  @endif

							@endif

							<div class="form-group">	

									<button type="submit" class="btn btn-primary">

										{{ __('Submit') }}

									</button>

							</div>
						</div>
						<div class="col-md-1"></div>
					</div>

				</form>

		</div>

	</div>   


</div>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>
<script>
 var editor = CKEDITOR.replace( 'editor1' );
// The "change" event is fired whenever a change is made in the editor.
editor.on( 'change', function( evt ) {
    // getData() returns CKEditor's HTML content.
  console.log( 'This is what you typed ' + evt.editor.getData() + typeof evt.editor.getData() );
    console.log( 'Total bytes: ' + evt.editor.getData().length );
  $('#hiddedn input').val(evt.editor.getData());
});
          </script>
@endsection

