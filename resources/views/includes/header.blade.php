<style>
  .dropdown-toggle::after{
    border-top: 0.5em solid #526082 !important;
  }
</style>

<nav class="navbar navbar-expand-lg">
  <a class="navbar-brand" href="{{url('/')}}"><img src="{{ asset('img/logo.png') }}"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  @php
  $activeTab = 'home';
  if(request()->is('about*')){
    $activeTab = 'about';
  }
  if(request()->is('benefit*')){
    $activeTab = 'benefit';
  }
  $is_homepage = false;
  if(request()->is('/')){
    $is_homepage = true;
  }
  @endphp
  <div class="collapse navbar-collapse" id="menu">
    <ul class="navbar-nav ml-auto">

      <li class="nav-item {{ $activeTab == 'home' ? 'active': '' }}">
        <a class="nav-link" href="{{route('index')}}">Home</a>
      </li>

      <li class="nav-item {{ $activeTab == 'about' ? 'active': '' }}">
        <a class="nav-link" href="{{route('about')}}">About</a>
      </li>

     <!--  <li class="nav-item {{ $activeTab == 'benefit' ? 'active': '' }}">
        <a class="nav-link" href="{{route('benefit')}}">Benefits</a>
      </li> -->
      <li class="nav-item {{ $activeTab == 'contact' ? 'active': '' }}">
        <a class="nav-link" href="{{ $is_homepage? '#contact': url('/#contact')}}">Contact</a>
      </li>
   <?php   if (Auth::check()) {  
      $totalNotifiaction = Helper::notification_messages(Auth::id())['total'];
      $no_list = Helper::getnotificationList(Auth::id());
    ?>
     <!--<a class="nav-link" href="{{ url('logout') }}"> Logout </a>-->	
     @if (count($no_list) > 0)
     <li class="nav-item {{ $activeTab == 'notification' ? 'active': '' }}">
     <div id="not_icons" class="dropdown show align-self-center">
       <a class="nav-link" href="#" id="bell">
         <img class="bell_icon" src="{{ url('img/bell.png') }}">
         <label class="badge badge-warning notification_badge changebadgeNotification" style="color:white !important; padding:2px 5px !important; font-size:11px; width:auto; height:auto">{{ $totalNotifiaction > 0 ? $totalNotifiaction : '' }}</label>
       </a>
     </div>
     </li>
     @endif


     {{-- ------------------------------------------ --}}
     <li class="nav-item">
        <div class="notifications" id="box" style="display: none">
            <div class="notifications_header">

              <h2 style="margin-bottom:0px;border:none;color:#323B62;">Notifications 
               @if($totalNotifiaction > 0)
               - <span>{{$totalNotifiaction}}</span>
               @endif 
              </h2>
              <span id="closenotification" class="material-icons" style="color: #323B62; font-size:24px;">close</span>
            </div>
           
            <div style="    max-height: 330px;overflow-y: auto;">
            @foreach ($no_list as $row)
             @php
               if($row['status'] == '0'){
                 $styles = 'style="background: #F7FCE8;"';
               }
               else{
                $styles = "";
               }
             @endphp
              <a href="#" style="text-decoration: none;">
                <div id="notification_row_{{$row['id']}}" data-link="{{url($row->url)}}" data-id="{{$row["id"]}}" data-status="{{$row['status']}}" class="notifications-item" {!! $styles !!}>
                    <div class="text">
                      <div style="display: flex;align-items: center;">
                         <img id="not_loading_{{$row['id']}}" src="{{asset('img/loading.gif')}}" style="height:30px; width:30px;display:none;" alt="">
                         <p style="margin-bottom: 0px;color:black">{{$row->description}}</p>
                      </div>
                        <p style="margin-bottom: 0px;
                        padding-top: 4px;
                        text-align: right;
                        font-weight: bold;
                        font-size: 10px;
                        color: #323B62;">{{date('d-m-Y H:i',strtotime($row->created_at))}}</p>
                    </div>
                </div>
              </a>
            @endforeach
          
          </div>

            <div style="text-align: center;padding: 6px;">
              <a href="{{route("notification")}}" style="font-weight: bold;text-decoration: none;">View all</a>
            </div>

        </div>
     </li>
     {{-- ------------------------------------------ --}}

     
	<div class="dropdown show" style="
    display: flex;
    align-items: center;
    justify-content: center;">

      <a class="btn btn-secondary dropdown-toggle px-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        @php
            $user1 = Helper::User(); 
        @endphp

        @if ($user1->profilepic !=null)
            @if($user1->role_id == 2)
                  <img class="headerProfImag" style="width:30px; height:30px; border-color:white;" src="{{ url('uploads/profilepics/lawyer/'.$user1->profilepic) }}"> 
            @elseif ($user1->role_id == 1)
                  <img class="headerProfImag" style="width:30px; height:30px; border-color:white;" src="{{ url('uploads/profilepics/doctor/'.$user1->profilepic) }}"> 
            @elseif ($user1->role_id == 4)
                <img class="headerProfImag" style="width:30px; height:30px; border-color:white;" src="{{ url('uploads/profilepics/clinic/'.$user1->profilepic) }}"> 
            @elseif ($user1->role_id == 5)
                <img class="headerProfImag" style="width:30px; height:30px; border-color:white;" src="{{ url('uploads/profilepics/patient/'.$user1->profilepic) }}"> 
            @endif

        @else
              <img class="headerProfImag" src="{{ url('img/user.jpg') }}">
        @endif

        @if (Auth::user()->role_id == 1)
        <span>{{{ ucfirst(Auth::user()->name)}}} 
          <img src="{{ asset('img/doctor_icon_dark.png') }}" alt="Doctor's Dashboard" class="dashboard_icon_dark">
        </span>

        @elseif (Auth::user()->role_id == 2)
            <span>{{{ ucfirst(Auth::user()->name)}}} 
              <img src="{{ asset('img/lawyer_icon_dark.png') }}" alt="Layers's Dashboard" class="dashboard_icon_dark">
            </span>

            @elseif (Auth::user()->role_id == 5)
            <span>{{{ ucfirst(Auth::user()->name)}}} 
               <img src="{{ asset('img/patient.png') }}" alt="Patient Dashboard" class="dashboard_icon_dark">
            </span>

        @elseif (Auth::user()->role_id == 4)
            <span>{{{ ucfirst(Auth::user()->name)}}} 
            <img src="{{ asset('n_img/clinic_dark.png') }}" alt="Clinic Dashboard" class="dashboard_icon_dark">
          </span>
        @endif
          
        </a>

        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

        {{-- Doctor Mene --}}
        @if(Auth::user()->role_id == 1)
            <a class="dropdown-item "  href="{{ url('doctor-dashboard') }}">Dashboard</a>
            <a class="dropdown-item "  href="{{ url('doctor-profile') }}">Profile</a>
            <a class="dropdown-item "  href="{{ route('privateInfor') }}">Private information</a>
        @endif

        @if (Auth::user()->role_id == 1 && Auth::user()->doctor && Auth::user()->doctor->instruction_form==null)
            <a id='updatelnk' class="dropdown-item opensss" onclick="openUploadQuestion(0)" data-toggle="modal" data-target="#upload_questionnair"  href="#">Upload Questionnaire</a>
        @elseif (Auth::user()->role_id == 1 && Auth::user()->doctor && Auth::user()->doctor->instruction_form != null)
            <a id='updatelnk' class="dropdown-item opensss" onclick="openUploadQuestion(1)"  data-toggle="modal" data-target="#upload_questionnair" href="#">Update Questionnaire</a>
        @endif

        {{-- Lawyer Menu --}}
        @if (Auth::user()->role_id == 2)
          <a class="dropdown-item"  href="{{ url('lawyer-dashboard/my-patients') }}">Dashboard</a>
          <a class="dropdown-item fdfdf"  data-toggle="modal" data-target="#lawyerchangePassword">Change Password</a>
          <a class="dropdown-item"  href="{{ url('lawyer-profile') }}">Profile</a>
        @endif

        {{-- Patient Menu --}}
        @if (Auth::user()->role_id == 5)
          <a class="dropdown-item"  href="{{ url('patient-dashboard') }}">Dashboard</a>
          <a class="dropdown-item fdfdf"  data-toggle="modal" data-target="#lawyerchangePassword">Change Password</a>
          <a class="dropdown-item"  href="{{ url('patient-profile') }}">Profile</a>
        @endif

        {{-- Clinic Menu --}}
        @if (Auth::user()->role_id == 4)
            <a class="dropdown-item" href="{{url('/clinic-dashboard')}}">Dashboard</a>
            <a class="dropdown-item" href="{{url('/addClinicDoctor')}}">New Doctor</a>
            <a class="dropdown-item" href="{{url('/showClinicDoctorList')}}">Doctors List</a>
            <a class="dropdown-item fdfdf"  data-toggle="modal" data-target="#lawyerchangePassword">Change Password</a>
            <a class="dropdown-item" href="{{url('clinic-profile')}}">Profile</a>
            <a class="dropdown-item" href="{{url('/clinic_subscription')}}">Subscription</a>
        @endif

        <a class="dropdown-item" href="{{ url('logout') }}">Logout</a>	
      </div>

</div>
   
  
    @php 
    $notif_sender = [];
    @endphp 
   
     <?php   }
     else{  ?>

      <li class="nav-item">
        <a class="nav-link login" href="#" data-toggle="modal" data-target="#login"><img src="{{ asset('img/login.svg') }}"> login</a>
      </li>

      <li class="nav-item">
        <a class="nav-link signup" href="#" data-toggle="modal" onClick="openTesting()"><img src="{{ asset('img/signup.svg') }}"> Signup</a>
        {{-- <a class="nav-link signup" href="#" data-toggle="modal" onClick="displaySignupForm()" data-target="#signup"><img src="{{ asset('img/signup.svg') }}"> Signup</a> --}}
      </li>
      
      <?php     } ?>
    </ul>
</nav>

