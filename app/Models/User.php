<?php



namespace App\Models;



use Illuminate\Contracts\Auth\MustVerifyEmail;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

//use App\Models\User;

//use App\Models\lawyers;



class User extends Authenticatable

{

    use HasFactory, Notifiable;



    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

   

    protected $fillable = [

        'name',

        'email',

        'unique_id',

        'password',

        'phone',

        'role_id',

        'business_address',

        'location',

        'remember_token',

    ];



    



    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [

        'password',

        'remember_token',

    ];



    /**

     * The attributes that should be cast to native types.

     *

     * @var array

     */

    protected $casts = [

        'email_verified_at' => 'datetime',

    ];

	

	/*lawyers data*/

	public function role()

    {

        return $this->belongsTo(role::class);

    }

	public function lawyer(){

        return $this->hasOne(lawyers::class);

    }

	public function doctor(){

        return $this->hasOne(doctors::class);

    }	

	public function city(){

        return $this->belongsTo(City::class,'location');

    }

    

    public function hasRole($role)

    {

        return $this->role->role == $role;

    }

    

}

