$(document).ready(function(){

    
//sidebar
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
        $(this).toggleClass('active');
    });
    // $('#selectize-owner-channel').selectize({
    //     maxItems: 4
    // });
    // $('body').on('mouseenter','#sidebar.active ul.sidebar-mainmenu', function() {
    //     $('#sidebar').toggleClass('active');
    // })
    if(URLRoute == 'AdminParentController@create' || URLRoute == 'AdminParentController@edit' || typeof(AllowProfilePictureCroppie) != 'undefined'){
        var croppie_width = typeof(croppieImgWidth)!='undefined' ? croppieImgWidth : 250;
        var croppie_height = typeof(croppieImgHeight)!='undefined'?croppieImgHeight:250;
        var basicCrop = $('#profile_picure_cropper').croppie({
            viewport: { width: croppie_width, height: croppie_height },
            enableExif: true,
            boundary: { width: croppie_width+100, height: croppie_height+100 },
        });
        $('#cropImageBtn').on('click', function (ev) {
            basicCrop.croppie('result', {
                type: 'base64',
                size: {width: croppie_width, height: croppie_height}
            }).then(function (resp) {
                $('#base64_img').val(resp);
                $('#parent-form-avathar img').attr('src', resp).show();
                $('#profile-pic-crop-modal').modal('hide');
                uploadProfilePicture(resp);
            });
        });
          
        $('#profile_pic_input').on('change', function () {
            if(this.value){
               $('#profile-pic-crop-modal').modal('show');   
                readFile(this);
            }
        });
        $('#cancelCroppie').click(function(){
                $('#base64_img').val('');
                $('#parent-form-avathar img').attr('src', '').hide();
                $('#profile-pic-crop-modal').modal('hide');
                $('#profile_pic_input').next('.custom-file-label').html('Choose file');
        })
    }else if(URLRoute == 'AdminChannelController@create' || URLRoute == 'AdminChannelController@edit'){
        $('input[name="channel_type"]').change(function(){
            var channel_type = $(this).val();
            if(channel_type == 'exclusive') {
                $('.input-channel-partner-block').addClass('d-flex').removeClass('d-none');
            }else{
                $('.input-channel-partner-block').addClass('d-none').removeClass('d-flex');
            }
        })
    }else if(URLRoute == 'AdminCmsController@create' || URLRoute == 'AdminCmsController@edit'){
    }       

if(URLRoute == 'AdminTeacherController@show' || URLRoute == 'AdminVolunteerController@show'){
    $('#cancelCroppie').click(function(){
        location.reload();
    })
}
if(typeof(AllowInputVideoPreview) !='undefined') {
    $('#introduction_video').change(function(e){
        $(this).parent().parent().find('.invalid-feedback strong').html('');
        var $block = $('#intro-video-block');
        var $source = $block.find('source');
        var $file = URL.createObjectURL(this.files[0]);
        var FileSize = this.files[0].size / 1024 / 1024;
        console.log(FileSize);
        if(FileSize > 5){
            $(this).parent().parent().find('.invalid-feedback').show().find('strong').html('Error: Video size exceeded 5MB.');
            $block.hide();
            $(this).focus();
            return false;
        }
        $source[0].src = $file;
        $source.parent()[0].load();
        $block.show();
    })
}
    $(document).on("click", function(event){
        var $trigger = $(".collapse");
        // console.log();
        if($trigger !== event.target && !$trigger.has(event.target).length && $(event.target).attr('class') =='dropdown-toggle'){
            $(".collapse").collapse('hide');	
        }            
    });
//sidebar
    $('.show_password_block_link').click(function(){
        $('#form_password_block').toggleClass('d-none');
        $('#input-password').prop('required',true);
        $('#password-link-block').toggle();
    })
    $('#close_password_link').click(function(){
        $('#form_password_block').toggleClass('d-none');
        $('#input-password').prop('required',false);
        $('#password-link-block').toggle();
    })
//hide show password
$(document).ready(function() {
    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });
    $('.show_input_ele_block_link').click(function(){
        var inputBlock = $(this).attr('data-target-input-ele-block');
        $('#'+inputBlock).toggleClass('d-none');
        $('#'+inputBlock+' input').prop('required',true);
        $(this).toggle();
    })
    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    });
//set new password acition -view volunteer/teacher
$( "#set-new-password-btn" ).on('click', function(){
    $('#pwd-modal-sbt-btn').prop('diabled',false);
    $('#set-new-password-form')[0].reset();
    $('#new-password-modal-resp').removeClass('text-danger').removeClass('text-danger').removeClass('text-success').html('');
});
    $('#set-new-password-form').submit(function(e){
        e.preventDefault();
        $('#pwd-modal-sbt-btn').prop('diabled',true);
        var password = $('#input-password').val();
        var confirmPassword = $('#confirm-password').val();
        if(password !== confirmPassword){
            $('#new-password-modal-resp').addClass('text-danger').html('Password and confirm password are not matched.');
            return false;
        }else{
            $.ajax({
                url: base_url + "admin/users/set-new-password",
                type: "post",
                data:$(this).serialize(),
                dataType: "json",
                success: function(result){
                    $('#pwd-modal-sbt-btn').prop('diabled',false);
                    if(result.status == 'success'){
                        $('#new-password-modal-resp').removeClass('text-danger').addClass('text-success').html(result.message);
                        setTimeout(function(){
                           $('#set-password-modal').modal('hide');
                        },3000);
                    }else{
                        $('#new-password-modal-resp').addClass('text-danger').html(
                            $.each(result.message.password,function(key,error){
                                return error;
                            })
                        );
                        return false;
                    } 
            }
        });
        }    
    });


    //close alert autometically
    setTimeout(function(){
        $('.alert-success, .alert-success').slideUp(2000);
    },5000);
});
$('input[name="smpt_status"]').click(function(){
    if($(this).is(":checked")){
       $('input.smpt_input').prop('required',true);
    }else{
        $('input.smpt_input').prop('required',false);
   }
})
})
function generate_randomPassword(input)
{
    var password = get_random_string();
    $('#'+input).val(password);
}
function get_random_string(length=10)
{
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789[]!@#$%&*()-+';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
}
function remove_selectedSchedle(input)
{
    if(!confirm('Are sure want to remove this schedule?')){
        return false;
    }else{
        $(input).parents('table').hide();
    }
}
  function minTimeFromMidnight(tm){
    var ampm= tm.substr(-2)
    var clk = tm.substr(0, 5);
    var m  = parseInt(clk.match(/\d+$/)[0], 10);
    var h  = parseInt(clk.match(/^\d+/)[0], 10);
    h += (ampm.match(/pm/i))? 12: 0;
    return h*60+m;
   }
   function readFile(input,cropperEle='') {
       if(cropperEle == ''){
        cropperEle = $('#profile_picure_cropper');
       }
    if (input.files && input.files[0]) {
      var reader = new FileReader();
  
      reader.onload = function (e) {
        cropperEle.croppie('bind', {
          url: e.target.result
        });
      }
  
      reader.readAsDataURL(input.files[0]);
    }
  }
   function timeConvertTo12Hrs (time) {
    // Check correct time format and split into components
    time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
  
    if (time.length > 1) { // If time format correct
      time = time.slice (1);  // Remove full string match value
      time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join (''); // return adjusted time or original string
  }
  
  function uploadProfilePicture(base64img){
    if(URLRoute == 'AdminTeacherController@show' || URLRoute == 'AdminVolunteerController@show' || URLRoute == 'AdminTeachPageCmsController@index'){
       $('#profile-photo-form').submit();  
    }   
    return false;
  }
