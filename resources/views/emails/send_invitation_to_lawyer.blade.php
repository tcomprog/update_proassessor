@extends("includes.email_container")
@section("content")

            <div>
                <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Dear {{{ ucfirst($invitation->user->name) }}},</h3>
                @if($invitation->status == 'active')
                    <p style="font-size: 14px;line-height: 17px;">Your invite to <b>{{ ucfirst($invitation->user1->name) }}</b> for <b>{{ ucfirst(MyHelper::Decrypt($invitation->patient->name)) }}</b> has been accepted. We have sent an email to {{ ucfirst(MyHelper::Decrypt($invitation->patient->name)) }} to respond with their confirmation.</p>										<?php if($invitation->user1->doctor->instruction_form!=null){ ?>					
                        	Instruction Form: <a  href="{{url('uploads/instruction_form')}}/<?php echo $invitation->user1->doctor->instruction_form; ?>" target="_blank">Download													<?php } ?></a>
				   
					<p style="font-size: 14px;line-height: 17px;">Please upload patient's documents in Files & Reports if not uploaded yet.</p>
                @elseif($invitation->status == 'rejected' && $invitation->rejected_by == 'patient')
                    <p style="font-size: 14px;line-height: 17px;">Your invite to <b>{{ ucfirst($invitation->user1->name) }}</b> for {{ ucfirst($invitation->patient->name) }} has been rejected by Patient <b>{{ ucfirst($invitation->patient->name) }}</b>.</p>
                @elseif($invitation->status == 'rejected')
                    <p style="font-size: 14px;line-height: 17px;">Your invite to <b>{{ ucfirst($invitation->user1->name) }}</b> for {{ ucfirst($invitation->patient->name) }} has been rejected by Doctor <b>{{ ucfirst($invitation->user1->name) }}</b>.</p>
                @else
                    <p style="font-size: 14px;line-height: 17px;">Your invite to <b>{{ ucfirst($invitation->user1->name) }}</b> for {{ ucfirst($invitation->patient->name) }} has been expired.</p>
                @endif
                    
                    <?php if($invitation->user1->doctor->instruction_form!=null){ ?>
                        <div style="display: flex;
                            justify-content: center;
                            padding: 20px 0px;">
                        <a href="{{url('uploads/instruction_form')}}/<?php echo $invitation->user1->doctor->instruction_form; ?>" style="background: linear-gradient(230.76deg, #515C84 48.46%, rgba(81, 92, 132, 0.42) 77.66%);
                            border-radius: 20px;
                            padding: 9px 20px;
                            color: white;
                            text-decoration: none;
                            font-weight: bold;" target="_blank" download>Download Questionnaire</a>
                            </div>
					<?php } ?> 

                    @if($invitation->status == 'active')
                    <div style="display: flex;
                    justify-content: center;
                    padding: 20px 0px;">

                    <a href="{{url('tracking')}}/<?php echo MyHelper::Encrypt($invitation->user->id)."/".MyHelper::Encrypt($invitation->id);?>" style="background: linear-gradient(230.76deg, #515C84 48.46%, rgba(81, 92, 132, 0.42) 77.66%);
                        border-radius: 20px;
                        padding: 9px 20px;
                        color: white;
                        text-decoration: none;
                        font-weight: bold;" target="_blank" download>Track It</a>
                    </div>
                    @endif
               
                  

                    <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
                    <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 
              </div>  
</div> 

@endsection



