@extends('layouts.inner-benefit')



@section('title', 'Benefits')



@section('content')



<!-- Process Section -->

<style>
    ol{
        margin: 0px;
        padding: 0px 32px 25px;
    }
    ol li{
        font-size: 17px;
        margin-bottom: 10px;
    }
    ol li::marker{
        color: #141a45;
        font-weight: 600;
    }
    .about_sec h3{
        font-size: 30px;
    }
</style>

<section class="about_sec">

         <div class="container">    
            <div class="row">
                <div class="col-md-12">
                    <h3>Legal</h3>
                    <h2>Why use Pro Assessors ?</h2>
                    <br>
                    <p><strong>Major benefits to Lawyers:</strong>
                    <p>The application help lawyers to</p>
                    <ul>
                        <li>Simplify their process to obtain expert opinion with diverse and expansive specialties available </li>
                        <li>Ensure secure and private mode of transmission </li>
                        <li>Easily requestfor expert reports for their clients with a Click at any time</li>
                        <li>allows easy traceability </li>
                        <li>timely response to their request.</li>
                        <li>Reduces staff time and associated costs</li>
                        <li>Reduces human errors and miscommunications </li>
                        <li>Meets your deadline</li>
                        <li>No upfront fees or cost for the expert opinions </li>
                        <li>Incur zero material costs </li>
                    </ul>
                    <h3>Medical experts</h3>
                    <p><strong>Major benefits to Specialist and Medical experts</strong>
                        <ul>
                            <li>Creates increased Exposure of services to wider audience </li>
                            <li>Trackability </li>
                            <li>Reduces staff time and associated costs</li>
                            <li>Getting paid efficiently and promptly</li>
                            <li>Security and privacy </li>
                            <li>Reduces human errors and miscommunications </li>
                            <li>Simples booking and accounting</li>
                        </ul>
                </div>
            </div>
         </div>

</section>

<!-- Process Section -->

@endsection

