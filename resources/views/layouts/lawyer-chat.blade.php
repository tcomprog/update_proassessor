<!doctype html>

<html>

<head>

    @include('includes.head')

</head>

<body>

    <header>

        <div class="container">

            <div class="row">

                @include('includes.header')

            </div>

        </div>

    </header>



    

    @yield('content')

    

    <footer>

        @include('includes.footer')

    </footer>

    @include('includes.lawyer-chat-scripts')

    @yield('footerscripts')

</body>

</html>