@extends('layouts.custom')

@section('content')
<style>
.error{color:red !important;}
</style>

<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="{{asset("js/custom.js")}}"></script>

<!-- Process Section -->

 <!-- Banner Section -->

    <section class="intro_banner_sec inner_banner_sec" style="padding-top:25px; padding-bottom:25px;">
        <div class="container">

            <div class="row align-items-center">

                <div class="col-md-6">

                    <div class="welcome_text">

                        <h1>Password Reset</h1>

                    </div>

                </div>

                <!--<div class="col-md-6 text-right">

                    <a href="#" class="search_btn">Search Doctors</a>
                </div>-->
            </div>
        </div>
    </section>
<!-- Lawyer Dashboard Section -->
    <section class="dashboard_sec">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="reset_password">

                        <div class="profile_update" id="pwdset">
						
							<div class="alert alert-success fade show" role="alert" id="pwdsuccess" style="display:none;">
								Password Changed Successfully
							</div>
							<div class="alert alert-danger fade show" role="alert" id="pwdfail" style="display:none;">
								You are Already Changed the Password
							</div>
							
                            <div class="row">

                                <div class="col-md-12">

                                    <h2>Reset Password</h2>

                                </div>

                                <div class="col-md-12">

                                    <div class="form-group">

                                        <input type="password" class="form-control" name="new_password" id="new_password" aria-describedby="new_password" placeholder="New Password">
                                        <span id="password_error" class="errorMsg">Error message</span>
                                    </div>

                                    <script>
                                        $("#new_password").on('input',function(){
                                            let pw = $(this).val();
                                            let validate = checkPasswordValidity(pw);
                                            if(!isEmpty(pw) && validate != null){
                                               $("#password_error").show();
                                               $("#password_error").text(validate);
                                            }
                                            else{
                                                $("#password_error").hide();
                                            }
                    
                                            if(isEmpty(pw)){
                                                $("#password_error").hide();
                                            }
                                        });
                                    </script>

                                </div>

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <input type="password" class="form-control" id="confirm_password" aria-describedby="confirm_password" name="confirm_password"  placeholder="Confirm Password">
                                        <span id="repassword_error" class="errorMsg">Error message</span>
                                    </div>

                                    <script>
                                        $("#confirm_password").on('input',function(){
                                            let pw = $(this).val();
                                            let pw2 = $("#new_password").val();
                    
                                            if(isEmpty(pw)){
                                                $("#repassword_error").hide();
                                            }
                    
                                            if(pw != pw2){
                                               $("#repassword_error").show();
                                               $("#repassword_error").text("Passwords don`t match");
                                            }
                                            else{
                                                $("#repassword_error").hide();
                                            }
                    
                                        });
                                    </script>

                                </div>
								<input type="hidden" name="pwdreset_token" id="pwdreset_token" value="<?php if(isset($id)){echo $id;} ?>">

                            </div>

                            <button type="submit" class="btn btn-primary update_btn" id="pwdsetbtn">Submit</button>
                            <img id="forgotpassloading" style="display: none" width="50" src="{{asset("img/loading.gif")}}" alt="">
                        </form>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <!-- Lawyer Dashboard Section -->
<!-- Process Section -->


@endsection
