
   <style>
         .invoicebody .clearfix:after {
         content: "";
         display: table;
         clear: both;
         }
         .invoicebody body{
         position: relative;
         width: 21cm;  
         height: 29.7cm; 
         margin: 0 auto; 
         color: #555555;
         background: #FFFFFF;
         font-family: Arial, sans-serif; 
         font-size: 14px; 
         font-family: 'Source Sans Pro', sans-serif;
         }
         .invoicebody .header {
         padding: 10px 0;
         margin-bottom: 20px;
         border-bottom: 1px solid #AAAAAA;
         }
         .invoicebody #logo {
         float: none;
         margin-top: 8px;
         text-align: center;
         }
         .invoicebody #logo img {
         height: 70px;
         }
         .invoicebody #company {
         float: right;
         text-align: right;
         }
         .invoicebody #details {
         margin-bottom: 30px;
         }
         .invoicebody #client {
         padding-left: 6px;
         border-left: 6px solid #00528c;
         float: left;
         }
         .invoicebody #client .to {
         color: #777777;
         }
         .invoicebody h2.name {
         font-size: 1.5em;
         font-weight: normal;
         margin: 0;
         margin-bottom: 5px;
         color: #000000;
         }
         .invoicebody #client .address{
          margin-bottom: 5px;
          font-size: 1.1em;
          color: #000000;
         }
         .invoicebody #client .email{
          margin-bottom: 5px;
          font-size: 1.1em;
         }
         .invoicebody #invoice {
         float: right;
         text-align: right;
         }
         .invoicebody #invoice h1 {
         color: #0087C3;
         font-size: 2.4em;
         line-height: 1em;
         font-weight: normal;
         margin: 0  0 10px 0;
         }
         .invoicebody #invoice .date {
         font-size: 1.1em;
         color: #000000;
         }
         .invoicebody table {
         width: 100%;
         border-collapse: collapse;
         border-spacing: 0;
         margin-bottom: 20px;
         }
         .invoicebody table th,
         .invoicebody table td {
         padding: 20px;
         background: #ffffff;
         text-align: center;
         border-bottom: 1px solid #ddd;
         border-right: 1px solid #ddd;
         border-left: 1px solid #ddd;
         }
         .invoicebody table th {
         white-space: nowrap;        
         font-weight: normal;
         }
         .invoicebody table h4{
          margin: 0px 0px 5px;
         }
         .invoicebody table td {
         }
         .invoicebody table td h3{
         color: #000;
         font-size: 1.2em;
         font-weight: normal;
         margin: 0 0 0.2em 0;
         }
         .invoicebody table .heading{
          color: #fff !important;
          font-size: 1.2em;
          background: #004b82;
         }
         .invoicebody table .no {
         color: #000;
         font-size: 1.2em;
         text-align: left;
         }
         .invoicebody table .desc {
         text-align: left;
         }
         .invoicebody table .unit {
          color: #000;
         }
         .invoicebody table .qty {
          color: #000;
         }
         .invoicebody table .total {
         background: #57B223;
         color: #FFFFFF;
         }
         .invoicebody table td.unit,
         .invoicebody table td.qty,
         .invoicebody table td.total {
         font-size: 1.2em;
         text-align: center;
         }
         .invoicebody table tfoot td {
         padding: 10px 20px;
         background: #FFFFFF;
         font-size: 1.2em;
         white-space: nowrap; 
         border-top: 1px solid #AAAAAA; 
         }
         .invoicebody table tfoot tr:first-child td {
         border-top: none; 
         }
         .invoicebody table tfoot tr:last-child td {
         color: #000;
         font-size: 1.4em;
         border-top: 1px solid #57B223; 
         font-weight: 600;
         }
         .invoicebody table tfoot tr td:first-child {
         border: none;
         }
         .invoicebody #thanks{
         font-size: 2em;
         margin-bottom: 50px;
         }
         .invoicebody #notices{
         padding-left: 6px;
         border-left: 6px solid #0087C3;  
         }
         .invoicebody #notices .notice {
         font-size: 1.2em;
         }
         .invoicebody .footer {
         color: #777777;
         width: 100%;
         height: 30px;
         position: absolute;
         bottom: 0;
         border-top: 1px solid #AAAAAA;
         padding: 8px 0;
         text-align: center;
         }
		 .cataddress{max-width:none;}
      </style>
      <style>
          .addpatient_btn{
                color: #ffffff;
                font-size: 16px;
                min-width: 125px;
                height: 40px;
                border-radius: 30px 0px 30px 30px;
                text-align: center;
                line-height: 23px;
                box-shadow: 0px 0px 15px rgb(0 0 0 / 20%);
                background: #5a8627;
                background: linear-gradient(
                  90deg
            , #5a8627 0%, #7aac2d 100%);
                display: inline-block;
                font-weight: 500;
          }
          .login_box{
            width: 100%;
            border-radius: 0px 8px 0px 8px;
            border: 1px dotted #5A8627;
            box-shadow: 0px 0px 15px rgb(0 0 0 / 20%);
            padding: 15px;
            position: relative;
                }
      </style>
   <div class="invoicebody">
         <a href=""><i class="fa fa-times" aria-hidden="true" data-dismiss="modal" aria-label="Close" style="float: right;"></i></a>
      <div class="clearfix header">
         <div id="logo">
            <img src="{{ asset('img/logo.png') }}">
         </div>
      </div>
      <div>
         <div id="details" class="clearfix">
            <div id="client">
               <h2 class="name" id="doctorname"></h2>
               <div class="address cataddress" >
                    <span id="categoryname"></span>:<span id="doctorlicencenumber"></span>
               </div>					
               <div class="email">PH: <a href="" style="color: #00528C;text-decoration: none;background-color: transparent;"><span id='doctorphone'></span></a></div>
            </div>
            <div id="invoice">
            <h2 class="name"></h2>
               <h2 class="name">Invoice #<span id="doctor_invoice_number"></span></h2>
               <h2 class="name">Bill to: Pro Assessors</h2>
               <div class="date">Invoice Date: <span id="todaydate"></span></div>
            </div>
         </div>
         <table border="0" cellspacing="0" cellpadding="0">
            <thead>
               <tr>
                  <th class="no heading">PATIENT</th>
                  <th class="desc heading">SERVICE PROVIDED</th>
                  <th class="unit heading">SERVICE DATE</th>
                  <th class="qty heading">FEE</th>
               </tr>
            </thead>
            <tbody>
              <!-- ROW -->
               <tr>
                  <td class="no">
                    <h4 id="patientname"></h4>
                    <span style="color: #888;" >Date of Birth:</span><span id="patientdateofbirth"></span> <br/>
                    <span style="color: #888;">Date of Accident:</span><span id="patientdateofaccident"></span>
                  </td>
                  <td class="desc">
                     <h3><span id="new_service_provided"></span></h3>
                  </td>
                  <td class="unit"><span id="invitationdate"></span></td>
                  <td class="qty">$<span class="new_bill_amt"></span></td>
               </tr>
               <!-- ROW -->
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="2"></td>
                  <td colspan="1">TOTAL</td>
                  <td>$<span class="new_bill_amt" ></span></td>
               </tr>               
                  <tr id="hsttr" > 
                     <td colspan="2"></td>
                     <td colspan="1">HST</td>
                     <td>$<span id="hstamount" ></span></td>
                  </tr>
                  <tr id="subtotaltr" >  
                     <td colspan="2"></td>
                     <td colspan="1">SUB TOTAL</td>
                     <td>$<span id="subtotal" ></span></td>
                  </tr>

            </tfoot>
         </table>
                        </div>      
                        </div>
