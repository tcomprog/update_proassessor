<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\lawyers;
use App\Models\doctors;
use App\Models\Sitedetails;
use App\Models\user_invitations;
use App\Models\Tracability;
use App\Models\Designation;
use App\Models\InvitationAttachment;
use App\Models\Doctorappointment;
use App\Models\Patient;
use App\Models\Pagecms;
use App\Models\Nofication;
use App\Http\Controllers\EmailController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\DoctorTimeSlot;
use App\Helpers\Helper as MyHelper;
use App\Models\Holiday;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use DatePeriod;
use DateTime;
use DateInterval;
use Config;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class lawyerController extends Controller
{  
	private $medical_file_encrypt_key;
	private $medical_file_encyption_blocks = 1000; 
	
	public function __construct()
	{
		$this->medical_file_encrypt_key = Config::get('constants.FILE_ENCRYPTION_KEY');
	}
	
	public function profile(Request $request){
		$user = auth()->user();
		$users = User::find($user->id);
		return view('/pages/lawyer/profile',['users' => $users]);
	}
	
	public function download_agreement(Request $request){
		//echo "hii";
		$user = auth()->user();	
		$file = $user->terms_agreement;
        $name = basename($file);
        return response()->download($file, $name);
	}
	
	/*add event*/
	public function sendinvitation(Request $request){

	 
		$user = auth()->user();
		$res_message = false;	
		$unique_id=md5(uniqid());		
		$inviter_user_id=$user->id;
		$invitee_user_id=$request->docid;
		$patientid=$request->patientid;
		$notes=$request->notes;
		$timeslot=$request->timeslot;

	
		if(isset($request->appointment_type)){
			$appointment_status=$request->appointment_type;
		}else{
			$appointment_status='inperson';
		}
		//$duedate=$request->duedate;
		$duedate=date("Y-m-d", strtotime($request->duedate));;
		
		if(!empty($request->selecteddate)){		
			$invitation_date=date("Y-m-d", strtotime($request->selecteddate));		
		}
		else{
			$invitation_date = date('Y-m-d');
		}		
		$status='pending';
		$doctor = User::find($invitee_user_id);	
		$doctorname=ucfirst($doctor->name);	
		$settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
		$expireTime = date('Y-m-d H:i:s',strtotime('-'.$settings->invitation_expire_in_minutes.' minutes'));

		$check_patient_slot = user_invitations::where('timeslot',$timeslot)->where('invitation_date',$invitation_date)
								->where(function($q) use($expireTime){
									$q->whereIn('status',['active','completed'])
									->orWhere('status','pending')->where('created_at','>=',$expireTime);
								})->first();
		if($check_patient_slot) {
			return response()->json(['status'=>'error','message'=>'Already this Patient has an appointment with The Doctor - '.$check_patient_slot->user1->name.' on '.date('d/M/Y').' at '.$timeslot ]);
		}

		$lawyername=ucfirst($user->name);	
		
		$doctoremail=$doctor->email;	
			$sendint = new user_invitations();

			$sendint->unique_id=$unique_id;
			$sendint->inviter_user_id=$inviter_user_id;
			$sendint->invitee_user_id=$invitee_user_id;
			$sendint->invitation_date=$invitation_date;
			$sendint->patient_user_id=$patientid;
			$sendint->message=$notes;
			$sendint->enceypKey=MyHelper::randomString(100);
			$sendint->timeslot=$timeslot;
			$sendint->invitee_category_id=$request->input_category_id_new;
			$sendint->duedate=$duedate;
			$sendint->status=$status;
			$sendint->appointment_type=$appointment_status;
			
			$sendint->save();		
			$last_insertid=$sendint->id;		
			if(isset($last_insertid)){
				$tracability = new Tracability();
				$tracability->date = date('Y-m-d H:i:s');
				$tracability->invitation_id	 = $last_insertid;
				$tracability->status = '0';
				$tracability->save();

				$invitation = user_invitations::find($last_insertid);
				$email_controller = new EmailController();
				
				if($doctor->clinic_id > 0){
                    $clinic = User::find($doctor->clinic_id);	
					$senderEmail = $clinic->email;
					$sendName = $doctor->name."  [".$clinic->name."]";
				}
				else{
					$senderEmail = $doctor->email;
					$sendName = $doctor->name;
				}

				$email_controller->lawyer_invite_doctor($invitation,$senderEmail,$sendName);
				
				$lawyer = User::find($inviter_user_id);	
				$notif = new Nofication();

				if($doctor->clinic_id > 0){
                    $notif->url = "clinic-dashboard";
					$notif->to_id = $doctor->clinic_id;
					$notif->description = str_replace("##",$doctor->name,str_replace("@@",$lawyer->name,Helper::c_strings()["invit_to_doctor_clinic"]));
				}
				else{
                    $notif->url = "doctor-dashboard/my-invitations";
					$notif->to_id = $invitee_user_id;
					$notif->description = str_replace("@@",$lawyer->name,Helper::c_strings()["invit_to_doctor"]);
				}

                $notif->from_id = $inviter_user_id;
				$notif->save();


				$pt_data = User::select("id")->where('patient_id',$patientid)->first();

				$notif_1 = new Nofication();
				$notif_1->description = "New invitation has been created by your lawyer ".Auth::user()->name;
				$notif_1->url = 'invitation_details_view/'.$invitation->id;
				$notif_1->status = '0';
				$notif_1->to_id = $pt_data->id;
				$notif_1->from_id = $user->id;
				$notif_1->save();

				return response()->json(['status'=>'success','url'=>url('viewcalendar')."/".$invitee_user_id]);
			}else{
					return response()->json(['status'=>'error']);
			}
	  }


     public function deleteAllReports(Request $request){ //aabb
		  // type =>  1 - Medical, 2 - Summary, 3 - Letter en, 4 - form, 5 - Questionari
		$invitationID = $request->assessID;
		$type = $request->type;
		$user = InvitationAttachment::where('invitation_id',$invitationID)->first();
		if($type == 1){
			$files = json_decode($user->medicalsfilename);
			foreach($files as $url){
				Storage::delete('uploads/l_medical/'.$url);
			}
			$user->medicalsimagename = '';
			$user->medicalsfilename = '';
		}
		elseif($type == 2){
			$files = json_decode($user->filename);
			foreach($files as $url){
				Storage::delete('uploads/l_summary/'.$url);
			}
			$user->imagename='';
			$user->filename='';
		}
		elseif($type == 3){
			$files = json_decode($user->loefilename);
			foreach($files as $url){
				Storage::delete('uploads/l_LetterOfEngagement/'.$url);
			}
			$user->loefilename='';
			$user->loeimagename='';
		}
		elseif($type == 4){
			$files = json_decode($user->form53filename);
			foreach($files as $url){
				Storage::delete('uploads/l_form_53/'.$url);
			}
			$user->form53filename='';
			$user->form53imagename='';
		}
		elseif($type == 5){
			$files = json_decode($user->insformfilename);
			foreach($files as $url){
				Storage::delete('uploads/l_questionair/'.$url);
			}
			$user->insformfilename='';
			$user->insformimagename='';
		}
		elseif($type == 6){
			$files = json_decode($user->reportfilename);
			foreach($files as $url){
				Storage::delete('uploads/medical_reports/'.$url);
			}
			$user->reportfilename='';
			$user->reportimagename='';
		}

		$user->save();
		return response()->json(['status'=>1,'message'=>"Files deleted successfully"],200);
	}


	/*lawyer dashboard*/
	public function dashboard(Request $request)
	{		
			$user = auth()->user();
		    $users = User::find($user->id);
		   
		    $sendint = user_invitations::query();
			$sendint->where('inviter_user_id', '=', $user->id);
			$sendint =$sendint->paginate(3); 
			
			$patient = patient::query();
			$patient->where('lawyer_id', '=', $user->id);

			$clinic = User::where("role_id","4")->select("id","name")->get();
	
			//$invitations1 = user_invitations::where('inviter_user_id',$user->id);

			//print_r($request->all());
			//search
			if($request->has("patient_name")){
				$inputPatient = $request->patient_name;
				$patient->where('name', 'like', "%{$inputPatient}%");
				//$patient->orWhere('lname', 'like', "%{$inputPatient}%");
			}

			if($request->has('tracking_search')){
				$tracking_search = $request->tracking_search;
				$patient->where('name', 'like', "%{$tracking_search}%");
				//$patient->orWhere('lname', 'like', "%{$tracking_search}%");
			}

			$patient = $patient->get();	
			$searchIDs = [-1];
			foreach($patient as $row){
			   array_push($searchIDs,$row->id);
			}
			
			$invitations1 = user_invitations::where('inviter_user_id',$user->id);
			if($request->has('tracking_search')){
				$invitations1->whereIn('patient_user_id',$searchIDs);
			}
			
			$invitations1 = $invitations1->orderBy('invitation_date', 'DESC')->get();
			

			$settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
			$expireTime =date('Y-m-d H:i:s',strtotime('-'.$settings->invitation_expire_in_minutes.' minutes'));

			$sent=$accepted=$confirmed=$expired=$rejected=$cancelled= [];
			
			
			foreach($invitations1 as $invitation){
				
				if($invitation->status == 'pending' && strtotime($invitation->created_at) > strtotime($expireTime)) {
					$sent[]=$invitation;
				}
				elseif($invitation->status=="active" && $invitation->doctor_accepted_on != null && $invitation->is_patient_accepted=='no'){
					$accepted[]=$invitation;
				}
				elseif($invitation->status=="active" && $invitation->is_patient_accepted=='yes'){
					$confirmed[]=$invitation;
				}				
				elseif($invitation->status=='rejected'){
					$rejected[]=$invitation;
				}
				elseif($invitation->status == 'pending' && strtotime($invitation->created_at) <= strtotime($expireTime)) {
					$expired[]=$invitation;
				}
				elseif($invitation->status == 'cancelled') {
					$cancelled[]=$invitation;
				}
			}

			$iagree = Pagecms::where('page_slug','terms_adding_client')->first();
			$doctor = DB::select("select account_status from tbl_lawyers where user_id = '$user->id'");

			$selectedTab = isset($request->selectedTab) ? $request->selectedTab : 0;
            $desg = Designation::get();
			$desAr = [];
			foreach($desg as $it){
				$desAr[$it->id] = $it->designation;
			}

			$ac_status = count($doctor) > 0 ? $doctor[0]->account_status : 0;
		 
			$response =  [
				'ac_status'=> $ac_status,
				'sendint' => $sendint,
				'selectedTab' => $selectedTab,
				'activeTab'=>$request->activeTab,
				'users' => $users,
				'patient'=>$patient,
				'sent'=>$sent,
				'accepted'=>$accepted,
				'confirmed'=>$confirmed,
				'expired'=>$expired,
				'rejected'=>$rejected,
				'cancelled'=>$cancelled,
				'iagree'=>$iagree,
				'desgination' => $desAr,
				'clinic' => $clinic,
			];

			return view('/pages/lawyer-profile', $response);
			
	}


	public function changeLawyerPassword(Request $request){
		$old_password = $request->oldP;
		$new_password = Hash::make($request->newP);
		$user = auth()->user();		   
		$users = User::find($user->id);	

		if (Hash::check($old_password, $user->password)) {
			$user->password = $new_password;
			$user->save();
			return response()->json(['status'=> "success","message"=>"Password changed successfully"]);
		}
		else{
			return response()->json(['status'=> "mismatch","message"=>"Old password doesn`t match"]);
		}

	}


	public function lawyerTracking($lawyerID,$reportID){
		if(Auth::check() && (Auth::user()->role_id == 2 || Auth::user()->role_id == 3)){
			$lawyerID = Helper::Decrypt($lawyerID);
			$reportID = Helper::Decrypt($reportID);
			$user = User::where('id', $lawyerID)->get(); 
        
		  if($user && count($user) > 0)	{
			  $invitations = user_invitations::where('inviter_user_id',$lawyerID)->where('id',$reportID)->get();	
			  if($invitations && count($invitations) > 0){
				 return view("pages.lawyer.lawyer_tracking",['status'=>'1','reportID'=>$reportID]);
			  }
			  else{
				 return view("pages.lawyer.lawyer_tracking",['status'=>'0','reportID'=>$reportID]);
			  }
		  }
		  else{
			   return view("pages.lawyer.lawyer_tracking",['status'=>'0','reportID'=>$reportID]);
		  }

		}
		else{
			return redirect()->route('index',["login_pop"=>"true"]);
		}
	}
	
	
	/*update doctor page*/
	public function updatelawyer(Request $request){	
		
		$rules=[
				'image' => 'mimes:png,jpg,jpeg|max:1024',
		];
		$validator = Validator::make($request->all(), $rules);
			if ($validator->fails()) {
				$response['response'] = $validator->messages();
				//return $response;
				return response()->json(['status'=>'error', 'message'=>$validator->messages()]);					
			}
			else{
				
				if($request->hasFile('image')) {
					$file = $request->file('image');

					$filename = time().$request['phone'].'.'.$file->getClientOriginalExtension();
					//$image['filePath'] = $filename;
					$file->move('uploads/profilepics', $filename);
					//return response()->json(['status'=>'success', 'file'=>$filename]);
				}/*else{
					$filename="";
				}*/

				$user = User::where('id', $request->id)->first(); 
				$user->name = $request->name;
				//$user->email = $request->email;
				$user->phone = $request->phone;
				$user->business_address = $request->business_address;
				$user->location = $request->location;
				if(isset($filename)){
				$user->profilepic = $filename;;
				}
				if(($request->password)!=null){
					$password=Hash::make($request->password);
					$user->password = $password;
				}		
				$user->save();
				if(isset($user->id)){			
					$lawyer = lawyers::where('user_id', $user->id)->first();
					$lawyer->website_url= isset($request->website_url) ? $request->website_url : '';
					$lawyer->lawfirmname=isset($request->lawfirmname) ? $request->lawfirmname : '';
					$lawyer->account_status='1';
					$lawyer->postal_code=$request->postcode;
					$lawyer->save();
						if(isset($lawyer->id)){
						return response()->json(['status'=> "success", 'url'=>url('lawyer-profile')]);
						}
					else{ return response()->json(['status'=> "error"]); }
				}   
				
			}   
		
	}
	/*search doctor view*/
	public function searchdoctor(Request $request){
		
		  $user = auth()->user();
		   
		    $keyword=$request->keyword;
			$location=$request->location;
			$category_id=$request->category_id;
			$users = User::query();
			if(($keyword)!=null){
			$users->where('name', 'like', "%{$keyword}%");
			//$users->orWhere('email', 'like', "%{$keyword}%");  
			}
			if(($location)!=null){
				$users->where('location', '=', "$location");				
			}
			if(($category_id)!=null){				
				/*$users->whereHas('doctor', function($q) use($category_id){
                      $q->where('category_id', '=', $category_id);  });*/
			//	$users->whereRaw("find_in_set($category_id,category_id)");
			  $users->whereHas('doctor', function($q) use($category_id){
                      $q->whereRaw("find_in_set($category_id, category_id)");  });
			}
			$users->where('role_id', '=', 1);
		    $users->where('status', '=', "active");
			$users =$users->paginate(5);
			$users->appends(['keyword' => $keyword,'location'=>$location,'category_id'=>$category_id]);
			
			/*$doctorapp = Doctorappointment::where('doctor_id', 104)->select('appointment_date')->get();
			$newList=array();
			foreach($doctorapp as $key => $val){		
				$newList[$key] = [				
					'blacklist' => $val['appointment_date'],
				];		
			}*/
			
			
		//	return view('/pages/lawyer/search-doctor', ['users' => $users, 'newList' => $newList]);
			return view('/pages/lawyer/search-doctor', ['users' => $users]);

			/*$users1= User::whereHas('doctor', function($q) use($category_id){
			  $q->where('category_id', '=', $category_id);
		   })->get();  
		   dd($users1->doctor);*/
	}
	/*doctor calendar view*/
	public function viewcalendar($id, $categoryid = null){
		
		//doctor name
		$doctordata = User::where('id',$id)->first(); 

		$user = auth()->user();	
		$invitations = user_invitations::where('invitee_user_id',$id)->where('inviter_user_id',$user->id)->whereNotIn('status',['expired','rejected','cancelled','completed'])->get();	
		
		//print_r($invitations);exit;
		
		$settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
		$expireTime =strtotime('-'.$settings->invitation_expire_in_minutes.' minutes');

		$doctorHolidays = Holiday::where('doctor_user_id',$id)->where('date','>=',date('Y-m-d'))->get();
		$holidays = [];
		if($doctorHolidays) {
			foreach($doctorHolidays as $docHoliday) {
					$holidays[] = date('m/d/Y',strtotime($docHoliday->date));
			}
		}	
		//$assessment_days=['10/10/2021','10/11/2021'];		

		$newlist=array(); 
		$newlist1=array();
		$assessment_days=[];

	
		foreach($invitations as $key => $invitation){
			if($invitation->status == 'pending' && strtotime($invitation->created_at) <= $expireTime){
				continue;
			}
			$status_name = "";
			if($invitation->status == 'pending'){
				$status_name='Assessment requested';
			}elseif($invitation->status == 'active'){
				$status_name='Assessment Accepted';
			}
			$newlist1[$key] = [				
				'id'   =>  $invitation['unique_id'],
				'name' =>    $status_name,
				'date' => date('Y-m-d', strtotime($invitation['invitation_date'] .' +1 day')), 
				'timeslot' =>  $invitation['timeslot'],						
				'patientname' => $invitation->patient->name,
				'docid' => $id,
				'type'  => "holiday", 
				];
			
			$assessment_days[]=date('m/d/Y',strtotime($invitation['invitation_date']));			
		}
		// echo '<pre>';
		// print_r($newlist1);
		// echo '</pre>';
		// exit;

		$all = array_merge($newlist,$newlist1);
		 return view('/pages/lawyer/viewcalendar', 
		 [
		  'all' => $all,
		 'doctordata'=>$doctordata,
		 'holidays'=>json_encode($holidays),
		 'assessment_days'=>json_encode($assessment_days),
		 'input_category_id'=>$categoryid]);    
	}	


	/*Upload summary Docs*/
	public function uploaddocuments(Request $request)
    {	
		$intid=$request->newintid;
		$patientid=$request->patientid;
		$invitation = user_invitations::find($intid);

		$filep = $request->file('file')->path();
		$image = base64_encode(file_get_contents($filep));
		$image = MyHelper::Encrypt($image,$invitation->enceypKey);
					$chnagedname= date('Y').'/'. time().$intid.'_'.Str::random(15).'.txt';

                    $file = $request->file('file');
					$ext=$file->extension();
	            	$name = MyHelper::clearnName($file->getClientOriginalName(),$ext);

					Storage::disk('local')->put('uploads/l_summary/'.$chnagedname, $image);							
					$imgnameData[] = $name; 				
					$imgData[] = $chnagedname; 
			
			$user = InvitationAttachment::where('invitation_id', $intid)->first(); 
			if( $user!=null){
				if($user->filename!=null){					
					$filenamenew=$imgData;
					$filenamenew1=json_decode($user->filename);
					$cgefilename=array_merge($filenamenew, $filenamenew1);
					 $cgefilename = array_values($cgefilename);
					$user->filename=json_encode($cgefilename);
					
					$imgnamenew=$imgnameData;
					$imgnamenew1=json_decode($user->imagename);
					$cgeimagename=array_merge($imgnamenew, $imgnamenew1);
					 $cgeimagename = array_values($cgeimagename);
					$user->imagename=json_encode($cgeimagename);
					$user->save();				
					}
				else{
					$user->filename=json_encode($imgData);
					$user->imagename=json_encode($imgnameData);
					$user->save();
				}
			}			
			else{
				$data1 = new InvitationAttachment();
				$data1->invitation_id=$intid;
				$data1->filename=json_encode($imgData);
				$data1->imagename=json_encode($imgnameData);
				$data1->save();	
		    }

			$doctorinvitation = user_invitations::find($intid);
            $summarydocument='';
            return response()->json(['status'=>'1','message'=>'Files Uploaded Successfully']);             
    }  

	/*delete summary documents*/
	public function deletedocument(Request $request){ //aabb
		
		 $user = InvitationAttachment::where('invitation_id', $request->newintid)->first();

		 Storage::disk('local')->delete('uploads/l_summary/'.$request->DeleteDocsid);

		//  $delimgfile='uploads/docs/'.$request->DeleteDocsid;
		//  @unlink($delimgfile);	
			
		 $newfilename=$request->DeleteDocsid;
		 $num_array=json_decode($user->filename);
		// print_r( $num_array);
		 $num_array=json_decode( json_encode($num_array), true);		
		 $key = array_search($newfilename, $num_array);		
		 unset($num_array[$key]);
		 $num_array=array_values($num_array);
		 
		 
		 $newimagename=$request->DeleteDocsname;
		 $num_array1=json_decode($user->imagename);
		 $num_array1=json_decode( json_encode($num_array1), true);		
		 $key1 = array_search($newimagename, $num_array1);		
		 unset($num_array1[$key1]);	
		 $num_array1=array_values($num_array1);
		  
		 //print_r(json_encode($num_array1));		 
		 //exit;
		$num_array1count=count($num_array1);
		$num_arraycount=count($num_array);
		//exit;
		 if($num_array1count=='0'){
			 $user->imagename='';
			 $user->filename='';
			 $user->save();
		 }else{
		 $user->imagename=json_encode($num_array1);
		 $user->filename=json_encode($num_array);
		 $user->save();
		 }		
	//	return response()->json(["status" =>  "success"]);
	     $doctorinvitation = user_invitations::find($request->newintid);
		 return response()->json(['status'=>'success','activeliid'=>$request->activeliid, 'int_id'=>$request->newintid]);  
	}
	
	/*delete loe*/
	public function deleteloe(Request $request){
		
		 $user = InvitationAttachment::where('invitation_id', $request->loenewintid)->first();
		 Storage::disk('local')->delete('uploads/l_LetterOfEngagement/'.$request->DeleteDocsid);

		//  $delimgfile='uploads/loe/'.$request->loeDeleteDocsid;
		// @unlink($delimgfile);	
			
		 $newfilename=$request->loeDeleteDocsid;
		 $num_array=json_decode($user->loefilename);
		// print_r( $num_array);
		 $num_array=json_decode( json_encode($num_array), true);		
		 $key = array_search($newfilename, $num_array);		
		 unset($num_array[$key]);
		 $num_array=array_values($num_array);
		 
		 
		 $newimagename=$request->loeDeleteDocsname;
		 $num_array1=json_decode($user->loeimagename);
		 $num_array1=json_decode( json_encode($num_array1), true);		
		 $key1 = array_search($newimagename, $num_array1);		
		 unset($num_array1[$key1]);	
		 $num_array1=array_values($num_array1);
		  
		
		$num_array1count=count($num_array1);
		$num_arraycount=count($num_array);
		//exit;
		 if($num_array1count=='0'){
			 $user->loeimagename='';
			 $user->loefilename='';
			 $user->save();
		 }else{
		 $user->loeimagename=json_encode($num_array1);
		 $user->loefilename=json_encode($num_array);
		 $user->save();
		 }
		 $doctorinvitation = user_invitations::find($request->loenewintid);
		 return response()->json(['status'=>'success','activeliid'=>$request->activeliid,'int_id'=>$request->loenewintid]);  
	}
	/*delete form53*/
	public function deleteform53(Request $request){
		
		 $user = InvitationAttachment::where('invitation_id', $request->form53newintid)->first();
		 Storage::disk('local')->delete('uploads/l_form_53/'.$request->DeleteDocsid);

		// $delimgfile='uploads/form53/'.$request->loeDeleteDocsid;
		// @unlink($delimgfile);	
			
		 $newfilename=$request->form53DeleteDocsid;
		 $num_array=json_decode($user->form53filename);
		// print_r( $num_array);
		 $num_array=json_decode( json_encode($num_array), true);		
		 $key = array_search($newfilename, $num_array);		
		 unset($num_array[$key]);
		 $num_array=array_values($num_array);
		 
		 
		 $newimagename=$request->form53DeleteDocsname;
		 $num_array1=json_decode($user->form53imagename);
		 $num_array1=json_decode( json_encode($num_array1), true);		
		 $key1 = array_search($newimagename, $num_array1);		
		 unset($num_array1[$key1]);	
		 $num_array1=array_values($num_array1);
		  
		
		$num_array1count=count($num_array1);
		$num_arraycount=count($num_array);
		//exit;
		 if($num_array1count=='0'){
			 $user->form53imagename='';
			 $user->form53filename='';
			 $user->save();
		 }else{
		 $user->form53imagename=json_encode($num_array1);
		 $user->form53filename=json_encode($num_array);
		 $user->save();
		 }
		 $doctorinvitation = user_invitations::find($request->form53newintid);
		 return response()->json(['status'=>'success','activeliid'=>$request->activeliid, 'int_id'=>$request->form53newintid]);  
	}


	/*delete Medical file*/
	public function delete_medical_file(Request $request){ 
		
		$user = InvitationAttachment::where('invitation_id', $request->medicalsformnewintid)->first();
		// echo 'l_medical/'.$request->medicalsDeleteDocsid; exit;
		Storage::delete('uploads/l_medical/'.$request->medicalsDeleteDocsid);
		//$delimgfile='uploads/medicals/'.$request->medicalsDeleteDocsid;
		//@unlink($delimgfile);	
		   
		$newfilename=$request->medicalsDeleteDocsid;
		$num_array=json_decode($user->medicalsfilename);
	   // print_r( $num_array);
		$num_array=json_decode( json_encode($num_array), true);		
		$key = array_search($newfilename, $num_array);		
		unset($num_array[$key]);
		$num_array=array_values($num_array);
		
		
		$newimagename=$request->medicalsDeleteDocsname;
		$num_array1=json_decode($user->medicalsimagename);
		$num_array1=json_decode( json_encode($num_array1), true);		
		$key1 = array_search($newimagename, $num_array1);		
		unset($num_array1[$key1]);	
		$num_array1=array_values($num_array1);
		 
	   
	   $num_array1count=count($num_array1);
	   //exit;
		if($num_array1count=='0'){
			$user->medicalsimagename='';
			$user->medicalsfilename='';
			$user->save();
		}else{
		$user->medicalsimagename=json_encode($num_array1);
		$user->medicalsfilename=json_encode($num_array);
		$user->save();
		}

		$doctorinvitation = user_invitations::find($request->medicalsformnewintid);
		return response()->json(['status'=>'success','activeliid'=>$request->activeliid, 'int_id'=>$request->medicalsformnewintid]);  
   } 

	
	/*delete insform*/
	public function deleteinsform(Request $request){
		
		 $user = InvitationAttachment::where('invitation_id', $request->insformnewintid)->first();
		 Storage::disk('local')->delete('uploads/l_questionair/'.$request->DeleteDocsid);

		// $delimgfile='uploads/insform/'.$request->loeDeleteDocsid;
		// @unlink($delimgfile);	
			
		 $newfilename=$request->insformDeleteDocsid;
		 $num_array=json_decode($user->insformfilename);
		// print_r( $num_array);
		 $num_array=json_decode( json_encode($num_array), true);		
		 $key = array_search($newfilename, $num_array);		
		 unset($num_array[$key]);
		 $num_array=array_values($num_array);
		 
		 
		 $newimagename=$request->insformDeleteDocsname;
		 $num_array1=json_decode($user->insformimagename);
		 $num_array1=json_decode( json_encode($num_array1), true);		
		 $key1 = array_search($newimagename, $num_array1);		
		 unset($num_array1[$key1]);	
		 $num_array1=array_values($num_array1);
		  
		
		$num_array1count=count($num_array1);
		$num_arraycount=count($num_array);
		//exit;
		 if($num_array1count=='0'){
			 $user->insformimagename='';
			 $user->insformfilename='';
			 $user->save();
		 }else{
		 $user->insformimagename=json_encode($num_array1);
		 $user->insformfilename=json_encode($num_array);
		 $user->save();
		 }
		 $doctorinvitation = user_invitations::find($request->insformnewintid);
		 return response()->json(['status'=>'success','insformactiveliid'=>$request->insformactiveliid, 'int_id'=>$request->insformnewintid]);  
		 
	}


	/*delete deletehyperlink*/
	public function deletehyperlink(Request $request){
		
		 $user = InvitationAttachment::where('invitation_id', $request->linkinvitationid)->first();
		 $user->medicals_hyperlink='';	
		 $user->save();	
         $invitation = user_invitations::where('id', $request->linkinvitationid)->first();		 
		 return response()->json(['status'=>'success','url'=>url('patient_details')."/".$invitation->patient_user_id."#Reports"]);  
	}
	
	/*add patient */

	public function addpatient(Request  $request)
    {  
		$helper = new Helper();	
		$patient = patient::where("email",$request->email)->where("lawyer_id",Auth::id())->get();
		$checkUser = User::where("email",$request->email)->get();

		if(count($patient) > 0 || count($checkUser) > 0){
			return response()->json(['status'=>'exist', 'message'=>"Patient with this email address exit, try different one"]);
		}
		else{

			$newUser = new User();
			$user = auth()->user();
			$patient = new patient();

			$username = $request->email;
			$password = ucfirst(Helper::randomString(10));

			$request->email = $helper->Encrypt($request->email);

			$patient->lawyer_id=$user->id;   
			$patient->fname=$request->fname;
			$patient->lname=$request->lname;
			$patient->name=$request->fname.' '.$request->lname;
			$patient->email=$request->email;
			$patient->dateofaccident=$request->doa;
			$patient->dateofbirth=$request->dob;
			$patient->address=$helper->Encrypt($request->address);	
			$patient->casetype=$request->casetype;	
			$patient->gender=$helper->Encrypt($request->gender);	
			$patient->phonenumber= $helper->Encrypt($request->phonenumber);
			$patient->filenumber= Helper::Encrypt($request->filenumber);
			$patient->save();		

		   $last_insertid=$patient->id;

		   if(isset($last_insertid)){
				$data = [
					'patient_name' => $request->fname . " " . $request->lname,
					'lawyer_name' => $user->name,
					'username' => $username,
					'password' => $password,
				];

				$email_verify_token = md5(time());
				$uniqueid = md5(uniqid());				
				$newUser->name= $request->fname.' '.$request->lname;
				$newUser->email= $username;
				$newUser->unique_id= $uniqueid;
				$newUser->password= Hash::make($password);
				$newUser->phone= $request->phonenumber;
				$newUser->business_address= $request->address;
				$newUser->remember_token= '';
				$newUser->email_verify_token= $email_verify_token;
				$newUser->desgination= '';
				$newUser->role_id= '5';
				$newUser->status= 'active';
				$newUser->authentication_status= 'active';
				$newUser->patient_id = $last_insertid;
				$newUser->save();

				$email_controller = new EmailController();					
				$email_controller->sendPatientAccountCreatedEmail($data);   

		      return response()->json(['status'=>'success', 'message'=>" Thank You Request Sent Successfully"]);
		   }
		   else{
			  return response()->json(['status'=>'exist', 'message'=>"Something went wrong try again"]);
		   }	

	  }
    }
	
	/*edit patient */

	public function editpatient(Request  $request)
    {   
		$user = auth()->user();
		$patient = patient::find($request->id);
		$userData = User::where('patient_id',$request->id)->where("email",Helper::Decrypt($patient->email))->first();
		
		$patient->lawyer_id=$user->id;   
		$patient->fname=$request->fname;
		$patient->lname=$request->lname;
		$patient->name=$request->fname." ".$request->lname;
		$patient->dateofaccident=$request->doa;
		$patient->dateofbirth=$request->dob;
		$patient->address=Helper::Encrypt($request->address);	
		$patient->casetype= $request->casetype;	
		$patient->gender=Helper::Encrypt($request->gender);	
		$patient->phonenumber=Helper::Encrypt($request->phonenumber);	
		$patient->filenumber=Helper::Encrypt($request->filenumber);	  
		$patient->save();

		$last_insertid = $patient->id;
				
		$userData->name= $request->fname.' '.$request->lname;
		$userData->phone= $request->phonenumber;
		$userData->business_address= $request->address;
		$userData->save();

		$notif = new Nofication();
		$notif->description = "Your account has been updated by your lawyer ".Auth::user()->name;
		$notif->url = 'patient-profile';
		$notif->status = '0';
		$notif->to_id = $userData->id;
		$notif->from_id = $user->id;
		$notif->save();

		if(isset($last_insertid)){
		   return response()->json(['status'=>'success', 'message'=>" Thank You Request Sent Successfully",'url'=>url('patient_details')."/".$request->id."?clientinformation=true"]);
		}		
    }
	
	/*delete profilepic*/
	public function deletelawprofilepic(Request $request){
		
		$id=$request->userid;
		$user = User::where('id', $id)->first(); 
		
		$user->profilepic = "";
		$user->save();
		if(isset($user->id)){ 
			return response()->json(['status'=> "success",'url'=>url('lawyer-profile')]);
		}	
			
	}
	
	/*patient details view calendar*/
	public function patientdetails(Request $request){
		$user = auth()->user();
		$doctor_category_id = $request->doctor_category_id ? : null;
		$doctor_user_id = $request->doctor_user_id ? : NULL;
		$settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
		$expireTime =date('Y-m-d H:i:s',strtotime('-'.$settings->invitation_expire_in_minutes.' minutes'));
		
		$patients = patient::where('lawyer_id',$user->id)->select('id','fname','lname')->get();

		$virtual_appointment = doctors::where('user_id',$request->doctor_user_id)->select(['virtual_appointment'])->first();

		if($patients!=null){
			$output_patients = [];
			foreach($patients as $patient) {
				if($patient->invitations && $doctor_category_id) {
					$check = user_invitations::where('patient_user_id',$patient->id)->where('invitee_category_id',$doctor_category_id)
					->where('status','!=','rejected')->where(function($q) use($expireTime){
						$q->where(function($q1){
							$q1->whereIn('status',['active','completed']);
						})->orWhere('created_at','>',$expireTime)->where('status','pending');
					})->exists();
					if($check) {
						continue;
					}

					$invitationData = ['id'=>$patient->id,'name'=>ucfirst($patient->fname)." ".ucfirst($patient->lname),'is_already_sent'=>false, 'is_cancelled'=>false];
					foreach($patient->invitations as $invitation) {
						if($invitation->invitee_user_id == $doctor_user_id && $invitation->invitee_category_id == $doctor_category_id ) {
							$invitationData['is_already_sent'] = $invitation->status == 'rejected'?'rejected':($invitation->status != 'completed'? 'expired'  : false);
							$invitationData['is_cancelled'] = $invitation->status == 'cancelled'?'cancelled':($invitation->status != 'completed'? 'expired'  : false);
							$invitationData['name'] = $invitation->fname." ".$invitation->lname;
						}
					}
				}else{
					$invitationData = ['id'=>$patient->id,'name'=>ucfirst($patient->name),'is_already_sent'=>false, 'is_cancelled'=>false];
				}
				$output_patients[] = $invitationData;
			}
			return response()->json(['status'=>'success', 'patientdetails'=>$output_patients,'virtual_appointment'=>$virtual_appointment]);
		}else{
			return response()->json(['status'=>'error', 'patientdetails'=>"No Patients Found", 'virtual_appointment'=>$virtual_appointment]);

		}

	}
	/*doctor slots */
	public function doctorslots(Request $request){

		$doctor_id=$request->id;
		$date=$request->date;	
		$appointment_date=date("Y-m-d", strtotime($date));
		
		$settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
		$expireTime =date('Y-m-d H:i:s',strtotime('-'.$settings->invitation_expire_in_minutes.' minutes'));
		$doctordates = Doctorappointment::where('appointment_date',$appointment_date)->where('doctor_id', $doctor_id)->select('timeslot')->first();
		$timeslots = DoctorTimeSlot::select(['time_slot'])->orderBy('time_slot','ASC')->get();
		$doctor_invitation_slots = user_invitations::select(['timeslot','invitation_date'])->where('invitee_user_id',$doctor_id)->where(function($q) use($expireTime){	
			$q->where(function($q1) use($expireTime){
				$q1->where('status','pending')->where('created_at','>',$expireTime);					
			})->orWhere(function($q1){
				$q1->whereIn('status',['active']);					
			});	
		})->get();

			$doctorSlots = [];
			if($doctor_invitation_slots){					
				foreach($doctor_invitation_slots as $slot) {
					$slot_time = date('Y-m-d H:i:s',strtotime($slot->invitation_date.' '.$slot->timeslot));
					$doctorSlots[] = $slot_time;
				}
			}
			$doctor_invitations = $timeslot11=[];
			if($doctordates) {
				$timeslot11=explode(",",$doctordates->timeslot);
			}
			foreach($timeslots as $timeslot) {
				$slotTime = strtotime($appointment_date.' '.$timeslot->time_slot);
				$time_slot_arr = ['timeslot'=>date('h:i A',strtotime($timeslot->time_slot)),'is_booked'=>false];
				if(in_array(date('Y-m-d H:i:s',$slotTime), $doctorSlots) ){
					$time_slot_arr['is_booked'] = true;
				}else if($appointment_date == date('Y-m-d')){
					if($slotTime < time()){
						$time_slot_arr['is_booked'] = true;
					} 
				}
				if(in_array(date('h:i A',strtotime($timeslot->time_slot)),$timeslot11)) {
					$time_slot_arr['is_booked'] = true;
				}

				$doctor_invitations[] = $time_slot_arr;
			}	

			$doctorBlockedHoliday = Holiday::where('doctor_user_id',$doctor_id)->where('date','=',date("Y-m-d", strtotime($date)))->first();

		return response()->json([
			'status'=>'success', 
		    'timeslot'=>$doctor_invitations, 
		    'doctorBlockedHoliday'=>$doctorBlockedHoliday
	]);
	}
	
	/*doctor slots */
	public function doctorslots_old(Request $request){
		$user = auth()->user();

		$id=$request->id;
		$date=$request->date;	
		$appointment_date=date("Y-m-d", strtotime($date));
		
		
		$timeslot = DoctorTimeSlot::select(['time_slot'])->get()->toArray();

		$lawyerint = Doctorappointment::query();
		$lawyerint->where('doctor_id', '=', $id);
		$lawyerint->where('appointment_date', '=', $appointment_date);		
		$lawyerint=$lawyerint->select('timeslot')->first();
		
		//$count = count($lawyerint);		
		$count='0';
		//echo $count;
		if($lawyerint!=''){		
				$lawyerint = json_decode(json_encode($lawyerint));
				$lawyerint = json_decode(json_encode($lawyerint),true);
				
				$lawyerint=explode(',',$lawyerint['timeslot']);
				//print_r($lawyerint);
				//exit;
		}else{
			$lawyerint=$timeslot;
		}
				//$result = !empty(array_intersect($timeslot, $lawyerint));
				
				$clean1 = array_diff($timeslot, $lawyerint); 
		$clean2 = array_diff($lawyerint, $timeslot); 
		$final_output = array_intersect($timeslot, $lawyerint);
		//print_r($final_output);
				/*lawyer timeslot */
				$usrint = user_invitations::query();
				$usrint->where('inviter_user_id', '=', $user->id);
				$usrint->where('invitee_user_id', '=', $id);
				$usrint->where('invitation_date','=', $appointment_date);
				$usrint=$usrint->select('timeslot')->get();	
				
				$count1 = count($usrint);		
				if($count1!='0'){ 
				$usrint = json_decode(json_encode($usrint));
				$usrint = json_decode(json_encode($usrint),true);
				$array_test=array();
				foreach($usrint as $item) 
				{
					$array_test[]= $item['timeslot'];
					
				}
				}else{
			$array_test=array();
		}
//print_r($array_test);
//exit;
	//	$lawyerint=explode(',',$lawyerint['usrint']);
	//	$result = !empty(array_intersect($timeslot, $lawyerint));
		
		$clean1 = array_diff($final_output, $array_test); 
		$clean2 = array_diff($array_test, $final_output); 
		$final_output = array_filter(array_merge($clean1, $clean2));
		//echo count($final_output);
		if(count($final_output)=='0'){
		$final_output=	$timeslot;
		}else{
		$final_output=$final_output;	
		}
			return response()->json(['status'=>'success', 'timeslot'=>$final_output]);
		
	}

	public function getDatesFromRange($start, $end, $format = 'Y-m-d') 
	{
		$array = array();
		$interval = new DateInterval('P1D');
	  
		$realEnd = new DateTime($end);
		$realEnd->add($interval);
	  
		$period = new DatePeriod(new DateTime($start), $interval, $realEnd);
		foreach($period as $date) {                 
			$array[] = $date->format($format); 
		}
		return $array;
	}

	
	public function patient_details($patientid,$active_tab = null){		
	
		$desg = Designation::get();
		$desAr = [];
		foreach($desg as $it){
			$desAr[$it->id] = $it->designation;
		}
		$patient = patient::find($patientid);
		$settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
		$expireTime = date('Y-m-d H:i:s',strtotime('-'.$settings->invitation_expire_in_minutes.' minutes'));

		$categories = Category::select(['id','category_name'])->get();

		// foreach($categories as $row){
		// 	echo "<pre>";
		// 	print_r($row); exit;
		// 	echo "</pre>";
		// }
		// exit;
		
		return view('/pages/lawyer/patient_details',
		[
		  'patient' => $patient, 
		  'expire_time'=>$expireTime,
		  'active_tab' => $active_tab,
		  'patientid'=>$patientid, 
		  'categories'=>$categories,
		  'desgination' => $desAr
		  ]
		);		
	}	

	/*delete patient*/	
	public function patientdelete(Request $request){		
	if($request->id) {						
		$id=$request->id;					
		$patientInt = user_invitations::where('patient_user_id', $id)->first();					
		if($patientInt!=null){								
		   return response()->json(['status'=> "error"]);						
		}
		else
		{		
			$userData = User::where('patient_id',$id)->first();	
			$name = $userData->name;
			$email = $userData->email;

			$patient = patient::where('id',$id)->delete();			
			$user = User::where('patient_id',$id)->delete();	
			
			$data = [
				'name' => $name,
				'email' => $email,
			];

			$email_controller = new EmailController();					
			$email_controller->sendDeletePatientEmail($data); 
			
			return response()->json(['status'=> "success",'url'=>url('lawyer-dashboard')."/my-patients"]);	
		}				
	}
  }		
	
	
	public function update_transport_transalation(Request $request)
	{
		$inv = user_invitations::find($request->invitation_id);
		if($inv) {
			$inv->transportation = 'yes';
			$inv->save();
		}
		return ['data'=>$inv,'status'=>'success','message'=>'Request sent'];	
	}
	
	public function update_transalation(Request $request)
	{
		$inv = user_invitations::find($request->invitation_id);
		if($inv) {
			$inv->translation = 'yes';
			$inv->save();
		}
		return ['data'=>$inv,'status'=>'success','message'=>'Request sent'];	
	}
	public function update_amendment(Request $request)
	{
		$inv = user_invitations::find($request->invitation_id);
		if($inv) {
			$inv->amendment = 'yes';
			$inv->save();
		}
		return ['data'=>$inv,'status'=>'success','message'=>'Request sent'];	
	}

	public function cancel_appointment(Request $request)
	{
		$user = auth()->user();
		$inv = user_invitations::find($request->invitation_id);
		if($inv) {
			$inv->status = 'cancelled';
			$inv->save();
		}
		if($inv->id){
			$invitation = user_invitations::find($request->invitation_id);

			$data = Patient::find($invitation->patient_user_id);

			$des = Helper::c_strings()["cancelled_app"];
			$des = str_replace("@patient",$data->name,$des);
			$des = str_replace("@date",date("d-m-Y H:i"),$des);

            $notif = new Nofication();
			$notif->url = "doctor-dashboard/my-invitations/#cancelled-invitations";
			$notif->description = $des;;
			$notif->from_id = $invitation->inviter_user_id;
			$notif->to_id = $invitation->invitee_user_id;
			$notif->save();

			
			$emailCtrl = new EmailController();
			$emailCtrl->send_notification_email_to_doctor_after_lawyer_cancel_appointment($invitation);
			$emailCtrl->send_notification_email_to_patient_after_lawyer_cancel_appointment($invitation);
		}


		$pt_data = User::select("id")->where('patient_id',$invitation->patient_user_id)->first();
		$notif_1 = new Nofication();
		$notif_1->description = "Your appointment created by your lawyer: ".Auth::user()->name." has been cancelled.";
		$notif_1->url = 'invitation_details_view/'.$invitation->id;
		$notif_1->status = '0';
		$notif_1->to_id = $pt_data->id;
		$notif_1->from_id = $user->id;
		$notif_1->save();

		return ['data'=>$inv,'status'=>'success','message'=>'Request sent','url'=>url('lawyer-dashboard')."/tracking#cancelled-invitations"];	
	}

	/**
	 * This function not using check $this->upload_medicals.
	 */
	public function medicals(Request $request){
		
		$medicals_hyperlink=$request->medicals_hyperlink;
		$invitation_id=$request->invitation_id;
		
		$user = InvitationAttachment::where('invitation_id', $invitation_id)->first(); 
		if($user!=null)
		{
			$user->medicals_hyperlink = $medicals_hyperlink;
			$user->save();
		}else{
			$user = new InvitationAttachment();
			$user->medicals_hyperlink = $medicals_hyperlink;
			$user->invitation_id = $invitation_id;
			$user->save();
		}
		$userintvitation = user_invitations::where('id', $invitation_id)->first(); 
		if(isset($user->id)){		
			$invitation = user_invitations::find($invitation_id);
			if($invitation->status=="active"){
				$emailCtrl = new EmailController();
				$emailCtrl->send_notification_email_to_doctor_after_lawyer_upload_documents($invitation);
			}			
			return response()->json(['status'=> "success", 'message'=>" Thank You Request Sent Successfully",'url'=>url('patient_details')."/".$userintvitation->patient_user_id."#Reports"]);
		}
				
	}
/**
 * Uploading Medicals 27/08/21. @Balu
 * Changed medical tabs from URLs to files.
 */
	public function upload_medicals(Request $request)
	{
		$request->validate([
			'file' => 'required',
		    'file.*' => 'mimes:docx,pdf,doc|max:2000000'
		 ]);
		 
		 $intid=$request->medicalsnewintid;
		 $patientid=$request->medicalspatientid;
		 $inv_attchment = InvitationAttachment::where('invitation_id', $intid)->first(); 
		 $invitation = user_invitations::find($intid);

		 $medical_filenames = [];
		 $medical_imagenames = [];
		 if($inv_attchment && $inv_attchment->medicalsimagename && $inv_attchment->medicalsfilename){
			$medical_filenames = json_decode($inv_attchment->medicalsfilename);
			$medical_imagenames = json_decode($inv_attchment->medicalsimagename);
	     }

		 $filep = $request->file('file')->path();
		 $image = base64_encode(file_get_contents($filep));
		 $image = MyHelper::Encrypt($image,$invitation->enceypKey);
		 $chnagedname= date('Y').'/'. time()."_".'_'.Str::random(20).'.txt';

		 $file = $request->file('file');
		 $ext=$file->extension();
		 $name = MyHelper::clearnName($file->getClientOriginalName(),$ext);
		

		 Storage::disk('local')->put('uploads/l_medical/'.$chnagedname, $image);

		 $fileName = time()."_".Str::random(10);						
		 $medical_filenames[] = $chnagedname; 
		 $medicaldocumentchangedname[]= $chnagedname; 				
		 $medical_imagenames[] = $name;  
		 $medicaldocumentname[]= $name;   

		 if(!$inv_attchment) {
			$inv_attchment = InvitationAttachment::create([
				'invitation_id'=>$intid,
				'medicalsfilename'=>json_encode($medical_filenames),
				'medicalsimagename'=>json_encode($medical_imagenames),
			]);
		}else{
			$inv_attchment->medicalsfilename=json_encode($medical_filenames);
			$inv_attchment->medicalsimagename=json_encode($medical_imagenames);
			$inv_attchment->save();	
		}	
		chmod(public_path().'/uploads/medicals/encrypted',0600);

		if($inv_attchment->invitation->status=="active"){
			$emailCtrl = new EmailController();
			$doctor = $inv_attchment->invitation->user1;
			//aaaa
			$email = $name = "";
			if($doctor->clinic_id > 0){
				$clinic = User::find($doctor->clinic_id);
				$email = $clinic->email;
				$name = $clinic->name;
			}else{
				$email = $doctor->email;
				$name = $doctor->name;
			}
			
			$emailCtrl->send_notification_email_to_doctor_after_lawyer_upload_documents($inv_attchment->invitation,$name,$email);
		}

		return response()->json(['status'=>'1','message'=>'Files Uploaded Successfully']);     
	}

	// public function upload_medicals_backup(Request $request)
	// {

	// 	$request->validate([
	// 		'medicalsfiles' => 'required',
	// 	    'medicalsfiles.*' => 'mimes:docx,pdf,doc|max:2000000'
	// 	 ]);
		 
	// 	 $intid=$request->medicalsnewintid;
	// 	 $patientid=$request->medicalspatientid;
	// 	 $Tfiles = $request->TotalFiles;
	// 	 $inv_attchment = InvitationAttachment::where('invitation_id', $intid)->first(); 
		 
	// 	 $medical_filenames = [];
	// 	 $medical_imagenames = [];
	// 		if($inv_attchment && $inv_attchment->medicalsimagename && $inv_attchment->medicalsfilename){
	// 			$medical_filenames = json_decode($inv_attchment->medicalsfilename);
	// 			$medical_imagenames = json_decode($inv_attchment->medicalsimagename);
	// 		}

	// 	 if($Tfiles > 0)
	// 	 {        
	// 	//	chmod(public_path().'/uploads/medicals/encrypted',0755);
	// 		for ($x = 0; $x < $Tfiles; $x++) 
	// 		{	 
	// 				if ($request->hasFile('files'.$x)) 
	// 				{
	// 					$filep = $request->file('files'.$x)->path();
	// 				    $image = base64_encode(file_get_contents($filep));
	// 					 $image = MyHelper::Encrypt($image,Auth::user()->unique_id);
	// 					$chnagedname= date('Y').'/'. time().$x.'_'.Str::random(12).'.txt';

	// 					$file = $request->file('files'.$x);
	// 					$ext=$file->extension();
	//	$name = MyHelper::clearnName($file->getClientOriginalName(),$ext);

	// 					Storage::disk('local')->put('uploads/l_medical/'.$chnagedname, $image);

	// 					$fileName = time().$x;
	// 					//$chnagedname= $fileName.'.'.$ext;							
	// 					$medical_filenames[] = $chnagedname; 
	// 					$medicaldocumentchangedname[]= $chnagedname; 				
	// 					$medical_imagenames[] = $name;  
	// 					$medicaldocumentname[]= $name;    
	// 					//$file->move(public_path().'/uploads/medicals/', $chnagedname);//move to folder		
	// 					//$this->encryptFile(public_path().'/uploads/medicals/'.$chnagedname, public_path().'/uploads/medicals/encrypted/'.$fileName.'.enc');
	// 					//@unlink(public_path().'/uploads/medicals/', $chnagedname);   						
	// 				}				
	// 		}
				
	// 			if(!$inv_attchment) {
	// 				$inv_attchment = InvitationAttachment::create([
	// 					'invitation_id'=>$intid,
	// 					'medicalsfilename'=>json_encode($medical_filenames),
	// 					'medicalsimagename'=>json_encode($medical_imagenames),
	// 				]);
	// 			}else{
	// 				$inv_attchment->medicalsfilename=json_encode($medical_filenames);
	// 				$inv_attchment->medicalsimagename=json_encode($medical_imagenames);
	// 				$inv_attchment->save();	
	// 			}	
	// 			chmod(public_path().'/uploads/medicals/encrypted',0600);
				
	// 			if($inv_attchment->invitation->status=="active"){
	// 				$emailCtrl = new EmailController();
	// 				$emailCtrl->send_notification_email_to_doctor_after_lawyer_upload_documents($inv_attchment->invitation);
	// 			}	
	// 			$doctorinvitation = user_invitations::find($intid);
	// 			$medicaldocument='';
	// 			foreach($medicaldocumentname as $key => $medname ){
	// 				$medicaldocument.="<li id='medicals$intid$key'>
	// 			 <span><a onclick='document.getElementById(`medical-file-download-form$key`).submit()'  href='#' style='text-decoration:none;' >$medname</a></span>
	// 			 <a href='javascript:void(0)'  onClick='reply_click_medicals(this.id,`$medname`, `$intid`, `$doctorinvitation->patient_user_id`,`medicals$intid$key`)'   id='$medicaldocumentchangedname[$key]'>  <i class='fa fa-close' style='font-size: 18px;color: red;float: right;' ></i></a></li>
	// 			 <form id='medical-file-download-form$key'  action='".url('download-medicals-file')."'  method='post'>
	// 			 <input type='hidden' name='_token' value='".csrf_token()."' />
	// 			 <input type='hidden' name='filename' value='$medicaldocumentchangedname[$key]'>
	// 			 <input type='hidden' name='user_id' value=$doctorinvitation->inviter_user_id />
	// 			 <input type='hidden' name='download_name' value='$medname'>
	// 			 </form>";
	// 			} 
	// 		return response()->json(['success'=>'Files Uploaded Successfully',  'medicaldocument'=>$medicaldocument,'medicaldocumentid'=>$intid]);   
	// 	 }
	// 	 else
	// 	 {
	// 		return response()->json(["message" => "Please try again."]);  
	// 	 }   
	// }

	/**
	 * Download medical file 
	 * decrypt and download
	 */
	public function download_medical_file(Request $request)
	{
		if($filename = $request->filename) {
			if($request->user_id != auth()->id()){
				abort(401,'Unauthorized');
			}
			chmod(public_path().'/uploads/medicals/encrypted',0755);
			$file_name = $x = pathinfo($filename, PATHINFO_FILENAME);
			$ext = pathinfo($request->download_name, PATHINFO_EXTENSION);
			$destPath = public_path().'/uploads/medicals/temp/'.$file_name.'.'.$ext;   	
			$this->decryptFile(public_path().'/uploads/medicals/encrypted/'.$file_name.'.enc',$destPath);
			header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.$request->download_name.'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($destPath));
            flush(); // Flush system output buffer
            readfile($destPath);	
			@unlink($destPath);				
			chmod(public_path().'/uploads/medicals/encrypted',0600);
		}
	}
	
	/*Upload Loe*/
	public function uploadLetterOfEngagementDocs(Request $request)
    {
		$intid=$request->loenewintid;
		$patientid=$request->loepatientid;
		$invitation = user_invitations::find($intid);

		$filep = $request->file('file')->path();
		$image = base64_encode(file_get_contents($filep));
		$image = MyHelper::Encrypt($image,$invitation->enceypKey);
		$chnagedname= date('Y').'/'. time().$intid.'_'.Str::random(12).'.txt';

		$file = $request->file('file');
		$ext=$file->extension();
		$name = MyHelper::clearnName($file->getClientOriginalName(),$ext);

		Storage::disk('local')->put('uploads/l_LetterOfEngagement/'.$chnagedname, $image);							
		$imgnameData[] = $name; 				
		$imgData[] = $chnagedname;        						
          
		$user = InvitationAttachment::where('invitation_id', $intid)->first(); 
		
        if( $user!=null){
			if($user->loefilename != null){					
				$filenamenew=$imgData;
				$filenamenew1=json_decode($user->loefilename);
				
				$cgefilename=array_merge($filenamenew, $filenamenew1);
				$cgefilename = array_values($cgefilename);
				$user->loefilename=json_encode($cgefilename);
				
				$imgnamenew=$imgnameData;
				$imgnamenew1=json_decode($user->loeimagename);
				$cgeimagename=array_merge($imgnamenew, $imgnamenew1);
				$cgeimagename = array_values($cgeimagename);
				$user->loeimagename=json_encode($cgeimagename);
				$user->save();				
				}
			else{
				$user->loefilename=json_encode($imgData);
				$user->loeimagename=json_encode($imgnameData);
				$user->save();
			}
		}			
		else{
			$data1 = new InvitationAttachment();
			$data1->invitation_id=$intid;
			$data1->loefilename=json_encode($imgData);
			$data1->loeimagename=json_encode($imgnameData);
			$data1->save();	
		}

		return response()->json(['status'=>"1",'message'=>'Files Uploaded Successfully']);  
 
    }


	
	/*Upload Letter of form53 Docs*/
	public function uploadForm53Docs(Request $request)
    {
		$intid=$request->form53newintid;
		$patientid=$request->form53patientid;
		$invitation = user_invitations::find($intid);

		$filep = $request->file('file')->path();
		$image = base64_encode(file_get_contents($filep));
		$image = MyHelper::Encrypt($image,$invitation->enceypKey);
		$chnagedname= date('Y').'/'. time().$intid.'_'.Str::random(15).'.txt';

		$file = $request->file('file');
		$ext=$file->extension();
		$name = MyHelper::clearnName($file->getClientOriginalName(),$ext);

		Storage::disk('local')->put('uploads/l_form_53/'.$chnagedname, $image);
		
		$imgnameData[] = $name; 				
		$imgData[] = $chnagedname;   
		
		$user = InvitationAttachment::where('invitation_id', $intid)->first(); 
        if( $user!=null){
			if($user->form53filename!=null){					
				$filenamenew=$imgData;
				//if($user->filename!=null){
						$filenamenew1=json_decode($user->form53filename);
						$cgefilename=array_merge($filenamenew, $filenamenew1);
						$cgefilename = array_values($cgefilename);
				// }else{
				// 	$cgefilename = $filenamenew;
				// }
				
				$user->form53filename=json_encode($cgefilename);
				
				$imgnamenew=$imgnameData;
				//if($user->imagename!=null){
				$imgnamenew1= json_decode($user->form53imagename);
				$cgeimagename= array_merge($imgnamenew, $imgnamenew1);
				 $cgeimagename = array_values($cgeimagename);
				// }else{
				// 	$cgeimagename = $imgnamenew;
				// }

				$user->form53imagename=json_encode($cgeimagename);
				$user->save();
									
				}
			else{
				$user->form53filename=json_encode($imgData);
				$user->form53imagename=json_encode($imgnameData);
				$user->save();
			}
		}			
		else{
			$data1 = new InvitationAttachment();
			$data1->invitation_id=$intid;
			$data1->form53filename=json_encode($imgData);
			$data1->form53imagename=json_encode($imgnameData);
			$data1->save();	
		}  
		return response()->json(['status'=>'1','message'=>'Files Uploaded Successfully']);  
}
	
	/*Upload ins form*/
	public function uploadinsform(Request $request)
    {
		$intid=$request->insformnewintid;
		$patientid=$request->insformpatientid;
		$invitation = user_invitations::find($intid);

		$filep = $request->file('file')->path();
		$image = base64_encode(file_get_contents($filep));
		$image = MyHelper::Encrypt($image,$invitation->enceypKey);
		$chnagedname= date('Y').'/'. time().$intid.'_'.Str::random(15).'.txt';

		$file = $request->file('file');
		$ext=$file->extension();
		$name = MyHelper::clearnName($file->getClientOriginalName(),$ext);
		
		Storage::disk('local')->put('uploads/l_questionair/'.$chnagedname, $image);							
		$imgnameData[] = $name; 				
		$imgData[] = $chnagedname; 

		$user = InvitationAttachment::where('invitation_id', $intid)->first(); 
		if( $user!=null){
			if($user->insformfilename!=null){					
				$filenamenew=$imgData;
				//if($user->filename!=null){
				$filenamenew1=json_decode($user->insformfilename);
				$cgefilename=array_merge($filenamenew, $filenamenew1);
				 $cgefilename = array_values($cgefilename);
				/*}else{
					$cgefilename = $filenamenew;
				}*/
				$user->insformfilename=json_encode($cgefilename);
				
				$imgnamenew=$imgnameData;
				//if($user->imagename!=null){
				$imgnamenew1=json_decode($user->insformimagename);
				$cgeimagename=array_merge($imgnamenew, $imgnamenew1);
				 $cgeimagename = array_values($cgeimagename);
				/*}else{
					$cgeimagename = $imgnamenew;
				}*/
				$user->insformimagename=json_encode($cgeimagename);
				$user->save();
					/*	if($user->id){
						$invitation = user_invitations::find($user->invitation_id);
						$emailCtrl = new EmailController();
						$emailCtrl->send_notification_email_to_doctor_after_lawyer_upload_documents($invitation);
						}		*/				
				}
			else{
				$user->insformfilename=json_encode($imgData);
				$user->insformimagename=json_encode($imgnameData);
				$user->save();
						/*if($user->id){
						$invitation = user_invitations::find($user->invitation_id);
						$emailCtrl = new EmailController();
						$emailCtrl->send_notification_email_to_doctor_after_lawyer_upload_documents($invitation);
					}*/
			}
		}			
		else{
				$data1 = new InvitationAttachment();
				$data1->invitation_id=$intid;
				$data1->insformfilename=json_encode($imgData);
				$data1->insformimagename=json_encode($imgnameData);
				$data1->save();	
		} 

		return response()->json(['status'=>'1','message'=>'Files Uploaded Successfully']);
  
 
    }	


	public function custominvoice(){
		/*$name="test";
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->WriteHTML("<h1>Hello world!</h1>$name");
		$mpdf->Output();*/
	}

	/*Resend Mail to patient*/
	public function resend_mail_to_patient(Request $request)
	{ 
			$invitation = user_invitations::find($request->invitation_id);
			$invitation->reject_status = 0;
			$invitation->save();
			
			$emailCtrl = new EmailController();
			$emailCtrl->send_invitation_confirmation_to_patient($invitation);     
		
			return ['data'=>$invitation,'status'=>'success','message'=>'Request sent','url'=>url('lawyer-dashboard')."/tracking"];	
	}

	public function encryptFile($source,$dest)	
	{
		$key = substr(sha1($this->medical_file_encrypt_key, true), 0, 16);
		$iv = openssl_random_pseudo_bytes(16);

		$error = false;
		if ($fpOut = fopen($dest, 'w')) {
			// Put the initialzation vector to the beginning of the file
			fwrite($fpOut, $iv);
			if ($fpIn = fopen($source, 'rb')) {
				while (!feof($fpIn)) {
					$plaintext = fread($fpIn, 16 * $this->medical_file_encyption_blocks);
					$ciphertext = openssl_encrypt($plaintext, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $iv);
					// Use the first 16 bytes of the ciphertext as the next initialization vector
					$iv = substr($ciphertext, 0, 16);
					fwrite($fpOut, $ciphertext);
				}
				fclose($fpIn);
			} else {
				$error = true;
			}
			fclose($fpOut);
		} else {
			$error = true;
		}

		return $error ? false : $dest;
	}


	public function decryptFile($source,$dest)
	{
		$key = substr(sha1($this->medical_file_encrypt_key, true), 0, 16);
		$error = false;
		if ($fpOut = fopen($dest, 'w')) {
			if ($fpIn = fopen($source, 'r')) {
				// Get the initialzation vector from the beginning of the file
				$iv = fread($fpIn, 16);
				while (!feof($fpIn)) {
					$ciphertext = fread($fpIn, 16 * ($this->medical_file_encyption_blocks + 1)); // we have to read one block more for decrypting than for encrypting
					$plaintext = openssl_decrypt($ciphertext, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $iv);
					// Use the first 16 bytes of the ciphertext as the next initialization vector
					$iv = substr($ciphertext, 0, 16);
					fwrite($fpOut, $plaintext);
				}
				fclose($fpIn);
			} else {
				$error = true;
			}
			fclose($fpOut);
		} else {
			$error = true;
		}

		return $error ? false : $dest;
	}


	public function uploadProfileImage(Request $request){
		$user = auth()->user();

		if($user->role_id == "4"){
			$user = User::find($request->docID);
		}

		$role_id = $user->role_id;
		$file = $request->file('picture');
		$imageName = time().$user->unique_id.'.png';

		if($role_id == 2){
			$subfolerder = 'lawyer';
		}
		elseif($role_id == 5){
			$subfolerder = 'patient';
		}
		else{
			$subfolerder = 'doctor';
		}

		//$subfolerder = ($role_id == 2) ? 'lawyer' : 'doctor';
		$path = "uploads/profilepics/".$subfolerder;
		if(!empty($user->profilepic) && file_exists($path."/".$user->profilepic)){
			unlink($path."/".$user->profilepic);
		}

		if(!file_exists($path)){
			mkdir($path);
		}

		$file->move($path,$imageName);
		$user->profilepic = $imageName;
		$user->save();

		return response()->json(['status'=>'1','rr'=>$role_id,'lnk'=>url($path.'/'.$imageName)]);
	}

	public function deleteProfileImage(){
		$user = auth()->user();
		$role_id = $user->role_id;

		if($role_id == 2){
			$subfolerder = 'lawyer';
		}
		elseif($role_id == 5){
			$subfolerder = 'patient';
		}
		else{
			$subfolerder = 'doctor';
		}

		//$subfolerder = ($role_id == 2) ? 'lawyer' : 'doctor';
		$path = "uploads/profilepics/".$subfolerder;
		if(!empty($user->profilepic) && file_exists($path."/".$user->profilepic)){
			unlink($path."/".$user->profilepic);
		}
		$user->profilepic = '';
		$user->save();

		return response()->json(['status'=>'1','msg'=>'Message deleted successfully']);
	}

}
