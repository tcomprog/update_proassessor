 
 @extends("includes.email_container")
 @section("content")

            <div>

                <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Hello {{$name}},</h3>

                    <p style="font-size: 14px;line-height: 17px;">
                      As per your request invitation date has been changed from: <b>{{$from}}</b>, to: <b>{{$to}}</b> by Admin and Admin has approved the invitation on behalf you.
                    </p>

                    <br/>

                    <br/>

                    <br/>

                    <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
                    <p style="font-size: 14px;line-height: 17px;">ProAssessors Team.</p> 

                    </div>  

               </div>

            </div>

  @endsection