@extends('layouts.admin_app')

<style>
    .cnt{
        box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
    }

    body {
  margin: 0;
  font-family: 'Ubuntu', sans-serif;
}
section {
  margin: 0 auto; 
  max-width: 660px;
  padding: 0 20px;
}

.ribbon {
  width: 48%;
  min-height: 50px;
  position: relative;
  float: left;
  margin-bottom: 20px;
  background: url(https://html5book.ru/wp-content/uploads/2015/10/snow-road.jpg);
  background-size: cover;
  text-transform: uppercase;
  color: white;
}
.ribbon:nth-child(even) {
  margin-right: 4%;
}
@media (max-width: 500px) {
  .ribbon {
    width: 100%;
  }
  .ribbon:nth-child(even) {
    margin-right: 0%;
  }
}
.ribbon1 {
  position: absolute;
  top: -6.1px;
  right: 10px;
}
.ribbon3 {
  min-width: 250px;
  height: 50px;
  line-height: 50px;
  padding-left: 15px;
  position: absolute;
  left: -8px;
  top: 20px;
  background: #526082;
}
.ribbon3:before, .ribbon3:after {
  content: "";
  position: absolute;
}
.ribbon3:before {
  height: 0;
  width: 0;
  top: -8.5px;
  left: 0.1px;
  border-bottom: 9px solid grey;
  border-left: 9px solid transparent;
}
.ribbon3:after {
  height: 0;
  width: 0;
  right: -14.5px;
  border-top: 25px solid transparent;
  border-bottom: 25px solid transparent;
  border-left: 15px solid #526082;
}

p{
  color: #000 !important;
}

</style>

@php
$options = [
    "1" => "Option 1",
    "2" => "Option 2",
    "3" => "Option 3",
    "4" => "Other"
];
$casetype = [
    "1" => "MVA",
    "2" => " WSIB",
    "3" => "Slip and fall",
];
@endphp

@section('content')

<div class="container-fluid bg-white rounded cnt pb-4 ml-2">

   <div class="row pt-3">
       <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Date</p></div>
       <div class="col-md-10 col-sm-8"><p class="mb-0">{{date('d-m-Y',strtotime($reject_request->created_at))}}</p></div>
   </div>
   <div class="row">
       <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Option</p></div>
       <div class="col-md-10 col-sm-8"><p class="mb-0">{{$options[$reject_request->option]}}</p></div>
   </div>
   <div class="row pb-3">
       <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Description</p></div>
       <div class="col-md-10 col-sm-8"><p class="mb-0">{{$reject_request->desc}}</p></div>
   </div>

  <div id="s_msg" class="alert alert-success" style="display: none" role="alert">
    A simple success alert—check it out!
  </div>
  <div id="r_msg" class="alert alert-danger" style="display: none" role="alert">
    A simple danger alert—check it out!
  </div>

   @if ($reject_request->status == 0)
   <div id="datechangeSection" class="row my-3">
     <div class="col">
       <div class="form-group">
         <label for="exampleInputEmail1" class="font-weight-bold">Change date</label>
         <input type="text" autocomplete="off" class="form-control col-md-4 col-sm-12" id="datepicker">
       </div>
       <div class="mt-2">
           <button id="changeDateRequestBtn" data-id="{{$reject_request->id}}" class="btn btn-outline-success">Update date</button>
           <img id="cng_loader" style="display: none" width="35" class="ml-3" src="{{asset('img/loading.gif')}}" alt="">
       </div>
     </div>
   </div>
   @endif



    <div class="row">
        <div class="ribbon">
            <span class="ribbon3">Invitation Details</span>
        </div>

        <div class="container-fluid my-3">

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Appointment type</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{ucfirst($invitation->appointment_type)}}</p></div>
          </div>
          
          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Date</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{date("d-m-Y",strtotime($invitation->invitation_date))}}  | {{$invitation->timeslot}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Due date</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{date("d-m-Y",strtotime($invitation->duedate))}}</p></div>
          </div>
       
        </div>

    </div>


    <div class="row">
        <div class="ribbon">
            <span class="ribbon3">Patient Details</span>
        </div>

      <div class="container-fluid my-3">
          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Name</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$patient->name}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Phone no</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{MyHelper::Decrypt($patient->phonenumber)}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Email</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{MyHelper::Decrypt($patient->email)}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Gender</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{MyHelper::Decrypt($patient->gender)}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">File number</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{MyHelper::Decrypt($patient->filenumber)}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Case type</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$casetype[$patient->casetype]}}</p></div>
          </div>
      </div>

    </div>


    <div class="row">
        <div class="ribbon">
            <span class="ribbon3">Lawyer Details</span>
        </div>

        <div class="container-fluid my-3">

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Name</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$lawyer->name}}</p></div>
          </div>
          
          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Email</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$lawyer->email}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Phone no</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$lawyer->phone}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Address</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$lawyer->business_address}}</p></div>
          </div>
        </div>

    </div>

    <div class="row">
        <div class="ribbon">
            <span class="ribbon3">Doctor Details</span>
        </div>

        <div class="container-fluid my-3">

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Name</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$doctor->name}}</p></div>
          </div>
          
          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Email</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$doctor->email}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Phone no</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$doctor->phone}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Address</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$doctor->business_address}}</p></div>
          </div>
        </div>
        
    </div>


@endsection

