<?php 
   $home = request()->is('/') ? 'style="border-bottom: 4px solid #515C84;"' : "";
   $about = request()->is('about') ? 'style="border-bottom: 4px solid #515C84;"' : "";
   $notification = request()->is('notification') ? 'style="border-bottom: 4px solid #515C84;"' : "";

   $is_homepage = false;
  if(request()->is('/')){
    $is_homepage = true;
  }

?>

@php
   $clinicSubscriptionStatus = Helper::checkClinicSubscription();
@endphp

{{-- Web header --}}
<header id="nheadeContainer" class="container-fluid" >
    <div class="container-fluid d-flex contactContainer">
        <p class="inforp" data-type="0" data-value="{{$pagedetails->phone}}">
            <span class="material-icons">call</span>
            {{$pagedetails->phone}}
        </p>
        <p class="dashhold"></p>
        <p class="inforp"  data-type="1" data-value="{{$pagedetails->site_email}}">
            <span class="material-icons">mail</span>
            {{$pagedetails->site_email}}
        </p>
        <p class="dashhold"></p>
        <p class="inforp" data-type="2" data-value="{{$pagedetails->address}}">
            <span class="material-icons">location_on</span>
            {{$pagedetails->address}}
        </p>
    </div>

    {{-- ************** Navigation container **************** --}}
    <div class="nav-container d-flex">
        <div class="col-4 first">
           <img class="topiconlogo" src="{{asset('img/bottom_logo.png')}}" alt="">
        </div>
        <div class="col-8  d-flex justify-content-end align-items-center second">
            <div class="flexsideMenue">
            <a href="{{route('index')}}"><span {!!$home!!}>HOME</span></a>
            <a href="{{route('about')}}"><span {!!$about!!}>ABOUT</span></a>
            <a href="{{$is_homepage? '#contact': url('/#contact')}}"><span>CONTACT</span></a>

   @if (Auth::check())
    {{-- ------------------------------------------ --}}
    @php
       $totalNotifiaction = Helper::notification_messages(Auth::id())['total'];
       $no_list = Helper::getnotificationList(Auth::id());
    @endphp

    <div>
    @if (count($no_list) > 0)
    <div class="nav-item" {{!!$notification!!}}>
    <div id="not_icons" class="dropdown show align-self-center">
      <a class="nav-link" href="#" id="bell">
        <img class="bell_icon" src="{{ url('n_img/bell.png') }}">
        <label class="badge badge-warning notification_badge changebadgeNotification" style="color:white !important; padding:2px 5px !important; font-size:11px; width:auto; height:auto">{{ $totalNotifiaction > 0 ? $totalNotifiaction : '' }}</label>
      </a>
    </div>
    </div>
    @endif

    @if($clinicSubscriptionStatus == 1)
     <div class="nav-item">
        <div class="notifications" id="box" style="display: none">
            <div class="notifications_header">
              <h2 style="margin-bottom:0px;border:none;color:#323B62;">Notifications 
               @if($totalNotifiaction > 0)
               - <span>{{$totalNotifiaction}}</span>
               @endif 
              </h2>
              <span id="closenotification" class="material-icons" style="color: #323B62; font-size:24px;">close</span>
            </div>
           
            <div style="    max-height: 330px;overflow-y: auto;">
            @foreach ($no_list as $row)
              @php
              if($row['status'] == '0'){
                $styles = 'style="background: #F7FCE8;"';
              }
              else{
              $styles = "";
              }
          @endphp
              <a href="#" style="text-decoration: none;">
                <div id="notification_row_{{$row['id']}}" data-link="{{url($row->url)}}" data-id="{{$row["id"]}}" data-status="{{$row['status']}}" class="notifications-item" {!! $styles !!}>
                    <div class="text">
                        <p style="margin-bottom: 0px;color:black">{{$row->description}}</p>
                        <p style="margin-bottom: 0px;
                        padding-top: 4px;
                        text-align: right;
                        font-weight: bold;
                        font-size: 10px;
                        color: #323B62;">{{date('d-m-Y H:i',strtotime($row->created_at))}}</p>
                    </div>
                </div>
              </a>
            @endforeach
          
          </div>

            <div style="text-align: center;padding: 6px;">
              <a href="{{route("notification")}}" style="font-weight: bold;text-decoration: none;">View all</a>
            </div>

        </div>
    </div>
    @endif
    </div>
     {{-- ------------------------------------------ --}}
     @endif

     @if (Auth::check())
     {{-- ************** Drop Down ******************** --}}
     <div class="dropdown" style="
     display: flex;
     align-items: center;
     justify-content: center;">
 
        <a class="btn btn-secondary dropdown-toggle px-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           @php
               $user1 = Helper::User(); 
           @endphp

           @if ($user1->profilepic !=null)
                @if($user1->role_id == 2)
                     <img class="headerProfImag" style="width:30px; height:30px; border-color:white;" src="{{ url('uploads/profilepics/lawyer/'.$user1->profilepic) }}"> 
                @elseif ($user1->role_id == 1)
                      <img class="headerProfImag" style="width:30px; height:30px; border-color:white;" src="{{ url('uploads/profilepics/doctor/'.$user1->profilepic) }}"> 
                @elseif ($user1->role_id == 4)
                    <img class="headerProfImag" style="width:30px; height:30px; border-color:white;" src="{{ url('uploads/profilepics/clinic/'.$user1->profilepic) }}"> 
                @elseif ($user1->role_id == 5)
                    <img class="headerProfImag" style="width:30px; height:30px; border-color:white;" src="{{ url('uploads/profilepics/patient/'.$user1->profilepic) }}"> 
                @endif

            @else
                 <img class="headerProfImag" src="{{ url('img/user.jpg') }}">
           @endif

           @if (Auth::user()->role_id == 1)
           <span class="text-light">{{{ ucfirst(Auth::user()->name)}}} 
              <img src="{{ asset('n_img/doctor.png') }}" alt="Doctor's Dashboard" class="dashboard_icon_dark">
            </span>

            @elseif (Auth::user()->role_id == 2)
                <span class="text-light">{{{ ucfirst(Auth::user()->name)}}} 
                  <img src="{{ asset('n_img/lawyer.png') }}" alt="Layers's Dashboard" class="dashboard_icon_dark">
                </span>

            @elseif (Auth::user()->role_id == 5)
                <span class="text-light">{{{ ucfirst(Auth::user()->name)}}} 
                   <img src="{{ asset('n_img/patient.png') }}" alt="Patient Dashboard" class="dashboard_icon_dark">
                </span>

            @elseif (Auth::user()->role_id == 4)
              <span class="text-light">{{{ ucfirst(Auth::user()->name)}}} 
                <img src="{{ asset('n_img/clinic.png') }}" alt="Clinic Dashboard" class="dashboard_icon_dark">
              </span>
            @endif
              
           </a>

           <div class="dropdown-menu bg-light" aria-labelledby="dropdownMenuLink">

            {{-- Doctor Mene --}}
            @if(Auth::user()->role_id == 1)
                <a class="dropdown-item linkcolors"  href="{{ url('doctor-dashboard') }}">Dashboard</a>
                <a class="dropdown-item linkcolors"  href="{{ url('doctor-profile') }}">Profile</a>
                <a class="dropdown-item linkcolors"  href="{{ route('privateInfor') }}">Private information</a>
            @endif
  
            @if (Auth::user()->role_id == 1 && Auth::user()->doctor->instruction_form==null)
               <a id='updatelnk' class="dropdown-item linkcolors opensss" onclick="openUploadQuestion(0)" data-toggle="modal" data-target="#upload_questionnair"  href="#">Upload Questionnaire</a>
            @elseif (Auth::user()->role_id == 1 && Auth::user()->doctor->instruction_form != null)
               <a id='updatelnk' class="dropdown-item linkcolors opensss" onclick="openUploadQuestion(1)"  data-toggle="modal" data-target="#upload_questionnair" href="#">Upload Questionnaire</a>
            @endif
  
           {{-- Lawyer Menu --}}
           @if (Auth::user()->role_id == 2)
              <a class="dropdown-item linkcolors"  href="{{ url('lawyer-dashboard/my-patients') }}">Dashboard</a>
              <a class="dropdown-item fdfdf linkcolors"  data-toggle="modal" data-target="#lawyerchangePassword">Change Password</a>
              <a class="dropdown-item linkcolors"  href="{{ url('lawyer-profile') }}">Profile</a>
           @endif


           {{-- Patient Menu --}}
           @if (Auth::user()->role_id == 5)
              <a class="dropdown-item linkcolors"  href="{{ url('patient-dashboard') }}">Dashboard</a>
              <a class="dropdown-item fdfdf linkcolors"  data-toggle="modal" data-target="#lawyerchangePassword">Change Password</a>
              <a class="dropdown-item linkcolors"  href="{{ url('patient-profile') }}">Profile</a>
           @endif
  
            {{-- Clinic Menu --}}
            @if (Auth::user()->role_id == 4)
              @if($clinicSubscriptionStatus == 0)
                <a class="dropdown-item linkcolors" href="{{url('/subscription')}}">Activate Package</a>
              @endif

              <a class="dropdown-item linkcolors" href="{{url('/clinic-dashboard')}}">Dashboard</a>
              <a class="dropdown-item linkcolors" href="{{url('/addClinicDoctor')}}">New Doctor</a>
              <a class="dropdown-item linkcolors" href="{{url('/showClinicDoctorList')}}">Doctors List</a>
              @if($clinicSubscriptionStatus == 1)
              <a class="dropdown-item fdfdf linkcolors"  data-toggle="modal" data-target="#lawyerchangePassword">Change Password</a>
              @endif
              <a class="dropdown-item linkcolors" href="{{url('clinic-profile')}}">Profile</a>

              @if($clinicSubscriptionStatus == 1)
                <a class="dropdown-item linkcolors" href="{{url('/clinic_subscription')}}">Subscription</a>
              @endif

            @endif
  
           <a class="dropdown-item linkcolors" href="{{ url('logout') }}">Logout</a>	
  
         </div>
    </div>
     {{-- ************** Drop Down ******************** --}}
     @endif
           
            @if (!Auth::check())
            <div class="btn-group" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-secondary" onClick="openTesting()" style="padding-right:15px; padding-left:25px;">SIGNUP</button>
                <div style="background:white;display: flex; align-items: center;">
                  <div style="width: 2.5px; background:#20215E;height: 18px;"></div>
                </div>
                <button type="button" class="btn btn-secondary openLoginPopup" style="padding-right:25px; padding-left:15px;" >LOGIN</button>
            </div>
            @endif         

           </div>
            <div data-status="open" class="openmnuebtn cls-open-mobile-menu">
                <span class="material-icons" style="color:white; font-size:35px">menu</span>
            </div>

        </div>
    </div>

</header>




{{-- Mobile menue --}}

<div id='fullScreenSideMenu'></div>

<div id="sideMenuContainer" class="sideMenuContainer">
      
      <div class="d-flex flex-column" style="height:85vh">
      <div class="headerContainer d-flex justify-content-between align-items-center">
        <img width="150px" src="{{asset('img/bottom_logo.png')}}" alt="">
        <span status="close" class="material-icons text-white openmnuebtn" style="font-size:27px">close</span>
      </div> 
      
      <div class="mainboxContainer pt-3 d-flex flex-column justify-content-center" style="flex:1">
        <a href="{{route('index')}}" class="option selected">HOME</a>
        <a href="{{route('about')}}" class="option">ABOUT</a>
        <a href="{{$is_homepage? '#contact': url('/#contact')}}" class="option menumobclick">CONTACT</a>

        @if (!Auth::check())
        <a class="option openLoginPopup">LOGIN</a>
        <a class="option" onClick="openTesting()">SIGNUP</a>
        @endif

        @if (Auth::check())
       {{-- ************** Drop Down ******************** --}}
       <div class="dropdown show" style="
       display: flex;
       align-items: center;
      ">
   
          <a class="btn btn-secondary dropdown-toggle px-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             @php
                 $user1 = Helper::User(); 
             @endphp
  
             @if ($user1->profilepic !=null)
                  @if($user1->role_id == 2)
                       <img class="headerProfImag" style="width:30px; height:30px; border-color:white;" src="{{ url('uploads/profilepics/lawyer/'.$user1->profilepic) }}"> 
                  @elseif ($user1->role_id == 1)
                        <img class="headerProfImag" style="width:30px; height:30px; border-color:white;" src="{{ url('uploads/profilepics/doctor/'.$user1->profilepic) }}"> 
                  @elseif ($user1->role_id == 4)
                      <img class="headerProfImag" style="width:30px; height:30px; border-color:white;" src="{{ url('uploads/profilepics/clinic/'.$user1->profilepic) }}"> 
                  @elseif ($user1->role_id == 5)
                      <img class="headerProfImag" style="width:30px; height:30px; border-color:white;" src="{{ url('uploads/profilepics/patient/'.$user1->profilepic) }}"> 
                  @endif
  
              @else
                   <img class="headerProfImag" src="{{ url('img/user.jpg') }}">
             @endif
  
             @if (Auth::user()->role_id == 1)
             <span class="text-light">{{{ ucfirst(Auth::user()->name)}}} 
                <img src="{{ asset('n_img/doctor.png') }}" alt="Doctor's Dashboard" class="dashboard_icon_dark">
              </span>
  
              @elseif (Auth::user()->role_id == 2)
                  <span class="text-light">{{{ ucfirst(Auth::user()->name)}}} 
                    <img src="{{ asset('n_img/lawyer.png') }}" alt="Layers's Dashboard" class="dashboard_icon_dark">
                  </span>
  
              @elseif (Auth::user()->role_id == 5)
                  <span class="text-light">{{{ ucfirst(Auth::user()->name)}}} 
                    <img src="{{ asset('n_img/patient.png') }}" alt="Patient Dashboard" class="dashboard_icon_dark">
                  </span>
  
              @elseif (Auth::user()->role_id == 4)
                <span class="text-light">{{{ ucfirst(Auth::user()->name)}}} 
                  <img src="{{ asset('n_img/clinic.png') }}" alt="Clinic Dashboard" class="dashboard_icon_dark">
                </span>
              @endif
                
             </a>
  
             <div class="dropdown-menu bg-light" aria-labelledby="dropdownMenuLink">
  
              {{-- Doctor Mene --}}
              @if(Auth::user()->role_id == 1)
                  <a class="dropdown-item linkcolors"  href="{{ url('doctor-dashboard') }}">Dashboard</a>
                  <a class="dropdown-item linkcolors"  href="{{ url('doctor-profile') }}">Profile</a>
                  <a class="dropdown-item linkcolors"  href="{{ route('privateInfor') }}">Private information</a>
              @endif
    
              @if (Auth::user()->role_id == 1 && Auth::user()->doctor->instruction_form==null)
                 <a id='updatelnk' class="dropdown-item linkcolors opensss" onclick="openUploadQuestion(0)" data-toggle="modal" data-target="#upload_questionnair"  href="#">Upload Questionnaire</a>
              @elseif (Auth::user()->role_id == 1 && Auth::user()->doctor->instruction_form != null)
                 <a id='updatelnk' class="dropdown-item linkcolors opensss" onclick="openUploadQuestion(1)"  data-toggle="modal" data-target="#upload_questionnair" href="#">Upload Questionnaire</a>
              @endif
    
             {{-- Lawyer Menu --}}
             @if (Auth::user()->role_id == 2)
                <a class="dropdown-item linkcolors"  href="{{ url('lawyer-dashboard/my-patients') }}">Dashboard</a>
                <a class="dropdown-item fdfdf linkcolors"  data-toggle="modal" data-target="#lawyerchangePassword">Change Password</a>
                <a class="dropdown-item linkcolors"  href="{{ url('lawyer-profile') }}">Profile</a>
             @endif

             {{-- Patient Menu --}}
             @if (Auth::user()->role_id == 5)
                <a class="dropdown-item linkcolors"  href="{{ url('patient-dashboard') }}">Dashboard</a>
                <a class="dropdown-item fdfdf linkcolors"  data-toggle="modal" data-target="#lawyerchangePassword">Change Password</a>
                <a class="dropdown-item linkcolors"  href="{{ url('patient-profile') }}">Profile</a>
             @endif
    
              {{-- Clinic Menu --}}
              @if (Auth::user()->role_id == 4)
              <a class="dropdown-item linkcolors" href="{{url('/clinic-dashboard')}}">Dashboard</a>
              <a class="dropdown-item linkcolors" href="{{url('/addClinicDoctor')}}">New Doctor</a>
              <a class="dropdown-item linkcolors" href="{{url('/showClinicDoctorList')}}">Doctors List</a>
              <a class="dropdown-item linkcolors" href="{{url('clinic-profile')}}">Profile</a>
              @if($clinicSubscriptionStatus == 1)
                <a class="dropdown-item linkcolors" href="{{url('/clinic_subscription')}}">Subscription</a>
              @endif
              @endif
    
             <a class="dropdown-item linkcolors" href="{{ url('logout') }}">Logout</a>	
    
           </div>
      </div>
       {{-- ************** Drop Down ******************** --}}
     @endif

      </div>
      
      <div class="bottomContactContainer py-3">
        <p class="inforp mb-2 text-white" style="font-size: 13px" data-type="0" data-value="{{$pagedetails->phone}}">
          <span class="material-icons align-bottom" style="font-size: 20px;padding-right: 8px;">call</span>
          {{$pagedetails->phone}}
        </p>
        <p class="inforp mb-2 text-white" style="font-size: 13px"  data-type="1" data-value="{{$pagedetails->site_email}}">
            <span class="material-icons align-bottom" style="font-size: 20px;padding-right: 8px;">mail</span>
            {{$pagedetails->site_email}}
        </p>
        <p class="inforp mb-2 text-white" style="font-size: 13px" data-type="2" data-value="{{$pagedetails->address}}">
            <span class="material-icons align-bottom" style="font-size: 20px;padding-right: 8px;">location_on</span>
            {{$pagedetails->address}}
        </p>
      </div>

    </div>

</div>


