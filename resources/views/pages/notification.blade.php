@if (Auth::user()->role_id == '1')
   @php $path = 'layouts.doctor-profile-custom'; @endphp
@elseif (Auth::user()->role_id == '2' || Auth::user()->role_id == '5') 
   @php $path = 'layouts.lawyer-das-custom'; @endphp
@elseif (Auth::user()->role_id == '4') 
    @php $path = 'layouts.clinic-layout'; @endphp
@endif

@extends($path) 

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.css">
<style>
    td {
        padding-top: 5px !important;
        padding-bottom: 5px !important;
        vertical-align: middle !important;
    }
</style>

<section class="intro_banner_sec inner_banner_sec" style="padding:30px 0px;">
    <div class="container">
        <div class="row align-items-center"> 
            <div class="col-md-6">
                <div class="welcome_text">
                    <h1>Notifications</h1>
                </div>
            </div>
          
        </div>
    </div>
</section>


<section class="pb-0 pt-0">

<div class="container">
    <div class="row mt-3">
        <div class="col-12">
        <h2 style="display: none" id="nothing_to_show" style="align-text:center">Nothing to show</h2>
        <div class="col d-flex" style="justify-content: end; padding:0px">
            <img id="loader_ere" style="display: none" width="25" class="mr-3" src="{{asset('img/loading.gif')}}" />
            <button id="clear_all_notification" class="btn btn-outline-danger btn-sm ">Clear all</button>
        </div>
    </div>
    </div>
    <div class="row">
        <div class="col-12 mt-3 mb-5">
        <table class="table mt-3 display" id="table_id">
            <thead>
              <tr>
                <th width="25%" scope="col">Date</th>
                <th width="60%" scope="col">Detail</th>
                <th width="15%" scope="col">Action</th>
              </tr>
            </thead>
            <tbody>

            @foreach ($notification as $row)
                    <tr>
                        <td scope="row">{{date('d-m-Y H:i',strtotime($row->created_at))}}</td>
                        <td>{{$row->description}}</td>
                        <td>
                            <a href="{{$row->url}}">
                                <span class="material-icons" style="color: #515C84; font-size:28px;">link</span>
                            </a>
                        </td>
                    </tr>
            @endforeach
             
            </tbody>
          </table>
        </div>

    </div>
</div>   
    

</section>

<!-- Process Section -->

@endsection




