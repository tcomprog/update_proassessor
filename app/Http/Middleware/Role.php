<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class Role
{
    public function handle($request, Closure $next, ... $roles)
    {
        if (!Auth::check()) // I included this check because you have it, but it really should be part of your 'auth' middleware, most likely added as part of a route group.
        return redirect()->route('login');
        
        $user = Auth::user();
        
        foreach($roles as $role) {
            // Check if user has the role This check will depend on how your roles are set up
            if($user->hasRole($role))
                return $next($request);
        }
        if($user->role->role == 'lawyer'){
            redirect('lawyer-dashboard');
        }else if($user->role->role == 'doctor'){
            redirect('doctor-dashboard');
        }
        return redirect('/');
    }
}
