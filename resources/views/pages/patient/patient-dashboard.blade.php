@extends('layouts.lawyer-das-custom') 
@section('content')	
<link href="{{ asset('css/lawyer_profile.css') }}" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="{{ asset('css/doctor_dashboard.css') }}" rel="stylesheet">

<style>
    .lawyer_p{
        color: #3A456A;
        margin-bottom:0px;
        line-height: 18px;
        font-weight: bold !important;
    }

    .ui-datepicker-trigger{
		background: transparent !important;
	}
	.n_links{
		font-weight: bold;
		color:#00396b;
	}
	.n_links:hover{
		text-decoration: none;
	}
    .tag-design{
        margin-bottom: 0px;
    background: #e8f4da;
    padding: 4px 10px;
    border-radius: 5px;
    color: #2A325A;
    font-weight: bold;
    font-size: 14px;
    }

    .showBtn{
        display: flex !important;
    }
    .hideBtn{
        display: none !important;
    }

</style>

	 <!-- Banner Section -->
    <section class="intro_banner_sec inner_banner_sec" style="padding-top: 24px;padding-bottom: 24px;">
        <div class="container">
            <div class="row align-items-center"> 
                <div class="col-md-6">
                    <div class="welcome_text">
                        <h1>Patient's Dashboard <img src="{{ asset('n_img/patient.png') }}" height="35" alt="Lawyer's Dashboard" class="dashboard_icon"></h1>
                    </div>
                </div>
                <div class="col-md-6 text-right">
                    <!-- <a href="<?= url('search-doctor'); ?>" class="search_btn">Search Doctors</a> -->
                </div>
            </div>
        </div>
    </section>

	
	<section class="dashboard_sec" style="padding-top: 20px;">
        <div class="container" style="min-height: 400px;">

            <div class="jumbotron jumbotron-fluid py-2 rounded-sm" style="border-left:#3A456A 5px solid;">
                <div class="container">
                  <h4 style="color:#3A456A">Your Lawyer</h4>
                  <div class="row">
                    <div class="col-2">
                         <img style="width:90px; height:90px;" class="img-thumbnail" src="@if(!empty($lawyer->profilepic)) {{asset('uploads/profilepics/lawyer/'.$lawyer->profilepic)}} @else {{asset('images/man.png')}} @endif" />
                    </div>
                    <div class="col-8">
                        <p class="lead font-weight-bold" style="color: #3A456A;margin-bottom:0px">{{$lawyer->name}}</p>
                        <p class="lead lawyer_p"><small>{{$lawyer->email}}</small></p>
                        <p class="lead lawyer_p"><small>{{$lawyer->phone}}</small></p>
                        <p class="lead lawyer_p"><small><b>Address: </b>{{$lawyer->business_address}}</small></p>
                    </div>
                  </div>
                </div>
              </div>
           
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">

                <a class="nav-item nav-link active" id="search-doctors" data-toggle="tab" href="#pendingInvitation" role="tab" aria-selected="false" style="font-weight: 600; font-size: 21px;">Pending</a>
                <a class="nav-item nav-link" id="search-clinic" data-toggle="tab" href="#activeInvitation" role="tab"  aria-selected="false" style="font-weight: 600; font-size: 21px;">Active</a>
                <a class="nav-item nav-link"  id="my-patients" data-toggle="tab" href="#completedInvitation" role="tab" aria-controls="my-patients"    aria-selected="false" style="font-weight: 600;font-size: 21px;">Completed</a>
                <a class="nav-item nav-link"  id="my-patients" data-toggle="tab" href="#cancelledInvitation" role="tab" aria-controls="my-patients"    aria-selected="false" style="font-weight: 600;font-size: 21px;">Cancalled</a>

                </div>
            </nav>


            <div class="tab-content" id="nav-tabContent">

                {{-- ********************* Pending Invitation Start ******************* --}}
				<section class="tab-pane fade show active" id="pendingInvitation" role="tabpanel" aria-labelledby="nav-patients-tab">
                      <div class="container">
                        <div class="row">
                        @if(count($pending) > 0)
                            @foreach ($pending as $invitation)
                              <div class="col-md-12" style="padding: 0px; padding-left: 10px;" id="invitation_{{$invitation->id}}">
                                 <div class="design_card_new" style="background: white !important;"> 
                                  
                                    <div class="d-flex flex-row justify-content-between">
                                    <div class="d-flex">
                                        @if (($invitation->user1->profilepic) !=null)
                                             <img class="n_images" src="{{ url('uploads/profilepics/doctor/'.$invitation->user1->profilepic) }}" />
                                         @else
                                            <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
                                        @endif
                                        <div class='n_c_container'>
                                            <h5 class="d_name_text" style="20px;">{{ucfirst($invitation->user1->name)}}</h5>
                                            <p class="firm_name font-weight-bold">{{ucfirst(@$desgination[$invitation->user1->desgination])}}</p>
                                        </div>
                                   </div>

                                   @if($invitation->status == 'pending')
                                    <div>
                                        <p class="tag-design">Waiting for doctor approval</p>
                                    </div>
                                   @else
                                    <div>
                                       <p class="tag-design" style="background:#b3db86">Approved by doctor</p>
                                    </div>
                                   @endif

                                </div>

                                   <table id="n_desing_tbl" class="mt-3">
                                      <tr>
                                        <th>Email:</th>
                                        <td>{{ $invitation->user1->email }}</td>
                                       </tr>

                                       <tr>
                                        <th>Date/Time:</th>
                                        <td><?php echo date('d F Y', strtotime($invitation->invitation_date)); ?> |  {{ $invitation->timeslot }}</td>
                                       </tr>

                                       <tr>
                                        <th>Type:</th>
                                        <td>{{ucfirst($invitation->appointment_type)}}</td>
                                       </tr>

                                      <tr>
                                        <th>Category:</th>
                                        <td>{{ $invitation->category->category_name }}</td>
                                       </tr>

                                       <tr>
                                        <th>Doctor Address:</th>
                                        <td>{{ucfirst($invitation->user1->business_address)}}</td>
                                       </tr>

                                       <tr>
                                        <th>License Number:</th>
                                        <td>{{ $invitation->user1->doctor->licence_number }}</td>
                                       </tr>
                                   
                                   </table>

                                   @if($invitation->status == 'active' && $invitation->is_patient_accepted == 'no')
                                   <div class="button_containter">

                                        {{-- <a class="rountedBtn " style="background: #323B62 !important;"  href="{{url('invitation_details_view/'.$invitation->id)}}"  title="View Details">
                                            <span class="material-icons" style="color: white; font-size:27px;">visibility</span>
                                        </a> --}}

                                       <img id="loader_{{ $invitation->id }}" src="{{asset('img/loading.gif')}}" style="height: 45px; height:45px;display:none"/> 

                                       <div class="rountedBtn changedStatus" id="approveBtn_{{ $invitation->id }}" data-status="approve" data-id="{{ $invitation->id }}"  title="Accept">
                                           <span class="material-icons" style="color: white; font-size:30px;">check</span>
                                       </div>

                                       <a class="rountedBtn" id="rejectBtn_{{ $invitation->id }}" href="{{ url('/patient/reject-invitation/'.$invitation->unique_id) }}" data-status="reject" style="background:#FA1D2F !important" title="Reject">
                                           <span class="material-icons" style="color: white; font-size:30px;">close</span>
                                       </a>

                                   </div>
                                   @endif

                                 </div>
                              </div>
                            @endforeach
                            @else
                              <h4 class="pt-3" style="color: #3A456A;">No appointment found!</h4>
                            @endif
                        </div>
                      </div>
                </section>
                {{-- ********************* Pending Invitation End  ******************* --}}

                
                {{-- ********************* Active Invitation Start ******************* --}}
				<section class="tab-pane fade" id="activeInvitation" role="tabpanel" aria-labelledby="nav-patients-tab">
                    <div class="container">
                        <div class="row">
                        @if(count($active) > 0)
                            @foreach ($active as $invitation)
                              <div class="col-md-12" style="padding: 0px; padding-left: 10px;" id="invitation_{{$invitation->id}}">
                                 <div class="design_card_new" style="background: white !important;"> 
                                    
                                    <div class="d-flex">
                                        @if (($invitation->user1->profilepic) !=null)
                                             <img class="n_images" src="{{ url('uploads/profilepics/doctor/'.$invitation->user1->profilepic) }}" />
                                         @else
                                            <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
                                        @endif
                                        <div class='n_c_container'>
                                            <h5 class="d_name_text" style="20px;">{{ucfirst($invitation->user1->name)}}</h5>
                                            <p class="firm_name font-weight-bold">{{ucfirst(@$desgination[$invitation->user1->desgination])}}</p>
                                        </div>
                                   </div>

                                   <table id="n_desing_tbl" class="mt-3">
                                      <tr>
                                        <th>Email:</th>
                                        <td>{{ $invitation->user1->email }}</td>
                                       </tr>

                                       <tr>
                                        <th>Date/Time:</th>
                                        <td><?php echo date('d F Y', strtotime($invitation->invitation_date)); ?> |  {{ $invitation->timeslot }}</td>
                                       </tr>

                                       <tr>
                                        <th>Type:</th>
                                        <td>{{ucfirst($invitation->appointment_type)}}</td>
                                       </tr>

                                      <tr>
                                        <th>Category:</th>
                                        <td>{{ $invitation->category->category_name }}</td>
                                       </tr>

                                       <tr>
                                        <th>Doctor Address:</th>
                                        <td>{{ucfirst($invitation->user1->business_address)}}</td>
                                       </tr>

                                       <tr>
                                        <th>License Number:</th>
                                        <td>{{ $invitation->user1->doctor->licence_number }}</td>
                                       </tr>
                                   
                                   </table>

                                   <div class="button_containter">

                                        <a class="rountedBtn " style="background: #323B62 !important;"  href="{{url('invitation_details_view/'.$invitation->id)}}"  title="View Details">
                                            <span class="material-icons" style="color: white; font-size:27px;">visibility</span>
                                        </a>

                                   </div>

                                 </div>
                              </div>
                            @endforeach
                            @else
                              <h4 class="pt-3" style="color: #3A456A;">No appointment found!</h4>
                            @endif
                        </div>
                      </div>
                </section>
               {{-- ********************* Active Invitation End   ******************* --}}


                {{-- ********************* Completed Invitation Start ******************* --}}
				<section class="tab-pane fade" id="completedInvitation" role="tabpanel" aria-labelledby="nav-patients-tab">
                    <div class="container">
                        <div class="row">
                        @if(count($completed) > 0)
                            @foreach ($completed as $invitation)
                              <div class="col-md-12" style="padding: 0px; padding-left: 10px;" id="invitation_{{$invitation->id}}">
                                 <div class="design_card_new" style="background: white !important;"> 
                                    
                                    <div class="d-flex">
                                        @if (($invitation->user1->profilepic) !=null)
                                             <img class="n_images" src="{{ url('uploads/profilepics/doctor/'.$invitation->user1->profilepic) }}" />
                                         @else
                                            <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
                                        @endif
                                        <div class='n_c_container'>
                                            <h5 class="d_name_text" style="20px;">{{ucfirst($invitation->user1->name)}}</h5>
                                            <p class="firm_name font-weight-bold">{{ucfirst(@$desgination[$invitation->user1->desgination])}}</p>
                                        </div>
                                   </div>

                                   <table id="n_desing_tbl" class="mt-3">
                                      <tr>
                                        <th>Email:</th>
                                        <td>{{ $invitation->user1->email }}</td>
                                       </tr>

                                       <tr>
                                        <th>Date/Time:</th>
                                        <td><?php echo date('d F Y', strtotime($invitation->invitation_date)); ?> |  {{ $invitation->timeslot }}</td>
                                       </tr>

                                       <tr>
                                        <th>Type:</th>
                                        <td>{{ucfirst($invitation->appointment_type)}}</td>
                                       </tr>

                                      <tr>
                                        <th>Category:</th>
                                        <td>{{ $invitation->category->category_name }}</td>
                                       </tr>

                                       <tr>
                                        <th>Doctor Address:</th>
                                        <td>{{ucfirst($invitation->user1->business_address)}}</td>
                                       </tr>

                                       <tr>
                                        <th>License Number:</th>
                                        <td>{{ $invitation->user1->doctor->licence_number }}</td>
                                       </tr>
                                   
                                   </table>

                                   <div class="button_containter">

                                        <a class="rountedBtn " style="background: #323B62 !important;"  href="{{url('invitation_details_view/'.$invitation->id)}}"  title="View Details">
                                            <span class="material-icons" style="color: white; font-size:27px;">visibility</span>
                                        </a>

                                   </div>

                                 </div>
                              </div>
                            @endforeach
                            @else
                              <h4 class="pt-3" style="color: #3A456A;">No appointment found!</h4>
                            @endif
                        </div>
                      </div>
                </section>
                {{-- ********************* Completed Invitation End ******************* --}}


                {{-- ********************* Cancalled Invitation Start ******************* --}}
				<section class="tab-pane fade" id="cancelledInvitation" role="tabpanel" aria-labelledby="nav-patients-tab">
                    <div class="container">
                        <div class="row">
                        @if(count($cancelled) > 0)
                            @foreach ($cancelled as $invitation)
                              <div class="col-md-12" style="padding: 0px; padding-left: 10px;" id="invitation_{{$invitation->id}}">
                                 <div class="design_card_new" style="background: white !important;"> 
                                    
                                    <div class="d-flex">
                                        @if (($invitation->user1->profilepic) !=null)
                                             <img class="n_images" src="{{ url('uploads/profilepics/doctor/'.$invitation->user1->profilepic) }}" />
                                         @else
                                            <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
                                        @endif
                                        <div class='n_c_container'>
                                            <h5 class="d_name_text" style="20px;">{{ucfirst($invitation->user1->name)}}</h5>
                                            <p class="firm_name font-weight-bold">{{ucfirst(@$desgination[$invitation->user1->desgination])}}</p>
                                        </div>
                                   </div>

                                   <table id="n_desing_tbl" class="mt-3">
                                      <tr>
                                        <th>Email:</th>
                                        <td>{{ $invitation->user1->email }}</td>
                                       </tr>

                                       <tr>
                                        <th>Date/Time:</th>
                                        <td><?php echo date('d F Y', strtotime($invitation->invitation_date)); ?> |  {{ $invitation->timeslot }}</td>
                                       </tr>

                                       <tr>
                                        <th>Type:</th>
                                        <td>{{ucfirst($invitation->appointment_type)}}</td>
                                       </tr>

                                      <tr>
                                        <th>Category:</th>
                                        <td>{{ $invitation->category->category_name }}</td>
                                       </tr>

                                       <tr>
                                        <th>Doctor Address:</th>
                                        <td>{{ucfirst($invitation->user1->business_address)}}</td>
                                       </tr>

                                       <tr>
                                        <th>License Number:</th>
                                        <td>{{ $invitation->user1->doctor->licence_number }}</td>
                                       </tr>
                                   
                                   </table>

                                   {{-- <div class="button_containter">

                                        <a class="rountedBtn " style="background: #323B62 !important;"  href="{{url('invitation_details_view/'.$invitation->id)}}"  title="View Details">
                                            <span class="material-icons" style="color: white; font-size:27px;">visibility</span>
                                        </a>

                                   </div> --}}

                                 </div>
                              </div>
                            @endforeach
                            @else
                              <h4 class="pt-3" style="color: #3A456A;">No appointment found!</h4>
                            @endif
                        </div>
                      </div>
                </section>
                {{-- ********************* Cancalled Invitation End ******************* --}}


            </div>
	
        </div>
    </section>
@endsection

@section('footerscripts') 

@isset($selectedTab)
@if ($selectedTab > 0)
    <script>
         var item = $("#myTab_"+"{{$selectedTab}}");
         $(".mytbslnk").removeClass("active");
        // console.log(item)
         item.click();
        // item.addClass("active");
    </script>
@endif
@endisset

@endsection