<style>
	.dllicon:hover{
		cursor: pointer;
	}
</style>


{{-- ***************** Upload doctor questionair start ******************* --}}
<div class="modal fade" id="upload_questionnair" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	  <div class="modal-content login_box">
		<div class="modal-header">
		  <h5 class="modal-title" >Upload Questionnaire</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
	
		  <form id="uploadquestionarie"  enctype="multipart/form-data">

        <div class="form-group files">
          <input type="file" class="form-control" name="questionari" id="questionfiles" required>
		  <p style="color:#DA1919;">Allowed formats: jpg, jpeg, png, docx, pdf</p>
			</div>

			  <div id="messageDetailsBlock" style="background: #F3F2F2; display: flex;align-items: center; padding: 4px 15px;border-radius: 5px; flex-wrap: wrap; overflow: hidden;">
				<p id="fileName" style="color:#323B62;flex:1; padding-right:15px; font-weight:bold;font-size:14px;">Your uploaded questionnaire</p>
				  <div style="display: flex;gap: 10px;">

					 <a id="vPathQ"  target="_blank" title="View questionnaire">
						<span class="material-icons" style="color: #323B62; font-size:24px;">visibility</span>
					 </a>

					  <a id="dPathQ"  download target="_blank" title="Download questionnaire">
						<span class="material-icons" style="color: #323B62; font-size:24px;">cloud_download</span>
					  </a>
					 
					  <span id='deletequestioers' title="Delete questionnaire" class="material-icons dllicon" style="color: red; font-size:24px;">delete</span>

				  </div>
			  </div>
			
			  <div id="questionar_success" class="alert alert-success" role="alert" style="display: none">
			     	File uploaded successfully
			  </div>

			  <div id="questionar_error" class="alert alert-danger" role="alert" style="display: none">
			     	Invalid file type, You can only upload jpeg, png, docx, pdf
			  </div>
	
			  <div id="progress_bar_questio">
				  
			  </div>	
               
			  <div id="loadingContainer" style="display: flex;align-items: center; display:none;">
				<img width="30" src="{{asset('img/loading.gif')}}" alt="">
                <p style="display:inline-block; margin-left:6px; font-weight:bold;">Loading...</p>
			  </div>

			  <button  type="button" class="invite_btn medicals_upload_btn" id="upload_questionair">Upload</button> 
		  </form>
	
		</div>
	
	  </div>
	</div>
	</div>	


 <script>

    $("#deletequestioers").click(function(){
		$("#loadingContainer").show();
		$("#upload_questionair").hide();
		$("#messageDetailsBlock").hide();

		//abs
		var docID = $("#accessDoctorID").val();
		var docID = docID != undefined || !isEmpty(docID) ? docID : "doctor";

		$.ajax({
			type:'GET',
			url: "{{ url('/delete_questionari')}}/"+docID,
			cache:false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: (data) => {
				if(data.status == '1'){
					$("#messageDetailsBlock").hide();
					$("#loadingContainer").hide();
					$("#upload_questionair").show();
					$("#questionar_success").text("Questionnaire deleted successfully!");
					$("#questionar_success").show();
					$("#updatelnk").attr('onclick',"openUploadQuestion(0)");

					setTimeout(() => {
						$("#questionar_success").hide();
					}, 5000);
				}
			},
			error: function(data){
				console.log("Error => ",data)
			}

		});

	});

	function openUploadQuestion(typ){
	
		$("#messageDetailsBlock").hide();
        $("#questionfiles").val(null);
		var docID = $("#accessDoctorID").val();
		var docID = docID != undefined || !isEmpty(docID) ? docID : "doctor";

		if(typ == 1){
			// update questionair
			$("#upload_questionair").hide();
			$("#loadingContainer").show();

			$.ajax({
			type:'GET',
			url: "{{ url('/doctorqpath')}}/"+docID,
			cache:false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: (data) => {
				if(data.status == '1'){
					$("#messageDetailsBlock").show();
					$("#loadingContainer").hide();
					$("#fileName").text(data.name);
					$("#vPathQ").attr('href',data.pt)
					$("#dPathQ").attr('href',data.pt)
					$("#upload_questionair").show();
					$("#updatelnk").attr('onclick',"openUploadQuestion(1)");
				}
				else{
					$("#messageDetailsBlock").hide();
					$("#loadingContainer").hide();
					$("#upload_questionair").show();
				}
			},
			error: function(data){
				console.log("Error => ",data)
			}

		});

		}
	 }

	 $("#upload_questionair").click(function(){
       //abs
        $("#questionar_error").hide();
        $("#questionar_success").hide();
        $("#progress_bar_questio").empty();

		var docID = $("#accessDoctorID").val();
		var docID = docID != undefined || !isEmpty(docID) ? docID : "doctor";

		let totalFiles = $('#questionfiles')[0].files.length; //Total files
        let files = $('#questionfiles')[0];
        var filenames = $('#questionfiles')[0].files;

		if(totalFiles > 0){
		    	const allowed = [
					"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
					"application/msword",
					"application/pdf",
					"image/jpeg",
					"image/png",
					"image/jpg",
				];
		    files = files.files[0];
			
			if(!allowed.includes(files.type)){
				$("#questionar_error").text("Invalid file type, You can only upload jpeg, png, docx, pdf");
				$("#questionar_error").show();
				setTimeout(() => {
					$("#questionar_error").hide();
				}, 5000);
              return;
			}

			var progress = `<div  class="progress mt-2"><div id="qprog11" class="progress-bar" style='background:#323B62' role="progressbar" style="width: 5%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">0% </div> </div><p>${files.name}</p>`;
	        $('#progress_bar_questio').append(progress);

			var formData = new FormData();
	        formData.append('file', files);
	        formData.append('docID', docID);

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}})
			$("#upload_questionair").hide();
			$.ajax({
			xhr: function() {
			    var xhr = new window.XMLHttpRequest();
				xhr.upload.onprogress = function (e) {
                  // For uploads
					if (e.lengthComputable) {
						var progress = (e.loaded / e.total) * 100;
						$("#qprog11").css('width',progress+"%");
                        $("#qprog11").text(progress+"%");
					}
				};
				return xhr;
			},
			type:'POST',
			url: "{{ url('/uploadQuestionair')}}",
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: (data) => {
				if(data.status == '1'){
					$('#progress_bar_questio').empty();
					$("#messageDetailsBlock").show();
					$("#loadingContainer").hide();
					$("#fileName").text(data.name);
					$("#vPathQ").attr('href',data.pt)
					$("#dPathQ").attr('href',data.pt)
					$("#upload_questionair").show();
				}else{
				  $('#progress_bar_questio').empty();
			      $("#questionar_error").show();
				  $("#questionar_error").text('Something went wrog try again');
				}
			},
			error: function(data){
				$('#progress_bar_questio').empty();
			    $("#questionar_error").show();
				$("#questionar_error").text('Error occured while uploading files, try again');
				$("#questionfiles").val(null);
				$("#upload_questionair").show();

				setTimeout(() => {
					$("#questionar_error").hide();
				}, 5000);
			}
		});
			

		}
		else{
			$("#questionar_error").text("Please choose file first!");
			$("#questionar_error").show();
			setTimeout(() => {
				$("#questionar_error").hide();
			}, 5000);
		}

	 });


 </script>

{{-- ***************** Upload doctor questionair end   ******************* --}}