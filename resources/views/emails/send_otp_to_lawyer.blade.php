@extends("includes.email_container")
@section("content")
            <div>

                 <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Hello {{ ucfirst($name) }},</h3>

                    <p style="font-size: 14px;line-height: 17px;">Verification Code for login is: <b>{{$otp}}</b></p>
                    <p style="font-size: 14px;line-height: 17px;">This code is valid for 40 seconds.</p>  
                   
                     <br/>

                    <br/>

                    <br/>

                    <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
                    <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 

            </div>  

               </div>

            </div>

@endsection