@extends("includes.email_container")
@section("content")
            <div>

                  <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Hello <b>{{$doctor}}</b>,</h3>

                    <p style="font-size: 14px;line-height: 17px;">Today is the last day for uploading the report for <b>{{$patient}}</b></p>           
                    <p style="font-size: 14px;line-height: 17px;">Click here to <a target="_blank" href="https://proassessors.com">login</a> and upload the report</p>           
                    
                   

                    <br/>

                    <br/>

                    <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
                    <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 

                    </div>  

               </div>

            </div>

 @endsection