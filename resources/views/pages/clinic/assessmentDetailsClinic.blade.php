@extends('layouts.clinic-layout') 
@section('content')	
<link href="{{ asset('css/clinic_dashboard.css') }}" rel="stylesheet">


<div class="trac_container container">
    <a href="/tracking/{{$user->id}}/{{$invitation->id}}" target="_blank" id='traceability_id'>Traceability</a>
</div>

<div class="container dashboardContainer mb-5">
   
    <div class="row">
       <div class="col-12">


           <div class="container-fluid">
             <div class="row">

                <div class="col-md-6 col-sm-12">
                    <h3 class="inv-content-header">Lawyer</h3>
                    @if ($user->profilepic)
                       <img class="img-thumbnail" style="width: 80px" src="{{ url('uploads/profilepics/lawyer/'.$user->profilepic) }}" alt=""> 
                    @else
                      <img class="img-thumbnail" style="width: 80px" src="{{asset("n_img/no_lawyer.jpg")}}" alt="">
                    @endif

                    <table class="mt-3 dtable">
                        <tbody>
                            <tr>
                                <th width="160">Name</th>
                                <td>{{$user->name}}</td>
                            </tr>
                            <tr>
                                <th width="160">Business Address</th>
                                <td>{{$user->business_address}}</td>
                            </tr>
                            <tr>
                                <th width="160">City</th>
                                <td>
                                    <?php $city = Helper::cities();  ?>  
                                    <?php  foreach ($city as $city1){ ?>
                                        <?php if(($city1->id)==($user->location)){echo ucfirst(strtolower($city1->city_name));} ?>
                                    <?php } ?></h5>
                                </td>
                            </tr>
                            <tr>
                                <th width="160">Law Firm Name</th>
                                <td>{{ ucfirst($user->lawyer->lawfirmname) }}</td>
                            </tr>
                            <tr>
                                <th width="160">Phone</th>
                                <td>{{$user->phone}}</td>
                            </tr>
                            <tr>
                                <th width="160">Email</th>
                                <td>{{$user->email}}</td>
                            </tr>
                            <tr>
                                <th width="160">Website Url</th>
                                <td>{{ $user->lawyer->website_url }}</td>
                            </tr>
                        </tbody>
                    </table>

                 </div>

                 <div class="col-md-6 col-sm-12">
                     <h3 class="inv-content-header">Patient</h3>
                     <img class="img-thumbnail" style="width: 80px" src="{{asset("n_img/no_patient.jpg")}}" alt="">
                     <table class="mt-3 dtable pl-md-4 pl-sm-0">
                        <tbody>
                            <tr>
                                <th width="160">Name</th>
                                <td>{{$patient->name}}</td>
                            </tr>
                            <tr>
                                <th width="160">Gender</th>
                                <td>{{ucfirst(MyHelper::Decrypt($patient->gender))}}</td>
                            </tr>
                            <tr>
                                <th width="160">Address</th>
                                <td>{{ucfirst(MyHelper::Decrypt($patient->address))}}</td>
                            </tr>
                            <tr>
                                <th width="160">Date of birth</th>
                                <td>{{date('d F Y', strtotime($patient->dateofbirth))}}</td>
                            </tr>
                            <tr>
                                <th width="160">Date of accident</th>
                                <td>{{date('d F Y', strtotime($patient->dateofaccident))}}</td>
                            </tr>
                            <tr>
                                <th width="160">Email</th>
                                <td>{{MyHelper::Decrypt($patient->email)}}</td>
                            </tr>
                            <tr>
                                <th width="160">Case type</th>
                                <td>
                                    @if ($patient->casetype == 1)
                                    MVA
                                    @elseif ($patient->casetype == 2)
                                    WSIB
                                    @elseif($patient->casetype == 3)
                                    Slip & Fall
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                 </div>


                 <div class="col-12 mt-5">
                    <h3 class="inv-content-header">Reports</h3>
                    <section class="py-3 header">
                        <div class="container py-3">
                          
                            <div class="row">
                                <div class="col-md-3">
                                    <!-- Tabs nav -->
                                    <div class="nav flex-column nav-pills nav-pills-custom" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                       
                                      <a class="nav-link mb-2 px-3 tabsShawo active" id="report-new" data-toggle="pill" href="#report" role="tab" aria-controls="v-pills-home" aria-selected="true">
                                            <i class="fa fa-book mr-2"></i>
                                            <span class="font-weight-bold small text-uppercase">Reports</span>
                                      </a>
                    
                                        <a class="nav-link mb-2 px-3  tabsShawo" id="medical-tab" data-toggle="pill" href="#medical" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                                            <i class="fa fa-user-md mr-2"></i>
                                            <span class="font-weight-bold small text-uppercase">Medicals</span>
                                        </a>
                    
                                        <a class="nav-link mb-2 px-3 tabsShawo" id="summary-tab" data-toggle="pill" href="#summary" role="tab" aria-controls="v-pills-messages" aria-selected="false">
                                            <i class="fa fa-file-text-o mr-2"></i>
                                            <span class="font-weight-bold small text-uppercase">Summary</span>
                                        </a>
                    
                                        <a class="nav-link mb-2 px-3 tabsShawo" id="completed-tab" data-toggle="pill" href="#letter_of_engagement" role="tab" aria-controls="v-pills-settings" aria-selected="false">
                                            <i class="fa fa-file-o mr-2"></i>
                                            <span class="font-weight-bold small text-uppercase">Letter of Engagement</span>
                                        </a>
                                    
                    
                                        <a class="nav-link mb-2 px-3 tabsShawo" id="form53-tab" data-toggle="pill" href="#form53" role="tab" aria-controls="v-pills-settings" aria-selected="false">
                                            <i class="fa fa-file mr-2"></i>
                                            <span class="font-weight-bold small text-uppercase">Form 53</span>
                                        </a>
                    
                                        <a class="nav-link mb-2 px-3 tabsShawo" id="questionare-tab" data-toggle="pill" href="#questionare" role="tab" aria-controls="v-pills-settings" aria-selected="false">
                                            <i class="fa fa-question-circle-o mr-2"></i>
                                            <span class="font-weight-bold small text-uppercase">Questionnaire</span>
                                        </a>
                    
                                    </div>
                                </div>
                    
                    
                                <div class="col-md-9">
                                    <!-- Tabs content -->
                                    <div class="tab-content h-100" id="v-pills-tabContent">

                                        @php
                                          $reportStatus = '';
                                          $reportClass = "";
                                        @endphp

                                        @if ($invitation->attachments != null)
                                        @if ($invitation->attachments->reportfilename == null)
                                            @php
                                              $reportStatus = '<h3 class="nothingTxt">Nothing to show</h3>';
                                              $reportClass = "emptyContainer";
                                            @endphp
                                        @endif
                                        @else     
                                          @php
                                              $reportStatus = '<h3 class="nothingTxt">Nothing to show</h3>';
                                              $reportClass = "emptyContainer";
                                          @endphp
                                        @endif
                    
                                      {{-- Report Section --}}
                                        <div class="tab-pane fade rounded bg-white show {{$reportClass}} active" id="report" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                            <ul class="mt-2 reportList_6" id="reports-ul{{$invitation->id}}" style="padding-left: 0px !important;" >
                                               {!! $reportStatus !!}
                                                @if ($invitation->attachments != null)
                                                   @php
                                                        $st=0;
                                                   @endphp
                                                   @if ($invitation->attachments->reportfilename != null)
                                                      @php
                                                        $a=json_decode($invitation->attachments->reportfilename);
                                                        $b=json_decode($invitation->attachments->reportimagename);
                                                        $b=json_decode( json_encode($b), true);
                                                        $i=0;
                                                        $ar = [
                                                        'pdf' => 'picture_as_pdf',
                                                        'doc' => 'upload_file',
                                                        'docx' => 'description'];
                                                      @endphp  

                                                    @foreach ($a as $key => $value)
                                                         @php
                                                              $st++;		
                                                         @endphp

                                                        <div style="background: #F3F2F2;border-radius: 5px; margin-bottom: 12px;padding: 5px 15px;display:flex;" id="reports{{ $invitation->id.$key }}">
                                                            <span class="material-icons" style="color: #20215E; font-size:24px; margin-right:8px;">{{$ar[explode('.',$b[$i])[1]]}}</span>

                                                            <form action="{{route('downloadEncFile')}}" style="margin-bottom: 0px; flex:1; display: inline-block" method="POST">
                                                                @csrf
                                                                <input name="path" type="hidden" value="{{$value}}">
                                                                <input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
                                                                <input name="name" type="hidden" value="{{$b[$i]}}">
                                                                <input name="directory" type="hidden" value="medical_reports">
                                                                <button style="background: transparent;
                                                                border: none;
                                                                font-weight: bold;
                                                                color: #20215E"><?php echo $b[$i]; ?></button>
                                                            </form>

                                                            <form action="{{route('downloadEncFile')}}" style="margin-bottom: 0px; display: inline-block" method="POST">
                                                                @csrf
                                                                <input name="path" type="hidden" value="{{$value}}">
                                                                <input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
                                                                <input name="name" type="hidden" value="{{$b[$i]}}">
                                                                <input name="directory" type="hidden" value="medical_reports">
                                                                <button style="border:none;">
                                                                    <i class="fa fa-cloud-download" style="font-size: 20px;color: #20215E; vertical-align: bottom;" ></i>
                                                                </button>
                                                            </form>
                                                        </div>
                                                       @php
                                                          $i++; 
                                                       @endphp
                                                    @endforeach

                                                   @endif
                                                @endif
                                              																		
                                            </ul>
                                        </div>
                                        
                                        {{-- Medical section --}}
                                        @php
                                            $reportStatus_1 = '';
                                           
                                        @endphp

                                        @if ($invitation->attachments != null)
                                        @if ($invitation->attachments->medicalsfilename == null)
                                            @php
                                                $reportStatus_1 = '<h3 class="nothingTxt">Nothing to show</h3>';
                                               
                                            @endphp
                                        @endif
                                        @else     
                                            @php
                                                $reportStatus_1 = '<h3 class="nothingTxt">Nothing to show</h3>';
                                                
                                            @endphp
                                        @endif

                                        <div class="tab-pane fade  rounded bg-white" id="medical" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                            <ul class="mt-4" style="padding-left:0px;">
                                                {!! $reportStatus_1 !!}
                                                @if ($invitation->attachments && $invitation->attachments->medicalsfilename)
                                                    @php
                                                          $a=json_decode($invitation->attachments->medicalsfilename);
                                                          $b=json_decode($invitation->attachments->medicalsimagename);
                                                          $b=json_decode( json_encode($b), true);
                                                          $i=0;
                                                    @endphp

                                                 @foreach ($a as $key => $value)
                                                 <div>
                                                    <form action="{{route('downloadEncFile')}}" class="downloadForDes" method="POST">
                                                        @csrf
                                                        <input name="path" type="hidden" value="{{$value}}">
                                                        <input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
                                                        <input name="name" type="hidden" value="{{$b[$i]}}">
                                                        <input name="directory" type="hidden" value="l_medical">
                                                        <button class="downloadTitleDesign"><?php echo $b[$i]; ?></button>
                                                        <button type="submit" style="background: transparent;border: none;">
                                                            <i class="fa fa-cloud-download" style="font-size: 20px;color: #20215E; vertical-align: bottom;" ></i>
                                                        </button>
                                                    </form>
                                                </div>
                                                <form id="medical-file-download-form{{ $invitation->id.$key }}" action="{{ url('download-medicals-file') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="filename" value="{{ $value }}">
                                                    <input type="hidden" name="user_id" value="{{ $invitation->invitee_user_id }}" />
                                                    <input type="hidden" name="download_name" value="{{ $b[$i] }}">
                                                </form>
                                                  @php
                                                      $i++;
                                                  @endphp
                                                 @endforeach

                                                @endif												
                                           </ul>

                                        </div>
                                        
                                         {{-- Summary section --}}
                                         @php
                                            $reportStatus_2 = "";
                                        @endphp

                                        @if ($invitation->attachments != null)
                                        @if ($invitation->attachments->filename == null)
                                            @php
                                                $reportStatus_2 = '<h3 class="nothingTxt">Nothing to show</h3>';
                                            @endphp
                                        @endif
                                        @else     
                                            @php
                                                $reportStatus_2 = '<h3 class="nothingTxt">Nothing to show</h3>';
                                            @endphp
                                        @endif

                                        <div class="tab-pane fade  rounded bg-white" id="summary" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                            <ul class="mt-4" style="padding-left:0px;">
                                                {!! $reportStatus_2 !!}
                                                @if ($invitation->attachments && $invitation->attachments->filename)
                                                    @php
                                                        $a=json_decode($invitation->attachments->filename);
													    $b=json_decode($invitation->attachments->imagename);
													     $b=json_decode( json_encode($b), true);
                                                          $i=0;
                                                    @endphp

                                                 @foreach ($a as $key => $value)
                                                 <div>
                                                    <form action="{{route('downloadEncFile')}}" class="downloadForDes" method="POST">
                                                        @csrf
                                                        <input name="path" type="hidden" value="{{$value}}">
                                                        <input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
                                                        <input name="name" type="hidden" value="{{$b[$i]}}">
                                                        <input name="directory" type="hidden" value="l_summary">
                                                        <button class="downloadTitleDesign"><?php echo $b[$i]; ?></button>
                                                        <button type="submit" style="background: transparent;border: none;">
                                                            <i class="fa fa-cloud-download" style="font-size: 20px;color: #20215E; vertical-align: bottom;" ></i>
                                                        </button>
                                                    </form>
                                                </div>
                                                <form id="medical-file-download-form{{ $invitation->id.$key }}" action="{{ url('download-medicals-file') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="filename" value="{{ $value }}">
                                                    <input type="hidden" name="user_id" value="{{ $invitation->invitee_user_id }}" />
                                                    <input type="hidden" name="download_name" value="{{ $b[$i] }}">
                                                </form>
                                                  @php
                                                      $i++;
                                                  @endphp
                                                 @endforeach

                                                @endif												
                                           </ul>
                                        </div>
                                        
                                         {{-- letter_of_engagement section --}}
                                          @php
                                            $reportStatus_3 = "";
                                            @endphp

                                            @if ($invitation->attachments != null)
                                            @if ($invitation->attachments->loefilename == null)
                                                @php
                                                    $reportStatus_3 = '<h3 class="nothingTxt">Nothing to show</h3>';
                                                @endphp
                                            @endif
                                            @else     
                                                @php
                                                    $reportStatus_3 = '<h3 class="nothingTxt">Nothing to show</h3>';
                                                @endphp
                                            @endif

                                        <div class="tab-pane fade  rounded bg-white" id="letter_of_engagement" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                            <ul class="mt-4" style="padding-left:0px;">
                                               {!! $reportStatus_3 !!}
                                                @if ($invitation->attachments && $invitation->attachments->loefilename)
                                                    @php
                                                        $a=json_decode($invitation->attachments->loefilename);
												    	$b=json_decode($invitation->attachments->loeimagename);
													    $b=json_decode( json_encode($b), true);
                                                          $i=0;
                                                    @endphp

                                                 @foreach ($a as $key => $value)
                                                 <div>
                                                    <form action="{{route('downloadEncFile')}}" class="downloadForDes" method="POST">
                                                        @csrf
                                                        <input name="path" type="hidden" value="{{$value}}">
                                                        <input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
                                                        <input name="name" type="hidden" value="{{$b[$i]}}">
                                                        <input name="directory" type="hidden" value="l_LetterOfEngagement">
                                                        <button class="downloadTitleDesign"><?php echo $b[$i]; ?></button>
                                                        <button type="submit" style="background: transparent;border: none;">
                                                            <i class="fa fa-cloud-download" style="font-size: 20px;color: #20215E; vertical-align: bottom;" ></i>
                                                        </button>
                                                    </form>
                                                </div>
                                                <form id="medical-file-download-form{{ $invitation->id.$key }}" action="{{ url('download-medicals-file') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="filename" value="{{ $value }}">
                                                    <input type="hidden" name="user_id" value="{{ $invitation->invitee_user_id }}" />
                                                    <input type="hidden" name="download_name" value="{{ $b[$i] }}">
                                                </form>
                                                  @php
                                                      $i++;
                                                  @endphp
                                                 @endforeach

                                                @endif												
                                           </ul>
                                        </div>
                    
                                         {{-- Form 53 section --}}
                                         @php
                                         $reportStatus_4 = "";
                                         @endphp

                                         @if ($invitation->attachments != null)
                                         @if ($invitation->attachments->form53filename == null)
                                             @php
                                                 $reportStatus_4 = '<h3 class="nothingTxt">Nothing to show</h3>';
                                             @endphp
                                         @endif
                                         @else     
                                             @php
                                                 $reportStatus_4 = '<h3 class="nothingTxt">Nothing to show</h3>';
                                             @endphp
                                         @endif
                                        <div class="tab-pane fade  rounded bg-white" id="form53" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                            <ul class="mt-4" style="padding-left:0px;">
                                                {!! $reportStatus_4 !!}
                                                @if ($invitation->attachments && $invitation->attachments->form53filename)
                                                    @php
                                                        $a=json_decode($invitation->attachments->form53filename);
												     	$b=json_decode($invitation->attachments->form53imagename);
												    	$b=json_decode( json_encode($b), true);
                                                          $i=0;
                                                    @endphp

                                                 @foreach ($a as $key => $value)
                                                 <div>
                                                    <form action="{{route('downloadEncFile')}}" class="downloadForDes" method="POST">
                                                        @csrf
                                                        <input name="path" type="hidden" value="{{$value}}">
                                                        <input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
                                                        <input name="name" type="hidden" value="{{$b[$i]}}">
                                                        <input name="directory" type="hidden" value="l_form_53">
                                                        <button class="downloadTitleDesign"><?php echo $b[$i]; ?></button>
                                                        <button type="submit" style="background: transparent;border: none;">
                                                            <i class="fa fa-cloud-download" style="font-size: 20px;color: #20215E; vertical-align: bottom;" ></i>
                                                        </button>
                                                    </form>
                                                </div>
                                                <form id="medical-file-download-form{{ $invitation->id.$key }}" action="{{ url('download-medicals-file') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="filename" value="{{ $value }}">
                                                    <input type="hidden" name="user_id" value="{{ $invitation->invitee_user_id }}" />
                                                    <input type="hidden" name="download_name" value="{{ $b[$i] }}">
                                                </form>
                                                  @php
                                                      $i++;
                                                  @endphp
                                                 @endforeach

                                                @endif												
                                           </ul>
                                        </div>
                    
                                         {{-- Questionare section --}}
                                         @php
                                         $reportStatus_5 = "";
                                         @endphp

                                         @if ($invitation->attachments != null)
                                         @if ($invitation->attachments->insformfilename == null)
                                             @php
                                                 $reportStatus_5 = '<h3 class="nothingTxt">Nothing to show</h3>';
                                             @endphp
                                         @endif
                                         @else     
                                             @php
                                                 $reportStatus_5 = '<h3 class="nothingTxt">Nothing to show</h3>';
                                             @endphp
                                         @endif
                                        <div class="tab-pane fade  rounded bg-white" id="questionare" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                            <ul class="mt-4" style="padding-left:0px;">
                                                {!! $reportStatus_5 !!}
                                                @if ($invitation->attachments && $invitation->attachments->insformfilename)
                                                    @php
                                                        $a=json_decode($invitation->attachments->insformfilename);
													    $b=json_decode($invitation->attachments->insformimagename);
													    $b=json_decode( json_encode($b), true);
                                                        $i=0;
                                                    @endphp

                                                 @foreach ($a as $key => $value)
                                                 <div>
                                                    <form action="{{route('downloadEncFile')}}" class="downloadForDes" method="POST">
                                                        @csrf
                                                        <input name="path" type="hidden" value="{{$value}}">
                                                        <input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
                                                        <input name="name" type="hidden" value="{{$b[$i]}}">
                                                        <input name="directory" type="hidden" value="l_questionair">
                                                        <button class="downloadTitleDesign"><?php echo $b[$i]; ?></button>
                                                        <button type="submit" style="background: transparent;border: none;">
                                                            <i class="fa fa-cloud-download" style="font-size: 20px;color: #20215E; vertical-align: bottom;" ></i>
                                                        </button>
                                                    </form>
                                                </div>
                                                <form id="medical-file-download-form{{ $invitation->id.$key }}" action="{{ url('download-medicals-file') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="filename" value="{{ $value }}">
                                                    <input type="hidden" name="user_id" value="{{ $invitation->invitee_user_id }}" />
                                                    <input type="hidden" name="download_name" value="{{ $b[$i] }}">
                                                </form>
                                                  @php
                                                      $i++;
                                                  @endphp
                                                 @endforeach

                                                @endif												
                                           </ul>
                                        </div>
                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    
                </div>

             </div>
           </div>

       </div>
    </div>
</div>

@endsection