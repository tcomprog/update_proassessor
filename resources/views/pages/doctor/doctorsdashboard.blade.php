@extends('layouts.app')
@section('content')

<div class="container">
    <div class="">                
        <h1>Dashboard</h1>
        @if(Session::has('updatesuccess'))
        <div class="alert alert-success alert-dismissible fade show">
                <strong>{{ Session::get('updatesuccess') }}</strong>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                    @php
                    Session::forget('updatesuccess');
                    @endphp
            </div>
         @endif
         @if(Session::has('deletesuccess'))
        <div class="alert alert-success alert-dismissible fade show">
                <strong>{{ Session::get('deletesuccess') }}</strong>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                    @php
                    Session::forget('deletesuccess');
                    @endphp
            </div>
         @endif
    </div>
    <!----------modal start----------------------->
     <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form method="POST" action="{{ route('invitedoctor') }}">
                        @csrf
                        <div class="form-group row">
                           
                            <div class="col-md-6">
                                <input id="invitee_user_id" type="hidden"  name="invitee_user_id" >
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <h4>Are you sure ?</h4>
                                <button type="submit" class="btn btn-success">
                                    Send Invitation
                                </button>
                            </div>
                        </div>
                    </form>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
    <!----------------modal end-------------------->
    <!----search-------------->
    <form method="GET" action="{{ route('doctor/dashboard') }}" >
        

        <div class="form-group row">
            <div class="col-md-7">

               
                <input id="keyword" type="text" class="form-control @error('keyword') is-invalid @enderror" name="keyword"   placeholder="Please Enter Any Keyword" value="<?php if(isset($_GET['keyword'])){echo $_GET['keyword'];}?>   " autocomplete="keyword" autofocus>

                @error('keyword')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="col-md-3">
                <button type="submit" class="btn btn-primary">
                    {{ __('Search') }}
                </button>
            </div>
        </div>
    </form>
    <!------search end--------->
    <div class="row justify-content-center">
        <!------table start-------------->

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">                   
                    <table   id="myTable" class="table table-bordered table-hover" style="width:100%">
                        <tr>
                        <td>S.No</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Phone</td>
                        <td>Designation</td>
                        <td>Business Location</td>
                        <td >Action</td>
                        </tr>
                        <!--------test code-------->
                        
                        <!-----------test code--->

                        <?php $number = 1; ?>
                        @foreach ($users as $user)
                        <tr>
                        <td>{{ $number }}</td>
                        <?php $number++; ?>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td><?php if($user->role_id==1){ echo "Doctor"; } if($user->role_id==2){ echo "Lawyer"; } if($user->role_id==3){ echo "Admin"; }?></td>
                        <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
                        <td>{{ $user->business_address }}</td>
                       
                        <td>
                        <!--<button type="button"
                            class="btn btn-warning  openBtn"
                            data-toggle="modal"
                            data-id = {!! $user->id !!}>
                            Invite
                        </button>-->                                      
                            <button type="button" id="inviter_user_id" class="btn btn-info" data-id ={!! $user->id !!} >No Requests</button>
                             
                        </td>
                        </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
       
        <!----------table end------------->
    </div>
</div>
@endsection

@section('footerscripts')

<script type="text/javascript">

$(document).ready(function () {

    $('#myTable #inviter_user_id').on('click', function (e) {
        e.preventDefault();
       var inviter_user_id = $(this).data('id');        
        //alert(inviter_user_id);
        $('#invitee_user_id').val(inviter_user_id);       
          
        });      
});
</script>

@endsection
