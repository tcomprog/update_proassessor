<?php
namespace App\Http\Middleware;
use App\Article;//
use Closure;
use Illuminate\Contracts\Auth\Guard;//
use Illuminate\Http\Request;
class AdminMiddleware
{
    public function handle(Request $request, Closure $next)
    { 
        $user = auth()->user();      
        //print_r($user);
        //die;
        if($user->role_id != 3){
            abort(403, 'Unauthorized action.');
        }
        return $next($request);
    }
}
