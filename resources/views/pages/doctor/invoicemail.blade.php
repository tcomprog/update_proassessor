   <div class="invoicebody">
      <div class="clearfix header" style="padding: 10px 0;margin-bottom: 20px;border-bottom: 1px solid #AAAAAA;">
         <div id="logo" style="float: none;margin-top: 8px;text-align: center;">
            <img style="height: 70px;" src="{{ asset('img/logo.jpg') }}">
         </div>
      </div>
      <div>
         <div id="details" class="clearfix" style="margin-bottom: 20px;">
            <table border="0" cellspacing="0" cellpadding="0">
               <tr>
                  <td style="border-left: 6px solid #00528c;padding-left: 10px;">
               <p style="font-family: 'Source Sans Pro', sans-serif;font-size: 19px;"><b style="font-weight: 800;">{{ $invitation->user1->name }}</b><p>
               <p style="color:#000;font-size: 6px;visibility: hidden;">-----</p>
               <p style="font-family: 'Source Sans Pro', sans-serif;margin-bottom: 10px;padding-bottom: 10px;font-size: 15px;"><?php  $category = Helper::categorydetails();  
									    foreach ($category as $categories){ 
											if(($categories->id)==($invitation->user1->doctor->category_id)){
												echo ucfirst($categories->category_name); }
											} ?>: {{ $invitation->user1->doctor->licence_number }}</p>								
					<p style="color:#000;font-size: 6px;visibility: hidden;">-----</p>
               <p style="font-family: 'Source Sans Pro', sans-serif;font-size: 15px;">PH: <a href="tel:{{ $invitation->user1->phone }}">{{ $invitation->user1->phone }}</a></p>
                  </td>
                  <td style="width:270px"></td>
                  <td align="right">
                     <p style="font-family: 'Source Sans Pro', sans-serif;font-size: 17px;"><b style="font-weight: 800;">Invoice #{{str_pad($invitation->invoices->doctor_invoice_number, 2, '0', STR_PAD_LEFT)}}</b><P>
                     <p style="color:#000;font-size: 6px;visibility: hidden;">-----</p>
                     <p style="font-family: 'Source Sans Pro', sans-serif;font-size: 19px;"><b style="font-weight: 800;">Bill to: Pro Assessors</b><P>
                     <p style="color:#000;font-size: 6px;visibility: hidden;">-----</p>
                     <p style="font-family: 'Source Sans Pro', sans-serif;font-size: 15px;">Invoice Date: <?php $date=date("Y-m-d"); echo date('F d, Y', strtotime($date)); ?></p>
                  </td>
               </tr>
            </table>
         </div>
         <table class="invoice-table" border="0" cellspacing="0" cellpadding="0" style="width: 100%;border-collapse: collapse;
               border-spacing: 0;
               margin-bottom: 20px;">
            <thead>
               <tr>
                  <th class="no heading" style="color:#fff;font-family: 'Source Sans Pro', sans-serif;padding: 20px;
                     background: #ffffff;
                     text-align: center;
                     border-bottom: 1px solid #ddd;
                     border-right: 1px solid #ddd;
                     border-left: 1px solid #ddd; color: #fff !important;
                      font-size: 1em;
                      background: #004b82;">PATIENT</th>
                  <th class="desc heading" style="color:#fff;font-family: 'Source Sans Pro', sans-serif;padding: 20px;
                     background: #ffffff;
                     text-align: center;
                     border-bottom: 1px solid #ddd;
                     border-right: 1px solid #ddd;
                     border-left: 1px solid #ddd; color: #fff !important;
                      font-size: 1em;
                      background: #004b82;">SERVICE PROVIDED</th>
                  <th class="unit heading" style="color:#fff;font-family: 'Source Sans Pro', sans-serif;padding: 20px;
                     background: #ffffff;
                     text-align: center;
                     border-bottom: 1px solid #ddd;
                     border-right: 1px solid #ddd;
                     border-left: 1px solid #ddd; color: #fff !important;
                      font-size: 1em;
                      background: #004b82;">SERVICE DATE</th>
                  <th class="qty heading" style="color:#fff;font-family: 'Source Sans Pro', sans-serif;padding: 20px;
                     background: #ffffff;
                     text-align: center;
                     border-bottom: 1px solid #ddd;
                     border-right: 1px solid #ddd;
                     border-left: 1px solid #ddd; color: #fff !important;
                      font-size: 1em;
                      background: #004b82;">FEE</th>
               </tr>
            </thead>
            <tbody>
              <!-- ROW -->
               <tr>
                  <td class="no" style="padding:16px;padding: 20px;
                     background: #ffffff;
                     text-align: left;
                     border-bottom: 1px solid #ddd;
                     border-right: 1px solid #ddd;
                     border-left: 1px solid #ddd;">
                    <h4 style="font-family: 'Source Sans Pro', sans-serif;font-weight: 700;font-size: 16px;">{{ $invitation->patient->name}}</h4>
                    <span style="color: #888;font-family: 'Source Sans Pro', sans-serif;font-size: 15px;">Date of Birth:</span> <span style="font-size: 15px;"><?php echo date('F d, Y', strtotime($invitation->patient->dateofbirth)); ?></span><br/>
                    <span style="color: #888;font-family: 'Source Sans Pro', sans-serif;font-size: 15px;">Date of Accident:</span>  <span style="font-size: 15px;"><?php echo date('F d, Y', strtotime($invitation->patient->dateofaccident)); ?></span>
                  </td>
                  <td class="desc" style="padding:16px;padding: 20px;
                     background: #ffffff;
                     text-align: center;
                     border-bottom: 1px solid #ddd;
                     border-right: 1px solid #ddd;
                     border-left: 1px solid #ddd;">
                     <h3 style="font-family: 'Source Sans Pro', sans-serif;font-weight: 500;font-size: 16px;">{{$invitation->invoices->serviceprovided}}</h3>
                  </td>
                  <td class="unit" style="font-family: 'Source Sans Pro', sans-serif;padding:16px;padding: 20px;
                     background: #ffffff;
                     text-align: center;
                     border-bottom: 1px solid #ddd;
                     border-right: 1px solid #ddd;
                     border-left: 1px solid #ddd;"><?php echo date('F d, Y', strtotime($invitation->invitation_date)); ?></td>
                  <td class="qty" style="font-family: 'Source Sans Pro', sans-serif;padding:16px;padding: 20px;
                     background: #ffffff;
                     text-align: center;
                     border-bottom: 1px solid #ddd;
                     border-right: 1px solid #ddd;
                     border-left: 1px solid #ddd;">$<span class="new_bill_amt"><?php if($invitation->invoices->payment_amount!=null){
                        
                        if($invitation->user1->doctor->hst =='yes') { 
                           $actualamt=$invitation->invoices->payment_amount;
                           $withouthstpercentage=($invitation->invoices->payment_amount/1.13);
                           $hstpercentage=($actualamt-$withouthstpercentage);
                           echo number_format((float) $withouthstpercentage, 2, '.', '');
                       }else{
                           echo number_format((float) $invitation->invoices->payment_amount, 2, '.', '');
                       }
                        
                     } ?></span></td>
               </tr>
               <!-- ROW -->
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="2" style="border: none;background: #ffffff;"></td>
                  <td colspan="1" style="font-family: 'Source Sans Pro', sans-serif;font-weight: 800;background: #ffffff;
                      padding: 10px 20px;
                        background: #FFFFFF;
                        font-size: 1.2em;
                        white-space: nowrap; 
                        border-bottom: 1px solid #ddd;
                     border-right: 1px solid #ddd;
                     border-left: 1px solid #ddd;text-align: center;">TOTAL</td>
                  <td style="font-family: 'Source Sans Pro', sans-serif;font-weight: 800;background: #ffffff;
                      padding: 10px 20px;
                     background: #FFFFFF;
                     font-size: 1.2em;
                     white-space: nowrap; 
                     border-bottom: 1px solid #ddd;
                     border-right: 1px solid #ddd;
                     border-left: 1px solid #ddd;text-align: center;">$<span class="new_bill_amt" ><?php if($invitation->invoices->payment_amount!=null){
                             if($invitation->user1->doctor->hst =='yes') { 
                                 $actualamt=$invitation->invoices->payment_amount;
                                 $withouthstpercentage=($invitation->invoices->payment_amount/1.13);
                                 $hstpercentage=($actualamt-$withouthstpercentage);
                                 echo number_format((float) $withouthstpercentage, 2, '.', '');
                             }else{
                                    echo number_format((float) $invitation->invoices->payment_amount, 2, '.', '');
                             }
                     } ?></span></td> 
                  
               </tr>
               <?php  if($invitation->user1->doctor->hst =='yes') { ?> 
               <tr>
                  <td colspan="2" style="border: none;background: #ffffff;"></td>
                  <td colspan="1" style="font-family: 'Source Sans Pro', sans-serif;font-weight: 800;background: #ffffff;
                      padding: 10px 20px;
                        background: #FFFFFF;
                        font-size: 1.2em;
                        white-space: nowrap; 
                        border-bottom: 1px solid #ddd;
                     border-right: 1px solid #ddd;
                     border-left: 1px solid #ddd;text-align: center;">HST</td>
                  <td style="font-family: 'Source Sans Pro', sans-serif;font-weight: 800;background: #ffffff;
                      padding: 10px 20px;
                     background: #FFFFFF;
                     font-size: 1.2em;
                     white-space: nowrap; 
                     border-bottom: 1px solid #ddd;
                     border-right: 1px solid #ddd;
                     border-left: 1px solid #ddd;text-align: center;">$<span class="new_bill_amt" ><?php if($invitation->invoices->payment_amount!=null){

                        $actualamt=$invitation->invoices->payment_amount;
                        $withouthstpercentage=($invitation->invoices->payment_amount/1.13);
                        $hstpercentage=($actualamt-$withouthstpercentage);
                        echo number_format((float) $hstpercentage, 2, '.', '');
                        } ?></span></td> 
                  
               </tr>
               <tr>
                  <td colspan="2" style="border: none;background: #ffffff;"></td>
                  <td colspan="1" style="font-family: 'Source Sans Pro', sans-serif;font-weight: 800;background: #ffffff;
                      padding: 10px 20px;
                        background: #FFFFFF;
                        font-size: 1.2em;
                        white-space: nowrap; 
                        border-bottom: 1px solid #ddd;
                     border-right: 1px solid #ddd;
                     border-left: 1px solid #ddd;text-align: center;">SUB TOTAL</td>
                  <td style="font-family: 'Source Sans Pro', sans-serif;font-weight: 800;background: #ffffff;
                      padding: 10px 20px;
                     background: #FFFFFF;
                     font-size: 1.2em;
                     white-space: nowrap; 
                     border-bottom: 1px solid #ddd;
                     border-right: 1px solid #ddd;
                     border-left: 1px solid #ddd;text-align: center;">$<span class="new_bill_amt" ><?php if($invitation->invoices->payment_amount!=null){
                         echo number_format((float) $invitation->invoices->payment_amount, 2, '.', '');
                        } ?></span></td> 
                  
               </tr>
               <?php   }  ?>
            </tfoot>
         </table>
      </div>      
</div>
