<?php

namespace App\Console\Commands;

use App\Models\ClientSubscription;
use App\Mail\ExpirarySubscriptionWarning;
use App\Models\User;

use Mail;

use Illuminate\Console\Command;

class ExpirySubWarningEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'package_expiry_warning_email:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expiry warning';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = date('Y-m-d', strtotime(' +3 day'));
        $sub = ClientSubscription::where('status','1')->where('to_date',$date)->get();
 
        foreach($sub as $row){
           $user = User::select('name','email')->find($row->user_id);
 
           Mail::to($user->email)->send(new ExpirarySubscriptionWarning(
             [
               'to'=> $user->email,
               'subject'=>"Subscription expiry warning",
               'name'=>$user->name,
               'package'=>$row->package,
               'from_date'=>date("d-m-Y",strtotime($row->from_date)),
               'to_date'=>date("d-m-Y",strtotime($row->to_date)),
               ]
           ));
        }
    }
}
