<script src="{{ asset('js/app.js') }}" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" ></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

<!--added-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/js/standalone/selectize.js"></script><!--select--> 
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    $("#cln_changeHstValue").on('click',function(){
        var hst_number = $("#cl_hst_number_inp").val();

        if(!isEmpty(hst_number)){
            $("#hst_numberInp").val(hst_number); 
            $("#addcl_hstNumberModel").modal('hide');
        }else{
                $("#hst_success").hide();
                $("#hst_error").show();
                $("#hst_error").text("Please enter HST number first");
                setTimeout(() => {
                $("#hst_error").hide();
                }, 3000);
        }
});



// ************************* Add new doctor section ************************
$("#addNewDoctorBtn").on('click',function(){

$("#agrrur_error").hide();
$("#descField_error").hide();
$("#phone_error").hide();
$("#address_error").hide();
$("#name_error").hide();
$("#category_error").hide();
$("#hst_number_error").hide();
$("#price_error").hide();
$("#license_no_error").hide();
$("#assessmentOnly_error").hide();
$("#lateCancellationFee_error").hide();
$("#hstEditnumber").hide();

let name = $("#name1").val();
let business_address = $("#business_address1").val();
//let phone = $("#phone1").val();
let agrrur = $("#agrrur").val();
let licence_number = $("#license_no").val();
let category = $('#category_id').val();
let city = $('#city_drop :selected').val();
let desgination = $('#designation_drop :selected').val();
let price =  $("#actualPriceDoctor").val(); 
let assessmentOnly =  $("#assessmentOnly").val(); 
let lateCancellationFee =  $("#lateCancellationFee").val(); 
let introduction =  $("#introduction").val(); 
agrrur = 1;
let virtualAppoiment = $('input[name="virtual_appointment"]:checked').val();
let hstValue = $('input[name="adhst"]:checked').val();
let hst_number = "null";
let error = 0;

if(!isEmpty(hstValue) && hstValue == "yes"){
    hst_number = $("#hst_numberInp").val();
    if(isEmpty(hst_number) || hst_number == 'null'){ 
       $("#hst_number_error").show();
       $("#hst_number_error").text("Please enter HST number.");
       $("#hstEditnumber").show();
       error = 1;
         setTimeout(() => {
         $("#hst_number_error").hide();
         $("#hstEditnumber").hide();
       }, 6000);
    }
}

 if(isEmpty(licence_number)){
     $("#license_no_error").show();
     $("#license_no_error").text("This field is required");
     error = 1;
     setTimeout(() => {
       $("#license_no_error").hide();
      }, 4000);
 }
 else{
     $("#license_no_error").hide();
 }

 if(isEmpty(price) || price <= 0){
     $("#price_error").show();
     $("#price_error").text("Price should be greater than zero");
     error = 1;
     setTimeout(() => {
       $("#price_error").hide();
      }, 4000);
 }
 else{
     $("#price_error").hide();
 }

 if(isEmpty(lateCancellationFee) || lateCancellationFee <= 0){
     $("#lateCancellationFee_error").show();
     $("#lateCancellationFee_error").text("Value should be greate than zero");
     error = 1;
     setTimeout(() => {
       $("#lateCancellationFee_error").hide();
      }, 4000);
 }
 else{
     $("#lateCancellationFee_error").hide();
 }

 if(isEmpty(assessmentOnly) || assessmentOnly <= 0){
     $("#assessmentOnly_error").show();
     $("#assessmentOnly_error").text("Value should be greate than zero");
     error = 1;
     setTimeout(() => {
       $("#assessmentOnly_error").hide();
      }, 4000);
 }
 else{
     $("#assessmentOnly_error").hide();
 }

 if(isEmpty(category)){
     $("#category_error").show();
     $("#category_error").text("This field is required");
     error = 1;
     setTimeout(() => {
       $("#category_error").hide();
      }, 4000);
 }
 else{
     $("#category_error").hide();
 }

 if(isEmpty(business_address)){
     $("#address_error").show();
     $("#address_error").text("Business address is required");
     error = 1;
     setTimeout(() => {
       $("#address_error").hide();
      }, 4000);
 }
 else{
     $("#address_error").hide();
 }

 if(isEmpty(name)){
     $("#name_error").show();
     $("#name_error").text("Name is required");
     error = 1;
     setTimeout(() => {
       $("#name_error").hide();
      }, 4000);
 }
 else{
     $("#name_error").hide();
 }


 // if(isEmpty(phone)){
 //   $("#phone_error").show();
 //   $("#phone_error").text("Phone number is required");
 //   error = 1;
 //   setTimeout(() => {
 //     $("#phone_error").hide();
 //   }, 4000);
 // }
 // else{
 //   if(digits_only(phone) == false){
 //     $("#phone_error").show();
 //     $("#phone_error").text("Invalid phone number");
 //     error = 1;
 //   }else{
 //     $("#phone_error").hide();
 //   }
 // }

 if(error == 1){
     return;
 }else{
     $("#loadingIMG").show();
     $("#addNewDoctorBtn").hide();

     category = category.toString();
   
     $.ajax({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         url : "/addNewDoctorProfile",
         data : {
             name:name,
             email:"null",
             address:business_address,
             password:"null",
             re_password:"null",
             phone:'',
             city:city,
             licence_number:licence_number,
             category:category,
             term_condition:agrrur,
             desginationDoctor:$('#designation_drop :selected').text(),
             desginationDoctorID:desgination,
             assessmentOnly:assessmentOnly,
             lateCancellationFee:lateCancellationFee,
             introduction:introduction,
             hst:hstValue,
             hst_number:hst_number,
             virtualAppoiment:virtualAppoiment,
             price:price
         },
         type : 'POST',
         dataType : 'json',
         success : function(response){
             $("#loadingIMG").hide();
             $("#addNewDoctorBtn").show();

             console.log(response)

             if(response.status == 'success'){
                 $("#success_msg").show();
                 $("#error_msg").hide();
                 $("#success_msg").text("Doctor added successfully and account activation email has been send to doctor");

                 $("#name1").val('');
                 $("#business_address1").val('');
                 $("#phone1").val('');
                 $("#license_no").val('');
                 $("#category_id").val('');
                 $("#city_drop").val(1);
                 $("#designation_drop").val(1);
                 $("#actualPriceDoctor").val(0);
                 $("#assessmentOnly").val(0);
                 $("#lateCancellationFee").val(0);
                 $("#introduction").val('');
                 $("#hst_numberInp").val('null');

                 setTimeout(() => {
                     $("#success_msg").hide();
                 }, 4000);
             } 
             else if(response.status == 'exist'){
                 $("#success_msg").hide();
                 $("#error_msg").show();
                 $("#error_msg").text("Doctor account already exist with provided email try different one");
                 setTimeout(() => {
                     $("#error_msg").hide();
                 }, 4000);
             }

         },
         error:function(){
           $("#loadingIMG").hide();
           $("#addNewDoctorBtn").show();
           $("#success_msg").hide();
           $("#error_msg").show();
           $("#error_msg").text("Something went wrong try again later");
           setTimeout(() => {
             $("#error_msg").hide();
           }, 4000);
      }
   });
 }

});

</script>