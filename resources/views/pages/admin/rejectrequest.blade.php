@extends('layouts.admin_app')


@section('content')

<div class="container-fluid py-4 adminPageContainer">

    {{-- <div class="row">
		<div class="col-md-12">
			 <nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
				<li class="breadcrumb-item active"><a href="{{url('/doctors')}}">Rejected Requests</a></li>
			  </ol>
			</nav>
		</div>
    </div> --}}


    @php
        $Options = [
            "1" => "Option 1",
            "2" => "Option 2",
            "3" => "Option 3",
            "4" => "Other"
        ];
    @endphp


    <div class="row">

      <h4 class="inv-content-header mb-3">Reject Requests</h4>


        <table id="datatbl11" class="row-border hover cell-border" style="width:100%">
            <thead>
                <tr>
                 <th width="10%">S.No</th>
                 <th width="15%">Patient</th>
                 <th width="15%">Option</th>
                 <th>Description</th>
                 <th width="10%">Date</th>
                 <th width="10%"></th>
                </tr>
            </thead>

             <tbody>
                @foreach ($records as $ind => $row)
                @php
                   $str = (strlen($row->desc) > 110) ? substr($row->desc,0,100)."..."  : $row->desc;
                @endphp
                  <tr>
                    <td>{{$ind+1}}</td>
                    <td>{{$row->patient_id}}</td>
                    <td>{{$Options[$row->option]}}</td>
                    <td title="{{$row->desc}}">{{$str}}</td>
                    <td>{{date('d-m-Y',strtotime($row->created_at))}}</td>
                    <td align="center">
                        <a href="reject_details/{{$row->id}}" class="btn btn-success btn-sm">View</a>
                    </td>
                  </tr>
                @endforeach
            </tbody>

        </table>

    </div>
 

@endsection


