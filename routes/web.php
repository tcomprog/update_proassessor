<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use App\Models\user_invitations;
use App\Models\Patient;
use App\Mail\Report_Due_Alert;
use App\Models\User;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('clear-cache', function () {
	$run = Artisan::call('config:clear');
    $run = Artisan::call('cache:clear');
    $route = Artisan::call('route:clear');
    return 'FINISHED';
}); 

Auth::routes();
Route::get('/',  [App\Http\Controllers\HomepageController::class, 'index'])->name('index');
Route::get('/htest',  [App\Http\Controllers\HomepageController::class, 'testing'])->name('htest');

Route::group(['middleware' => ['App\Http\Middleware\AdminMiddleware']], function() {   
	Route::get('lawyer_agreement/{id}', [App\Http\Controllers\adminController::class,'lawyer_agreement'])->name('lawyer_agreement');
});


Route::get('/tracking/{lawyerID}/{reportID}', [App\Http\Controllers\lawyerController::class, 'lawyerTracking'])->name('lawyerTracking');


// !!!!!!!!!!!!!!!!!!  Admin Section Start !!!!!!!!!!!!!!!!!!!!!!!!!
 Route::group(['middleware' => ['otpcookie','check-login','auth','prevent-back-history','App\Http\Middleware\AdminMiddleware']], function() {   
    
	Route::post('admin/addLawyerType',[App\Http\Controllers\adminController::class,'addLawyerType']);
	Route::post('admin/addCity',[App\Http\Controllers\adminController::class,'addCity']);
	Route::post('admin/addCategory',[App\Http\Controllers\adminController::class,'addCategory']);
	Route::post('admin/addDesgination',[App\Http\Controllers\adminController::class,'addDesgination']);
	Route::post('admin/changesLawyerStatus',[App\Http\Controllers\adminController::class,'changesLawyerStatus']);
	Route::post('changePriceRequestStatus',[App\Http\Controllers\adminController::class,'changePriceRequestStatus']);
	Route::post('updateCompanyProfile',[App\Http\Controllers\adminController::class,'updateCompanyProfile']);
	Route::post('assignSpecailPrice',[App\Http\Controllers\adminController::class,'assignSpecailPrice']);
	Route::post('deleteSpecialOffer',[App\Http\Controllers\adminController::class,'deleteSpecialOffer']);

	Route::get('/admin_assess_details/{id}', [App\Http\Controllers\adminController::class, 'admin_assess_details'])->name("admin_assess_details");

	Route::get('/config/lawyer_types', [App\Http\Controllers\adminController::class, 'lawyer_types'])->name('/config/lawyer_types');
	Route::get('/config/city', [App\Http\Controllers\adminController::class, 'config_city']);
	Route::get('/config/category', [App\Http\Controllers\adminController::class, 'config_category']);
	Route::get('/config/designation', [App\Http\Controllers\adminController::class, 'config_desgination']);
	Route::get('/config/cmp_profile', [App\Http\Controllers\adminController::class, 'companyProfile'])->name("loadProfilePage");

	Route::get('/clinic/profile/{id}', [App\Http\Controllers\adminController::class, 'clinicProfile'])->name('/clinic/profile');
	Route::get('/delete_contact_email/{id}', [App\Http\Controllers\adminController::class, 'deleteContactEmail'])->name('deleteContactEmail');


	Route::get('/dashboard', [App\Http\Controllers\adminController::class, 'index'])->name('dashboard');
	Route::get('/reject_req', [App\Http\Controllers\adminController::class, 'reject_req'])->name('reject_req');
	Route::get('/doctors/{type}', [App\Http\Controllers\adminController::class, 'doctors'])->name('doctors');
	
	Route::get('/clinic/{type}', [App\Http\Controllers\adminController::class, 'clinic'])->name('doctors');
	Route::get('/clinic/edit_package/{type}', [App\Http\Controllers\adminController::class, 'editPackages'])->name('editPackages');
	Route::get('/clinic/packages/view', [App\Http\Controllers\adminController::class, 'packages'])->name('packages');
	
	Route::post('/updatePackage', [App\Http\Controllers\adminController::class, 'updatePackage']);
	
	Route::post('/doctors-paginate', [App\Http\Controllers\adminController::class, 'get_doctors_paginate']);
	Route::get('/lawyers/{type}', [App\Http\Controllers\adminController::class, 'lawyers'])->name('lawyers');	
	Route::get('/manage_appointments', [App\Http\Controllers\adminController::class, 'manage_appointments'])->name('manage_appointments');
	Route::post('/admin_logout', [App\Http\Controllers\adminController::class, 'admin_logout'])->name('admin_logout');
	Route::post('/doctor_payment_status_update', [App\Http\Controllers\adminController::class, 'doctor_payment_status_update'])->name('doctor_payment_status_update');
	Route::get('doctor_file/{id}', [App\Http\Controllers\adminController::class,'doctor_file'])->name('doctor_file');
	Route::get('lawyer_file/{id}', [App\Http\Controllers\adminController::class,'lawyer_file'])->name('lawyer_file');
	
	Route::get('admin/change-password', [App\Http\Controllers\adminController::class,'change_password']);
	Route::get('admin/contact_us', [App\Http\Controllers\adminController::class,'customerEmails'])->name('customerEmails');
	Route::get('admin/send_email', [App\Http\Controllers\adminController::class,'sendEmailView'])->name('sendEmailView');

	Route::post('admin/sendEmailToUser',[App\Http\Controllers\adminController::class,'sendEmailToUser']);
	Route::post('admin/change-password',[App\Http\Controllers\adminController::class,'set_new_password']);
	Route::get('admin/Lawyer_transportation_translation',[App\Http\Controllers\adminController::class,'lawyerTransportationTranslation'])->name('admin/Lawyer_transportation_translation');	
	Route::post('admin_invoice_modal_view',[App\Http\Controllers\adminController::class,'admin_invoice_modal_view']);
	Route::post('/admin_custom_invoice', [App\Http\Controllers\adminController::class,'custominvoice'])->name('custom_invoice');
	Route::post('/doctor_status_update', [App\Http\Controllers\adminController::class,'doctor_status_update']);
	Route::post('/lawyer_status_update', [App\Http\Controllers\adminController::class,'lawyer_status_update']);
	Route::get('admin/cms', [App\Http\Controllers\adminController::class,'cms']);
	Route::get('admin/editpage/{slug}', [App\Http\Controllers\adminController::class,'editpage']);
	Route::post('admin/editpage', [App\Http\Controllers\adminController::class,'edit_page']);
	Route::post('lawyer_email_update', [App\Http\Controllers\adminController::class,'lawyer_email_update']);
	Route::post('admin/settings-update', [App\Http\Controllers\adminController::class,'settings_update']);
	Route::get('admin/addsettings', [App\Http\Controllers\adminController::class,'addsettings'])->name('admin/addsettings');
	Route::get('admin/editsettings/{id}', [App\Http\Controllers\adminController::class,'editsettings']);
	Route::post('admin/edit_settings', [App\Http\Controllers\adminController::class,'edit_settings'])->name('admin/edit_settings'); 
	Route::post('changeDateRequest', [App\Http\Controllers\adminController::class,'changeDateRequest'])->name('changeDateRequest'); 
	Route::get('reject_details/{id}', [App\Http\Controllers\adminController::class,'reject_details'])->name('reject_details'); 
	Route::get('/priceRequest', [App\Http\Controllers\adminController::class,'changePriceRequest']); 
});
// !!!!!!!!!!!!!!!!!!  Admin Section End !!!!!!!!!!!!!!!!!!!!!!!!!


// @@@@@@@@@@@@@@@@@ Doctor Section Start @@@@@@@@@@@@@@@@@@@@@@
Route::group(['middleware' => ['check-login','check-session','otpcookie','prevent-back-history','auth','role:doctor']], function(){
	
	Route::get('/doctor-dashboard/{activeTab?}', [App\Http\Controllers\doctorController::class, 'dashboard'])->name('doctor-dashboard');
	Route::get('/doctor-dashboard/archived-invitations', [App\Http\Controllers\doctorController::class, 'dashboard'])->name('doctor-dashboard/archived-invitations');
	Route::get('/doctor-private', [App\Http\Controllers\doctorController::class, 'privateInfor'])->name('privateInfor');
	Route::get('/search-confirmed-patient', [App\Http\Controllers\doctorController::class, 'dashboard'])->name('search-confirmed-patient');
	Route::get('check-doctor-confirmed-invitations', [App\Http\Controllers\doctorController::class, 'check_doctor_confirmed_invitations']);

	Route::post('/deletereports', [App\Http\Controllers\doctorController::class, 'deletereports'])->name('deletereports');	
	Route::post('/updateprivateinfo', [App\Http\Controllers\doctorController::class, 'updateprivateinfo'])->name('updateprivateinfo');	
	Route::post('/deletetask', [App\Http\Controllers\doctorController::class, 'deletetask'])->name('deletetask');	
	Route::post('/deleteprofilepic', [App\Http\Controllers\doctorController::class, 'deleteprofilepic'])->name('deleteprofilepic');	
	Route::post('/edittask', [App\Http\Controllers\doctorController::class, 'edittask'])->name('edittask');	
	Route::post('/deleteInsform', [App\Http\Controllers\doctorController::class, 'deleteInsform'])->name('deleteInsform');	
	Route::post('doctor_confirmed_archive', [App\Http\Controllers\doctorController::class, 'doctor_confirmed_archive']);
	Route::post('deleteMedicalReport', [App\Http\Controllers\doctorController::class, 'deleteMedicalReport']);	

});	
// @@@@@@@@@@@@@@@@@ Doctor Section End @@@@@@@@@@@@@@@@@@@@@@

Route::group(['middleware' => ['auth','role:lawyer']], function(){
	Route::get('download_agreement', [App\Http\Controllers\lawyerController::class,'download_agreement'])->name('download_agreement');
});	


// |||||||||||||||||||||||| Doctor & Clinic Common Start |||||||||||||||||||||||||
Route::group(['middleware' => ['check-login','check-session','otpcookie','prevent-back-history','auth','role:clinic,doctor']], function(){
	Route::get('/doctor-profile/{id?}', [App\Http\Controllers\doctorController::class, 'profile'])->name('doctor-profile');
	Route::get('/doctorqpath/{docID?}', [App\Http\Controllers\doctorController::class, 'doctorqpath'])->name('doctorqpath');
	Route::get('/delete_questionari/{docID?}', [App\Http\Controllers\doctorController::class, 'delete_questionari'])->name('delete_questionari');
	Route::get('invitedetail/{invitation_id}/{activeTab?}', [App\Http\Controllers\doctorController::class, 'invitedetail'])->name('invitedetail');
	Route::get('check-doctor-new-invitations/{doctorID?}', [App\Http\Controllers\doctorController::class, 'check_doctor_new_invitations']);

	Route::post('doctor/block-dates', [App\Http\Controllers\doctorController::class, 'block_dates']);
	Route::post('/doctortasks', [App\Http\Controllers\doctorController::class, 'doctortasks'])->name('doctortasks');	
	Route::post('/updatehstNumber', [App\Http\Controllers\doctorController::class, 'hstUpdate'])->name('updatehstNumber');	
	Route::post('/blockeddates', [App\Http\Controllers\doctorController::class, 'blockeddates'])->name('blockeddates');
	Route::post('doctor_invoice_modal_view', [App\Http\Controllers\doctorController::class, 'doctor_invoice_modal_view']);
	Route::post('/custom_invoice', [App\Http\Controllers\doctorController::class,'custominvoice'])->name('custom_invoice');
	Route::post('/uploadinvoice', [App\Http\Controllers\doctorController::class, 'uploadinvoice'])->name('uploadinvoice');
	Route::post('/deleteinvoice', [App\Http\Controllers\doctorController::class, 'deleteinvoice'])->name('deleteinvoice');	
	Route::post('/uploadreports', [App\Http\Controllers\doctorController::class, 'uploadreports'])->name('uploadreports');
	Route::post('/acceptint', [App\Http\Controllers\doctorController::class, 'acceptint'])->name('acceptint');	
	Route::post('/rejectint', [App\Http\Controllers\doctorController::class, 'rejectint'])->name('rejectint');	
	Route::post('/uploadQuestionair', [App\Http\Controllers\doctorController::class, 'uploadQuestionair'])->name('uploadQuestionair');
	Route::post('/deletecv', [App\Http\Controllers\doctorController::class, 'deletecv'])->name('deletecv');	
	Route::post('/updatedoctor', [App\Http\Controllers\doctorController::class, 'updatedoctor'])->name('updatedoctor');	
	Route::post('changePriceRequest', [App\Http\Controllers\doctorController::class, 'changePriceRequest']);
});
// |||||||||||||||||||||||| Doctor & Clinic Common End   |||||||||||||||||||||||||


// ***************** Clinic Section Start *****************
Route::group(['middleware' => ['check-login','check-session','otpcookie','prevent-back-history','auth','role:clinic','check_clinic_subscritpion']], function(){
     Route::get("/clinic-dashboard",[App\Http\Controllers\ClinicController::class, 'dashboard'])->name("clinicDashboard");
	 Route::get('/clinic-profile', [App\Http\Controllers\ClinicController::class, 'profile']);
	 Route::get('/addClinicDoctor', [App\Http\Controllers\ClinicController::class, 'addClinicDoctor'])->name("addClinicDoctor");
	 Route::get('/showClinicDoctorList', [App\Http\Controllers\ClinicController::class, 'showClinicDoctorList'])->name("showClinicDoctorList");
	 Route::get('/cldoctorprofile/{id}', [App\Http\Controllers\ClinicController::class, 'clincDoctorProfile'])->name("cldoctorprofile");
	 Route::get('/approveDoctor/{id}', [App\Http\Controllers\ClinicController::class, 'approveDoctor'])->name("approveDoctor");
	 Route::get('/assessmentDetails/{id}', [App\Http\Controllers\ClinicController::class, 'assessmentDetails'])->name("assessmentDetails");
	 Route::get('/viewClinicCalender/{id}', [App\Http\Controllers\ClinicController::class, 'viewClinicCalender'])->name("viewClinicCalender");
	 Route::get('/clinic_subscription', [App\Http\Controllers\ClinicController::class, 'clinicSubscriptionDetails'])->name("clinic_subscription");
	 
	// ******** Stripe integration start *********
	  Route::get('/subscription', [App\Http\Controllers\Stripe::class, 'index'])->name('subscription');
	  Route::post('/sub_checkout_page', [App\Http\Controllers\Stripe::class, 'subsctionCheckoutPage'])->name('sub_checkout_page');
	  Route::post('/init_ui', [App\Http\Controllers\Stripe::class, 'intStripUi'])->name('init_ui');
	  Route::get('/complete_subscr', [App\Http\Controllers\Stripe::class, 'complete_subscr'])->middleware("prevent-back-history")->name('complete_subscr');
	// ******** Stripe integration end   *********


	 Route::post('/clinic-profile-update', [App\Http\Controllers\ClinicController::class, 'updateProfile']);
	 Route::post('/changeClinicPassword', [App\Http\Controllers\ClinicController::class, 'changeClinicPassword'])->name('changeClinicPassword');
	 Route::post('/addNewDoctorProfile', [App\Http\Controllers\ClinicController::class, 'addNewDoctorProfile'])->name('addNewDoctorProfile');

});
// ***************** Clinic Section End  *****************


// ################ Lawyer Section Start ##################
Route::group(['middleware' => ['check-login','check-session','otpcookie','prevent-back-history','auth','role:lawyer,patient']], function(){
	
	Route::get('/patient-profile', [App\Http\Controllers\PatientController::class, 'openPatientProfile'])->name('patient-profile');
	Route::get('/patient-dashboard', [App\Http\Controllers\PatientController::class, 'patientDashboard'])->name('patientDashboard');
	Route::get('/invitation_details_view/{id}', [App\Http\Controllers\PatientController::class, 'viewInvitationDetails'])->name('invitation_details_view');

	Route::post('/updatePatientProfile', [App\Http\Controllers\PatientController::class, 'updatePatientProfile'])->name('updatePatientProfile');
	Route::post('/changeInvitationStatus', [App\Http\Controllers\PatientController::class, 'changeInvitationStatus'])->name('changeInvitationStatus');



	Route::get('/lawyer-profile', [App\Http\Controllers\lawyerController::class, 'profile']);
	
	Route::get('/notification', [App\Http\Controllers\HomeController::class, 'notification'])->name('notification');
	Route::get('/lawyer-dashboard/{activeTab?}', [App\Http\Controllers\lawyerController::class, 'dashboard'])->name('lawyer-dashboard');
	Route::get('/patient_status', [App\Http\Controllers\lawyerController::class, 'dashboard'])->name('patient_status');
	Route::get('/search-doctor', [App\Http\Controllers\lawyerController::class, 'searchdoctor'])->name('search-doctor');
	Route::get('/search-patient', [App\Http\Controllers\lawyerController::class, 'dashboard'])->name('search-patient');
	Route::get('/tracking', [App\Http\Controllers\lawyerController::class, 'dashboard'])->name('tracking');
	Route::get('/doctorcalview', [App\Http\Controllers\lawyerController::class,'doctorcalview'])->name('doctorcalview');
	Route::get('/viewcalendar/{id}/{categoryid?}', [App\Http\Controllers\lawyerController::class,'viewcalendar'])->name('viewcalendar');
	Route::get('/patient_details/{patientid}', [App\Http\Controllers\lawyerController::class,'patient_details'])->name('patient_details');	
	Route::get('/patientdetails', [App\Http\Controllers\lawyerController::class,'patientdetails'])->name('patientdetails');		
	Route::get('lawyer/ajax-search-doctors-listing', [App\Http\Controllers\AjaxUtilController::class, 'ajax_search_doctors_listing']);
	Route::get('/lawyer-chat/{invitationId}', [App\Http\Controllers\ChatController::class, 'lawyerChat'])->name('lawyer-chat');	


	Route::post('/clear_notif', [App\Http\Controllers\HomeController::class, 'clear_notif'])->name('clear_notif');
	Route::post('/updatelawyer', [App\Http\Controllers\lawyerController::class, 'updatelawyer'])->name('updatelawyer');
	Route::post('/changeLawyerPassword', [App\Http\Controllers\lawyerController::class, 'changeLawyerPassword'])->name('changeLawyerPassword');
	Route::post('/uploaddocuments', [App\Http\Controllers\lawyerController::class, 'uploaddocuments'])->name('uploaddocuments');
	Route::post('/addpatient', [App\Http\Controllers\lawyerController::class, 'addpatient'])->name('addpatient');
	Route::post('/editpatient', [App\Http\Controllers\lawyerController::class, 'editpatient'])->name('editpatient');	
	Route::post('/deletelawprofilepic', [App\Http\Controllers\lawyerController::class, 'deletelawprofilepic'])->name('deletelawprofilepic');
	Route::post('/doctorslots', [App\Http\Controllers\lawyerController::class, 'doctorslots'])->name('doctorslots');	
	Route::post('/deletedocument', [App\Http\Controllers\lawyerController::class, 'deletedocument'])->name('deletedocument');
	Route::post('/deletehyperlink', [App\Http\Controllers\lawyerController::class, 'deletehyperlink']);
	Route::post('/patientdelete', [App\Http\Controllers\lawyerController::class,'patientdelete'])->name('patientdelete');		
	Route::post('/invitedoctor', [App\Http\Controllers\lawyerController::class, 'invitedoctor'])->name('invitedoctor');
	Route::post('/sendinvitation', [App\Http\Controllers\lawyerController::class, 'sendinvitation'])->name('sendinvitation');
	Route::post('request-transport-transalation', [App\Http\Controllers\lawyerController::class, 'update_transport_transalation']);	
	Route::post('request-transalation', [App\Http\Controllers\lawyerController::class, 'update_transalation']);	
	Route::post('request-amendment', [App\Http\Controllers\lawyerController::class, 'update_amendment']);
	Route::post('cancel-appointment', [App\Http\Controllers\lawyerController::class, 'cancel_appointment']);	
	Route::post('upload-medicals', [App\Http\Controllers\lawyerController::class, 'upload_medicals']);
	Route::post('delete-medicals-file', [App\Http\Controllers\lawyerController::class, 'delete_medical_file']);
	Route::post('/uploadLetterOfEngagementDocs', [App\Http\Controllers\lawyerController::class, 'uploadLetterOfEngagementDocs'])->name('uploadLetterOfEngagementDocs');
	Route::post('/uploadForm53Docs', [App\Http\Controllers\lawyerController::class, 'uploadForm53Docs']);
	Route::post('/uploadinsform', [App\Http\Controllers\lawyerController::class, 'uploadinsform']);
	Route::post('/deleteloe', [App\Http\Controllers\lawyerController::class, 'deleteloe']);
	Route::post('/deleteform53', [App\Http\Controllers\lawyerController::class, 'deleteform53']);
	Route::post('/deleteinsform', [App\Http\Controllers\lawyerController::class, 'deleteinsform']);
	Route::post('resend-mail-to-patient', [App\Http\Controllers\lawyerController::class, 'resend_mail_to_patient']);	
	Route::post('deleteAllReports', [App\Http\Controllers\lawyerController::class, 'deleteAllReports']);	
});
// ################ Lawyer Section End  ##################

Route::group(['middleware' => ['otpcookie','prevent-back-history','auth']], function(){
	Route::get('logout', [App\Http\Controllers\SigninController::class, 'logout']);	
	Route::get('chat/load-messages', [App\Http\Controllers\ChatController::class, 'load_messages']);
	Route::post('chat/store-message', [App\Http\Controllers\ChatController::class, 'store_message']);	
	Route::get('ajax-get-doctor-categories', [App\Http\Controllers\AjaxUtilController::class, 'doctor_categories']);
	Route::post('download-medicals-file', [App\Http\Controllers\lawyerController::class, 'download_medical_file']); //Download Medical file can be downloaded lawyer/Doctor
	Route::get('/notification', [App\Http\Controllers\HomeController::class, 'notification'])->name('notification');
	Route::post('/clear_notif', [App\Http\Controllers\HomeController::class, 'clear_notif'])->name('clear_notif');

});


Route::group(['middleware' => ['check-login','check-session','otpcookie','prevent-back-history','auth','role:clinic,doctor,lawyer,patient']], function(){
	Route::post('/unreadNotification', [App\Http\Controllers\HomeController::class, 'unreadNotification'])->name('unreadNotification');
});


Route::get('/about', [App\Http\Controllers\HomeController::class, 'about'])->name('about');
Route::get('/benefit', [App\Http\Controllers\HomeController::class, 'benefit'])->name('benefit');
Route::get('/activate_account/{id}', [App\Http\Controllers\HomeController::class, 'activateaccount'])->name('activate_account');
Route::post('/user/resetpwd', [App\Http\Controllers\HomeController::class, 'resetpwd'])->name('user/resetpwd');

Route::get('/user/pwdset', [App\Http\Controllers\HomeController::class, 'pwdset'])->name('user/pwdset');

Route::get('/reset_newpwd/{id}', [App\Http\Controllers\HomeController::class, 'resetnewpwd'])->name('reset_newpwd');
Route::get('/terms&conditions', [App\Http\Controllers\HomeController::class, 'termsconditions'])->name('terms&conditions');
Route::get('/privacy_policy', [App\Http\Controllers\HomeController::class, 'privacypolicy'])->name('privacy_policy');

Route::post('/signupcreate', [App\Http\Controllers\SignupController::class, 'signupcreate'])->name('signupcreate');
Route::post('/signupcreatenew', [App\Http\Controllers\SignupController::class, 'signupcreate_new'])->name('signupcreatenew');

Route::post('/lawyersignupcreate', [App\Http\Controllers\SignupController::class, 'lawyersignupcreate'])->name('lawyersignupcreate');
Route::post('/assessment', [App\Http\Controllers\HomeController::class, 'assessment'])->name('assessment');
Route::get('/admin', [App\Http\Controllers\SigninController::class, 'index'])->name('admin');
Route::post('/user/login', [App\Http\Controllers\SigninController::class, 'loginUser'])->name('loginUser');
Route::post('/user/otplogin', [App\Http\Controllers\SigninController::class, 'loginUserWithOtp'])->name('loginUserWithOtp');
Route::post('/login', [App\Http\Controllers\SigninController::class, 'login'])->name('login');
Route::post('/resendotp', [App\Http\Controllers\SigninController::class, 'resendotp'])->name('resendotp');

Route::post('/uploadProfileImage', [App\Http\Controllers\lawyerController::class, 'uploadProfileImage'])->name('uploadProfileImage');
Route::get('/deleteProfileImage', [App\Http\Controllers\lawyerController::class, 'deleteProfileImage'])->name('deleteProfileImage');

Route::get('/patient/reject-invitation/{invitation_id}', [App\Http\Controllers\PatientController::class, 'reject_invitation']);
Route::get('patient/accept-invitation/{invitation_id}', [App\Http\Controllers\PatientController::class, 'accept_invitation']);
Route::post('patient/tracking', [App\Http\Controllers\PatientController::class, 'invitation_tracking']);
Route::post('acceptreports', [App\Http\Controllers\PatientController::class, 'accept_reports']);
Route::post('patient_visit', [App\Http\Controllers\PatientController::class, 'patient_visit']);

Route::post('rejectReportRequest', [App\Http\Controllers\PatientController::class, 'rejectReportRequest'])->name("rejectReportRequest");
Route::post('/downloadEncFile', [App\Http\Controllers\fileEncryption::class, 'downloadMedicalReport'])->name("downloadEncFile");


Route::post('/webhook', [App\Http\Controllers\Stripe::class, 'webhook'])->name('webhook');


Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

Route::get('/testing', function(){
    return view('emails.patient_account_created');
   // return view('testing');
});



