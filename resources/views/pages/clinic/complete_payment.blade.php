<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ProAssessors</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('fav/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('fav/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('fav/favicon-16x16.png')}}/">
    <link rel="manifest" href="{{asset('fav/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('fav/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <style>
body {
  background: #383562;
}

.container {
  max-width: 380px;
  margin: 30px auto;
  overflow: hidden;
}

.printer-top {
  z-index: 1;
  border: 6px solid #666666;
  height: 6px;
  border-bottom: 0;
  border-radius: 6px 6px 0 0;
  background: #333333;
}

.printer-bottom {
  z-index: 0;
  border: 6px solid #666666;
  height: 6px;
  border-top: 0;
  border-radius: 0 0 6px 6px;
  background: #333333;
}

.paper-container {
  position: relative;
  overflow: hidden;
  height: 467px;
}

.paper {
  background: #ffffff;
  height: 447px;
  position: absolute;
  z-index: 2;
  margin: 0 12px;
  margin-top: -12px;
  animation: print 1800ms cubic-bezier(0.68, -0.55, 0.265, 0.9);
  -moz-animation: print 1800ms cubic-bezier(0.68, -0.55, 0.265, 0.9);
}

.main-contents {
  margin: 0 12px;
  padding: 24px;
}

// Paper Jagged Edge
.jagged-edge {
  position: relative;
  height: 20px;
  width: 100%;
  margin-top: -1px;
}

.jagged-edge:after {
  content: "";
  display: block;
  position: absolute;
  //bottom: 20px;
  left: 0;
  right: 0;
  height: 20px;
  background: linear-gradient( 45deg, transparent 33.333%, #ffffff 33.333%, #ffffff 66.667%, transparent 66.667%), linear-gradient( -45deg, transparent 33.333%, #ffffff 33.333%, #ffffff 66.667%, transparent 66.667%);
  background-size: 16px 40px;
  background-position: 0 -20px;
}

.success-icon {
  text-align: center;
  font-size: 48px;
  height: 72px;
  background: #359d00;
  border-radius: 50%;
  width: 72px;
  height: 72px;
  margin: 16px auto;
  color: #fff;
}

.success-title {
  font-size: 22px;
  text-align: center;
  color: #666;
  font-weight: bold;
  margin-bottom: 16px;
}

.success-description {
  font-size: 15px;
  line-height: 21px;
  color: #999;
  text-align: center;
  margin-bottom: 24px;
}

.order-details {
  text-align: center;
  color: #333;
  font-weight: bold;
  .order-number-label {
    font-size: 18px;
    margin-bottom: 8px;
  }
  .order-number {
    border-top: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
    line-height: 48px;
    font-size: 48px;
    padding: 8px 0;
    margin-bottom: 24px;
  }
}

.order-footer {
  text-align: center;
  line-height: 18px;
  font-size: 18px;
  margin-bottom: 8px;
  font-weight: bold;
  color: #999;
}
@keyframes print {
  0% {
    transform: translateY(-90%);
  }
  100% {
    transform: translateY(0%);
  }
}
@-webkit-keyframes print {
  0% {
    -webkit-transform: translateY(-90%);
  }
  100% {
    -webkit-transform: translateY(0%);
  }
}

@-moz-keyframes print {
  0% {
    -moz-transform: translateY(-90%);
  }
  100% {
    -moz-transform: translateY(0%);
  }
}

@-ms-keyframes print {
  0% {
    -ms-transform: translateY(-90%);
  }
  100% {
    -ms-transform: translateY(0%);
  }
}

.er_container{
    width: 100%;
    height: 97vh;
    background: white;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    border-radius: 10px;
}
.er_container > h2{
    color:#707070;
    font-size: 2.5rem;
}
.btns{
    border: 1px solid #383562;
    padding: 7px 30px;
    border-radius: 5px;
    color: #383562;
    text-decoration: none;
}
.btns:hover{
    border: 1px solid #383562;
    padding: 7px 30px;
    color: white;
    border-radius: 5px;
    background: #383562;
    text-decoration: none;
}
</style>

</head>
<body>
    
    @if ($status == "completed")
    <div class="container">
        <div class="printer-top"></div>
          
        <div class="paper-container">
          <div class="printer-bottom"></div>
      
          <div class="paper">
            <div class="main-contents">
              <div class="success-icon">&#10004;</div>
              <div class="success-title">
                Payment Complete
              </div>
              <div class="success-description">
                We have successfully received your payment. You will be able to use the system after redirecting. Please check your email for payment confirmation.
              </div>
              <div class="order-details">
                <div class="order-number-label">Status</div>
                <div class="order-number">Subscripted</div>
              </div>
                <div class="order-footer">Thank you!</div>
            </div>
            <div class="jagged-edge"></div>
          </div>
        </div>
      </div>
      @elseif ($status == "error")
      <div class="er_container">
          <h2>Something went wrong try again</h2>
          <a class="btns" href="{{route("subscription")}}">Back</a>
      </div>
      @endif


</body>
</html>

<script>
    setTimeout(() => {
       location.replace("/");
    }, 10000);
</script>
