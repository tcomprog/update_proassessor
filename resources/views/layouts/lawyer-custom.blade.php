<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body style="display: flex; flex-direction:column; height:100vh;">
    <header>
        <div class="container">
            <div class="row">
                @include('includes.header')
            </div>
        </div>
    </header>

    
    @yield('content')

    <footer style="margin-top:auto; padding-bottom:0px; padding-top:25px;">
        @include('includes.footer')
    </footer>
    @include('includes.model')
    @include('includes.lawyer-scripts')
</body>
</html>