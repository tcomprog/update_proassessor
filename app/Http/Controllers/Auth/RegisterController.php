<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\doctors;
use App\Models\lawyers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $uniqueid = uniqid();

        $data1 = new User();
        $data1->name=$data['name'];
        $data1->email=$data['email'];
        $data1->unique_id=$uniqueid;
        $data1->password=Hash::make($data['password']);
        $data1->phone=$data['phone'];
        $data1->role_id=$data['designation'];
        $data1->business_address=$data['business_address'];
        $data1->location=$data['location'];
        $data1->remember_token=$data['_token'];
       // print_r($data1);exit;
        $data1->save();
       //echo  $data1->id;exit;

       $last_insertid=$data1->id;

       if(isset($last_insertid)){
           if($data1->role_id==1){

            $data2 = new doctors();
            $data2->user_id=$last_insertid;
            $data2->unique_id=$uniqueid;
            $data2->licence_number=$data['licencenumber'];
            $data2->introduction=$data['introduction'];
            $data2->save();
           return redirect()->route('login');

           }
       }
       if(isset($last_insertid)){
        if($data1->role_id==2){           
            $data3 = new lawyers();
            $data3->user_id= $last_insertid;
            $data3->website_url=$data['websiteurl'];
            $data3->save();
            return redirect()->route('login');

        }
      }
        /*
        print_r($data);exit;
        $uniqueid = uniqid();
       // echo $uniqueid;exit;
         User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'unique_id' => $uniqueid,
            'password' => Hash::make($data['password']),
            'phone' => $data['phone'],
            'role_id' => $data['designation'],
            'business_address' => $data['business_address'],
            'location' => $data['location'],
            'remember_token' => $data['_token'],
        ]);*/
    }
}
