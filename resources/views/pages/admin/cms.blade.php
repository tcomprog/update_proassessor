@extends('layouts.admin_app')



@section('content')

<style>
	.addpatientdata p{
		color:#000000;
	}
	.addpatientdata b{
		font-weight:500;
	}
	
</style>


<div class="container-fluid">

    <div class="row">

	    <div class="col-md-12">

			 <nav aria-label="breadcrumb">

			  <ol class="breadcrumb">

				<li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>

				<li class="breadcrumb-item active"><a href="#">CMS</a></li>

			  </ol>

			</nav>

		</div>
		 <div class="col-md-12">
		<div class="row justify-content-center">

         <div class="container-fluid">

			<div class="card">

				 <div class="card-header">

					CMS

				  </div>

		  <div class="card-body">

				<div class="row justify-content-center">

					<div class="col-md-12">                   

						<table   id="myTable" class="table table-bordered table-striped" style="width:100%">

							<tr>

							<td>S.No</td>
							
							 <td>Title</td>
							 <td>Action</td>

							

							</tr>

							 <?php $number = 1; ?>

							@if($pages->isNotEmpty())

							@foreach ($pages as $page)

							<tr>						

							<td>{{ $number }}</td>

							 <?php $number++; ?>
							 
							  <td>{{ ucfirst($page->page_title)  }}</td>

							  <td><a href="{{ url('admin/editpage') }}/{{$page->page_slug}}" target="_blank">Edit</a></td> 

							</tr>

							 @endforeach

							@else 

								<div>

									<h2>No Records found</h2>

								</div>

							@endif

						</table> 

					</div>

				</div>

			</div>

			</div>

        </div>

    </div> 
    </div> 

	</div>   


</div>
@endsection

