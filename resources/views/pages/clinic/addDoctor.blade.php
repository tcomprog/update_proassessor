@extends('layouts.clinic-layout') 
@section('content')	

<style>
  .eddddddd{
    color: #141A45;
  }
  .eddddddd:hover{
    cursor: pointer;
  }
</style>

<link href="{{ asset('css/doctor_profiles.css') }}" rel="stylesheet">
<link href="{{ asset('css/clinic_dashboard.css') }}" rel="stylesheet">
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>

<div class="container dashboardContainer mb-5 pb-4">
    <h3 class="inv-content-header">Add New Doctor</h3>

    <div class="row">

          <div class="container" class="addDoctorForm">
            
            <div id="success_msg" style="display: none;" class="alert alert-success" role="alert">
                A simple success alert—check it out!
              </div>
              <div id="error_msg" style="display: none;" class="alert alert-danger" role="alert">
                A simple danger alert—check it out!
              </div>

            <div class="row">

              <div class="col-md-6 col-sm-12 mt-3">
                <label class="clinicLabel">Name<sup class="requirdInput">*</sup></label>
                <input role="presentation" autocomplete="off" id="name1" name="name" type="text" class="form-control clinicInput" placeholder="Name">
                <span id="name_error" class="clinicErrorMsg">Error message</span> 
             </div>

            <script>
             $(document).ready(function(){
                $("#email1").on('input',function(){
                    if(validateEmail($(this).val()) == false){
                      $("#email_error").show();
                      $("#email_error").text("Invalid email address");
                    }	
                    else{
                        $("#email_error").hide();
                    }	

                    if(isEmpty($(this).val()))	{
                        $("#email_error").hide();
                    }		
                });
            });
            </script>


              <div class="col-md-6 col-sm-12 mt-3">
                <label class="clinicLabel">Business Address<sup class="requirdInput">*</sup></label>
                <input role="presentation" autocomplete="off" id="business_address1" name="business_address" type="text" class="form-control clinicInput" placeholder="Business Address">
                <span id="address_error" class="clinicErrorMsg">Error message</span>
            </div>


              {{-- <div class="col-md-6 col-sm-12 mt-3">
                <label class="clinicLabel">Phone number<sup class="requirdInput">*</sup></label>
                <input role="presentation" autocomplete="off" id="phone1" maxlength="10" name="phone" type="text" class="form-control clinicInput" placeholder="Phone number">
                <span id="phone_error" class="clinicErrorMsg">Error message</span>
              </div> --}}

              <script>
              $(document).ready(function(){
                $("#phone1").on('input',function(){
                   let pw = $(this).val();
                   if(digits_only(pw) == false){
                       $("#phone_error").show();
                       $("#phone_error").text("Invalid phone number");
                   }else{
                       $("#phone_error").hide();
                   }
               });
              });
            </script>

              <div class="col-md-6 col-sm-12 mt-3">
                <label class="clinicLabel">License Number<sup class="requirdInput">*</sup></label>
                <input role="presentation" autocomplete="off" id="license_no" type="text" class="form-control clinicInput" placeholder="License Number">
                <span id="license_no_error" class="clinicErrorMsg">Error message</span>
              </div>

              <div class="col-md-6 col-sm-12 mt-3">
                <div class="form-group">
                    <label class="clinicLabel">Choose Designation<sup class="requirdInput">*</sup></label>
                    <select id='designation_drop' class="form-control-file clinicInput pl-3 pr-3" >
                        <?php  foreach (MyHelper::desginationList() as $designation){ ?>
                        <option value="<?php echo $designation->id; ?>" ><?php echo $designation->designation; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>


            <div class="col-md-6 col-sm-12 mt-3">
                <div class="form-group">
                    <label class="clinicLabel">Choose City<sup class="requirdInput">*</sup></label>
                    <select id='city_drop' class="form-control-file clinicInput pl-3 pr-3" >
                        <?php  foreach (Helper::cities() as $city){ ?>
                        <option value="<?php echo $city->id; ?>" ><?php echo $city->city_name; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>


            <div class="col-md-12">
              <div>
                  <div class="demo">
                    <div class="control-group">
                    <label class="clinicLabel">Choose Category<sup class="requirdInput">*</sup></label>
                    <?php  $category = Helper::categorydetails();  ?>  
                    <select multiple class="form-control-file is-invalid demo-default" id="category_id" name="category_id[]" multiple   placeholder="Choose Category">
                    <?php //$categoryids= ($users->doctor) ? explode(',',$users->doctor->category_id) : [];  
                        foreach ($category as $categories){  ?>
                        <option value="<?php echo $categories->id; ?>"><?php echo ucfirst($categories->category_name); ?></option>
                      <?php  }?>
                    </select>
                    <span id="category_error" class="clinicErrorMsg">Error message</span>
                    </div>
                  </div>
              </div>
            </div> 

            <script>
              $(document).ready(function() {
                  $('#category_id').select2();
              });
            </script>

        <div class="col-md-5 col-sm-12 mt-3">
          <label for="license">Price<sup class="requirdInput">*</sup></label>
          <div class="range11 col-12 p-0">
            <div class="form-group range__slider col-12 p-0">
            <input type="range" step="50">
            </div>
            <div style="display: flex;justify-content: space-between;align-items: center;">
            <p id="priceDescription" class="m-0" style="font-size: 15px;text-align: center;flex: 1;">Non competitive</p>
            <div class="form-group range__value">
            <span></span>            
            </div>
            </div>
          </div>
          <input type="hidden" name="price" id="actualPriceDoctor" value="0">
          <span id="price_error" class="clinicErrorMsg">Error message</span>
        </div>


          <div class="col-12 d-flex mt-3" style="align-items: center;">
            <p class="price_tagDesi">What is your charge for assessment only?<sup class="requirdInput">*</sup></p>
            <input type="text" id="assessmentOnly" class="form-control-file currencyInput" maxlength="14"  value="0" placeholder="00.00">
            <span id="assessmentOnly_error" style="padding-left: 10px;" class="clinicErrorMsg">Error message</span>
          </div>

          <div class="col-12 d-flex mt-3" style="align-items: center;">
            <p class="price_tagDesi">What is your change for late cancellation?<sup class="requirdInput">*</sup></p>
            <input type="text" id="lateCancellationFee" name="lateCancellationFee" class="form-control-file currencyInput" maxlength="14" value="0" placeholder="00.00">
            <span id="lateCancellationFee_error" style="padding-left: 10px;" class="clinicErrorMsg">Error message</span>
          </div>

          <div class="col-md-12 mt-3">
            <div class="form-group">
              <label for="license">Introduction</label>
              <textarea class="form-control inputsss" style="max-height:120px;" name="introduction" id="introduction" placeholder="Introduction"></textarea>
            </div>
          </div>


          <div class="col-md-12 mt-3">
            <div class="form-group" style="display: flex; align-item:center; margin-bottom:12px;">
            <label for="profile">Are you open for Virtual Appointment ?<sup class="requirdInput">*</sup></label>
                              <div style="display: flex">
              <div class="form-check" >                           
                <input class="inputsss" style="height: auto !important; margin-left:10px;" name="virtual_appointment" type="radio" value="yes" >                               
                <label class="form-check-label" for="yes" >
                  Yes
                </label>
                 </div>

                <div class="form-check">
                <input checked='checked' class="inputsss" style="height: auto !important;" name="virtual_appointment" type="radio" value="no" >                            
                  <label class="form-check-label" for="no" >
                      No
                  </label> 
                </div>
              </div>
            </div>
          </div>


          <div class="col-md-12">
            <div class="form-group" style="display: flex; align-item:center; margin-bottom:12px;">
              <label for="profile">Do you charge HST?<sup class="requirdInput">*</sup></label>
              <div style="display: flex">
                  <div class="form-check">                           
                      <input name="adhst" class="inputsss" type="radio" value="yes" style="height: auto !important; margin-left:10px;" >                               
                      <label class="form-check-label" for="yes" >
                        Yes
                      </label>
                  </div>
                </li>
                  <div class="form-check" >
                  <input checked='checked' name="adhst" class="inputsss" type="radio" value="no"   style="height: auto !important;">                               
                      <label class="form-check-label" for="no" >
                        No
                      </label> 
                  </div>

              </div>
            </div>

            <div style="display: flex;">
              <span id="hst_number_error" class="clinicErrorMsg">Error message</span>
              <a id="hstEditnumber" style="display:none;padding-left:6px" class="eddddddd" data-toggle="modal" data-target="#addcl_hstNumberModel">Edit</a>
            </div>
           
          </div>

          <input type="hidden" id="hst_numberInp" value="null">


              <div class="col-12 mt-4">
                <a href="{{url('/clinic-dashboard')}}" class="btn btn-outline-danger" style="padding: 5px 35px;">Back</a>
                <button id="addNewDoctorBtn" class="btn btn-outline-success  ml-3" style="padding: 5px 30px;">Submit</button>
                <img id="loadingIMG" style="display:none" width="35" src="{{asset("img/loading.gif")}}" alt="">
              </div>

            </div>
          </form>  

    </div>
</div>

@endsection



{{-- -----------------  HST Price model --------------------- --}}
<div class="modal fade" id="addcl_hstNumberModel" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" style="color:#545C84 !important">HST Number</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">

			 <div id="hst_success" style="display: none" class="alert alert-success" role="alert">
				A simple success alert—check it out!
			  </div>
			  <div id="hst_error" style="display: none" class="alert alert-danger" role="alert">
				A simple danger alert—check it out!
			  </div>
			  
		 
			<div class="col-md-12">
				<div class="form-group">
					<label for="email">HST Number<sup class="requirdInput">*</sup></label>
					<input maxlength="15" type="text" class="form-control-file inputsss" id="cl_hst_number_inp" placeholder="HST Number">
				</div>
			</div>

		   <button class="btn btn-outline-success px-4" id="cln_changeHstValue" style="margin-left:17px">Save</button>
		</div>
	  </div>
	</div>
  </div>
  