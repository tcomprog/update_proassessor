<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\doctors;
use App\Models\lawyers;
use App\Models\AdminSettings;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\EmailController;
use App\Models\Pagecms;
use Mail;

class SignupController extends Controller
{

 function signupcreate_new(Request $request){
	
	$name = $request->input("name");
	$email = $request->input("email");
	$address = $request->input("address");
	$password = $request->input("password");
	$phone = $request->input("phone");
	$designation = $request->input("designation");
	$descField = $request->input("descField");
	$term_condition = $request->input("term_condition");

	$user = AdminSettings::where('settings_name', 'comission')->first();
	if($user!=null){
		$comission=$user->settings_value;
	}else{
		$comission='';
	}

	$rules=['email' => 'unique:users',
	'phone'=> 'required',
	'address'=>'required',
	];

	$validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
		$response['response'] = $validator->messages();
		//return $response;
		return response()->json(['status'=>'error', 'message'=>$validator->messages()]);					
	}
	else{
		//  **************** Doctor signup start ********************
		if($designation == '1') {
		    $email_verify_token = md5(time());
			$uniqueid = md5(uniqid());				
			$data1 = new User();
			$data1->name=$name;
			$data1->email= $email;
			$data1->unique_id=$uniqueid;
			$data1->password=Hash::make($password);
			$data1->phone= $phone;
			$data1->role_id=$designation;
			$data1->business_address=$address;
			$data1->remember_token=$request->_token;
			$data1->email_verify_token=$email_verify_token;
			$data1->desgination=$request->desginationDoctorID;
			
			$data1->save();

			$last_insertid=$data1->id;
			$emailtoken=$data1->email_verify_token;
			$regemail=$data1->email;
			$regname=$data1->name;

			if(isset($last_insertid)){			
				$data2 = new doctors();
				$data2->user_id=$last_insertid;
				$data2->unique_id=$uniqueid;
				$data2->licence_number=$descField;
				//$data2->credentials=$request->desginationDoctor;
				$data2->save();
				$last_insertid1=$data2->id;

				$user = User::where('id', $last_insertid)->first(); 

				$email_controller = new EmailController();					
				$email_controller->send_account_activation_mail_to_doctor($user);    
				
			      return response()->json(['status'=>'success', 'message'=>" Thank You Request Sent Successfully"]);
			}
	}
	//  **************** Doctor signup end ********************

	//  **************** Lawyer signup start ********************
	elseif($designation == '2'){
		$uniqueid = md5(uniqid());
		$page_slug="terms&conditions";
		$pdfHeader = '<div style="text-align: center;margin:0 auto"><img src="https://proassessors.com/img/logo.png"></div>';
		
		$pdfContent = Pagecms::where('page_slug',$page_slug)->first();  

		$pdfHeader .= '<div style="text-align: center;margin:0 auto"><h1>'.$pdfContent->page_title.'</h1></div>';

		$pdfContent = $pdfHeader.$pdfContent->content;

		$pdfContent .= '<h3>Signed by: '.$request->name.'</h3>';
		$pdfContent .= '<h3>Dated: '.date('d-M-Y').'</h3>';
		
		$upload_dir = public_path(); 
		$pdfFilename = $upload_dir.'/uploads/lawyer_agreement/'.$request->name.'_'.$uniqueid.'.pdf'; 
				$mpdf = new \Mpdf\Mpdf();
				$mpdf->WriteHTML($pdfContent);
				$mpdf->Output($pdfFilename,'F');

		$email_verify_token = md5(time());

	     	$data1 = new User();
			$data1->name=$name;
			$data1->email= $email;
			$data1->unique_id=$uniqueid;
			$data1->password=Hash::make($password);
			$data1->phone= $phone;
			$data1->role_id=$designation;
			$data1->business_address=$address;
			$data1->remember_token=$request->_token;
			$data1->email_verify_token=$email_verify_token;
			$data1->authentication_status='inactive';
			$data1->terms_agreement=$pdfFilename;

			$data1->save();
			$last_insertid=$data1->id;


			if(isset($last_insertid)){				         
				$data3 = new lawyers();
				$data3->user_id= $last_insertid;
				$data3->lawfirmname=$descField;
				$data3->save();

				$last_insertid2=$data3->id;
				$user = User::where('id', $last_insertid)->first(); 
			

				try{
					$email_controller = new EmailController();					
					$email_controller->send_account_activation_mail_to_lawyer($user); 
				}
				catch(Exception $ep){
					print_r($ep);
					exit ("Herere");
				}


				return response()->json(['status'=>'success', 'message'=>" Thank You Request Sent Successfully"]);
					
		}
	}
	else{
		// Clinic here if specific information is needed
		$uniqueid = md5(uniqid());
		$email_verify_token = md5(time());

		$data1 = new User();
		$data1->name=$name;
		$data1->email= $email;
		$data1->unique_id=$uniqueid;
		$data1->password=Hash::make($password);
		$data1->phone= $phone;
		$data1->role_id=$designation;
		$data1->business_address=$address;
		$data1->remember_token=$request->_token;
		$data1->email_verify_token=$email_verify_token;
		$data1->authentication_status='inactive';

		$data1->save();
		$last_insertid=$data1->id;

		$user = User::where('id', $last_insertid)->first(); 
		$email_controller = new EmailController();					
		$email_controller->send_account_activation_mail_to_lawyer($user); 
		return response()->json(['status'=>'success', 'message'=>" Thank You Request Sent Successfully"]);
	}
	//  **************** Lawyer signup end ********************

   }

	return $response;	
 }
   
/*doctor*/
    public function signupcreate(Request  $request)
    {	
    	$user = AdminSettings::where('settings_name', 'comission')->first();
		if($user!=null){
			$comission=$user->settings_value;
		}else{
			$comission='';
		}
	

		$rules=['email' => 'unique:users',
				'phone'=> 'required',
				'image' => 'mimes:png,jpg,jpeg|max:1024',
				'cvimage' => 'mimes:png,jpg,jpeg,doc,docx,pdf|max:1024',
				];
		$response = [];
		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()) {
			$response['response'] = $validator->messages();
			//return $response;
			return response()->json(['status'=>'error', 'message'=>$validator->messages()]);					
		}
		 else{			
				if($request->hasFile('image')) {
					$file = $request->file('image');

					$filename = time().$request['phone'].'.'.$file->getClientOriginalExtension();
					//$image['filePath'] = $filename;
					$file->move('uploads/profilepics', $filename);
					//return response()->json(['status'=>'success', 'file'=>$filename]);
				}else{
					$filename="";
				}
				if($request->hasFile('cvimage')) {
					$file1 = $request->file('cvimage');

					$cvfilename = time().$request['phone'].'.'.$file1->getClientOriginalExtension();
					//$image['filePath'] = $filename;
					$file1->move('uploads/cv', $cvfilename);
					//return response()->json(['status'=>'success', 'file'=>$filename]);
				}else{
					$cvfilename="";
				}
				if($request->category_id){
				$category_new=implode(',', $request->category_id);
				//print_r($category_new);exit;
			    }else{
			    	$category_new='';
			    }

				$email_verify_token = md5(time());
				$uniqueid = md5(uniqid());				
				$data1 = new User();
				$data1->name=$request->name;
				$data1->email= $request->email;
				$data1->unique_id=$uniqueid;
				$data1->password=Hash::make($request->password);
				$data1->phone= $request->phone;
				$data1->role_id=$request->role_id;
				$data1->business_address=$request->business_address;
				$data1->location=$request->location;
				$data1->profilepic=$filename;
				$data1->remember_token=$request->_token;
				$data1->email_verify_token=$email_verify_token;
				
			//print_r($data1);exit;
				$data1->save();
			//echo  $data1->id;exit;

			$last_insertid=$data1->id;
			$emailtoken=$data1->email_verify_token;
			$regemail=$data1->email;
			$regname=$data1->name;

			if(isset($last_insertid)){			

					$data2 = new doctors();
					$data2->user_id=$last_insertid;
					$data2->unique_id=$uniqueid;
					$data2->licence_number=$request->licencenumber;
					$data2->introduction=$request->introduction;
					$data2->category_id=$category_new;
					$data2->cvimage=$cvfilename;
					//$data2->credentials=$request->credentials;
					$data2->virtual_appointment=$request->virtual_appointment;
					$data2->hst=$request->hst;
					$data2->actual_price=$request->price;
					
					//print_r($data2);exit;
					$data2->save();

					$last_insertid1=$data2->id;

					$user = User::where('id', $last_insertid)->first(); 

					$email_controller = new EmailController();					
					$email_controller->send_account_activation_mail_to_doctor($user);    
					
				return response()->json(['status'=>'success', 'message'=>" Thank You Request Sent Successfully"]);
			}
			
		}
		return $response;		  
    }

/*lawyer*/
	public function lawyersignupcreate(Request  $request)
    {		
		$rules=['email' => 'unique:users',
				'phone'=> 'required',
				'image' => 'mimes:png,jpg,jpeg|max:1024',
				];
		$response = [];
		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()) {
			$response['response'] = $validator->messages();
			//return $response;
			return response()->json(['status'=>'error', 'message'=>$validator->messages()]);					
		}
		 else{			
				if($request->hasFile('image')) {
					$file = $request->file('image');

					$filename = time().$request['phone'].'.'.$file->getClientOriginalExtension();
					//$image['filePath'] = $filename;
					$file->move('uploads/profilepics', $filename);
					//return response()->json(['status'=>'success', 'file'=>$filename]);
				}else{
					$filename="";
				}
				

				$uniqueid = md5(uniqid());

				$page_slug="terms&conditions";
				$pdfHeader = '<div style="text-align: center;margin:0 auto"><img src="https://proassessors.com/img/logo.png"></div>';
				
				$pdfContent = Pagecms::where('page_slug',$page_slug)->first();  

				$pdfHeader .= '<div style="text-align: center;margin:0 auto"><h1>'.$pdfContent->page_title.'</h1></div>';

				$pdfContent = $pdfHeader.$pdfContent->content;

				$pdfContent .= '<h3>Signed by: '.$request->name.'</h3>';
				$pdfContent .= '<h3>Dated: '.date('d-M-Y').'</h3>';
				
				$upload_dir = public_path(); 

				$pdfFilename = $upload_dir.'/uploads/lawyer_agreement/'.$request->name.'_'.$uniqueid.'.pdf'; 
				$mpdf = new \Mpdf\Mpdf();
				$mpdf->WriteHTML($pdfContent);
				$mpdf->Output($pdfFilename,'F');

				//die;
				$email_verify_token = md5(time());
				
				//print_r( $uniqueid);exit;
				$data1 = new User();
				$data1->name=$request->name;
				$data1->email= $request->email;
				$data1->unique_id=$uniqueid;
				$data1->password=Hash::make($request->apassword);
				$data1->phone= $request->phone;
				$data1->role_id=$request->role_id;
				$data1->business_address=$request->business_address;
				$data1->location=$request->location;
				$data1->profilepic=$filename;
				$data1->remember_token=$request->_token;
				$data1->email_verify_token=$email_verify_token;
				$data1->authentication_status='inactive';
				$data1->terms_agreement=$pdfFilename;
				
			//print_r($data1);exit;
				$data1->save();
			//echo  $data1->id;exit;

			$last_insertid=$data1->id;
			$emailtoken=$data1->email_verify_token;
			$regemail=$data1->email;
			$regname=$data1->name;
			
			if(isset($last_insertid)){				         
					$data3 = new lawyers();
					$data3->user_id= $last_insertid;
					$data3->website_url=$request->websiteurl;				
					$data3->lawfirmname=$request->lawfirmname;
					$data3->save();

					$last_insertid2=$data3->id;
					$user = User::where('id', $last_insertid)->first(); 

					try{
						$email_controller = new EmailController();					
						$email_controller->send_account_activation_mail_to_lawyer($user); 
					}
					catch(Exception $ep){
						print_r($ep);
						exit ("Herere");
					}

					return response()->json(['status'=>'success', 'message'=>" Thank You Request Sent Successfully"]);
						
			}
				
		}
		return $response;		  
    }
}
