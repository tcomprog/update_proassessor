@extends('layouts.lawyer-custom')

@section('content')	
<link href="{{ asset('css/view_calenderar.css') }}" rel="stylesheet">

	 <!-- Banner Section -->

    <section class="intro_banner_sec inner_banner_sec" style="padding-top:25px; padding-bottom:25px;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-11">
                    <div class="welcome_text">
                        <h2>{{  ucfirst($doctordata->name) }} Calendar</h2>
							{{-- <h3 class="text-white"><?php
							// $categories = Helper::categorydetails();
							// $categoryids=explode(',',$doctordata->doctor->category_id);    
                            //         $categories_length = count($categoryids);                                
                            //         $i=1;
                            //         foreach ($categories as $category) { 
                            //             if($input_category_id){
                            //                 if($category->id == $input_category_id){
                            //                     echo ucfirst($category->category_name);
                            //                     break;
                            //                 }
                            //             }else{  
                            //                 if(in_array($category->id,$categoryids)) {
                            //                     echo ucfirst($category->category_name);
                            //                     echo $i < $categories_length ? ', ':''; 
                            //                     $i++; 
                            //                 }
                            //             }
                            //         }

							?></h3> --}}
                    </div>
                </div>
                <div class="col-md-1 text-right">
                    <button class="btn btn-primary" onClick="javascript:history.go(-1)" style="padding: 10px 30px;
					background: #141a45;
					border: none;
					font-size: 18px;
					line-height: 28px;">Back</button>

                </div>

            </div>

        </div>

    </section>

	

	 <!-- Lawyer Dashboard Section --> 

	<?php  

	//	print_r($assessment_days);	exit;
		//	print_r($holidays);
		//	exit;		
		$newlist1=json_encode($all);  
		//print_r($newlist1); exit;

	?>

	<input type="hidden" id="docidd" name="docidd" value="{{ request()->route('id') }}">

    <section class="dashboard_sec">

		

        <div class="container">

			

			<div class="row">

				<div class="col-md-12">

					<div class="search_sec">

						<div id="calendar"></div>

					</div>

				</div>

			</div>	

			

			<!--------- accept invitation------->

			<div class="modal fade" id="acptint" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">				

				  <div class="modal-dialog modal-dialog-centered" role="document" style="display: flex;justify-content: center;align-items: center;">

					<div class="modal-content login_box box11221" style="padding: 35px 25px;">

					  <div class="modal-header justify-content-center" style="border: none;">

						<h5 class="modal-title ntexte" id="loginTitle" style="text-align:center;">Confirm to proceed?</h5>
                        <br><br>
						{{-- <button type="button" class="close nclose" data-dismiss="modal" aria-label="Close">
							<span class="material-icons" style="color: #fff; font-size:23px;
							vertical-align: middle;">close</span>
						</button> --}}

					  </div>

					  <div class="modal-body" style="padding-top:0px;">

							  <form id="addevent_form">

									@csrf

									<div class="row">


										<div class="col-md-12">	
											<h5 class="modal-title ntexte" style="text-align: start !important;margin-bottom: 20px;">Appointment</h5>

											{{-- <p id="para_newappointment_type"><div class="ttttt">Appointment Type:</div><span class="norttttt" id="newappointment_type" style="margin-left: 20px;"></span></p> --}}
											<div><div class="ttttt">Date:</div><span class="norttttt" id="sselecteddate" style="margin-left: 20px;"></span></div>

											<div style="height:20px;"></div>

											<div><div class="ttttt">Time Slot:</div><span class="norttttt" id="newtslot" style="margin-left: 20px;"></span></div>

											<div style="height:20px;"></div>

											<div><div class="ttttt">Patient Name:</div><span class="norttttt" id="newppid" style="margin-left: 20px;"></span></div>
											
											<div style="height:20px;"></div>

											<div><div class="ttttt">Report Due Date:</div><span class="norttttt" id="dduedate" style="margin-left: 20px;"></span></div>
										</div>
									</div>
									<br>

									<div class="row mt-3">							

										<div class="col-md-12 mt-3 justify-content-center" style="display: flex;gap: 35px; margin-top:0px;">									

											<button type="button" class="yes_btn"  title="Yes"  id="yesbtn">
												Yes
											</button>

											<button type="button" class="yes_btn" style="background-color:#CC0A11;" title="No"  id="close_aler_model">
												No
											</button>

										</div>																		

									</div>

							</form>

					  </div>

					</div>

				  </div>

			</div>

			<!---------end send invitation------->

			

				

			<!---------send invitation------->

			<div class="modal fade" id="addevent" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">				

					<?php

							//$newList1=json_encode($newList);

					

					?>

				  <div class="modal-dialog modal-dialog-centered" role="document">

					<div class="modal-content login_box">

					  <div class="modal-header">

						<h5 class="modal-title" id="loginTitle">Send Invitation</h5>

						<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						  <span aria-hidden="true">&times;</span>

						</button>

					  </div>

					  <div class="modal-body">

							<form id="addevent_form"><!-------method="POST" action="{{ route('login') }}--------->

								@csrf

								<div class="row">

									<div class="col-md-12">

										<div class="form-group">

											<input type="hidden" id="selecteddate" name="selecteddate">

											<input type="hidden" id="docid" name="docid" value="{{ request()->route('id') }}">

											<input type="text" class="form-control" id="notes" name="notes" aria-describedby="notes" placeholder="Enter Message">

										</div>

									</div>

									<div class="col-md-12">

										<p id="successtask" style="color:green;display:none;">Event Added Successfully</p>

									</div>

								</div>

								<button type="submit" class="btn btn-primary submit_btn"  id="addevent_btn">Submit</button>

							</form>	

					  </div>

					</div>

				  </div>

				</div>

			<!---------end send invitation------->

        </div>

    </section>

    <!-- Lawyer Dashboard Section -->

	

@endsection