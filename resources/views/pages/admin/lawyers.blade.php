@extends('layouts.admin_app')
@section('content')

<div class="container-fluid bg-white rounded py-4 adminPageContainer">

    <div class="row">
     
        <h4 class="inv-content-header" style="margin-bottom:0px;">Lawyer List</h4>
        @if (count($users) > 0)
			<div class="mb-3">
				<a href="{{ url('/lawyers/all') }}" class="btn btn-outline-success btn-sm">All</a>
				<a href="{{ url('/lawyers/active') }}" class="btn btn-outline-success btn-sm">Approved</a>
				<a href="{{ url('/lawyers/inactive') }}" class="btn btn-outline-success btn-sm">Pending</a>
			</div>
		@endif

        <table id="datatbl11" class="row-border hover cell-border" style="width:100%">
            <thead>
                <tr>
					<th>S.No</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Auth status</th>
					<th>Date</th>
					<th>Type</th>
					<th >Action</th>
                </tr>
            </thead>

             <tbody>
                @foreach ($users as $ind => $row)
                @php
                   $str = (strlen($row->desc) > 110) ? substr($row->desc,0,100)."..."  : $row->desc;
                @endphp
                  <tr>
                    <td>{{$ind+1}}</td>
					<td width="15%">{{$row->name}}</td>
					<td width="15%">{{$row->email}}</td>
					<td width="10%">{{$row->phone}}</td>
					<td width="8%" style="font-weight: bold; @if($row->status=="active") {{"color:green;"}} @else {{"color:orange;"}} @endif">	 
						@if ($row->status=="active")
						    {{"Active"}}
						@else
						    {{"Pending"}}
						@endif
					</td>
					<td width="12%">{{date('d-m-Y', strtotime($row->created_at))}}</td>
					<td width="15%" id="typesRow_{{$row->id}}">
						@if($row->authentication_status != "active")
						 <select id="lawyerType_{{$row->id}}" class="form-select" aria-label="Default select example">
							@foreach ($types as $item)
						    	<option value="{{$item->id}}">{{$item->name}}</option>
							@endforeach
						  </select>
						  @else
						  @php
							echo isset($typeList[$row->lawyer_type]) ? $typeList[$row->lawyer_type] : '';
						  @endphp
						  @endif
					</td>
                    <td align="left">
						<a href ="{{url('/lawyer_file')}}/{{$row->id}}" class="btn btn-warning btn-sm" title="View profile">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>

						<a href ="{{url('/lawyer_agreement')}}/{{$row->id}}" class="btn btn-info btn-sm" title="Download Agreement">
                            <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>
                        </a>

						@if($row->authentication_status != "active")
						<button id="changeLawyerStatusBtn_{{$row->id}}" data-type="2" data-id="{{$row->id}}"  class="btn btn-success btn-sm changeLawyerStatusBtn" title="Approve">
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </button>
						<img id="loader_{{$row->id}}" width="25" style="display: none;" src="{{asset("img/loading.gif")}}" alt="">
                        @endif
                    </td>
                  </tr>
                @endforeach

                
            </tbody>

        </table>

    </div>
</div>

@endsection

