@extends('layouts.custom')
@section('content')	
<style>
.error{color:red !important;}
.blur{
	
	color: transparent;
	text-shadow: 0 0 4px #000;
}
</style>
	 <!-- Banner Section -->
    <section class="intro_banner_sec inner_banner_sec" style="padding-top:22px; padding-bottom:22px;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6"> 
                    <div class="welcome_text">
                        <h1>Search Doctors</h1>
                    </div>
                </div>
				<!--
                <div class="col-md-6 text-right">
                    <a href="#" class="search_btn">Search Doctors</a>
                </div>-->
            </div>
        </div>
    </section>
	
	 <!-- Lawyer Dashboard Section -->
	 
	 <?php   
			//$newlist1=json_encode($newList);
			//dd($newlist1)
	 ?>
    <section class="dashboard_sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="search_sec">
                            <div class="row">
                                <div class="col-md-12">
									
									<!--<button class="btn btn-primary" onclick="javascript:history.go(-1)">Back</button>-->
                                    <div class="search_box">
                                        <form method="GET" action="{{ route('search-doctor') }}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h2>Search a Doctor</h2>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="keyword" id="keyword" aria-describedby="name" placeholder="Name" value="<?php if(isset($_GET['keyword'])){echo $_GET['keyword'];}?>" >
                                                    </div>
                                                </div>                                              
                                                <div class="col-md-3">
                                                    <!-- <div class="form-group">
                                                        <input type="date" class="form-control" id="date" aria-describedby="date" placeholder="Date">
                                                    </div> -->
                                                    <div class="form-group">
                                                        <select class="form-control category_select" id="category_id" name="category_id">
                                                            <option value="">Category</option>
                                                            <option value="1" <?php if(isset($_GET['category_id'])){if($_GET['category_id']==1){echo "selected";}}?>>Psychologist</option>
                                                            <option value="2"<?php if(isset($_GET['category_id'])){if($_GET['category_id']==2){echo "selected";}}?>>Neurologist</option>
                                                            <option value="3" <?php if(isset($_GET['category_id'])){if($_GET['category_id']==3){echo "selected";}}?>>Orthopaedic</option>
                                                            <option value="4" <?php if(isset($_GET['category_id'])){if($_GET['category_id']==4){echo "selected";}}?>>Psychiatrist</option>
                                                            <option value="5" <?php if(isset($_GET['category_id'])){if($_GET['category_id']==5){echo "selected";}}?>>OT Assessment</option>
                                                        </select>
                                                    </div>
                                                </div>
												
												 <div class="col-md-3">
                                                    <div class="form-group new_form_group">
                                                        <!--<input type="text" class="form-control"  id="location" name="location" aria-describedby="location" placeholder="Location">-->
                                                            <div class="form-group">
                                                                <?php  $cities = Helper::cities();  ?>  
                                                                <select class="form-control @error('location') is-invalid @enderror category_select" id='location' name="location"   autofocus>
                                                                    <option value="">Choose City</option>
                                                                    <?php  foreach ($cities as $cities1){ ?>								    
                                                                    <option value="<?php echo $cities1->id; ?>"  
                                                                        <?php if(isset($_GET['location'])){
                                                                        if(($cities1->id)==($_GET['location'])){echo "selected";} } ?> > 
                                                                           <?php echo $cities1->city_name; ?></option> <?php } ?>   
                                                                </select>
                                                            </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="submit" class="btn btn-primary search_btn">Search</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
			
            <div class="row1">
                <div class="accordion" id="accordionExample">
                    <div class="row">
						 @if (!$users->isEmpty())
                        <?php   foreach($users as $user){    ?>
                        <div class="col-md-12">
                            <div class="card patient_tab box_div new_padding">
                                <div class="card-head" id="heading{{ $user->id }}">
                                    <h2 class="mb-0 docter_list" >
                                       <div class="main">
                                            <div class="sub">
                                                <?php $user1 = Helper::User();  // echo $user1->id;             
                                                if(($user->profilepic)!=null){ ?>
                                                      <img src="uploads/profilepics/doctor/<?php echo $user->profilepic; ?>" class="profile">
                                                <?php
                                                }else {
                                                ?> <img src="{{ asset('img/user.jpg') }}" class="profile">
                                                <?php  } ?>
                                            </div>
                                            <div class="sub">
                                               <span class="name">{{ $user->name }}</span>   
                                               <span class="category">
                                                    <?php  $category = Helper::categorydetails();  ?>  
                                                    <?php  foreach ($category as $categories){ ?>
                                                        <?php if(($categories->id)==($user->doctor->category_id)){echo $categories->category_name;} ?>
                                                    <?php } ?>
                                               </span>  
                                               <!--<span class="city">{{ $user->location }}</span>--->
											   <span  class="city">
                                                    <?php  $city = Helper::cities();  ?>  
                                                    <?php  foreach ($city as $city1){ ?>
                                                        <?php if(($city1->id)==($user->location)){echo $city1->city_name;} ?>
                                                    <?php } ?>
                                               </span>
                                            </div>
                                       </div>
                                       <a href="viewcalendar/{{ $user->doctor->user_id }}" class="nav-link addpatient_btn" >Invite</a>
								  </h2>
							      <a class="mb-0 collapsed collapsed-read-more" data-toggle="collapse" data-target="#collapse{{ $user->id }}" aria-expanded="false" aria-controls="collapse{{ $user->id }}">
                                    </a>
                                </div>
                                <div id="collapse{{ $user->id }}" class="collapse" aria-labelledby="heading{{ $user->id }}" data-parent="#accordionExample">
                                  <div class="card-body">
                                       <!-- <p><span class="img_bg"><img src="{{ asset('img/mail.svg') }}" class="mail"></span> <span>{{ $user->email }}</span></p>
                                        <p><span class="img_bg"><img src="{{ asset('img/telephone.svg') }}" class="telephone"></span> <span>{{ $user->phone }}</span></p>-->
                                        <p><span class="img_bg"><img src="{{ asset('img/address.svg') }}" class="address"></span> <span>{{ $user->business_address }}</span></p>
										
                                        <p style="margin: 0px 0px 5px;"><b>License Number:</b> 
										
										<?php if(isset($user->doctor->licence_number)){
											$str1=strlen($user->doctor->licence_number);
											$str2=$str1-4;											
											?>
										<span><b  class="blur"><?php echo substr($user->doctor->licence_number, 0, $str2);  ?></b><?php echo substr($user->doctor->licence_number, $str2); ?></span>
										<?php } ?>
										</p>
										
                                        <p style="margin: 0px 0px 5px;"><b>Credentials:</b> <span>{{ @$user->doctor->credentials }}</span></p>
                                        <p class="last"><b>Introduction:</b> <span>{{ $user->doctor->introduction }}</span></p>
                                  </div>
                                </div>
                            </div>
                        </div>
                        
                        <?php  }  ?>
						@else
                        <h4 style="text-align:center;">No Records Found</h4>
                        @endif</div>
                    </div>
                    {{$users->links("pagination::bootstrap-4")}}
                </div>
            </div>
			
			 <!-- Available Calendar Model -->
				<div class="modal fade" id="Availablecalendar" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true"> 
				  <div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content login_box">
					<p class="alert alert-success" role="alert" id="invitscss" style="display:none;">
						Invitation Sent Successfully
					 </p>
					  <div class="modal-header">
						
						<h5 class="modal-title" id="calendarTitle">Please Select Available Date</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
						  <div class="calendar">
								<div id="date-calendar"></div>
						  </div>
						  
						  <form id="invitesent">
								 <!--<input type="text" name="docid1" id="docid1">-->
							    <input type="hidden" name="docid" id="docid">
								<input type="hidden" name="seldate" id="seldate">
								<button href="#" class="invite_btn" id="invitesent_btn">Invite</button>
						  </form>
					  </div>
					</div>
				  </div>
				</div>
        </div>
    </section>
    <!-- Lawyer Dashboard Section -->
	
	
	
@endsection