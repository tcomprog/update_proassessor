<style>
select option:disabled {
    color: #fff;
    font-weight: 600;
	background-color: #ea9090;
}
</style>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" ></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" ></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

<!--added-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="{{ asset('js/lawyer-evo-calendar.min.js') }}"></script>
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- =======================================calender===================== -->
<script>
</script>
<!--id selection view calendar -->
<script>
	var selectedCatId  = "<?php echo $input_category_id ? : 0; ?>";

function getPatients(inputCategoryId)
	{
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
				url: "{{url('/patientdetails')}}",
				type: "GET",
				data: {doctor_category_id:inputCategoryId,doctor_user_id:'{{ $doctordata->id }}'}, 
				dataType:'json',
				success: function( response ) {
					if(response.status == 'success') {								
						var data=	response['patientdetails'];
						$('#patientid').empty();
						$('#patientid').append("<option value=''>Choose Patient</option>");
						$.each(data, function(i, item) {
							var isInvitationSent = data[i].is_already_sent;
							var isInvitationCancelled = data[i].is_cancelled;
							$('#patientid').append('<option  data-is-invitation-sent="'+isInvitationSent+'"  data-is-invitation-cancelled="'+isInvitationCancelled+'" value="' + data[i].id + '">' + data[i].name + '</option>');
						});
						if((response['virtual_appointment']['virtual_appointment'])=='yes'){
							document.getElementById("appointmentdiv").innerHTML = "";
							document.getElementById("appointmentdiv").innerHTML += 
							"<div class='row'><div class='col-md-3 text-right'><strong>Type</strong></div><div class='col-md-4'><div class='form-check'><label class='form-check-label' for='virtual'><input type='radio' class='form-check-input'  name='appointment_type' value='virtual' >Virtual</label></div></div><div class='col-md-4'><div class='form-check'><label class='form-check-label' for='inperson'><input type='radio' class='form-check-input ' name='appointment_type'  value='inperson' checked>In-Person</label></div></div></div>";
						}else{
							document.getElementById("appointmentdiv").style.visibility = "hidden";
							document.getElementById("para_newappointment_type").style.visibility = "hidden";
						}
						
					}
				}
		});	
	}

$(document).ready(function() {	
		jQuery.validator.addMethod("greaterThan", 
			function(value, element, params) {
		if (!/Invalid|NaN/.test(new Date(value))) {
			return new Date(value) >= new Date($(params).val());
		}
    	return isNaN(value) && isNaN($(params).val()) 
        || (Number(value) >= Number($(params).val())); 
		},'Report Due date must be greater than {0}.');


  let config = {
	 holidays: <?=$holidays?>,
	 assessmentdays:<?=$assessment_days?>,
	 inputCategoryId:'<?=$input_category_id ? : 0?>',
     todayHighlight:false,
 };

  $("#calendar").evoCalendar(config);	

  $("#calendar").evoCalendar('addCalendarEvent', 
  	<?php echo $newlist1; ?> 
  );

    var docidd=$('#docidd').val();
	var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
	var yyyy = today.getFullYear();

	today = mm + '/' + dd + '/' + yyyy;

    	let dates = new Date()
        let tomorrow = new Date(dates)
        tomorrow.setDate(tomorrow.getDate() + 1);
        dates = tomorrow.toLocaleDateString();

	timeslotdata(docidd,dates);
		
		function findValueInArray(value,arr){
				var result = "Doesn't exist";
				
				for(var i=0; i<arr.length; i++){
					var name = arr[i];
					if(name == value){
					result = 'Exist';
					break;
					}
				}

				return result;
		}
			///alert(docidd);
			$('#docid').val(docidd);		
			});

		<?php if(!$input_category_id || $input_category_id == null ){ ?>	

			$.ajaxSetup({
					headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
						url: "{{url('/ajax-get-doctor-categories')}}",
						type: "GET",
						data: {doctor_user_id:'{{ $doctordata->id }}'}, 
						dataType:'json',
						success: function( response ) {
						
							if(response.status == 'success') {								
								var categories=	response.categories;
								var checkboxString = '<label>Cateogry</label></br><div>';
								var select_new = `<select id='input_category_id_new' name="input_category_id_new" class="form-control mb-3 mt-3">`;
								select_new += `<option value="">Choose Category</option>`;
								$.each(categories, function(i, category) {
									if(i==0) {
										getPatients(category.id);
									}
									checkboxString += '<label for="'+'category-id'+i+'"><input '+(i==0 ? 'checked="checked"':'')+' id="'+'category-id'+i+'" type="radio" name="input_category_id" value="'+category.id+'" > '+category.category_name+' </label>';
									select_new += `<option value="${category.id}">${category.category_name}</option>`;
								});
								select_new += "</select>";

								//$('#doctor-categories-block').html(checkboxString);
								$('#doctor-categories-block').html(select_new);
								$("#input_category_id_new").change(function(){
								   var selectedID = $('#input_category_id_new :selected').val();
								   getPatients(selectedID);
								});
							}
						}
				});	

				// $('body').on('change', "input[name='input_category_id']", function() {
				// 		selectedCatId = $(this).val();
				// 		$('#patientid').html('<option value="">Choose Patient</option>');
				// 	getPatients(selectedCatId);
				// });	


				
			<?php }else{ ?>
				getPatients('{{ $input_category_id }}');
			<?php } ?>

</script>

<script>
	

	// initialize your calendar, once the page's DOM is ready
	$(document).ready(function() {	
		$('#patientid').change(function() {
			$('.doctor-patient-warning-alert').html('');
			if($(this).val() ==''){
				return false;
			}
				var option = $(this).find('option:selected');
				var isSent = option.attr('data-is-invitation-sent');
				var isCancelled = option.attr('data-is-invitation-cancelled');
				var msg = '<strong>Note:</strong> Already appointment sent to this Doctor for this patient <strong>'+option.text()+'</strong>, but the appointment went to expired';
				if(isSent == 'rejected'){
					var msg = '<strong>Note:</strong> This Doctor rejected your appointment for this patient (<strong>'+option.text()+'</strong>) previously.';
				}
				if(isSent != false && isSent != 'false') {
					$('.doctor-patient-warning-alert').html(msg).css('color','#ef9f27');
				}
				if(isCancelled=='cancelled'){
					$('.doctor-patient-warning-alert').html('');
					var msg = '<strong>Note:</strong> You have cancelled the appointment with this doctor for the Patient  (<strong>'+option.text()+'</strong>) previously.';
				}
				if(isCancelled != false && isCancelled != 'false') {
					$('.doctor-patient-warning-alert').html(msg).css('color','#ef9f27');
				}

		})


		function findValueInArray(value,arr){
		var result = "Doesn't exist";
		
		for(var i=0; i<arr.length; i++){
			var name = arr[i];
			if(name == value){
			result = 'Exist';
			break;
			}
		}
		return result;
		}
		
		$('#calendar').on('selectDate', function(event, newDate, oldDate) {	
			$("#addevent_form").trigger('reset');
			$('#selecteddate').val(newDate);
			$('#delteddate').val(newDate);
			
			/*doctor slots */
			var dociddd=$('#docidd').val();
			timeslotdata(dociddd,newDate);
			$("input[name=input_category_id][value=" + selectedCatId + "]").attr('checked', 'checked');
	
			//$('#editdate').val(test);
			document.getElementById('successtask').style.display = "none";
			document.getElementById('successtask1').style.display = "none";
			document.getElementById('edittask').style.display = "none";

		});
	
		$("#addevent_btn").click(function(){	
			document.getElementById('successtask').style.display = "none";
			document.getElementById('errortask').style.display = "none";

			$("#addevent_form").validate({
				rules: {
					           notes: {
								required: false
								},
								input_category_id_new: {
								required: true
								},
								timeslot: {
									required: true
								},
								patientid: {
									required: true
								},
								duedate: {
									required: true,
									greaterThan:'#selecteddate'
								},
				},
				messages: {
					notes: {
						required: "Please Enter Message"
					},
					timeslot: {
						required: "Please select timeslot"
					},
					patientid: {
						required: "Please Choose Patient"
					},
					duedate: {
						required: "Please Select Report Due Date",
						greaterThan:'Due date must me greater than Appointment date'
					},
					input_category_id_new: {
						required: "Please select category"
					}
				},
				//started				
				submitHandler: function (form) {
					//append data
					var tslot=$('#slotid').val();		
					var ppid=$('#patientid').children("option:selected").text();	
					var dduedate=$('#duedate').val();
					var sselecteddate=$('#selecteddate').val();
					var newappointment_type=$("input[type='radio'][name='appointment_type']:checked").val();
					
					//apend
					$('#newtslot').text(tslot);
					$('#newppid').text(ppid);
					$('#dduedate').text(dduedate);
					if(newappointment_type=='virtual'){
					$('#newappointment_type').text('Virtual');
					}else{
						$('#newappointment_type').text('In-Person');
					}
					if (!Date.parse(sselecteddate)) {
						var today = new Date();
						var dd = String(today.getDate()).padStart(2, '0');
						var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
						var yyyy = today.getFullYear();
						today = mm + '/' + dd + '/' + yyyy;						
						$('#sselecteddate').text(today);	
					} else{
					$('#sselecteddate').text(sselecteddate);	
					}
					$('#acptint').modal('show');
					
				}				
				})
			/**/			
		  }); 


		  $("#close_aler_model").click(function(){
			$('#acptint').modal('hide');
		  });

		  
		  /*add event*/
		  $("#yesbtn").click(function(){	
		//	console.log($('#addevent_form').serialize()); return		  
			$('#acptint').modal('hide');
			$("#addevent_btn"). attr("disabled", true);
			//var cateId = $("input[name='input_category_id']:checked").val();
			//console.log("category ID ="+cateId)	
			//$('#invitee_category_id').val(cateId);
			if($('#selecteddate').val() == ''){
				$('#selecteddate').val('{{ date('Y-m-d', strtotime(' +1 day')) }}');
			}
			// alert('hi');
			 $.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
				$.ajax({
				url: "{{url('/sendinvitation')}}",
				type: "POST",
				data: $('#addevent_form').serialize(),
				dataType:'json',
				beforeSend: function() {$('#addevent_btn').html('Please Wait');},
				success: function( response ) {
				$('#addevent_btn').html('Submit');	
				$("#addevent_btn"). attr("disabled", false);
					if(response.status == 'success'){						
						document.getElementById('successtask').style.display = "block"; 
						document.getElementById("addevent_form").reset();						
						 
						window.location.href=response.url;
							location.reload();
							setTimeout(function () {
								location.reload(true);
							  }, 20000);
					}else if(response.status == 'error'){
						if(response.message) {
							$.alert({
								title: 'Sorry!',
								content: response.message,
							});
						}else{
							document.getElementById('errortask').style.display = "block"; 
						}
					}	 				
				}
				});
			 
		  });
		  /*end event*/	
	
	/*Disable pastdates*/
		  
})
</script>

<script>
$(function() {
    $("#duedate" ).datepicker({ minDate: 1});
});

  function getDoctorCategories()
	{
		$.ajaxSetup({
					headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
						url: "{{url('/ajax-get-doctor-categories')}}",
						type: "GET",
						data: {doctor_user_id:'{{ $doctordata->id }}'}, 
						dataType:'json',
						success: function( response ) {
							if(response.status == 'success') {								
								var categories=	response.categories;
								var checkboxString = '<br> <label>Cateogry</label></br><div>';
									var select_new = `<select id='input_category_id_new' name="input_category_id_new" class="form-control mb-3">`;
								select_new += `<option value="">Choose Category</option>`;
								$.each(categories, function(i, category) {
									checkboxString += '<label for="'+'category-id'+i+'"><input id="'+'category-id'+i+'" type="radio" name="input_category_id" value="'+category.id+'" >'+category.category_name+'</label>';
									select_new += `<option value="${category.id}">${category.category_name}</option>`;
								});
								select_new += "</select>";
								//$('#doctor-categories-block').html(checkboxString);
								$('#doctor-categories-block').html(select_new);
							}
						}
				});	
	}

	function timeslotdata(dociddd,test)
	{ 
		$('#addevent_form').show();
		$('.event-list').show(); 
		$('#blockdatemsg').hide();
		if($('#selecteddate').val() == '') {
			$('#selecteddate').val(`{{ date('Y-m-d', strtotime(' +1 day')) }}`);
		}
		$.ajaxSetup({
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
			});
			$.ajax({
				url: "{{url('/doctorslots')}}",
				type: "POST",
				data: {id:dociddd, date:test},
				dataType:'json',
				success: function( response ) {
					var data1=	response['timeslot'];
					var doctorBlockedHoliday =	response['doctorBlockedHoliday'];
					if(doctorBlockedHoliday){
						$('#addevent_form').hide();
						$('.event-list').hide();
						$('#blockdatemsg').show(); 
					}
					$('#addevent_form #slotid').empty();
					$('#addevent_form #slotid').append('<option   value="" >Choose Time Slot</option> ');
					$.each(data1, function(i, item) {
						$('#addevent_form #slotid').append('<option  '+(item.is_booked == true ? 'disabled="disabled"' : "" )+' value="' + item.timeslot + '" >' + item.timeslot + '</option> ');
					})
				}
			});
	}





	

</script>
