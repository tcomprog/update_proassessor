<script src="{{ asset('js/app.js') }}" ></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" ></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="{{ asset('js/jquery-ui.multidatespicker.js') }}"></script>
<link rel="stylesheet" href="{{ asset('css/jquery-ui.multidatespicker.css') }}" />


<script>
$(document).ready(function() {	

//  ******************** Submit clinic profile form start *********************

$.validator.addMethod('filesize', function (value, element,param) {
   var size=element.files[0].size;
   size=size/1024;
   size=Math.round(size);
   return this.optional(element) || size <=param ;
   
 }, 'File size must be less than {0}');
 

$("#update-clinic-profile").validate({
        rules: {
            name: {
                required: true,
                maxlength:255,
            },
            business_address:{
                required:true,
            },
            phone:{
                required:true,
                maxlength:12,
            },
        },

      messages: {
            name: {
                required: "Please enter name",
                maxlength:"Name shouldn`t be greater then 255 character",
            },
            business_address:{
                required: "Please enter address",
            },
            phone:{
                required: "Please enter phone",
                maxlength:"Name shouldn`t be greater then 12 digits",
            },
    },
  
  submitHandler: function(form) {
   
    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });

    var form = $('#update-clinic-profile')[0];
    var formData = new FormData(form);
    
        $.ajax({
            url: "<?=url('clinic-profile-update') ?>",
            type: "POST",
            processData: false,
            contentType: false,
            data: formData,
            beforeSend: function() {
              $("#updateClinicProfile").hide();
              $("#loadingImg").show();
              $(".succes-msg").hide();
              $(".error-msg").hide();
            },
            success: function( response ) {
               if(response.status == "invalid"){
                  $(".succes-msg").hide();
                  $(".error-msg").show();
                  $("#updateClinicProfile").show();
                  $("#loadingImg").hide();
                  $(".error-msg").text(response.message?.image[0]);
                  setTimeout(() => {
                     $(".succes-msg").hide();
                     $(".error-msg").hide();
                  }, 3000);
               }
               else if(response.status == "success"){
                  $(".succes-msg").show();
                  $(".error-msg").hide();
                  $("#updateClinicProfile").show();
                  $("#loadingImg").hide();
                  $(".succes-msg").text(response.message);
                  setTimeout(() => {
                     $(".succes-msg").hide();
                     $(".error-msg").hide();
                     location.reload();
                  }, 2000);
               }
            }
        });
    }
    })
    //  ******************** Submit clinic profile form end   *********************


//===========================================================================================

    $.noConflict();
    $('#tableSorter').DataTable({
        "paging": true,
        "ordering":false
    });

});
</script>


{{-- Assessction Accept & Reject Code Start --}}
<script>
    $(document).ready(function() {	

        $(document).on('click', '.reject-inv-btn', function () {
           var Button = $(this);
           var invId = Button.attr("data-id");	

           var status = confirm("Are you sure want to Reject this Invitation?");
           if(status == true){
               Button.attr("disabled", true);
             
               $("#loading-gif-"+invId).show();	
               $("#cr-btn-accept-"+invId).css({'cssText': 'display: none !important'});	
               $("#cr-btn-reject-"+invId).css({'cssText': 'display: none !important'});	
                           
               $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   }
                 });
   
                 $.ajax({
                       type:'POST',
                       url:"<?=url('/rejectint') ?>",               
                       data :{id: invId},
                       dataType:'json',
                                       //beforeSend: function() {$('#int_reject').html('Please Wait');},
                   success:function(response) {
                       if(response.status== 'success'){					  
                           Button. attr("disabled", false);
                           $("#loading-gif-"+invId).hide();	
                            window.location.reload();

                           }else{
                               $("#cr-btn-accept-"+invId).css({'cssText': 'display: flex !important'});	
                               $("#cr-btn-reject-"+invId).css({'cssText': 'display: flex !important'});	
                               $("#loading-gif-"+invId).hide();	
                               alert('Sorry something went wrong')
                           }
                   }
               });	
           }
               
       });


       $('.accept-invitation-btn').click(function () {
           var Button = $(this);
           var status = confirm("Are you sure want to Accept this Invitation?"); 
           if(status == true){
               Button. attr("disabled", true);
		       var invId = Button.attr("data-id");	

               $("#loading-gif-"+invId).show();	
               $("#cr-btn-accept-"+invId).css({'cssText': 'display: none !important'});	
               $("#cr-btn-reject-"+invId).css({'cssText': 'display: none !important'});	
               
               $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });

                $.ajax({
				type:'POST',
				url:"<?=url('/acceptint') ?>",               
				data :{id: invId},
				dataType:'json',
                success:function(response) {
                    if(response.status== 'success'){					  
                        Button. attr("disabled", false);
                        window.location.reload();
                    }else{
                        $("#cr-btn-accept-"+invId).css({'cssText': 'display: flex !important'});	
                        $("#cr-btn-reject-"+invId).css({'cssText': 'display: flex !important'});	
                        $("#loading-gif-"+invId).hide();	
                        alert('Sorry something went wrong')
                    }
                }
			});

           }
       });
    });
</script>