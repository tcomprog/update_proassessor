
{{-- @if(@$typess != 'home') --}}
<script src="{{ asset('js/tavo-calendar.js') }}"></script>
<script src="{{ asset('js/app.js') }}" ></script>
{{-- @endif --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" ></script>
<!--added-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/evo-calendar@1.1.2/evo-calendar/js/evo-calendar.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/js/standalone/selectize.js"></script><!--select--> 

<!-- ===========================sign up======================================= -->    
<script>

window.addEventListener( "pageshow", function ( event ) {
  var historyTraversal = event.persisted || 
                         ( typeof window.performance != "undefined" && 
                              window.performance.navigation.type === 2 );
  if ( historyTraversal ) {
    // Handle page restore.
    window.location.reload();
  }
});

$(document).ready(function(){
    $('.lawyerdiv').show();
    $('.lawyerdiv').hide();
    $('.signup-input-user-type').change(function(){
        $('.modal-dialog-centered .alert').css('display','none');
        var inputValue = $(this).attr("value");
      //  alert(inputValue);
      if(inputValue==1){        
      	 $('.doctordiv').show();
         $('.lawyerdiv').hide();
      }
        if(inputValue==2){        
      	 $('.lawyerdiv').show();
         $('.doctordiv').hide();
      }
    });

});
</script>
<!-------doctor---------->
<script>    
    $.validator.addMethod('numericOnly', function (value) {
       return /^[0-9]+$/.test(value);
    }, 'Please enter only numeric values (0-9)');

    $("#signup_form").validate({  
            rules: {
                    name: {
                    required: true,
                    maxlength: 50
                    },
                    email: {
                    required: true,
                    maxlength: 50,
                    email: true,
                    },  
                    password:{
                            required:true,
                            minlength: 8
                        },
                    business_address:{
                                required:true
                            },  
                        location:{
                        required:true
                    },
                    phone: {
                            required: true,
							minlength: 8,
							maxlength: 16,
							number: true,
                        },
                    terms: {
                            required: true
                        },
                    location: {
                        required: true
                    },
                    category_id: {
                        required: true
                    },  
                    introduction: {
                        required: false
                    },  
                    licencenumber: {
                        required: true
                    },  
                    credentials: {
                        required: true
                    }, 
                    confirmpassword: {
                        required: true,
                        equalTo : '[name="password"]',
                    }, 
					cvimage: {
                        required: true
                    }, 
                    price: {
                        required: true,
                        number: true,
                        numericOnly:true
                    }, 
                },
                messages: {
                    name: {
                    required: "Please enter name",
                    maxlength: "Your name maxlength should be 50 characters long."
                    },
                    email: {
                    required: "Please enter valid email",
                    email: "Please enter valid email",
                    maxlength: "The email name should less than or equal to 50 characters",
                    },   
                    password:{
                                required:"Please Enter Password",
                                minlength: "Password must be at least 8 characters"
                            },  
                    business_address:{
                            required:"Please Enter  Address"
                        },
                    location:{
                        required:"Please Enter  location"
                    },
                    phone: {
                            required: "Please Enter Phone Number",
                            minlength: "Please Enter Minimum 8 Digits",
                            maxlength: "Please Enter Below 16 Digits Number",
							number:"Enter Phone Valid Number"
                        },
                    terms: {
                        required: "Please Check and Agree"
                    },
                    location: {
                        required: "Please Select City"
                    },
                    category_id: {
                        required: "Please Select Category"
                    },
                    introduction: {
                        required: "Please Enter Introduction"
                    },  
                    licencenumber: {
                        required: "Please Enter Licence Number"
                    }, 
                    credentials: {
                        required: "Please Enter Designation"
                    },
                    confirmpassword: {
                        required: "Please retype passwoord",
                        equalTo:"Please Enter same as password"
                    },
					cvimage: {
                        required: "Please Upload CV"
                    },
                    price: {
                        required: "Please Enter Price",
                        number: "PLease Enter Number"
                    },
                },
            submitHandler: function(form) {
                $("#signup_btn"). attr("disabled", true);
                $('#emailexists').text('');   
                $('#invalidformat').text(''); 
				 $('#cverror').text('');
                $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
           
            var formData = new FormData($('#signup_form')[0]);
          //  return false;
            
            $.ajax
            ({
                url: "{{url('signupcreate')}}",
                type: 'POST',              
               data: formData,
               dataType:'json',
               beforeSend: function() {$('#signup_btn').html('Please Wait');},
               processData: false,
               contentType: false,
               success: function( response ) {
                    $('#signup_btn').html('Submit');
                     if(response.status == 'error'){                            
                            $("#signup_btn"). attr("disabled", false);
                            //alert('hii');
                             console.log(response.message); 
							 if(response.message.cvimage){
								if (typeof response.message.cvimage[0] === "string" ){ 
									//alert('hii');
									var cverror1 = "Allowed Formats Jpg, Jpeg, Png, Pdf, Doc & Max Size 1Mb";
									$('#cverror').text(cverror1);
								}  
							 }
							 if(response.message.image){
								 if (typeof response.message.image[0] === "string" ){ 
									//alert('hii');
									var invalidformats = "Allowed Formats Jpg, Jpeg, Png & Max Size 1Mb";
									$('#invalidformat').text(invalidformats);
								}   
							 }	
							if(response.message.email){
								if (typeof response.message.email[0] === "string"){                               
									var emailexists1 = "Email Already Exists";
									$('#emailexists').text(emailexists1);
								 }                          
                            }  
							if(response.message.phone){
								if (typeof response.message.phone[0] === "string"){                               
									var doctorphoneerror1 = "Please Enter Phone Number";
									$('#doctorphoneerror').text(doctorphoneerror1);
								 }                          
                            } 
                    }
                    //console.log(response);
                    if(response.status == 'success'){
                        $("#signup_btn"). attr("disabled", false);
                        document.getElementById('lawyersuccesssignupform').style.display = "none";
                        document.getElementById('successsignupform').style.display = "block"; 
                        document.getElementById('signup-form-body').style.display = "none"; 
                        document.getElementById('close-model').style.display = "block";
                        document.getElementById("signup_form").reset(); 
                        $('body,html').animate({scrollTop:0},800);
                    }
                }/*,  
                error:function (response) {
                   console.log(response);
                }*/
            });
            }
        });
</script>

<!-------lawyer---------->
<script>    
    $("#lawyer_signup_form").validate({  
            rules: {
                    name: {
                    required: true,
                    maxlength: 50
                    },
                    email: {
                    required: true,
                    maxlength: 50,
                    email: true,
                    },  
                    apassword:{
                            required:true,
                            minlength: 8
                        },
                    business_address:{
                                required:true
                            },  
                        location:{
                        required:true
                    },
                    phone: {
                            required: true,
							minlength: 8,
							maxlength: 16,
							number: true
                        },
                    terms: {
                            required: true
                        },
                    location: {
                        required: true
                    },
					lawfirmname: {
                        required: true
                    },   
                      
                    websiteurl: {
                        required: false
                    },   
                    cpassword: {
                        required: true,
                        equalTo : '#apassword',
                    }, 
                },
                messages: {
                    name: {
                    required: "Please enter name",
                    maxlength: "Your name maxlength should be 50 characters long."
                    },
                    email: {
                    required: "Please enter valid email",
                    email: "Please enter valid email",
                    maxlength: "The email name should less than or equal to 50 characters",
                    },   
                    apassword:{
                                required:"Please Enter Password",
                                minlength: "Password must be at least 8 characters"
                            },  
                    business_address:{
                            required:"Please Enter  Address"
                        },
                    location:{
                        required:"Please Enter  location"
                    },
                    phone: {
                            required: "Please Enter Phone Number",
                            minlength: "Please Enter Minimum 8 Digits",
                            maxlength: "Please Enter Below 16 Digits Number",
							number:"Enter Valid Phone Number"
                        },
                    terms: {
                        required: "Please Check and Agree"
                    },
                    location: {
                        required: "Please Select City"
                    },   
					 lawfirmname: {
                        required: "Please Enter Law Firm Name"
                    }, 
                    websiteurl: {
                        required: "Please Enter Website Url"
                    }, 
                    cpassword: {
                        required: "Please retype passwoord",
                        equalTo:"Please Enter same as password"
                    },
                },
            submitHandler: function(form) {
                $("#lawyer_signup_btn"). attr("disabled", true);
                $('#lawyeremailexists').text('');   
                $('#lawyerinvalidformat').text('');   

                $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
           
            var formData = new FormData($('#lawyer_signup_form')[0]);
          //  return false;
            
            $.ajax
            ({
                url: "{{url('lawyersignupcreate')}}",
                type: 'POST',              
               data: formData,
               dataType:'json',
               beforeSend: function() {$('#lawyer_signup_btn').html('Please Wait');},
               processData: false,
               contentType: false,
               success: function( response ) {
                    $('#lawyer_signup_btn').html('Submit');
                     if(response.status == 'error'){                            
                            $("#lawyer_signup_btn"). attr("disabled", false);
                            //alert('hii');
							if(response.message.image){
								 if (typeof response.message.image[0] === "string" ){ 
									//alert('hii');
									var invalidformats = "Allowed Formats Jpg, Jpeg, Png & Max Size 1Mb";
									$('#lawyerinvalidformat').text(invalidformats);
								} 
							}
							if(response.message.email){
								if (typeof response.message.email[0] === "string"){                               
									var emailexists1 = "Email Already Exists";
									$('#lawyeremailexists').text(emailexists1);
								 }  
							}
							if(response.message.phone){
								if (typeof response.message.phone[0] === "string"){                               
									var lawyerphoneerror1 = "Please Enter Phone Number";
									$('#lawyerphoneerror').text(lawyerphoneerror1);
								 }  
							}
                                            
                    }
                    //console.log(response);
                    if(response.status == 'success'){
                        $("#lawyer_signup_btn"). attr("disabled", false);
                        document.getElementById('successsignupform').style.display = "none";
                        document.getElementById('lawyersuccesssignupform').style.display = "block"; 
                        document.getElementById('signup-form-body').style.display = "none"; 
                        document.getElementById('close-model').style.display = "block";
                        document.getElementById("lawyer_signup_form").reset(); 
                        $('body,html').animate({scrollTop:0},800);
                    }
                }/*,
                error:function (response) {
                   console.log(response);
                }*/
            });
            }
        });
        function displaySignupForm() {
            document.getElementById('successsignupform').style.display = "none";
            document.getElementById('lawyersuccesssignupform').style.display = "none"; 
            document.getElementById('signup-form-body').style.display = "block"; 
            document.getElementById('close-model').style.display = "none";
            document.getElementById("signup_form").reset(); 
            document.getElementById("lawyer_signup_form").reset(); 
        }


</script>




<!========================================login==============================================>
<script>

// $(document).ready(function(){
//     $("#loginotp").modal('show');
// })

function funcs_close(){
    clearInterval(mycounter);
}

function call_otp_api(){
clearInterval(mycounter);
$("#loginsubmit").attr("disabled", true);
$("#otploginsubmit").attr("disabled", true);
$("#otploginsubmit").text("Loading");
$("#otp_loader").show();

document.getElementById('admininactive').style.display = "none";
document.getElementById('loginfail').innerHTML = "";
document.getElementById('inactive').innerHTML = ""; 

$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

$.ajax({
url: "<?=url('user/login') ?>",
type: "POST",
data: $('#loginform').serialize(),
beforeSend: function() {$('#loginsubmit').html('Please Wait'); $("#login_loadingIMG").show(); },
success: function( response ) {

$('#loginsubmit').html('Submit');
$("#loginsubmit"). attr("disabled", false);
$("#login_loadingIMG").hide();

//console.log(response);

if(response.status == 'error'){ 
   document.getElementById('loginfail').innerHTML = "Invalid email or password!";   
}
else if(response.status == 'inactive'){
   document.getElementById('inactive').innerHTML = "Please check your email to authenticate your account.";  
}
else if(response.status == 'admininactive'){
   document.getElementById('admininactive').style.display = "block";
}
else if(response.status == 'opt_send'){
    $("#login").modal('hide');
    $("#otploginsubmit").attr("disabled", false);
    $("#otploginsubmit").text("Verify");
    $("#otp_loader").hide();
    $("#otpuniqueid").val(response.unc);   
    $("#loginotp").modal('show');
    $('#resendView').show();
    $("#ressendClick").show();
    $("#ressendClick").attr("onclick",`resendotp()`);

    $("#opt_counter").show();

  var minute = 1;
  var sec = 60;

  mycounter = setInterval(function() {
    // if(minute == 1)
    // $("#opt_counter").text(ressendClickminute + " : " + sec);
    // else
    $("#opt_counter").text(sec);
    
    sec--;
    // if (sec == 0 && minute == 1) {
    //     minute = 0;
    //     sec = 60;
    // }
    
    //if(sec == 0 && minute == 0){
    if(sec == 0){
        clearInterval(mycounter);
        $("#opt_counter").hide();
        $("#resendView").show();
      //  call_otp_api();
    }
    
  }, 1000);


}
else if(response.status == 'login_success'){
    location.replace(response.path);
}

}
}); 
}




var mycounter;

$(document).ready(function() {
if ($("#loginform").length > 0) {
$("#loginform").validate({
rules: {
	role_id: {
                    required: true
                },
    email: {
                    required: true
                },
    password:{
            required:true,
            minlength: 8
        },
    terms: {
                        required: true
                    },
},
messages: {
	role_id: {
                required: "Please Choose One Option"
            },
    email: {
    required: "Please enter valid email",
    email: "Please enter valid email",
    maxlength: "The email name should less than or equal to 50 characters",
    },   
    password:{
                required:"Please Enter Password"
            },  
    terms:{
            required:"Please Check and agree"
    },
},
submitHandler: function(form) {
   //here
call_otp_api();

}
})
}
});

</script>

<!============================forgot password=========================================>
<script>
$(document).ready(function() {
	 document.getElementById('emailsuccess').style.display = "none";   
	 document.getElementById('emailfail').style.display = "none";   
if ($("#resetpwdform").length > 0) {
	$("#resetpwdform").validate({
        rules: {
            email: {
                    required: true
                },
           },
messages: {
    email: {
    required: "Please enter valid email",
    email: "Please enter valid email",
    maxlength: "The email name should less than or equal to 50 characters",
    },
},
submitHandler: function(form) {
$("#resetpwd"). attr("disabled", true);
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

$.ajax({
url: "<?=url('user/resetpwd') ?>",
type: "POST",
data: $('#resetpwdform').serialize(),
beforeSend: function() {
    $("#resetpwd"). attr("disabled", true);
    $('#resetpwd').html('Please Wait');
    $('#forgotpasswordloading').show();
},
success: function( response ) {
$('#resetpwd');
$('#forgotpasswordloading').hide();
if(response.status == 'success'){
   // alert(response.error);
   document.getElementById('emailfail').style.display = "none";
   document.getElementById('emailsuccess').style.display = "block";  
	$("#resetpwd"). attr("disabled", false);
	document.getElementById("resetpwdform").reset(); 
    $('#resetpwd').html('Reset Password');
}
if(response.status == 'error'){
   // alert(response.error);
   document.getElementById('emailsuccess').style.display = "none"; 
   document.getElementById('emailfail').style.display = "block";   
   $("#resetpwd"). attr("disabled", false);
}
}
});
}
})
}
});
</script>


<!================================new password==============================>
<script>
    $("#pwdsetbtn").on('click',function(){
        $("#password_error").hide();
        $("#repassword_error").hide();
        $("#forgotpassloading").hide();
        $("#pwdsuccess").hide();
        $("#pwdfail").hide();
        let error = 0;

        let p1 = $("#new_password").val();
        let p2 = $("#confirm_password").val();

    if(isEmpty(p1)){
       $("#password_error").show();
       $("#password_error").text("Password is required");
       error = 1;
      setTimeout(() => {
        $("#password_error").hide();
      }, 4000);
    }
    else{
       var check = checkPasswordValidity(p1);
       if(check == null){
          $("#password_error").hide();
       }
       else{
        $("#password_error").show();
        $("#password_error").text(check);
        error = 1;
       }
    }

    if(isEmpty(p2)){
      $("#repassword_error").show();
      $("#repassword_error").text("Re-password is required");
      error = 1;
      setTimeout(() => {
        $("#repassword_error").hide();
       }, 4000);
    }
    else{
      if(p1 != p2){
        $("#repassword_error").show();
        $("#repassword_error").text("Passwords don`t match");
        error = 1;
      }
      else{
          $("#repassword_error").hide();
      }
    }

    if(error == 0){
       $("#forgotpassloading").show();
       $("#pwdsetbtn").hide();

       $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
       });

       $.ajax({
            url: "<?=url('user/pwdset') ?>",
            type: "GET",
            data: {new_password:p1,confirm_password:p2,pwdreset_token:$("#pwdreset_token").val()},
            beforeSend: function() {$('#resetpwd').html('Please Wait');},
            success: function( response ) {
                if(response.status == 'success'){
                    $("#forgotpassloading").hide();
                    $("#pwdsetbtn").show();
                    document.getElementById('pwdfail').style.display = "none"; 
                    document.getElementById('pwdsuccess').style.display = "block"; 
                    window.location.href=response.url;
                    setTimeout(function () {
                        location.reload(true);
                    }, 15000); 
                }
                else if(response.status == 'error'){
                    $("#forgotpassloading").hide();
                    $("#pwdsetbtn").show();
                    document.getElementById('pwdsuccess').style.display = "none"; 
                    document.getElementById('pwdfail').style.display = "block";  
                }
        }
        });
    }

});


// $(document).ready(function() {
// 	 document.getElementById('pwdsuccess').style.display = "none"; 
// 	 document.getElementById('pwdfail').style.display = "none";  
// if ($("#pwdset").length > 0) {
// 	$("#pwdset").validate({
// rules: {
//     new_password: {
//                     required: true
//                 },
// 	 confirm_password: {
//                     required: true,
// 					equalTo: "#new_password"
//                 },
// },
// messages: {
//     new_password: {
//     required: "Please enter Password",
//     },
// 	confirm_password: {
//     required: "Please enter New Password",
//     },
// },
// submitHandler: function(form) {
// $("#pwdsetbtn"). attr("disabled", true);
// $.ajaxSetup({
// headers: {
// 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
// }
// });

// $.ajax({
// url: "<?=url('user/pwdset') ?>",
// type: "GET",
// data: $('#pwdset').serialize(),
// beforeSend: function() {$('#resetpwd').html('Please Wait');},
// success: function( response ) {

// $('#pwdsetbtn');
// $("#pwdsetbtn"). attr("disabled", false);
// console.log(response.status);

// if(response.status == 'success'){
//    // alert(response.error);
//   // document.getElementById('pwdsuccess').innerHTML = "Password Changed Successfully";  
//   document.getElementById('pwdfail').style.display = "none"; 
//    document.getElementById('pwdsuccess').style.display = "block"; 
//    window.location.href=response.url;
	
// 	setTimeout(function () {
// 		location.reload(true);
// 	  }, 15000); 
// }

// if(response.status == 'error'){
//    // alert(response.error);
//    document.getElementById('pwdsuccess').style.display = "none"; 
//    document.getElementById('pwdfail').style.display = "block";  
// }/*
// if(response.status == 'success'){
//     window.location.href=response.url;
// }*/

// }
// });
// }
// })

// }
// });
</script>
<script>

//*********************** Contact us section start ********************************
$("#invitebtn1").click(function(){
   $("#ctr_fname").hide();  $("#ctr_lname").hide();  $("#ctr_email").hide();
   $("#ctr_phone").hide();  $("#ctr_subject").hide();  $("#ctr_message").hide();

   var fname = $("#fcname").val();
   var lname = $("#lastname").val();
   var subject = $("#cssubject").val();
   var message = $("#csmessage").val();
   var email = $("#emailaddresscs").val();
   var phone = $("#phonecs").val();

   var error_status = false;
   if(isEmpty(fname)) { $("#ctr_fname").show(); $("#ctr_fname").text("Please enter first name"); error_status = true; }
   if(isEmpty(fname)) { $("#ctr_lname").show(); $("#ctr_lname").text("Please enter last name"); error_status = true; }
   if(isEmpty(subject)) { $("#ctr_subject").show(); $("#ctr_subject").text("Please enter subject"); error_status = true; }
   if(isEmpty(message)) { $("#ctr_message").show(); $("#ctr_message").text("Please enter message"); error_status = true; }

   if(isEmpty(email)) {
        $("#ctr_email").show(); 
        $("#ctr_email").text("Please enter email"); 
        error_status = true;
   }else{
        if(validateEmail(email) == false)
        {
          $("#ctr_email").show(); 
          $("#ctr_email").text("Invalid email"); 
          error_status = true;
        }
   }

   if(isEmpty(phone)) {
        $("#ctr_phone").show(); 
        $("#ctr_phone").text("Please enter phone number"); 
        error_status = true;
   }else{
       if(phone.length < 8 || phone.length > 16){
         $("#ctr_phone").show(); 
         $("#ctr_phone").text("Length must be between 8 to 16 digits"); 
         error_status = true;
       }

       if(phone.match(/^[0-9]+$/) == null){
         $("#ctr_phone").show(); 
         $("#ctr_phone").text("Phone number should only contain digits"); 
         error_status = true;
       }
   }

   if(!error_status){
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

            $.ajax({
            url: "<?=url('assessment') ?>",
            type: "POST",
            data: $('#assessment_form').serialize(),
            beforeSend: function() {
                $("#invitebtn1" ).prop("disabled", true);
                $('#invitebtn1').html('Please Wait');
            },
            success: function( response ) {
            $('#invitebtn1').html('Submit');
            $("#invitebtn1"). attr("disabled", false);
            if(response.status == 'success'){
                document.getElementById('successform').style.display = "block";
                    $("#successform").fadeOut(5000);
                document.getElementById("assessment_form").reset(); 
            }
            if(response.status == 'error'){
                if(response.message.phone){
                    if (typeof response.message.phone[0] === "string"){                               
                            var assessmentphoneerror1 = "Please Enter Phone Number";
                            $('#assessmentphoneerror').text(assessmentphoneerror1);
                        }                          
                    } 
                } 
            }
            });
   }
   else{
    return;
   }


});
//*********************** Contact us section end   ********************************

/*------------------------------------send assessment home page-------------------------*/
$(document).ready(function() {


/*----------------------login/signup  button sho hide-------------------*/
$('#signup-login-link').click(function(){
	$("#newsignup").modal('hide');
	$(".modal-backdrop.fade.show").remove();
    document.getElementById('resendotpsuccess').innerHTML = "";  
	$("#login").modal('show');   
})
$('#login-signup-link').click(function(){
	$("#login").modal('hide');
	$(".modal-backdrop.fade.show").remove();
	$("#newsignup").modal('show');
})
$('#fwd-login-link').click(function(){
	//alert('hi');
	$("#forgot").modal('hide');
	$(".modal-backdrop.fade.show").remove();
	/*$("#signup").modal('show');*/
})
});
</script>


<!-------------------------------contact ---------------------------->
<script>
$(function() {
	$('a[href*=\\#]:not([href=\\#])').on('click', function() {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.substr(1) +']');
		if (target.length) {
			$('html,body').animate({
			  scrollTop: (target.offset().top - 65)
			}, 100);
			return false;
		}
	});
});


$(document).ready(function(){
  var currentURL = $(location).attr('href');
  if(currentURL.indexOf('#contact') != -1){
    	$('html,body').animate({
			  scrollTop: (target.offset().top - 65)
			}, 100);

		}
})
</script>

<!---otp checking---->

<script>

function submitOTP(){
       $("#invalidotp").html('');
       var digit_1 = $("#digit-1").val();
       var digit_2 = $("#digit-2").val();
       var digit_3 = $("#digit-3").val();
       var digit_4 = $("#digit-4").val();
       var digit_5 = $("#digit-5").val();
       var digit_6 = $("#digit-6").val();

       var otp = digit_1+digit_2+digit_3+digit_4+digit_5+digit_6;
       console.log(otp)

       if(!isEmpty(otp) && otp.length >= 5){
        
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        $.ajax({
            url: "<?=url('user/otplogin') ?>",
            type: "POST",
            data: $('#otploginform').serialize(),
           beforeSend: function() {
             $('#otploginsubmit').hide();
             $("#otp_loader").show();  
             clearInterval(mycounter);
            },
           success: function( response ) {
            $("#opt_counter").hide();
            $("#resendView").show();
            $('#otploginsubmit').show()
            $("#otp_loader").hide();

            if(response.status == 'login_success'){
              $('#otploginsubmit').hide();
              $('#resendView').hide();
              window.location.replace(response.path);
            }

            if(response.status == 'error'){
                document.getElementById('invalidotp').innerHTML = "Please enter valid verification code";  
            }

            if(response.status == 'unkown'){
                document.getElementById('invalidotp').innerHTML = "Something weng wrong try again";  
            }

            if(response.status == 'invalid_otp'){
              document.getElementById('invalidotp').innerHTML = "Please Enter Valid Verification Code";  
            }

            if(response.status == 'success'){
                setTimeout('location.reload()', 1000) 
                window.location.href=response.url;
            }
        }
        });

       }
       else{
         $("#invalidotp").show();
         $("#resendView").show();
         $("#otploginsubmit").attr("disabled", false);
         $("#invalidotp").html("Enter complete otp");
         $("#opt_counter").html("00");
         clearInterval(mycounter);
       }
}


$(document).ready(function() {
    $("#otploginform").submit(function(e){
       e.preventDefault();
       submitOTP();
    });
});

/*resend otp */
function resendotp(unc)
{	
clearInterval(mycounter); 
$("#opt_counter").hide();
$("#opt_counter").text("00");
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

    var uniqueid = $('#otpuniqueid').val();
    document.getElementById('resendotpsuccess').innerHTML = "";  
    $("#otp_loader").show();
    $.ajax({
       url: "<?=url('resendotp') ?>",
       type: 'POST',
       dataType: 'json',
       data:{
          uniqueid:uniqueid
       },
       success: function( response ) {
        $("#opt_counter").show();
        $("#otp_loader").hide();
            if(response.status == 'resendotpsuccess'){    
                $("#opt_counter").show();
              //  $("#resendView").hide();
                var minute = 1;
                var sec = 60;
                mycounter = setInterval(function() {
                    // if(minute == 1)
                    // $("#opt_counter").text(minute + " : " + sec);
                    // else
                    $("#opt_counter").text(sec);
                    
                    sec--;
                    // if (sec == 0 && minute == 1) {
                    //     minute = 0;
                    //     sec = 60;
                    // }
                    
                    if(sec == 0){
                        clearInterval(mycounter);
                        $("#opt_counter").hide();
                        $("#resendView").show();
                    }
                    
                }, 1000);            
             //   document.getElementById('resendotpsuccess').innerHTML = "Verification Code sent again";
                
            }
       }

   })
  }

</script>

<script>
    $(document).ready(function() {
        $('#category_id').selectize();
    });
 </script>





