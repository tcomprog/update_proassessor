<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\EmailController;
use App\Models\User;
use App\Helper;
use App\Models\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\user_invitations;

class ChatController extends Controller
{  
    public function lawyerChat($invitationID){
       
        $invitation = user_invitations::where('unique_id',$invitationID)->first();
        Helper::clear_un_read_messages(Auth::id(),$invitation->id);
        if(!$invitation){
            return redirect('lawyer-dashboard');
        }
        
        return view('/pages/lawyer/lawyer_chat',compact('invitation'));
    }

    public function load_messages(Request $request)
    {
        $messages = Message::whereHas('invitation',function($q) use($request){
            $q->where('unique_id',$request->invitation_id);   
        })->orderBy('id','ASC')->get();

        $output = '';
        if($messages) {
            foreach($messages as $message) {
                if($message->sender_user_id == Auth::id()) {
                    $messageString = '<div class="outgoing_msg">
                    <div class="incoming_msg_img"> <img src="'.url('img/user.jpg').'" alt="user"> </div>
                    <div class="sent_msg">
                        <p>'.$message->message.'</p>
                        <span class="time_date">'.date('h:i A | M y',strtotime($message->message_time)).'</span> </div>
                    </div>';
                }else{
                    $messageString = '<div class="incoming_msg"><div class="incoming_msg_img"> <img src="'.url('img/user.jpg').'" alt="user"> </div>
                    <div class="received_msg">
                        <div class="received_withd_msg">
                        <p>'.$message->message.'</p>
                        <span class="time_date">'.date('h:i A | M y',strtotime($message->message_time)).'</span></div>
                    </div>
                    </div>';
                }
                $output .= $messageString;
            }
        }
        return ['data'=>$output,'status'=>'success'];
    }

    public function store_message(Request $request)
    {
        $invitation = user_invitations::where('unique_id',$request->invitation_id)->first();
        $user = Auth::user();
        $receiver_id = $user->role->role == 'lawyer'? $invitation->invitee_user_id : $invitation->inviter_user_id;   
        $messageData = [
            'sender_user_id'=>$user->id,
            'message'=>$request->message,
            'message_time'=>date('Y-m-d H:i:s'),
            'invitation_id'=>$invitation->id,
            'receiver_user_id'=>$receiver_id,
            'is_read'=>$request->is_read=='true'?'1':'0',
        ];
        $oldUnRead = Message::where('receiver_user_id',$receiver_id)->where('invitation_id',$invitation->id)->get()->count(); 
        $message = Message::create($messageData);
        if($message && $request->is_read == 'false' && $oldUnRead) {
            $email_ctrl = new EmailController();
            $partner = User::find($receiver_id);
            if($partner) {
                $email_ctrl->send_unread_message_notification($partner,$user);
            }
        }
        return ['data'=>$message->id,'status'=>'success'];
    }

}