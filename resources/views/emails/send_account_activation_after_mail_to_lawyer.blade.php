@extends("includes.email_container")
@section("content")

<div>

        <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Dear {{ ucfirst($user->name) }},</h3>

        <p style="font-size: 14px;
        line-height: 17px;">Your email has been authenticated successfully. You will be able to login as soon as Admin approves your registration.</p> 
        
        <br/>    

        <br/>

        <br/>

        <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>


        <p style="font-size: 14px;
        line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 

</div>  

@endsection