@extends('layouts.admin_app')
@section('content')

<div class="container-fluid bg-white rounded py-4 adminPageContainer">

    <div class="row">

        <h4 class="inv-content-header" style="margin-bottom:0px;">Customer Emails</h4>

        <table id="datatbl11" class="row-border hover cell-border my-5" style="width:100%">
            <thead>
                <tr>
                    <th>Date</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Subject</th>
					<th>Message</th>
					<th>Action</th>
                </tr>
            </thead>
            <tbody>
               @foreach ($customer_email as $row)
                   <tr>
                      <td>{{date("d-m-Y",strtotime($row->created_at))}}</td>
                      <td>{{ucfirst($row->f_name)}}  {{ucfirst($row->l_name)}}</td>
                      <td>{{$row->email}}</td>
                      <td>{{$row->phone}}</td>
                      <td>{{$row->subject}}</td>
                      <td>{{$row->message}}</td>
                      <td>
                         <a href="{{url('delete_contact_email/')."/".$row->id}}" title="Delete">
                            <span class="material-icons text-danger" style="font-size:24px; text-align:center; line-height:60px;">delete</span>
                         </a>
                      </td>
                   </tr>
               @endforeach
            </tbody>
        </table>


    </div>
</div>

@endsection

