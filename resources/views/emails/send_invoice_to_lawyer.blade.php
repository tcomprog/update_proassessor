
@extends("includes.email_container")
@section("content")
            <div>

                 <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Hello Admin,</h3>

                    <p style="font-size: 14px;line-height: 17px;">This mail is notifying about  {{ ucfirst($invitation->user1->name) }} has sent invoice to you.</p>                    
                    

                    <div style="text-align: center; width:100%; margin:20px 0px;"> <a style="background: linear-gradient(254.35deg, #515C84 39.06%, rgba(81, 92, 132, 0.42) 60.46%);
                        border-radius: 20px;
                        padding: 9px 30px;
                        color: white;
                        text-decoration: none;
                        font-size: 17;
                        font-weight: bold;" 
                         href="{{url('/uploads/custom_invoice')}}/{{$invitation->invoices->invoice_filename}}"  download target='_blank'>Download Invoice</a>
                    </div> 

                    <br/>

                    <br/>

                    <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
                    <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 

            </div>  

               </div>

            </div>

  @endsection