@extends('layouts.doctor-custom')
@section('content')	
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="{{ asset('css/doctor_dashboard.css') }}" rel="stylesheet">

<style>
	.ui-datepicker-trigger{
		background: transparent !important;
	}
	.n_links{
		font-weight: bold;
		color:#00396b;
	}
	.n_links:hover{
		text-decoration: none;
	}
</style>

	 <!-- Banner Section -->
    <section class="intro_banner_sec inner_banner_sec" style="padding-top:25px; padding-bottom:25px;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="welcome_text">
                        <h1>Doctor's  Dashboard <img src="{{ asset('img/doctor_icon.png') }}" alt="Doctor's Dashboard" class="dashboard_icon"></h1>
                    </div>
                </div>
              <!--  <div class="col-md-6 text-right">
                    <a href="#" class="search_btn">View Invitations</a>
                </div>-->
            </div>
        </div>
    </section>
	
	<!-- Lawyer Dashboard Section -->
    <section class="dashboard_sec">

        <div class="container">
			 @if($ac_status == 0)
			  <div class="alert alert-danger" role="alert">
				 Please complete your profile in order access dashboard!
				 <a href="{{route('doctor-profile')}}">Click here</a>
			  </div>
			  @endif
			  
			<?php 
			$routeName = \Request::route()->getName(); 
			$archivedinvitations = Request::segment(2);
			?>

			<script>
				// $("#mainDiv").on('click',function(e){
				// 	alert("hi")
				// })
			</script>

          <div id="mainDiv"  @if($ac_status == 0) style="pointer-events:none; display:none;" @endif>

            <nav>
                <div class="nav nav-tabs" id="dashboard-tabs" role="tablist">
					<!--<a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile"    aria-selected="false">My Profile</a>-->
                    <a class="nav-item nav-link @if($activeTab == null || $activeTab == 'my-invitations' || $routeName == 'search-confirmed-patient' || $archivedinvitations == 'archived-invitations') active @endif" id="my-invitations-tab" data-toggle="tab" href="#my-invitations" role="tab" aria-controls="my-invitations"  aria-selected="false" style="font-weight: 600;font-size: 21px;">Assessments</a>
                    <a class="nav-item nav-link @if($activeTab == 'update-calendar') active @endif" id="update-calendar-tab" data-toggle="tab" href="#update-calendar" role="tab" aria-controls="update-calendar" aria-selected="false" style="font-weight: 600;font-size: 21px;"><?php //Update ?>Calendar </a>
					<a class="nav-item nav-link @if($activeTab == 'invoices') active @endif" id="invoices-tab" data-toggle="tab" href="#invoices" role="tab" aria-controls="invoices" aria-selected="false" style="font-weight: 600;font-size: 21px;">Invoices </a>
                  
                    <!-- <a class="nav-item nav-link @if($activeTab == 'chatroom') active @endif" id="chatroom-tab" data-toggle="tab" href="#chatroom" role="tab" aria-controls="chatroom" aria-selected="false">Chatroom</a> -->
                </div>
            </nav>
			
			{{-- ###### --}}
            <div class="tab-content" id="nav-tabContent"> 
                <div class="tab-pane fade @if($activeTab == null || $activeTab == 'my-invitations' || $routeName == 'search-confirmed-patient' || $archivedinvitations == 'archived-invitations') show active @endif" id="my-invitations" role="tabpanel" aria-labelledby="my-invitations-tab">
					<!---added new design---->
					<div class="row1">
						<div class="accordion patient_details_box1" id="accordionExample">
						<div class="invitation-tabs">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link ns1  @if( $routeName != 'search-confirmed-patient' && $archivedinvitations != 'archived-invitations')  active @endif tbss" style="border-radius: 100px !important; margin-top:5px;" data-toggle="tab" href="#new-invitations" role="tab" aria-controls="new-invitations">
								<span class="material-icons doctor_activit_tab_icon">add</span>New
								<?php // Patients ?></a>
							</li>
							<li class="nav-item">
								<a class="nav-link ns1" data-toggle="tab" href="#accepted-invitations" role="tab" style="border-radius: 100px !important; margin-top:5px;" aria-controls="accepted-invitations tbss">
									<span class="material-icons doctor_activit_tab_icon">check</span>
								    Accepted<?php // Patients ?></a>
							</li>
							<li class="nav-item">
								<a class="nav-link ns1 @if( $routeName == 'search-confirmed-patient' && $archivedinvitations=='archived-invitations')  active @endif " style="border-radius: 100px !important; margin-top:5px;" data-toggle="tab" href="#confirmed-invitations" role="tab" aria-controls="confirmed-invitations">
									<span class="material-icons doctor_activit_tab_icon">done_all</span>Confirmed<?php // Patients ?></a>
							</li>
							<li class="nav-item">
								<a class="nav-link ns1 @if( $routeName == 'completed-invitations' && $archivedinvitations=='completed-invitations')  active @endif " style="border-radius: 100px !important; margin-top:5px;" data-toggle="tab" href="#completed-invitations" role="tab" aria-controls="completed-invitations">
									<span class="material-icons doctor_activit_tab_icon">offline_pin</span>Completed<?php // Patients ?></a>
							</li>
							<li class="nav-item">
								<a class="nav-link ns1" data-toggle="tab" href="#expired-invitations" style="border-radius: 100px !important; margin-top:5px;" role="tab" aria-controls="expired-invitations">
									<span class="material-icons doctor_activit_tab_icon">schedule</span>Expired</a>
							</li>
							<li class="nav-item">
								<a class="nav-link ns1" data-toggle="tab" href="#rejected-invitations" style="border-radius: 100px !important; margin-top:5px;" role="tab" aria-controls="expired-invitations">
									<span class="material-icons doctor_activit_tab_icon">info</span>Rejected</a>
							</li>
							<li class="nav-item">
								<a class="nav-link ns1" data-toggle="tab" href="#cancelled-invitations" style="border-radius: 100px !important; margin-top:5px;" role="tab" aria-controls="expired-invitations">
									<span class="material-icons doctor_activit_tab_icon">close</span>Cancelled</a>
							</li>

							<li class="nav-item">
								<a class="nav-link ns1  @if( $routeName != 'archived-invitations' && $archivedinvitations=='archived-invitations')  active @endif " href="{{url('/doctor-dashboard/archived-invitations')}}" style="border-radius: 100px !important; margin-top:5px; margin-bottom:10px;" role="tab" aria-controls="expired-invitations">
								<span class="material-icons doctor_activit_tab_icon">logout</span>Archived</a>
							</li>

							</ul>
							<div class="tab-content" id="invitations-tab-content">
								<div class="tab-pane @if( $routeName != 'search-confirmed-patient' && $archivedinvitations != 'archived-invitations')  active @endif" id="new-invitations" role="tabpanel">
									<div class="row" >
										{{-- <div class="col-md-12" style="padding-left: 10px;">
											<h3 class="inv-content-header">New Assessment(s)</h3>
										</div> --}}

								 @if ($new_invitations)
									@foreach($new_invitations as $invitation)
								<div class="col-md-12" style="padding: 0px; padding-left: 10px;">
									<div class="design_card_new">
                                       <div class="d-flex">
										    @if (($invitation->user->profilepic)!=null)
											     <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
											 @else
											    <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
											@endif
											<div class='n_c_container'>
												<h5 class="d_name_text">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</h5>
												<p class="firm_name">{{ucfirst($invitation->user->name)}}</p>
											</div>
									   </div>
									   <table id="n_desing_tbl">

										@if($doctor_category_id !== $invitation->invitee_category_id)
									 	 <tr>
											<th>Category:</th>
											<td>{{ $invitation->category->category_name }}</td>
										  </tr>
										@endif
										 
										  <tr>
											<th>Date:</th>
											<td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
										  </tr>
										  <tr>
											<th>Due date:</th>
											<td>{{date('d F Y', strtotime($invitation->duedate ))}}
											</td>
										  </tr>

										  <tr>
											<th>Type:</th>
											<td>{{ucfirst($invitation->appointment_type)}}</td>
										  </tr>
									   </table>

									   <div class="button_containter">

										<label for="view-cal-inp{{ $invitation->id }}">
											{{-- <div class="rountedBtn " style="background: #323B62 !important"  data-id="{{ $invitation->id }}"  title="Accept">
												<span class="material-icons" style="color: white; font-size:27px;">calendar_month </span>
											</div> --}}
											<input type="hidden" value="{{ date('m/d/Y',strtotime($invitation->invitation_date)) }}"  id="view-cal-inp{{ $invitation->id }}" style="margin-left:60px;cursor:pointer;" class="calendar_icon my-calendar-input" data-invite-date="{{ date('m/d/Y',strtotime($invitation->invitation_date)) }}">
										</label>

                                           <div class="accept-invitation-btn rountedBtn" data-id="{{ $invitation->id }}"  title="Accept">
											   <span class="material-icons" style="color: white; font-size:30px;">check</span>
										   </div>

                                           <div class="reject-inv-btn rountedBtn"  id="int_reject" style="background:#FA1D2F !important" title="Reject" data-id="{{ $invitation->id }}">
											   <span class="material-icons" style="color: white; font-size:30px;">close</span>
										   </div>

									   </div>

									</div>
									{{-- <div class="card patient_tab box_div new_padding">
										<div class="card-head" id="heading{{ $invitation->unique_id }}">
										  <h2 class="mb-0  docter_list" >
											   <div class="main1">

											   	<!-- Updated on 26-06-2021 -->

											   		<div class="row">
											   			<div class="col-md-2">
											   				<div class="sub">
																<?php          
																if(($invitation->user->profilepic)!=null){ ?>
																	  <img src="{{ url('uploads/profilepics/'.$invitation->user->profilepic) }}" class="profile">
																<?php
																}else {
																?> <img src="{{ asset('img/user.jpg') }}" class="profile">
																<?php  } ?>
															</div>
											   			</div>
											   			<div class="col-md-10">
											   				<div class="row mt-2">
											   					<div class="col-md-7">
											   						 <span class="name"><?php echo ucfirst($invitation->user->name); ?> &nbsp;<?php if($invitation->user->lawyer->lawfirmname!=null){ ?> ( <?php echo ucfirst($invitation->user->lawyer->lawfirmname);?>)<?php }?></span>  
											   					</div>
											   					<div class="col-md-5">
											   						<?php  
											    
																	 if($invitation->status=="pending" && strtotime($invitation->created_at) > strtotime($expire_time)) { ?>
																	   <a href="#" class="nav-link addpatient_btn accept-invitation-btn new_btn" style="right:0%;" data-id="{{ $invitation->id }}">Accept</a>
																	   <div class="acceptscss">
																	   <!--<p style="display:none;color:green;text-align: center;" id="acceptscss"><b>Invitation Accepted</b></p>-->
																	   </div>

																	   <a  class="nav-link addpatient_btn reject-inv-btn new_btn"type="button" id="int_reject" data-id="{{ $invitation->id }}" style="background:#d61f12;">Reject</a>
																	   <div class="rjtscss">
																	  <!-- <p style="display:none;color:red;text-align: center;" id="rjtscss"><b>Invitation Rejected</b></p>-->
																	   </div>
																		<?php  }  ?>
																		
																		 <?php  if($invitation->status=="active"){ ?>											   
																		<p class="nav-link addpatient_btn" style="border-radius:0px;">Accepted</p>
																		<?php  }  ?>
																		 <?php  if($invitation->status=="rejected"){ ?>											   
																		<a  class="nav-link addpatient_btn"  style="background:#d61f12;border-radius:0px;" >Rejected</a>
																		<?php  }  ?>
																		 <?php  if($invitation->status=="pending" && strtotime($invitation->created_at) <= strtotime($expire_time)){ ?>											   
																		<a  class="nav-link addpatient_btn" style="background:#e0ac12 !important;border-radius:0px;">Expired</a>
																		<?php  }  ?>
											   					</div>
											   				</div>
											   				<div class="row">
																   @if($doctor_category_id !== $invitation->invitee_category_id)
																   <div class="col-md-12">
																	   <span class="mb-1"><strong style="color: #00396b;font-weight: 600;">Category:</strong> {{ $invitation->category->category_name }}</span>
																   </div>
																   @endif
											   					<div class="col-md-5">
											   						<span class="calendar_p virtualbold">
																		<span style="color: #00396b;font-weight: 600;margin-bottom: 5px;">Appointment Date</span><img src="{{ url('img/calendar.png') }}" class="calendar_icon"><?php echo date('d F Y', strtotime($invitation->invitation_date )); ?> |  {{ $invitation->timeslot }} 
																		<?php  if($invitation->appointment_type=='virtual') { ?>
																			<b> - Virtual</b>
																		<?php  } ?>
																		</span>
												   					</div>
												   					<div class="col-md-7">
												   						<span class="calendar_p" style="margin-top: 0%;">
																	<span style="color: red;font-weight: 600;margin-bottom: 5px;">Report Due Date:</span><img src="{{ url('img/calendar.png') }}" class="calendar_icon"><?php echo date('d F Y', strtotime($invitation->duedate )); ?>		
																	
																	<label for="view-cal-inp{{ $invitation->id }}"><img src="{{ asset('img/calendar_new.png') }}" class="calendar_img"><input type="hidden" value="{{ date('m/d/Y',strtotime($invitation->invitation_date)) }}"  id="view-cal-inp{{ $invitation->id }}" style="margin-left:60px;cursor:pointer;" class="calendar_icon my-calendar-input" data-invite-date="{{ date('m/d/Y',strtotime($invitation->invitation_date)) }}"></label>
																 </span> 
											   					</div>																
											   				</div>
											   			</div>
											   		</div>
											   		<!-- Updated on 26-06-2021 -->
											   		
											   </div>
										  </h2>		
										 <a class="mb-0 collapsed collapsed read-more-block" data-toggle="collapse" data-target="#collapse{{ $invitation->unique_id }}" aria-expanded="false" aria-controls="collapse{{ $invitation->unique_id }}">
											 <img src="{{ asset('images/up-chevron.svg') }}" class="arrow-up" width="20"  alt="up">
											<img src="{{ asset('images/down-chevron.svg') }}" class="arrow-down" width="20" alt="down">
										 </a>
										</div>    
										<div id="collapse{{ $invitation->unique_id }}" class="collapse" aria-labelledby="heading{{ $invitation->unique_id }}" data-parent="#accordionExample">
										  <div class="card-body">
												 <p><span class="img_bg"><img src="{{ asset('img/address.svg') }}" class="address"></span> <span>{{ $invitation->user->business_address }}</span></p>
											   <p style="margin: 0px 0px 5px;">Website Url: <span>{{$invitation->user->lawyer->website_url }}</span></p>
										  </div>
										</div>
									</div> --}}
								</div>	
								@endforeach
								@else
								<div class="col-md-12">
									<h4 style="text-align:center;margin-top: 15px;">No new assessment(s) received.</h4>
								</div>
								@endif
							</div>
						</div>
						<div class="tab-pane" id="accepted-invitations" role="tabpanel">
							<div class="row">
								<div class="col-md-12">
									{{-- <h3 class="inv-content-header">Accepted Assessments</h3> --}}
									@if (Session::has('success'))
										<div class="mt-2 alert alert-success">{{ Session::get('success') }}</div>
									@endif
								</div>
								@if ($accepted_invitations)
									@foreach($accepted_invitations as $invitation)								
								<!---start--->
								<div class="col-md-12" style="padding: 0px; padding-left: 10px;">
									<div class="design_card_new">
                                       <div class="d-flex">
										    @if (($invitation->user->profilepic)!=null)
											     <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
											 @else
											    <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
											@endif
											<div class='n_c_container'>
												<h5 class="d_name_text">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</h5>
												<p class="firm_name">{{ucfirst($invitation->user->name)}}</p>
											</div>
									   </div>
									   <table id="n_desing_tbl">

										@if($doctor_category_id !== $invitation->invitee_category_id)
									 	 <tr>
											<th>Category:</th>
											<td>{{ $invitation->category->category_name }}</td>
										  </tr>
										@endif
										 
										  <tr>
											<th>Date:</th>
											<td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
										  </tr>
										  <tr>
											<th>Due date:</th>
											<td>{{date('d F Y', strtotime($invitation->duedate ))}}
											</td>
										  </tr>

										  <tr>
											<th>Type:</th>
											<td>{{ucfirst($invitation->appointment_type)}}</td>
										  </tr>
									   </table>

									</div>
								</div>	
								<!---end--->
								@endforeach
								@else
								<div class="col-md-12">
									<h4 style="text-align:center;">No accepted assessment(s) found.</h4>
								</div>
								@endif
							</div>
						</div>


						{{-- ****************** Confirmed section ************** --}}
							<div class="tab-pane @if( $routeName == 'search-confirmed-patient')  active @endif" id="confirmed-invitations" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										{{-- <h3 class="inv-content-header">Confirmed Assessment(s)</h3>									 --}}
									</div>									
									<div class="col-md-8">
										<form class="search_form" method="GET" action="{{ route('search-confirmed-patient') }}">
											<div class="input-group">
												<input class="form-control" id="system-search" name="lawyerkeyword" placeholder="Search Name" value="<?php if(isset($_GET['lawyerkeyword'])){echo $_GET['lawyerkeyword'];}?>">
												<span class="input-group-btn">
													<button type="submit" class="btn btn-default search_btn_new">
														<!--<img src="img/search.png">-->
														<img src="{{ url('img/search.png') }}">
													</button>
												</span>
											</div>
										</form>
									</div>
									<?php  if(isset($_GET['lawyerkeyword'])) { ?>
										<div class="col-md-1">
											<a class="nav-link" style="font-size: 19px;" href="{{url('doctor-dashboard')}}/my-invitations/#confirmed-invitations" >clear</a>
										</div>
									<?php } ?>
									<div class="col-md-4"></div>
									<div class="col-md-4 text-right">
										<!--<a class="btn addpatient_btn" data-toggle="tab" href="#archived-invitations" role="tab" aria-controls="archived-invitations">Archived Invitations</a>-->
										{{-- <a  href="{{url('/doctor-dashboard/archived-invitations')}}" class="btn addpatient_btn">Archived</a> --}}
									</div>
									<div class="col-md-12">
										<div class="search_sec">
											<div class="row1">
												<div class="accordion" id="accordionExample">
													<div class="row">
													@if ($confirmed_invitations)
													    @if (count($confirmed_invitations) > 0)
														<div class="mb-2">
															<label class="label"> 
																<input id="choooseAllConfirm" type="checkbox" name="checkbox" value="text"> Select All
															</label>
														</div>
														@endif
														 
														 <form method="post" action="{{url('/doctor_confirmed_archive')}}" style="width: 100%">
														@csrf
														<?php   
												foreach($confirmed_invitations as $invitation){   
															$current_date=date("Y-m-d"); ?>
														
														@if($invitation->is_archive=='no')
														<!---start--->

														<div class="col-md-12" style="padding: 0px; padding-left: 10px;">
															<div class="design_card_new">
															   <div class="d-flex">
																	@if (($invitation->user->profilepic)!=null)
																	<a class="n_links" href="{{ url('/invitedetail/'.$invitation->unique_id) }}"> <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" /></a>
																	 @else
																		<img class="n_images" src="{{asset('img/no_image.jpg')}}" />
																	@endif
																	<div class='n_c_container'>
																		<h5 class="d_name_text"> 
																			<a class="n_links" href="{{ url('/invitedetail/'.$invitation->unique_id) }}">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</a>
																		</h5>
																		<p class="firm_name">{{ucfirst($invitation->user->name)}}</p>
																	</div>
															   </div>
															   <table id="n_desing_tbl">

																<tr>
																	<th>Patient:</th>
																	<td>{{ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname)}}</td>
																  </tr>
																  <tr>
						
																@if($doctor_category_id !== $invitation->invitee_category_id)
																  <tr>
																	<th>Category:</th>
																	<td>{{ $invitation->category->category_name }}</td>
																  </tr>
																@endif
																 
																  <tr>
																	<th>Date:</th>
																	<td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
																  </tr>
																  <tr>
																	<th>Due date:</th>
																	<td>{{date('d F Y', strtotime($invitation->duedate ))}}
																	</td>
																  </tr>
						
																  <tr>
																	<th>Type:</th>
																	<td>
																		@if ($invitation->appointment_type == "inperson")
																		{{"In-Person"}}
																		@else
                                                                           {{ucfirst($invitation->appointment_type)}}
																		@endif
																	
																  </tr>
															   </table>
						
															   <div class="button_containter">
																@if($invitation->duedate < $current_date)
																		<input type="checkbox"  name="archive[]" value="{{$invitation->id}}" class="check_box confirmedTaskCheckbox">
																	@endif 

																   <div class="rountedBtn" style="background:#546E7A !important; width:30px !important; height:30px !important;" data-id="{{ $invitation->id }}"  title="View more">
																	   <a  href="{{ url('/invitedetail/'.$invitation->unique_id) }}" style="line-height: 0;">
																		<span class="material-icons" style="color: white; font-size:21px;">person</span>
			
																	</a>
																   </div>
						
															   </div>
						
															</div>
														</div>

														
														<!---end //abss --->
														@endif
														<?php  }  ?>
														@if (count($confirmed_invitations) > 0)
													     	<div class="text-right">
																<button type="submit" class="btn addpatient_btn" style="position:relative;margin-right:15px;">Archive</button>
															</div>
															@endif
													   </form>
														@else
														<h4 style="text-align:center;margin-left: 15px;">No Records Found</h4>
														@endif
													</div>											
												</div>	
											</div>      
										</div>
										
									</div>
								</div>
							</div>


							{{-- #################### Completed task start #################### --}}
							<div class="tab-pane @if( $routeName == 'completed-invitations')  active @endif" id="completed-invitations" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										{{-- <h3 class="inv-content-header">Completed Assessment(s)</h3>									 --}}
									</div>									

									
									<div class="col-md-12">
										<div class="search_sec">
											<div class="row1">
												<div class="accordion" id="accordionExample">
													<div class="row">
													@if ($completedAr)
														 <form style="width:100%" method="post" action="{{url('/doctor_confirmed_archive')}}">
														@csrf
														<?php   

													  foreach($completedAr as $invitation){   
															$current_date=date("Y-m-d"); ?>
														
														@if($invitation->is_archive=='no')
														<!---start--->

														<div class="col-md-12" style="padding: 0px; padding-left: 10px;">
															<div class="design_card_new">
															   <div class="d-flex">
																	@if (($invitation->user->profilepic)!=null)
																		 <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
																	 @else
																		<img class="n_images" src="{{asset('img/no_image.jpg')}}" />
																	@endif
																	<div class='n_c_container'>
																		<h5 class="d_name_text">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</h5>
											                        	<p class="firm_name">{{ucfirst($invitation->user->name)}}</p>
																	</div>
															   </div>
															   <table id="n_desing_tbl">

																<tr>
																	<th>Patient:</th>
																	<td>{{ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname)}}</td>
																  </tr>
																  <tr>
						
																@if($doctor_category_id !== $invitation->invitee_category_id)
																  <tr>
																	<th>Category:</th>
																	<td>{{ $invitation->category->category_name }}</td>
																  </tr>
																@endif
																 
																  <tr>
																	<th>Date:</th>
																	<td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
																  </tr>
																  <tr>
																	<th>Due date:</th>
																	<td>{{date('d F Y', strtotime($invitation->duedate ))}}
																	</td>
																  </tr>
						
																  <tr>
																	<th>Type:</th>
																	<td>{{ucfirst($invitation->appointment_type)}}</td>
																  </tr>
															   </table>
						
															   <div class="button_containter">
						
																<div class="rountedBtn" style="background:#546E7A !important; width:30px !important; height:30px !important;" data-id="{{ $invitation->id }}"  title="View more">
																	<a  href="{{ url('/invitedetail/'.$invitation->unique_id) }}" style="line-height: 0;">
																	 <span class="material-icons" style="color: white; font-size:21px;">person</span>
		 
																 </a>
																</div>
						
															   </div>
						
															</div>
														</div>

														<!---end--->
														@endif
														<?php  }  ?>
														
																</form>
														@else
														<h4 style="text-align:center;margin-left: 15px;">No Records Found</h4>
														@endif
													</div>											
												</div>	
											</div>      
										</div>
										
									</div>
								</div>
							</div>
							{{-- #################### Completed task edn #################### --}}



							<!--Archive start-->
							<div class="tab-pane @if($archivedinvitations == 'archived-invitations') active @endif" id="archived-invitations" role="tabpanel">
								<div class="row">
									<div class="col-md-9">
										{{-- <h3 class="inv-content-header">Archived Assessment(s)</h3> --}}
										@if (Session::has('archivedsuccess'))
										<div class="mt-2 alert alert-success">{{ Session::get('archivedsuccess') }}</div>
									@endif
									</div>	
									<div class="col-md-3">
										<a class="btn addpatient_btn" data-toggle="tab" href="#confirmed-invitations" role="tab" aria-controls="archived-invitations" style="float:right;">Back</a>
									</div>
									<div class="col-md-12">
										<div class="search_sec">
											<div class="row1">
												<div class="accordion" id="accordionExample">
													<div class="row">
														 @if ($archived_invitations->isNotEmpty())
														
														<?php   foreach($archived_invitations as $invitation){    ?>
														<!---start--->
														<div class="col-md-12" style="padding: 0px; padding-left: 10px;">
															<div class="design_card_new">
															   <div class="d-flex">
																	@if (($invitation->user->profilepic)!=null)
																		 <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
																	 @else
																		<img class="n_images" src="{{asset('img/no_image.jpg')}}" />
																	@endif
																	<div class='n_c_container'>
																		
																		
																		<h5 class="d_name_text"> {{ucfirst($invitation->user->lawyer->lawfirmname)}}
																			<a href="{{url('invitedetail/'.$invitation->unique_id.'/chatroom')}}" title="Chat" class="position-relative1"  style="position: relative !important;"><img src="{{ url('img/messenger.png') }}" class="chat_icon" style="width:28px !important">
																				@if(Helper::un_read_messages(Auth::id(),$invitation->id))	
																					<label class="badge badge-warning notification_badge" style="left:inherit">{{ Helper::un_read_messages(Auth::id(),$invitation->id) }}</label>
																				@endif
																			</a>
																		</h5>
																		<p class="firm_name">{{ucfirst($invitation->user->name)}}</p>
																	</div>
															   </div>
															   <table id="n_desing_tbl">

																<tr>
																	<th>Patient:</th>
																	<td>{{ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname)}}</td>
																  </tr>
																  <tr>
						
																@if($doctor_category_id !== $invitation->invitee_category_id)
																  <tr>
																	<th>Category:</th>
																	<td>{{ $invitation->category->category_name }}</td>
																  </tr>
																@endif
																 
																  <tr>
																	<th>Date:</th>
																	<td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
																  </tr>
																  <tr>
																	<th>Due date:</th>
																	<td>{{date('d F Y', strtotime($invitation->duedate ))}}
																	</td>
																  </tr>
						
																  <tr>
																	<th>Type:</th>
																	<td>{{ucfirst($invitation->appointment_type)}}</td>
																  </tr>
															   </table>
						
															   <div class="button_containter">
													 
																<div class="rountedBtn" style="background:#546E7A !important; width:30px !important; height:30px !important;" data-id="{{ $invitation->id }}"  title="View more">
																	<a  href="{{ url('/invitedetail/'.$invitation->unique_id) }}" style="line-height: 0;">
																	 <span class="material-icons" style="color: white; font-size:21px;">person</span>
																 </a>
																</div>
						
															   </div>
						
															</div>
														</div>	
														<!---end--->
														
														<?php  }  ?>
														@else
														<h4 style="text-align:center;margin-left: 15px;">No Records Found</h4>
														@endif
													</div>											
												</div>	
											</div>      
										</div>
										
									</div>
								</div>
							</div>
							<!--Archive end-->





							<div class="tab-pane" id="expired-invitations" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										{{-- <h3 class="inv-content-header">Expired Assessment(s)</h3> --}}
									</div>
									@if ($expired_invitations)

									<div class="mb-2">
										<label class="label"> 
											<input id="choooseAllTask" type="checkbox" name="checkbox" value="text"> Select All
										</label>
									</div>
									

									<form method="post" action="{{url('/doctor_confirmed_archive')}}" style="width: 100%">
										@csrf
										@foreach($expired_invitations as $invitation)									
									<!---start--->
									<div class="col-md-12" style="padding: 0px; padding-left: 10px;">
										<div class="design_card_new">
										   <div class="d-flex">
												@if (($invitation->user->profilepic)!=null)
													 <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
												 @else
													<img class="n_images" src="{{asset('img/no_image.jpg')}}" />
												@endif
												<div class='n_c_container'>
													<h5 class="d_name_text">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</h5>
											        <p class="firm_name">{{ucfirst($invitation->user->name)}}</p>
												</div>
										   </div>
										   <table id="n_desing_tbl">
	
											@if($doctor_category_id !== $invitation->invitee_category_id)
											  <tr>
												<th>Category:</th>
												<td>{{ $invitation->category->category_name }}</td>
											  </tr>
											@endif
											 
											  <tr>
												<th>Date:</th>
												<td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
											  </tr>
											  <tr>
												<th>Due date:</th>
												<td>{{date('d F Y', strtotime($invitation->duedate ))}}
												</td>
											  </tr>
	
											  <tr>
												<th>Type:</th>
												<td>{{ucfirst($invitation->appointment_type)}}</td>
											  </tr>
										   </table>

										   <div class="button_containter">
												 <input type="checkbox"  name="archive[]" value="{{$invitation->id}}" class="check_box expiredTaskCheckbox">
											</div>
	
										</div>
									</div>	
									<!---end //abss--->
									@endforeach

									<div class="text-right">
										<button type="submit" class="btn addpatient_btn" style="position:relative;margin-right:15px;">Archive</button>
									</div>
							   </form>

									@else
									<div class="col-md-12">
										<h4 style="text-align:center;">No expired assessment(s) found.</h4>
									</div>
									@endif
								</div>
							</div>
							<div class="tab-pane" id="rejected-invitations" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										{{-- <h3 class="inv-content-header">Rejected Assessment(s)</h3> --}}
										@if (Session::has('success'))
											<div class="mt-2 alert alert-success">{{ Session::get('success') }}</div>
										@endif
									</div>
									@if ($rejected_invitations)
										@foreach($rejected_invitations as $invitation)				

										<div class="col-md-12" style="padding: 0px; padding-left: 10px;">
											<div class="design_card_new">
											   <div class="d-flex">
													@if (($invitation->user->profilepic)!=null)
														 <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
													 @else
														<img class="n_images" src="{{asset('img/no_image.jpg')}}" />
													@endif
													<div class='n_c_container'>
														<h5 class="d_name_text">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</h5>
											        <p class="firm_name">{{ucfirst($invitation->user->name)}}</p>
													</div>
											   </div>
											   <table id="n_desing_tbl">
		
												@if($doctor_category_id !== $invitation->invitee_category_id)
												  <tr>
													<th>Category:</th>
													<td>{{ $invitation->category->category_name }}</td>
												  </tr>
												@endif
												 
												  <tr>
													<th>Date:</th>
													<td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
												  </tr>
												  <tr>
													<th>Due date:</th>
													<td>{{date('d F Y', strtotime($invitation->duedate ))}}
													</td>
												  </tr>
		
												  <tr>
													<th>Type:</th>
													<td>{{ucfirst($invitation->appointment_type)}}</td>
												  </tr>
											   </table>
		
											   <div class="button_containter">
												<a  class="nav-link addpatient_btn"  style="background:#d61f12 !important;border-radius:10px !important;" >Rejected By {{($invitation->rejected_by == 'patient' ? 'Patient' : 'You')}}</a>
											   </div>
		
											</div>
									
										
									</div>	



									<!---end--->
									@endforeach
									@else
									<div class="col-md-12">
										<h4 style="text-align:center;">No rejected assessment(s) found.</h4>
									</div>
									@endif
								</div>
							</div>
							<div class="tab-pane" id="cancelled-invitations" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										{{-- <h3 class="inv-content-header">Cancelled Assessment(s)</h3> --}}
									</div>
									@if ($cancelled_invitations)
										@foreach($cancelled_invitations as $invitation)				
										<div class="col-md-12" style="padding: 0px; padding-left: 10px;">
											<div class="design_card_new">
											   <div class="d-flex">
													@if (($invitation->user->profilepic)!=null)
														 <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
													 @else
														<img class="n_images" src="{{asset('img/no_image.jpg')}}" />
													@endif
													<div class='n_c_container'>
														<h5 class="d_name_text">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</h5>
											        <p class="firm_name">{{ucfirst($invitation->user->name)}}</p>
													</div>
											   </div>
											   <table id="n_desing_tbl">
		
												@if($doctor_category_id !== $invitation->invitee_category_id)
												  <tr>
													<th>Category:</th>
													<td>{{ $invitation->category->category_name }}</td>
												  </tr>
												@endif
												 
												  <tr>
													<th>Date:</th>
													<td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
												  </tr>
												  <tr>
													<th>Due date:</th>
													<td>{{date('d F Y', strtotime($invitation->duedate ))}}
													</td>
												  </tr>
		
												  <tr>
													<th>Type:</th>
													<td>{{ucfirst($invitation->appointment_type)}}</td>
												  </tr>
											   </table>
		
											   <div class="button_containter" style="align-items: center;">
												<div class="rountedBtn ml-0" style="background:#546E7A !important; width:30px !important; height:30px !important;" data-id="{{ $invitation->id }}"  title="View more">
													<a  href="{{ url('/invitedetail/'.$invitation->unique_id) }}" style="line-height: 0;">
														<span class="material-icons" style="color: white; font-size:21px;">person</span>
													</a>
												</div>

												<a  class="nav-link addpatient_btn"  style="background:#d61f12 !important;border-radius:10px !important;" >Cancelled By Lawyer</a>
											   </div>
		
											</div>
									
										
									</div>
										
										

									<!---end--->
									@endforeach
									@else
									<div class="col-md-12">
										<h4 style="text-align:center;">No Cancelled assessment(s) found.</h4>
									</div>
									@endif
								</div>
							</div>
							</div>
							</div>	
						</div>	
					</div>
					<!---end new design---->
					
					<!---start---->
					<div class="modal fade" id="scss_acc_pt" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
					
					  <div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content login_box">
						  <div class="modal-body">		
						  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  <div class="row">
								<div class="col-md-3 text-center m-auto">
									<img id="modal-inv-succes-img" src="{{ asset('img/checkmark.svg') }}" class="w-100" alt="success" />				
								</div>
						  </div>
						  <h5 class="modal-title"  style="display:none;color:green;text-align: center;" id="acceptscss">Assessment Accepted</h5>
							<h5 class="modal-title"  style="display:none;color:red;text-align: center;" id="rjtscss">Assessment Rejected</h5>
							
						  </div>
						</div>
					  </div>
					</div>
					<!----end----->
                    </div>					
					<!---   </div>-->			
					

					<div class="tab-pane fade @if($activeTab == 'update-calendar') show active @endif"  id="update-calendar" role="tabpanel" aria-labelledby="update-calendar-tab">					
                    <div class="row">
                        <div class="col-md-12">
						@if(Session::has('message'))
							<div class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</div>
						@endif	
                            <div class="search_sec">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
					 
				<div class="modal fade" id="addevent" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
				
					<?php
							//print_r($dueblockedDates);exit;
							//print_r($assessment_days);exit;
							//print_r($due_days);exit;
							$newList1=json_encode($newList);  
					
					?>
				  <div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content login_box">
					  <div class="modal-header">
						<h5 class="modal-title" id="loginTitle">Add Event</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
							  <form id="addevent_form"><!-------method="POST" action="{{ route('login') }}--------->
									@csrf
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<input type="hidden" id="selecteddate" name="selecteddate">
												<input type="text" class="form-control" id="notes" name="notes" aria-describedby="notes" placeholder="Enter Notes">
											</div>
										</div>
										<div class="col-md-12">
										   <p id="successtask" style="color:green;display:none;">Event Added Successfully</p>
										</div>
									</div>
									<button type="submit" class="btn btn-primary submit_btn"  id="addevent_btn">Submit</button>
								</form>
					  </div>
					</div>
				  </div>
				</div>
				<!---delete event---->
				<div class="modal fade" id="deleteevent" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
				
					<?php
							$newList1=json_encode($newList);
					
					?>
				  <div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content login_box">
					  <div class="modal-header" >
						<h5 class="modal-title" id="loginTitle" >Are Sure Want to Delete ?</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
							  <form id="deleteevent_form"><!-------method="POST" action="{{ route('login') }}--------->
									@csrf
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<input type="hidden" id="rowid1" name="rowid1">
												<input type="hidden" class="form-control" id="rownotes1" name="rownotes1" aria-describedby="rownotes" placeholder="Enter Edited Notes" disabled>
											</div>
										</div>
										<div class="col-md-12">
										   <p id="successtask1" style="color:green;display:none;">Event Deleted Successfully</p>
										</div>
									</div>
									<button type="submit" class="btn btn-primary submit_btn"  id="addevent_btn1">Yes</button>
									<button type="submit" class="btn btn-primary submit_btn"  id="deleteevent_btn1" data-dismiss="modal" aria-label="Close">No</button>
								</form>
					  </div>
					</div>
				  </div>
				</div>
				
				<!------edit------->
				
				 
				<div class="modal fade" id="editevent" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
				
					<?php
							$newList1=json_encode($newList);
					
					?>
				  <div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content login_box">
					  <div class="modal-header">
						<h5 class="modal-title" id="loginTitle">Edit Event</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
							<form id="editevent_form"><!-------method="POST" action="{{ route('login') }}--------->
								@csrf
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<input type="hidden" id="rowid" name="rowid">
											<input type="text" class="form-control" id="rownotes" name="rownotes" aria-describedby="rownotes" placeholder="Enter Edited Notes">
										</div>
									</div>
									<div class="col-md-12">
										<p id="successedit" style="color:green;display:none;">Event Updated Successfully</p>
									</div>
								</div>
								<button type="submit" class="btn btn-primary submit_btn"  id="editevent_btn">Submit</button>
							</form>
					  </div>
					</div>
				  </div>
				</div>
                </div>
               
				<!----upload docs---->
				 <!-- Available Calendar Model -->
				<div class="modal fade" id="Availablecalendar" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content login_box">
					  <div class="modal-header">
						<h5 class="modal-title" id="calendarTitle">Please upload patients documents</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
						<form method="post" action="#" id="#">
							<div class="form-group files">
							  <input type="file" class="form-control" multiple="">
							</div>
						</form>
						  <a href="#" class="invite_btn">Submit</a>
					  </div>
					</div>
				  </div>
				</div>
				<!----end  upload docs---->

                <div class="tab-pane fade @if($activeTab == 'invoices') show active @endif" id="invoices" role="tabpanel" 			aria-labelledby="invoices-tab">
					
						<div class="col-md-12">
                            <div class="patients_list_main">
								<?php if($invoices->isEmpty() || ($suminvoices == 0)){  ?>
								<h3 style="text-align:center;">No Records Found</h3>
								<?php  }else{  ?>
                                <div class="row">
									<div class="col-md-12">
											<h4 style="float:right;">
											<?php
												echo 'Total Billed to Date '.date('d F Y');
											?><b style="font-weight: 600;"> ${{$suminvoices}}</b></h4>
									</div>	
									<?php //$number =1; ?>
                                    <?php	
										//print_r($invoices); 									
										foreach($invoices as $invoices1) {                                        
                                        	//print_r($invoices1);                                        
                                        ?>
                                    <div class="col-md-12">
											<?php  if($invoices1->invitations->status!='cancelled') { ?>
                                            <div class="single_patient">
                                                <ul>
                                                    <li>
													<img src="https://proassessors.com/img/checkmark.svg" width="20" />
													<span style="color: #00528c;">
													Invoice Submitted Date:</span>  <?php echo date('d F Y', strtotime($invoices1->invoice_date )); ?>
													<span style="margin-left: 13px;"><span style="color: #00528c;">Patient: </span>{{ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname)}}</span>
													<span class="patientfile">
														<?php
															if($invoices1->payment_status == 'success') {
																echo 'Paid';
															} else {
																echo 'Pending';
															}
														?>
														<a href="javascript:void(0)" onClick="reply_click_doctorinvoice(id)" id="{{ $invoices1->invitations->id }}"  title="View Invoice"><i class="fa fa-info-circle" ></i></a>
														<!--<a href="" title="Appointments"><i class="fa fa-calendar" ></i></a>-->
														<a title="Download Invoice" href="{{url('/uploads/custom_invoice')}}/{{$invoices1->invoice_filename}}" download="invoice_{{$invoices1->invoice_filename}}" target="_blank"><i class="fa fa-download"></i></span></li></a> 

														
													</span>
													</li>
														
                                                </ul>
                                            </div>	
											<?php } ?>	
                                        <!--</a>-->
                                    </div>
										<?php  }  ?>                     
									
                                </div>
								{{$invoices->links("pagination::bootstrap-4")}}
								
								<?php  } ?>
								
                            </div>
							
                        </div>										
									
                </div>
				<!---start invoice modal view---->
				<div class="modal fade" id="invoicemodal" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 960px;">
						<div class="modal-content login_box">		
										@include('pages.doctor.doctorinvoicemodalview') 
						</div>
					</div>
				</div>
				<!---end invoice modal view---->
            </div>
		</div>
        </div>
    </section>


<div class="modal" id="block-calendar-modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
	<form action="{{ url('doctor/block-dates') }}" method="post" id="block-date-calendar-form">
      <div class="modal-header">
        <h5 class="modal-title">Block/Un Block Dates</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <p class="text-right"><input type="checkbox" name="select_all" id="block_select_all" value="1"/> <label for="block_select_all" class="cursor-pointer">Select All</label></p>
	  <div id="mdp-demo"></div>
	  @csrf
	  <input type="hidden" name="dates" id="blocked_dates" />
	  <input type="hidden" name="selected_date" id="selected_date" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Block Dates</button>
      </div>
	  </form>
    </div>
  </div>
</div>
		<!----end----->
    <!-- Lawyer Dashboard Section -->
@endsection

@section('footerScripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
function addCanderNote(inp,inst){
	console.log(inp)
	console.log(inst)
	console.log($('.ui-datepicker-div'));
	$(`<div class="calender-note">
	<p><span class="yellow"></span> - Booked</p><p><span class="red"></span> - Report Due date</p>	
	</div>`).insertAfter('.ui-datepicker-calendar')
}

	$(document).ready(function() {

		var dueDateArr = <?=$dueDateArr?>;
		var invitationDateArr = <?=$invitationDateArr?>;

	var dueDates = {};
	var invitationDates = {};

		if(dueDateArr.length){
			for(var i =0; i<dueDateArr.length; i++){
				dueDates[new Date(dueDateArr[i])] = new Date(dueDateArr[i]).toString();
			}
		}
		
		if(invitationDateArr.length){
			for(var j =0; j<invitationDateArr.length; j++){
				invitationDates[new Date(invitationDateArr[j])] = new Date(invitationDateArr[j]).toString();
			}
		}

$('.my-calendar-input').datepicker({
  showOn: 'button',
  buttonText:`<div class="rountedBtn " style="background: #323B62 !important"  data-id="@isset($invitation) {{  $invitation->id }} @endisset"  title="Calendar">
			 <span class="material-icons" style="color: white; font-size:27px;">calendar_month </span>
			 </div>`,
  options: function(dateText, inst) { inst.show() },
  beforeShow:function(inp, inst){
	setTimeout(function () {
		addCanderNote(inp,inst);
	},500)
  },
  beforeShowDay: function(date) {
            var dueDate = dueDates[date];
            var inviteDate = invitationDates[date];
            if (dueDate) {
                return [true, "highlight-due-date", dueDate];
            }else if(inviteDate){
                return [true, "highlight-invite-date", inviteDate];
			}
            else {
                return [true, '', ''];
            }
        }
		
});

		$('#dashboard-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var target = $(e.target).attr("href")
			var activeTab = target.replace('#','');
    		var baseURL = '{{url("doctor-dashboard") }}';  
			window.history.pushState({}, null, baseURL+'/'+activeTab);
		});
		
		$('.invitation-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var activeTab = $(e.target).attr("href")
		var baseURL = '{{url("doctor-dashboard/my-invitations") }}'; 
			window.history.pushState({}, null, baseURL+'/'+activeTab);
		});


		var hash = window.location.hash;
		if(hash.length > 0) {
			$('.invitation-tabs .nav-item .nav-link').removeClass('active');
			$('.invitation-tabs #invitations-tab-content .tab-pane').removeClass('active');
			$('.invitation-tabs .nav-item .nav-link[href="'+hash+'"]').addClass('active');
			$('.invitation-tabs #invitations-tab-content '+hash).addClass('active');
		}
		
});

$("#choooseAllTask").on("change",function(){
   if($('#choooseAllTask:checkbox:checked').length > 0){
	  $('.expiredTaskCheckbox').prop('checked', true); 
   }else{
	$('.expiredTaskCheckbox').prop('checked', false); 
   }
});


$("#choooseAllConfirm").on("change",function(){
   if($('#choooseAllConfirm:checkbox:checked').length > 0){
	  $('.confirmedTaskCheckbox').prop('checked', true); 
   }else{
	$('.confirmedTaskCheckbox').prop('checked', false); 
   }
});

</script>

@endsection