<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Mail\PatientInvitation;
use App\Mail\LawyerCancelAppointment;
use App\Mail\ChatNotificationMail;
use App\Mail\PatientAcceptInvitation;
use App\Mail\PatientRejectInvitation;
use App\Mail\PatientInvitationAcceptence;
use App\Mail\AuthenticationMail;
use App\Mail\AuthenticationNotificationMail;
use App\Mail\LawyerAuthenticationStatusChange;
use App\Mail\ResetPasswordMail;
use App\Mail\Invoice;
use App\Helpers\Helper;
use App\Mail\Loginotp;
use App\Mail\LawyerSendInvitation;
use App\Mail\FileUploadInvitation;
use App\Mail\PatientAccountCreated;
use App\Mail\DeletePatientAccountMail;
use Mail;

class EmailController extends Controller
{  
		/*lawyer invites doctor*/
		/*
		public function lawyer_invite_doctor($doctorname,$lawyername,$doctoremail){			
			$body = "<p>Dear ".$doctorname.",</p>
			<p>You have received an Invite from <b>".$lawyername .".</b></p>
			<p>Please <a href='".<?=url('/') ?>."' target='_blank'>login<a> to Pro Assessors for more details.</p>
			<p style='margin-bottom: -3px;margin-top: 21px;'>Regards</p>
			<p><b>Pro Assessors Team</b></p>";
			$regemail=$doctoremail;
			$regname=$doctorname;
			Mail::send([],[], function ($message) use($body,$regemail,$regname) {
			$message->subject('Pro Assessors - New Invitation Notification');
			$message->from('showmacktest@gmail.com','HCMG');
			$message->to($regemail,$regname);
			$message->setBody($body, 'text/html');
			});
		}*/
		public function lawyer_invite_doctor($invitation,$senderMail,$sendName){
			$doctor = $invitation->user1;
			$doctor->email = $senderMail;
			$doctor->name = $sendName;
			Mail::to($doctor->email,$doctor->name)->send(new LawyerSendInvitation($invitation,['type'=>'doctor']));
		}
		public function send_invitation_confirmation_to_patient($invitation)
		{
			$patient = $invitation->patient;
			Mail::to(Helper::Decrypt($patient->email),$patient->name)->send(new PatientInvitation($invitation,['type'=>'patient']));
		}
		public function send_invitation_confirmation_to_lawyer($invitation)
		{
			$lawyer = $invitation->user;
			Mail::to($lawyer->email,$lawyer->name)->send(new PatientInvitation($invitation,['type'=>'lawyer']));
		}
		public function send_notification_email_to_lawyer_after_doctor_rejects($invitation)
		{
			$lawyer = $invitation->user;
			Mail::to($lawyer->email,$lawyer->name)->send(new PatientInvitation($invitation,['type'=>'lawyer']));
		}
		public function send_notification_email_to_lawyer_after_patient_accepts($invitation,$name,$email)
		{
			$lawyer = $invitation->user;			
			//$doctor = $invitation->user1;			
			Mail::to($lawyer->email,$lawyer->name)->send(new PatientAcceptInvitation($invitation,['type'=>'lawyer']));
			Mail::to($email,$name)->send(new PatientAcceptInvitation($invitation,['type'=>'doctor']));
            if(($invitation->attachments == null) || ($invitation->attachments->medicalsfilename == null) ){
				Mail::to($lawyer->email,$lawyer->name)->send(new PatientAcceptInvitation($invitation,['type'=>'lawyermedicals']));
			}
		}		
		public function send_notification_email_to_lawyer_after_patient_rejects($invitation)
		{
			$lawyer = $invitation->user;			
			$doctor = $invitation->user1;			
			Mail::to($lawyer->email,$lawyer->name)->send(new PatientRejectInvitation($invitation,['type'=>'lawyer']));
			Mail::to($doctor->email,$doctor->name)->send(new PatientRejectInvitation($invitation,['type'=>'doctor']));			
		}
		public function send_notification_email_to_doctor_after_lawyer_upload_documents($invitation,$name=null,$email=null)
		{
			if($email == null){
				$doctor = $invitation->user1;
				$email = $doctor->email;
				$name = $doctor->name;
			}
			
			Mail::to($email,$name)->send(new FileUploadInvitation($invitation,['type'=>'doctor']));			
		}
		public function send_notification_email_to_lawyer_after_doctor_upload_reports($invitation)
		{
			$lawyer = $invitation->user;
			Mail::to($lawyer->email,$lawyer->name)->send(new FileUploadInvitation($invitation,['type'=>'lawyer']));			
		}
		public function patient_not_accepted_invitation_notification_to_lawyer($invitation)
		{
			$lawyer = $invitation->user;
			Mail::to($lawyer->email,$lawyer->name)->send(new PatientInvitationAcceptence($invitation,['type'=>'lawyer']));
		}
		public function patient_not_accepted_invitation_notification_to_doctor($invitation)
		{
			$doctor = $invitation->user1;
			Mail::to($doctor->email,$doctor->name)->send(new PatientInvitationAcceptence($invitation,['type'=>'doctor']));	
		}
		public function send_unread_message_notification($user,$sender)
		{	
			Mail::to($user->email,$user->name)->send(new ChatNotificationMail($user,$sender));	
		}
		public function send_invoice_to_lawyer($invitation)
		{	
				$email="showmacktest@gmail.com";
				//$email="test@hcmg.com";
				$name="admin";
				Mail::to($email,$name)->send(new Invoice($invitation,['type'=>'doctor']));		
		}
		public function send_invoice_admin_to_lawyer($invitation)
		{	
				$lawyer = $invitation->user;
				Mail::to($lawyer->email,$lawyer->name)->send(new Invoice($invitation,['type'=>'admin']));		
		}
		public function sent_otp_to_lawyer($email,$name,$otp)
		{	
				Mail::to($email,$name)->send(new Loginotp($email,$name,$otp));		
		}
		
		public function send_notification_email_to_doctor_after_lawyer_cancel_appointment($invitation)
		{
			$doctor = $invitation->user1;
			Mail::to($doctor->email,$doctor->name)->send(new LawyerCancelAppointment($invitation,['type'=>'doctor']));
		}
		public function send_notification_email_to_patient_after_lawyer_cancel_appointment($invitation)
		{
			Mail::to(Helper::Decrypt($invitation->patient->email), Helper::Decrypt($invitation->patient->email))->send(new LawyerCancelAppointment($invitation,['type'=>'patient']));
		}
		public function send_account_activation_mail_to_doctor($user){
			Mail::to($user->email, $user->name)->send(new AuthenticationMail($user,['type'=>'doctor']));
		}
		public function send_account_activation_mail_to_lawyer($user){
			 Mail::to($user->email, $user->name)->send(new AuthenticationMail($user,['type'=>'lawyer']));
		}
		public function send_reset_password_mail($user){
			Mail::to($user->email, $user->name)->send(new ResetPasswordMail($user,['type'=>'user']));
		}
		public function send_account_activation_after_mail_to_lawyer($user){
			 Mail::to($user->email, $user->name)->send(new AuthenticationNotificationMail($user,['type'=>'lawyer']));
	    }
		public function send_approve_notification_to_lawyer($user){
			Mail::to($user->email, $user->name)->send(new LawyerAuthenticationStatusChange($user,['type'=>'approve']));
	    }
		
		public function send_decline_notification_to_lawyer($user){
			Mail::to($user->email, $user->name)->send(new LawyerAuthenticationStatusChange($user,['type'=>'decline']));
	    }

		public function sendPatientAccountCreatedEmail($data){
			Mail::to($data['username'], $data['patient_name'])->send(new PatientAccountCreated($data));
	    }

		public function sendDeletePatientEmail($data){
			Mail::to($data['email'], $data['name'])->send(new DeletePatientAccountMail($data));
	    }
}
?>