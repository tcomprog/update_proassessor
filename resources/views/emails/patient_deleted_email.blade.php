@extends("includes.email_container")
@section("content")
<div>

        <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Dear {{ucfirst($name)}},</h3>
          <p style="font-size: 14px;line-height: 17px;">Your account has been deleted by your lawyer. You will no longer be able to use your account or login into the system.</p> 
        <br/>    

        <br/>

        <br/>

        <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
        <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 

</div>  

@endsection