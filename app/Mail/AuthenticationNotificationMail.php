<?php

namespace App\Mail;

use App\Models\user_invitations;
use App\Models\User;

use Config;

use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

class AuthenticationNotificationMail extends Mailable

{

    use Queueable, SerializesModels;

    /**

     * The order instance.

     *

     * @var \App\Models\Order

     */

    public $user;

    public $options = [];

    /**

     * Create a new message instance.

     *

     * @param  \App\Models\Order  $order

     * @return void

     */

    public function __construct(User $user,$options)

    {

        $this->user = $user;

        $this->options = $options;

    }

    /**

     * Build the message.

     *

     * @return $this

     */

    public function build()



    {



        $fromAddress = Config::get('constants.MAIL_FROM_ADDRESS');



        $site_title_old = Config::get('constants.site_title');
        $site_title=str_replace("-"," ",$site_title_old);

		
        /*if(isset($this->options['type']) && $this->options['type'] == 'doctor') {

            return $this->subject($site_title.' - Thank You for Registration. Activate your Email.')->from($fromAddress,$site_title)->view('emails.send_account_activation_mail_to_doctor',['user'=>$this->user,'site_title'=>$site_title]);

        }*/

        if(isset($this->options['type']) && $this->options['type'] == 'lawyer') {    
            $activation_link = url('activate_account').'/'.$this->user->email_verify_token;    
            // echo $activation_link;exit;
            return $this->subject($site_title.' - Thank you for registration.')->from($fromAddress,$site_title)->view('emails.send_account_activation_after_mail_to_lawyer',['user'=>$this->user,'site_title'=>$site_title]);

        }


    }

}