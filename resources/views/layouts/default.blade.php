<!doctype html>
<html>
<head>
    @include('includes.nhead')
</head>
<body>
    <div class="n_wraper-container">
        <div class="n-top-container">
              @yield('content')
        </div>

      <div class="n-bottom-container">
          @include('includes.nfooter')
      </div>

   </div>

    @include('includes.model')
    @include('includes.scripts')
</body>
</html>



<script>
    // ******************************* OPT Section ***********************************
$(document).ready(function(){
      $('.digit_groupss').find('input').each(function() {
        console.log("Working here")
      $(this).attr('maxlength', 1);
      $(this).on('keyup', function(e) {
        var parent = $($(this).parent());
        
        if(e.keyCode === 8 || e.keyCode === 37) {
          var prev = parent.find('input#' + $(this).data('previous'));
          
          if(prev.length) {
            $(prev).select();
          }
        } else if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
          var next = parent.find('input#' + $(this).data('next'));
          console.log(next.length)
          if(next.length) {
            $(next).select();
          } else {
            //submit the here
            submitOTP();
          }
        }
      });
    });
});
// ******************************* OPT Section ***********************************
</script>