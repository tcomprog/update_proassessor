@extends("includes.email_container")
@section("content")
            <div>

                  <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Hello {{{ ucfirst($invitation->user1->name) }}},</h3>

                    <p style="font-size: 14px;line-height: 17px;">{{ ucfirst($invitation->patient->name) }} has accepted the appointment date/time : {{ date('d/M/Y', strtotime($invitation->invitation_date)) }}/{{ $invitation->timeslot }}</p>           

                   

                    <br/>

                    <br/>

                    <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
                    <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 

                    </div>  

               </div>

            </div>

 @endsection