<?php

namespace App\Mail;

use App\Models\user_invitations;
use App\Models\User;

use Config;

use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

class ResetPasswordMail extends Mailable

{

    use Queueable, SerializesModels;

    /**

     * The order instance.

     *

     * @var \App\Models\Order

     */

    public $user;

    public $options = [];

    /**

     * Create a new message instance.

     *

     * @param  \App\Models\Order  $order

     * @return void

     */

    public function __construct(User $user,$options)

    {

        $this->user = $user;

        $this->options = $options;

    }

    /**

     * Build the message.

     *

     * @return $this

     */

    public function build()



    {



        $fromAddress = Config::get('constants.MAIL_FROM_ADDRESS');



        $site_title_old = Config::get('constants.site_title');
        $site_title=str_replace("-"," ",$site_title_old);

		
        if(isset($this->options['type']) && $this->options['type'] == 'user') {

            return $this->subject($site_title.' - Reset Password.')->from($fromAddress,$site_title)->view('emails.send_reset_password_mail',['user'=>$this->user,'site_title'=>$site_title]);

        }


    }

}