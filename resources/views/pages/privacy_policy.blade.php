@extends('layouts.inner')
@section('title', 'About us')
@section('content')

<!-- Process Section -->
<link href="{{ asset('css/home.css') }}" rel="stylesheet">

<style>

.about_sec img{

    width: 100%;

    border-radius: 85px 0px 85px 85px;

    max-width: 465px;

    float: left;

    margin-right: 15px;

    margin-top: 8px;

}</style>

<section class="about_sec">

    <div class="container">    

           <section id="privacy_policy_section" class="privacy_section container mb-5  p-4">
               <div class="row">
                <div class="col-12">
                    
                   <h1 class="textStyle" style="margin:0px auto;">
                       <h1  class="typewrite textStyle" data-period="2000" data-type='["Privacy policy"]'>
                         <span class="wrap"></span>
                       </h1>
                     </h1>
                    <?php echo"$pageuser->content"; ?>
            
                </div>
               </div>
            </section>

    </div>

</section>

<!-- Process Section -->

@endsection

