<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class ExposeMiddleware
{
    public function handle($request, Closure $next)
    {
        $filters = new \Expose\FilterCollection();
        $filters->load();
        $logger = App::make('log');
       
        $manager = new \Expose\Manager($filters, $logger);
        $manager->setThreshold(8);
        $manager->run($request->input());
        $impacto = $manager->getImpact();

        if ( $impacto > 0) {
            Log::info(sprintf("PHP IDS punctuation: %d", $manager->getImpact()));
            $reports = $manager->getReports();
            $reports = str_replace('|', '
            ',$manager->export());
            $txt = "PHP IDS punctuation: ".$manager->getImpact().'
            '.$reports;
            // //log to database ONLY YOU HAVE
            // $logdb = DB::connection('logsys')
            // ->table('roster')
            // ->insert(['oc' => $txt, 'user_id' => 2, 'flag' => 2, 'ip' => $_SERVER['REMOTE_ADDR']]);
        }
        //Mataburro: if impact more than 20, kill the donkey...
        if ($impacto > 20) {
            die('The request was processed as dangerous to the system.');
        }

        return $next($request);
    }
}