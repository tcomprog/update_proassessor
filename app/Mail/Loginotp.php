<?php

namespace App\Mail;

use App\Models\user_invitations;
use App\Models\User;

use Config;

use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

class Loginotp extends Mailable

{

    use Queueable, SerializesModels;

    /**

     * The order instance.

     *

     * @var \App\Models\Order

     */

    public $user;

    public $options = [];

    /**

     * Create a new message instance.

     *

     * @param  \App\Models\Order  $order

     * @return void

     */

    public function __construct($email,$name,$otp)

    {
        $this->name = $name;
        $this->email = $email;
        $this->otp = $otp;
    }

    /**

     * Build the message.

     *

     * @return $this

     */

    public function build()
    {
        $fromAddress = Config::get('constants.MAIL_FROM_ADDRESS');
        $site_title_old = Config::get('constants.site_title');
        $site_title=str_replace("-"," ",$site_title_old);

        return $this->subject($site_title.' - Verification code for login')->from($fromAddress,$site_title)->view('emails.send_otp_to_lawyer',['name'=>$this->name,'otp'=>$this->otp,'site_title'=>$site_title]);
    }

}