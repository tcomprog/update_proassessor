<style>
  .pccs:hover{
    cursor: pointer;
  }
  .title_font_size{
    font-size: 3rem;
  }

  @media only screen and (max-width: 480px) {
    .title_font_size{
    font-size: 1.5rem;
  }
}

</style>

<!-- Banner Section -->
<section class="intro_banner_sec">
    <div id="demo" class="carousel slide" data-ride="carousel">
      <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
      </ul>

      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="{{ asset('img/slider1.jpg') }}" alt="Slider-1">
          
          <div class="carousel-caption" style="  position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%); width:90%;">
            <div class="row">

              <div class="col-12">
                <div style="margin-top:15px;background: #30CFD099; padding: 10px 20px;border-radius: 10px;text-align: center;">
                  <h1 class="title_font_size" >Welcome To Pro Assessors</h1>
                  <h3 style="font-size: 1.2rem;">The platform to request & process med-legal assessment</h3>
                </div>
                </div>
                {{-- <div class="col-md-7">
                    <div class="welcome_text">
                        <h2>Welcome to</h2>
                        <h1>Pro Assessors</h1>
                        <a href="javascript:void(0);" class="more_btn" style="max-width: 100% !important;">The Platform to request & process Med-legal Assessment</a>
                    </div>
                </div> --}}
            </div>
          </div>   
        </div>

        <div class="carousel-item">
          <img src="{{ asset('img/slider2.jpg') }}" alt="Slider-2">
          <div class="carousel-caption" style="  position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%); width:90%;">
                <div class="row">
                  <div class="col-12">
                    <div style="margin-top:15px;background: #4B587B99; padding: 10px 20px;border-radius: 10px;text-align: center;">
                      <h1 class="title_font_size" >Welcome to Pro Assessors</h1>
                      <h3 style="font-size: 1.2rem;">The platform to request & process med-legal assessment</h3>
                    </div>
                </div>
                    {{-- <div class="col-md-7">
                        <div class="welcome_text">
                            <h2>Welcome to</h2>
                            <h1>Pro Assessors</h1>
                            
                            <a href="javascript:void(0);" class="more_btn" style="max-width: 100% !important;">The Platform to request & process Med-legal Assessment</a>
                        </div>
                    </div> --}}
                </div>
          </div>   
        </div>
      </div>

      <a class="carousel-control-prev pccs" data-target="#demo" data-slide-to="0">
        <span class="carousel-control-prev-icon"></span>
      </a>
      <a class="carousel-control-next pccs" data-target="#demo" data-slide-to="1">
        <span class="carousel-control-next-icon"></span>
      </a>
      
    </div>
</section>
<!-- Banner Section -->


