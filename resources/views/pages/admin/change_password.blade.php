@extends('layouts.admin_app')

@section('content')
<div class="container">
	
	<div class="row">
		<div class="col-md-12">
			 <nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
				<li class="breadcrumb-item active"><a href="{{url('/admin/change-password')}}">Change Password</a></li>
			  </ol>
			</nav>
		</div>
    </div>

    <div class="row justify-content-center">

        <div class="col-md-8">

            <div class="card">

                <div class="card-header">{{ __('Change Password') }}</div>



                <div class="card-body">

                @if(Session::has('message'))

                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>

                @endif

                    <form method="POST" action="{{ url('admin/change-password') }}">

                        @csrf

                        <div class="form-group row">

                            <label for="current_password" class="col-md-4 col-form-label text-md-right">{{ __('Old Password') }}</label>

                            <div class="col-md-6">

                                <input id="current_password" type="password" class="form-control @error('current_password') is-invalid @enderror"` name="current_password" required autocomplete="new-password">

                                @error('current_password')

                                    <span class="invalid-feedback" role="alert">

                                        <strong>{{ $message }}</strong>

                                    </span>

                                @enderror

                            </div>

                        </div>

                        <div class="form-group row">

                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>



                            <div class="col-md-6">

                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">



                                @error('password')

                                    <span class="invalid-feedback" role="alert">

                                        <strong>{{ $message }}</strong>

                                    </span>

                                @enderror

                            </div>

                        </div>



                        <div class="form-group row">

                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>



                            <div class="col-md-6">

                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

                            </div>

                        </div>



                        <div class="form-group row mb-0">

                            <div class="col-md-6 offset-md-4">

                                <button type="submit" class="btn btn-primary">

                                    {{ __('Change Password') }}

                                </button>

                            </div>

                        </div>

                    </form>

                </div>

            </div>

        </div>

    </div>

</div>
@endsection
