<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class user_invitations extends Model
{
    use HasFactory, Notifiable;
    protected $guarded = [];
	/*Lawyer belongs to invitations*/
	public function user(){
        return $this->hasOne(User::class, 'id', 'inviter_user_id');
    }
	public function doctor(){
        return $this->hasOne(User::class, 'id', 'invitee_user_id');
    }
    public function lawyer_category(){
        return $this->belongsTo('App\Models\Category', 'id', 'inviter_user_id');
    }
    public function category(){
        return $this->belongsTo('App\Models\Category','invitee_category_id');
    }
	/*Doctor belongs to invitations*/
	public function user1(){
        return $this->hasOne(User::class, 'id', 'invitee_user_id');
    }
    public function patient(){
        return $this->hasOne(Patient::class, 'id', 'patient_user_id');
    }
	public function attachments(){        
		return $this->hasOne(InvitationAttachment::class, 'invitation_id', 'id');
    }
    //all invoices
	public function invoices(){        
		return $this->hasOne(Invites_Payments::class, 'invitation_id', 'id');
    }
}
