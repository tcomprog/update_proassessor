
   <style>
         .invoicebody .clearfix:after {
         content: "";
         display: table;
         clear: both;
         }
         .invoicebody body{
         position: relative;
         width: 21cm;  
         height: 29.7cm; 
         margin: 0 auto; 
         color: #555555;
         background: #FFFFFF;
         font-family: Arial, sans-serif; 
         font-size: 14px; 
         font-family: 'Source Sans Pro', sans-serif;
         }
         .invoicebody .header {
         padding: 10px 0;
         margin-bottom: 20px;
         border-bottom: 1px solid #AAAAAA;
         }
         .invoicebody #logo {
         float: none;
         margin-top: 8px;
         text-align: center;
         }
         .invoicebody #logo img {
         height: 70px;
         }
         .invoicebody #company {
         float: right;
         text-align: right;
         }
         .invoicebody #details {
         margin-bottom: 30px;
         }
         .invoicebody #client {
         padding-left: 6px;
         border-left: 6px solid #00528c;
         float: left;
         }
         .invoicebody #client .to {
         color: #777777;
         }
         .invoicebody h2.name {
         font-size: 1.5em;
         font-weight: normal;
         margin: 0;
         margin-bottom: 5px;
         color: #000000;
         }
         .invoicebody #client .address{
          margin-bottom: 5px;
          font-size: 1.1em;
          color: #000000;
         }
         .invoicebody #client .email{
          margin-bottom: 5px;
          font-size: 1.1em;
         }
         .invoicebody #invoice {
         float: right;
         text-align: right;
         }
         .invoicebody #invoice h1 {
         color: #0087C3;
         font-size: 2.4em;
         line-height: 1em;
         font-weight: normal;
         margin: 0  0 10px 0;
         }
         .invoicebody #invoice .date {
         font-size: 1.1em;
         color: #000000;
         }
         .invoicebody table {
         width: 100%;
         border-collapse: collapse;
         border-spacing: 0;
         margin-bottom: 20px;
         }
         .invoicebody table th,
         .invoicebody table td {
         padding: 20px;
         background: #ffffff;
         text-align: center;
         border-bottom: 1px solid #ddd;
         border-right: 1px solid #ddd;
         border-left: 1px solid #ddd;
         }
         .invoicebody table th {
         white-space: nowrap;        
         font-weight: normal;
         }
         .invoicebody table h4{
          margin: 0px 0px 5px;
         }
         .invoicebody table td {
         }
         .invoicebody table td h3{
         color: #000;
         font-size: 1.2em;
         font-weight: normal;
         margin: 0 0 0.2em 0;
         }
         .invoicebody table .heading{
          color: #fff !important;
          font-size: 1.2em;
          background: #004b82;
         }
         .invoicebody table .no {
         color: #000;
         font-size: 1.2em;
         text-align: left;
         }
         .invoicebody table .desc {
         text-align: left;
         }
         .invoicebody table .unit {
          color: #000;
         }
         .invoicebody table .qty {
          color: #000;
         }
         .invoicebody table .total {
         background: #57B223;
         color: #FFFFFF;
         }
         .invoicebody table td.unit,
         .invoicebody table td.qty,
         .invoicebody table td.total {
         font-size: 1.2em;
         text-align: center;
         }
         .invoicebody table tfoot td {
         padding: 10px 20px;
         background: #FFFFFF;
         font-size: 1.2em;
         white-space: nowrap; 
         border-top: 1px solid #AAAAAA; 
         }
         .invoicebody table tfoot tr:first-child td {
         border-top: none; 
         }
         .invoicebody table tfoot tr:last-child td {
         color: #000;
         font-size: 1.4em;
         border-top: 1px solid #57B223; 
         font-weight: 600;
         }
         .invoicebody table tfoot tr td:first-child {
         border: none;
         }
         .invoicebody #thanks{
         font-size: 2em;
         margin-bottom: 50px;
         }
         .invoicebody #notices{
         padding-left: 6px;
         border-left: 6px solid #0087C3;  
         }
         .invoicebody #notices .notice {
         font-size: 1.2em;
         }
         .invoicebody .footer {
         color: #777777;
         width: 100%;
         height: 30px;
         position: absolute;
         bottom: 0;
         border-top: 1px solid #AAAAAA;
         padding: 8px 0;
         text-align: center;
         }
		 .cataddress{max-width:none;}
      </style>
   <div class="invoicebody">
      <div class="clearfix header">
         <div id="logo">
            <img src="{{ asset('img/logo.png') }}">
         </div>
      </div>
      <div>
         <div id="details" class="clearfix">
            <div id="client">
               <h2 class="name">{{ $invitation->user1->name }}</h2>
               <div class="address cataddress"> <?php  $category = Helper::categorydetails();  
									    foreach ($category as $categories){ 
											if(($categories->id)==($invitation->user1->doctor->category_id)){
												echo ucfirst($categories->category_name); }
											} ?>: {{ $invitation->user1->doctor->licence_number }}</div>										
										
               <div class="email">PH: <a href="tel:{{ $invitation->user1->phone }}">{{ $invitation->user1->phone }}</a></div>
            </div>
            <div id="invoice">
            <h2 class="name"><?php if(($doctor_invoice_number==null)&&(!isset($invitation->invoices))){ echo "Invoice #01";}
            elseif(($doctor_invoice_number!=null)){
               if(isset($invitation->invoices)&&($invitation->invoices->doctor_invoice_number!=null)){
               echo "Invoice #".str_pad($doctor_invoice_number, 2, '0', STR_PAD_LEFT);}
               else{
                  $new_doctor_invoice_number=$doctor_invoice_number+1;
                  echo "Invoice #".str_pad($new_doctor_invoice_number, 2, '0', STR_PAD_LEFT);}
               } ?></h2>
               <h2 class="name">Bill to: Pro Assessors</h2>
               <div class="date">Invoice Date: <?php $date=date("Y-m-d"); echo date('F d, Y', strtotime($date)); ?></div>
            </div>
         </div>
         <table border="0" cellspacing="0" cellpadding="0">
            <thead>
               <tr>
                  <th class="no heading">PATIENT</th>
                  <th class="desc heading">SERVICE PROVIDED</th>
                  <th class="unit heading">SERVICE DATE</th>
                  <th class="qty heading">FEE</th>
               </tr>
            </thead>
            <tbody>
              <!-- ROW -->
               <tr>
                  <td class="no">
                    <h4>{{ $invitation->patient->name}}</h4>
                    <span style="color: #888;">Date of Birth:</span> <?php echo date('F d, Y', strtotime($invitation->patient->dateofbirth)); ?><br/>
                    <span style="color: #888;">Date of Accident:</span> <?php echo date('F d, Y', strtotime($invitation->patient->dateofaccident)); ?>
                  </td>
                  <td class="desc">
                     <h3><span class="new_service_provided"></span></h3>
                  </td>
                  <td class="unit"><?php echo date('F d, Y', strtotime($invitation->invitation_date)); ?></td>
                  <td class="qty">$<span class="new_bill_amt"><?php if(isset($invitation->invoices)&&($invitation->invoices->payment_amount!=null)){echo $invitation->invoices->payment_amount;} ?></span></td>
               </tr>
               <!-- ROW -->
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="2"></td>
                  <td colspan="1">TOTAL</td>
                  <td>$<span class="new_bill_amt" ></span></td>
               </tr>
               <?php if($invitation->user1->doctor->hst=='yes') { ?>
               <tr>
                  <td colspan="2"></td>
                  <td colspan="1">HST</td>
                  <td>$<span id="hst" ></span></td>
               </tr>
               <tr>
                  <td colspan="2"></td>
                  <td colspan="1">SUB TOTAL</td>
                  <td>$<span id="subtotal" ></span></td>
               </tr>
               <?php  } ?>
            </tfoot>
         </table>
         <img style="display: none" id="sendInvoiceLoading" width="50" src="{{asset('img/loading.gif')}}" alt="">
			<button type="submit" class="addpatient_btn px-3"  id="sendinvoice_btn"> Send Invoice</button>		
		   <button  class="addpatient_btn"  class="close" data-dismiss="modal" aria-label="Close"> Cancel</button>
         </div>      
   </div>


