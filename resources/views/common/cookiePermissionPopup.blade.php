<div id='fullScreencontrol'></div>

<div id="mySidenav" class="sidenav">
   
    <h4 class="offcanvas-title" id="offcanvasBottomLabel">We value your privacy</h4>

    <div class="row" id="cookiecontent" style="display: none">
        <div class="col-md-9 col-sm-12">
             <p style="font-size: 13px">
               We use cookies to enhance your browsing experience, serve personalized ads or content,
               and enalyze our traffic. By clicking "Accept All" you consent to our use of cookie.
               <a style="font-weight: bold; color:#0D6EFD" href="privacy_policy">Read More</a>
             </p>
        </div>

        <div class="col-md-3 col-sm-12 btnsDesing" >
           <button onclick="closeCookie()" type="button" class="accept_all_outline">Reject All</button>
           <button onclick="acceptCookie()" type="button" class="accept_all ">Accept All</button>
        </div>
       
      </div>

</div>