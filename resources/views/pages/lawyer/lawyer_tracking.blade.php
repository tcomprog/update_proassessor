<!doctype html>
<html>
<head>
    @include('includes.head')

    <style>
        @font-face {
            font-family: LoraBold;
            src: url("{{asset('fonts/Lora-Bold.ttf')}}");
        }
        @font-face {
            font-family: LoraRegular;
            src: url("{{asset('fonts/Lora-Regular.ttf')}}");
        }
    </style>

    <link href="{{ asset('css/patient_details.css') }}" rel="stylesheet">
</head>
<body>
    <div style="display: flex; flex-direction:column;height: 100vh;">
    <header>
        <div class="container">
            <div class="row">
                @include('includes.header')
            </div>
        </div>
    </header>
        <!-- Banner Section -->
        <section class="intro_banner_sec inner_banner_sec" style="padding-top:20px; padding-bottom:20px;">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="welcome_text">
                            <h1>Tracking</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Banner Section -->

                  <div class="mainTrackingcontainer" style="margin:70px 0px;text-align:center">
                        @if($status == '1')
                        <img style="display: none;" id="loading_msg" width="100" src="{{asset('img/loading.gif')}}" />

                        <div  id="content11">
                        </div>

                        @else
                            <h1>Invalid access</h1>
                        @endif
                  </div>
   
<br><br>
        {{-- <div class="container my-5" style="flex: 1;">
            <div class="row">
                <div class="col-12">
                    @if($status == '1')
                    <img style="display: none;" id="loading_msg" width="100" src="{{asset('img/loading.gif')}}" />

                    <div  id="content11">
                    </div>

                    @else
                        <h1>Invalid access</h1>
                    @endif
                </div>
            </div>
        </div> --}}

        <footer style="margin-top:auto; padding-bottom:0px; padding-top:25px;">
        @include('includes.footer')
    </footer>
    @include('includes.scripts')

</div>
</body>
</html>

@if($status == '1')
<script>
openModal("{{$reportID}}");
function openModal(id){

		$.ajax({
        url:"{{ url('patient/tracking') }}",
        data:{'id':id,'_token':'{{ csrf_token() }}'},
        type:'post',
        dataType:'json',
		beforeSend:function(){
			$("#content11").empty();
		    $("#closeButton").hide(); 
		    $("#loading_msg").show(); 
	    	$("#tracability").modal('show');
		},
        success:function(res) {
            $()

            let firstLine = `<div class="trackingContainer">`;
            let secondLine = `<div class="trackingContainer">`;
            let thirdLine = `<div class="trackingContainer">`;
            
            let data = res.data;

            let len = data.length;
            let width = 12.5;
            // if(len == 1 || len == 2){
            //     // width => 30%
            //     width = 30;
            // }
            // else if(len == 3 || len == 4){
            //     // width => 20%
            //     width = 20;
            // }
            // else if(len == 5 || len == 6){
            //     // width => 15.5%
            //     width = 15.5;
            // }
            // else{
            //     // 100 / len => width
            //     width = 12.4;
            // }

            width = 12.4;

            if(data[0] != undefined){
               let it_0 = data[0];
               var dd = new Date(it_0["date"]);
               dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes();
               firstLine += ` <div class="s_section">
                             <div class="circles" style="background: #C1CDE3;"> <h1>1</h1> </div>
                             <div class="st-line" style="background: #C1CDE3;"></div>
                        </div>`;
               secondLine += `<div class="s_section bx-1"> <div class="tooltip-top-1" style="border-top-left-radius: 6px;border-bottom-left-radius: 6px;"></div></div>`;
               thirdLine += `<div class="s_section pt-4">
                                <h5 class="dateP" style="color:#6688C8;margin-bottom:8px">`+dd+`</h5>
                                <h4 style="color: #6688C8;">Assessment Requested</h4>
                                <p>( Waiting for Dr. Confirmation )</p>
                            </div>`;
            }
            
            if(data[1] != undefined){
                let it_1 = data[1];
                var dd = new Date(it_1["date"]);
               dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes();
               firstLine += ` <div class="s_section pb-4">
                            <h4 style="color: #3D5AF1;">The assessment was confirmed by the Doctor</h4>
                            <p>( waiting for the patient's confirmation)</p>
                            <h5 class="dateP" style="color:#3D5AF1; margin-bottom:0px; margin-top:8px;">`+dd+`</h5>
                        </div>`;
               secondLine += ` <div class="s_section"><div class="tooltip-bottom-1"></div></div>`;
               thirdLine += ` <div class="s_section">
                                <div class="st-line" style="background: #3D5AF1;"></div>
                                <div class="circles" style="background: #3D5AF1;"> <h1>2</h1> </div>
                            </div>`;
            }

            if(data[2] != undefined){
                let it_2 = data[2];
                var dd = new Date(it_2["date"]);
               dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes();
                firstLine += ` <div class="s_section">
                            <div class="circles" style="background: #E2767A;"> <h1>3</h1> </div>
                            <div class="st-line" style="background: #E2767A;"></div>
                        </div>`;
               secondLine += ` <div class="s_section"><div class="tooltip-top-2"></div></div>`;
               thirdLine += `  <div class="s_section pt-4">
                                <h5 class="dateP" style="color:#E2767A">`+dd+`</h5>
                                <h4 style="color: #E2767A;">Assessment booked</h4>
                                <p>(Patient confirmed the booking, waiting for the assessment to be completed)</p>
                            </div>`;
            }

            if(data[3] != undefined){
                let it_3 = data[3];
                var dd = new Date(it_3["date"]);
                dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes(); 
                firstLine += `   <div class="s_section pb-4">
                            <h4  style="color: #8DD0C0;">Assessment completed</h4>
                            <h5 class="dateP" style="color:#8DD0C0; margin-bottom:0px; margin-top:8px;">`+dd+`</h5>
                            <p></p>
                        </div>`;
               secondLine += ` <div class="s_section"><div class="tooltip-bottom-2"></div></div>`;
               thirdLine += `   <div class="s_section">
                                <div class="st-line" style="background: #8DD0C0;"></div>
                                <div class="circles" style="background: #8DD0C0;"> <h1>4</h1> </div>
                            </div>`;
            }

            if(data[4] != undefined){
                let it_4 = data[4];
                var dd = new Date(it_4["date"]);
                dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes();
                firstLine += `<div class="s_section">
                            <div class="circles" style="background: #97A0C2;"> <h1>5</h1> </div>
                            <div class="st-line" style="background: #97A0C2;"></div>
                        </div>`;
               secondLine += `<div class="s_section"><div class="tooltip-top-3"></div></div>`;
               thirdLine += ` <div class="s_section pt-4">
                             <h5 class="dateP" style="color:#97A0C2; margin-bottom:8px">`+dd+`</h5>

                                <h4 style="color: #97A0C2;">Report in progress</h4>
                                <p></p>
                            </div>`;
            }

            if(data[5] != undefined){
                let it_5 = data[5];
                var dd = new Date(it_5["date"]);
                dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes();
                firstLine += `  <div class="s_section pb-4">
                            <h4  style="color: #5D9FFF;">Report submitted by the doctor</h4>
                            <h5 class="dateP" style="color:#5D9FFF; margin-bottom:0px; margin-top:8px;">`+dd+`</h5>
                            <p></p>
                        </div>`;
               secondLine += `<div class="s_section"><div class="tooltip-bottom-3"></div></div>`;
               thirdLine += `  <div class="s_section">
                                <div class="st-line" style="background: #5D9FFF;"></div>
                                <div class="circles" style="background: #5D9FFF; "> <h1>6</h1> </div>
                            </div>`;
            }

            if(data[6] != undefined){
                let it_6 = data[6];
                var dd = new Date(it_6["date"]);
                dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes();
                firstLine += `<div class="s_section">
                            <div class="circles" style="background: #9BCF5D;"> <h1>7</h1> </div>
                            <div class="st-line" style="background: #9BCF5D;"></div>
                        </div>`;
               secondLine += `  <div class="s_section"><div class="tooltip-top-4"></div></div>`;
               thirdLine += `   <div class="s_section pt-4">
                                <h5 class="dateP" style="color:#9BCF5D; margin-bottom:8px">`+dd+`</h5>

                                <h4 style="color: #9BCF5D;">Report Being reviewed</h4>
                                <p></p>
                            </div>`;
            }

            if(data[7] != undefined){
                let it_7 = data[7];
                var dd = new Date(it_7["date"]);
                dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes();
                firstLine += `   <div class="s_section pb-4">
                            <h4  style="color: #BB5A77;">The report was Accepted by the lawyer</h4>
                            <h5 class="dateP" style="color:#BB5A77; margin-bottom:0px; margin-top:8px;">`+dd+`</h5>
                            <p></p>
                        </div>`;
               secondLine += `<div class="s_section"><div class="tooltip-bottom-4"></div></div>`;
               thirdLine += `  <div class="s_section">
                                <div class="st-line" style="background: #BB5A77;"></div>
                                <div class="circles" style="background: #BB5A77;"> <h1>8</h1> </div>
                            </div>`;
            }

            firstLine += `</div>`;
            secondLine += `</div>`;
            thirdLine += `</div>`;

            let nhtml = firstLine+secondLine+thirdLine;
          
            $(".mainTrackingcontainer").append(nhtml);
            $("#loading_msg").hide();

            var cols = document.getElementsByClassName('s_section');
            for(i = 0; i < cols.length; i++) {
                cols[i].style.width = width+'%';
            }


			$("#content11").append(html);
		    $("#closeButton").show(); 
		    $("#loading_msg").hide(); 
        },
		error:function(xhr,status,error){
			console.log('xhr => ',xhr," status => ",status," error => ",error)
		}
    });

	}
</script>
@endif