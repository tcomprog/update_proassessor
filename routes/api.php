<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('clear-cache', function () {
    $run = Artisan::call('config:clear');
    $run = Artisan::call('cache:clear');
    $run = Artisan::call('config:cache');
    $route = Artisan::call('route:clear');
    // $route = Artisan::call('schedule:run');
    return 'FINISHED';
}); 

Route::get('/test-mail',function(){
        Mail::send([],[], function ($message) {
        $message->subject('test Mail from HCMG');
        $message->to('balu.chandar@webixion.com','Balu chand');
        $message->setBody('Test message', 'text/html');
        });
        echo "sent";
    });