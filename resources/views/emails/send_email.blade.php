@extends("includes.email_container")
@section("content")
            <div>
                  <br>

                     <p>
                        {{$content}}
                     </p>

                    <br/>
                    <br/>
                    <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
                    <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 
                    </div>  
               </div>
            </div>
  @endsection