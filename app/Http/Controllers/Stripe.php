<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pagecms;
use App\Models\Sitedetails;
use App\Models\AdminSettings;
use App\Models\ClinicPackages;
use App\Models\ClientPayment;
use App\Models\ClinicSpecialOffer;
use App\Models\User;
use App\Models\ClientSubscription;
use App\Mail\SubscriptionConfirmation;
use App\Mail\ExpirarySubscriptionWarning;
use Auth;
use Session;
use Mail;


class Stripe extends Controller
{
    public function index(){
        $package_type = "";
        $expire_date = "";
        $exp_status = "1";

        $user_id = Auth::id();   
        $subscription = ClientSubscription::where('user_id',$user_id)->get();

        if(count($subscription) > 0){
          $subscription = $subscription[0];

          if($subscription->status == 1){
             return redirect()->route('clinicDashboard');
          }
          elseif($subscription->status == 2){
            $package_type = $subscription->package;
            $expire_date = date("d/m/Y",strtotime($subscription->to_date));
            $exp_status = '2';
          }
        }

        $page_slug="subscritpion";
        $pageuser = Pagecms::where('page_slug',$page_slug)->first();     
        $pagedetails = Sitedetails::find(1);
        $setting_about_image = AdminSettings::where('settings_name','aboutus_image')->first();
    
        $monthly = ClinicPackages::where('name','monthly')->first();
        $yearly = ClinicPackages::where('name','yearly')->first();

        $offer = ClinicSpecialOffer::where('clinic_id',Auth::id())->get();

        if(count($offer) > 0){
            $offer = $offer[0];
            if($offer->monthly > 0){
                $monthly->amount = $offer->monthly;
            }
    
            if($offer->yearly > 0){
                $yearly->amount = $offer->yearly;
            }
        }
    
    
        return view('pages.clinic.subscription', [
           'pageuser' => $pageuser,
           'pagedetails' => $pagedetails,
           'monthly' => $monthly,
           'yearly' => $yearly,
           'package_type' => $package_type,
           'expire_date' => $expire_date,
           'exp_status' => $exp_status,
           ]); 
    }

    public function subsctionCheckoutPage(Request $request){
        $page_slug="subscritpion";
        $pageuser = Pagecms::where('page_slug',$page_slug)->first();     
        $pagedetails = Sitedetails::find(1);
        $setting_about_image = AdminSettings::where('settings_name','aboutus_image')->first();
    
        if($request->package == 'monthly' or $request->package == "yearly"){
          
            return view('pages.clinic.paymentCheckoutPage', [
                'pageuser' => $pageuser,
                'pagedetails' => $pagedetails,
                'package'=>$request->package,
                'amount'=>$request->amount
             ]); 
        }
        else{
            return view("error");
        }
    }


    public function intStripUi(Request $request){
         $user = Auth::user();   
         \Stripe\Stripe::setApiKey(env("SECRET_KEY"));
         try {
          // retrieve JSON from POST body
          $jsonStr = file_get_contents('php://input');
          $jsonObj = json_decode($jsonStr);
      
          // Create a PaymentIntent with amount and currency
          $paymentIntent = \Stripe\PaymentIntent::create([
              'amount' => $request->amount."00",
              'currency' => 'cad',
              'metadata' => ['clinic_id'=>$user->id,'clinic_email'=>$user->email,'package'=>$request->package,'date'=>date('m/d/Y h:i:s a', time())],
              'receipt_email' => $user->email,
              'automatic_payment_methods' => ['enabled' => true],
              'description' => ucfirst($request->package).' package purchased by ID: '.$user->id.' on date: '.date('d-m-Y'),
          ]);
      
          $output = [
              'clientSecret' => $paymentIntent->client_secret,
          ];
      
          echo json_encode($output);
          } catch (Error $e) {
              http_response_code(500);
              echo json_encode(['error' => $e->getMessage()]);
          }
       //  print_r($request->all());
      }

      public function complete_subscr(){
        Session::flush();
        Auth::logout();
        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0"); // Proxies.
        session()->flash('logout-alert-success', 'Success logged out');

        if(isset($_GET["payment_intent"]) && isset($_GET["redirect_status"]) && $_GET["redirect_status"] == "succeeded"){
           return view("pages.clinic.complete_payment",['status'=>'completed']);
        }
        else{
           return view("pages.clinic.complete_payment",['status'=>'error']);
        }
        
      }

      public function webhook(Request $request){ 
        \Stripe\Stripe::setApiKey(env("SECRET_KEY"));
        $endpoint_secret = 'whsec_onUnOUvdL0FBUDfaI66E0tXPQrR7MAyc';
        $payload = @file_get_contents('php://input');
        $event = null;

        try {
            $event = \Stripe\Event::constructFrom(
              json_decode($payload, true)
            );
          } catch(\UnexpectedValueException $e) {
            // Invalid payload
            echo '⚠️  Webhook error while parsing basic request.';
            http_response_code(400);
            exit();
          }

          if ($endpoint_secret) {
            // Only verify the event if there is an endpoint secret defined
            // Otherwise use the basic decoded event
            $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
            try {
              $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
              );
            } catch(\Stripe\Exception\SignatureVerificationException $e) {
              // Invalid signature
              echo '⚠️  Webhook error while validating signature.';
              http_response_code(400);
              exit();
            }
          }

          if($event->type == "payment_intent.succeeded"){
            $data = $event->data->object;
      
            $metadata = $data->metadata;
    
            $email = $metadata->clinic_email;
            $package = $metadata->package;
            $clinic_id = $metadata->clinic_id;
            $payment_id = $data->id;
            $amount = substr($data->amount, 0, -2);
            $currency = $data->currency;
            $description = $data->description;
            $data = date("Y:m:d h h:i");
            
            $fromData = date("Y:m:d");
            $toDate = "";
            if($package == "monthly"){
               $toDate = date('Y:m:d', strtotime('+1 month'));
            }
            elseif($package == "yearly"){
              $toDate = date('Y:m:d', strtotime('+12 month'));
            }

            $user = User::select('name')->find($clinic_id);
            $subject = 'Payment Confirmation';
    
            $check_package = ClientSubscription::where("user_id",$clinic_id)->get();
            if(count($check_package) > 0){
              ClientSubscription::where("user_id",$clinic_id)->delete();
            }
    
            $clinet_payment = new ClientPayment();
            $clinet_payment->user_id = $clinic_id;
            $clinet_payment->payment_id = $payment_id;
            $clinet_payment->date = date('Y:m:d');
            $clinet_payment->package = $package;
            $clinet_payment->amount = $amount;
            $clinet_payment->email = $email;
            $clinet_payment->curreny = $currency;
            $clinet_payment->desc = $description;
            $clinet_payment->from_date = $fromData;
            $clinet_payment->to_date = $toDate;
            $clinet_payment->save();
    
            $lastID = $clinet_payment->id;
           
            $client_subsciption = new ClientSubscription();
            $client_subsciption->user_id = $clinic_id;
            $client_subsciption->transaction_id = $lastID;
            $client_subsciption->package = $package;
            $client_subsciption->date = date("Y:m:d");
            $client_subsciption->from_date = $fromData;
            $client_subsciption->to_date = $toDate;
            $client_subsciption->status	 = '1';
            $client_subsciption->save();
    
            Mail::to($email)->send(new SubscriptionConfirmation(
              [
                'to'=>$email,
                'subject'=>$subject,
                'name'=>$user->name,
                'currency'=>$currency,
                'amount'=>$amount,
                ]
            ));

          }          
          http_response_code(200);
    }

}
