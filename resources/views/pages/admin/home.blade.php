@extends('layouts.admin_app')
<link href="{{ asset('css/admin_css.css') }}" rel="stylesheet">

@section('content')


<div class="container-fluid p-2" style="background: white; box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;">
  <div class="row">

  <a class="col-md-3" href="{{ url('/doctors/all') }}" title="Total doctors">
    <div class="card-counter danger">
      <i class="fa fa-user-md"></i>
      <span class="count-numbers">{{$doctorscount}}</span>
      <span class="count-name">Doctors</span>
    </div>
  </a>

  <a class="col-md-3" href="{{ url('/lawyers/all') }}" title="Total Lawyers">
    <div class="card-counter success">
      <i class="fa fa-user-plus"></i>
      <span class="count-numbers">{{$lawyerscount}}</span>
      <span class="count-name">Lawyers</span>
    </div>
  </a>

  <a class="col-md-3" href="{{ url('/clinic/all') }}" title="Total Lawyers">
    <div class="card-counter primary">
      <i class="fa fa-hospital-o"></i>
      <span class="count-numbers">{{$cliniccount}}</span>
      <span class="count-name">Clinic</span>
    </div>
  </a>

  @if ($reject_req > 0)
      <a class="col-md-3" href="{{ url('/reject_req') }}" title="Patient assessment reject request">
        <div class="card-counter info">
          <i class="fa fa-ban"></i>
          <span class="count-numbers">{{$reject_req}}</span>
          <span class="count-name">Reject requests</span>
        </div>
      </a>
  @endif

  @if ($changeRequest > 0)
  <a class="col-md-3" href="{{ url('/priceRequest') }}" title="Doctor price changed request">
    <div class="card-counter bg-danger-subtle">
      <i class="fa fa-usd"></i>
      <span class="count-numbers">{{$changeRequest}}</span>
      <span class="count-name">Price request</span>
    </div>
  </a>
  @endif

</div>
</div>

@endsection
