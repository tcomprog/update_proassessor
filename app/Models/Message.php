<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Message extends Model
{
    //
    use HasFactory;

    protected $table = 'messages';
    protected $guarded = [];

    public function sender() {
        return $this->belongsTo(User::class,'sender_user_id','id');
    }
   
    public function receiver() {
        return $this->belongsTo(User::class,'receiver_user_id','id');
    }
    
    public function invitation() {
        return $this->belongsTo(user_invitations::class,'invitation_id','id');
    }

}
