<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class doctors extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   
    protected $fillable = [
        'user_id',
        'unique_id',
        'licence_number',
        'introduction',
    ];
    
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
   
}
