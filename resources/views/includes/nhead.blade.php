<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="<?php  if(isset($pageuser->meta_keyword)){echo $pageuser->meta_keyword;}else{echo "nothing";} ?>">
<meta name="description" content="<meta name="keywords" content="<?php  if(isset($pageuser->meta_description)){echo $pageuser->meta_description;}else{echo 'nothing';} ?>">
<meta name="author" content="Scotch">

<!-- CSRF Token -->

<meta name="csrf-token" content="{{ csrf_token() }}">

<title><?php $sitedata1 = Helper::Sitedetails();   echo $sitedata1->site_title; ?> </title>

<!-- Styles -->

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Montserrat&family=Roboto:wght@900&display=swap" rel="stylesheet">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link href="{{ asset('css/tavo-calendar.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/evo-calendar@1.1.2/evo-calendar/css/evo-calendar.min.css"/>

<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/custom.css')."?111111111" }}" rel="stylesheet">
<link href="{{ asset('css/ndesign.css')."?989898" }}" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/css/selectize.bootstrap4.css"/><!--select-->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/css/selectize.css"/><!--select-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!--evo calender--

<link href="{{ asset('css/evo-calendar.css') }}" rel="stylesheet">-->

<!--added-->



<link rel="apple-touch-icon" sizes="180x180" href="{{asset('fav/apple-touch-icon.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{asset('fav/favicon-32x32.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('fav/favicon-16x16.png')}}/">
<link rel="manifest" href="{{asset('fav/site.webmanifest')}}">
<link rel="mask-icon" href="{{asset('fav/safari-pinned-tab.svg')}}" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">



<style>

.error{
 color: #FF0000; 
}

.login_box .form-check label {
    font-size: 14px;
}

@font-face {
    font-family: DubaiBold;
    src: url("{{asset('dubaifont/Dubai-Bold.ttf')}}");
}

@font-face {
    font-family: Fahkwang;
    src: url("{{asset('fonts/Fahkwang.ttf')}}");
}

@font-face {
    font-family: Montserrat;
    src: url("{{asset('fonts/Montserrat.ttf')}}");
}

@font-face {
    font-family: EncodeSans;
    src: url("{{asset('fonts/EncodeSans.ttf')}}");
}

@font-face {
    font-family: DubaiRegular;
    src: url("{{asset('dubaifont/Dubai-Regular.ttf')}}");
}

@font-face {
    font-family: DubaiMedium;
    src: url("{{asset('dubaifont/Dubai-Medium.ttf')}}");
}

@font-face {
    font-family: DubaiLight;
    src: url("{{asset('dubaifont/Dubai-Light.ttf')}}");
}

</style>