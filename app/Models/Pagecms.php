<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pagecms extends Model
{
    use HasFactory;

    protected $table = 'cms';
}
