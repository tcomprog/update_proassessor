@extends("includes.email_container")
@section("content")
            <div>
                  <br>

                     <p>
                        <b>Dear {{$name}}</b> <br><br>
                        Your <b>{{$package}}</b> subscription, subscribed on <b>{{$from_date}}</b> will
                        expire on <b>{{$to_date}}</b>. In order to resume using our services
                        you are required to resubscribe the package. You will be able
                        to resubscribe to the new package once your old package expired.
                     </p>

                    <br/>
                    <br/>
                    <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
                    <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 
                    </div>  
               </div>
            </div>
  @endsection