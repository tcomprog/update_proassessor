<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body class="d-flex flex-column min-vh-100">
    <header class="wrapper" >
        <div class="container">
            <div class="row">
                @include('includes.header')
            </div>
        </div>
    </header>
    {{-- @include('includes.inner-banner') --}}
    <div style="flex: 1">
    @yield('content')
    </div>
    <footer>
       @include('includes.nfooter')
    </footer>
    @include('includes.model')
    @include('includes.scripts')

</body>
</html>

