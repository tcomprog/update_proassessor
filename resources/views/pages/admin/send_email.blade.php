@extends('layouts.admin_app')
@section('content')

<div class="container-fluid bg-white rounded py-4 adminPageContainer">

    <form method="post" class="row" action="">
        @csrf
        <h4 class="inv-content-header" style="margin-bottom:0px;">Send Email</h4>

        <div class="my-4">
            <div class="form-group col-6">
              <label for="to_email" class="font-weight-bold">To</label>
              <input id="send_email_to_email" required type="email" class="form-control" id="to_email" maxlength="50" placeholder="To email address">
            </div>

            <div class="form-group col-6">
              <label for="subject" class="font-weight-bold">Subject</label>
              <input id="send_email_subject" required type="text" class="form-control" id="subject" placeholder="Subject" maxlength="100">
            </div>
    
            <div class="col-12">
                <label for="subject" class="font-weight-bold">Message</label>
                <textarea class="form-control" id="mainMessage" rows="5"></textarea>
            </div>
             
            <div class="col-12">
                <button id="send_email_btn" type="button" class="btn btn-primary mt-3">Send</button>
                <img style="display: none" id="loading_send_mail" width="40px" src="{{asset('img/loading.gif')}}" alt="">
            </div>
        </div>

    </form>
</div>

@endsection




