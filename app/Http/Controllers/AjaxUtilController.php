<?php







namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\User;
use App\Models\doctors;
use App\Models\LawyerTypes;
use App\Models\AdminSettings;
use App\Helpers\Helper as MyHelper;
use Illuminate\Http\Request;
use Auth;


class AjaxUtilController extends Controller

{  

    public function ajax_search_doctors_listing(Request $request)
    {
        $user = auth()->user();
        $keyword=$request->keyword;
        $location=$request->location;
        $category_id=$request->category_id;
        $price_range=$request->price_range;
        $cliniclist=$request->cliniclist;
        $types = $request->types;


        $users = User::query();
        if(($keyword)!=null){
           $users->where('name', 'like', "%{$keyword}%");
        }

        if($types == 0){
            $users->where('clinic_id', "0"); 
        }else{
            if(($cliniclist) == null){
                $cliniclist = -1;
            }

            $users->where('clinic_id',"!=","0"); 
            $users->where('clinic_id', $cliniclist);
        }

        if(($location)!=null){
            $users->where('location', '=', "$location");				
        }

        if(($category_id)!=null){				
            $users->whereHas('doctor', function($q) use($category_id){
                    $q->whereRaw('FIND_IN_SET('.$category_id.',category_id)');
            });
        }

        if(($price_range)!=null){               
        $users->whereHas('doctor', function($p) use($price_range){

                if($price_range==1500){
                     $p->where('actual_price', '>', '0');
                     $p->where('actual_price', '<=', '1500');
                }elseif($price_range>=6500){

                     //$p->where('updated_price', '>=', '5500');
                     $p->where('actual_price', '>=', '6500');

                }else{
                         $new_price_range=($price_range-1000);
                         $p->where('actual_price', '>', $new_price_range);
                         $p->where('actual_price', '<=', $price_range);
                }
             });

        }

        $users->where('role_id', '=', 1);
       // $users->where('account_status', '=', 1);
        $users->where('status', '=', "active");
        $users =$users->paginate(5);
        $users->appends(['keyword' => $keyword,'location'=>$location,'category_id'=>$category_id,'price_range'=>$price_range]);
        $categories = Category::select(['id','category_name'])->get();
        //$comission = AdminSettings::where('settings_name', 'comission')->select('settings_value')->first();

        $types = LawyerTypes::find(Auth::user()->lawyer_type);
        $commission = $types ? $types->commission : 2700;

        $view =  view('/pages/ajax/ajax-search-doctors-list', 
        ['users' => $users,
        'categories'=>$categories,
        'input_category_id'=>$category_id,
        'commission'=> $commission
        ])->render();

        return ['data'=>$view,'message'=>'data feched successfully.','status'=>'success'];
    }



    public function doctor_categories(Request $request)

    {

        $doctor = doctors::where('user_id',$request->doctor_user_id)->first();

        $categories = $doctor->category_id;

        $category_ids = explode(',',$categories);

        $categories = Category::select(['id','category_name'])->whereIn('id',$category_ids)->get();

        return ['categories'=>$categories,'status'=>'success'];

    }



}