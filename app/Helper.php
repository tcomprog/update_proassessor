<?php 
namespace App;
use App\Models\Category;
use App\Models\Message;
use App\Models\Sitedetails;
use App\Models\City;
use App\Models\DoctorRating;
use App\Models\User;
use App\Models\Patient;
use App\Models\Designation;
use App\Models\ClientSubscription;
use App\Models\Nofication;
use Auth;

class Helper
{

	public static function checkClinicSubscription(){
       $user = Auth::user();
	   if(Auth::check() && $user->role_id == 4){
		   $user_id = Auth::id();  
		   $subscription = ClientSubscription::select("status")->where('user_id',$user_id)->get();
		   if(count($subscription) > 0 && $subscription[0]->status == 1){
			 return 1;
		   }
		   else{
		  	 return 0;
		   }
	   }
	   else{
		 return 1;
	   }
	}

	public static function categorydetails(){
	$categorydetails = Category::all();
	return $categorydetails;
	}
	public static function patient(){
	$patient= Patient::all();
	return $patient;
	}
	
	public static function cities(){
		//$cities = City::all();
		$cities = City::select('id', 'city_name')->orderBy('city_name', 'ASC')->get();	
		return $cities;
	}
	
	public static function designations(){
		$designations = Designation::all();	
		return $designations;
	}

	public static function Sitedetails(){
	$sitedata = Sitedetails::where('id',1)->first(); 
	return $sitedata;
	} 
	public static function un_read_messages($user_id,$invitation_id)
	{
		$count = Message::where('receiver_user_id',$user_id)->where('is_read','0')->where('invitation_id',$invitation_id)->get()->count();
		return $count;
	} 
	public static function user_unread_messages($user_id)
	{
		$count = Message::where('receiver_user_id',$user_id)->where('is_read','0')->get()->count();
		return $count;
	} 
	public static function notification_messages($user_id)
	{
		$notification = Nofication::where('to_id',$user_id)->where('status','0')->get()->count();
		$total_ck = Nofication::where('to_id',$user_id)->where('status','0')->get()->count();
		return ['cnt'=>$notification,'total'=>$total_ck ];
	} 

	public static function getnotificationList($user_id)
	{
		$notification = Nofication::where('to_id',$user_id)->orderBy('created_at','desc')->limit(10)->get();
		return $notification;
	} 


	public static function clear_un_read_messages($user_id,$invitation_id)
	{
		Message::where('receiver_user_id',$user_id)->where('is_read','0')->where('invitation_id',$invitation_id)->update(['is_read'=>'1']);
		return true;
	}
	public static function User(){		
		$user = auth()->user();
		$userdata = User::where('id',$user->id)->first(); 
		return $user;		
	}


	public static function doctorRating($doctorId,$tttt = null){
		$rating = DoctorRating::where('doctor_id',$doctorId)->get();
		$start_1 = 0;
		$start_2 = 0;
		$start_3 = 0;
		$start_4 = 0;
		$start_5 = 0;

		foreach($rating as $row){
			switch($row->rating){
			 case 1:{
				 $start_1++;
				 break;
			 }
			 case 2:{
				 $start_2++;
				 break;
			 }
			 case 3:{
				 $start_3++;
				 break;
			 }
			 case 4:{
				 $start_4++;
				 break;
			 }
			 case 5:{
				 $start_5++;
				 break;
			 }
			}
		 }

		 $average = 0;
		 $total_score = ($start_5*5) + ($start_4*4) + ($start_3*3) + ($start_2*2) + ($start_1*1);
		 $total_response = $start_1 + $start_2 + $start_3 + $start_4 + $start_5;
		 if($total_response > 0){
			$average = ($total_score / $total_response);
			$average = number_format($average, 1, '.', '');
		 }

		 if($tttt == null){
		    $arrarys = [];
		    $arrarys['rating_1'] = $start_1;
			$arrarys['rating_2'] = $start_2;
			$arrarys['rating_3'] = $start_3;
			$arrarys['rating_4'] = $start_4;
			$arrarys['rating_5'] = $start_5;
			$arrarys['doc_avg'] = $average;
			return $arrarys;
		 }
		 else{
			return $average;
		 }
	}
}
?>