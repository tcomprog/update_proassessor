<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Providers\RouteServiceProvider;
use App\Models\Sitedetails;
use App\Models\Pagecms;
use App\Models\AdminSettings;
use Illuminate\Support\Facades\Crypt;
use Helper;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\RejectInvitation;
use App\Models\PriceChangeRequest;

class HomepageController extends Controller
{
    public function index($lg="null")
    {
        if(Auth::check() && Auth::user()->role_id == 3){
           return redirect('dashboard');
        }
        else{
            $setting_about_image = AdminSettings::where('settings_name','aboutus_image')->first();
            $pagedetails = Sitedetails::find(1);
           
            $page_slug="about";
            $about = Pagecms::where('page_slug',$page_slug)->first();
            
            return view('/pages/home', 
            [
                'pagedetails' => $pagedetails,
                'about' => $about,
                'lg' => $lg,
                'setting_about_image'=>$setting_about_image
            ]);
      }
    }

    public function testing(){
        $setting_about_image = AdminSettings::where('settings_name','aboutus_image')->first();
        $pagedetails = Sitedetails::find(1);
       
        $page_slug="about";
        $about = Pagecms::where('page_slug',$page_slug)->first();
        
        return view('home_testing', 
        [
            'pagedetails' => $pagedetails,
            'about' => $about,
            'setting_about_image'=>$setting_about_image
        ]);
    }
}
