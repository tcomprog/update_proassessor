<?php

namespace App\Mail;

use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RejectResponsePatient extends Mailable
{
    use Queueable, SerializesModels;

    public $options = [];
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$fromdte,$todate)
    {
        $this->name = $name;
        $this->fromdte = $fromdte;
        $this->todate = $todate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $fromAddress = Config::get('constants.MAIL_FROM_ADDRESS');
        $site_title_old = Config::get('constants.site_title');
        $site_title= str_replace("-"," ",$site_title_old);

        return $this->subject($site_title.' - Change date response')
                     ->from($fromAddress,$site_title)
                     ->view('emails.reject_mail_to_patient',["name"=>$this->name,"from"=>$this->fromdte,"to"=>$this->todate]);
    }
}
