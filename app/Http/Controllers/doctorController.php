<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Mail;
use Session;
use App\Models\doctors;
use App\Models\Holiday;
use App\Models\Designation;
use App\Models\DoctorTimeSlot;
use App\Models\Doctorappointment;
use App\Helper;
use App\Models\Patient;
use App\Models\Category;
use App\Models\Tracability;
use App\Helpers\Helper as MyHelper;
use App\Models\user_invitations;
use App\Models\Invites_Payments;
use App\Models\AdminSettings;
use App\Models\Nofication;
use App\Models\DoctorRating;
use App\Models\PriceChangeRequest;
use App\Models\InvitationAttachment;
use App\Http\Controllers\EmailController;
use App\Models\Sitedetails;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


class doctorController extends Controller
{ 
	
	public function profile($docID = null)
    {
       if($docID != null){ 
		    $user = User::find($docID);		  
			$users = User::find($docID);
			$ac_type = 4; // clinic	
	   }
	   else{
			$user = auth()->user();		   
			$users = User::find($user->id);	
			$ac_type = 1; // doctor	
	   }
			
			$designations = Designation::all();
			
			
			$ratings = Helper::doctorRating($user->id);
			$check = PriceChangeRequest::where("doctorID",$user->id)->where('status','0')->get();

			if(count($check) > 0){
				$users->showPriceBtn = '0';
			}
			else{
				$users->showPriceBtn = '1';
			}
			
			$pr_request = $check = PriceChangeRequest::where("doctorID",$user->id)->orderBy('id','desc')->get();

			$arrarys = compact('users','designations','pr_request');
			$arrarys['rating_1'] = $ratings['rating_1'];
			$arrarys['rating_2'] = $ratings['rating_2'];
			$arrarys['rating_3'] = $ratings['rating_3'];
			$arrarys['rating_4'] = $ratings['rating_4'];
			$arrarys['rating_5'] = $ratings['rating_5'];
			$arrarys['doc_avg'] = $ratings['doc_avg'];
			$arrarys['ac_type'] = $ac_type;
			
			return view('/pages/doctor/doctor-profile',  $arrarys);
    }

	public function privateInfor()
    {
			$user = auth()->user();		   
			$users = User::find($user->id);		
			$designations = Designation::all();
			
			return view('/pages/doctor/private-information', compact('users','designations'));
    }



	/*create task*/
	public function doctortasks(Request $request)
    {
		 if(Auth::user()->role_id == "4"){
            $user = User::find($request->doctorID);
		 }
		 else{
			$user = auth()->user();  
		 }
			
		$id=$user->id;

			if(!empty($request->selecteddate)){
				//$selecteddate=$request->selecteddate;
				$selecteddate = date("Y-m-d", strtotime($request->selecteddate)); 
			}else{
				$tomorrowUnix = strtotime("+1 day");
				$selecteddate=date('Y-m-d',$tomorrowUnix);
			}	

			 if(!$request->timeslot) {
				return response()->json(['status'=> "error",'error'=>'Please check timeslots.']);
			}
			$timeslot = implode(",",$request->timeslot);
			//return response()->json(['error'=> "error",'timeslot' => $timeslot]);exit;
						  
				

		    $chkslot = Doctorappointment::query();
			//$chkslot->where('timeslot', '=', $timeslot);
			$chkslot->where('appointment_date', '=', $selecteddate);
			$chkslot->where('doctor_id', '=', $id);
			$chkslot =$chkslot->first();
			
			if(isset($chkslot->id)){			
			$chkslot->timeslot=$timeslot;
			$chkslot->save();	
			return response()->json(['status'=> "success",'url'=>url('doctor-dashboard')."/update-calendar"]);
			
			}else{
				$data1 = new Doctorappointment();
				$data1->doctor_id=$id;
				//$data1->notes=$notes;
				$data1->appointment_date=$selecteddate;
				$data1->timeslot=$timeslot;
				$data1->save();
				return response()->json(['status'=> "success",'url'=>url('doctor-dashboard')."/update-calendar"]);
				
			}		
			
		
    }


	public function updateprivateinfo(Request $request){
		$user = User::where('id', $request->id)->first(); 
		$user->phone = $request->phone;
		$user->business_address = $request->business_address;
	
		if(($request->password)!=null){
			$password=Hash::make($request->password);
			$user->password = $password;
		}		
		$user->save();

		if(isset($user->id)){			
			$doctor = doctors::where('user_id', $user->id)->first();
			$doctor->licence_number=$request->licence_number;
          
			$doctor->save();
			if(isset($doctor->id)){
				return response()->json(['status'=> "success", 'url'=>url('doctor-private')]);
			}
			else{
				 return response()->json(['status'=> "error"]); 
			}
		}

	}


	public function changePriceRequest(Request $request){
		$oldPrice = $request->oldPrice;
		$newPrice = $request->newPrice;
		$doctorID = $request->doctorID;
		$description = $request->description;

		$check = PriceChangeRequest::where("doctorID",$doctorID)->where('status','0')->get();
		
		if(count($check) > 0){
           return response()->json(["status"=>"exist",'message'=>"Your have already submitted request kindly wait for admin response"],200);
		}
		else{
            $pr_request = new PriceChangeRequest();
			$pr_request->doctorID = $doctorID;
			$pr_request->oldPrice = $oldPrice;
			$pr_request->newPrice = $newPrice;
			$pr_request->status = '0';
			$pr_request->des = $description;
			$pr_request->save();

			return response()->json(["status"=>"success",'message'=>"You request has been submitted to admin, kindly wait for admin response"],200);
		}
	}
	
	public function hstUpdate(Request $request){
		$hst = $request->hst;
		$doctorID = $request->doctorID;
		$doctor = doctors::where('user_id', $doctorID)->first();
		$doctor->hst="yes"; 
	    $doctor->hst_number = $hst;
		$doctor->save();

		return response()->json(['status'=>"success",'message'=>"HST number updated successfully"],200);
	}

	/*update profile*/
	public function updatedoctor(Request $request){	
	//exit("here");
        $doctorID = $request->accessDoctorID;
		//abs
		$user = AdminSettings::where('settings_name', 'comission')->first();
		if($user!=null){
			$comission=$user->settings_value;
		}else{
			$comission='';
		}

		$rules=[//'email' => 'unique:users',
				//'name'=> 'required',
				'image' => 'mimes:png,jpg,jpeg|max:1024',
				'cvimage' => 'mimes:png,jpg,jpeg,doc,docx,pdf|max:1024',
				'instructionform' => 'mimes:png,jpg,jpeg,doc,docx,pdf|max:1024'
				];
		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()) {
			$response['response'] = $validator->messages();
			//return $response;
			return response()->json(['status'=>'error', 'message'=>$validator->messages()]);					
		}
		else{	
		
		if($request->hasFile('image')) {
			$file = $request->file('image');

			$filename = time().$request['phone'].'.'.$file->getClientOriginalExtension();
			//$image['filePath'] = $filename;
			$file->move('uploads/profilepics', $filename);
			//return response()->json(['status'=>'success', 'file'=>$filename]);
		}/*else{
			$filename="";
		}*/
		if($request->hasFile('cvimage')) {
					$file1 = $request->file('cvimage');

					$cvfilename = time().$request['phone'].'.'.$file1->getClientOriginalExtension();
					//$image['filePath'] = $filename;
					$file1->move('uploads/cv', $cvfilename);
					//return response()->json(['status'=>'success', 'file'=>$filename]);
				}
		if($request->hasFile('instructionform')) {
					$insfile1 = $request->file('instructionform');

					$insfilename = time().$request['phone'].'.'.$insfile1->getClientOriginalExtension();
					//$image['filePath'] = $filename;
					$insfile1->move('uploads/instruction_form', $insfilename);
					//return response()->json(['status'=>'success', 'file'=>$filename]);
				}
		if($request->category_id){		
			$category_new=implode(',', $request->category_id);
		}else{
			$category_new='';
		}

        $user = User::where('id', $doctorID)->first(); 
		$updateStatus = $user->account_status == '0' ? 0 : 1;
		
		$user->name = $request->name;
		$user->desgination = $request->credentials;
		$user->account_status=1; 
		$user->location = $request->location;
		if(isset($filename)){
		$user->profilepic = $filename;
		}
		
		$user->save();

		if(isset($user->id)){			
			$doctor = doctors::where('user_id', $user->id)->first();
			$doctor->introduction=$request->introduction;
			$doctor->category_id=$category_new; 
			$doctor->virtual_appointment=$request->virtual_appointment;
			$doctor->hst=$request->hst; 

			if($request->has("hst") && $request->hst == 'no'){
				$doctor->hst_number = "null";
			}

			$doctor->assessmentOnlyCharges=$request->assessmentOnlyCharges; 
			$doctor->lateCancellationFee=$request->lateCancellationFee; 
			$doctor->account_status=1;

			if($updateStatus == 0){
				$doctor->actual_price= $request->price;
			}
          
			if(isset($cvfilename)){
				if($doctor->cvimage!=null){	
					$delcvfile='uploads/cv/'.$doctor->cvimage;
					unlink($delcvfile);
				}
				$doctor->cvimage = $cvfilename;
			}
			if(isset($insfilename)){
				if($doctor->instruction_form!=null){	
					$delcvfile1='uploads/instruction_form/'.$doctor->instruction_form;
					unlink($delcvfile1);
				}
				$doctor->instruction_form = $insfilename;
			}
			$doctor->save();

				if(isset($doctor->id)){
				   return response()->json(['status'=> "success", 'url'=>url('doctor-profile')]);
				   }
			   else{ return response()->json(['status'=> "error"]); }
		} 
		
	}
		
	}	
	/*dashboard*/
	public function dashboard(Request $request, $activeTab=null)
    {
		$user = auth()->user();		
		// $doctorapp = Doctorappointment::where('doctor_id', $user->id)->select('id', 'notes','timeslot', 'appointment_date')->get();	
		$settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
		$expireTime =date('Y-m-d H:i:s',strtotime('-'.$settings->invitation_expire_in_minutes.' minutes'));
		
		$newList=$blocked_dates=array();
		$invitations = user_invitations::where('invitee_user_id',$user->id)->orderBy('id','desc')->get();

		$archived_invitations=user_invitations::where('invitee_user_id',$user->id)->where('is_archive','yes')->orderBy('id','desc')->get();

		$completedAr = $new_invitations = $accepted_invitations =$confirmed_invitations= $expired_invitations = $rejected_invitations = $cancelled_invitations  = [];
	
		$dueDateArray = $invitationDateArray=[];
		$un_blockable_dates=[];
		$assessment_days_all=[];
		$due_days_all=[];
		$result='';
	

		if($invitations) {
		foreach($invitations as $invitation) {
			if($invitation->status == 'pending' && strtotime($invitation->created_at) > strtotime($expireTime)) {
				$new_invitations[] = $invitation;
				$un_blockable_dates[] = intval(strtotime($invitation->invitation_date).'000');
			}else if($invitation->status == 'active' && $invitation->is_patient_accepted == 'yes') {
				if(isset($request->lawyerkeyword)){					
					if((strripos($invitation->user->name, $request->lawyerkeyword)!==false)){						
						$confirmed_invitations[] = $invitation;
					}elseif(strripos($invitation->patient->name, $request->lawyerkeyword)!==false){
						$confirmed_invitations[] = $invitation;
					}
					elseif(strripos($invitation->user->lawyer->lawfirmname, $request->lawyerkeyword)!==false){
						$confirmed_invitations[] = $invitation;
					}
				}else{ 
					$confirmed_invitations[] = $invitation;
				}
				$un_blockable_dates[] = intval(strtotime($invitation->invitation_date).'000');
				$invitationDateArray[] = $invitation->invitation_date.' 00:00:00';
				$dueDateArray[] = $invitation->duedate.' 00:00:00';
			}
			else if($invitation->status == 'rejected') {
				$rejected_invitations[] = $invitation;
			}
			else if($invitation->status == 'completed') {
				$completedAr[] = $invitation;
			}
			else if($invitation->status == 'cancelled') {
				$cancelled_invitations[] = $invitation;
			}else if($invitation->status == 'active' && $invitation->is_patient_accepted == 'no') {
				$accepted_invitations[] = $invitation;
				//$invitationDateArray[] = $invitation->invitation_date.' 00:00:00';
				$dueDateArray[] = $invitation->duedate.' 00:00:00';
				$un_blockable_dates[] = intval(strtotime($invitation->invitation_date).'000');
			}else if($invitation->status == 'pending' && $invitation->is_archive == 'no' && strtotime($invitation->created_at) <= strtotime($expireTime)) {
				$expired_invitations[] = $invitation;
			}

			if(($invitation->status == 'pending' && strtotime($invitation->created_at) <= strtotime($expireTime)) || $invitation->status == 'rejected' || $invitation->status == 'cancelled') {
				continue;
			}	
					
			$newList1[] = [				
				'id'   =>  $invitation['unique_id'],
				'name' =>  $invitation->status ==='pending'?'New Assessment':'Assessment  Accepted',				
				'date' => date('Y-m-d', strtotime($invitation['invitation_date'] .' +1 day')),
				'timeslot' => $invitation['timeslot'],						
				'patientname' => ucfirst($invitation->patient->name),	
				'docid' => $invitation->inviter_user_id,
				'type'  => "holiday", 
				];
			$newList2[] = [				
				'id'   =>  $invitation['unique_id'],
				'name' =>    'Report Due Date',
				'date' => date('Y-m-d', strtotime($invitation['duedate'] .' +1 day')),
				'timeslot' => $invitation['timeslot'],  						
				'patientname' => ucfirst($invitation->patient->name),	
				'docid' => $invitation->inviter_user_id,
				'type'  => "event", 
				];	
				$assessment_days_all[]=date('Y-m-d',strtotime($invitation['invitation_date']));				
				$due_days_all[]=date('Y-m-d',strtotime($invitation['duedate']));				
				
				//$newList3=[]; 
				$actual_blocked_dates = Holiday::where('doctor_user_id',$user->id)->where('date','>=',date('Y-m-d'))->get()->pluck('date')->toArray();
				$actual_due_dates = user_invitations::where('invitee_user_id',$user->id)->where('duedate','>=',date('Y-m-d'))->get()->pluck('duedate')->toArray();
				if($actual_due_dates){
				$result = array_intersect($actual_blocked_dates, $actual_due_dates);			
				//print_r($result);exit;
				/* foreach($result as $key => $result1) {	 			
						$newList3[] = [				
							'id'   =>  $invitation['unique_id'],
							'name' =>    'Blocked day',
							'date' => $result1,
							'timeslot' => '',  						
							'patientname' => '',	
							'docid' => $invitation->inviter_user_id,
							'type'  => "birthday", 
							];
					}*/
				} 

				$newList = array_unique(array_merge($newList1, $newList2),SORT_REGULAR);	
			}	
		}	

		$blockedDates = Holiday::where('doctor_user_id',$user->id)->where('date','>=',date('Y-m-d'))->get()->pluck('date')->toArray();
		if($blockedDates) {
			foreach($blockedDates as $blockedDate) {
				$blocked_dates[] = intval(strtotime($blockedDate).'000'); 
			}
		}
		$assessment_days=[];
		if($assessment_days_all) {
			foreach($assessment_days_all as $assessment_day) {
				$assessment_days[] = intval(strtotime($assessment_day).'000'); 
			}
		}
		$due_days=[];
		if($due_days_all) {
			foreach($due_days_all as $due_day) {
				$due_days[] = intval(strtotime($due_day).'000'); 
			}
		}
			
		$due_block_dates=[];
		if($result){
			foreach($result  as $due_block_result) {
				$due_block_dates = intval(strtotime($due_block_result).'000'); 
			}
		}

		$invoices = Invites_Payments::where('doctor_user_id', $user->id)->orderBy('id', 'desc')->paginate(10);
		//$suminvoices = Invites_Payments::where('doctor_user_id', $user->id)->sum('payment_amount');
		$suminvoices = Invites_Payments::query();
		$suminvoices->whereHas('invitations', function($q){
				$q->where('status', '=', 'active');  });
		$suminvoices=$suminvoices->sum('payment_amount'); 
       
		$doctor = DB::select("select account_status from tbl_doctors where user_id = '$user->id'");
        $ac_status = count($doctor) > 0 ? $doctor[0]->account_status : 0;

		$categor_id = $user->doctor ? $user->doctor->category_id : '';

		return view('/pages/doctor/doctor-dashboard',[
			  'ac_status'=>$ac_status,
			  'newList' => $newList,
			  'users' => $user,
			  'invoices' => $invoices, 
			  'suminvoices' => $suminvoices,
			  'expire_time'=>$expireTime,
			  'invitations' => $invitations,
			  'new_invitations'=>$new_invitations,
			  'accepted_invitations'=>$accepted_invitations, 
			  'archived_invitations'=>$archived_invitations,
			  'expired_invitations'=>$expired_invitations,
			  'confirmed_invitations'=>$confirmed_invitations,
			  'completedAr'=>$completedAr,
			  'rejected_invitations'=>$rejected_invitations,
			  'cancelled_invitations'=>$cancelled_invitations,
			  'dueDateArr'=>json_encode($dueDateArray),
			  'invitationDateArr'=>json_encode($invitationDateArray),
			  'activeTab'=>$activeTab,
			  'blockedDates'=>json_encode($blocked_dates),
			  'dueblockedDates'=>json_encode($due_block_dates),
			  'un_blockable_dates'=>json_encode($un_blockable_dates),
			  'doctor_category_id'=>$categor_id,
			  'assessment_days'=>json_encode($assessment_days),
			  'due_days'=>json_encode($due_days)
			]); 	
    }

	/*delete task*/
	public function deletetask(Request $request){
		
		$id=$request->rowid1;
		//$notes=$request->rownotes;
		$user = Doctorappointment::where('id', $id)->delete(); 
		
		return response()->json(['status'=> "success",'url'=>url('doctor-dashboard')."#nav-calendar-tab"]);
			
	}
	
	/*edit task*/
	public function edittask(Request $request){
		   	
		$id=$request->rowid;
		$notes=$request->rownotes;
		$user = Doctorappointment::where('id', $id)->first(); 
		$user->notes = $notes;
		$user->save();
		if(isset($user->id)){
			return response()->json(['status'=> "success",'url'=>url('doctor-dashboard')."#nav-calendar-tab"]);
		}	
			
	}
	
	public function acceptint(Request $request){
		if($request->id) {		
			$sendint = user_invitations::find($request->id);
			$sendint->status = "active"; TODO:
			$sendint->doctor_accepted_on = date('Y-m-d H:i:s');
			$sendint->save();

			$tracability = new Tracability();
			$tracability->date = date('Y-m-d H:i:s');
			$tracability->invitation_id	 = $request->id;
			$tracability->status = '1';
			$tracability->save();
			
			$data = Patient::find($sendint->patient_user_id);
			$doctor = User::find($sendint->invitee_user_id);

			$des = MyHelper::c_strings()["docotor_inv"];
			$des = str_replace("##",'accepted',$des);
			$des = str_replace("@patient@",$data->name,$des);
			$des = str_replace("@doctor@",$doctor->name,$des);

            $notif = new Nofication();
			$notif->url = "lawyer-dashboard/tracking";
			$notif->description = $des;;
			$notif->to_id = $sendint->inviter_user_id;
			$notif->from_id = $sendint->invitee_user_id;
			$notif->save();

			$pt_data = User::select("id")->where('patient_id',$sendint->patient_user_id)->first();
            $des_1 = "Your invitation created by your lawyer has been approved by doctor: ".$doctor->name;
			$notif_1 = new Nofication();
			$notif_1->description =  $des_1;
			$notif_1->url = 'invitation_details_view/'.$sendint->id;
			$notif_1->status = '0';
			$notif_1->to_id = $pt_data->id;
			$notif_1->from_id = $sendint->invitee_user_id;
			$notif_1->save();

			if($sendint->id){
				$emailCtrl = new EmailController();
				$emailCtrl->send_invitation_confirmation_to_patient($sendint); 
				$emailCtrl->send_invitation_confirmation_to_lawyer($sendint); 
				Session::flash('success', "Assessment  Accepted successfully.");
				return response()->json(['status'=> "success",'url'=>url('doctor-dashboard')."#nav-reports-tab"]);
			}
		}	  
		return response()->json(['status'=> "error"]);	
	}
	
	public function rejectint(Request $request){
		
		$id=$request->id; 
		$sendint = user_invitations::find($id);		
		$sendint->status = "rejected";
		$sendint->rejected_by= "doctor";
		$sendint->save();


		    $data = Patient::find($sendint->patient_user_id);
			$doctor = User::find($sendint->invitee_user_id);

			$des = MyHelper::c_strings()["docotor_inv"];
			$des = str_replace("##",'rejected',$des);
			$des = str_replace("@patient@",$data->name,$des);
			$des = str_replace("@doctor@",$doctor->name,$des);

            $notif = new Nofication();
			$notif->url = "lawyer-dashboard/tracking";
			$notif->description = $des;;
			$notif->to_id = $sendint->inviter_user_id;
			$notif->from_id = $sendint->invitee_user_id;
			$notif->save();

		
		if(isset($sendint->id)){ 
			$emailCtrl = new EmailController();
			$emailCtrl->send_notification_email_to_lawyer_after_doctor_rejects($sendint); 
			Session::flash('success', "Invitation Rejected successfully.");
			return response()->json(['status'=> "success",'url'=>url('doctor-dashboard')."#nav-invitations-tab"]);
			//return response()->json(['status'=> 'success', 'id'=> $sendint->id]);
		}	  
			
	}
	
	/*delete profilepic*/
	public function deleteprofilepic(Request $request){
		
		$id=$request->userid;
		$userpic=$request->userpic;
		$user = User::where('id', $id)->first(); 
		$a='uploads/profilepics/'.$userpic;
		unlink($a);
		$user->profilepic = "";
		$user->save();
		if(isset($user->id)){ 
			return response()->json(['status'=> "success",'url'=>url('doctor-profile')]);
		}	
			
	}



	public function block_dates(Request $request)
	{
		$userID = auth()->id();
		if(Auth::user()->role_id == "4" && $request->has("doctorID")){
			$userID = $request->input("doctorID");
		}

		if($dates = $request->dates){
			$blockdates = explode(',',$dates);
			$blockedIds=[];
			foreach($blockdates as $date) {
				$holidayData = ['doctor_user_id'=>$userID,'date'=>date('Y-m-d',strtotime($date))];
				$created = Holiday::create($holidayData);
				$blockedIds[] = $created->id;
			}
			if($blockedIds) {
				Holiday::where('doctor_user_id',$userID)->whereNotIn('id',$blockedIds)->orWhere('date','<',date('Y-m-d',strtotime('-1 day')))->delete();
			}
		}else{
			$check = Holiday::where('doctor_user_id',$userID)->first();
			if($check) {
				Holiday::where('doctor_user_id',$userID)->delete();
			}
		}	
		Session::flash('alert-class', 'alert-success');
		Session::flash('message', 'Calendar Block dates updated successfully.'); 

		if(Auth::user()->role_id == "4" && $request->has("doctorID")){
			return redirect('viewClinicCalender/'.$userID);
		}
		else{
			if($selected_date = $request->selected_date){
				return redirect('doctor-dashboard/update-calendar?date='.date('d/F/Y',strtotime($selected_date)));
			}else{
				return redirect('doctor-dashboard/update-calendar');
			}
		}
		
	}

	
	public function blockeddates(Request $request){
		$date=$request->date;	
		$blockDay = false;

		if(Auth::user()->role_id == '4'){
			$user = User::find($request->doctorID);
		}
		else{
			$user = auth()->user();
		}


		$selecteddate = date("Y-m-d", strtotime($date)); 	
		// $blockDay = Holiday::where('date_from','<=',$selecteddate)->where('date_to','>=',$selecteddate)->where('doctor_user_id',$user->id)->exists();
		$timeslotsold = DoctorTimeSlot::orderBy('time_slot','asc')->get();	
		$timeslots=[];
		foreach($timeslotsold as $newtimeslots)
		{
			$changedslot=date('h:i A',strtotime($newtimeslots['time_slot']));
			$timeslots[]= [				
				'id'   =>  $newtimeslots['id'],
				'time_slot' =>  $changedslot,
			];
		}
		$settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
		$expireTime =date('Y-m-d H:i:s',strtotime('-'.$settings->invitation_expire_in_minutes.' minutes'));

		$invitedSlots = user_invitations::where('invitation_date',$selecteddate)->where('invitee_user_id', $user->id)
		->where(function($q) use($expireTime){
			$q->whereIn('status',['active','completed'])
			->orWhere(function($q2) use($expireTime){
				$q2->where('status','pending')->where('created_at','>=',$expireTime);
			});
		})->get()->pluck('timeslot')->toArray();
		$doctordates = Doctorappointment::where('appointment_date',$selecteddate)->where('doctor_id', $user->id)->select('timeslot')->first();
		
		$doctorTimeSlots = [];
		if($doctordates!=null){
			$timeslot11=explode(",",$doctordates->timeslot);
			foreach($timeslotsold as $slot) {
				$slot->time_slot = date('h:i A',strtotime($slot->time_slot));
				$slot->is_checked = false;
				$slot->is_disabled = false;
				if(in_array(date('h:i A',strtotime($slot->time_slot)),$timeslot11)){
					$slot->is_checked = true;
				}

				if(in_array(date('h:i A',strtotime($slot->time_slot)),$invitedSlots)){
					$slot->is_disabled = true;
				}

				$doctorTimeSlots[] = $slot;
			}
			return response()->json(['status'=>'success','blockDay'=>$blockDay, 'timeslots'=>$doctorTimeSlots]);
		}else{
			return response()->json(['status'=>'success','blockDay'=>$blockDay, 'timeslots'=>$timeslots]);

		}

	}
	
	public function invitedetail($invitation_id,$activeTab = null){
       
		$invitation = user_invitations::where('unique_id',$invitation_id)->first();
		$patient = $invitation->patient;		
		Helper::clear_un_read_messages($invitation->invitee_user_id,$invitation->id);
		$doctor = doctors::select(['id','category_id','actual_price','lateCancellationFee'])->where('user_id',$invitation->invitee_user_id)->first();
		$user = $invitation->user;
		$doctor_invoice_number = Invites_Payments::orderBy('doctor_invoice_number', 'DESC')->pluck('doctor_invoice_number')->first();
		
		$showUpload = 'false';
		$check = Tracability::where("invitation_id",$invitation->id)->where('status','4')->get();
		if(count($check) > 0 && $invitation->status == 'active'){
			$showUpload = 'true';
		}

		if($invitation->status == "cancelled"){
			$category = "Cancellation fee";
			$doctor_invoice_price = $doctor->lateCancellationFee;
		}
		else{
			$category = Category::find($invitation->invitee_category_id);
			$category = $category->category_name;
			$doctor_invoice_price = $doctor->actual_price;
		}
	
		return view('/pages/doctor/invitedetail',[
			'invitation'=>$invitation,
			'showUpload'=>$showUpload,
			'patient' => $patient, 
			'activeTab'=>$activeTab,
			'doctor'=>$doctor,
			'user' => $user,
			'doctor_invoice_number'=>$doctor_invoice_number,
			'category' => $category,
			'doctor_invoice_price' => $doctor_invoice_price
		]);		
	}
	
	/*Upload Reports*/
	public function uploadreports(Request $request)
    {
		
		$intid=$request->newintid;
		$patientid=$request->patientid;
		$total=$request->total;
		$current=$request->current;
		$invitation = user_invitations::find($intid);

		$filep = $request->file('file')->path();
		$image = base64_encode(file_get_contents($filep));
		$image = MyHelper::Encrypt($image,$invitation->enceypKey);
		$chnagedname= date('Y').'/'. time().$intid.'_'.Str::random(18).'.txt';

		$file = $request->file('file');
		$ext=$file->extension();
		$name = MyHelper::clearnName($file->getClientOriginalName(),$ext);
		
		Storage::disk('local')->put('uploads/medical_reports/'.$chnagedname, $image);						
		$imgnameData[] = $name; 				
		$imgData[] = $chnagedname; 	

		$user = InvitationAttachment::where('invitation_id', $intid)->first(); 

		if(empty($user->reportfilename) || $user->reportfilename == 'null'){
			$tracability = new Tracability();
			$tracability->date = date('Y-m-d H:i:s');
			$tracability->invitation_id	 = $intid;
			$tracability->status = '5';
			$tracability->save();

			$tracability = new Tracability();
			$tracability->date = date('Y-m-d H:i:s');
			$tracability->invitation_id	 = $intid;
			$tracability->status = '6';
			$tracability->save();

			// not _her
			$data = Patient::find($invitation->patient_user_id);
			$doctor = User::find($invitation->invitee_user_id);

			$des = MyHelper::c_strings()["report_alert"];
			$des = str_replace("@patient",$data->name,$des);
			$des = str_replace("@doctor",$doctor->name,$des);

			$notif = new Nofication();
			$notif->url = "patient_details/".$invitation->patient_user_id."?doctorid=".$invitation->invitee_category_id;
			$notif->description = $des;;
			$notif->to_id = $invitation->inviter_user_id;
			$notif->from_id = $invitation->invitee_user_id;
			$notif->save();
		}


		if($user!=null){
			if($user->reportfilename!=null){				
				$filenamenew=$imgData;
				$filenamenew1=json_decode($user->reportfilename);
				$cgefilename=array_merge($filenamenew, $filenamenew1);
				 $cgefilename = array_values($cgefilename);
				$user->reportfilename=json_encode($cgefilename);
				
				$imgnamenew=$imgnameData;
				$imgnamenew1=json_decode($user->reportimagename);
				$cgeimagename=array_merge($imgnamenew, $imgnamenew1);
				 $cgeimagename = array_values($cgeimagename);
				$user->reportimagename=json_encode($cgeimagename);				
				$user->save();	

				 if($user->id && (($total == 1 && $current == 0) || ($total-1 == $current)) ){
					$invitation = user_invitations::find($user->invitation_id);
					$invitation->report_status = '1';
					$invitation->save();

					$emailCtrl = new EmailController();
					$emailCtrl->send_notification_email_to_lawyer_after_doctor_upload_reports($invitation);
				 }	
			}
			else{
				$user->reportfilename=json_encode($imgData);
				$user->reportimagename=json_encode($imgnameData);
				$user->save();
				if($user->id && (($total == 1 && $current == 0) || ($total-1 == $current))){
					$emailCtrl = new EmailController();
					$invitation = user_invitations::find($user->invitation_id);
					$emailCtrl->send_notification_email_to_lawyer_after_doctor_upload_reports($invitation);
				}	
			}
		}
		else{
				$data1 = new InvitationAttachment();
				$data1->invitation_id=$intid;
				$data1->reportfilename=json_encode($imgData);
				$data1->reportimagename=json_encode($imgnameData);
				$data1->save();	

				if($data1->id && (($total == 1 && $current == 0) || ($total-1 == $current))){
					$emailCtrl = new EmailController();
					$invitation = user_invitations::find($data1->invitation_id);
					$emailCtrl->send_notification_email_to_lawyer_after_doctor_upload_reports($invitation);
				}
		
		}

		$pt_data = User::select("id")->where('patient_id',$invitation->patient_user_id)->first();
		$des_1 = "Doctor: ".auth()->user()->name." has uploaded the final report for your appointment created by your lawyer.";
		$notif_1 = new Nofication();
		$notif_1->description =  $des_1;
		$notif_1->url = 'invitation_details_view/'.$invitation->id;
		$notif_1->status = '0';
		$notif_1->to_id = $pt_data->id;
		$notif_1->from_id = auth()->user()->id;
		$notif_1->save();

		return response()->json(
			[
			'status'=>'1',
			'message'=>'Files Uploaded Successfully',
			'reportsid'=>$intid,
			'total'=>$request->total,
			'current'=>$request->current,
		]);             
 
    }	
	
	/*delete documents*/
	public function deletereports(Request $request){
		 $user = InvitationAttachment::where('invitation_id', $request->newintid)->first();
		 Storage::disk('local')->delete('uploads/medical_reports/'.$request->DeleteDocsid);
			
		 $newfilename=$request->DeleteDocsid;
		 $num_array=json_decode($user->reportfilename);
		 $num_array=json_decode( json_encode($num_array), true);	
		 $key = array_search($newfilename, $num_array);		
		 unset($num_array[$key]);
		 $num_array=array_values($num_array);
		 
		 $newimagename=$request->DeleteDocsname;
		 $num_array1=json_decode($user->reportimagename);
		 $num_array1=json_decode( json_encode($num_array1), true);		
		 $key1 = array_search($newimagename, $num_array1);	
		 $key1 = empty($key1) ? $key : $key1;
		 unset($num_array1[$key1]);	
		 $num_array1=array_values($num_array1);
		 
		$num_array1count=count($num_array1);
		$num_arraycount=count($num_array);
		//exit;
		 if($num_array1count=='0'){

			$invitation = user_invitations::find($user->invitation_id);
			$invitation->report_status = '0';
			$invitation->save();

			 $user->reportimagename='';
			 $user->reportfilename='';
			 Tracability::where("invitation_id",$user->invitation_id)->where('status','5')->orWhere('status','6')->delete();
			 $user->save();
		 }else{
		   $user->reportimagename=json_encode($num_array1);
		   $user->reportfilename=json_encode($num_array);
		   $user->save();
		 }
		$invitation = user_invitations::select('unique_id')->where('id',$request->newintid)->first();
		return response()->json(['status'=>'success','activeliid'=>$request->reportsactiveliid, 'int_id'=>$request->newintid]); 
	}	
	
	
	public function deletecv(Request $request){		
		$cvuserid=$request->cvuserid;
		$cvuserpic=$request->cvuserpic;	
		
		$user = User::where('id', $cvuserid)->first(); 
		$a='uploads/cv/'.$user->doctor->cvimage;
		unlink($a);
		$user->doctor->cvimage = "";
		$user->doctor->save();
		if(isset($user->doctor->id)){ 
			return response()->json(['status'=> "success",'url'=>url('doctor-profile')]);
		}
	}
	
	public function deleteInsform(Request $request){		
		$insuserid=$request->insuserid;
		
		$user = User::where('id', $insuserid)->first(); 
		$a='uploads/instruction_form/'.$user->doctor->instruction_form;
		unlink($a);
		$user->doctor->instruction_form = "";
		$user->doctor->save();
		if(isset($user->doctor->id)){ 
			return response()->json(['status'=> "success",'url'=>url('doctor-profile')]);
		}
	}
	
	/*Upload Invoice*/
	/*public function uploadinvoice(Request $request)
    {
		$validatedData = $request->validate([
       	'invoicefiles.*' => 'required|file|mimes:pdf,doc,docx,zip|max:4048',
        ]);		
		$intid=$request->invoicenewintid;
		$doctorid=$request->invoicedoctorid;
		$date=date("Y-m-d");
        if($request->TotalFiles > 0)
        {                
           for ($x = 0; $x < $request->TotalFiles; $x++) 
           { 
               if ($request->hasFile('files'.$x)) 
                {
                    $file = $request->file('files'.$x);                    
					$ext=$file->extension();
					$chnagedname= time().$x.'.'.$ext;							
									
					$imgData[] = $chnagedname; 				
					$file->move(public_path().'/uploads/invoice/', $chnagedname);//move to folder					
                }				
           }		   
		    $user = Invites_Payments::where('invitation_id', $intid)->first(); 
			if($user!=null){
				if($user->invoice_filename!=null){				
					$filenamenew=$imgData;
					$filenamenew1=json_decode($user->invoice_filename);
					$cgefilename=array_merge($filenamenew, $filenamenew1);
					 $cgefilename = array_values($cgefilename);
					$user->invoice_filename=json_encode($cgefilename);
					$user->invoice_date=$date;								
					$user->save();	
				}
				else{
					$user->invoice_date=$date;
					$user->invoice_filename=json_encode($imgData);
					$user->save();
				}
			}
			else{
				$data1 = new Invites_Payments();
				$data1->invitation_id=$intid;
				$data1->doctor_user_id=$doctorid;
				$data1->invoice_date=$date;
				$data1->invoice_filename=json_encode($imgData);
				$data1->save();			
		    }
			$invitation = user_invitations::select('unique_id')->where('id',$intid)->first();
            return response()->json(['success'=>'Files Uploaded Successfully','url'=>url('invitedetail')."/".$invitation->unique_id."/files-reports"]);             
        } 
        else
        {
           return response()->json(["message" => "Please try again."]);  
        }    
 
    }*/
	
	/*delete inoice*/
	public function deleteinvoice(Request $request){
		
		 $user = Invites_Payments::where('invitation_id', $request->invoiceintdid)->first();

		 $delimgfile='uploads/invoice/'.$request->invoiceintdname;
		 unlink($delimgfile);	
			
		 $newfilename=$request->invoiceintdname;
		 $num_array=json_decode($user->invoice_filename);
		 $num_array=json_decode( json_encode($num_array), true);		
		 $key = array_search($newfilename, $num_array);		
		 unset($num_array[$key]);
		 $num_array=array_values($num_array);
		 
		$num_arraycount=count($num_array);
		//exit;
		 if($num_arraycount=='0'){
			 $user->invoice_filename='';
			 $user->save();
		 }else{
		 $user->invoice_filename=json_encode($num_array);
		 $user->save();
		 }
		$invitation = user_invitations::select('unique_id')->where('id',$request->invoiceintdid)->first();
		 return response()->json(['status'=>'success','url'=>url('invitedetail')."/".$invitation->unique_id."/files-reports"]);  
	}

	public function deleteMedicalReport(Request $request){ //aabb
		// type =>  1 - Medical, 2 - Summary, 3 - Letter en, 4 - form, 5 - Questionari
	  $invitationID = $request->assessID;
	  $type = $request->type;
	  $user = InvitationAttachment::where('invitation_id',$invitationID)->first();
		  $files = json_decode($user->reportfilename);
		  foreach($files as $url){
			  Storage::delete('uploads/medical_reports/'.$url);
		  }
		  $user->reportfilename='';
		  $user->reportimagename='';
	  $user->save();
	  return response()->json(['status'=>1,'message'=>"Files deleted successfully"],200);
  }
	
	public function custominvoice(Request $request){ 
		    ini_set("pcre.backtrack_limit", "1000000");
			$date=date("Y-m-d");
			$invitation1 = user_invitations::select('unique_id')->where('id',$request->id)->first();
			$invitation = user_invitations::where('id',$request->id)->first();
			$user = Invites_Payments::where('invitation_id', $request->id)->first();
			$doctor_invoice_number = Invites_Payments::orderBy('doctor_invoice_number', 'DESC')->pluck('doctor_invoice_number')->first();
			$pdf = md5(time()).'.pdf';
			
			if($user!=null){
                   
					$user->payment_amount=$request->billamt;	

					$user->serviceprovided=$request->serviceprovided;
					$user->invoice_date=$date;
					
					$delimgfile='uploads/custom_invoice/'.$user->invoice_filename;
					if(file_exists($delimgfile))
					unlink($delimgfile);
					
					$user->invoice_filename=$pdf;								
					$user->save();	
					if($user->id){						
						$upload_dir = public_path(); 
						$filename = $upload_dir.'/uploads/custom_invoice/'.$pdf; 
						$mpdf = new \Mpdf\Mpdf();
						$mpdf->WriteHTML(\View::make('pages.doctor.invoicemail')->with('invitation',$invitation)->render());
						$mpdf->Output($filename,'F');
						
						$emailCtrl = new EmailController();
						$emailCtrl->send_invoice_to_lawyer($invitation); 						
						
						return response()->json(['status'=>'success','success'=>'Invoice Sent Successfully','url'=>url('invitedetail')."/".$invitation1->unique_id."/files-reports"]); 
					} 
			}
			else{
				$data1 = new Invites_Payments();
				$data1->invitation_id=$request->id;
				$data1->doctor_user_id=$request->docid;
				$data1->invoice_date=$date;
				$data1->payment_amount=$request->billamt;
				$data1->invoice_filename=$pdf;
				$data1->serviceprovided=$request->serviceprovided;
				if($doctor_invoice_number==null){
					$data1->doctor_invoice_number=1;
				}
				else{
					$data1->doctor_invoice_number=$doctor_invoice_number+1;
				}
				
				$data1->save();	
				if($data1->id){
 				try{
					$upload_dir = public_path(); 
					$filename = $upload_dir.'/uploads/custom_invoice/'.$pdf;  
					$mpdf = new \Mpdf\Mpdf();
					$mpdf->useSubstitutions = false;
					$mpdf->simpleTables = true;
					$mpdf->WriteHTML(\View::make('pages.doctor.invoicemail')->with('invitation',$invitation)->render(),\Mpdf\HTMLParserMode::HTML_BODY);
					$mpdf->Output($filename,'F');
					}
					catch(\Mpdf\MpdfException $e){
						echo "==> ".$e; 
					}
					$emailCtrl = new EmailController();
					$emailCtrl->send_invoice_to_lawyer($invitation); 
						
					return response()->json(['status'=>'success','success'=>'Invoice Sent Successfully','url'=>url('invitedetail')."/".$invitation1->unique_id."/files-reports"]);   
				}
		    }
		
	}
	public function doctor_invoice_modal_view(Request $request){
		$invitation= user_invitations::where('id', $request->id)->first(); 
		
		$doctorname=ucfirst($invitation->user1->name);
		$doctorlicencenumber=ucfirst($invitation->user1->doctor->licence_number);
		$doctorphone=$invitation->user1->phone;
		$invitationdate=date('F d, Y', strtotime($invitation->invitation_date));
		$patientname=ucfirst($invitation->patient->name);
		$patientdateofbirth=date('F d, Y', strtotime($invitation->patient->dateofbirth));
		$patientdateofaccident=date('F d, Y', strtotime($invitation->patient->dateofaccident));
		$serviceprovided=ucfirst($invitation->invoices->serviceprovided);
		$date=date("Y-m-d"); 
		$todaydate=date('F d, Y', strtotime($invitation->invoices->invoice_date));
		$billamt=$invitation->invoices->payment_amount;
		$doctor_invoice_number=$invitation->invoices->doctor_invoice_number;
		$doctorhst=$invitation->user1->doctor->hst;

		$category = Helper::categorydetails();  
		foreach ($category as $categories){ 
		if(($categories->id)==($invitation->user1->doctor->category_id)){
			$categoryname=ucfirst($categories->category_name); }
		}

		$subtotal=$invitation->invoices->payment_amount;
		$withouthstpercentage=($subtotal/1.13);
		$hstpercentageamount=($subtotal-$withouthstpercentage);

		$actualamount= number_format((float) $withouthstpercentage, 2, '.', '');
		$hstamount= number_format((float) $hstpercentageamount, 2, '.', '');
		$subtotal= number_format((float) $subtotal, 2, '.', '');

		return response()->json(['status'=>'success','doctorname'=>$doctorname,'categoryname'=>$categoryname,'doctorlicencenumber'=>$doctorlicencenumber, 'doctorphone'=>$doctorphone,'invitationdate'=>$invitationdate, 'patientname'=>$patientname, 'patientdateofbirth'=>$patientdateofbirth,'patientdateofaccident'=>$patientdateofaccident,'serviceprovided'=>$serviceprovided,'todaydate'=>$todaydate,'billamt'=>$actualamount,'doctor_invoice_number'=>$doctor_invoice_number,'doctorhst'=>$doctorhst,'hstamount'=>$hstamount, 'subtotal'=>$subtotal]);
	}

	
	public function test(Request $request, $activeTab=null)
    {	
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->WriteHTML('<h1>Hello world!</h1>');
		$upload_dir = public_path(); 
		$filename = $upload_dir.'/uploads/custom_invoice/testing1.pdf';
		$mpdf->Output($filename,'F');
		exit;
		// $sendint = user_invitations::find(6);
		// $emailCtrl = new EmailController();
		// $emailCtrl->send_invitation_confirmation_to_lawyer($sendint);  
		// die;
		$user = auth()->user();		
		// $doctorapp = Doctorappointment::where('doctor_id', $user->id)->select('id', 'notes','timeslot', 'appointment_date')->get();	
		$settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
		$expireTime =date('Y-m-d H:i:s',strtotime('-'.$settings->invitation_expire_in_minutes.' minutes'));
		
		$newList=$blocked_dates=array();
		$invitations = user_invitations::where('invitee_user_id',$user->id)->orderBy('id','desc')->get();	
		$new_invitations = $accepted_invitations =$confirmed_invitations= $expired_invitations = $rejected_invitations = $cancelled_invitations = [];
	
		$dueDateArray = $invitationDateArray=[];
		$un_blockable_dates=[];

		if($invitations) {
		foreach($invitations as $invitation) {
			if($invitation->status == 'pending' && strtotime($invitation->created_at) > strtotime($expireTime)) {
				$new_invitations[] = $invitation;
				$un_blockable_dates[] = intval(strtotime($invitation->invitation_date).'000');
			}else if($invitation->status == 'active' && $invitation->is_patient_accepted == 'yes') {
				if(isset($request->lawyerkeyword)){					
					if((strripos($invitation->user->name, $request->lawyerkeyword)!==false)){						
						$confirmed_invitations[] = $invitation;
					}elseif(strripos($invitation->patient->name, $request->lawyerkeyword)!==false){
						$confirmed_invitations[] = $invitation;
					}
					elseif(strripos($invitation->user->lawyer->lawfirmname, $request->lawyerkeyword)!==false){
						$confirmed_invitations[] = $invitation;
					}
				}else{ 
					$confirmed_invitations[] = $invitation;
				}
				$un_blockable_dates[] = intval(strtotime($invitation->invitation_date).'000');
				$invitationDateArray[] = $invitation->invitation_date.' 00:00:00';
				$dueDateArray[] = $invitation->duedate.' 00:00:00';
			}else if($invitation->status == 'rejected') {
				$rejected_invitations[] = $invitation;
			}else if($invitation->status == 'cancelled') {
				$cancelled_invitations[] = $invitation;
			}else if($invitation->status == 'active' && $invitation->is_patient_accepted == 'no') {
				$accepted_invitations[] = $invitation;
				//$invitationDateArray[] = $invitation->invitation_date.' 00:00:00';
				$dueDateArray[] = $invitation->duedate.' 00:00:00';
				$un_blockable_dates[] = intval(strtotime($invitation->invitation_date).'000');
			}else if($invitation->status == 'pending' && strtotime($invitation->created_at) <= strtotime($expireTime)) {
				$expired_invitations[] = $invitation;
			}

			if(($invitation->status == 'pending' && strtotime($invitation->created_at) <= strtotime($expireTime)) || $invitation->status == 'rejected' || $invitation->status == 'cancelled') {
				continue;
			}
			
			$newList1[] = [				
				'id'   =>  $invitation['unique_id'],
				'name' =>  'Assessment  Accepted',				
				'date' => $invitation['invitation_date'],
				'timeslot' => $invitation['timeslot'],						
				'patientname' => $invitation->patient->name,	
				'docid' => $invitation->inviter_user_id,
				'type'  => "holiday", 
				];
			$newList2[] = [				
				'id'   =>  $invitation['unique_id'],
				'name' =>    'Report Due Date',
				'date' => $invitation['duedate'],
				'timeslot' => $invitation['timeslot'],  						
				'patientname' => $invitation->patient->name,	
				'docid' => $invitation->inviter_user_id,
				'type'  => "event", 
				];				
				$newList = array_unique(array_merge($newList1, $newList2),SORT_REGULAR);	
			}
		}	
		$blockedDates = Holiday::where('doctor_user_id',$user->id)->where('date','>=',date('Y-m-d'))->get()->pluck('date')->toArray();
		if($blockedDates) {
			foreach($blockedDates as $blockedDate) {
				$blocked_dates[] = intval(strtotime($blockedDate).'000'); 
			}
		}
		$invoices = Invites_Payments::where('doctor_user_id', $user->id)->orderBy('id', 'desc')->paginate(10);
		$suminvoices = Invites_Payments::where('doctor_user_id', $user->id)->sum('payment_amount');
	/*	$suminvoices = Invites_Payments::query();
		$suminvoices->whereHas('user_invitations', function($q){
				$q->where('status', '=', 'active');  });
		$suminvoices=$suminvoices->sum('payment_amount'); */

		return view('/test',['newList' => $newList, 'users' => $user,'invoices' => $invoices, 'suminvoices' => $suminvoices,'expire_time'=>$expireTime,'invitations' => $invitations, 'new_invitations'=>$new_invitations,'accepted_invitations'=>$accepted_invitations, 'expired_invitations'=>$expired_invitations,'confirmed_invitations'=>$confirmed_invitations,'rejected_invitations'=>$rejected_invitations,'cancelled_invitations'=>$cancelled_invitations, 'dueDateArr'=>json_encode($dueDateArray),'invitationDateArr'=>json_encode($invitationDateArray),'activeTab'=>$activeTab,'blockedDates'=>json_encode($blocked_dates),'un_blockable_dates'=>json_encode($un_blockable_dates)]); 	
    }

	/*public function doctor_new_invitations_check(){

		$expireTime =date('Y-m-d H:i:s',strtotime('-1 minutes'));		
		$user = auth()->user();	
		$doctor_new_invitations_check = user_invitations::where('invitee_user_id',$user->id)->where('status','pending')->where('created_at','>',$expireTime)->first();
		if(isset($doctor_new_invitations_check)){
		return response()->json(['status'=> "success",'message'=>'Invitation found']);
		}else{
			return response()->json(['status'=> "false",'message'=>'No Invitation found']);
		}
	}*/

	public function check_doctor_new_invitations(Request $request)
	{
		//print_r($request->all()); exit;

		if(Auth::user()->role_id == '4'){
			$docID = $request->doctorID;
		}else{
			$docID = Auth::id();
		}

		$duration = $request->interval_duration;
		$check_time =  date('Y-m-d H:i:s',strtotime('-'.$duration.' seconds'));
		$invitation = user_invitations::select(['invitation_date'])->where('invitee_user_id',$docID)->where('status','pending')->where('created_at','>=',$check_time)->first();
		if($invitation){
			return ['data'=>['count'=>1, 'invitation_date'=>date('m/d/Y',strtotime($invitation->invitation_date))],'message'=>'new invitation count fetched successfully.','status'=>'success'];
		}else{
			return ['data'=>['coont'=>0],'message'=>'new invitation count fetched successfully.','status'=>'success'];
		}
	}


		public function check_doctor_confirmed_invitations(Request $request)
		{
		$duration = $request->interval_duration;
		$check_time =  date('Y-m-d H:i:s',strtotime('-'.$duration.' seconds'));	
		$invitation = user_invitations::select(['invitation_date'])->where('invitee_user_id',Auth::id())->where('status','active')->where('patient_accepted_on','>=',$check_time)->first();
			if($invitation){
				return ['data'=>['count'=>1, 'invitation_date'=>date('m/d/Y',strtotime($invitation->invitation_date))],'message'=>'confirmed invitation count fetched successfully.','status'=>'success'];
			}else{
				return ['data'=>['coont'=>0],'message'=>'confirmed invitation count fetched successfully.','status'=>'success'];
			}
		}

		public function doctor_confirmed_archive(Request $request){	
			
			if(is_array($request->archive))	{	
			foreach($request->archive as $row){
				$invitation = user_invitations::find($row);
				$invitation->is_archive='yes';
				$invitation->save();		
			}
			return redirect()->route('doctor-dashboard/archived-invitations')->with('archivedsuccess','Invitations Archived Successfully!');
		  }else{
		  	return redirect()->route('doctor-dashboard/archived-invitations');
		  }
		}


		public function uploadQuestionair(Request $request){
			//abs
			if(Auth::user()->role_id == '4'){
				$doctor = doctors::where('user_id', $request->docID)->first();
			}
			else{
				$doctor = doctors::where('user_id', Auth::id())->first();
			}

			if($request->hasFile('file')) {
            
				if(!empty($doctor->instruction_form) && $doctor->instruction_form != null){	
					$path = explode('||||',$doctor->instruction_form)[1];
					$delcvfile1='uploads/instruction_form/'.$path ;
					unlink($delcvfile1);
				}

				$insfile1 = $request->file('file');
				$organialname = $request->file('file')->getClientOriginalName();
				$insfilename = time()."_".$doctor->unique_id.'.'.$insfile1->getClientOriginalExtension();
				$insfile1->move('uploads/instruction_form', $insfilename);
                $doctor->instruction_form = $organialname.'||||'.$insfilename;
				$doctor->save();
				return response()->json(['status'=> 1, 'message'=>"File uploaded successfully","filePath"=>$insfilename,'name'=>$organialname,'pt'=>'uploads/instruction_form/'.$insfilename]);
			}
			else{
				return response()->json(['status'=> 0, 'message'=>"Invalid api request"]);
			}
			
		}

		public function doctorqpath($docID){
			if(Auth::user()->role_id == "4"){
				$doctor = doctors::where('user_id', $docID)->first();
			}
			else{
				$doctor = doctors::where('user_id', Auth::id())->first();
			}

			if(!empty($doctor->instruction_form) && $doctor->instruction_form != null){
				$path = explode('||||',$doctor->instruction_form);
				return response()->json(['status'=> 1, 'message'=>"Valid response",'pt'=>url('uploads/instruction_form/'.$path[1]),'name'=>$path[0]]);
			}
			else{
				return response()->json(['status'=> 0, 'message'=>"Valid response"]);
			}
		}

		public function delete_questionari($docID){
			if(Auth::user()->role_id == '4'){
				$doctor = doctors::where('user_id', $docID)->first();
			}
			else{
				$doctor = doctors::where('user_id', Auth::id())->first();
			}
			
			if(!empty($doctor->instruction_form) && $doctor->instruction_form != null){	
				$path = explode('||||',$doctor->instruction_form)[1];
				$delcvfile1='uploads/instruction_form/'.$path ;
				unlink($delcvfile1);
			}

			$doctor->instruction_form = '';
			$doctor->save();

			return response()->json(['status'=> 1, 'message'=>"File deleted successuflly"]);
		}

}
