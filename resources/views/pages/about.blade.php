@extends('layouts.default')
@section('content')

<link href="{{ asset('css/about.css')."?9484848" }}" rel="stylesheet">
<link href="{{ asset('css/home.css')."?0000" }}" rel="stylesheet">


<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
		
<link rel="stylesheet" href="{{asset('assets/carousel/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/carousel/css/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.5.6/css/ionicons.min.css">
{{-- <link rel="stylesheet" href="{{asset('assets/carousel/css/style.css')}}"> --}}


<style>
.topHomeContainerAbout{
    display: flex;
    flex-direction: column;
    background: url("{{asset('assets/about_banner.jpeg')}}");
}
.subcontainer{
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;
    background: rgba(32, 33, 94, 0.78);
}
</style>

<section class="topHomeContainerAbout">
    <div class="subcontainer">
    @include('includes.nheader')
    
    <div class="top-banner container">
       <div class="br-slide1">
           <h2>About Us</h2>
       </div>
    </div>
</div>
</section>


<section class="ab_subtitleContainer">
    <h2>Discovering Quality Medical Legal Solutions</h2>
    <h5>PRO-ASSESSORS- WEB-BASED PLATFORM FOR YOUR MED-LEGAL PROCESS</h5>
</section>

<div class="aboutContentSection">
        <p class="aboutContent" >Pro Assessors is proud to be the first seamless, web-based platform of its kind that simplifies and streamlines the process of reporting by connecting clients with legal services and medical specialties of their choice to obtain professional opinions, literally at a glance.
            <br><br>
            This service is the first of its kind enabling users to interact with providers to request medical assessments and obtain med-legal reports in an efficient and effective manner by employing faster and simpler working methods.
            Book the required assessment and obtain a report. This service is the first of its kind enabling users to interact with providers to request medical assessments and obtain med-legal reports
        </p>
</div>

{{-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% --}}
    <section class="container-fluid p-0 m-0" style="overflow: hidden;">
        <div class="row ab-topcard-container">
            <div class="col-md-12">
                <div class="featured-carousel owl-carousel">

                    <div class="item">
                        <div class="item11">
                            <div style="flex: 1;justify-content: flex-end;display: flex;align-items: center;">
                                <img src="assets/hammer.png" style="opacity: 0.3;width: 30px;" alt="">
                            </div>
                            <div style="flex:2.5">
                                <p>
                                    Pro Assessors is proud to be the first seamless, web-based platform of its kind that simplifies and streamlines the process of reporting by connecting clients with legal services and medical specialties of their choice to obtain professional opinions, literally at a glance.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item11">
                            <div style="flex: 1;justify-content: flex-end;display: flex;align-items: center;">
                                <img src="assets/hammer.png" style="opacity: 0.3;width: 30px;" alt="">
                            </div>
                            <div style="flex:2.5">
                                <p>
                                    Pro Assessors is proud to be the first seamless, web-based platform of its kind that simplifies and streamlines the process of reporting by connecting clients with legal services and medical specialties of their choice to obtain professional opinions, literally at a glance.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item11">
                            <div style="flex: 1;justify-content: flex-end;display: flex;align-items: center;">
                                <img src="assets/hammer.png" style="opacity: 0.3;width: 30px;" alt="">
                            </div>
                            <div style="flex:2.5">
                                <p>
                                    Pro Assessors is proud to be the first seamless, web-based platform of its kind that simplifies and streamlines the process of reporting by connecting clients with legal services and medical specialties of their choice to obtain professional opinions, literally at a glance.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item11">
                            <div style="flex: 1;justify-content: flex-end;display: flex;align-items: center;">
                                <img src="assets/hammer.png" style="opacity: 0.3;width: 30px;" alt="">
                            </div>
                            <div style="flex:2.5">
                                <p>
                                    Pro Assessors is proud to be the first seamless, web-based platform of its kind that simplifies and streamlines the process of reporting by connecting clients with legal services and medical specialties of their choice to obtain professional opinions, literally at a glance.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item11">
                            <div style="flex: 1;justify-content: flex-end;display: flex;align-items: center;">
                                <img src="assets/hammer.png" style="opacity: 0.3;width: 30px;" alt="">
                            </div>
                            <div style="flex:2.5">
                                <p>
                                    Pro Assessors is proud to be the first seamless, web-based platform of its kind that simplifies and streamlines the process of reporting by connecting clients with legal services and medical specialties of their choice to obtain professional opinions, literally at a glance.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item11">
                            <div style="flex: 1;justify-content: flex-end;display: flex;align-items: center;">
                                <img src="assets/hammer.png" style="opacity: 0.3;width: 30px;" alt="">
                            </div>
                            <div style="flex:2.5">
                                <p>
                                    Pro Assessors is proud to be the first seamless, web-based platform of its kind that simplifies and streamlines the process of reporting by connecting clients with legal services and medical specialties of their choice to obtain professional opinions, literally at a glance.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item11">
                            <div style="flex: 1;justify-content: flex-end;display: flex;align-items: center;">
                                <img src="assets/hammer.png" style="opacity: 0.3;width: 30px;" alt="">
                            </div>
                            <div style="flex:2.5">
                                <p>
                                    Pro Assessors is proud to be the first seamless, web-based platform of its kind that simplifies and streamlines the process of reporting by connecting clients with legal services and medical specialties of their choice to obtain professional opinions, literally at a glance.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="item11">
                            <div style="flex: 1;justify-content: flex-end;display: flex;align-items: center;">
                                <img src="assets/hammer.png" style="opacity: 0.3;width: 30px;" alt="">
                            </div>
                            <div style="flex:2.5">
                                <p>
                                    Pro Assessors is proud to be the first seamless, web-based platform of its kind that simplifies and streamlines the process of reporting by connecting clients with legal services and medical specialties of their choice to obtain professional opinions, literally at a glance.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
{{-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% --}}

<section class="about_new_section_11">
    <div class="first">
        <div class="legal">
                        
            <div style="flex:6;display: flex;flex-direction: column">
                <div style="flex: 1;align-items: center;display: flex;flex-direction: column;">
                    <p class="ab-menuTitle">Legal Experts</p>                    
                    <p class="ab-menuSecondTitl">What actions can be taken to support lawyers</p>
                </div>  

                <div style="flex:6;">
                    <div style="display: flex">
                        <img src="assets/hammer.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Simplify their process to obtain expert opinion with diverse and expansive specialties available</p>
                    </div>
                    <div style="display: flex">
                        <img src="assets/hammer.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Easily request for expert reports for their clients with a click at any time</p>
                    </div>
                    <div style="display: flex">
                        <img src="assets/hammer.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Ensure secure and private mode of transmission</p>
                    </div>
                    <div style="display: flex">
                        <img src="assets/hammer.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Allows easy traceability</p>
                    </div>
                    <div style="display: flex">
                        <img src="assets/hammer.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Timely response to their request.</p>
                    </div>
                    <div style="display: flex">
                        <img src="assets/hammer.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Reduces staff time and associated costs</p>
                    </div>
                    <div style="display: flex">
                        <img src="assets/hammer.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Reduces human errors and miscommunications</p>
                    </div>
                    <div style="display: flex">
                        <img src="assets/hammer.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Meets your deadline</p>
                    </div>
                    <div style="display: flex">
                        <img src="assets/hammer.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">No upfront fees or cost for the expert opinions</p>
                    </div>
                    <div style="display: flex">
                        <img src="assets/hammer.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Incur zero material costs</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="second">
        <div class="why">
            <p>Why us?</p>
            <img src="assets/verticalicons.png" alt="">
        </div>
    </div>

    <div class="third">
        <div class="medical">           
            <div style="flex:6;display: flex;flex-direction: column">
                <div style="flex: 1;align-items: center;display: flex;flex-direction: column;">
                    <p class="ab-menuTitle">Medical Experts</p>                    
                    <p class="ab-menuSecondTitl">What actions can be taken to support Specialist & medical experts</p>
                </div>                
                <div style="flex:6;">
                    <div style="display: flex">
                        <img src="assets/doctor-s.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Creates increased Exposure of services to wider audience</p>
                    </div>
                    <div style="display: flex">
                        <img src="assets/doctor-s.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Trackability</p>
                    </div>
                    <div style="display: flex">
                        <img src="assets/doctor-s.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Reduces staff time and associated costs</p>
                    </div>
                    <div style="display: flex">
                        <img src="assets/doctor-s.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Getting paid efficiently and promptly</p>
                    </div>

                    <div style="display: flex">
                        <img src="assets/doctor-s.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Security and privacy</p>
                    </div>

                    <div style="display: flex">
                        <img src="assets/doctor-s.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Reduces human errors and miscommunications</p>
                    </div>

                    <div style="display: flex">
                        <img src="assets/doctor-s.png" width="20px;" height="20px" alt="">
                        <p class="ab-menulisting">Simples booking and accounting</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

</section>




@include('common.cookiePermissionPopup')

<script src="{{asset('assets/carousel/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/carousel/js/popper.js')}}"></script>
{{-- <script src="{{asset('assets/carousel/js/bootstrap.min.js')}}"></script> --}}
<script src="{{asset('assets/carousel/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/carousel/js/main.js')}}"></script>
@endsection
