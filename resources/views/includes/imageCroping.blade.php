{{-- $$$$$$$$$$$$$$$$$$$$ Image croping popup start $$$$$$$$$$$$$$$$$$$$$ --}}
<link href="{{ asset("css/croppie.css") }}" rel="stylesheet">

<div class="modal fade" id="imageupload_modal" tabindex="-1" data-backdrop="static" data-keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">Profile image uploading</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
		 
			<div class="container-fluid">
			 
				<div class="panel panel-default">
				  <div class="panel-body">
					  <div class="row">
						  <div class="col-12 text-center">
							<div id="upload-image"></div>
						  </div>
					  </div>
					  <div class="row">
						<div class="col-12">
							<div id="cropImagAlert" class="alert alert-danger" style="display: none" role="alert">
								Invalid file type, you can only upload png, jpg and jpeg
							</div>
						</div>
						<div class="col-12">
							<div id="profileupload_prog" style="display: none">
								<img width="35" src="{{asset('img/loading.gif')}}" />
								<span>Uploading...</span>
							</div>
							
							<input type="file" id="images">
							<button type="button" id="uploadcrpImg" class="btn btn-outline-success cropped_image" style="border-radius: 10px;display:none;margin-left:20px;">Upload Image</button>
						</div>
									  
						  <div class="col-12 crop_preview">
							<div id="upload-image-i">
							</div>
						  </div>
					  </div>
				  </div>
				</div>			
			</div>

		</div>
	  </div>
	</div>
</div>
{{-- $$$$$$$$$$$$$$$$$$$$ Image croping popup end   $$$$$$$$$$$$$$$$$$$$$ --}}


<script>
function chooseImageModal(){
  $(".cr-image").attr('src','');
  $("#uploadcrpImg").hide();
  $("#images").val(null);
  $("#images").show();
  $("#profileupload_prog").hide();
  $("#imageupload_modal").modal('show');
}
</script>