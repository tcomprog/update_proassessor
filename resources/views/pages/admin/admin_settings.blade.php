@extends('layouts.admin_app')



@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">

             <nav aria-label="breadcrumb">

              <ol class="breadcrumb">

                <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>

                <li class="breadcrumb-item active"><a href="#">Settings</a></li>

              </ol>

            </nav>

        </div>
        <div class="col-md-9"></div>
        <div class="col-md-3"><a href="{{url('admin/addsettings')}}" class="btn btn-primary" style="float:right;">Add Settings</a></div>
        <br><br><div class="col-md-12">
                @if (Session::has('success'))
                   <div class="mt-2 alert alert-success">{{ Session::get('success') }}</div>
                 @endif
                 @if ($settings->isNotEmpty())

                               <table class="table">
                                  <thead>
                                    <tr>
                                      <th scope="col">S.No</th>
                                      <th scope="col">Settings Name</th>
                                      <th scope="col">Settings Value</th>
                                      <th scope="col">Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                     <?php $number=1; ?>
                                    <?php   foreach($settings as $setting){    ?>
                                    <tr>                                       
                                      <th scope="row">{{$number}}</th>
                                      <?php $number++; ?>
                                      <td>{{$setting->settings_display_name}}</td>
                                      <td>{{$setting->settings_value}}</td>
                                      <td><a href="{{url('admin/editsettings')}}/{{$setting->id}}">Edit</a></td>
                                    </tr>
                                <?php   }    ?>
                                  </tbody>
                                </table>
                 
                 @endif
        </div>

        

    </div>

</div>

@endsection

