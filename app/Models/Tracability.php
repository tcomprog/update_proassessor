<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tracability extends Model
{
    use HasFactory;
    protected $table = "tracability";
    protected $primaryKey = 'id';
}
