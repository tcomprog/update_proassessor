<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Config;


class PatientAccountCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $option = [];


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($option)
    {
        $this->option = $option;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $fromAddress = Config::get('constants.MAIL_FROM_ADDRESS');
        $site_title_old = Config::get('constants.site_title');
        $site_title=str_replace("-"," ",$site_title_old);
        $suject = "Account created!";

        return $this->subject($suject)->from($fromAddress,$site_title)->view('emails.patient_account_created',[
            'patient_name' => $this->option['patient_name'],
            'lawyer_name' => $this->option['lawyer_name'],
            'username' => $this->option['username'],
            'password' => $this->option['password'],
            'site_title'=>$site_title,
        ]);
    }
}
