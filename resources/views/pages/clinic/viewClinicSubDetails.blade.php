@extends('layouts.default')
@section('content')

<style>
    @font-face {
          font-family: Inter;
          src: url("{{asset('fonts/Inter-Regular.ttf')}}");
    }
</style>


<link rel="stylesheet" href="{{asset('css/invitedetail.css')}}?888888">

<link href="{{ asset('css/about.css') }}?99999999" rel="stylesheet">
<link href="{{ asset('css/subscription.css') }}?99999999" rel="stylesheet">
<link href="{{ asset('css/home.css') }}?99999999" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.css">



<style>
.topHomeContainerAbout{
    display: flex;
    flex-direction: column;
    background: url("{{asset('assets/about_banner.jpeg')}}");
}

.sub_card > .header{
    background: url("{{asset('n_img/offer_top.png')}}");
    height: 150px;
    width:100%;
    display: flex;
    justify-content: center;
    align-items: center;
    background-repeat: no-repeat;
    background-size: contain;
    background-position: center;
}
th,td{
    padding: 10px 6px;
}
th{
    color:#20215E;
}
</style>

<section class="topHomeContainerAbout">
<div class="subcontainer">
    @include('includes.nheader')

    <div class="top-banner container">
       <div class="br-slide1">
        <h2 style="font-family: 'DubaiBold' !important; margin-top:30px">Subscription</h2>
       </div>
    </div>
</div>
</section>



<div class="container mt-5">
    <div class="row">
        <div class="col-12 pt-4 pb-3 subsciption_heading">
            <h1>Your Subscription Details</h1>

            <div class="row">
                <div class="col-md-5 col-sm-12">
                    <div class="singlep_container">
                        <p>Package</p>
                        <p class="p-data">{{ucfirst($subscription->package)}}</p>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12">
                    <div class="singlep_container">
                        <p>Status</p>
                        <p class="p-data text-success">Active</p>
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-md-5 col-sm-12">
                    <div class="singlep_container">
                        <p>From Date</p>
                        <p class="p-data">{{date("d-m-Y",strtotime($subscription->from_date))}}</p>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12">
                    <div class="singlep_container">
                        <p>To Date</p>
                        <p class="p-data">{{date("d-m-Y",strtotime($subscription->to_date))}}</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>



<div class="container">
    <div class="row">
        <div class="col-12 pt-4 pb-2 subsciption_heading">
            <h5 class="mt-4">Transaction History</h5>
        </div>
    </div>
</div>


<section style="width:100%;margin:20px 0px 60px 0px;" >
<div class="container pb-5 pt-0">
    
    <table id="tableSorter" class="row-border hover cell-border" style="width:100%">
        <thead>
            <tr class="border-bottom border-top my-2">
                <th>Date</th>
                <th>Amount</th>
                <th>Payment ID</th>
                <th>Package</th>
                <th>From Date</th>
                <th>To Date</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($sub_tran as $row)
                <tr class="border-bottom my-2">
                    <td>{{date("d-m-Y",strtotime($row->date))}}</td>
                    <th>$CAD {{$row->amount}}</th>
                    <td>{{$row->payment_id}}</td>
                    <th>{{ucfirst($row->package)}}</th>
                    <td>{{date("d-m-Y",strtotime($row->from_date))}}</td>
                    <th>{{date("d-m-Y",strtotime($row->to_date))}}</th>
                </tr> 
            @endforeach
        </tbody>
    </table>

</div>
</section>

@endsection
