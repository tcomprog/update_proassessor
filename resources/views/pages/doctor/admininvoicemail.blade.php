
   <style>
         .invoicebody .clearfix:after {
         content: "";
         display: table;
         clear: both;
         }
         .invoicebody body{
         position: relative;
         width: 21cm;  
         height: 29.7cm; 
         margin: 0 auto; 
         color: #555555;
         background: #FFFFFF;
         font-family: Arial, sans-serif; 
         font-size: 14px; 
         font-family: 'Source Sans Pro', sans-serif;
         }
         .invoicebody .header {
         padding: 10px 0;
         margin-bottom: 20px;
         border-bottom: 1px solid #AAAAAA;
         }
         .invoicebody #logo {
         float: none;
         margin-top: 8px;
         text-align: center;
         }
         .invoicebody #logo img {
         height: 70px;
         }
         .invoicebody #company {
         float: right;
         text-align: right;
         }
         .invoicebody #details {
         margin-bottom: 30px;
         }
         .invoicebody #client {
         padding-left: 6px;
         border-left: 6px solid #00528c;
         float: left;
         }
         .invoicebody #client .to {
         color: #777777;
         }
         .invoicebody h2.name {
         font-size: 1.5em;
         font-weight: normal;
         margin: 0;
         margin-bottom: 5px;
         color: #000000;
         }
         .invoicebody #client .address{
          margin-bottom: 5px;
          font-size: 1.1em;
          color: #000000;
         }
         .invoicebody #client .email{
          margin-bottom: 5px;
          font-size: 1.1em;
         }
         .invoicebody #invoice {
         float: right;
         text-align: right;
         }
         .invoicebody #invoice h1 {
         color: #0087C3;
         font-size: 2.4em;
         line-height: 1em;
         font-weight: normal;
         margin: 0  0 10px 0;
         }
         .invoicebody #invoice .date {
         font-size: 1.1em;
         color: #000000;
         }
         .invoicebody .invoice-table,table {
         width: 100%;
         border-collapse: collapse;
         border-spacing: 0;
         margin-bottom: 20px;
         }
         .invoicebody .invoice-table th,
         .invoicebody .invoice-table td {
         padding: 20px;
         background: #ffffff;
         text-align: center;
         border-bottom: 1px solid #ddd;
         border-right: 1px solid #ddd;
         border-left: 1px solid #ddd;
         }
         .invoicebody .invoice-table th {
         white-space: nowrap;        
         font-weight: normal;
         }
         .invoicebody .invoice-table h4{
          margin: 0px 0px 5px;
         }
         .invoicebody .invoice-table td {
         }
         .invoicebody .invoice-table td h3{
         color: #000;
         font-size: 1.2em;
         font-weight: normal;
         margin: 0 0 0.2em 0;
         }
         .invoicebody .invoice-table .heading{
          color: #fff !important;
          font-size: 1.2em;
          background: #004b82;
         }
         .invoicebody .invoice-table .no {
         color: #000;
         font-size: 1.2em;
         text-align: left;
         }
         .invoicebody .invoice-table .desc {
         text-align: left;
         }
         .invoicebody .invoice-table .unit {
          color: #000;
         }
         .invoicebody .invoice-table .qty {
          color: #000;
         }
         .invoicebody .invoice-table .total {
         background: #57B223;
         color: #FFFFFF;
         }
         .invoicebody .invoice-table td.unit,
         .invoicebody .invoice-table td.qty,
         .invoicebody .invoice-table td.total {
         font-size: 1.2em;
         text-align: center;
         }
         .invoicebody .invoice-table tfoot td {
         padding: 10px 20px;
         background: #FFFFFF;
         font-size: 1.2em;
         white-space: nowrap; 
         border-top: 1px solid #AAAAAA; 
         }
         .invoicebody .invoice-table tfoot tr:first-child td {
         border-top: none; 
         }
         .invoicebody .invoice-table tfoot tr:last-child td {
         color: #000;
         font-size: 1.4em;
         border-top: 1px solid #57B223; 
         font-weight: 600;
         }
         .invoicebody .invoice-table tfoot tr td:first-child {
         border: none;
         }
         .invoicebody #thanks{
         font-size: 2em;
         margin-bottom: 50px;
         }
         .invoicebody #notices{
         padding-left: 6px;
         border-left: 6px solid #0087C3;  
         }
         .invoicebody #notices .notice {
         font-size: 1.2em;
         }
         .invoicebody .footer {
         color: #777777;
         width: 100%;
         height: 30px;
         position: absolute;
         bottom: 0;
         border-top: 1px solid #AAAAAA;
         padding: 8px 0;
         text-align: center;
         }
		 .cataddress{max-width:none;}
      </style>
   <div class="invoicebody">
      <div class="clearfix header">
         <div id="logo">
            <img style="height: 70px;" src="{{ asset('img/logo.png') }}">
         </div>
      </div>
      <div>
         <div id="details" class="clearfix" style="margin-bottom: 20px;">
            <table border="0" cellspacing="0" cellpadding="0">
               <tr>
                  <td style="border-left: 6px solid #00528c;padding-left: 10px;">
               <p style="font-family: 'Source Sans Pro', sans-serif;font-size: 19px;"><b style="font-weight: 800;">{{ $invitation->user1->name }}</b><p>
               <p style="color:#000;font-size: 6px;visibility: hidden;">-----</p>
               <p style="font-family: 'Source Sans Pro', sans-serif;margin-bottom: 10px;padding-bottom: 10px;font-size: 15px;"><?php  $category = Helper::categorydetails();  
									    foreach ($category as $categories){ 
											if(($categories->id)==($invitation->user1->doctor->category_id)){
												echo ucfirst($categories->category_name); }
											} ?>: {{ $invitation->user1->doctor->licence_number }}</p>								
					<p style="color:#000;font-size: 6px;visibility: hidden;">-----</p>
               <p style="font-family: 'Source Sans Pro', sans-serif;font-size: 15px;">PH: <a href="tel:{{ $invitation->user1->phone }}">{{ $invitation->user1->phone }}</a></p>
                  </td>
                  <td style="width:200px"></td>
                  <td align="right">
                  <p style="font-family: 'Source Sans Pro', sans-serif;font-size: 19px;"><b style="font-weight: 800;">Invoice #{{str_pad($invitation->invoices->lawyer_invoice_number, 2, '0', STR_PAD_LEFT)}}</b><P>
                     <p style="font-family: 'Source Sans Pro', sans-serif;font-size: 19px;"><b style="font-weight: 800;">Bill to: Pro Assessors</b><P>
                     <p style="color:#000;font-size: 6px;visibility: hidden;">-----</p>
                     <p style="font-family: 'Source Sans Pro', sans-serif;font-size: 15px;">Invoice Date: <?php $date=date("Y-m-d"); echo date('F d, Y', strtotime($date)); ?></p>
                  </td>
               </tr>
            </table>
         </div>
         <table class="invoice-table" border="0" cellspacing="0" cellpadding="0">
            <thead>
               <tr>
                  <th class="no heading" style="color:#fff;font-family: 'Source Sans Pro', sans-serif;">PATIENT</th>
                  <th class="desc heading" style="color:#fff;font-family: 'Source Sans Pro', sans-serif;">SERVICE PROVIDED</th>
                  <th class="unit heading" style="color:#fff;font-family: 'Source Sans Pro', sans-serif;">SERVICE DATE</th>
                  <th class="qty heading" style="color:#fff;font-family: 'Source Sans Pro', sans-serif;">FEE</th>
               </tr>
            </thead>
            <tbody>
              <!-- ROW -->
               <tr>
                  <td class="no" style="padding:16px;">
                    <h4 style="font-family: 'Source Sans Pro', sans-serif;font-weight: 700;">{{ $invitation->patient->name}}</h4>
                    <span style="color: #888;font-family: 'Source Sans Pro', sans-serif;font-size: 15px;">Date of Birth:</span> <span style="font-size: 15px;"><?php echo date('F d, Y', strtotime($invitation->patient->dateofbirth)); ?></span><br/>
                    <span style="color: #888;font-family: 'Source Sans Pro', sans-serif;font-size: 15px;">Date of Accident:</span>  <span style="font-size: 15px;"><?php echo date('F d, Y', strtotime($invitation->patient->dateofaccident)); ?></span>
                  </td>
                  <td class="desc" style="padding:16px;">
                     <h3 style="font-family: 'Source Sans Pro', sans-serif;font-weight: 500;">{{$invitation->invoices->serviceprovided}}</h3>
                  </td>
                  <td class="unit" style="font-family: 'Source Sans Pro', sans-serif;padding:16px;"><?php echo date('F d, Y', strtotime($invitation->invitation_date)); ?></td>
                  <td class="qty" style="font-family: 'Source Sans Pro', sans-serif;padding:16px;">$<span class="new_bill_amt"><?php if($invitation->invoices->lawyer_payment_amount!=null){echo $invitation->invoices->lawyer_payment_amount;} ?></span></td>
               </tr>
               <!-- ROW -->
            </tbody>
            <tfoot>
               <?php if($invitation->invoices->othercosts!=null){ ?>
               <tr>
                  <td colspan="2" style="border: none;"></td>
                  <td colspan="1" style="font-family: 'Source Sans Pro', sans-serif;font-weight: 800;">Other Costs</td>
                  <td style="font-family: 'Source Sans Pro', sans-serif;font-weight: 800;">$<span class="new_bill_amt" ><?php if($invitation->invoices->othercosts!=null){echo $invitation->invoices->othercosts;} ?></span></td>
               </tr>
              <?php  } ?>
               <tr>
                  <td colspan="2" style="border: none;"></td>
                  <td colspan="1" style="font-family: 'Source Sans Pro', sans-serif;font-weight: 800;">TOTAL</td>
                  <td style="font-family: 'Source Sans Pro', sans-serif;font-weight: 800;">$<span class="new_bill_amt" ><?php if($invitation->invoices->lawyer_payment_amount!=null){echo $invitation->invoices->lawyer_payment_amount+$invitation->invoices->othercosts;} ?></span></td>
               </tr>
            </tfoot>
         </table>
      </div>      
</div>
