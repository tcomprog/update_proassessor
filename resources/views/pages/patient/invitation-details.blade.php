<?php //dd($invitation->attachments);  ?>
@extends('layouts.lawyer-das-custom') 

@section('content')	

<style>
	@font-face {
		font-family: LoraBold;
		src: url("{{asset('fonts/Lora-Bold.ttf')}}");
	}
	@font-face {
		font-family: LoraRegular;
		src: url("{{asset('fonts/Lora-Regular.ttf')}}");
	}
</style>

<style>
	@font-face {
		  font-family: Inter;
		  src: url("{{asset('fonts/Inter-Regular.ttf')}}");
	}
</style>

<link href="{{ asset('css/invitedetail.css') }}?99999" rel="stylesheet">

<link href="{{ asset('css/patient_details.css') }}?999999" rel="stylesheet">


  <!-- Modal -->
  <div class="modal fade" id="deleteanimModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" style="display: flex;justify-content: center;align-items: center;">
	  <div class="modal-content" style="height: 199px;width: 309px;">
		<div class="modal-body" style="padding:5px;overflow:hidden">
		  
			<img width="100%" style="height: 99%" src="{{asset("img/delete_anim.gif")}}" />

		</div>
	  </div>
	</div>
  </div>


  {{-- ++++++++++++++++++++ Tracking modal start ++++++++++++++++++++ --}}

<div class="modal fade" id="tracability" tabindex="-1" data-backdrop="static" data-keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div style=" max-width: 100%;" class="modal-dialog modal-dialog-centered modal-xl">
	  <div class="modal-content" style="margin-left: 13px">
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">Traceability</h5>

		  <button id="closeButton" type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
		  </button>
		  
		</div>

		<div class="modal-body">
		
			<div class="row">
				<div class="col-md-12 mainTrackingcontainer" style="text-align:center">

					<img style="display: none;" id="loading_msg" width="100" src="{{asset('img/loading.gif')}}" />

						
				</div>
			</div>
		
		</div>
		
	  </div>
	</div>
  </div>
  

{{-- ++++++++++++++++++++ Tracking modal end   ++++++++++++++++++++ --}}

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<section class="intro_banner_sec inner_banner_sec" style="padding: 25px 0px;">
    <div class="container">
        <div class="row align-items-center"> 
            <div class="col-md-6 welcome_text">
                <h2>Appointment Details</h2>
            </div>
            <div class="col-md-6 text-right">
                <a class="btn btn-primary" href="{{url('/patient-dashboard')}}" style="padding: 10px 30px;
                background: #131946;
                border: none;
                border:1px solid white;
                font-size: 16px;
                line-height: 16px;" >Back to Dashboard</a>
            </div>               
        </div>
    </div>
</section>


<section class="dashboard_sec" style="padding-top: 5px;">

    <div class="container">	

    <?php  
        $color_tp = [
        'pending' => '#468499',
        'active' => '#66cdaa',
        'rejected' => '#cc0000',
        'expired' => '#f6546a',
        'completed' => '#ffd700',
        'cancelled' => '#ff4040'
        ];
        $now_time = time(); 
     ?>

    
  <div id="FilesReports">
    <div class="row1">	
      <div class="accordion" id="accordionExample">

        <div class="trac_container">
            <button id='traceability_id' onclick="openModal('{{$invitation->id}}')" target="_blank">Traceability</button>
        </div>

        @php
			$your_date = strtotime($invitation->duedate);
			$datediff = $now_time - $your_date;
			$check_time = round($datediff / (60 * 60 * 24));	
		@endphp


            <div class="patient_details_box" style="border-left: 5px solid {{$color_tp[$invitation->status]}};">
                <div class="row top_row">
                    <div class="col-md-6">
                        <p class="new_p">Name: <span>{{ ucfirst($invitation->user1->name) }}  </span></p> 
                        <p class="new_p">Category: <span>
                        <?php  $category = Helper::categorydetails();  ?>  
                        <?php  foreach ($category as $categories){ ?>
                            <?php if(($categories->id)==($invitation->user1->doctor->category_id)){echo ucfirst(strtolower($categories->category_name));} ?>
                        <?php } ?></span></p>
                        <p class="new_p">Status: <span style="color:{{$color_tp[$invitation->status]}}">{{ ucfirst($invitation->status) }}  </span></p> 
                    </div>
                    <div class="col-md-6 text-right">
                        <span class="category"><span class="font-weight-bold">Appointment Date/Time: </span>  <img src="{{ url('img/calendar.png') }}" class="calendar_icon"> <span ><?php echo date('d F Y', strtotime($invitation->invitation_date)); ?> |  {{ $invitation->timeslot }} </span>
                        <?php if($invitation->appointment_type=='virtual') { ?><span class="category virtualbold"><b> - Virtual</b></span><?php  } ?>	
                        </span>
                        <span  style="margin-bottom: 0px;display: inline-block;font-size: 16px;float:right1;"><span class="font-weight-bold" style="color:red;" >Report Due Date: </span>  <img src="{{ url('img/calendar.png') }}" class="calendar_icon"> <span style="font-size:16px;"><?php echo date('d F Y', strtotime($invitation->duedate)); ?> </span></span>									
                    </div>
                </div>

                <div class="invitation-tabs" style="min-height: 300px !important;">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item "><!--active-->
                    <a id="patient_lawyer_1" class="nav-link tab_design" data-toggle="tab" href="#new-invitations{{$invitation->id}}" role="tab" aria-controls="new-invitations" >Medicals</a><!--- active invitation-tab-->
                </li>
                <li class="nav-item">
                    <a id="patient_lawyer_2" class="nav-link tab_design" data-toggle="tab" href="#accepted-invitations{{$invitation->id}}" role="tab" aria-controls="accepted-invitations">Summary</a>
                </li>
                <li class="nav-item">
                    <a id="patient_lawyer_3" class="nav-link tab_design"  data-toggle="tab" href="#expired-invitations{{$invitation->id}}" role="tab" aria-controls="expired-invitations">Letter of Engagement</a>
                </li>							
                <li class="nav-item">
                    <a id="patient_lawyer_4" class="nav-link tab_design"  data-toggle="tab" href="#rejected-invitations{{$invitation->id}}" role="tab" aria-controls="expired-invitations">Form 53</a>
                </li>
                <li class="nav-item">
                    <a id="patient_lawyer_5" class="nav-link tab_design"  data-toggle="tab" href="#instruction-form{{$invitation->id}}" role="tab" aria-controls="instruction-form">Questionnaire</a>
                </li>
                <li class="nav-item">
                    <a id="patient_lawyer_6" class="nav-link tab_design" data-toggle="tab" href="#reports{{$invitation->id}}" role="tab" aria-controls="expired-invitations">Report (Med-Legal)</a>
                </li>
            </ul>
                <div class="tab-content active" id="invitations-tab-content">
                    <div class="tab-pane " id="new-invitations{{$invitation->id}}" role="tabpanel"><!---invitation-tab active-->
                        <div class="row">
                            <div class="col-md-12">
                                {{-- <h3 class="inv-content-header mb-0">Medicals</h3> --}}

                                @if(@$invitation->attachments->medicalsfilename) 
                                @if (@$invitation->status == 'active' || @$invitation->status == 'pending')
                                <div class="col d-flex mb-2" style="justify-content: end; padding:0px">
                                    <img id="deleteLoad_1" style="display: none" width="25" class="mr-3" src="{{asset('img/loading.gif')}}" />
                                    <button id="deleteBtn_1" onclick="deleteAllFiles('{{$invitation->id}}',1)" class="btn btn-outline-danger btn-sm ">Clear all</button>
                                </div>
                                @endif
                                @endif

                                <h5 class="text-success" id="medicals_dlte_scss{{$invitation->id}}" style="color: #C01D1D !important;display: none;">Medicals Documents Deleted Successsfully</h5>
                            

                            @php
                                $st = 0;
                            @endphp

                            <?php if(($invitation->attachments)!=null) { 
                                
                            ?>
                            <ul class="mt-2 reportList_1" id="medicaldocument{{$invitation->id}}">
                            <?php if(@$invitation->attachments->medicalsfilename) {
                                    $a=json_decode(@$invitation->attachments->medicalsfilename);
                                    $b=json_decode(@$invitation->attachments->medicalsimagename);
                                    $b=json_decode( json_encode($b), true);
                                    $i=0;
                                
                                    $ar = [
                                        'pdf' => 'picture_as_pdf',
                                        'doc' => 'upload_file',
                                        'docx' => 'description',
                                    ];
                                    
                                    foreach($a as $key => $value) { 
                                        // abaa
                                        $st++;
                                    ?>
                                    
                                    <div style="background: #F3F2F2;border-radius: 5px; margin-bottom: 0px;padding: 5px 15px;display:flex;" 
                                        id="med{{ $invitation->id.$key }}">
                                        
                                        <span class="material-icons" style="color: #20215E; font-size:24px; margin-right:8px;">{{$ar[explode('.',$b[$key])[1]]}}</span>

                                        <form style="margin-bottom: 0px; flex:1;" action="{{route('downloadEncFile')}}" style="display: inline-block" method="POST">
                                            @csrf
                                            <input name="path" type="hidden" value="{{$value}}">
                                            <input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
                                            <input name="name" type="hidden" value="{{$b[$i]}}">
                                            <input name="directory" type="hidden" value="l_medical">
                                            <button style="background: transparent;
                                            border: none;
                                            font-weight: bold;
                                            color: #20215E"><?php echo $b[$i]; ?></button>
                                        </form>
                                        @if (@$invitation->status == 'active' || @$invitation->status == 'pending')
                                        <a href="javascript:void(0)" onClick="reply_click_medicals(this.id, '{{ $b[$i] }}','{{ $invitation->id }}','{{ $invitation->patient_user_id }}','med{{ $invitation->id.$key }}')" id="{{ $value }}" >
                                            <i class="fa fa-close" style="font-size: 20px;color: red;" ></i>
                                        </a>
                                        @endif
                                    </div>
                                    <form id="medical-file-download-form{{ $invitation->id.$key }}" action="{{ url('download-medicals-file') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="filename" value="{{ $value }}">
                                        <input type="hidden" name="user_id" value="{{ $invitation->inviter_user_id }}" />
                                        <input type="hidden" name="download_name" value="{{ $b[$i] }}">
                                    </form>
                                <?php
                                $i++; } ?>  														
                                </ul> 
                            <?php } 
                        } else{ ?>
                        <ul class="mt-2" id="medicaldocument{{$invitation->id}}"></ul>
                        <?php  } ?>
                            @if ($invitation->status != 'completed')
                            <div @if($st > 0) {{'style=text-align:center'}} @else {{'style=text-align:center;padding-top:110px;'}} @endif>
                                <a class="upload_medical_btn new_upload_btn" data-patient-id="{{ $invitation->patient_user_id }}" data-initation-id="{{ $invitation->id }}" href="#" data-toggle="modal" data-target="#medicals-modal">
                                    <span class="material-icons upload_icon">backup</span>
                                    Upload
                                </a>
                        </div>
                            @endif
                        </div>
                        </div>
                    </div>
                <div class="tab-pane" id="accepted-invitations{{$invitation->id}}" role="tabpanel">
                <div class="row">
                    <div class="col-md-12">
                        {{-- <h3 class="inv-content-header mb-0">Summary</h3> --}}

                        @if(@$invitation->attachments->filename) 
                        @if (@$invitation->status == 'active' || @$invitation->status == 'pending')
                                <div class="col d-flex mb-2" style="justify-content: end; padding:0px">
                                    <img id="deleteLoad_2" style="display: none" width="25" class="mr-3" src="{{asset('img/loading.gif')}}" />
                                    <button id="deleteBtn_2" onclick="deleteAllFiles('{{$invitation->id}}',2)" class="btn btn-outline-danger btn-sm ">Clear all</button>
                                </div>
                        @endif
                        @endif

                        <h5 class="text-success" id="dlte_scss{{$invitation->id}}" style="color: #C01D1D !important;display: none;">Summary Documents Deleted Successsfully</h5>
                        <?php if(($invitation->attachments)!=null) { $st=0; ?>
                            <ul class="mt-2 reportList_2" id="summarydocument{{$invitation->id}}" >
                            <?php if(($invitation->attachments->filename)!=null) {?>
                            <?php  
                                    $a=json_decode($invitation->attachments->filename);
                                    $b=json_decode($invitation->attachments->imagename);
                                    $b=json_decode( json_encode($b), true);
                                    $i=0;
                                    
                                    $ar = [
                                        'pdf' => 'picture_as_pdf',
                                        'doc' => 'upload_file',
                                        'docx' => 'description',
                                    ];
                                    //print_r($b);exit;
                                    foreach($a as $key => $value) { 
                                        $st++;
                                    ?>
                                    <div style="background: #F3F2F2;border-radius: 5px; margin-bottom: 12px;padding: 5px 15px;display:flex;" id="sum{{ $invitation->id.$key }}">
                                        <span class="material-icons" style="color: #20215E; font-size:24px; margin-right:8px;">{{$ar[explode('.',$b[$i])[1]]}}</span>

                                        <form action="{{route('downloadEncFile')}}" style="margin-bottom: 0px; flex:1; display: inline-block" method="POST">
                                            @csrf
                                            <input name="path" type="hidden" value="{{$value}}">
                                            <input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
                                            <input name="name" type="hidden" value="{{$b[$i]}}">
                                            <input name="directory" type="hidden" value="l_summary">
                                            <button style="background: transparent;
                                            border: none;
                                            font-weight: bold;
                                            color: #20215E"><?php echo $b[$i]; ?></button>
                                        </form>
                                        @if (@$invitation->status == 'active' || @$invitation->status == 'pending')
                                        <a href="javascript:void(0)" onClick="reply_click1(this.id, '{{ $b[$i] }}','{{ $invitation->id }}','{{ $invitation->patient_user_id }}','sum{{ $invitation->id.$key }}')" id="{{ $value }}" >
                                            <i class="fa fa-close" style="font-size: 20px;color: red;" ></i>
                                        </a> 
                                        @endif
                                    </div>
                                <?php
                                $i++; } ?>  														
                                </ul> 
                            <?php } 
                        } else {?>
                                <ul class="mt-2" id="summarydocument{{$invitation->id}}" ></ul>
                        <?php  } ?>
                        {{-- @if ($invitation->status != 'completed')
                        <div @if($st > 0) {{'style=text-align:center'}} @else {{'style=text-align:center;padding-top:110px;'}} @endif>
                            <a class="upload_doc_btn new_upload_btn" data-patient-id="{{ $invitation->patient_user_id }}" data-initation-id="{{ $invitation->id }}" href="#" data-toggle="modal" data-target="#UploadDocs">
                                <span class="material-icons upload_icon">backup</span>
                                Upload
                            </a>
                        </div>
                        @endif --}}
                    </div>								
                </div>
            </div>
                <div class="tab-pane" id="expired-invitations{{$invitation->id}}" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            {{-- <h3 class="inv-content-header mb-0">Letter of Engagement</h3> --}}

                            @if(@$invitation->attachments->loefilename) 
                            @if (@$invitation->status == 'active' || @$invitation->status == 'pending')
                                <div class="col d-flex mb-2" style="justify-content: end; padding:0px">
                                    <img id="deleteLoad_3" style="display: none" width="25" class="mr-3" src="{{asset('img/loading.gif')}}" />
                                    <button id="deleteBtn_3" onclick="deleteAllFiles('{{$invitation->id}}',3)" class="btn btn-outline-danger btn-sm ">Clear all</button>
                                </div>
                                @endif
                            @endif

                            @php
                                $ar = [
                                        'pdf' => 'picture_as_pdf',
                                        'doc' => 'upload_file',
                                        'docx' => 'description',
                                    ];
                            @endphp

                            <h5 class="text-success" id="loe_dlte_scss{{$invitation->id}}" style="color: #C01D1D !important;display: none;">Letter of Engagement Documents Deleted Successsfully</h5>										
                            <?php if(($invitation->attachments)!=null) { $st=0; ?>
                                <ul class="mt-2 reportList_3" id="loedocument{{$invitation->id}}" >
                            <?php if(($invitation->attachments->loefilename)!=null) {?>
                            <?php  
                                    $a=json_decode($invitation->attachments->loefilename);
                                    $b=json_decode($invitation->attachments->loeimagename);
                                    $b=json_decode( json_encode($b), true);
                                    $i=0;
                                    
                                    
                                    foreach($a as $key => $value) { $st++; ?>
                                    <div style="background: #F3F2F2;border-radius: 5px; margin-bottom: 12px;padding: 5px 15px;display:flex;" id="loe{{ $invitation->id.$key }}">
                                        <span class="material-icons" style="color: #20215E; font-size:24px; margin-right:8px;">{{$ar[explode('.',$b[$i])[1]]}}</span>

                                        <form action="{{route('downloadEncFile')}}" style="margin-bottom: 0px; flex:1; display: inline-block" method="POST">
                                            @csrf
                                            <input name="path" type="hidden" value="{{$value}}">
                                            <input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
                                            <input name="name" type="hidden" value="{{$b[$i]}}">
                                            <input name="directory" type="hidden" value="l_LetterOfEngagement">
                                            <button style="background: transparent;
                                            border: none;
                                            font-weight: bold;
                                            color: #20215E"><?php echo $b[$i]; ?></button>
                                        </form>
                                        @if (@$invitation->status == 'active' || @$invitation->status == 'pending')
                                        <a href="javascript:void(0)" onClick="reply_click_loe(this.id, '{{ $b[$i] }}','{{ $invitation->id }}','{{ $invitation->patient_user_id }}','loe{{ $invitation->id.$key }}')" id="{{ $value }}" >
                                            <i class="fa fa-close" style="font-size: 20px;color: red;" ></i></a>
                                        @endif
                                        </div>
                                <?php
                                $i++; } ?>  														
                                </ul> 
                            <?php } 
                        } else{ ?>
                                <ul class="mt-2" id="loedocument{{$invitation->id}}" ></ul>
                        <?php  }  ?>
                        {{-- @if ($invitation->status != 'completed')
                        <div @if($st > 0) {{'style=text-align:center'}} @else {{'style=text-align:center;padding-top:110px;'}} @endif>
                            <a class="upload_loe_btn new_upload_btn" data-patient-id="{{ $invitation->patient_user_id }}" data-initation-id="{{ $invitation->id }}" href="#" data-toggle="modal" data-target="#loe">
                                <span class="material-icons upload_icon">backup</span>
                                Upload
                            </a>
                        </div>
                            @endif --}}
                        </div>
                        
                    </div>
                </div>
                
                <div class="tab-pane" id="rejected-invitations{{$invitation->id}}" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            {{-- <h3 class="inv-content-header mb-0">Form 53</h3> --}}

                            @if(@$invitation->attachments->form53filename) 
                            @if (@$invitation->status == 'active' || @$invitation->status == 'pending')
                            <div class="col d-flex mb-2" style="justify-content: end; padding:0px">
                                <img id="deleteLoad_4" style="display: none" width="25" class="mr-3" src="{{asset('img/loading.gif')}}" />
                                <button id="deleteBtn_4" onclick="deleteAllFiles('{{$invitation->id}}',4)" class="btn btn-outline-danger btn-sm ">Clear all</button>
                            </div>
                            @endif
                            @endif

                            <h5 class="text-success" id="form53_dlte_scss{{$invitation->id}}" style="color: #C01D1D !important; display: none;">Form 53 Documents Deleted Successsfully</h5>
                            <?php if(($invitation->attachments)!=null) { $st=0; ?>
                                <ul class="mt-2 reportList_4" id="form53document{{$invitation->id}}" >
                            <?php if(($invitation->attachments->form53filename)!=null) {?>
                            <?php  
                                    $a=json_decode($invitation->attachments->form53filename);
                                    $b=json_decode($invitation->attachments->form53imagename);
                                    $b=json_decode( json_encode($b), true);
                                    $i=0;
                                
                                    foreach($a as $key => $value) { $st++; ?>
                                    <div style="background: #F3F2F2;border-radius: 5px; margin-bottom: 12px;padding: 5px 15px;display:flex;" id="form53{{ $invitation->id.$key }}">
                                        <span class="material-icons" style="color: #20215E; font-size:24px; margin-right:8px;">{{$ar[explode('.',$b[$i])[1]]}}</span>

                                        <form action="{{route('downloadEncFile')}}" style="margin-bottom: 0px; flex:1; display: inline-block" method="POST">
                                            @csrf
                                            <input name="path" type="hidden" value="{{$value}}">
                                            <input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
                                            <input name="name" type="hidden" value="{{$b[$i]}}">
                                            <input name="directory" type="hidden" value="l_form_53">
                                            <button style="background: transparent;
                                            border: none;
                                            font-weight: bold;
                                            color: #20215E"><?php echo $b[$i]; ?></button>
                                        </form>
                                        @if (@$invitation->status == 'active' || @$invitation->status == 'pending')
                                        <a href="javascript:void(0)" onClick="reply_click_form53(this.id, '{{ $b[$i] }}','{{ $invitation->id }}','{{ $invitation->patient_user_id }}','form53{{ $invitation->id.$key }}')" id="{{ $value }}" >
                                            <i class="fa fa-close" style="font-size: 20px;color: red;" ></i></a>
                                        @endif
                                        </div>
                                <?php
                                $i++; } ?>  														
                                </ul> 
                            <?php } 
                        } else{  ?>
                            <ul class="mt-2" id="form53document{{$invitation->id}}" ></ul>
                        <?php } ?>
                        @if ($invitation->status != 'completed')
                        <div @if($st > 0) {{'style=text-align:center'}} @else {{'style=text-align:center;padding-top:110px;'}} @endif>
                            <a class="upload_form53_btn new_upload_btn" data-patient-id="{{ $invitation->patient_user_id }}" data-initation-id="{{ $invitation->id }}" href="#" data-toggle="modal" data-target="#form53">
                                <span class="material-icons upload_icon">backup</span>
                                Upload
                            </a>
                        </div>
                            @endif
                        </div>
                        
                    </div>
                </div>
                <div class="tab-pane" id="instruction-form{{$invitation->id}}" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            {{-- <h3 class="inv-content-header mb-0">Questionnaire</h3> --}}

                            @if(@$invitation->attachments->insformfilename)
                            @if (@$invitation->status == 'active' || @$invitation->status == 'pending') 
                            <div class="col d-flex mb-2" style="justify-content: end; padding:0px">
                                <img id="deleteLoad_5" style="display: none" width="25" class="mr-3" src="{{asset('img/loading.gif')}}" />
                                <button id="deleteBtn_5" onclick="deleteAllFiles('{{$invitation->id}}',5)" class="btn btn-outline-danger btn-sm ">Clear all</button>
                            </div>
                            @endif
                            @endif

                            <h5 class="text-success" id="insform_dlte_scss{{$invitation->id}}"  style="color: #C01D1D !important;display: none;">Questionnaire Documents Deleted Successsfully</h5>
                            
                            <?php if(($invitation->attachments)!=null) { $st = 0;  ?>
                                <ul class="mt-2 reportList_5" id="insformdocument{{$invitation->id}}" >
                            <?php if(($invitation->attachments->insformfilename)!=null) {?>
                            <?php  
                                    $a=json_decode($invitation->attachments->insformfilename);
                                    $b=json_decode($invitation->attachments->insformimagename);
                                    $b=json_decode( json_encode($b), true);
                                    $i=0;
                                    //print_r($b);exit;
                                    $ar = [
                                        'pdf' => 'picture_as_pdf',
                                        'doc' => 'upload_file',
                                        'docx' => 'description',
                                    ];
                                    
                                    foreach($a as $key => $value) { $st++; ?>
                                    <div style="background: #F3F2F2;border-radius: 5px; margin-bottom: 12px;padding: 5px 15px;display:flex;" id="insform{{ $invitation->id.$key }}">
                                        <span class="material-icons" style="color: #20215E; font-size:24px; margin-right:8px;">{{$ar[explode('.',$b[$i])[1]]}}</span>

                                        <form action="{{route('downloadEncFile')}}" style="margin-bottom: 0px; flex:1; display: inline-block" method="POST">
                                            @csrf
                                            <input name="path" type="hidden" value="{{$value}}">
                                            <input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
                                            <input name="name" type="hidden" value="{{$b[$i]}}">
                                            <input name="directory" type="hidden" value="l_questionair">
                                            <button style="background: transparent;
                                            border: none;
                                            font-weight: bold;
                                            color: #20215E"><?php echo $b[$i]; ?></button>
                                        </form>
                                        @if (@$invitation->status == 'active' || @$invitation->status == 'pending')
                                        <a href="javascript:void(0)" onClick="reply_click_insform(this.id, '{{ $b[$i] }}','{{ $invitation->id }}','{{ $invitation->patient_user_id }}','insform{{ $invitation->id.$key }}')" id="{{ $value }}" >
                                            <i class="fa fa-close" style="font-size: 20px;color: red;" ></i></a>
                                        @endif
                                        </div>
                                <?php
                                $i++; } ?>  														
                                </ul> 
                            <?php } 
                        }  else{?>
                                <ul class="mt-2" id="insformdocument{{$invitation->id}}" ></ul>
                        <?php  } ?>
                        @if ($invitation->status != 'completed')
                        <div @if($st > 0) {{'style=text-align:center'}} @else {{'style=text-align:center;padding-top:110px;'}} @endif>
                            <a class="upload_insform_btn new_upload_btn" data-patient-id="{{ $invitation->patient_user_id }}" data-initation-id="{{ $invitation->id }}" href="#" data-toggle="modal" data-target="#insform">
                                <span class="material-icons upload_icon">backup</span>
                                Upload
                            </a>
                        </div>
                            @endif
                        </div>
                        
                    </div>
                </div>
                <div class="tab-pane" id="reports{{$invitation->id}}" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">

                            <h3 class="inv-content-header">Report (Med-Legal)</h3>
                            
                            <?php if(($invitation->attachments)!=null && $invitation->attachments->reportfilename) { ?>
                                <ul class="mt-2">
                                <?php  //print_r(json_decode($invitation->attachments->filename)); 
                                        $a=json_decode($invitation->attachments->reportfilename);
                                        $b=json_decode($invitation->attachments->reportimagename);
                                        $b=json_decode( json_encode($b), true);
                                        $i=0;
                                        //print_r($b);exit;
                    
                                        foreach($a as $key => $value) { ?>
                                        <div>
                                            <form action="{{route('downloadEncFile')}}" style="background: #F3F2F2;border-radius: 5px; margin-bottom: 12px;padding: 5px 15px;display:flex;" method="POST">
                                                @csrf
                                                <input name="path" type="hidden" value="{{$value}}">
                                                <input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
                                                <input name="name" type="hidden" value="{{$b[$i]}}">
                                                <input name="directory" type="hidden" value="medical_reports">
                                                <button style="background: transparent;border: none;font-weight: bold;color: #20215E;flex:1;    text-align: start;"><?php echo $b[$i]; ?></button>
                                                <button type="submit" style="background: transparent;
                                                border: none;"><i class="fa fa-download" title="Download report" style="color:#20215E !important; font-size:20px"></i></button>
                                            </form>

                                            {{-- <span><a target="_blank" href="{{ url('uploads/reports/'.$value) }}" style="text-decoration:none;" ><?php echo $b[$i]; ?></a></span>	
                                            <a href="{{ url('uploads/reports/'.$value) }}" download="{{ $b[$i] }}"  id="{{ $value }}" ><i class="fa fa-download" style="font-size: 18px;float: right;" ></i></a> --}}
                                        </div>
                                    <?php
                                    $i++; } ?>  														
                                    </ul> 
                                <?php } else if($invitation->status == 'active') {
                                    echo "<p  style='color: #eca606;font-size: 18px; font-weight: 600;'>No Reports uploaded by Doctor.</p>";
                                } else if($invitation->is_patient_accepted == 'no' && $invitation->doctor_accepted_on != null) {
                                    echo "<p  style='color: #eca606;font-size: 18px; font-weight: 600;'>Not Accepted By Patient</p>";
                                } 
                                else {
                                    echo "<p  style='color: #eca606;font-size: 18px; font-weight: 600;'>Not Accepted By Doctor</p>";
                                } 
                            ?>


            {{-- //////////////////////////////////////////////////////////// --}}
            <div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" id="ratingModels" tabindex="1" role="dialog" aria-labelledby="forgotTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content login_box">

            <div class="modal-header" style="border: none;">
            <h5 class="modal-title" id="loginTitle">Complete Report</h5>
            <button onclick="funcs_close()" type="button" class="close nclose" data-dismiss="modal" aria-label="Close">

            <span class="material-icons" style="color: #fff; font-size:23px;
            vertical-align: middle;">close</span>

            </button>
            </div>

            <div class="modal-body" style="padding-top:0px;">
            <div class="container">
                <div class="row" style="display: flex;justify-content: center; ">
                    <p>Kindly share your experience with doctor</p>
                    <div id="starboxContainer" class="col-md-12" style="display: flex;justify-content: center;padding:0px">
                        
                    </div>
                <div class="col-12">
                    <div class="d-flex" style="justify-content: center">
                    <img style="display:none;" width="30" id="accept_report_loading_msg_{{$invitation->id}}" width="100" src="{{asset('img/loading.gif')}}" />
                    <button id="completeButton_{{$invitation->id}}" onclick="acceptReportFunc({{$invitation->id}})" class="act_btn">Complete report</button>
                    </div>
                    <div id="successMsg_{{$invitation->id}}" class="alert alert-success mt-3" style="text-align: start; display:none" role="alert">
                        Report completed successfully
                    </div>

                    <div id="erroMsg_{{$invitation->id}}" class="alert alert-danger mt-3" style="text-align: start;display:none" role="alert">
                        Something went wrong try again
                    </div>
                </div>
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            {{-- //////////////////////////////////////////////////////////// --}}

            <input type="hidden" id="doctorRating" value="0">
                            

                                @if (($invitation->attachments)!=null && $invitation->attachments->reportfilename && $invitation->status == 'active')
                                    <div style="text-align: end;">
                                        <button id="acptButton_{{$invitation->id}}" onclick="openPopup({{$invitation->id}})" class="act_btn">Accept report</button>  
                                    </div>
                                @endif

                        </div>
                        
                    </div>
                </div>

                </div>
                
                </div>	
                </div>	


      </div>
    </div>
  </div>


    </div>

</section>



{{-- =============== MODAL SECTION ================== --}}

{{-- ***************** Form 53 uploading modal ******************** --}}
<div class="modal fade" id="form53"  data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">
    
      <div class="modal-content login_box">
    
        <div class="modal-header">
    
          <h5 class="modal-title" id="calendarTitle">Please upload clients form 53 documents</h5>
    
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    
            <span aria-hidden="true">&times;</span>
    
          </button>
    
        </div>
    
        <div class="modal-body">
          <form id="form53_upload_form"  enctype="multipart/form-data">
    
              <div class="form-group files">
    
                <input type="file" class="form-control" name="form53files[]" id="form53files" multiple="" required>
                <span class="text-danger" id="form53-upload-error" style="color:red;"></span>
                <h5 class="text-success" id="form53-upload-scss" style="color:green;"></h5>
              </div>
              <div class="form-group">
                <input type="hidden" name="form53newintid" id="form53-upload-file-invitation-id" > 
                <input type="hidden" name="form53patientid" id="form53-upload-file-patient-id" > 
              </div>
    
              <div id="fileuploadError_form53" class="alert alert-danger" role="alert" style="display: none">
                Invalid file type, You can only upload pdf, doc, docx
              </div>
    
              <div id="progress_bar_form53">
                  
              </div>
    
              <button  type="button" class="invite_btn form53_upload_btn" id="submit_form53">Submit</button> 
          </form>
    
        </div>
    
      </div>
    </div>
    </div>



    {{-- ********************* Upload Medical Documents Popup ******************** --}}
    <div class="modal fade" id="medicals-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content login_box">
            <div class="modal-header">
              <h5 class="modal-title" >Please upload clients Medical docuement(s)</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
        
              <form id="medicals_upload_form"  enctype="multipart/form-data">
                  <div class="form-group files">
                    <input type="file" class="form-control" name="medicalsfiles[]" id="medicalsfiles" multiple="" required>
                    <span class="text-danger" id="medicals-upload-error" style="color:red;"></span>
                    <h5 class="text-success" id="medicals-upload-scss" style="color:green;"></h5>
                  </div>
                  <div class="form-group">
                    <input type="hidden" name="medicalsnewintid" id="medicals-upload-file-invitation-id" > 
                    <input type="hidden" name="medicalspatientid" id="medicals-upload-file-patient-id" > 
                  </div>
                
                  <div id="fileuploadError" class="alert alert-danger" role="alert" style="display: none">
                    Invalid file type, You can only upload pdf, doc, docx
                  </div>
        
                  <div id="progress_bar">
                      
                  </div>	
                  <button  type="button" class="invite_btn medicals_upload_btn" id="submit_medical_upload">Submit</button> 
              </form>
        
            </div>
        
          </div>
        </div>
        </div>


        {{-- ********************* Questionnaire uploading popup ******************** --}}
        <div class="modal fade" id="insform"  tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">

            <div class="modal-dialog modal-dialog-centered" role="document">
            
              <div class="modal-content login_box">
            
                <div class="modal-header">
            
                  <h5 class="modal-title" id="calendarTitle">Please upload clients Questionnaire documents</h5>
            
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            
                    <span aria-hidden="true">&times;</span>
            
                  </button>
            
                </div>
            
                <div class="modal-body">
                  <form id="insform_upload_form"  enctype="multipart/form-data">
                      <div class="form-group files">
                        <input type="file" class="form-control" name="insformfiles[]" id="insformfiles" multiple="" required>
                        <span class="text-danger" id="insform-upload-error" style="color:red;"></span>
                        <h5 class="text-success" id="insform-upload-scss" style="color:green;"></h5>
                      </div>
                      <div class="form-group">
                        <input type="hidden" name="insformnewintid" id="insform-upload-file-invitation-id" > 
                        <input type="hidden" name="insformpatientid" id="insform-upload-file-patient-id" > 
                      </div>
            
                      <div id="fileuploadError_Questionair" class="alert alert-danger" role="alert" style="display: none">
                        Invalid file type, You can only upload pdf, doc, docx
                      </div>
            
                      <div id="progress_bar_Questionair">
                          
                      </div>
            
                      <button  type="button" class="invite_btn insform_upload_btn" id="submit_Questionair">Submit</button> 
                  </form>
            
                </div>
            
              </div>
            </div>
            </div>
            
            
<!----delete Medicals---->
<div class="modal fade" id="medicalsDelete" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document" style="display: flex;justify-content: center;align-items: center;">
    
      <div class="modal-content p-0" style="max-width: 220px;max-height:120px">
            
        <div class="modal-body p-0" style="height: 90px">
          <form id="medicals_delete_form">
              
              <div class="form-group my-0">
                  <input type="hidden" name="medicalsformnewintid" id="medicals-del-invitation-id"> 
                    <input type="hidden" name="medicalspatientid" id="medicals-del-inv-patient_id"> 
                    <input type="hidden" name="medicalsDeleteDocsid" id="medicals-DeleteDocsid"> 
                    <input type="hidden" name="medicalsDeleteDocsname" id="medicals-DeleteDocsname">  
                    <input type="hidden" name="activeliid" id="medicals-activeliid">  
               </div>
    
               <div class="row">
    
              <div class="col-md-12">
                <div class="manualboxdesi mx-auto">
                  <button type="button" id="medicals_delete_form_btn">Yes</button> 
                  <button  type="button"  data-dismiss="modal" aria-label="Close" style="background: #CC0A11;">No</button> 
                </div>
              </div>
            </div>
          </form>
    
        </div>
    
      </div>
    </div>
    
    </div>	
    </div>


    <!----delete loe---->
<div class="modal fade" id="loeDelete" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document" style="display: flex;justify-content: center;align-items: center;">
    
      <div class="modal-content p-0" style="max-width: 220px;max-height:90px">
    
            
        <div class="modal-body p-0" style="height: 90px;">
          <form id="loe_delete_form" class="my-0" >
              
              <div class="form-group my-0">
                  <input type="hidden" name="loenewintid" id="loe-del-invitation-id"> 
                    <input type="hidden" name="loepatientid" id="loe-del-inv-patient_id"> 
                    <input type="hidden" name="loeDeleteDocsid" id="loe-DeleteDocsid"> 
                    <input type="hidden" name="loeDeleteDocsname" id="loe-DeleteDocsname">  
                    <input type="hidden" name="activeliid" id="loe-activeli"> 
               </div>
               <div class="row">
                <div class="col-md-12">
                    <div class="manualboxdesi mx-auto">
                      <button type="button" id="loe_delete_form_btn">Yes</button> 
                       <button  type="button"  data-dismiss="modal" aria-label="Close" style="background: #CC0A11;">No</button> 
                </div>
              
               </div>
          </form>
    
        </div>
    
      </div>
    </div>
    
    </div>	
    </div>



    <!------delete form53------->
<div class="modal fade" id="form53Delete" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document" style="display: flex;justify-content: center;align-items: center;">
    
      <div class="modal-content p-0" style="max-width: 220px;max-height:90px;">
            
        <div class="modal-body p-0" style="height: 90px">
          <form id="form53_delete_form" class="my-0" >
              
              <div class="form-group my-0 py-0">
                  <input type="hidden" name="form53newintid" id="form53-del-invitation-id"> 
                    <input type="hidden" name="form53patientid" id="form53-del-inv-patient_id"> 
                    <input type="hidden" name="form53DeleteDocsid" id="form53-DeleteDocsid"> 
                    <input type="hidden" name="form53DeleteDocsname" id="form53-DeleteDocsname">  
                    <input type="hidden" name="activeliid" id="form53-del-inv-activeli">  
               </div>
               <div class="row">
                <div class="col-md-12">
                    <div class="manualboxdesi mx-auto">
                      <button type="button" id="form53_delete_form_btn">Yes</button> 
                       <button  type="button"  data-dismiss="modal" style="background: #CC0A11;" aria-label="Close">No</button> 
                    </div>
                </div>
             
            </div>
          </form>
    
        </div>
    
      </div>
    </div>
    
    </div>	
</div>



<!-- Delete SUmmary  Model -->  
<div class="modal fade" id="DeleteDocs"  role="dialog" aria-labelledby="calendarTitle" aria-hidden="true" >

    <div class="modal-dialog modal-dialog-centered"  role="document" style="display: flex;justify-content: center;align-items: center;">
    
      <div class="modal-content p-0 " style="max-width: 220px;max-height:90px;">
            
        <div class="modal-body p-0" style="max-height: 220px">
          <form id="deletedoc_form" class="my-0 py-0">
              
              <div class="form-group m-0">
                  <input type="hidden" name="newintid" id="del-invitation-id"> 
                <input type="hidden" name="patientid" id="del-inv-patient_id"> 
                <input type="hidden" name="DeleteDocsid" id="DeleteDocsid"> 
                <input type="hidden" name="DeleteDocsname" id="DeleteDocsname"> 
                <input type="hidden" name="activeliid" id="del-inv-activeli"> 
               </div>
               <div class="row">
               {{-- <div class="col-md-2"></div> --}}
               {{-- <div class="col-md-8" id="del-summary-btns-div">
                  <button type="button" class="invite_btn" id="deletedoc_form_btn">Yes</button> 
                  <button  type="button"  class="invite_btn" data-dismiss="modal" aria-label="Close">No</button> 
              </div> --}}
              <div class="col-md-12">
              <div class="manualboxdesi mx-auto">
                <button type="button" id="deletedoc_form_btn">Yes</button> 
                 <button  type="button"  data-dismiss="modal" aria-label="Close"  style="background: #CC0A11;">No</button> 
              </div>
            </div>
            </div>
    
          </form>
    
        </div>
    
      </div>
    </div>
    
    </div>		  
    <!-- end delete Documents Model -->



<div class="modal fade" id="insformDelete" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document" style="display: flex;justify-content: center;align-items: center;">
    
      <div class="modal-content p-0" style="max-width: 220px;max-height:90px;">
    
        <div class="modal-body p-0" style="height: 90px">
          <form id="insform_delete_form" class="py-0 my-0" >
              
              <div class="form-group py-0 my-0">
                  <input type="hidden" name="insformnewintid" id="insform-del-invitation-id"> 
                    <input type="hidden" name="insformpatientid" id="insform-del-inv-patient_id"> 
                    <input type="hidden" name="insformDeleteDocsid" id="insform-DeleteDocsid"> 
                    <input type="hidden" name="insformDeleteDocsname" id="insform-DeleteDocsname">  
                    <input type="hidden" name="insformactiveliid" id="insform-del-inv-activeli">  
               </div>
               <div class="row">
              
              <div class="col-md-12">
                <div class="manualboxdesi mx-auto">
                  <button type="button" id="insform_delete_form_btn">Yes</button> 
                   <button  type="button"  data-dismiss="modal" aria-label="Close" style="background: #CC0A11;">No</button> 
                </div>
              </div>
               </div>
          </form>
    
        </div>
    
      </div>
    </div>
    
    </div>

</div>


@endsection


<script>
	function openModal(id){
		$.ajax({
        url:"{{ url('patient/tracking') }}",
        data:{'id':id,'_token':'{{ csrf_token() }}'},
        type:'post',
        dataType:'json',
		beforeSend:function(){
			$(".mainTrackingcontainer").empty();
		    $("#closeButton").hide(); 
		    $("#loading_msg").show(); 
	    	$("#tracability").modal('show');
		},
        success:function(res) {

			$()

let firstLine = `<div class="trackingContainer">`;
let secondLine = `<div class="trackingContainer">`;
let thirdLine = `<div class="trackingContainer">`;

let data = res.data;

let len = data.length;
let width = 12.5;

width = 12.4;

if(data[0] != undefined){
   let it_0 = data[0];
   var dd = new Date(it_0["date"]);
   dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes();
   firstLine += ` <div class="s_section">
				 <div class="circles" style="background: #C1CDE3;"> <h1>1</h1> </div>
				 <div class="st-line" style="background: #C1CDE3;"></div>
			</div>`;
   secondLine += `<div class="s_section bx-1"> <div class="tooltip-top-1" style="border-top-left-radius: 6px;border-bottom-left-radius: 6px;"></div></div>`;
   thirdLine += `<div class="s_section pt-4">
					<h5 class="dateP" style="color:#6688C8;margin-bottom:8px">`+dd+`</h5>
					<h4 style="color: #6688C8;">Assessment Requested</h4>
					<p>( Waiting for Dr. Confirmation )</p>
				</div>`;
}

if(data[1] != undefined){
	let it_1 = data[1];
	var dd = new Date(it_1["date"]);
   dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes();
   firstLine += ` <div class="s_section pb-4">
				<h4 style="color: #3D5AF1;">The assessment was confirmed by the Doctor</h4>
				<p>( waiting for the patient's confirmation)</p>
				<h5 class="dateP" style="color:#3D5AF1; margin-bottom:0px; margin-top:8px;">`+dd+`</h5>
			</div>`;
   secondLine += ` <div class="s_section"><div class="tooltip-bottom-1"></div></div>`;
   thirdLine += ` <div class="s_section">
					<div class="st-line" style="background: #3D5AF1;"></div>
					<div class="circles" style="background: #3D5AF1;"> <h1>2</h1> </div>
				</div>`;
}

if(data[2] != undefined){
	let it_2 = data[2];
	var dd = new Date(it_2["date"]);
   dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes();
	firstLine += ` <div class="s_section">
				<div class="circles" style="background: #E2767A;"> <h1>3</h1> </div>
				<div class="st-line" style="background: #E2767A;"></div>
			</div>`;
   secondLine += ` <div class="s_section"><div class="tooltip-top-2"></div></div>`;
   thirdLine += `  <div class="s_section pt-4">
					<h5 class="dateP" style="color:#E2767A">`+dd+`</h5>
					<h4 style="color: #E2767A;">Assessment booked</h4>
					<p>(Patient confirmed the booking, waiting for the assessment to be completed)</p>
				</div>`;
}

if(data[3] != undefined){
	let it_3 = data[3];
	var dd = new Date(it_3["date"]);
	dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes(); 
	firstLine += `   <div class="s_section pb-4">
				<h4  style="color: #8DD0C0;">Assessment completed</h4>
				<h5 class="dateP" style="color:#8DD0C0; margin-bottom:0px; margin-top:8px;">`+dd+`</h5>
				<p></p>
			</div>`;
   secondLine += ` <div class="s_section"><div class="tooltip-bottom-2"></div></div>`;
   thirdLine += `   <div class="s_section">
					<div class="st-line" style="background: #8DD0C0;"></div>
					<div class="circles" style="background: #8DD0C0;"> <h1>4</h1> </div>
				</div>`;
}

if(data[4] != undefined){
	let it_4 = data[4];
	var dd = new Date(it_4["date"]);
	dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes();
	firstLine += `<div class="s_section">
				<div class="circles" style="background: #97A0C2;"> <h1>5</h1> </div>
				<div class="st-line" style="background: #97A0C2;"></div>
			</div>`;
   secondLine += `<div class="s_section"><div class="tooltip-top-3"></div></div>`;
   thirdLine += ` <div class="s_section pt-4">
				 <h5 class="dateP" style="color:#97A0C2; margin-bottom:8px">`+dd+`</h5>

					<h4 style="color: #97A0C2;">Report in progress</h4>
					<p></p>
				</div>`;
}

if(data[5] != undefined){
	let it_5 = data[5];
	var dd = new Date(it_5["date"]);
	dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes();
	firstLine += `  <div class="s_section pb-4">
				<h4  style="color: #5D9FFF;">Report submitted by the doctor</h4>
				<h5 class="dateP" style="color:#5D9FFF; margin-bottom:0px; margin-top:8px;">`+dd+`</h5>
				<p></p>
			</div>`;
   secondLine += `<div class="s_section"><div class="tooltip-bottom-3"></div></div>`;
   thirdLine += `  <div class="s_section">
					<div class="st-line" style="background: #5D9FFF;"></div>
					<div class="circles" style="background: #5D9FFF; "> <h1>6</h1> </div>
				</div>`;
}

if(data[6] != undefined){
	let it_6 = data[6];
	var dd = new Date(it_6["date"]);
	dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes();
	firstLine += `<div class="s_section">
				<div class="circles" style="background: #9BCF5D;"> <h1>7</h1> </div>
				<div class="st-line" style="background: #9BCF5D;"></div>
			</div>`;
   secondLine += `  <div class="s_section"><div class="tooltip-top-4"></div></div>`;
   thirdLine += `   <div class="s_section pt-4">
					<h5 class="dateP" style="color:#9BCF5D; margin-bottom:8px">`+dd+`</h5>

					<h4 style="color: #9BCF5D;">Report Being reviewed</h4>
					<p></p>
				</div>`;
}

if(data[7] != undefined){
	let it_7 = data[7];
	var dd = new Date(it_7["date"]);
	dd = dd.getDate()+"/"+(dd.getMonth()+1)+"/"+dd.getFullYear()+" | "+dd.getHours()+":"+dd.getMinutes();
	firstLine += `   <div class="s_section pb-4">
				<h4  style="color: #BB5A77;">The report was Accepted by the lawyer</h4>
				<h5 class="dateP" style="color:#BB5A77; margin-bottom:0px; margin-top:8px;">`+dd+`</h5>
				<p></p>
			</div>`;
   secondLine += `<div class="s_section"><div class="tooltip-bottom-4"></div></div>`;
   thirdLine += `  <div class="s_section">
					<div class="st-line" style="background: #BB5A77;"></div>
					<div class="circles" style="background: #BB5A77;"> <h1>8</h1> </div>
				</div>`;
}

firstLine += `</div>`;
secondLine += `</div>`;
thirdLine += `</div>`;

let nhtml = firstLine+secondLine+thirdLine;

var cols = document.getElementsByClassName('s_section');
for(i = 0; i < cols.length; i++) {
	cols[i].style.width = width+'%';
}
			
			$(".mainTrackingcontainer").append(nhtml);
		    $("#closeButton").show(); 
		    $("#loading_msg").hide(); 
        },
		error:function(xhr,status,error){
			console.log('xhr => ',xhr," status => ",status," error => ",error)
		}
    });

	}

	
	function openPopup(id){
		var starboxes = `<div class="stars">
										<form action="">
											<input class="startclick star star-5" id="star-5" type="radio" name="star"/>
											<label class="star star-5" for="star-5"></label>
											<input class="startclick star star-4" id="star-4" type="radio" name="star"/>
											<label class="star star-4" for="star-4"></label>
											<input class="startclick star star-3" id="star-3" type="radio" name="star"/>
											<label class="star star-3" for="star-3"></label>
											<input class="startclick star star-2" id="star-2" type="radio" name="star"/>
											<label class="star star-2" for="star-2"></label>
											<input class="startclick star star-1" id="star-1" type="radio" name="star"/>
											<label class="star star-1" for="star-1"></label>
										</form>
									</div>	`;
		$("#doctorRating").val('0');
		$("#starboxContainer").empty();
		$("#successMsg_"+id).hide();
		$("#erroMsg_"+id).hide();
		$("#completeButton_"+id).show();
		$("#accept_report_loading_msg_"+id).hide();
		$("#starboxContainer").append(starboxes);
		$("#ratingModels").modal('show');

		$('.startclick').on('click',function(){
		   $("#doctorRating").val($(this).attr('id').split('-')[1]);
		});	
	}

    function acceptReportFunc(id){
	   var rating =	$("#doctorRating").val();
	   if(rating == 0){
		  $("#erroMsg_"+id).show();
		  $("#erroMsg_"+id).html("Please rate the doctor before completing report.");
		  setTimeout(() => {
			$("#erroMsg_"+id).hide();
		  }, 5000);
	   }
	   else{
            	$.ajax({
				url:"{{ url('acceptreports') }}",
				data:{rating:rating,'id':id,'_token':'{{ csrf_token() }}'},
				type:'post',
				dataType:'json',
				beforeSend:function(){
					$("#accept_report_loading_msg_"+id).show();
					$("#successMsg_"+id).hide();
					$("#erroMsg_"+id).hide();
					$("#completeButton_"+id).hide();
				},
				success:function(res) {
					$("#accept_report_loading_msg_"+id).hide();
					$("#completeButton_"+id).hide();
					$("#erroMsg_"+id).hide();
					$("#acptButton_"+id).hide();
					$("#successMsg_"+id).show();

					setTimeout(() => {
						$("#successMsg_"+id).hide();
						$("#ratingModels").modal('hide');
						history.back(-1);
					}, 4000);
				},
				error:function(xhr,status,error){
					$("#accept_report_loading_msg_"+id).hide();
					$("#successMsg_"+id).hide();
					$("#completeButton_"+id).show();
					$("#erroMsg_"+id).show();
					$("#erroMsg_"+id).html("Something went wrong try again later");
					console.log('xhr => ',xhr," status => ",status," error => ",error)
				}
			});
	   } 
}




</script>

