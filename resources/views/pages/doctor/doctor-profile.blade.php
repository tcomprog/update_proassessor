@extends('layouts.doctor-profile-custom')

@section('content')	
<link href="{{ asset('css/doctor_profiles.css') }}" rel="stylesheet">

	 <!-- Banner Section -->

    <section class="intro_banner_sec inner_banner_sec" style="padding-top:25px; padding-bottom:25px;">
        <div class="container">

            <div class="row align-items-center">

                <div class="col-md-6">

                    <div class="welcome_text">

                        <h1>My Profile</h1>

                    </div>

                </div>

            
            </div>
        </div>
    </section>
	
	<section class="dashboard_sec" style="padding:30px 0px;">
        <div class="container disc">
			{{-- <div class="profle_div">Profile</div> --}}
            @if($users->account_status == 1)
			<div class="row" style="padding: 0px 30px;">
				<div class="col-md-6 col-sm-12" style="margin-top: 20px;">
					<div class="rating-block">
						<h4 style="color: #323B62">Average user rating</h4>
						<h2 class="bold padding-bottom-7">{{$doc_avg}} <small>/ 5</small></h2>
						<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
						<button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
						  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
						</button>
					</div>
				</div>

				<div class="col-md-5 col-sm-12" style="margin-top: 20px;">
					<h4 style="color: #323B62">Rating breakdown</h4>
					<div class="pull-left singleProgressdesing">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">5 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px; flex:1">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 1000%;background:#50C878;">
								<span class="sr-only">80% Complete (danger)</span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;">{{$rating_5}}</div>
					</div>

					<div class="pull-left singleProgressdesing">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">4 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px; flex:1">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80%; background:#0096FF">
								<span class="sr-only">80% Complete (danger)</span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;">{{$rating_4}}</div>
					</div>

					<div class="pull-left singleProgressdesing">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">3 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px; flex:1">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width: 60%;background:#6F8FAF">
								<span class="sr-only">80% Complete (danger)</span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;">{{$rating_3}}</div>
					</div>

					<div class="pull-left singleProgressdesing">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">2 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px; flex:1">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: 40%;background:#FFC300">
								<span class="sr-only">80% Complete (danger)</span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;">{{$rating_2}}</div>
					</div>

					<div class="pull-left singleProgressdesing">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">1 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px; flex:1">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: 20%;background: #c72a00">
								<span class="sr-only">80% Complete (danger)</span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;">{{$rating_1}}</div>
					</div>
				</div>		

			</div>

			<hr>
			@endif

            <div class="row" style="padding: 20px 30px;">
				<div class="alert alert-success fade show" role="alert" id="updatesuccess" style="display:none;">
					Profile updated successfully
				</div>
                 <div class="col-md-12">
						<form class="profile_update" id="update_profile_doctor">
							<div class="row">

							<div class="col-md-6">
								<div class="form-group">								
									<label for="name">Name<sup class="requirdInput">*</sup></label>
									<input type="text" class="form-control-file inputsss" id="name" name="name" aria-describedby="name" placeholder="Name" value="{{ ucfirst($users->name) }} ">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="email">Email<sup class="requirdInput">*</sup></label>
									<input type="email" class="form-control-file inputsss" id="email" name="email" aria-describedby="email" placeholder="Email" value="@if($ac_type == 1) {{$users->email}} @endif" disabled>
								</div>
							</div>


							<input type="hidden" id="accessDoctorID" name="accessDoctorID" value="{{$users->id}}">

							
							<!---start---->
							
							<?php 
								// $categoryids=explode(',',$users->doctor->category_id);  
								 //foreach ($categoryids as $categoryid){
								// print_r($categoryid);echo "<br>";
								// }					
							?> 
							<!-----end----->	
							
							<div class="col-md-6">
								<div class="form-group">
									<label for="license">Designation<sup class="requirdInput">*</sup></label>
									<select class="form-control-file category_select pro_category_select @error('credentials') is-invalid @enderror" id='credentials' name="credentials">
                                            <option value="">Choose Designation</option>
                                            <?php  foreach ($designations as $designation){ ?>
                                            <option value="<?php echo $designation->id; ?>" <?php if(($designation->id)==($users->desgination)){echo "selected";} ?> ><?php echo $designation->designation; ?></option>
                                            <?php } ?>
                                    </select>

								</div>
							</div>
							
                            {{-- <?php  if(($users->profilepic)!=null){ ?>
							<div class="col-md-4">
                                <div class="form-group">								
                                    <label for="name">Update Profile Picture</label>
                                    <input type="file" class="form-control-file fil_upload inputsss"  name="image">
									<p style="color:#DA1919;">Allowed formats: jpg, jpeg, png</p>
                                    <span class="text-danger" id="lawyerinvalidformat" style="color:red;"></span>
                                </div>								
                            </div>
							<div class="col-md-2" style="display: flex">
                                <div class="form-group" style="display: flex; align-items:center; margin-bottom:0px !important;">
									<!--<label for="name">Present Profile Picture</label>	-->
                                      <img  src="uploads/profilepics/<?php echo $users->profilepic; ?>" class="profile_update_img">									  
                                
									  <a class="" title="Remove Profile Picture" id="removeprofilepic" style="color: #DA1919;font-weight: 600;font-size: 15px;">
										<span class="material-icons" style="color: #DA1919; font-size:24;">delete</span>
									  </a>
								
									</div>	
							
                            </div>
							<?php  }else { ?>
							<div class="col-md-6">
                                <div class="form-group">								
                                    <label for="name">Upload Profile Picture</label>
                                    <input type="file" class="form-control-file fil_upload inputsss" name="image">
									<p style="color:#DA1919;">Allowed formats: jpg, jpeg, png</p>
                                    <span class="text-danger" id="lawyerinvalidformat" style="color:red;"></span>
                                </div>
                            </div>
							
							<?php  } ?>  --}}


							<div class="col-md-6">
								<div class="form-group">
									<label for="location">City<sup class="requirdInput">*</sup></label>
									<?php  $cities = Helper::cities();  ?>  
									<select class="form-control-file category_select pro_category_select @error('location') is-invalid @enderror" id='location' name="location">
										<option value="">Choose City</option>
										<?php  foreach ($cities as $cities1){ ?>
										<option value="<?php echo $cities1->id; ?>" <?php if(($cities1->id)==($users->location)){echo "selected";} ?> ><?php echo $cities1->city_name; ?></option>
										<?php } ?>
									</select>
									<p class="city_txt" style="padding-left:8px; font-weight:bold;">Ontario, Canada</p>								
								</div>
						     </div>


							 <div class="col-md-12">
								<div class="form-group">
										<div class="demo">
											<div class="control-group">
											<label for="category">Choose Category<sup class="requirdInput">*</sup></label>
											<?php  $category = Helper::categorydetails();  ?>  
											<select class="form-control-file @error('category_id') is-invalid @enderror demo-default" id="category_id" name="category_id[]" multiple   placeholder="Choose Category">
											<?php $categoryids= ($users->doctor) ? explode(',',$users->doctor->category_id) : [];  
													foreach ($category as $categories){  ?>
													<option value="<?php echo $categories->id; ?>" <?php if(in_array($categories->id,$categoryids)){echo "selected";} ?> ><?php echo ucfirst($categories->category_name); ?></option>
												<?php  }?>
											</select>
											</div>
										</div>
								</div>
							</div> 


							<?php  if($users->doctor && ($users->doctor->cvimage)!=null){  ?>	
								<div class="col-md-6">
									<div class="form-group">								
										<label for="name">Update CV</label>
										<input type="file" class="form-control-file fil_upload inputsss"  name="cvimage">
										<p style="color:#DA1919;">Allowed formats: jpg, jpeg, png, docx, pdf</p>
										<span class="text-danger" id="cvlawyerinvalidformat" style="color:red;"></span>
									</div>
								</div>
								 <div class="col-md-6" style="display:flex; align-items:center;margin-bottom:0px !important;">
									<div class="form-group" style="display:flex;">
										<a class="" title="Delete CV" id="removecv" style="color: red;font-weight: 600;font-size: 15px;">
											<span class="material-icons" style="color: #DA1919; font-size:24;">delete</span>
										  </a>
									<!--<label for="name">Present CV</label>		-->				
										  <a  href="uploads/cv/<?php echo @$users->doctor->cvimage; ?>" target="_blank"><?php echo @$users->doctor->cvimage; ?></a><br>	
										 
									</div>
								 </div>
							<?php }else{ ?>
							<div class="col-md-12">
                                <div class="form-group">								
                                    <label for="name">Upload CV</label>
                                    <input type="file" class="form-control-file fil_upload inputsss"  name="cvimage">
									<p style="color:#DA1919;">Allowed formats: jpg, jpeg, png, docx, pdf</p>
                                    <span class="text-danger" id="cvlawyerinvalidformat" style="color:red;"></span>
                                </div>
                            </div>
							<?php } ?>	
							
							{{-- <?php  if(($users->doctor->instruction_form)!=null){  ?>	
								<div class="col-md-6 "  id="Questionnaire">
									<div class="form-group">								
										<label for="name">Update  Questionnaire </label>
										<input type="file" class="form-control-file fil_upload inputsss"  name="instructionform">
										<p style="color:#DA1919;">Allowed formats: jpg, jpeg, png, docx, pdf</p>
										<span class="text-danger" id="lawyerinvalidformat1" style="color:red;"></span>
									</div>
								</div>
								 <div class="col-md-6"  style="display: flex;align-items: center; margin-bottom:0px !important;">
									<div class="form-group" style="display: flex">
									<!--<label for="name">Present Questionnaire</label>		-->		
									<a title="Delete Questionnaire" class="removeinsform" id="removeinsform" style="color: red;font-weight: 600;font-size: 15px;">
										<span class="material-icons" style="color: #DA1919; font-size:24;">delete</span>
									</a>		
										  <a  href="uploads/instruction_form/<?php echo @$users->doctor->instruction_form; ?>" target="_blank"><?php echo @$users->doctor->instruction_form; ?></a><br>	
											
									</div>
								 </div>
							<?php }else{ ?>
							<div class="col-md-12" id="Questionnaire">
                                <div class="form-group">								
                                    <label for="name">Upload  Questionnaire</label>
                                    <input type="file" class="form-control-file fil_upload inputsss"  name="instructionform">
									<p style="color:#DA1919;">Allowed formats: jpeg, png, docx, pdf</p>
                                    <span class="text-danger" id="lawyerinvalidformat1" style="color:red;"></span>
                                </div>
                            </div>
							<?php } ?>			 --}}


							<div class="col-md-6 col-sm-12">
								<label for="license">Price<sup class="requirdInput">*</sup></label>
								<div class="range11 col-12 p-0">
								  <div class="form-group range__slider col-12 p-0">
									<input 
										@if($users->account_status == '1')
										{{'disabled=disabled style=opacity:0.7 !important;'}}
										@endif
									 type="range" step="50">
								  </div>
								  <div style="display: flex;
								  justify-content: space-between;
								  align-items: center;">
									<p id="priceDescription" class="m-0" style="font-size: 15px;
									text-align: center;
									flex: 1;">
									@if (@$users->doctor->actual_price <= 2000 && @$users->doctor->actual_price > 0)
										{{"Highly Competitive"}}
									@elseif (@$users->doctor->actual_price > 2000 && @$users->doctor->actual_price <= 3500)
										{{"Less competitive"}}
									@elseif (@$users->doctor->actual_price > 3500)
										{{"Non competitive"}}
									@endif	
								</p>
								  <div class="form-group range__value">
									<span></span>            
								  </div>
								  </div>
								</div>
								 @php
									 $bprice = $users->doctor && $users->doctor->actual_price > 0 ? $users->doctor->actual_price : 0;
								 @endphp
								<input type="hidden" name="price" id="actualPriceDoctor" value="{{$users->doctor->actual_price}}">
							</div>

							@if($users->account_status == '1')
							<div class="col-md-6 col-sm-12 p-md-4">
								@if ($users->showPriceBtn == '1')
								   <button type="button" id="changePriceRequestBtn" class="btn btn-outline-success btn-sm mr-1">Change Price</button>
								@endif
								<button type="button" id="viewChangePriceHistory" class="btn btn-outline-info btn-sm">Request History</button>
							</div>
							@else
							<div class="col-6"></div>
							@endif

							<div class="col-12 d-flex mt-3" style="align-items: center;">
								<p class="price_tagDesi">What is your charge for assessment only?<sup class="requirdInput">*</sup></p>
								<input type="text" id="assessmentOnlyCharges" name="assessmentOnlyCharges" class="form-control-file currencyInput decimialPointInput" maxlength="14"  value="{{ @$users->doctor->assessmentOnlyCharges }}" placeholder="00.00">
							</div>

							<div class="col-12 d-flex mt-3" style="align-items: center;">
								<p class="price_tagDesi">What is your change for late cancellation?<sup class="requirdInput">*</sup></p>
								<input type="text" id="lateCancellationFee" name="lateCancellationFee" class="form-control-file currencyInput decimialPointInput" maxlength="14" value="{{ @$users->doctor->lateCancellationFee }}" placeholder="00.00">
							</div>


							<div class="col-md-12 mt-3">
								<div class="form-group">
									<label for="license">Introduction</label>
									{{-- <input type="text" class="form-control-file inputsss"  aria-describedby="license" placeholder="Introduction" value="{{ $users->doctor->introduction }}" style="height:70px !important; border-radius:20px !important;"> --}}
									<textarea class="form-control inputsss" name="introduction" id="introduction" placeholder="Introduction">{{ @$users->doctor->introduction }}</textarea>
								</div>
							</div>
							

							<div class="col-md-12 mt-3">
								<div class="form-group" style="display: flex; align-item:center; margin-bottom:12px;">
								<label for="profile">Are you open for Virtual Appointment ?</label>
                                  <div style="display: flex">
									<div class="form-check" >                           
										<input class="inputsss" style="height: auto !important; margin-left:10px;" name="virtual_appointment" type="radio" value="yes"  <?php if(@$users->doctor->virtual_appointment=="yes") {echo "checked='checked'";} ?> >                               
										<label class="form-check-label" for="yes" >
											Yes
										</label>
								     </div>

									  <div class="form-check">
										<input  class="inputsss" style="height: auto !important;" name="virtual_appointment" type="radio" value="no"  
										<?php if(@$users->doctor->virtual_appointment=="no") {echo "checked='checked'";}?>>                               
											<label class="form-check-label" for="no" >
													No
											</label> 
										</div>
									</div>
								</div>
							</div>


							<div class="col-md-12">
								<div class="form-group" style="display: flex; align-item:center; margin-bottom:12px;">
									<label for="profile">Do you charge HST? 
										{{-- @if (@$users->doctor->hst == 'yes' && !empty(@$users->doctor->hst_number) && @$users->doctor->hst_number != 'null')
											{{@$users->doctor->hst_number}}
										@endif --}}
									</label>
									<div style="display: flex">
											<div class="form-check">                           
													<input name="hst" class="hst_radio" type="radio" value="yes" style="height: auto !important; margin-left:10px;"   <?php if(@$users->doctor->hst=="yes") {echo "checked='checked'";} ?> >                               
													<label class="form-check-label" for="yes" >
														Yes
													</label>
											</div>
										</li>
											<div class="form-check" >
											<input  name="hst" class="hst_radio" type="radio" value="no"   style="height: auto !important;"
											<?php if(@$users->doctor->hst=="no") {echo "checked='checked'";}?> >                               
													<label class="form-check-label" for="no" >
														No
													</label> 
											</div>
									</div>
										
								</div>
							</div>


							<div style="margin-bottom: 15px; margin-left:10px;">
                                <div id="imgSectionCrp" @if (empty($users->profilepic) || $users->profilepic == 'null') style="display:none" @endif>
                                    <img class="profileiamgecls" src="{{url('uploads/profilepics/doctor/'.$users->profilepic)}}" alt="">
                                    <span class="material-icons deletecropimga" style="color: #FFB81C; font-size:24px; margin-left:10px">delete</span>
                                    <img id='deleteCropImageLoader' width="30" style="display: none;" src="{{asset('img/loading.gif')}}" alt="">
                                </div>

                                <button @if (!empty($users->profilepic) && $users->profilepic != 'null') style="display:none" @endif class="uploadbtn"  onclick="chooseImageModal()" type="button">Upload profile image</button>
                            </div>

		
							{{-- <h3 class="pr_heading">Private Information</h3>

							<div class="col-md-6">
								<div class="form-group">
									<label for="license">Licence Number</label>
									<input type="text" class="form-control-file" name="licence_number" id="licence_number" aria-describedby="license" placeholder="Licence Number" value="{{ $users->doctor->licence_number }}">
								</div>
							</div>
                            <div class="col-md-6">
								<div class="form-group">
									<label for="phone">Phone Number</label>
									<input type="text" class="form-control-file" id="phone" name="phone" aria-describedby="phone" placeholder="Phone Number" value="{{ $users->phone }}">
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label for="address">Business Address</label>
									<input type="text" class="form-control-file" id="address" name="business_address" aria-describedby="address" placeholder="Business Address" value="{{ $users->business_address }}">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="license">Change Password</label>
									<input type="password" class="form-control-file" name="password" id="password" aria-describedby="license" placeholder="Password" >
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="license">Confirm Change Password</label>
									<input type="password" class="form-control-file" name="confirm_password" id="confirm_password" aria-describedby="license" placeholder="Confirm Password" >
								</div>
							</div> --}}
							 
								<input type="hidden" name="id" id="id" value="<?php  echo $users->id; ?>">
							</div>

							<div style="width: 100%; text-align:center">
							<button type="submit" class="btn btn-primary update_btn col-md-6 newbtn" id="update_profile_doctor_btn">Update</button>
						   </div>
						</form>
					</div>
            </div>
        </div>
		
		<!---delete profile pic---->
		<div class="modal fade" id="deleteprofilepicdiv" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
				
			<div class="modal-dialog modal-dialog-centered" role="document">

				<div class="modal-content login_box">

				  <div class="modal-header" >

					<h5 class="modal-title" id="loginTitle" >Are Sure Want to Delete Profile Pic?</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

					  <span aria-hidden="true">&times;</span>

					</button>

				  </div>

				  <div class="modal-body">

						  <form id="deleteprofilepic_form"><!-------method="POST" action="{{ route('login') }}--------->
								@csrf

								<div class="row">

									<div class="col-md-12">

										<div class="form-group">
											<input type="hidden" id="userid" name="userid" value="{{ $users->id }}">
											<input type="hidden" id="userpic" name="userpic" value="{{ $users->profilepic }}">
										</div>

									</div>
									<div class="col-md-12">
									   <p id="successtask1" style="color:green;display:none;">Profile Picture Deleted Successfully</p>
									</div>
								</div>

								<button type="submit" class="btn btn-primary submit_btn"  id="deleteprofilepic_btn">Yes</button>
								<button type="submit" class="btn btn-primary submit_btn"  id="deleteprofilepic_btn1" data-dismiss="modal" aria-label="Close">No</button>

							</form>

				  </div>

				</div>

			  </div>

			</div>
			
			<!---delete cv---->
		<div class="modal fade" id="removecvdiv" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
				
			<div class="modal-dialog modal-dialog-centered" role="document">

				<div class="modal-content login_box">

				  <div class="modal-header" >

					<h5 class="modal-title" id="loginTitle" >Are Sure Want to Delete CV?</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

					  <span aria-hidden="true">&times;</span>

					</button>

				  </div>

				  <div class="modal-body">

						  <form id="deletecv_form"><!-------method="POST" action="{{ route('login') }}--------->
								@csrf

								<div class="row">

									<div class="col-md-12">

										<div class="form-group">
											<input type="hidden" id="cvuserid" name="cvuserid" value="{{ $users->id }}">
											<input type="hidden" id="cvuserpic" name="cvuserpic" value="{{ @$users->doctor->cvimage }}">
										</div>

									</div>
									<div class="col-md-12">
									   <p id="delscsstask" style="color:green;display:none;">CV Deleted Successfully</p>
									</div>
								</div>

								<button type="submit" class="btn btn-primary submit_btn"  id="deletecv_btn">Yes</button>
								<button type="submit" class="btn btn-primary submit_btn"  id="deleteprofilepic_btn_1" data-dismiss="modal" aria-label="Close">No</button>

							</form>

				  </div>

				</div>

			  </div>

			</div>
			
			
	<!---delete ins form---->
		<div class="modal fade" id="removeinsdiv" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
				
			<div class="modal-dialog modal-dialog-centered" role="document">

				<div class="modal-content login_box">

				  <div class="modal-header" >

					<h5 class="modal-title" id="loginTitle" >Are Sure Want to Delete Questionnaire?</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

					  <span aria-hidden="true">&times;</span>

					</button>

				  </div>

				  <div class="modal-body">

						  <form id="deleteins_form"><!-------method="POST" action="{{ route('login') }}--------->
								@csrf

								<div class="row">

									<div class="col-md-12">

										<div class="form-group">
											<input type="hidden" id="insuserid" name="insuserid" value="{{ $users->id }}">
										</div>

									</div>
									<div class="col-md-12">
									   <p id="delscssins" style="color:green;display:none;">Questionnaire Deleted Successfully</p>
									</div>
								</div>

								<button type="submit" class="btn btn-primary submit_btn"  id="deleteins_btn">Yes</button>
								<button type="submit" class="btn btn-primary submit_btn"  id="deleteins_btn1" data-dismiss="modal" aria-label="Close">No</button>

								
							</form>

				  </div>

				</div>

			  </div>

			</div>
			
    </section>


	@include('includes.imageCroping');


@endsection

{{-- -----------------  Price change model --------------------- --}}
 <div class="modal fade" id="changePriceRequest" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" style="color:#545C84 !important">Send change price reqest</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">

			 <div id="pr_success" style="display: none" class="alert alert-success" role="alert">
				A simple success alert—check it out!
			  </div>
			  <div id="pr_error" style="display: none" class="alert alert-danger" role="alert">
				A simple danger alert—check it out!
			  </div>
			  
		 
			<div class="col-md-12">
				<div class="form-group">
					<label for="email">New Price<sup class="requirdInput">*</sup></label>
					<input maxlength="15" type="text" class="form-control-file inputsss" id="p_drnewPrice" placeholder="Price">
				</div>
			</div>

			<div class="col-md-12 mt-3">
				<div class="form-group">
					<label>Description</label>
					<textarea maxlength="255" class="form-control" rows="3" id="pr_description" placeholder="Description"></textarea>
				</div>
			</div>

           <input type="hidden" id="p_doctorID" value="{{$users->id}}">
           <input type="hidden" id="p_oldPrice" value="{{@$users->doctor->actual_price}}">

		   <button class="btn btn-outline-success px-4" id="p_changeRequestBtn" style="margin-left:17px">Submit</button>
		   <img width="30" id="p_loadingPr" style="display: none" src="{{asset("img/loading.gif")}}" alt="">

		</div>
	  </div>
	</div>
  </div>
  



  {{-- -----------------  Price request history model --------------------- --}}
 <div class="modal fade" id="changePriceHostoryModel" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header" style="border:0px; padding-bottom: 0px;">
		  <h5 class="modal-title" style="color:#545C84 !important">Price request history</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">

			<table class="table table-striped">
				<thead>
				  <tr>
					<th scope="col">Date</th>
					<th scope="col">Old Price</th>
					<th scope="col">New Price</th>
					<th scope="col">Status</th>
				  </tr>
				</thead>
				<tbody>
					@if (count($pr_request) > 0)
					  @foreach ($pr_request as $row)
						<tr>
							<td>{{date('d-m-Y',strtotime($row->created_at))}}</td>
							<td>${{$row->oldPrice}}</td>
							<td>${{$row->newPrice}}</td>
							<td>
                                @if($row->status == '0')
								  <span class="text-primary">Pending</span>
								@elseif ($row->status == '1')
								  <span class="text-success">Accepted</span>
								@elseif ($row->status == '2')
								  <span class="text-danger">Rejected</span>
								@endif
							</td>
						</tr>
					  @endforeach	
					@endif 
				</tbody>
			  </table>

		
		</div>
	  </div>
	</div>
  </div>




  {{-- -----------------  HST Price model --------------------- --}}
 <div class="modal fade" id="hstNumberModel" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title" style="color:#545C84 !important">HST Number</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">

			 <div id="hst_success" style="display: none" class="alert alert-success" role="alert">
				A simple success alert—check it out!
			  </div>
			  <div id="hst_error" style="display: none" class="alert alert-danger" role="alert">
				A simple danger alert—check it out!
			  </div>
			  
		 
			<div class="col-md-12">
				<div class="form-group">
					<label for="email">HST Number<sup class="requirdInput">*</sup></label>
					<input maxlength="15" type="text" class="form-control-file inputsss" id="hst_number_inp" placeholder="HST Number">
				</div>
			</div>

           <input type="hidden" id="hstdoctorID" value="{{$users->id}}">

		   <button class="btn btn-outline-success px-4" id="changeHstValue" style="margin-left:17px">Submit</button>
		   <img width="30" id="hst_loadingPr" style="display: none" src="{{asset("img/loading.gif")}}" alt="">

		</div>
	  </div>
	</div>
  </div>
  




  