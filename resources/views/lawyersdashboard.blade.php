@extends('layouts.inner')
@section('content')
<!-- Process Section -->
<section class="dashboard_sec">
    <div class="container">
       <!-- <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Dashboard</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Patients</a>
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Chatroom</a>
            </div>
        </nav>-->
       <!-- <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="search_sec">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6433072.288450607!2d-103.0968118028745!3d38.06692077779471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited%20States!5e0!3m2!1sen!2sin!4v1618484202297!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                                <div class="search_box">
                                    <form  method="GET" action="{{ route('lawyer/dashboard') }}">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2>Search a Doctor</h2>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="keyword" name="keyword"  aria-describedby="name" placeholder="Name or Email" value="<?php if(isset($_GET['keyword'])){echo $_GET['keyword'];}?>">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="location" name="location" aria-describedby="location" placeholder="Location">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="date" class="form-control" id="date" aria-describedby="date" placeholder="Date" name="date">
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary search_btn">Search</button>
                                    </form>
                                </div>
                                <div class="calendar_box">
                                    <div id="my-calendar" class="calendar"></div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" id="myTable">
                            <thead class="thead-dark">
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Location</th>
                                <th scope="col">License Number</th>
                                <th scope="col">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $number = 1; ?>
                            @if (!$users->isEmpty())
                                 @foreach ($users as $user)      
                                <tr>
                                    <th scope="row">{{ $number }}</th>
                                    <?php $number++; ?>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->location }}</td>
                                    <td>{{ $user->location }}</td>
                                    <td><a href="#" class="invite_btn" id="invitee_user_id" data-toggle="modal" data-id ="{{ $user->id}}" data-target="#Availablecalendar">Available Date</a></td>
                                </tr>                                
                                @endforeach
                                @else
                                <tr colspan="7"><td><b>No Records Found</b></td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
          <!--  </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">...</div>
            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">...</div>
        </div>-->
    </div>
</section>
<!-- Process Section -->
<!-- Available Calendar Model -->
<div class="modal fade" id="Availablecalendar" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content login_box">
      <div class="modal-header">
        <h5 class="modal-title" id="calendarTitle">Please Select Available Date</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="modal-body">
        <form  id="sendinvitation" >
            @csrf
            <div class="calendar">
                    <div id="date-calendar"></div>
                    <input id="invitation_date" type="text"  name="invitation_date" >
                    <input id="invitee_user_id_new" type="text"  name="invitee_user_id_new" >
                    <p id="invitefail" style="color:red;"></p>
            </div>
            <button type="submit" class="invite_btn" id="invitebtn">Invite</button>
        </form>
     </div> 
     </div>     
    </div>
  </div>
</div>
@endsection
