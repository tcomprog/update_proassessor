<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClinicSpecialOffer extends Model
{
    use HasFactory;
    protected $table = 'clinic_special_offers';
}
