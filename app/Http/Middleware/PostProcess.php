<?php

namespace App\Http\Middleware;

use App\Article; //
use Closure;
use Illuminate\Contracts\Auth\Guard; //
use Illuminate\Http\Request;

class PostProcess
{
    public function handle(Request $request, Closure $next)
    {

        $accessTime = time().'_'.$request->user()->id.'_'.$request->ip();
        $response = $next($request);
        return $response->withCookie(cookie()->forever('lastaccess', $accessTime));
    }
}
