@extends('layouts.admin_app')


@section('content')

<div class="container-fluid bg-white rounded py-4 adminPageContainer">

    {{-- <div class="row">
		<div class="col-md-12">
			 <nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
				<li class="breadcrumb-item active"><a href="{{url('/doctors')}}">Rejected Requests</a></li>
			  </ol>
			</nav>
		</div>
    </div> --}}


    <div class="row">

        <h4 class="inv-content-header" style="margin-bottom:0px;">Category List</h4>

        <div class="d-flex justify-content-end mb-3">
            <button id="addCategoryBtn" class="btn btn-outline-success btn-sm">
                <i class="fa fa-plus" style="padding-right:3px !important;" aria-hidden="true"></i>
                Add Category
            </button>
        </div>

        <table id="datatbl11" class="row-border hover cell-border" style="width:100%">
            <thead>
                <tr>
                 <th width="15%">Name</th>
                 <th width="15%">Date</th>
                 <th width="10%"></th>
                </tr>
            </thead>

             <tbody>
                @foreach ($records as $ind => $row)
                @php
                   $str = (strlen($row->desc) > 110) ? substr($row->desc,0,100)."..."  : $row->desc;
                @endphp
                  <tr>
                    <td>{{$row->category_name}}</td>
                    <td>{{date("d/m/Y",strtotime($row->updated_at))}}</td>
                    <td align="center">
                        <button data-id="{{$row->id}}" data-name="{{$row->category_name}}" class="btn btn-warning btn-sm editCityName">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </button>
                    </td>
                  </tr>
                @endforeach

                
            </tbody>

        </table>

    </div>
 

    {{--  ******** Add type model *********  --}}
    <div id="commonModel" class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title fw-bold" id="popupTitle">Add New Lawyer Type</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">

                  <div class="alert alert-success msg_success" style="display:none" role="alert">
                    A simple success alert—check it out!
                  </div>

                  <div class="alert alert-danger msg_error" style="display:none" role="alert">
                    A simple danger alert—check it out!
                  </div>
            
                <div class="mb-3">
                    <label class="form-label fw-bold">Name<sup class="text-danger">*</sup></label>
                    <input type="text" maxlength="100" class="form-control rounded-pill" id="addCategoryName">
                </div>

              <input type="hidden" id="addCategoryID" value="0">
              <img width="30" style="display:none" class="loadingGif" src="{{asset("img/loading.gif")}}" alt="">
              <button id="addNewCategorySub" type="button" class="btn btn-outline-success">Submit</button>

            </div>
            
          </div>
        </div>
      </div>

@endsection


