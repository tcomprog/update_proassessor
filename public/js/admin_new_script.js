// ^^^^^^^^^^^^^^^ Validation Section ^^^^^^^^^^^^^^^^
function isEmpty(e) {
    switch (e) {
      case "":
      case 0:
      case "0":
      case null:
      case false:
      case undefined:
        return true;
      default:
        return false;
    }
}

function isEmail(mail){
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(myForm.emailAddr.value))
    {
      return (true)
    }
    return (false)
}

function isNum(num){
    let status = (Number(num) != NaN && Number(num) > 0) ? true : false;
    return status;
}

function msg_error(msg){
    $(".msg_success").hide();
    $(".msg_error").show();
    $(".msg_error").text(msg);
    setTimeout(() => {
       $(".msg_error").hide();
    }, 3000); 
}

function msg_success(msg){
    $(".msg_success").show();
    $(".msg_error").hide();
    $(".msg_success").text(msg);
    setTimeout(() => {
       $(".msg_success").hide();
    }, 3000); 
}

function commonAction(title){
    $(".msg_success").hide();
    $(".msg_error").hide();
    $(".loadingGif").hide();
    $("#popupTitle").text(title);
    new bootstrap.Modal('#commonModel').show();
}


function msg_toast(msg,type = 0){
    // 0 - success, 1 = error
    const toast = new bootstrap.Toast(document.getElementById("liveToast"))
    var toast11 = $("#liveToast");
    toast11.removeClass("text-bg-success");
    toast11.removeClass("text-bg-danger");

    if(type == 0){
        toast11.addClass("text-bg-success");
    }
    else{
        toast11.addClass("text-bg-danger");
    }
    $("#toastContent").html(msg);
    toast.show()
}
// ^^^^^^^^^^^^^^^ Validation Section ^^^^^^^^^^^^^^^^



// =================== Lawyer Types Section Start =====================
$("#addLawyerTypeBtn").on('click',function(){
    $("#lawyerTypeName").val('');
    $("#lawyerTypeCommission").val('');
    $("#lawyerTypeID").val('0');
    $("#addNewLawyerType").show();
    commonAction("Add New Lawyer Type");
});

$(".editLawyerType").on("click",function(){
   let id = $(this).data("id");
   let name = $(this).data("name");
   let commission = $(this).data("commission");
   $("#lawyerTypeName").val(name);
   $("#lawyerTypeCommission").val(commission);
   $("#lawyerTypeID").val(id);
   $("#addNewLawyerType").show();

   commonAction("Update New Lawyer Type");
});

$("#addCityBtn").on("click",function(){
    $("#addCityName").val('');
    $("#addCityID").val('0');
    $("#addCityBtn").show();

    commonAction("Add City");
});

$(".editCityName").on("click",function(){
    let id = $(this).data("id");
    let name = $(this).data("name");
  
    $("#addCityName").val(name);
    $("#addCityID").val(id);
    $("#addCityBtn").show();
 
    commonAction("Update City Name");
 });

 $("#addCategoryBtn").on('click',function(){
    $("#addCategoryName").val('');
    $("#addCategoryID").val('0');
    $("#addNewCategorySub").show();
    commonAction("Add New Category");
});

$(".editCityName").on("click",function(){
    let id = $(this).data("id");
    let name = $(this).data("name");
  
    $("#addCategoryName").val(name);
    $("#addCategoryID").val(id);
    $("#addNewCategorySub").show();
 
    commonAction("Update Category Name");
 });


 $("#addDesginationModal").on('click',function(){
    $("#addDesingationName").val('');
    $("#addDesginationID").val('0');
    $("#addDesingationSubmit").show();
    commonAction("Add New Desgination");
});

$(".editDesginationName").on("click",function(){
    let id = $(this).data("id");
    let name = $(this).data("name");
  
    $("#addDesingationName").val(name);
    $("#addDesginationID").val(id);
    $("#addDesingationSubmit").show();
 
    commonAction("Update Desgination Name");
 });

// =================== Lawyer Types Section  End  =====================