<?php

namespace App\Mail;

use App\Models\user_invitations;
use App\Models\User;

use Config;

use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

class LawyerAuthenticationStatusChange extends Mailable

{

    use Queueable, SerializesModels;

    /**

     * The order instance.

     *

     * @var \App\Models\Order

     */

    public $user;

    public $options = [];

    /**

     * Create a new message instance.

     *

     * @param  \App\Models\Order  $order

     * @return void

     */

    public function __construct(User $user,$options)

    {

        $this->user = $user;

        $this->options = $options;

    }

    /**

     * Build the message.

     *

     * @return $this

     */

    public function build()



    {



        $fromAddress = Config::get('constants.MAIL_FROM_ADDRESS');



        $site_title_old = Config::get('constants.site_title');
        $site_title=str_replace("-"," ",$site_title_old);


        if(isset($this->options['type']) && $this->options['type'] == 'approve') {            
            return $this->subject($site_title.' - Approved by Admin.')->from($fromAddress,$site_title)->view('emails.send_lawyer_authentication_status_change',['user'=>$this->user,'site_title'=>$site_title,'auth_status'=>'approve']);

        }

        if(isset($this->options['type']) && $this->options['type'] == 'decline') {             
            return $this->subject($site_title.' - Declined  by Admin.')->from($fromAddress,$site_title)->view('emails.send_lawyer_authentication_status_change',['user'=>$this->user,'site_title'=>$site_title,'auth_status'=>'decline']);
        }


    }

}