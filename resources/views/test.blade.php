@extends('layouts.doctor-custom')
@section('content')	
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>

.past-day,.day.block-date{
	color: #bbb !important;;
}
.event-container>.event-icon>div.event-bullet-event{
	background-color: #ff7575!important;
}
.day.block-date span{
	position: absolute;
    font-size: 15px;
    color: red;
    font-weight: 600;
}
#addevent_form .block-calendar-modal-btn{
	padding: 5px 10px;
    font-size: 14px;
    margin-bottom: 10px;
}

.calendar-inner .circle{
	width: 20px;
    height: 20px;
    background: #ff7575;
	vertical-align:middle;
    border-radius: 10px;
	margin-left: 10px;
	margin-right: 5px;
    display: inline-block;
}
.calendar-note p{
	margin-bottom: 0;
}
.block-circle{
    margin-left: 10px;
    color: red;
    font-weight: 600;
    font-size: 18px;
    margin-right: 3px;

}
.calendar-inner .circle.circle-red{
    background: #ff7575;
}
.calendar-inner .circle.circle-yello{
	background-color: #ffc107;
}
.highlight-due-date a{
   background-color : #ff5722 !important;
   background-image :none !important;
   color: White !important;
}
.highlight-invite-date a{
	background-color : #ffc107 !important;
   background-image :none !important;
   color: White !important;
}

.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
    border: 1px solid #e2e2e1;
    background: #e2e2e2;
    color: #6b6b6b;
}

.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover {
	border: 1px solid #c5c5c5;
    background: #f6f6f6;
    font-weight: normal;
    color: #454545;
}

.calender-note span.yellow{
	background-color : #ffc107;
}
.calender-note span.red{
	background-color : #ff5722;
}
.calender-note span{
	width: 20px;
    display: inline-block;
	border-radius: 20px;
	vertical-align: middle;
    height: 20px;
}
.calender-note p{
	margin-bottom: 8px;
	color:#444;
	font-weight: 700;
}

	
.error{color:red !important;}
input.checkBoxClass {
    margin-right: 7px;
}
.docter_list p:hover{background: linear-gradient(
90deg
, #5a8627 0%, #7aac2d 100%);}
.acceptscss p:hover{background:none;}
.rjtscss p:hover{background:none;}
.inv-content-header {
	padding: 10px;
    background: #041531;
    background: linear-gradient(90deg, #5a8627 0%, #7aac2d 100%);
    color: #fff;
    border-bottom-right-radius: 20px;
}
.invitation-tabs{
	min-height: 450px;
}
.invitation-tabs .nav.nav-tabs {
    float: left;
    display: block;
    margin-right: 20px;
    border-bottom:0;
    border-right: 1px solid #ddd;
    padding-right: 15px;
}
.invitation-tabs .nav-tabs .nav-link {
    background: #7aac2d;
	color:#fff;
}
.invitation-tabs .nav-tabs .nav-item {
	width: 220px;
	margin-bottom: 2px;
}
.invitation-tabs .tab-content>.active {
    display: block;
	font-weight: 600;
    min-height: 165px;
}
.invitation-tabs .nav.nav-tabs {
    float: left;
    display: block;
    margin-right: 20px;
    border-bottom: 0;
    border-right: 1px solid transparent;
    padding-right: 15px;
}
.docter_list .new_btn{
	position: relative;
	display: inline-block;
	top: 0;
    right: 0;
}
.acceptscss{
	display: inline-block;
	top: 0;
    right: 0;
}
.docter_list .name{
	margin: 0px 15px 6px 0px;
	color: #00396b;
    font-weight: 600;
    margin-bottom: 6px;
}
.docter_list .profile {
    width: 100px;
    height: 100px;
    border: 1px solid #eee;
    margin-top: 5px;
}
.calendar_img{
	height: auto;
    width: 25px;
    border-radius: 0px;
    position: relative;
    top: -5px;
    left: 10px;
}
.ui-datepicker-trigger{
	border: none;
    background: #041531;
    color: #fff;
    padding: 2px 10px;
    margin-left: 15px;
    border-radius: 2px;
    position: relative;
    top: -2px;
}
#ui-datepicker-div{
	margin-top: 25px;
}
.dashboard_sec .search_sec::before{background:none;}

#mdp-demo .ui-datepicker td span, #mdp-demo .ui-datepicker td a{
	text-align: center!important;
	padding: 19% .2em!important;
}
#block-calendar-modal .ui-datepicker{
width: 100%!important;
}
#block-calendar-modal .ui-datepicker .ui-datepicker-title{
	line-height:2em;
}
.position-relative1{
	position: absolute;
    right: 0px;
    top: 0px;
}
.patient_p{
	margin: 0px;
    font-size: 16px;
    font-weight: 600;
    padding: 5px 0px;
}
.patient_p:hover{
	background: transparent !important;
}
.patient_p span{
	display: inline-block;
	color: #79ab2d;
}
.invitation-tabs .nav-tabs .nav-link.active{
	border-color: #ffffff #ffffff #7aac2d #ffffff;
}
.accordion .patient_details_box .invitation-tabs .nav-item a{
	padding: 0.3rem 0rem;
    text-align: center;
    font-size: 16px;
}
.invitation-tabs .nav-tabs .nav-link {
    background: #f7f7f7;
	color:#00396B;
}
.invitation-tabs .nav-tabs .nav-item {
	width: 171px;
	margin-bottom: 2px;
	display: inline-block;
	text-align: center;
}
.invitation-tabs .tab-content>.active {
    display: block;
	font-weight: 600;
    min-height: 165px;
}
.invitation-tabs .nav.nav-tabs {
    float: none;
    display: block;
    margin-right: 0px;
    border-bottom: 0;
    border-right: 1px solid transparent;
    padding-right: 0px;
    margin-bottom: 0px;
}
.invitation-tabs .tab-content > .tab-pane{
	margin-top: 25px;
}
hr{
	height: 1px;
    background: #00518b;
}
.invitation-tabs {
    min-height: auto;
}
.inv-content-header {
    padding: 0px 0px !important;
    background: #041531;
    background: linear-gradient(90deg, #004e86 0%, #00396b 100%);
    color: #fff;
    border-bottom-right-radius: 20px;
    font-size: 24px !important;
    background: transparent !important;
    font-weight: 700;
    margin-bottom: 20px;
    color: #00518a !important;
}
.patient_details_box1 {
    border: 1px solid #eee;
    padding: 20px;
    box-shadow: 0px 0px 15px #f5f4f4;
    border-radius: 8px;
    margin-bottom: 15px;
}
.virtualbold b{
	font-size: 16px;
    color: #0e3358;
    font-weight: 600;
}
</style>

	 <!-- Banner Section -->
    <section class="intro_banner_sec inner_banner_sec">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="welcome_text">
                        <h1>Doctor's  Dashboard</h1>
                    </div>
                </div>
              <!--  <div class="col-md-6 text-right">
                    <a href="#" class="search_btn">View Invitations</a>
                </div>-->
            </div>
        </div>
    </section>
	
	<!-- Lawyer Dashboard Section -->
    <section class="dashboard_sec">
        <div class="container">
			<?php $routeName = \Request::route()->getName(); ?>
            <nav>
                <div class="nav nav-tabs" id="dashboard-tabs" role="tablist">
					<!--<a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile"    aria-selected="false">My Profile</a>-->
                    <a class="nav-item nav-link @if($activeTab == null || $activeTab == 'my-invitations' || $routeName == 'search-confirmed-patient') active @endif" id="my-invitations-tab" data-toggle="tab" href="#my-invitations" role="tab" aria-controls="my-invitations"  aria-selected="false" style="font-weight: 600;font-size: 21px;">Invitations</a>
                    <a class="nav-item nav-link @if($activeTab == 'update-calendar') active @endif" id="update-calendar-tab" data-toggle="tab" href="#update-calendar" role="tab" aria-controls="update-calendar" aria-selected="false" style="font-weight: 600;font-size: 21px;">Update Calendar </a>
					<a class="nav-item nav-link @if($activeTab == 'invoices') active @endif" id="invoices-tab" data-toggle="tab" href="#invoices" role="tab" aria-controls="invoices" aria-selected="false" style="font-weight: 600;font-size: 21px;">Invoices </a>
                  
                    <!-- <a class="nav-item nav-link @if($activeTab == 'chatroom') active @endif" id="chatroom-tab" data-toggle="tab" href="#chatroom" role="tab" aria-controls="chatroom" aria-selected="false">Chatroom</a> -->
                </div>
            </nav>
			
            <div class="tab-content" id="nav-tabContent"> 
                <div class="tab-pane fade @if($activeTab == null || $activeTab == 'my-invitations' || $routeName == 'search-confirmed-patient') show active @endif" id="my-invitations" role="tabpanel" aria-labelledby="my-invitations-tab">
					<!---added new design---->
					<div class="row1">
						<div class="accordion patient_details_box1" id="accordionExample">
						<div class="invitation-tabs">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link  @if( $routeName != 'search-confirmed-patient')  active @endif" data-toggle="tab" href="#new-invitations" role="tab" aria-controls="new-invitations">New</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#accepted-invitations" role="tab" aria-controls="accepted-invitations">Accepted</a>
							</li>
							<li class="nav-item">
								<a class="nav-link @if( $routeName == 'search-confirmed-patient')  active @endif" data-toggle="tab" href="#confirmed-invitations" role="tab" aria-controls="confirmed-invitations">Confirmed</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#expired-invitations" role="tab" aria-controls="expired-invitations">Expired</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#rejected-invitations" role="tab" aria-controls="expired-invitations">Rejected</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#cancelled-invitations" role="tab" aria-controls="expired-invitations">Cancelled</a>
							</li>
							</ul>
							<div class="tab-content" id="invitations-tab-content">
								<div class="tab-pane @if( $routeName != 'search-confirmed-patient')  active @endif" id="new-invitations" role="tabpanel">
									<div class="row">
										<div class="col-md-12">
											<h3 class="inv-content-header">New Invitations</h3>
										</div>
								 @if ($new_invitations)
									@foreach($new_invitations as $invitation)
								<div class="col-md-12">
									<div class="card patient_tab box_div new_padding">
										<div class="card-head" id="heading{{ $invitation->unique_id }}">
										  <h2 class="mb-0  docter_list" >
											   <div class="main1">

											   	<!-- Updated on 26-06-2021 -->

											   		<div class="row">
											   			<div class="col-md-2">
											   				<div class="sub">
																<?php          
																if(($invitation->user->profilepic)!=null){ ?>
																	  <img src="{{ url('uploads/profilepics/'.$invitation->user->profilepic) }}" class="profile">
																<?php
																}else {
																?> <img src="{{ asset('img/user.jpg') }}" class="profile">
																<?php  } ?>
															</div>
											   			</div>
											   			<div class="col-md-10">
											   				<div class="row mt-2">
											   					<div class="col-md-7">
											   						 <span class="name"><?php echo ucfirst($invitation->user->name); ?> &nbsp;<?php if($invitation->user->lawyer->lawfirmname!=null){ ?> ( <?php echo ucfirst($invitation->user->lawyer->lawfirmname);?>)<?php }?></span>  
											   					</div>
											   					<div class="col-md-5">
											   						<?php  
											    
																	 if($invitation->status=="pending" && strtotime($invitation->created_at) > strtotime($expire_time)) { ?>
																	   <a href="#" class="nav-link addpatient_btn accept-invitation-btn new_btn" style="right:0%;" data-id="{{ $invitation->id }}">Accept</a>
																	   <div class="acceptscss">
																	   <!--<p style="display:none;color:green;text-align: center;" id="acceptscss"><b>Invitation Accepted</b></p>-->
																	   </div>
																	   <a  class="nav-link addpatient_btn reject-inv-btn new_btn"type="button" id="int_reject" data-id="{{ $invitation->id }}" style="background:#d61f12;">Reject</a>
																	   <div class="rjtscss">
																	  <!-- <p style="display:none;color:red;text-align: center;" id="rjtscss"><b>Invitation Rejected</b></p>-->
																	   </div>
																		<?php  }  ?>
																		
																		 <?php  if($invitation->status=="active"){ ?>											   
																		<p class="nav-link addpatient_btn" style="border-radius:0px;">Accepted</p>
																		<?php  }  ?>
																		 <?php  if($invitation->status=="rejected"){ ?>											   
																		<a  class="nav-link addpatient_btn"  style="background:#d61f12;border-radius:0px;" >Rejected</a>
																		<?php  }  ?>
																		 <?php  if($invitation->status=="pending" && strtotime($invitation->created_at) <= strtotime($expire_time)){ ?>											   
																		<a  class="nav-link addpatient_btn" style="background:#e0ac12;border-radius:0px;">Expired</a>
																		<?php  }  ?>
											   					</div>
											   				</div>
											   				<div class="row">
											   					<div class="col-md-5">
											   						<span class="calendar_p virtualbold">
																		<span style="color: #00396b;font-weight: 600;margin-bottom: 5px;">Appointment Date</span><img src="{{ url('img/calendar.png') }}" class="calendar_icon"><?php echo date('d F Y', strtotime($invitation->invitation_date )); ?> |  {{ $invitation->timeslot }} 
																		<?php  if($invitation->appointment_type=='virtual') { ?>
																			<b> - Virtual</b>
																		<?php  } ?>
																		</span>
												   					</div>
												   					<div class="col-md-7">
												   						<span class="calendar_p" style="margin-top: 0%;">
																	<span style="color: red;font-weight: 600;margin-bottom: 5px;">Report Due Date:</span><img src="{{ url('img/calendar.png') }}" class="calendar_icon"><?php echo date('d F Y', strtotime($invitation->duedate )); ?>		
																	
																	<label for="view-cal-inp{{ $invitation->id }}"><img src="{{ asset('img/calendar_new.png') }}" class="calendar_img"><input type="hidden" value="{{ date('m/d/Y',strtotime($invitation->invitation_date)) }}"  id="view-cal-inp{{ $invitation->id }}" style="margin-left:60px;cursor:pointer;" class="calendar_icon my-calendar-input" data-invite-date="{{ date('m/d/Y',strtotime($invitation->invitation_date)) }}"></label>
																 </span> 
											   					</div>																
											   				</div>
											   			</div>
											   		</div>
											   		<!-- Updated on 26-06-2021 -->
											   		
											   </div>
										  </h2>		
										 <a class="mb-0 collapsed collapsed read-more-block" data-toggle="collapse" data-target="#collapse{{ $invitation->unique_id }}" aria-expanded="false" aria-controls="collapse{{ $invitation->unique_id }}">
											 <img src="{{ asset('images/up-chevron.svg') }}" class="arrow-up" width="20"  alt="up">
											<img src="{{ asset('images/down-chevron.svg') }}" class="arrow-down" width="20" alt="down">
										 </a>
										</div>    
										<div id="collapse{{ $invitation->unique_id }}" class="collapse" aria-labelledby="heading{{ $invitation->unique_id }}" data-parent="#accordionExample">
										  <div class="card-body">
												 <p><span class="img_bg"><img src="{{ asset('img/address.svg') }}" class="address"></span> <span>{{ $invitation->user->business_address }}</span></p>
											   <p style="margin: 0px 0px 5px;">Website Url: <span>{{$invitation->user->lawyer->website_url }}</span></p>
										  </div>
										</div>
									</div>
								</div>	
								@endforeach
								@else
								<div class="col-md-12">
									<h4 style="text-align:center;">No new Invitations received.</h4>
								</div>
								@endif
							</div>
						</div>
						<div class="tab-pane" id="accepted-invitations" role="tabpanel">
							<div class="row">
								<div class="col-md-12">
									<h3 class="inv-content-header">Accepted Invitations</h3>
									@if (Session::has('success'))
										<div class="mt-2 alert alert-success">{{ Session::get('success') }}</div>
									@endif
								</div>
								@if ($accepted_invitations)
									@foreach($accepted_invitations as $invitation)								
								<!---start--->
								<div class="col-md-12">
									<div class="card patient_tab box_div new_padding">
										<div class="card-head" id="heading{{ $invitation->unique_id }}">
										  <h2 class="mb-0  docter_list" >
											   <div class="main1">

											   	<!-- Updated on 26-06-2021 -->

											   		<div class="row">
											   			<div class="col-md-2">
											   				<div class="sub">
																<?php          
																if(($invitation->user->profilepic)!=null){ ?>
																	  <img src="{{ url('uploads/profilepics/'.$invitation->user->profilepic) }}" class="profile">
																<?php
																}else {
																?> <img src="{{ asset('img/user.jpg') }}" class="profile">
																<?php  } ?>
															</div>
											   			</div>
											   			<div class="col-md-10">
											   				<div class="row mt-2">
											   					<div class="col-md-7">
											   						 <span class="name"><?php echo ucfirst($invitation->user->name); ?> &nbsp;<?php if($invitation->user->lawyer->lawfirmname!=null){ ?> ( <?php echo ucfirst($invitation->user->lawyer->lawfirmname);?>)<?php }?></span>  
											   					</div>
											   					<div class="col-md-5">											   
																		<p class="nav-link addpatient_btn" style="border-radius:0px;">Accepted</p>
											   					</div>
											   				</div>
											   				<div class="row">
											   					<div class="col-md-5">
											   						<span class="calendar_p virtualbold">
																		<span style="color: #00396b;font-weight: 600;margin-bottom: 5px;">Appointment Date</span><img src="{{ url('img/calendar.png') }}" class="calendar_icon"><?php echo date('d F Y', strtotime($invitation->invitation_date )); ?> |  {{ $invitation->timeslot }} 
																		<?php  if($invitation->appointment_type=='virtual') { ?>
																			<b> - Virtual</b>
																		<?php  } ?>
																		</span>
												   					</div>
												   					<div class="col-md-7">
												   						<span class="calendar_p" style="margin-top: 0%;">
																	<span style="color: red;font-weight: 600;margin-bottom: 5px;">Report Due Date:</span><img src="{{ url('img/calendar.png') }}" class="calendar_icon"><?php echo date('d F Y', strtotime($invitation->duedate )); ?>	
																 </span> 
											   					</div>
											   				</div>
											   			</div>
											   		</div>
											   		<!-- Updated on 26-06-2021 -->
											   		
											   </div>
										  </h2>		
										 <a class="mb-0 collapsed collapsed read-more-block" data-toggle="collapse" data-target="#collapse{{ $invitation->unique_id }}" aria-expanded="false" aria-controls="collapse{{ $invitation->unique_id }}">
											 <img src="{{ asset('images/up-chevron.svg') }}" class="arrow-up" width="20"  alt="up">
											<img src="{{ asset('images/down-chevron.svg') }}" class="arrow-down" width="20" alt="down">
										 </a>
										</div>    
										<div id="collapse{{ $invitation->unique_id }}" class="collapse" aria-labelledby="heading{{ $invitation->unique_id }}" data-parent="#accordionExample">
										  <div class="card-body">
												 <p><span class="img_bg"><img src="{{ asset('img/address.svg') }}" class="address"></span> <span>{{ $invitation->user->business_address }}</span></p>
											   <p style="margin: 0px 0px 5px;">Website Url: <span>{{$invitation->user->lawyer->website_url }}</span></p>
										  </div>
										</div>
									</div>
								</div>	
								<!---end--->
								@endforeach
								@else
								<div class="col-md-12">
									<h4 style="text-align:center;">No Accepted Invitations found.</h4>
								</div>
								@endif
							</div>
						</div>
							<div class="tab-pane @if( $routeName == 'search-confirmed-patient')  active @endif" id="confirmed-invitations" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										<h3 class="inv-content-header">Confirmed Invitations</h3>									
									</div>									
									<div class="col-md-4">
										<form class="search_form" method="GET" action="{{ route('search-confirmed-patient') }}">
											<div class="input-group">
												<input class="form-control" id="system-search" name="lawyerkeyword" placeholder="Search" value="<?php if(isset($_GET['lawyerkeyword'])){echo $_GET['lawyerkeyword'];}?>">
												<span class="input-group-btn">
													<button type="submit" class="btn btn-default search_btn_new">
														<!--<img src="img/search.png">-->
														<img src="{{ url('img/search.png') }}">
													</button>
												</span>
											</div>
										</form>
									</div>
									<?php  if(isset($_GET['lawyerkeyword'])) { ?>
										<div class="col-md-1">
											<a class="nav-link" style="font-size: 19px;" href="{{url('doctor-dashboard')}}/my-invitations/#confirmed-invitations" >clear</a>
										</div>
									<?php } ?>
									<div class="col-md-12">
										<div class="search_sec">
											<div class="row1">
												<div class="accordion" id="accordionExample">
													<div class="row">
														 @if ($confirmed_invitations)
														<?php   foreach($confirmed_invitations as $invitation){    ?>
														<!---start--->
														<div class="col-md-12">
															<div class="card patient_tab box_div new_padding">
																<div class="card-head" id="heading{{ $invitation->unique_id }}">
																<h2 class="mb-0  docter_list" >
																	<div class="main1">

																		<!-- Updated on 26-06-2021 -->

																			<div class="row">
																				<div class="col-md-2">
																					<div class="sub">
																						<?php          
																						if(($invitation->user->profilepic)!=null){ ?>
																							<img src="{{ url('uploads/profilepics/'.$invitation->user->profilepic) }}" class="profile">
																						<?php
																						}else {
																						?> <img src="{{ asset('img/user.jpg') }}" class="profile">
																						<?php  } ?>
																					</div>
																				</div>
																				<div class="col-md-10">
																					<div class="row mt-2">
																						<div class="col-md-7">
																							
																						<span class="name">{{ $invitation->user->name }}<?php  if($invitation->user->lawyer->lawfirmname!=null){?> (<?php echo ucfirst($invitation->user->lawyer->lawfirmname); ?>)  <?php } 
																						if($invitation->status=="active" && $invitation->is_patient_accepted == 'yes') { ?>
																							<a href="{{url('invitedetail/'.$invitation->unique_id.'/chatroom')}}" title="Chat" class="position-relative1"><img src="{{ url('img/messenger.png') }}" class="chat_icon">
																							@if(Helper::un_read_messages(Auth::id(),$invitation->id))	
																								<label class="badge badge-warning notification_badge" style="left:inherit">{{ Helper::un_read_messages(Auth::id(),$invitation->id) }}</label>
																							@endif
																							</a>
																							<?php  } ?>
																						</span>  
																						<p class="patient_p">Patient: <span><?php echo  ucfirst($invitation->patient->name); ?></span></p>
																						</div>
																						<div class="col-md-5">											   
																						<a class="nav-link addpatient_btn" href="{{ url('/invitedetail/'.$invitation->unique_id) }}">View More</a>
																						</div>
																					</div>
																					<div class="row">
																						<div class="col-md-5">
																							<span class="calendar_p virtualbold">
																								<span style="color: #00396b;font-weight: 600;margin-bottom: 5px;">Appointment Date</span><img src="{{ url('img/calendar.png') }}" class="calendar_icon"><?php echo date('d F Y', strtotime($invitation->invitation_date )); ?> |  {{ $invitation->timeslot }} 
																								<?php  if($invitation->appointment_type=='virtual') { ?>
																									<b> - Virtual</b>
																								<?php  } ?>
																								</span>
																						</div>
																						<div class="col-md-7">
																							<span class="calendar_p" style="margin-top: 0%;">
																						<span style="color: red;font-weight: 600;margin-bottom: 5px;">Report Due Date:</span><img src="{{ url('img/calendar.png') }}" class="calendar_icon"><?php echo date('d F Y', strtotime($invitation->duedate )); ?>	
																						</span> 
																						</div>
																					</div>
																				</div>
																			</div>
																			<!-- Updated on 26-06-2021 -->
																			
																	</div>
																</h2>		
																<a class="mb-0 collapsed collapsed read-more-block" data-toggle="collapse" data-target="#collapse{{ $invitation->unique_id }}" aria-expanded="false" aria-controls="collapse{{ $invitation->unique_id }}">
																	<img src="{{ asset('images/up-chevron.svg') }}" class="arrow-up" width="20"  alt="up">
																	<img src="{{ asset('images/down-chevron.svg') }}" class="arrow-down" width="20" alt="down">
																</a>
																</div>    
																<div id="collapse{{ $invitation->unique_id }}" class="collapse" aria-labelledby="heading{{ $invitation->unique_id }}" data-parent="#accordionExample">
																<div class="card-body">
																		<p><span class="img_bg"><img src="{{ asset('img/address.svg') }}" class="address"></span> <span>{{ $invitation->user->business_address }}</span></p>
																	<p style="margin: 0px 0px 5px;">Website Url: <span>{{$invitation->user->lawyer->website_url }}</span></p>
																</div>
																</div>
															</div>
														</div>	
														<!---end--->
														
														<?php  }  ?>
														@else
														<h4 style="text-align:center;margin-left: 15px;">No Records Found</h4>
														@endif
													</div>											
												</div>	
											</div>      
										</div>
										
									</div>
								</div>
							</div>
							<div class="tab-pane" id="expired-invitations" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										<h3 class="inv-content-header">Expired Invitations</h3>
									</div>
									@if ($expired_invitations)
										@foreach($expired_invitations as $invitation)									
									<!---start--->
									<div class="col-md-12">
										<div class="card patient_tab box_div new_padding">
											<div class="card-head" id="heading{{ $invitation->unique_id }}">
											  <h2 class="mb-0  docter_list" >
												   <div class="main1">

													<!-- Updated on 26-06-2021 -->

														<div class="row">
															<div class="col-md-2">
																<div class="sub">
																	<?php          
																	if(($invitation->user->profilepic)!=null){ ?>
																		  <img src="{{ url('uploads/profilepics/'.$invitation->user->profilepic) }}" class="profile">
																	<?php
																	}else {
																	?> <img src="{{ asset('img/user.jpg') }}" class="profile">
																	<?php  } ?>
																</div>
															</div>
															<div class="col-md-10">
																<div class="row mt-2">
																	<div class="col-md-7">
																		 <span class="name"><?php echo ucfirst($invitation->user->name); ?> &nbsp;<?php if($invitation->user->lawyer->lawfirmname!=null){ ?> ( <?php echo ucfirst($invitation->user->lawyer->lawfirmname);?>)<?php }?></span>  
																	</div>
																	<div class="col-md-5">											   
																			<a  class="nav-link addpatient_btn" style="background:#e0ac12;border-radius:0px;">Expired</a>
																	</div>
																</div>
																<div class="row">
																	<div class="col-md-5">
																		<span class="calendar_p virtualbold">
																			<span style="color: #00396b;font-weight: 600;margin-bottom: 5px;">Appointment Date</span><img src="{{ url('img/calendar.png') }}" class="calendar_icon"><?php echo date('d F Y', strtotime($invitation->invitation_date )); ?> |  {{ $invitation->timeslot }} 
																			<?php  if($invitation->appointment_type=='virtual') { ?>
																			<b> - Virtual</b>
																			<?php  } ?>
																			</span>
																		</div>
																		<div class="col-md-7">
																			<span class="calendar_p" style="margin-top: 0%;">
																		<span style="color: red;font-weight: 600;margin-bottom: 5px;">Report Due Date:</span><img src="{{ url('img/calendar.png') }}" class="calendar_icon"><?php echo date('d F Y', strtotime($invitation->duedate )); ?>	
																	 </span> 
																	</div>
																	
																</div>
															</div>
														</div>
														<!-- Updated on 26-06-2021 -->
														
												   </div>
											  </h2>		
											 <a class="mb-0 collapsed collapsed read-more-block" data-toggle="collapse" data-target="#collapse{{ $invitation->unique_id }}" aria-expanded="false" aria-controls="collapse{{ $invitation->unique_id }}">
												 <img src="{{ asset('images/up-chevron.svg') }}" class="arrow-up" width="20"  alt="up">
												<img src="{{ asset('images/down-chevron.svg') }}" class="arrow-down" width="20" alt="down">
											 </a>
											</div>    
											<div id="collapse{{ $invitation->unique_id }}" class="collapse" aria-labelledby="heading{{ $invitation->unique_id }}" data-parent="#accordionExample">
											  <div class="card-body">
													 <p><span class="img_bg"><img src="{{ asset('img/address.svg') }}" class="address"></span> <span>{{ $invitation->user->business_address }}</span></p>
												   <p style="margin: 0px 0px 5px;">Website Url: <span>{{$invitation->user->lawyer->website_url }}</span></p>
											  </div>
											</div>
										</div>
									</div>	
									<!---end--->
									@endforeach
									@else
									<div class="col-md-12">
										<h4 style="text-align:center;">No Expired Invitations found.</h4>
									</div>
									@endif
								</div>
							</div>
							<div class="tab-pane" id="rejected-invitations" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										<h3 class="inv-content-header">Rejected Invitations</h3>
										@if (Session::has('success'))
											<div class="mt-2 alert alert-success">{{ Session::get('success') }}</div>
										@endif
									</div>
									@if ($rejected_invitations)
										@foreach($rejected_invitations as $invitation)				

									<!---start--->
									<div class="col-md-12">
										<div class="card patient_tab box_div new_padding">
											<div class="card-head" id="heading{{ $invitation->unique_id }}">
											  <h2 class="mb-0  docter_list" >
												   <div class="main1">

													<!-- Updated on 26-06-2021 -->

														<div class="row">
															<div class="col-md-2">
																<div class="sub">
																	<?php          
																	if(($invitation->user->profilepic)!=null){ ?>
																		  <img src="{{ url('uploads/profilepics/'.$invitation->user->profilepic) }}" class="profile">
																	<?php
																	}else {
																	?> <img src="{{ asset('img/user.jpg') }}" class="profile">
																	<?php  } ?>
																</div>
															</div>
															<div class="col-md-10">
																<div class="row mt-2">
																	<div class="col-md-7">
																		 <span class="name"><?php echo ucfirst($invitation->user->name); ?> &nbsp;<?php if($invitation->user->lawyer->lawfirmname!=null){ ?> ( <?php echo ucfirst($invitation->user->lawyer->lawfirmname);?>)<?php }?></span>  
																	</div>
																	<div class="col-md-5">											   
																			<a  class="nav-link addpatient_btn"  style="background:#d61f12;border-radius:0px;" >Rejected By {{($invitation->rejected_by == 'patient' ? 'Patient' : 'You')}}</a>
																	</div>
																</div>
																<div class="row">
																		<div class="col-md-5">
																			<span class="calendar_p virtualbold">
																			<span style="color: #00396b;font-weight: 600;margin-bottom: 5px;">Appointment Date</span><img src="{{ url('img/calendar.png') }}" class="calendar_icon"><?php echo date('d F Y', strtotime($invitation->invitation_date )); ?> |  {{ $invitation->timeslot }} 
																			<?php  if($invitation->appointment_type=='virtual') { ?>
																				<b> - Virtual</b>
																			<?php  } ?>
																			</span>
																		</div>
																		<div class="col-md-7">
																			<span class="calendar_p" style="margin-top: 0%;">
																		<span style="color: red;font-weight: 600;margin-bottom: 5px;">Report Due Date:</span><img src="{{ url('img/calendar.png') }}" class="calendar_icon"><?php echo date('d F Y', strtotime($invitation->duedate )); ?>	
																	 </span> 
																	</div>
																	
																</div>
															</div>
														</div>
														<!-- Updated on 26-06-2021 -->
														
												   </div>
											  </h2>		
											 <a class="mb-0 collapsed collapsed read-more-block" data-toggle="collapse" data-target="#collapse{{ $invitation->unique_id }}" aria-expanded="false" aria-controls="collapse{{ $invitation->unique_id }}">
												 <img src="{{ asset('images/up-chevron.svg') }}" class="arrow-up" width="20"  alt="up">
												<img src="{{ asset('images/down-chevron.svg') }}" class="arrow-down" width="20" alt="down">
											 </a>
											</div>    
											<div id="collapse{{ $invitation->unique_id }}" class="collapse" aria-labelledby="heading{{ $invitation->unique_id }}" data-parent="#accordionExample">
											  <div class="card-body">
													 <p><span class="img_bg"><img src="{{ asset('img/address.svg') }}" class="address"></span> <span>{{ $invitation->user->business_address }}</span></p>
												   <p style="margin: 0px 0px 5px;">Website Url: <span>{{$invitation->user->lawyer->website_url }}</span></p>
											  </div>
											</div>
										</div>
									</div>	
									<!---end--->
									@endforeach
									@else
									<div class="col-md-12">
										<h4 style="text-align:center;">No Rejected Invitations found.</h4>
									</div>
									@endif
								</div>
							</div>
							<div class="tab-pane" id="cancelled-invitations" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										<h3 class="inv-content-header">Cancelled Invitations</h3>
									</div>
									@if ($cancelled_invitations)
										@foreach($cancelled_invitations as $invitation)				

									<!---start--->
									<div class="col-md-12">
										<div class="card patient_tab box_div new_padding">
											<div class="card-head" id="heading{{ $invitation->unique_id }}">
											  <h2 class="mb-0  docter_list" >
												   <div class="main1">

													<!-- Updated on 26-06-2021 -->

														<div class="row">
															<div class="col-md-2">
																<div class="sub">
																	<?php          
																	if(($invitation->user->profilepic)!=null){ ?>
																		  <img src="{{ url('uploads/profilepics/'.$invitation->user->profilepic) }}" class="profile">
																	<?php
																	}else {
																	?> <img src="{{ asset('img/user.jpg') }}" class="profile">
																	<?php  } ?>
																</div>
															</div>
															<div class="col-md-10">
																<div class="row mt-2">
																	<div class="col-md-7">
																		 <span class="name"><?php echo ucfirst($invitation->user->name); ?> &nbsp;<?php if($invitation->user->lawyer->lawfirmname!=null){ ?> ( <?php echo ucfirst($invitation->user->lawyer->lawfirmname);?>)<?php }?></span>  
																	</div>
																	<div class="col-md-5">											   
																			<a  class="nav-link addpatient_btn"  style="background:#d61f12;border-radius:0px;" >Cancelled By Lawyer</a>
																	</div>
																</div>
																<div class="row">
																		<div class="col-md-5">
																			<span class="calendar_p virtualbold">
																			<span style="color: #00396b;font-weight: 600;margin-bottom: 5px;">Appointment Date</span><img src="{{ url('img/calendar.png') }}" class="calendar_icon"><?php echo date('d F Y', strtotime($invitation->invitation_date )); ?> |  {{ $invitation->timeslot }} 
																			<?php  if($invitation->appointment_type=='virtual') { ?>
																				<b> - Virtual</b>
																			<?php  } ?>
																			</span>
																		</div>
																		<div class="col-md-7">
																			<span class="calendar_p" style="margin-top: 0%;">
																		<span style="color: red;font-weight: 600;margin-bottom: 5px;">Report Due Date:</span><img src="{{ url('img/calendar.png') }}" class="calendar_icon"><?php echo date('d F Y', strtotime($invitation->duedate )); ?>	
																	 </span> 
																	</div>
																	
																</div>
															</div>
														</div>
														<!-- Updated on 26-06-2021 -->
														
												   </div>
											  </h2>		
											 <a class="mb-0 collapsed collapsed read-more-block" data-toggle="collapse" data-target="#collapse{{ $invitation->unique_id }}" aria-expanded="false" aria-controls="collapse{{ $invitation->unique_id }}">
												 <img src="{{ asset('images/up-chevron.svg') }}" class="arrow-up" width="20"  alt="up">
												<img src="{{ asset('images/down-chevron.svg') }}" class="arrow-down" width="20" alt="down">
											 </a>
											</div>    
											<div id="collapse{{ $invitation->unique_id }}" class="collapse" aria-labelledby="heading{{ $invitation->unique_id }}" data-parent="#accordionExample">
											  <div class="card-body">
													 <p><span class="img_bg"><img src="{{ asset('img/address.svg') }}" class="address"></span> <span>{{ $invitation->user->business_address }}</span></p>
												   <p style="margin: 0px 0px 5px;">Website Url: <span>{{$invitation->user->lawyer->website_url }}</span></p>
											  </div>
											</div>
										</div>
									</div>	
									<!---end--->
									@endforeach
									@else
									<div class="col-md-12">
										<h4 style="text-align:center;">No Cancelled Invitations found.</h4>
									</div>
									@endif
								</div>
							</div>
							</div>
							</div>	
						</div>	
					</div>
					<!---end new design---->
					
					<!---start---->
					<div class="modal fade" id="scss_acc_pt" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
					
					  <div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content login_box">
						  <div class="modal-body">		
						  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  <div class="row">
								<div class="col-md-3 text-center m-auto">
									<img id="modal-inv-succes-img" src="{{ asset('img/checkmark.svg') }}" class="w-100" alt="success" />				
								</div>
						  </div>
						  <h5 class="modal-title"  style="display:none;color:green;text-align: center;" id="acceptscss">Invitation Accepted</h5>
							<h5 class="modal-title"  style="display:none;color:red;text-align: center;" id="rjtscss">Invitation Rejected</h5>
							
						  </div>
						</div>
					  </div>
					</div>
					<!----end----->
                    </div>					
					<!---   </div>-->			
					

					<div class="tab-pane fade @if($activeTab == 'update-calendar') show active @endif"  id="update-calendar" role="tabpanel" aria-labelledby="update-calendar-tab">					
                    <div class="row">
                        <div class="col-md-12">
						@if(Session::has('message'))
							<div class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</div>
						@endif	
                            <div class="search_sec">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
					 
				<div class="modal fade" id="addevent" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
				
					<?php
							$newList1=json_encode($newList);  
					
					?>
				  <div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content login_box">
					  <div class="modal-header">
						<h5 class="modal-title" id="loginTitle">Add Event</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
							  <form id="addevent_form"><!-------method="POST" action="{{ route('login') }}--------->
									@csrf
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<input type="hidden" id="selecteddate" name="selecteddate">
												<input type="text" class="form-control" id="notes" name="notes" aria-describedby="notes" placeholder="Enter Notes">
											</div>
										</div>
										<div class="col-md-12">
										   <p id="successtask" style="color:green;display:none;">Event Added Successfully</p>
										</div>
									</div>
									<button type="submit" class="btn btn-primary submit_btn"  id="addevent_btn">Submit</button>
								</form>
					  </div>
					</div>
				  </div>
				</div>
				<!---delete event---->
				<div class="modal fade" id="deleteevent" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
				
					<?php
							$newList1=json_encode($newList);
					
					?>
				  <div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content login_box">
					  <div class="modal-header" >
						<h5 class="modal-title" id="loginTitle" >Are Sure Want to Delete ?</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
							  <form id="deleteevent_form"><!-------method="POST" action="{{ route('login') }}--------->
									@csrf
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<input type="hidden" id="rowid1" name="rowid1">
												<input type="hidden" class="form-control" id="rownotes1" name="rownotes1" aria-describedby="rownotes" placeholder="Enter Edited Notes" disabled>
											</div>
										</div>
										<div class="col-md-12">
										   <p id="successtask1" style="color:green;display:none;">Event Deleted Successfully</p>
										</div>
									</div>
									<button type="submit" class="btn btn-primary submit_btn"  id="addevent_btn1">Yes</button>
									<button type="submit" class="btn btn-primary submit_btn"  id="deleteevent_btn1" data-dismiss="modal" aria-label="Close">No</button>
								</form>
					  </div>
					</div>
				  </div>
				</div>
				
				<!------edit------->
				
				 
				<div class="modal fade" id="editevent" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
				
					<?php
							$newList1=json_encode($newList);
					
					?>
				  <div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content login_box">
					  <div class="modal-header">
						<h5 class="modal-title" id="loginTitle">Edit Event</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
							<form id="editevent_form"><!-------method="POST" action="{{ route('login') }}--------->
								@csrf
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<input type="hidden" id="rowid" name="rowid">
											<input type="text" class="form-control" id="rownotes" name="rownotes" aria-describedby="rownotes" placeholder="Enter Edited Notes">
										</div>
									</div>
									<div class="col-md-12">
										<p id="successedit" style="color:green;display:none;">Event Updated Successfully</p>
									</div>
								</div>
								<button type="submit" class="btn btn-primary submit_btn"  id="editevent_btn">Submit</button>
							</form>
					  </div>
					</div>
				  </div>
				</div>
                </div>
               
				<!----upload docs---->
				 <!-- Available Calendar Model -->
				<div class="modal fade" id="Availablecalendar" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content login_box">
					  <div class="modal-header">
						<h5 class="modal-title" id="calendarTitle">Please upload patients documents</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
						<form method="post" action="#" id="#">
							<div class="form-group files">
							  <input type="file" class="form-control" multiple="">
							</div>
						</form>
						  <a href="#" class="invite_btn">Submit</a>
					  </div>
					</div>
				  </div>
				</div>
				<!----end  upload docs---->

                <div class="tab-pane fade @if($activeTab == 'invoices') show active @endif" id="invoices" role="tabpanel" 			aria-labelledby="invoices-tab">
					
						<div class="col-md-12">
                            <div class="patients_list_main">
								<?php if($invoices->isEmpty()){  ?>
								<h3 style="text-align:center;">No Records Found</h3>
								<?php  }else{  ?>
                                <div class="row">
									<div class="col-md-12">
											<h4 style="float:right;">Total Invoice Amount:<b style="font-weight: 600;"> ${{$suminvoices}}</b></h4>
									</div>	
									<?php //$number =1; ?>
                                    <?php										
										foreach($invoices as $invoices1) {                                        
                                        	//print_r($invoices1);                                        
                                        ?>
                                    <div class="col-md-12">
											<?php  if($invoices1->invitations->status!='cancelled') { ?>
                                            <div class="single_patient">
                                                <ul> 
                                                    <li>
													<span style="color: #00528c;">Invoice Date:</span>  <?php echo date('d F Y', strtotime($invoices1->invoice_date )); ?>
													<span style="margin-left: 13px;"><span style="color: #00528c;">Patient: </span><?php  echo ucfirst(strtolower($invoices1->invitations->patient->name)); ?></span>
													<span class="patientfile">
														<a href="javascript:void(0)" onClick="reply_click_doctorinvoice(id)" id="{{ $invoices1->invitations->id }}"  title="View Invoice"><i class="fa fa-info-circle" ></i></a>
														<!--<a href="" title="Appointments"><i class="fa fa-calendar" ></i></a>-->
														<a title="Download Invoice" href="{{url('/uploads/custom_invoice')}}/{{$invoices1->invoice_filename}}" download="invoice_{{$invoices1->invoice_filename}}" target="_blank"><i class="fa fa-download"></i></span></li></a> 

														
													</span>
													</li>
														
                                                </ul>
                                            </div>	
											<?php } ?>	
                                        <!--</a>-->
                                    </div>
										<?php  }  ?>                     
									
                                </div>
								{{$invoices->links("pagination::bootstrap-4")}}
								
								<?php  } ?>
								
                            </div>
							
                        </div>										
									
                </div>
				<!---start invoice modal view---->
				<div class="modal fade" id="invoicemodal" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 960px;">
						<div class="modal-content login_box">		
										@include('pages.doctor.doctorinvoicemodalview') 
						</div>
					</div>
				</div>
				<!---end invoice modal view---->
            </div>
        </div>
    </section>


		<div class="modal" id="block-calendar-modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
	<form action="{{ url('doctor/block-dates') }}" method="post" id="block-date-calendar-form">
      <div class="modal-header">
        <h5 class="modal-title">Block/Un Block Dates</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <div id="mdp-demo"></div>
	  @csrf
	  <input type="hidden" name="dates" id="blocked_dates" />
	  <input type="hidden" name="selected_date" id="selected_date" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary">Block Dates</button>
      </div>
	  </form>
    </div>
  </div>
</div>
		<!----end----->
    <!-- Lawyer Dashboard Section -->
@endsection

@section('footerScripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
function addCanderNote(inp,inst){
	console.log(inp)
	console.log(inst)
	console.log($('.ui-datepicker-div'));
	$(`
	<div class="calender-note">
	<p><span class="yellow"></span> - Booked</p><p><span class="red"></span> - Report Due date</p>	
	</div>`).insertAfter('.ui-datepicker-calendar')
}

	$(document).ready(function() {

		var dueDateArr = <?=$dueDateArr?>;
		var invitationDateArr = <?=$invitationDateArr?>;

	var dueDates = {};
	var invitationDates = {};

		if(dueDateArr.length){
			for(var i =0; i<dueDateArr.length; i++){
				dueDates[new Date(dueDateArr[i])] = new Date(dueDateArr[i]).toString();
			}
		}
		
		if(invitationDateArr.length){
			for(var j =0; j<invitationDateArr.length; j++){
				invitationDates[new Date(invitationDateArr[j])] = new Date(invitationDateArr[j]).toString();
			}
		}

$('.my-calendar-input').datepicker({
  showOn: 'button',
  buttonText:'view calendar',
  options: function(dateText, inst) { inst.show() },
  beforeShow:function(inp, inst){
	setTimeout(function () {
		addCanderNote(inp,inst);
	},500)
  },
  beforeShowDay: function(date) {
            var dueDate = dueDates[date];
            var inviteDate = invitationDates[date];
            if (dueDate) {
                return [true, "highlight-due-date", dueDate];
            }else if(inviteDate){
                return [true, "highlight-invite-date", inviteDate];
			}
            else {
                return [true, '', ''];
            }
        }
		
});

	/*	$('#dashboard-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var target = $(e.target).attr("href")
			var activeTab = target.replace('#','');
    		var baseURL = '{{url("doctor-dashboard") }}';  
			window.history.pushState({}, null, baseURL+'/'+activeTab);
		});
		
		$('.invitation-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var activeTab = $(e.target).attr("href")
		var baseURL = '{{url("doctor-dashboard/my-invitations") }}'; 
			window.history.pushState({}, null, baseURL+'/'+activeTab);
		});


		var hash = window.location.hash;
		if(hash.length > 0) {
			$('.invitation-tabs .nav-item .nav-link').removeClass('active');
			$('.invitation-tabs #invitations-tab-content .tab-pane').removeClass('active');
			$('.invitation-tabs .nav-item .nav-link[href="'+hash+'"]').addClass('active');
			$('.invitation-tabs #invitations-tab-content '+hash).addClass('active');
		}*/
		
});

$(document).ready(function() {    
    setInterval(function(){ 	
		$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
				$.ajax({
				url: "{{url('doctor_new_invitations_check')}}",
				type: "GET",
				dataType:'json',
				success: function( response ) {
                        if(response.status=='success'){
                            alert('You have got New Invitattion');
                            window.location.reload();
                        }
				} 
				}); 
        }, 60000);//300000
            
});
</script>

@endsection