@extends('layouts.admin_app')



@section('content')

<style>
	.addpatientdata p{
		color:#000000;
	}
	.addpatientdata b{
		font-weight:500;
	}
</style>


<div class="container-fluid">

    <div class="row">

	    <div class="col-md-12">

			 <nav aria-label="breadcrumb">

			  <ol class="breadcrumb">

				<li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>

				<li class="breadcrumb-item active"><a href="{{url('admin/Lawyer_transportation_translation')}}">Requests</a></li>

			  </ol>

			</nav>

		</div>

		<div class="col-md-12">  

			<!----search-------------->

				<form method="GET" action="{{ route('admin/Lawyer_transportation_translation') }}" >

					<div class="form-group row">	    

						

						<div class="col-md-5">

						   

							<input id="keyword" type="text" class="form-control @error('keyword') is-invalid @enderror" name="keyword" id="keyword" placeholder="Search Name" value="<?php if(isset($_GET['keyword'])){echo $_GET['keyword'];}?>"  autocomplete="keyword" autofocus>



							@error('keyword')

								<span class="invalid-feedback" role="alert">

									<strong>{{ $message }}</strong>

								</span>

							@enderror

						</div>          



						<div class="col-md-3">

							<button type="submit" class="btn btn-primary">

								{{ __('Search') }}

							</button>

							<?php  if(isset($_GET['keyword'])){  ?>

							<a href="{{url('admin/Lawyer_transportation_translation')}}" style="margin-left: 10px;color: #00528c;">

								Clear

							</a>

							<?php }  ?>

						</div>

					</div>

				</form>

		</div>

	</div>

	   <div class="row justify-content-center">

         <div class="container-fluid">

			<div class="card">

				 <div class="card-header">

					Requests

				  </div>

		  <div class="card-body">

				<div class="row justify-content-center">

					<div class="col-md-12">                   

						<table   id="myTable" class="table table-bordered table-striped" style="width:100%">

							<tr>

							<td>S.No</td>
							
							 <td>Lawyer Name</td>
							 
							 <td>Patient Name</td>

							 <td>Doctor Name</td>

							<td>Appointment date and time slot</td>  
							<td>Request Detail</td>  

							

							</tr>

							 <?php //$number = 1; ?>

							@if($userint->isNotEmpty())

							@foreach ($userint as $userint1)

							<tr>						

							<td>{{ $number }}</td>

							 <?php $number++; ?>
							 
							  <td>{{ ucfirst($userint1->user->name)  }}</td>
							  
							  <td>		
										<a href="javascript:void(0)" onClick="reply_click_loe(this.id, '{{$userint1->patient->name}}','{{MyHelper::Decrypt($userint1->patient->email)}}','{{MyHelper::Decrypt($userint1->patient->address)}}','<?php echo date(' d M Y',strtotime($userint1->patient->dateofbirth)); ?>','<?php echo date(' d M Y',strtotime($userint1->patient->dateofaccident)); ?>','{{MyHelper::Decrypt($userint1->patient->casetype)}}')" id="{{ $userint1->id }}" style="color: #00528c;font-weight: 500;" >{{ ucfirst($userint1->patient->name)  }}</a>												

							   </td>

							 <td>{{ ucfirst($userint1->user1->name) }}</td>							

							<td><?php echo date('d F Y', strtotime($userint1->invitation_date)); ?>&nbsp;&nbsp;&nbsp;{{$userint1->timeslot}}</td>	
							
												
							<td>@if($userint1->transportation =='yes')  Transportation<br> @endif
							@if($userint1->translation =='yes')  Translation <br> @endif
							@if($userint1->amendment =='yes') Amendment <br> @endif</td>						

							</tr>

							 @endforeach

							@else 

								<div>

									<h2>No Records found</h2>

								</div>

							@endif

						</table> 

					</div>

				</div>

			</div>

			</div>

			  {{$userint->links("pagination::bootstrap-4")}}

        </div>

    </div> 

	<!-- Add Patient Model -->
	<div class="modal fade" id="addPatient" tabindex="1" role="dialog" aria-labelledby="addPatientTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content login_box">
			<div class="modal-header">
			<h5 class="modal-title" id="addPatientTitle">Patient Information</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			</div>
			<div class="modal-body">
				<div class="row  addpatientdata">
					<div class="col-md-12">
						<div class="form-group">
							<p><b>Name:</b> <span id="newname"></span></p>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
						<p><b>Email:</b> <span id="newemail"></span></p>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
						<p><b>Address:</b> <span id="newaddress"></span></p>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
						<p><b>Date of Birth:</b> <span id="newdateofbirth"></span></p>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
						<p><b>Date of Accident:</b> <span id="newdateofaccident"></span></p>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
						<p><b>Case Type:</b> <span id="newcasetype"></span></p>
						</div>
					</div>

				</div>
			</div>
		</div>
		</div>
	</div>
<!-- End Patient Model -->


</div>
<script>
	 function reply_click_loe(clicked_id,name,email,address,dateofbirth,dateofaccident,casetype)
  {
	  var name1=name;	 
	  var email=email; 
	  var address=address;	  
	  var dateofbirth=dateofbirth; 
	  var dateofaccident=dateofaccident;	  
	  var casetype=casetype; 

	  $('#newname').html(name1);
	  $('#newemail').html(email);
	  $('#newaddress').html(address);
	  $('#newdateofbirth').html(dateofbirth);
	  $('#newdateofaccident').html(dateofaccident);
	  $('#newcasetype').html(casetype);

	  $('#addPatient').modal('show');
  }
</script>
@endsection

