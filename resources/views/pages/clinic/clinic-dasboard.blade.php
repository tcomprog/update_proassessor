@extends('layouts.clinic-layout') 
@section('content')	
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="{{ asset('css/doctor_dashboard.css') }}" rel="stylesheet">
<link href="{{ asset('css/clinic_dashboard.css') }}" rel="stylesheet">

<style>
	.ui-datepicker-trigger{
		background: transparent !important;
	}
	.n_links{
		font-weight: bold;
		color:#00396b;
	}
	.n_links:hover{
		text-decoration: none;
	}
    .dashboard_sec .nav-tabs .nav-link.active {
        color: #141a45;
        background-color: #ffffff;
        border-color: #ffffff #ffffff #526082;
    }
    .doctor_activit_tab_icon{
        vertical-align: bottom;
    }

.invitation-tabs .nav-tabs .nav-item .nav-link {
    padding: 0.6rem 1rem;
}

.nav-item > button {
    font-weight: 600;
    font-size: 18px;
    background: white;
    border-bottom: 1px solid #dee2e6 !important;
}

.nav-tabs .nav-link.active {
    border-bottom: none !important;
}
</style>

<?php 
   $routeName = \Request::route()->getName(); 
   $archivedinvitations = Request::segment(2);
?>

<div class="container mt-4">


    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
          <button class="nav-link active" id="home-tab" data-toggle="tab" data-target="#assessmentTab" type="button" role="tab" aria-controls="home" aria-selected="true">Assessments</button>
        </li>
        <li class="nav-item" role="presentation">
          <button class="nav-link" id="profile-tab" data-toggle="tab" data-target="#invoiceTab" type="button" role="tab" aria-controls="profile" aria-selected="false">Invoices</button>
        </li>
        <li class="nav-item" role="presentation">
          <button class="nav-link" id="profile-tab" data-toggle="tab" data-target="#doctorTab" type="button" role="tab" aria-controls="profile" aria-selected="false">Doctors</button>
        </li>
    </ul>

    <div class="tab-content" id="myTabContent">

        <div class="tab-pane fade show active" id="assessmentTab" role="tabpanel" aria-labelledby="Assessment Tab">
            {{-- Assessment Section Start --}}
        <div class="row1 mb-5" id="my-invitations">
            <div class="accordion patient_details_box1" id="accordionExample">
            <div class="invitation-tabs">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link ns1  @if( $routeName != 'search-confirmed-patient' && $archivedinvitations != 'archived-invitations')  active @endif tbss" style="border-radius: 100px !important; margin-top:5px;" data-toggle="tab" href="#new-invitations" role="tab" aria-controls="new-invitations">
                    <span class="material-icons doctor_activit_tab_icon">add</span>New
                    <?php // Patients ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ns1" data-toggle="tab" href="#accepted-invitations" role="tab" style="border-radius: 100px !important; margin-top:5px;" aria-controls="accepted-invitations tbss">
                        <span class="material-icons doctor_activit_tab_icon">check</span>
                        Accepted<?php // Patients ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ns1 @if( $routeName == 'search-confirmed-patient' && $archivedinvitations=='archived-invitations')  active @endif " style="border-radius: 100px !important; margin-top:5px;" data-toggle="tab" href="#confirmed-invitations" role="tab" aria-controls="confirmed-invitations">
                        <span class="material-icons doctor_activit_tab_icon">done_all</span>Confirmed<?php // Patients ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ns1 @if( $routeName == 'completed-invitations' && $archivedinvitations=='completed-invitations')  active @endif " style="border-radius: 100px !important; margin-top:5px;" data-toggle="tab" href="#completed-invitations" role="tab" aria-controls="completed-invitations">
                        <span class="material-icons doctor_activit_tab_icon">offline_pin</span>Completed<?php // Patients ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ns1" data-toggle="tab" href="#expired-invitations" style="border-radius: 100px !important; margin-top:5px;" role="tab" aria-controls="expired-invitations">
                        <span class="material-icons doctor_activit_tab_icon">schedule</span>Expired</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ns1" data-toggle="tab" href="#rejected-invitations" style="border-radius: 100px !important; margin-top:5px;" role="tab" aria-controls="expired-invitations">
                        <span class="material-icons doctor_activit_tab_icon">info</span>Rejected</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ns1" data-toggle="tab" href="#cancelled-invitations" style="border-radius: 100px !important; margin-top:5px;" role="tab" aria-controls="expired-invitations">
                        <span class="material-icons doctor_activit_tab_icon">close</span>Cancelled</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link ns1" data-toggle="tab" href="#archived-invitations" style="border-radius: 100px !important; margin-top:5px;" role="tab" aria-controls="expired-invitations">
                        <span class="material-icons doctor_activit_tab_icon">close</span>Archived</a>
                </li>

                </ul>
                <div class="tab-content" id="invitations-tab-content">
                    <div class="tab-pane @if( $routeName != 'search-confirmed-patient' && $archivedinvitations != 'archived-invitations')  active @endif" id="new-invitations" role="tabpanel">
                        <div class="row" >


                    @if ($new_invitations)
                        @foreach($new_invitations as $invitation)
                    <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
                        <div class="design_card_new">
                           <div class="d-flex">
                                @if (($invitation->user->profilepic)!=null)
                                     <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
                                 @else
                                    <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
                                @endif
                                <div class='n_c_container'>
                                    <h5 class="d_name_text">{{ucfirst($invitation->user->name)}}</h5>
                                    <p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
                                </div>
                           </div>
                           <table id="n_desing_tbl">

                            @php
                                $doctor = $doctorList[$invitation->invitee_user_id];
                            @endphp

                            <tr>
                                <th>Doctor:</th>
                                <th>{{$doctor->name}}</th>
                              </tr>
                            <tr>

                              <tr>
                                <th>Category:</th>
                                <td>{{ $invitation->category->category_name }}</td>
                              </tr>
                             
                              <tr>
                                <th>Date:</th>
                                <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
                              </tr>
                              <tr>

                                <th>Due date:</th>
                                <td>{{date('d F Y', strtotime($invitation->duedate ))}}
                                </td>
                              </tr>

                              <tr>
                                <th>Type:</th>
                                <td>{{ucfirst($invitation->appointment_type)}}</td>
                              </tr>
                           </table>

                           <div class="button_containter">

                            <label for="view-cal-inp{{ $invitation->id }}">
                                <input type="hidden" value="{{ date('m/d/Y',strtotime($invitation->invitation_date)) }}"  id="view-cal-inp{{ $invitation->id }}" style="margin-left:60px;cursor:pointer;" class="calendar_icon my-calendar-input" data-invite-date="{{ date('m/d/Y',strtotime($invitation->invitation_date)) }}">
                            </label>

                               <img id="loading-gif-{{$invitation->id}}" style="display: none" width="35" height="35" src="{{asset("img/loading.gif")}}" alt="">

                               <div class="accept-invitation-btn rountedBtn" id="cr-btn-accept-{{$invitation->id}}" data-id="{{ $invitation->id }}"  title="Accept">
                                   <span class="material-icons" style="color: white; font-size:30px;">check</span>
                               </div>

                               <div class="reject-inv-btn rountedBtn"  id="cr-btn-reject-{{$invitation->id}}" style="background:#FA1D2F !important" title="Reject" data-id="{{ $invitation->id }}">
                                   <span class="material-icons" style="color: white; font-size:30px;">close</span>
                               </div>

                           </div>

                        </div>
                     
                    </div>	
                    @endforeach
                    @else
                    <div class="col-md-12">
                        <h4 style="text-align:center;margin-top: 15px;">No new assessment(s) received.</h4>
                    </div>
                    @endif
                </div>
            </div>
            <div class="tab-pane" id="accepted-invitations" role="tabpanel">
                <div class="row">
                    <div class="col-md-12">
                        {{-- <h3 class="inv-content-header">Accepted Assessments</h3> --}}
                        @if (Session::has('success'))
                            <div class="mt-2 alert alert-success">{{ Session::get('success') }}</div>
                        @endif
                    </div>
                    @if ($accepted_invitations)
                        @foreach($accepted_invitations as $invitation)								
                    <!---start--->
                    <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
                        <div class="design_card_new">
                           <div class="d-flex">
                                @if (($invitation->user->profilepic)!=null)
                                     <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
                                 @else
                                    <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
                                @endif
                                <div class='n_c_container'>
                                    <h5 class="d_name_text">{{ucfirst($invitation->user->name)}}</h5>
                                    <p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
                                </div>
                           </div>
                           <table id="n_desing_tbl">

                            @php
                                $doctor = $doctorList[$invitation->invitee_user_id];
                            @endphp

                            <tr>
                                <th>Doctor:</th>
                                <th>{{$doctor->name}}</th>
                              </tr>
                            <tr>

                              <tr>
                                <th>Category:</th>
                                <td>{{ $invitation->category->category_name }}</td>
                              </tr>
                             
                              <tr>
                                <th>Date:</th>
                                <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
                              </tr>
                              <tr>
                                <th>Due date:</th>
                                <td>{{date('d F Y', strtotime($invitation->duedate ))}}
                                </td>
                              </tr>

                              <tr>
                                <th>Type:</th>
                                <td>{{ucfirst($invitation->appointment_type)}}</td>
                              </tr>
                           </table>

                        </div>
                    </div>	
                    <!---end--->
                    @endforeach
                    @else
                    <div class="col-md-12">
                        <h4 style="text-align:center;">No accepted assessment(s) found.</h4>
                    </div>
                    @endif
                </div>
            </div>


            {{-- ****************** Confirmed section ************** --}}
                <div class="tab-pane @if( $routeName == 'search-confirmed-patient')  active @endif" id="confirmed-invitations" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            {{-- <h3 class="inv-content-header">Confirmed Assessment(s)</h3>									 --}}
                        </div>									
                        <div class="col-md-8">
                            <form class="search_form" method="GET" action="{{ route('search-confirmed-patient') }}">
                                <div class="input-group">
                                    <input class="form-control" id="system-search" name="lawyerkeyword" placeholder="Search Name" value="<?php if(isset($_GET['lawyerkeyword'])){echo $_GET['lawyerkeyword'];}?>">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-default search_btn_new">
                                            <!--<img src="img/search.png">-->
                                            <img src="{{ url('img/search.png') }}">
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </div>
                        <?php  if(isset($_GET['lawyerkeyword'])) { ?>
                            <div class="col-md-1">
                                <a class="nav-link" style="font-size: 19px;" href="{{url('doctor-dashboard')}}/my-invitations/#confirmed-invitations" >clear</a>
                            </div>
                        <?php } ?>
                        <div class="col-md-4"></div>
                        <div class="col-md-4 text-right">
                            <!--<a class="btn addpatient_btn" data-toggle="tab" href="#archived-invitations" role="tab" aria-controls="archived-invitations">Archived Invitations</a>-->
                            {{-- <a  href="{{url('/doctor-dashboard/archived-invitations')}}" class="btn addpatient_btn">Archived</a> --}}
                        </div>
                        <div class="col-md-12">
                            <div class="search_sec">
                                <div class="row1">
                                    <div class="accordion" id="accordionExample">
                                        <div class="row">
                                        @if ($confirmed_invitations)
                                            @if (count($confirmed_invitations) > 0)
                                            <div class="mb-2">
                                                <label class="label"> 
                                                    <input id="choooseAllConfirm" type="checkbox" name="checkbox" value="text"> Select All
                                                </label>
                                            </div>
                                            @endif
                                             
                                             <form method="post" action="{{url('/doctor_confirmed_archive')}}" style="width: 100%">
                                            @csrf
                                            <?php   
                                    foreach($confirmed_invitations as $invitation){   
                                                $current_date=date("Y-m-d"); ?>
                                            
                                            @if($invitation->is_archive=='no')
                                            <!---start--->

                                            <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
                                                <div class="design_card_new">
                                                   <div class="d-flex">
                                                        @if (($invitation->user->profilepic)!=null)
                                                        <a class="n_links" href="{{ url('/invitedetail/'.$invitation->unique_id) }}"> <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" /></a>
                                                         @else
                                                            <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
                                                        @endif
                                                        <div class='n_c_container'>
                                                            <h5 class="d_name_text"> <a class="n_links" href="{{ url('/invitedetail/'.$invitation->unique_id) }}">{{ucfirst($invitation->user->name)}}</a>
                                                                {{-- <a href="{{url('invitedetail/'.$invitation->unique_id.'/chatroom')}}" title="Chat" class="position-relative1"  style="position: relative !important;"><img src="{{ url('img/messenger.png') }}" class="chat_icon" style="width:28px !important">
                                                                    @if(Helper::un_read_messages(Auth::id(),$invitation->id))	
                                                                        <label class="badge badge-warning notification_badge" style="left:inherit">{{ Helper::un_read_messages(Auth::id(),$invitation->id) }}</label>
                                                                    @endif
                                                                </a> --}}
                                                            </h5>
                                                            <p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
                                                        </div>
                                                   </div>
                                                   <table id="n_desing_tbl">

                                                    <tr>
                                                        <th>Patient:</th>
                                                        <td>{{ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname)}}</td>
                                                      </tr>
                                                      <tr>

                                                    @php
                                                        $doctor = $doctorList[$invitation->invitee_user_id];
                                                    @endphp
                        
                                                    <tr>
                                                        <th>Doctor:</th>
                                                        <th>{{$doctor->name}}</th>
                                                      </tr>
                                                    <tr>
                        
                                                      <tr>
                                                        <th>Category:</th>
                                                        <td>{{ $invitation->category->category_name }}</td>
                                                      </tr>
                                                     
                                                      <tr>
                                                        <th>Date:</th>
                                                        <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
                                                      </tr>
                                                      <tr>
                                                        <th>Due date:</th>
                                                        <td>{{date('d F Y', strtotime($invitation->duedate ))}}
                                                        </td>
                                                      </tr>
            
                                                      <tr>
                                                        <th>Type:</th>
                                                        <td>
                                                            @if ($invitation->appointment_type == "inperson")
                                                            {{"In-Person"}}
                                                            @else
                                                               {{ucfirst($invitation->appointment_type)}}
                                                            @endif
                                                        
                                                      </tr>
                                                   </table>
            
                                                   <div class="button_containter">
                                                    @if($invitation->duedate < $current_date)
                                                            <input type="checkbox"  name="archive[]" value="{{$invitation->id}}" class="check_box confirmedTaskCheckbox">
                                                        @endif 

                                                       <div class="rountedBtn" style="background:#546E7A !important; width:30px !important; height:30px !important;" data-id="{{ $invitation->id }}"  title="View more">
                                                           <a href="{{ url('/invitedetail/'.$invitation->unique_id) }}" style="line-height: 0;">
                                                            <span class="material-icons" style="color: white; font-size:21px;">person</span>

                                                        </a>
                                                       </div>
            
                                                   </div>
            
                                                </div>
                                            </div>

                                            
                                            <!---end //abss --->
                                            @endif
                                            <?php  }  ?>
                                            @if (count($confirmed_invitations) > 0)
                                                 <div class="text-right">
                                                    <button type="submit" class="btn addpatient_btn" style="position:relative;margin-right:15px;">Archive</button>
                                                </div>
                                                @endif
                                           </form>
                                            @else
                                            <h4 style="text-align:center;margin-left: 15px;">No Records Found</h4>
                                            @endif
                                        </div>											
                                    </div>	
                                </div>      
                            </div>
                            
                        </div>
                    </div>
                </div>


                {{-- #################### Completed task start #################### --}}
                <div class="tab-pane @if( $routeName == 'completed-invitations')  active @endif" id="completed-invitations" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            {{-- <h3 class="inv-content-header">Completed Assessment(s)</h3>									 --}}
                        </div>									

                        
                        <div class="col-md-12">
                            <div class="search_sec">
                                <div class="row1">
                                    <div class="accordion" id="accordionExample">
                                        <div class="row">
                                        @if ($completedAr)
                                             <form style="width:100%" method="post" action="{{url('/doctor_confirmed_archive')}}">
                                            @csrf
                                            <?php   

                                          foreach($completedAr as $invitation){   
                                                $current_date=date("Y-m-d"); ?>
                                            
                                            @if($invitation->is_archive=='no')
                                            <!---start--->

                                            <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
                                                <div class="design_card_new">
                                                   <div class="d-flex">
                                                        @if (($invitation->user->profilepic)!=null)
                                                             <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
                                                         @else
                                                            <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
                                                        @endif
                                                        <div class='n_c_container'>
                                                            <h5 class="d_name_text">{{ucfirst($invitation->user->name)}}
                                                                
                                                            </h5>
                                                            <p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
                                                        </div>
                                                   </div>
                                                   <table id="n_desing_tbl">

                                                    <tr>
                                                        <th>Patient:</th>
                                                        <td>{{ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname)}}</td>
                                                      </tr>
                                                      <tr>
            
                                                        @php
                                                        $doctor = $doctorList[$invitation->invitee_user_id];
                                                    @endphp
                        
                                                    <tr>
                                                        <th>Doctor:</th>
                                                        <th>{{$doctor->name}}</th>
                                                      </tr>
                                                    <tr>
                        
                                                      <tr>
                                                        <th>Category:</th>
                                                        <td>{{ $invitation->category->category_name }}</td>
                                                      </tr>
                                                     
                                                      <tr>
                                                        <th>Date:</th>
                                                        <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
                                                      </tr>
                                                      <tr>
                                                        <th>Due date:</th>
                                                        <td>{{date('d F Y', strtotime($invitation->duedate ))}}
                                                        </td>
                                                      </tr>
            
                                                      <tr>
                                                        <th>Type:</th>
                                                        <td>{{ucfirst($invitation->appointment_type)}}</td>
                                                      </tr>
                                                   </table>
            
                                                   <div class="button_containter">
            
                                                       <div class="rountedBtn" style="background:#546E7A !important;" data-id="{{ $invitation->id }}"  title="View more">
                                                           <a href="{{ url('/invitedetail/'.$invitation->unique_id) }}" style="line-height: 0;">
                                                            <span class="material-icons" style="color: white; font-size:30px;">person</span>

                                                        </a>
                                                       </div>
            
                                                   </div>
            
                                                </div>
                                            </div>

                                            <!---end--->
                                            @endif
                                            <?php  }  ?>
                                            
                                                    </form>
                                            @else
                                            <h4 style="text-align:center;margin-left: 15px;">No Records Found</h4>
                                            @endif
                                        </div>											
                                    </div>	
                                </div>      
                            </div>
                            
                        </div>
                    </div>
                </div>
                {{-- #################### Completed task edn #################### --}}



                <!--Archive start-->
                <div class="tab-pane @if($archivedinvitations == 'archived-invitations') active @endif" id="archived-invitations" role="tabpanel">
                    <div class="row">
                        <div class="col-md-9">
                            {{-- <h3 class="inv-content-header">Archived Assessment(s)</h3> --}}
                            @if (Session::has('archivedsuccess'))
                            <div class="mt-2 alert alert-success">{{ Session::get('archivedsuccess') }}</div>
                        @endif
                        </div>	
                        <div class="col-md-3">
                            <a class="btn addpatient_btn" data-toggle="tab" href="#confirmed-invitations" role="tab" aria-controls="archived-invitations" style="float:right;">Back</a>
                        </div>
                        <div class="col-md-12">
                            <div class="search_sec">
                                <div class="row1">
                                    <div class="accordion" id="accordionExample">
                                        <div class="row">
                                             @if ($archived_invitations->isNotEmpty())
                                            
                                            <?php   foreach($archived_invitations as $invitation){    ?>
                                            <!---start--->
                                            <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
                                                <div class="design_card_new">
                                                   <div class="d-flex">
                                                        @if (($invitation->user->profilepic)!=null)
                                                             <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
                                                         @else
                                                            <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
                                                        @endif
                                                        <div class='n_c_container'>
                                                            <h5 class="d_name_text">{{ucfirst($invitation->user->name)}}
                                                                <a href="{{url('invitedetail/'.$invitation->unique_id.'/chatroom')}}" title="Chat" class="position-relative1"  style="position: relative !important;"><img src="{{ url('img/messenger.png') }}" class="chat_icon" style="width:28px !important">
                                                                    @if(Helper::un_read_messages(Auth::id(),$invitation->id))	
                                                                        <label class="badge badge-warning notification_badge" style="left:inherit">{{ Helper::un_read_messages(Auth::id(),$invitation->id) }}</label>
                                                                    @endif
                                                                </a>
                                                            </h5>
                                                            <p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
                                                        </div>
                                                   </div>
                                                   <table id="n_desing_tbl">

                                                    <tr>
                                                        <th>Patient:</th>
                                                        <td>{{ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname)}}</td>
                                                      </tr>
                                                      <tr>
            
                                                        @php
                                                        $doctor = $doctorList[$invitation->invitee_user_id];
                                                    @endphp
                        
                                                    <tr>
                                                        <th>Doctor:</th>
                                                        <th>{{$doctor->name}}</th>
                                                      </tr>
                                                    <tr>
                        
                                                      <tr>
                                                        <th>Category:</th>
                                                        <td>{{ $invitation->category->category_name }}</td>
                                                      </tr>
                                                     
                                                      <tr>
                                                        <th>Date:</th>
                                                        <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
                                                      </tr>
                                                      <tr>
                                                        <th>Due date:</th>
                                                        <td>{{date('d F Y', strtotime($invitation->duedate ))}}
                                                        </td>
                                                      </tr>
            
                                                      <tr>
                                                        <th>Type:</th>
                                                        <td>{{ucfirst($invitation->appointment_type)}}</td>
                                                      </tr>
                                                   </table>
            
                                                   <div class="button_containter">
                                         
                                                       <div class="rountedBtn" style="background:#546E7A !important;" data-id="{{ $invitation->id }}"  title="View more">
                                                           <a href="{{ url('/invitedetail/'.$invitation->unique_id) }}" style="line-height: 0;">
                                                            <span class="material-icons" style="color: white; font-size:30px;">person</span>

                                                        </a>
                                                       </div>
            
                                                   </div>
            
                                                </div>
                                            </div>	
                                            <!---end--->
                                            
                                            <?php  }  ?>
                                            @else
                                            <h4 style="text-align:center;margin-left: 15px;">No Records Found</h4>
                                            @endif
                                        </div>											
                                    </div>	
                                </div>      
                            </div>
                            
                        </div>
                    </div>
                </div>
                <!--Archive end-->





                <div class="tab-pane" id="expired-invitations" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            {{-- <h3 class="inv-content-header">Expired Assessment(s)</h3> --}}
                        </div>
                        @if ($expired_invitations)

                        <div class="mb-2">
                            <label class="label"> 
                                <input id="choooseAllTask" type="checkbox" name="checkbox" value="text"> Select All
                            </label>
                        </div>
                        

                        <form method="post" action="{{url('/doctor_confirmed_archive')}}" style="width: 100%">
                            @csrf
                            @foreach($expired_invitations as $invitation)									
                        <!---start--->
                        <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
                            <div class="design_card_new">
                               <div class="d-flex">
                                    @if (($invitation->user->profilepic)!=null)
                                         <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
                                     @else
                                        <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
                                    @endif
                                    <div class='n_c_container'>
                                        <h5 class="d_name_text">{{ucfirst($invitation->user->name)}}</h5>
                                        <p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
                                    </div>
                               </div>
                               <table id="n_desing_tbl">

                                @php
                                $doctor = $doctorList[$invitation->invitee_user_id];
                            @endphp

                            <tr>
                                <th>Doctor:</th>
                                <th>{{$doctor->name}}</th>
                              </tr>
                            <tr>

                              <tr>
                                <th>Category:</th>
                                <td>{{ $invitation->category->category_name }}</td>
                              </tr>
                                 
                                  <tr>
                                    <th>Date:</th>
                                    <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
                                  </tr>
                                  <tr>
                                    <th>Due date:</th>
                                    <td>{{date('d F Y', strtotime($invitation->duedate ))}}
                                    </td>
                                  </tr>

                                  <tr>
                                    <th>Type:</th>
                                    <td>{{ucfirst($invitation->appointment_type)}}</td>
                                  </tr>
                               </table>

                               <div class="button_containter">
                                     <input type="checkbox"  name="archive[]" value="{{$invitation->id}}" class="check_box expiredTaskCheckbox">
                                </div>

                            </div>
                        </div>	
                        <!---end //abss--->
                        @endforeach

                        <div class="text-right">
                            <button type="submit" class="btn addpatient_btn" style="position:relative;margin-right:15px;">Archive</button>
                        </div>
                   </form>

                        @else
                        <div class="col-md-12">
                            <h4 style="text-align:center;">No expired assessment(s) found.</h4>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="tab-pane" id="rejected-invitations" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            {{-- <h3 class="inv-content-header">Rejected Assessment(s)</h3> --}}
                            @if (Session::has('success'))
                                <div class="mt-2 alert alert-success">{{ Session::get('success') }}</div>
                            @endif
                        </div>
                        @if ($rejected_invitations)
                            @foreach($rejected_invitations as $invitation)				

                            <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
                                <div class="design_card_new">
                                   <div class="d-flex">
                                        @if (($invitation->user->profilepic)!=null)
                                             <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
                                         @else
                                            <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
                                        @endif
                                        <div class='n_c_container'>
                                            <h5 class="d_name_text">{{ucfirst($invitation->user->name)}}</h5>
                                            <p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
                                        </div>
                                   </div>
                                   <table id="n_desing_tbl">

                                    @php
                                       $doctor = $doctorList[$invitation->invitee_user_id];
                                    @endphp

                                    <tr>
                                        <th>Doctor:</th>
                                        <th>{{$doctor->name}}</th>
                                    </tr>
                                    <tr>

                                    <tr>
                                        <th>Category:</th>
                                        <td>{{ $invitation->category->category_name }}</td>
                                    </tr>
                                     
                                      <tr>
                                        <th>Date:</th>
                                        <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
                                      </tr>
                                      <tr>
                                        <th>Due date:</th>
                                        <td>{{date('d F Y', strtotime($invitation->duedate ))}}
                                        </td>
                                      </tr>

                                      <tr>
                                        <th>Type:</th>
                                        <td>{{ucfirst($invitation->appointment_type)}}</td>
                                      </tr>
                                   </table>

                                   <div class="button_containter">

                                    <a  class="nav-link addpatient_btn"  style="background:#d61f12 !important;border-radius:10px !important;" >Rejected By {{($invitation->rejected_by == 'patient' ? 'Patient' : 'You')}}</a>

                                   </div>

                                </div>
                        
                            
                        </div>	



                        <!---end--->
                        @endforeach
                        @else
                        <div class="col-md-12">
                            <h4 style="text-align:center;">No rejected assessment(s) found.</h4>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="tab-pane" id="cancelled-invitations" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            {{-- <h3 class="inv-content-header">Cancelled Assessment(s)</h3> --}}
                        </div>
                        @if ($cancelled_invitations)
                            @foreach($cancelled_invitations as $invitation)				
                            <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
                                <div class="design_card_new">
                                   <div class="d-flex">
                                        @if (($invitation->user->profilepic)!=null)
                                             <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
                                         @else
                                            <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
                                        @endif
                                        <div class='n_c_container'>
                                            <h5 class="d_name_text">{{ucfirst($invitation->user->name)}}</h5>
                                            <p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
                                        </div>
                                   </div>
                                   <table id="n_desing_tbl">

                                    @php
                                $doctor = $doctorList[$invitation->invitee_user_id];
                            @endphp

                            <tr>
                                <th>Doctor:</th>
                                <th>{{$doctor->name}}</th>
                              </tr>
                            <tr>

                              <tr>
                                <th>Category:</th>
                                <td>{{ $invitation->category->category_name }}</td>
                              </tr>
                                     
                                      <tr>
                                        <th>Date:</th>
                                        <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
                                      </tr>
                                      <tr>
                                        <th>Due date:</th>
                                        <td>{{date('d F Y', strtotime($invitation->duedate ))}}
                                        </td>
                                      </tr>

                                      <tr>
                                        <th>Type:</th>
                                        <td>{{ucfirst($invitation->appointment_type)}}</td>
                                      </tr>
                                   </table>

                                   <div class="button_containter">

                                    <a  class="nav-link addpatient_btn"  style="background:#d61f12 !important;border-radius:10px !important;" >Cancelled By Lawyer</a>

                                   </div>

                                </div>
                        
                            
                        </div>
                            
                            

                        <!---end--->
                        @endforeach
                        @else
                        <div class="col-md-12">
                            <h4 style="text-align:center;">No Cancelled assessment(s) found.</h4>
                        </div>
                        @endif
                    </div>
                </div>
                </div>
                </div>	
            </div>	
        </div>
        {{-- Assessment Section End   --}}
        </div>

        {{-- ************** Invoice Section Start ************** --}}
        <div class="tab-pane fade" id="invoiceTab" role="tabpanel" aria-labelledby="Invoice Tab">
           <div class="accordion patient_details_box1">
            <div class="col-md-12">
                <div class="patients_list_main">
                    <?php if($invoices->isEmpty() || ($suminvoices == 0)){  ?>
                    <h3 style="text-align:center;">No Records Found</h3>
                    <?php  }else{  ?>
                    <div class="row">
                        <div class="col-md-12">
                                <h4 style="float:right;">
                                <?php
                                    echo 'Total Billed to Date '.date('d F Y');
                                ?><b style="font-weight: 600;"> ${{$suminvoices}}</b></h4>
                        </div>	
                        <?php //$number =1; ?>
                        <?php	
                            //print_r($invoices); 									
                            foreach($invoices as $invoices1) {                                        
                                //print_r($invoices1);                                        
                            ?>
                        <div class="col-md-12">
                                <?php  if($invoices1->invitations->status!='cancelled') { ?>
                                <div class="single_patient">
                                    <ul>
                                        <li>
                                        <img src="https://proassessors.com/img/checkmark.svg" width="20" />
                                        <span style="color: #00528c;">
                                        Invoice Submitted Date:</span>  <?php echo date('d F Y', strtotime($invoices1->invoice_date )); ?>
                                        <span style="margin-left: 13px;"><span style="color: #00528c;">Patient: </span>{{ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname)}}</span>
                                        <span class="patientfile">
                                            <?php
                                                if($invoices1->payment_status == 'success') {
                                                    echo 'Paid';
                                                } else {
                                                    echo 'Pending';
                                                }
                                            ?>
                                            <a href="javascript:void(0)" onClick="reply_click_doctorinvoice(id)" id="{{ $invoices1->invitations->id }}"  title="View Invoice"><i class="fa fa-info-circle" ></i></a>
                                            <!--<a href="" title="Appointments"><i class="fa fa-calendar" ></i></a>-->
                                            <a title="Download Invoice" href="{{url('/uploads/custom_invoice')}}/{{$invoices1->invoice_filename}}" download="invoice_{{$invoices1->invoice_filename}}" target="_blank"><i class="fa fa-download"></i></span></li></a> 

                                            
                                        </span>
                                        </li>
                                            
                                    </ul>
                                </div>	
                                <?php } ?>	
                            <!--</a>-->
                        </div>
                            <?php  }  ?>                     
                        
                    </div>
                    {{$invoices->links("pagination::bootstrap-4")}}
                    
                    <?php  } ?>
                    
                </div>
                
            </div>	
           </div>
        </div>
         {{-- ************** Invoice Section End ************** --}}



         {{-- ################### Doctors List Start ################## --}}
         <div class="tab-pane fade" id="doctorTab" role="tabpanel" aria-labelledby="Invoice Tab">
            <div class="accordion patient_details_box1">
             <div class="col-md-12">

                <div style="display:flex;justify-content: end;padding-bottom: 10px;">
                    <a href="{{url('/addClinicDoctor')}}" class="btn btn-outline-success btn-sm">
                        <i class="fa fa-user-md pr-1" aria-hidden="true"></i>
                        New Doctor
                    </a>
                </div>

                <table id="tableSorter" class="row-border hover cell-border" style="width:100%">
                    <thead>
                        <tr>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>License Number</th>
                        {{-- <th>Status</th>
                        <th>Admin status</th> --}}
                        <th></th>
                        </tr>
                    </thead>
        
                    <tbody>
                        @if (count($dcotorLs) > 0)
                          @foreach ($dcotorLs as $row)
                              <tr>
                                <th>{{$row->name}}</th>
                               
                                <td>{{$row->phone}}</td>
                                <td>{{$row->doctor->licence_number}}</td>
                                {{-- <td>
                                    @if ($row->status == 'active')
                                       <span class="bg-success px-2 rounded">Active</span> 
                                       @else
                                       <span class="bg-warning px-2 rounded">Pending</span> 
                                    @endif
                                </td>
                                <td>
                                    @if ($row->authentication_status == 'active')
                                    <span class="bg-success px-2 rounded">Active</span> 
                                    @else
                                    <span class="bg-warning px-2 rounded">Pending</span> 
                                 @endif
                                </td> --}}
                                <td align="center">
                                    <a href ="{{url('/cldoctorprofile/'.$row->id)}}" class="btn btn-info btn-sm" title="View profile">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>

                                    <a href ="{{url('/viewClinicCalender/'.$row->id)}}" class="btn btn-warning btn-sm" title="View calendar">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </a>

                                    <a href ="{{url('/doctor-profile/'.$row->id)}}" class="btn btn-danger btn-sm" title="Edit profile">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
        
                                </td>
                              </tr>
                          @endforeach                    
                        @endif
                    </tbody>
        
                  </table>

             </div>
            </div>
         </div>
         {{-- ################### Doctors List End    ################## --}}

    </div>

</div>

@endsection