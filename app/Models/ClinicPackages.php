<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClinicPackages extends Model
{
    use HasFactory;
    protected $table = 'clinic_packages';
}
