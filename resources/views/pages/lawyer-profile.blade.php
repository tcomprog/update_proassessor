@extends('layouts.lawyer-das-custom') 
@section('content')	
<link href="{{ asset('css/lawyer_profile.css') }}" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<?php
		$activeTab = $activeTab !='my-patients' && $activeTab !='chat-room' && $activeTab !='tracking' && $activeTab !='search-clinic' ? 'search-doctors' : $activeTab;
		$route = Route::current()->getName();
		if($route=="search-patient"){
			$activeTab = 'my-patients';
		}

		if($route=="tracking"){
		$activeTab = 'tracking';
		}
?>

	 <!-- Banner Section -->
    <section class="intro_banner_sec inner_banner_sec" style="padding-top: 24px;padding-bottom: 24px;">
        <div class="container">
            <div class="row align-items-center"> 
                <div class="col-md-6">
                    <div class="welcome_text">
                        <h1>Lawyer's Dashboard <img src="{{ asset('img/lawyer_icon_white.png') }}" height="35" alt="Lawyer's Dashboard" class="dashboard_icon"></h1>
                    </div>
                </div>
                <div class="col-md-6 text-right">
                    <!-- <a href="<?= url('search-doctor'); ?>" class="search_btn">Search Doctors</a> -->
                </div>
            </div>
        </div>
    </section>
	
	<section class="dashboard_sec">
        <div class="container" style="min-height: 400px;">
            @if($ac_status == 0)
            <div class="alert alert-danger" role="alert">
               Please complete your profile!
               <a href="/lawyer-profile">Click here</a>
            </div>
            @endif
        
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">

	<a class="nav-item nav-link @if($activeTab == 'search-doctors') active @endif"
     id="search-doctors" data-toggle="tab" href="#nav-search-doctors" role="tab"
      aria-controls="nav-search-doctors" aria-selected="false" style="font-weight: 600; font-size: 21px;">Search Doctors</a>

	<a class="nav-item nav-link @if($activeTab == 'search-clinic') active @endif"
     id="search-clinic" data-toggle="tab" href="#search-tab-clinic" role="tab"
      aria-controls="search-tab-clinic" aria-selected="false" style="font-weight: 600; font-size: 21px;">Search Facility</a>

    <a class="nav-item nav-link @if($activeTab == 'my-patients') active @endif"  id="my-patients" data-toggle="tab" href="#nav-my-patients" role="tab" aria-controls="my-patients"    aria-selected="false" style="font-weight: 600;
    font-size: 21px;">My Clients</a>

    <a 
    class="nav-item nav-link @if($activeTab == 'tracking') active @endif"
    id="tracking" data-toggle="tab" href="#nav-tracking" role="tab" aria-controls="tracking"
    aria-selected="false" style="font-weight: 600; font-size: 21px;">History</a>

                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">

              
				<div class="tab-pane fade @if($activeTab == 'my-patients') show active @endif" id="nav-my-patients" role="tabpanel" aria-labelledby="nav-patients-tab">
                    <div class="row">
                        <div class="col-md-6" style="padding-right: 0px;">
						
						<form class="search_form" method="GET" action="{{ route('search-patient') }}">
                                <div class="input-group">
                                    <input class="form-control" id="system_search_lawyer" name="patient_name" placeholder="Search Client Name" value="<?php if(isset($_GET['patient_name'])){echo $_GET['patient_name'];}?>">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-default search_btn_new">
                                            <!--<img src="img/search.png">-->
											<img src="{{ url('img/search.png') }}">
                                        </button>
                                    </span>
                                </div>
                         </form>
                            
                        </div>
                        <div class="col-md-2">
                           
                        </div>
                     
                        <?php  if(isset($_GET['patient_name'])) { ?>
                           
						<div class="col-md-2 text-right">
                            <a class="nav-link addpatient_btn" href="<?= url('lawyer-dashboard/my-patients'); ?>" >View All</a>
                        </div>
						
						<div class="col-md-2 text-right">
                            <a href="#" class="nav-link addpatient_btn"  data-toggle="modal" data-target="#addPatient"> <i class="fa fa-user-plus" aria-hidden="true"></i> Client</a>
                        </div>

						<?php }
                        else { ?>
                        <div class="col-md-4 text-right">
                            <a href="#" class="nav-link addpatient_btn" data-toggle="modal" data-target="#addPatient"> <i class="fa fa-user-plus" aria-hidden="true"></i> Client</a>
                        </div>
						<?php } ?>
					
                        <div class="col-md-12">
                            <div class="patients_list_main">
								<?php if($patient->isEmpty()){  ?>
								<h3 style="text-align:center;">No Records Found</h3>
								<?php  } else{  ?>
                                <div class="row">	
									<?php //$number =1; ?>
                                    <?php										
										foreach($patient as $patient1) {                                        
                                       // dd($patient1->patientstatus['status']);                                        
                                        ?>
                                    <div class="col-md-12">
									         <?php 	
											//var_dump($patient1->patientinvitation->status);die();
											if($patient1->patientinvitation!=null){  ?>
                                            <div class="single_patient single_patient222" onclick="location.href=`{{ url('/patient_details/') }}/{{ $patient1->id }}?appointments=true`;" style="cursor: pointer;">                                                
                                                <ul> 
                                                  <li><span ><?php //echo $number; ?></span>									
                                                    <a href="{{ url('/patient_details/') }}/{{ $patient1->id }}?appointments=true" title="Patient Information" style="text-decoration:none;">
                                                    <?php  echo ucfirst(strtolower($patient1->fname)).' '.ucfirst(strtolower($patient1->lname)); ?>														</a>													<span class="patientfile">
													<a href="{{ url('/patient_details/') }}/{{ $patient1->id }}?clientinformation=true"><i class="fa fa-info-circle" ></i></a>
													<a href="{{ url('/patient_details/') }}/{{ $patient1->id }}?appointments=true"><i class="fa fa-calendar" ></i></a>
													<a href="{{ url('/patient_details/') }}/{{ $patient1->id }}#Reports">
													<i class="fa fa-file"></i></span></a> </li>
														
                                                </ul>                                               
                                            </div>										
											<?php  } else{?>											 
											
											
												<div class="single_patient single_patient222" onclick="location.href=`{{ url('/patient_details/') }}/{{ $patient1->id }}?appointments=true`;" style="cursor: pointer;"><!--patientfile-->
													<ul> 
													   <li><span ><?php //echo $number; ?></span>			
                                                    <a style="text-decoration:none;" href="{{ url('/patient_details/') }}/{{ $patient1->id }}#patientfile" title="Patient Information">														
                                                        <?php  echo ucfirst(strtolower($patient1->fname)).' '.ucfirst(strtolower($patient1->lname)); ?>														</a>														<span class="patientfile">
													<a href="{{ url('/patient_details/') }}/{{ $patient1->id }}?clientinformation=true" title="Patient Information"><i class="fa fa-info-circle" ></i></a>
													<a href="{{ url('/patient_details/') }}/{{ $patient1->id }}?appointments=true" title="Appointments"><i class="fa fa-calendar" ></i></a>
													<a href="{{ url('/patient_details/') }}/{{ $patient1->id }}#Reports" title="Reports">
													<i class="fa fa-file"></i></span></a> </li>
													</ul>
												</div>
											
											<?php  }  ?>	
										 <?php //$number++; ?>
										
                                        <!--</a>-->
                                    </div>
										<?php  }  ?>                     
									
                                </div>
								
								<?php  } ?>
								
                            </div>
							
                        </div>
                       
                    </div>
            </div>
				
				<!-- Add Patient Model -->
                <div class="modal fade " id="addPatient" aria-labelledby="addPatientTitle" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered addPatentMod" role="document">
                    <div class="modal-content login_box addPatentMod">
                       
                        <div class="modal-header" style="border:none; padding:0px !important; margin-left:0px !important;">
                        <h5 class="modal-title addTitle" id="addPatientTitle">Add Client</h5>
                        
                        <button type="button" class="close close1122" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        
                        <div class="modal-body">
                        <div class="alert alert-success" role="alert" id="successform" style="display:none;">
                            Client Added successfully.
                        </div>
                        <form id="addpatient_form" style="padding: 15px 10px; padding-top:5px;">
                            @csrf
                            <div class="row">
								<div class="col-md-12">
                                    <div class="form-group" style="margin-bottom:0px;">
											<label for="dob" style="font-weight: bold !important;">Gender</label> 
											<input checked type="radio" id="male" name="gender" value="male" style="margin-left: 10px;">
  											<label for="male">Male</label>&nbsp;&nbsp;&nbsp;
											<input type="radio" id="female" name="gender" value="female">
  											<label for="female">Female</label>
											&nbsp;&nbsp;&nbsp;&nbsp;<label id="gender-error" class="error" for="gender"></label>
                                    </div>
                                </div>
								
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control addP_inc" name="fname" id="fname" aria-describedby="fname" placeholder="First name">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control addP_inc" name="lname" id="lname" aria-describedby="fname" placeholder="Last name">
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="email" class="form-control addP_inc"  name="email"  id="email" aria-describedby="email" placeholder="Email">
                                        <span class="text-danger" id="emailexists" style="color:red;"></span>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control addP_inc" name="filenumber" id="filenumber" aria-describedby="filenumber" placeholder="File Number">
                                    </div>
                                </div>							
								
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="adss_ads">Phone number</label>
                                        <input type="text" class="form-control addP_inc" name="phonenumber" id="phonenumber" aria-describedby="Phone Number" maxlength="10" placeholder="Phone number">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                    {{-- <input type="text" class="form-control addP_inc" name="casetype"  id="casetype" aria-describedby="casetype" placeholder="Case Type"> --}}
                                        <label for="adss_ads">Chose Case Type</label>
                                        <select name="casetype" class="form-control" id="adss_ads">
                                            <option value="1" selected>MVA</option>
                                            <option value="2">WSIB</option>
                                            <option value="3">Slip & Fall</option>
                                        </select>
                                      </div>
                                </div>
								
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="pl-1">Date of Birth</label>
                                        <input autocomplete="off" class="form-control  date_select2 addP_inc" name="dob"  id="dob" aria-describedby="lname" placeholder="Date of Birth">
                                    </div>
                                </div>
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="pl-1">Date of Accident</label>
                                        <input autocomplete="off" type="text" class="form-control  date_select2 addP_inc" name="doa" id="doa" aria-describedby="lname" placeholder="Date of Accident">
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control addP_inc" name="address"  id="address" aria-describedby="address" placeholder="Address">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="terms" name="terms">
                                        <label class="form-check-label" for="exampleCheck1"><a href="" data-toggle="modal" data-target="#iagree">I agree to the terms and conditions.</a></label><br>
                                        <label class="error" generated="true" for="terms" style="display:none;"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center mt-2">
                            <button type="submit" class="btn btn-primary submit_btn submit_btn22" id="addpatient_form_button">Submit</button>
                        </div>
                        </form>
                        </div>
                    </div>
                    </div>
                </div>
                <!--end add patient-->

                <!-- Add I agree -->
                <div class="modal fade" id="iagree" tabindex="1" role="dialog" aria-labelledby="addPatientTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content login_box">
                        <div class="modal-header">
                        <h5 class="modal-title" id="addPatientTitle">Terms for adding client</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                        
                           <?php echo htmlspecialchars_decode($iagree->content); ?>
                        
                        
                        </div>
                    </div>
                    </div>
                </div>
                <!--end I agree-->

                <div class="tab-pane fade " id="nav-invitations" role="tabpanel" aria-labelledby="nav-invitations-tab">
                     <div class="row">
                       <div id="tag_container">
							   @include('pages.lawyer.presult')
						</div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-calendar" role="tabpanel" aria-labelledby="nav-calendar-tab">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="search_sec">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
                </div>
				<!-------calendar----->
				<div class="modal fade" id="UploadDocs" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">
				  <div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content login_box">
					  <div class="modal-header">
						<h5 class="modal-title" id="calendarTitle">Please upload patients documents</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>
					  <div class="modal-body">
						<form id="upload_form"  enctype="multipart/form-data">
							<div class="form-group files">
							  <input type="file" class="form-control" name="files[]" id="files" multiple="" required>
							  <span class="text-danger" id="upload-error" style="color:red;"></span>
							  <h5 class="text-success" id="upload-scss" style="color:green;"></h5>
							</div>
							<div class="form-group">
							  <input type="hidden" name="newintid" id="newintid"> 
							</div>
							<button  type="submit" class="invite_btn upload_btn" id="submit">Submit</button> 
						</form>
					  </div>
					</div>
				  </div>
				</div>
                <div class="tab-pane fade" id="nav-reports" role="tabpanel" aria-labelledby="nav-reports-tab">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table_wrapper">
                                <div class="list_box">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <ul>
                                                <li>Name: <span>Cara Stevens</span></li>
                                                <li>Email: <span>carastevens@example.com</span></li>
                                                <li>Phone: <span>9876543210</span></li>
                                                <li>Business Address: <span>#123, Sample Address</span></li>
                                                <li>Location: <span>Test location</span></li>
                                                <li>Date: <span>20-04-2021</span></li>
                                                <li>Website URL: <span>http://example.com/</span></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 line text-center">
                                            <a href="#" class="invite_btn reports_invite">Accepted</a>
                                            <a href="#"  data-toggle="modal" data-target="#Availablecalendar" class="invite_btn reports_invite">Upload Reports</a>
                                            <a href="#" class="invite_btn reports_invite" data-toggle="modal" data-target="#Availablecalendar">Send Invoice</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="list_box">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <ul>
                                                <li>Name: <span>John Doe</span></li>
                                                <li>Email: <span>john@example.com</span></li>
                                                <li>Phone: <span>9876543210</span></li>
                                                <li>Business Address: <span>#123, Sample Address</span></li>
                                                <li>Location: <span>Test location</span></li>
                                                <li>Date: <span>20-04-2021</span></li>
                                                <li>Website URL: <span>http://example.com/</span></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 line text-center">
                                            <a href="#" class="invite_btn reports_invite">Rejected</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="list_box">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <ul>
                                                <li>Name: <span>Airi Satou</span></li>
                                                <li>Email: <span>airi@example.com</span></li>
                                                <li>Phone: <span>1234567890</span></li>
                                                <li>Business Address: <span>#123, Sample Address</span></li>
                                                <li>Location: <span>Test location</span></li>
                                                <li>Date: <span>20-04-2021</span></li>
                                                <li>Website URL: <span>http://example.com/</span></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 line text-center">
                                            <a href="#" class="invite_btn reports_invite">Accepted</a>
                                            <a href="#"  data-toggle="modal" data-target="#Availablecalendar" class="invite_btn reports_invite">Upload Reports</a>
                                            <a href="#" class="invite_btn reports_invite" data-toggle="modal" data-target="#Availablecalendar">Send Invoice</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="list_box">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <ul>
                                                <li>Name: <span>Angelica Ramos</span></li>
                                                <li>Email: <span>angelica@example.com</span></li>
                                                <li>Phone: <span>1234567890</span></li>
                                                <li>Business Address: <span>#123, Sample Address</span></li>
                                                <li>Location: <span>Test location</span></li>
                                                <li>Date: <span>20-04-2021</span></li>
                                                <li>Website URL: <span>http://example.com/</span></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 line text-center">
                                            <a href="#" class="invite_btn reports_invite">Rejected</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="list_box">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <ul>
                                                <li>Name: <span>Jens Brincker</span></li>
                                                <li>Email: <span>brincker@example.com</span></li>
                                                <li>Phone: <span>1234567890</span></li>
                                                <li>Business Address: <span>#123, Sample Address</span></li>
                                                <li>Location: <span>Test location</span></li>
                                                <li>Date: <span>20-04-2021</span></li>
                                                <li>Website URL: <span>http://example.com/</span></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 line text-center">
                                            <a href="#" class="invite_btn reports_invite">Accepted</a>
                                            <a href="#"  data-toggle="modal" data-target="#Availablecalendar" class="invite_btn reports_invite">Upload Reports</a>
                                            <a href="#" class="invite_btn reports_invite" data-toggle="modal" data-target="#Availablecalendar">Send Invoice</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link active" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade @if($activeTab == 'chat-room') show active @endif" id="nav-chatroom" role="tabpanel" aria-labelledby="chat-room">
                    <div class="messaging">
                        <div class="inbox_msg">
                            <div class="inbox_people">
                            <div class="headind_srch">
                                <div class="recent_heading">
                                <h4>Recent</h4>
                                </div>
                                <div class="srch_bar">
                                <div class="stylish-input-group">
                                    <input type="text" class="search-bar"  placeholder="Search" >
                                    <span class="input-group-addon">
                                    <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                                    </span> </div>
                                </div>
                            </div>
                            <div class="inbox_chat">
                                <div class="chat_list active_chat">
                                <div class="chat_people">
                                    <div class="chat_img"> <img src="img/user.jpg" alt="user"> </div>
                                    <div class="chat_ib">
                                    <h5>Sunil Rajput <span class="chat_date">April 15</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                        astrology under one roof.</p>
                                    </div>
                                </div>
                                </div>
                                <div class="chat_list">
                                <div class="chat_people">
                                    <div class="chat_img"> <img src="img/user.jpg" alt="user"> </div>
                                    <div class="chat_ib">
                                    <h5>Sunil Rajput <span class="chat_date">April 15</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                        astrology under one roof.</p>
                                    </div>
                                </div>
                                </div>
                                <div class="chat_list">
                                <div class="chat_people">
                                    <div class="chat_img"> <img src="img/user.jpg" alt="user"> </div>
                                    <div class="chat_ib">
                                    <h5>Sunil Rajput <span class="chat_date">April 15</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                        astrology under one roof.</p>
                                    </div>
                                </div>
                                </div>
                                <div class="chat_list">
                                <div class="chat_people">
                                    <div class="chat_img"> <img src="img/user.jpg" alt="user"> </div>
                                    <div class="chat_ib">
                                    <h5>Sunil Rajput <span class="chat_date">April 15</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                        astrology under one roof.</p>
                                    </div>
                                </div>
                                </div>
                                <div class="chat_list">
                                <div class="chat_people">
                                    <div class="chat_img"> <img src="img/user.jpg" alt="user"> </div>
                                    <div class="chat_ib">
                                    <h5>Sunil Rajput <span class="chat_date">April 15</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                        astrology under one roof.</p>
                                    </div>
                                </div>
                                </div>
                                <div class="chat_list">
                                <div class="chat_people">
                                    <div class="chat_img"> <img src="img/user.jpg" alt="user"> </div>
                                    <div class="chat_ib">
                                    <h5>Sunil Rajput <span class="chat_date">April 15</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                        astrology under one roof.</p>
                                    </div>
                                </div>
                                </div>
                                <div class="chat_list">
                                <div class="chat_people">
                                    <div class="chat_img"> <img src="img/user.jpg" alt="user"> </div>
                                    <div class="chat_ib">
                                    <h5>Sunil Rajput <span class="chat_date">April 15</span></h5>
                                    <p>Test, which is a new approach to have all solutions 
                                        astrology under one roof.</p>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div class="mesgs">
                            <div class="msg_history">
                                <div class="incoming_msg">
                                <div class="incoming_msg_img"> <img src="img/user.jpg" alt="user"> </div>
                                <div class="received_msg">
                                    <div class="received_withd_msg">
                                    <p>Test which is a new approach to have all
                                        solutions</p>
                                    <span class="time_date"> 11:01 AM    |    April 16</span></div>
                                </div>
                                </div>
                                <div class="outgoing_msg">
                                <div class="incoming_msg_img"> <img src="img/user.jpg" alt="user"> </div>
                                <div class="sent_msg">
                                    <p>Test which is a new approach to have all
                                    solutions</p>
                                    <span class="time_date"> 11:01 AM    |    April 16</span> </div>
                                </div>
                                <div class="incoming_msg">
                                <div class="incoming_msg_img"> <img src="img/user.jpg" alt="user"> </div>
                                <div class="received_msg">
                                    <div class="received_withd_msg">
                                    <p>Test, which is a new approach to have</p>
                                    <span class="time_date"> 11:02 AM    |    Yesterday</span></div>
                                </div>
                                </div>
                                <div class="outgoing_msg">
                                <div class="incoming_msg_img"> <img src="img/user.jpg" alt="user"> </div>
                                <div class="sent_msg">
                                    <p>Apollo University, Delhi, India Test</p>
                                    <span class="time_date"> 11:01 AM    |    Today</span> </div>
                                </div>
                                <div class="incoming_msg">
                                <div class="incoming_msg_img"> <img src="img/user.jpg" alt="user"> </div>
                                <div class="received_msg">
                                    <div class="received_withd_msg">
                                    <p>We work directly with our designers and suppliers,
                                        and sell direct to you, which means quality, exclusive
                                        products, at a price anyone can afford.</p>
                                    <span class="time_date"> 11:01 AM    |    Today</span></div>
                                </div>
                                </div>
                            </div>
                            <div class="type_msg">
                                <div class="input_msg_write">
                                <input type="text" class="write_msg" placeholder="Type a message" />
                                <button class="msg_send_btn" type="button"><img src="img/telegram.svg"></button>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="tab-pane fade @if($activeTab == 'search-doctors') show active @endif" id="nav-search-doctors" role="tabpanel" aria-labelledby="search-doctors">
                    <div id="search-doctors-tab-content">
                    <section class="pt-0 dashboard_sec">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="search_sec1">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!--<button class="btn btn-primary" onclick="javascript:history.go(-1)">Back</button>-->
                                                <div class="search_box1">
                                                    <form method="GET" id="search-doctors-form" action="{{ route('search-doctor') }}">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <!-- <h2>Search a Doctor</h2> -->
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="keyword" id="search-keyword" aria-describedby="name" placeholder="Name" value="<?php if(isset($_GET['keyword'])){echo $_GET['keyword'];}?>" >
                                                                </div>
                                                            </div> 
                                                            <!---start---->
                                                            <div class="col-md-3">                    
                                                                <div class="form-group">
                                                                    <?php  $category = Helper::categorydetails();  ?>
                                                                    <select class="form-control category_select @error('designation') is-invalid @enderror" id='search-category_id' name="category_id"   autofocus>
                                                                        <option value="">Choose Category</option>
                                                                        <?php  foreach ($category as $categories){ ?>
                                                                        <option value="<?php echo $categories->id; ?>" style ="text-transform:capitalize;"><?php echo $categories->category_name; ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                            </div> 
                                                            <!-----end----->

                                                            <div class="col-md-2">
                                                                <div class="form-group new_form_group">
                                                                    <!--<input type="text" class="form-control"  id="location" name="location" aria-describedby="location" placeholder="Location">-->
                                                                        <div class="form-group">
                                                                            <?php  $cities = Helper::cities();  ?>  
                                                                            <select class="form-control @error('location') is-invalid @enderror category_select" id='search-location' name="location"   autofocus>
                                                                                <option value="">Choose City</option>
                                                                                <?php  foreach ($cities as $cities1){ ?>								    
                                                                                <option value="<?php echo $cities1->id; ?>"  
                                                                                    <?php if(isset($_GET['location'])){
                                                                                    if(($cities1->id)==($_GET['location'])){echo "selected";} } ?> > 
                                                                                    <?php echo $cities1->city_name; ?></option> <?php } ?>   
                                                                            </select>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            
                                                            {{-- <div class="col-md-2">
                                                                <div class="form-group new_form_group">
                                                                    <div class="form-group">
                                                                        <select class="form-control category_select" id='cliniclist' name="cliniclist" autofocus>
                                                                            <option value="" selected>Choose Clinic</option>
                                                                            @foreach ($clinic as $row){}
                                                                                <option value="{{$row->id}}">{{ucfirst($row->name)}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div> --}}

                                                            <div class="col-md-2">
                                                                <div class="form-group new_form_group">
                                                                    <div class="form-group">
                                                                        <select class="form-control category_select" id='price_range' name="price_range"   autofocus>
                                                                          <option value="" selected>Price Range</option>
                                                                           <option value="1500">$0-$1500</option>
                                                                            <option value="2500">$1500-$2500</option>
                                                                            <option value="3500">$2500-$3500</option>
                                                                            <option value="4500">$3500-$4500</option>
                                                                            <option value="5500">$4500-$5500</option>
                                                                            <option value="6499">$5500-$6500</option> 
                                                                            <option value="6500">$6500 and above</option> 
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2 text-right">
                                                                <button type="submit" class="nav-link search-btn addpatient_btn border border-none">Search</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END Search box --->
                            <div class="text-center w-100"> <img src="{{ asset('images/loading.gif') }}" id="search-doctors-loading-img" width="80px" alt=""></div>
                            <div id="search-doctors-ajax-list"></div>
                        </div>
                    </section>
					</div>
                </div>


                {{-- ********** Search by clinic start ************** --}}
                <div class="tab-pane fade @if($activeTab == 'search-clinic') show active @endif" id="search-tab-clinic" role="tabpanel" aria-labelledby="search-doctors">
                    <div id="search-clinic-tab-content">
                        <section class="pt-0 dashboard_sec">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="search_sec1">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <!--<button class="btn btn-primary" onclick="javascript:history.go(-1)">Back</button>-->
                                                    <div class="search_box1">
                                                        <form method="GET" id="search-clinic-form" action="{{ route('search-doctor') }}">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <!-- <h2>Search a Doctor</h2> -->
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" name="keyword" id="search-clinic-keyword" aria-describedby="name" placeholder="Name" value="<?php if(isset($_GET['keyword'])){echo $_GET['keyword'];}?>" >
                                                                    </div>
                                                                </div> 
                                                                <!---start---->
                                                                <div class="col-md-2">                    
                                                                    <div class="form-group">
                                                                        <?php  $category = Helper::categorydetails();  ?>
                                                                        <select class="form-control category_select @error('designation') is-invalid @enderror" style="height: auto;" id='search-clinic-category_id' name="category_id"   autofocus>
                                                                            <option value="">Choose Category</option>
                                                                            <?php  foreach ($category as $categories){ ?>
                                                                            <option value="<?php echo $categories->id; ?>" style ="text-transform:capitalize;"><?php echo $categories->category_name; ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </div> 
                                                                <!-----end-----> 
    
                                                                <div class="col-md-2">
                                                                    <div class="form-group new_form_group">
                                                                            <div class="form-group">
                                                                                <?php  $cities = Helper::cities();  ?>  
                                                                                <select class="form-control @error('location') is-invalid @enderror category_select" style="height: auto;" id='search-clinic-location' name="location"   autofocus>
                                                                                    <option value="">Choose City</option>
                                                                                    <?php  foreach ($cities as $cities1){ ?>								    
                                                                                    <option value="<?php echo $cities1->id; ?>"  
                                                                                        <?php if(isset($_GET['location'])){
                                                                                        if(($cities1->id)==($_GET['location'])){echo "selected";} } ?> > 
                                                                                        <?php echo $cities1->city_name; ?></option> <?php } ?>   
                                                                                </select>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="col-md-2">
                                                                    <div class="form-group new_form_group">
                                                                        <div class="form-group">
                                                                            <select class="form-control category_select" style="height: auto;" id='cliniclist' name="cliniclist" autofocus>
                                                                                <option value="" selected>Choose Clinic</option>
                                                                                @foreach ($clinic as $row){}
                                                                                    <option value="{{$row->id}}">{{ucfirst($row->name)}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
    
                                                                <div class="col-md-2">
                                                                    <div class="form-group new_form_group">
                                                                        <div class="form-group">
                                                                            <select class="form-control category_select" id='price_clinic_range' name="price_range" style="height: auto;"  autofocus>
                                                                              <option value="" selected>Price Range</option>
                                                                               <option value="1500">$0-$1500</option>
                                                                                <option value="2500">$1500-$2500</option>
                                                                                <option value="3500">$2500-$3500</option>
                                                                                <option value="4500">$3500-$4500</option>
                                                                                <option value="5500">$4500-$5500</option>
                                                                                <option value="6499">$5500-$6500</option> 
                                                                                <option value="6500">$6500 and above</option> 
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
    
                                                                <div class="col-md-2 text-right">
                                                                    <button type="submit" class="nav-link search-btn addpatient_btn border border-none">Search</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--END Search box --->
                                <div class="text-center w-100"> <img src="{{ asset('images/loading.gif') }}" id="search-clinic-loading-img" width="80px" alt=""></div>
                                <div id="search-clinic-ajax-list"></div>
                            </div>
                        </section>
                        </div>
                </div>
                {{-- ********* Search by clinic end ************ --}}




                <!--start tracking-->
                <div class="tab-pane fade @if($activeTab == 'tracking') show active @endif" id="nav-tracking" role="tabpanel" aria-labelledby="nav-tracking">
                
                    <div class="row my-3">
                        <div class="col-6">

                       <form class="search_form" method="GET" action="{{ route('tracking') }}">
                           <div class="input-group">
                               <input required class="form-control" name="tracking_search" placeholder="Search Client Name" value="<?php if(isset($_GET['tracking_search'])){echo $_GET['tracking_search'];}?>">
                                <input type="hidden" name="selectedTab" value="1" id="trackingSelectedTab">
                               <span class="input-group-btn">
                                   <button type="submit" class="btn btn-default search_btn_new">
                                       <img src="{{ url('img/search.png') }}">
                                   </button>
                               </span>
                           </div>
                        </form>
                          
                       </div>

                       @isset($_GET["tracking_search"])
                           <div class="col-md-2 text-right">
                               <a class="nav-link addpatient_btn" href="<?= url('lawyer-dashboard/tracking'); ?>" >View All</a>
                           </div>
                       @endisset
                     
                   </div>
                    <!-- =========== 16-07-2021 ##### =========== -->
                    <div class="patient_details_box1 invitation-tabs" style="min-height: 400px;">

                        {{-- TODO: <ab --}}
                    

                        <ul class="nav nav-tabs" id="myTab" role="tablist" style="border:none;">
                            <li class="nav-item">
                                <a id="myTab_1" data-id="1" class="mytbslnk nav-link sent active" data-toggle="tab" href="#sent-invitations" role="tab" aria-controls="sent-invitations" aria-selected="true">Sent</a>
                            </li>
                            <li class="nav-item">
                                <a id="myTab_2" data-id="2" class="mytbslnk nav-link accepted" data-toggle="tab" href="#accepted-invitations" role="tab" aria-controls="accepted-invitations" aria-selected="false">Accepted</a>
                            </li>
                            <li class="nav-item">
                                <a id="myTab_3" data-id="3" class="mytbslnk nav-link confirmed" data-toggle="tab" href="#confirmed-invitations" role="tab" aria-controls="confirmed-invitations" aria-selected="false">Confirmed</a>
                            </li>
                            <li class="nav-item">
                                <a id="myTab_4" data-id="4" class="mytbslnk nav-link expired" data-toggle="tab" href="#expired-invitations" role="tab" aria-controls="expired-invitations" aria-selected="false">Expired</a>
                            </li>
                            <li class="nav-item">
                                <a id="myTab_5" data-id="5" class="mytbslnk nav-link not-accepted" data-toggle="tab" href="#rejected-invitations" role="tab" aria-controls="expired-invitations" aria-selected="false">Not accepted</a>
                            </li>
                            <li class="nav-item">
                                <a id="myTab_6" data-id="6" class="mytbslnk nav-link cancelled" data-toggle="tab" href="#cancelled-invitations" role="tab" aria-controls="cancelled-invitations" aria-selected="false">Cancelled</a>
                            </li>
                        </ul>
                         <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="sent-invitations" role="tabpanel" aria-labelledby="nav-invitations-tab">
                                
                          

                            @if ($sent)
									@foreach($sent as $invitation)
                                        
                                        <!-- Box1 -->
                                        <div class="tracking_box" id="invitaion-rec-81">
                                            <div class="row">
                                                <div class="col-md-1">
                                                    
                                                <?php          
                                                        if(($invitation->user1->profilepic)!=null){ ?>
                                                                <img src="{{ url('uploads/profilepics/doctor/'.$invitation->user1->profilepic) }}" class="profile" style="border-radius:8px">
                                                        <?php
                                                        }else {
                                                        ?> <img src="{{ asset('img/no_image.jpg') }}" class="profile" style="border-radius:8px">
                                                <?php  } ?> 

                                                </div>
                                                <div class="col-md-4">
                                                    <div class="sub left_div">
                                                        <p><span class="name">{{ ucfirst($invitation->user1->name) }}  </span> </p>  
                                                        <p><span class="category">
                                                            <?php  $category = Helper::categorydetails();  ?>  
                                                            <?php  foreach ($category as $categories){ ?>
                                                                <?php if(($categories->id)==($invitation->user1->doctor->category_id)){echo ucfirst(strtolower($categories->category_name));} ?>
                                                            <?php } ?></span> 
                                                        </p>
                                                        <p><span class="category">
                                                            <?php   $city = Helper::cities();  ?>  
                                                            <?php  foreach ($city as $city1){ ?>
                                                                <?php if(($city1->id)==($invitation->user1->location)){echo ucfirst(strtolower($city1->city_name));} ?>
                                                            <?php } ?> </span></p> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="sub left_div">
                                                        <p><span class="category"><span>Appointment Date/Time: </span>  <img src="{{ url('img/calendar.png') }}" class="calendar_icon"> <span><?php echo date('d F Y', strtotime($invitation->invitation_date)); ?> |  {{ $invitation->timeslot }} </span></span>
                                                        <span class="virtualbold"><b><?php  if($invitation->appointment_type=='virtual') {echo " - Virtual";} ?></b></span>
                                                        </p>
                                                        <p><span style="">Client:</span> {{ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname)}}</p>
                                                        <p><span style="">Type:</span> @if($invitation->appointment_type == 'virtual') {{ucfirst($invitation->appointment_type)}} @else {{"In-Person"}} @endif </p>

                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <ul id="accordion{{$invitation->id}}" class="accordion1">
                                                        <div class="">
                                                            <div class="card-header1 collapsed" data-toggle="collapse" href="#collapse{{$invitation->id}}" aria-expanded="false">
                                                                <a style="text-decoration:none;" class="card-title"><li><span>Address: </span>{{ $invitation->user1->business_address }}</li></a>
                                                            </div>                              
                                                        </div>
                                                        <div id="collapse{{$invitation->id}}" class="card-body collapse" data-parent="#accordion{{$invitation->id}}" style="margin-top: -12px;width: 100%;">
                                                            <li><span>Designation: </span>{{ @$desgination[$invitation->user1->desgination] }}<br>
                                                            <span>License Number: </span>{{ $invitation->user1->doctor->licence_number }}<br>
                                                            <span>Introduction: </span>{{ $invitation->user1->doctor->introduction }}
                                                            </li>
                                                        </div>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Box1 -->

                                    @endforeach
                            @else
                            <div class="col-md-12">
                                <h4 style="text-align:center;" class="mt-4">No sent invitations.</h4>
                            </div>
                            @endif
                            </div>
                            <div class="tab-pane" id="accepted-invitations" role="tabpanel" aria-labelledby="nav-invitations-tab">
                                
                            @if ($accepted)
									@foreach($accepted as $invitation)

                                    @php
                                        $today = date("Y-m-d");
                                        $expire = $invitation->invitation_date;
                                        $today_time = strtotime($today);
                                        $expire_time = strtotime($expire);
                                        if ($expire_time < $today_time) {
                                            $pastStatus = 'disabled="disabled"';
                                        }
                                        else{
                                            $pastStatus = '';
                                        }
                                    @endphp
                                    
                                    <!-- Box1 -->
                                        <div class="tracking_box" id="invitaion-rec-81">
                                            <div class="row">
                                                <div class="col-md-1">
                                                <?php          
                                                        if(($invitation->user1->profilepic)!=null){ ?>
                                                                <img src="{{ url('uploads/profilepics/doctor/'.$invitation->user1->profilepic) }}" class="profile">
                                                        <?php
                                                        }else {
                                                        ?> <img src="{{ asset('img/user.jpg') }}" class="profile">
                                                        <?php  } ?> 
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="sub left_div">
                                                        <p><span class="name">{{ ucfirst($invitation->user1->name) }}  </span> </p>  
                                                        <p><span class="category">
                                                            <?php  $category = Helper::categorydetails();  ?>  
                                                            <?php  foreach ($category as $categories){ ?>
                                                                <?php if(($categories->id)==($invitation->user1->doctor->category_id)){echo ucfirst(strtolower($categories->category_name));} ?>
                                                            <?php } ?></span> 
                                                        </p>
                                                        <p><span class="category">
                                                            <?php   $city = Helper::cities();  ?>  
                                                            <?php  foreach ($city as $city1){ ?>
                                                                <?php if(($city1->id)==($invitation->user1->location)){echo ucfirst(strtolower($city1->city_name));} ?>
                                                            <?php } ?> </span></p> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="sub left_div">
                                                        <p><span class="category"><span>Appointment Date/Time: </span>  <img src="{{ url('img/calendar.png') }}" class="calendar_icon"> <span><?php echo date('d F Y', strtotime($invitation->invitation_date)); ?> |  {{ $invitation->timeslot }} </span>
                                                        </span>
                                                        <span class="virtualbold"><b><?php  if($invitation->appointment_type=='virtual') {echo " - Virtual";} ?></b></span>
                                                        </p>
                                                        <p><span style="">Client:</span>{{ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname)}}</p>
                                                        <p><span style="">Type:</span> @if($invitation->appointment_type == 'virtual') {{ucfirst($invitation->appointment_type)}} @else {{"In-Person"}} @endif </p>

                                                        @if(empty($pastStatus))
                                                          <p><a href="javascript:void(0)" id="resent-mail-patient{{$invitation->id}}" data-resent-mail-patient-id="{{ $invitation->id }}" class="req-resend-mail-patient-btn btn btn-success mr-2" style="margin-top:5px;">Resend Email to Patient</a></p>
                                                        @endif

                                                        @if($invitation->status == 'active' && $invitation->is_patient_accepted == 'no' && $invitation->reject_status == '1')
                                                           <span class="badge badge-danger">Request rejected by patient</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <ul id="accordionaccepted{{$invitation->id}}" class="accordion1">
                                                        <div class="">
                                                            <div class="card-header1 collapsed" data-toggle="collapse" href="#collapseaccepted{{$invitation->id}}" aria-expanded="false">
                                                                <a style="text-decoration:none;" class="card-title"><li><span>Address: </span>{{ $invitation->user1->business_address }}</li></a>
                                                            </div>                              
                                                        </div>
                                                        <div id="collapseaccepted{{$invitation->id}}" class="card-body collapse" data-parent="#accordionaccepted{{$invitation->id}}" style="margin-top: -12px;width: 100%;">
                                                            <li><span>Designation: </span>{{ @$desgination[$invitation->user1->desgination] }}<br>
                                                            <span>License Number: </span>{{ $invitation->user1->doctor->licence_number }}<br>
                                                            <span>Introduction: </span>{{ $invitation->user1->doctor->introduction }}
                                                            </li>
                                                        </div>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Box1 -->
                                @endforeach
                            @else
                            <div class="col-md-12">
                                <h4 style="text-align:center;" class="mt-4">No accepted invitations.</h4>
                            </div>
                            @endif
                            </div>
                            <div class="tab-pane" id="confirmed-invitations" role="tabpanel" aria-labelledby="nav-invitations-tab">
                                @if ($confirmed)
                                        @foreach($confirmed as $invitation)
                                            <!-- Box1 -->
                                        @php
                                            $today = date("Y-m-d");
                                            $expire = $invitation->invitation_date;
                                            $today_time = strtotime($today);
                                            $expire_time = strtotime($expire);
                                            if ($expire_time < $today_time) {
                                                $pastStatus = 'disabled="disabled"';
                                            }
                                            else{
                                                $pastStatus = '';
                                            }
                                        @endphp
                                        <div class="tracking_box" id="invitaion-rec-81">
                                            <div class="row">
                                                <div class="col-md-1">
                                                <?php          
                                                        if(($invitation->user1->profilepic) != null){ ?>
                                                                <img src="{{ url('uploads/profilepics/doctor/'.$invitation->user1->profilepic) }}" class="profile">
                                                        <?php
                                                        }else {
                                                        ?> <img src="{{ asset('img/user.jpg') }}" class="profile">
                                                        <?php  } ?> 
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="sub left_div">
                                                        <p><span class="name">{{ ucfirst($invitation->user1->name) }}  </span> 
                                                        <a href="{{url('lawyer-chat/'.$invitation->unique_id)}}" title="Chat" class="position-relative"><img src="{{ url('img/messenger.png') }}" class="chat_icon">
                                                        @if(Helper::un_read_messages(Auth::id(),$invitation->id))	
                                                            <label class="badge badge-warning notification_badge" style="left:inherit">{{ Helper::un_read_messages(Auth::id(),$invitation->id) }}</label>
                                                        @endif
                                                        </a>
                                                        </p>  
                                                        <p><span class="category">
                                                            <?php  $category = Helper::categorydetails();  ?>  
                                                            <?php  foreach ($category as $categories){ ?>
                                                                <?php if(($categories->id)==($invitation->user1->doctor->category_id)){echo ucfirst(strtolower($categories->category_name));} ?>
                                                            <?php } ?></span> 
                                                        </p>
                                                        <p><span class="category">
                                                            <?php   $city = Helper::cities();  ?>  
                                                            <?php  foreach ($city as $city1){ ?>
                                                                <?php if(($city1->id)==($invitation->user1->location)){echo ucfirst(strtolower($city1->city_name));} ?>
                                                            <?php } ?> </span></p>                                                         
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="sub left_div">
                                                        <p><span class="category"><span>Appointment Date/Time: </span>  <img src="{{ url('img/calendar.png') }}" class="calendar_icon"> <span><?php echo date('d F Y', strtotime($invitation->invitation_date)); ?> |  {{ $invitation->timeslot }} </span>
                                                         </span>
                                                        </p>
                                                        <p><span style="">Client:</span> {{ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname)}}</p>
                                                        <p><span style="">Type:</span> @if($invitation->appointment_type == 'virtual') {{ucfirst($invitation->appointment_type)}} @else {{"In-Person"}} @endif </p>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="margin-left: 90px;">
                                                    
                                                    
                                                     
                                                    <span id="transport-option-yes{{$invitation->id}}" class="disable_btn mr-2" style="background: #9e9e9e; color: #ffffff; padding: 5px 10px;line-height: 28px;position: relative;top: 4px; border-radius: 4px;display:@if($invitation->transportation == 'yes')inline-block @else none @endif" >Transportation  request sent</span>		
                                                    <a href="javascript:void(0)" id="transport-option-no{{$invitation->id}}" data-req-invit-id="{{ $invitation->id }}" class="@if(empty($pastStatus)) {{"req-transport-btn"}} @else {{"opctity"}}  @endif demosddd btn btn-info mr-2" style="margin-top:5px;display:@if($invitation->transportation == 'yes')none @else inline-block @endif"><i class="fa fa-bell"></i> 
                                                        Request Transportation
                                                     </a>											
                                                
                                                    <span id="translate-option-yes{{$invitation->id}}" class="disable_btn" style="background: #9e9e9e; color: #ffffff; padding: 5px 10px;line-height: 28px;position: relative;top: 4px; border-radius: 4px;display:@if($invitation->translation == 'yes')inline-block @else none @endif" class="mr-2">Translation request sent</span>		
                                                    <a href="javascript:void(0)" id="translate-option-no{{$invitation->id}}" data-req-invit-translate-id="{{ $invitation->id }}" class="@if(empty($pastStatus)) {{"req-translate-btn"}} @else {{"opctity"}} demosddd  @endif btn btn-info mr-2" style="margin-top:5px;display:@if($invitation->translation == 'yes')none @else inline-block @endif"><i class="fa fa-bell"></i> 
                                                        Request Translation
                                                    </a>   									
                                                    
                                                    <span id="amendment-option-yes{{$invitation->id}}" class="disable_btn" style="background: #9e9e9e; color: #ffffff; padding: 5px 10px;line-height: 28px;position: relative;top: 4px; border-radius: 4px;display:@if($invitation->amendment == 'yes')inline-block @else none @endif" class="mr-2">Amendment request sent</span>		
                                                    <a href="javascript:void(0)" id="amendment-option-no{{$invitation->id}}" data-req-invit-amendment-id="{{ $invitation->id }}" class="req-amendment-btn btn btn-info mr-2" style="margin-top:5px;display:@if($invitation->amendment == 'yes')none @else inline-block @endif"><i class="fa fa-bell"></i> 
                                                        Request Amendment
                                                    </a>
                                                   

                                                    <span id="cancel-appointmen-yes{{$invitation->id}}" class="disable_btn" style="background: #9e9e9e; color: #ffffff; padding: 5px 10px;line-height: 28px;position: relative;top: 4px; border-radius: 4px;display:@if($invitation->status == 'cancelled')inline-block @else none @endif" class="mr-2">Appointment Cancelled</span>
                                                    <a href="javascript:void(0)" id="cancel-appointmen-no{{$invitation->id}}" data-appointment-cancel-id="{{ $invitation->id }}" class="@if(empty($pastStatus)) {{"req-cancel-btn"}} @else {{"opctity"}}  @endif btn btn-danger mr-2" style="margin-top:5px;display:@if($invitation->status == 'cancelled')none @else inline-block @endif">
                                                        Cancel Appointment
                                                    </a>

                                                 </div>
                                                <div class="col-md-12">
                                                    <ul id="accordionconfirmed{{$invitation->id}}" class="accordion1">
                                                        <div class="">
                                                            <div class="card-header1 collapsed" data-toggle="collapse" href="#collapseconfirmed{{$invitation->id}}" aria-expanded="false">
                                                                <a style="text-decoration:none;" class="card-title"><li><span>Address: </span>{{ $invitation->user1->business_address }}</li></a>
                                                            </div>                              
                                                        </div>
                                                        <div id="collapseconfirmed{{$invitation->id}}" class="card-body collapse" data-parent="#accordionconfirmed{{$invitation->id}}" style="margin-top: -12px;width: 100%;">
                                                            <li><span>Designation: </span>{{ @$desgination[$invitation->user1->desgination] }}<br>
                                                            <span>License Number: </span>{{ $invitation->user1->doctor->licence_number }}<br>
                                                            <span>Introduction: </span>{{ $invitation->user1->doctor->introduction }}
                                                            </li>
                                                        </div>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Box1 -->
                                    @endforeach
                                @else
                                <div class="col-md-12">
                                    <h4 style="text-align:center;" class="mt-4">No confirmed invitations.</h4>
                                </div>
                                @endif
                            </div>
                            <div class="tab-pane" id="expired-invitations" role="tabpanel" aria-labelledby="nav-invitations-tab">
                                @if ($expired)
                                        @foreach($expired as $invitation)
                                    <!-- Box1 -->
                                        <div class="tracking_box" id="invitaion-rec-81">
                                            <div class="row">
                                                <div class="col-md-1">
                                                <?php          
                                                        if(($invitation->user1->profilepic)!=null){ ?>
                                                                <img src="{{ url('uploads/profilepics/doctor/'.$invitation->user1->profilepic) }}" class="profile">
                                                        <?php
                                                        }else {
                                                        ?> <img src="{{ asset('img/user.jpg') }}" class="profile">
                                                        <?php  } ?> 
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="sub left_div">
                                                        <p><span class="name">{{ ucfirst($invitation->user1->name) }}  </span> </p>  
                                                        <p><span class="category">
                                                            <?php  $category = Helper::categorydetails();  ?>  
                                                            <?php  foreach ($category as $categories){ ?>
                                                                <?php if(($categories->id)==($invitation->user1->doctor->category_id)){echo ucfirst(strtolower($categories->category_name));} ?>
                                                            <?php } ?></span> 
                                                        </p>
                                                        <p><span class="category">
                                                            <?php   $city = Helper::cities();  ?>  
                                                            <?php  foreach ($city as $city1){ ?>
                                                                <?php if(($city1->id)==($invitation->user1->location)){echo ucfirst(strtolower($city1->city_name));} ?>
                                                            <?php } ?> </span></p> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="sub left_div">
                                                        <p><span class="category"><span>Appointment Date/Time: </span>  <img src="{{ url('img/calendar.png') }}" class="calendar_icon"> <span><?php echo date('d F Y', strtotime($invitation->invitation_date)); ?> |  {{ $invitation->timeslot }} </span></span>
                                                        <span class="virtualbold"><b><?php  if($invitation->appointment_type=='virtual') {echo " - Virtual";} ?></b></span>
                                                        </p>
                                                        <p><span style="">Client:</span> {{ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname)}}</p>
                                                        <p><span style="">Type:</span> @if($invitation->appointment_type == 'virtual') {{ucfirst($invitation->appointment_type)}} @else {{"In-Person"}} @endif </p>

                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <ul id="accordionexpired{{$invitation->id}}" class="accordion1">
                                                        <div class="">
                                                            <div class="card-header1 collapsed" data-toggle="collapse" href="#collapseexpired{{$invitation->id}}" aria-expanded="false">
                                                                <a style="text-decoration:none;" class="card-title"><li><span>Address: </span>{{ $invitation->user1->business_address }}</li></a>
                                                            </div>                              
                                                        </div>
                                                        <div id="collapseexpired{{$invitation->id}}" class="card-body collapse" data-parent="#accordionexpired{{$invitation->id}}" style="margin-top: -12px;width: 100%;">
                                                            <li><span>Designation: </span>{{ @$desgination[$invitation->user1->desgination] }}<br>
                                                            <span>License Number: </span>{{ $invitation->user1->doctor->licence_number }}<br>
                                                            <span>Introduction: </span>{{ $invitation->user1->doctor->introduction }}
                                                            </li>
                                                        </div>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Box1 -->
                                    @endforeach
                                @else
                                <div class="col-md-12">
                                    <h4 style="text-align:center;" class="mt-4">No expired invitations.</h4>
                                </div>
                                @endif
                            </div>
                            <div class="tab-pane" id="rejected-invitations" role="tabpanel" aria-labelledby="nav-invitations-tab">
                                 @if ($rejected)
                                        @foreach($rejected as $invitation)
                                            <!-- Box1 -->
                                        <div class="tracking_box" id="invitaion-rec-81">
                                            <div class="row">
                                                <div class="col-md-1">
                                                <?php          
                                                        if(($invitation->user1->profilepic)!=null){ ?>
                                                                <img src="{{ url('uploads/profilepics/doctor/'.$invitation->user1->profilepic) }}" class="profile">
                                                        <?php
                                                        }else {
                                                        ?> <img src="{{ asset('img/user.jpg') }}" class="profile">
                                                        <?php  } ?> 
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="sub left_div">
                                                        <p><span class="name">{{ ucfirst($invitation->user1->name) }}  </span> </p>  
                                                        <p><span class="category">
                                                            <?php  $category = Helper::categorydetails();  ?>  
                                                            <?php  foreach ($category as $categories){ ?>
                                                                <?php if(($categories->id)==($invitation->user1->doctor->category_id)){echo ucfirst(strtolower($categories->category_name));} ?>
                                                            <?php } ?></span> 
                                                        </p>
                                                        <p><span class="category">
                                                            <?php   $city = Helper::cities();  ?>  
                                                            <?php  foreach ($city as $city1){ ?>
                                                                <?php if(($city1->id)==($invitation->user1->location)){echo ucfirst(strtolower($city1->city_name));} ?>
                                                            <?php } ?> </span></p> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="sub left_div">
                                                        <p><span class="category"><span>Appointment Date/Time: </span>  <img src="{{ url('img/calendar.png') }}" class="calendar_icon"> <span><?php echo date('d F Y', strtotime($invitation->invitation_date)); ?> |  {{ $invitation->timeslot }} </span></span>
                                                        <span class="virtualbold"><b><?php  if($invitation->appointment_type=='virtual') {echo " - Virtual";} ?></b></span>
                                                        </p>
                                                        <p><span style="">Client:</span> {{ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname)}}</p>
                                                        <p><span style="">Type:</span> @if($invitation->appointment_type == 'virtual') {{ucfirst($invitation->appointment_type)}} @else {{"In-Person"}} @endif </p>

                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <ul id="accordionrejected{{$invitation->id}}" class="accordion1">
                                                        <div class="">
                                                            <div class="card-header1 collapsed" data-toggle="collapse" href="#collapserejected{{$invitation->id}}" aria-expanded="false">
                                                                <a style="text-decoration:none;" class="card-title"><li><span>Address: </span>{{ $invitation->user1->business_address }}</li></a>
                                                            </div>                              
                                                        </div>
                                                        <div id="collapserejected{{$invitation->id}}" class="card-body collapse" data-parent="#accordionrejected{{$invitation->id}}" style="margin-top: -12px;width: 100%;">
                                                            <li><span>Designation: </span>{{ @$desgination[$invitation->user1->desgination] }}<br>
                                                            <span>License Number: </span>{{ $invitation->user1->doctor->licence_number }}<br>
                                                            <span>Introduction: </span>{{ $invitation->user1->doctor->introduction }}
                                                            </li>
                                                        </div>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Box1 -->
                                    @endforeach
                                @else
                                <div class="col-md-12">
                                    <h4 style="text-align:center;" class="mt-4">No rejected invitations.</h4>
                                </div>
                                @endif
                            </div>
                            <div class="tab-pane" id="cancelled-invitations" role="tabpanel" aria-labelledby="nav-cancelled-tab">
                                 @if ($cancelled)
                                        @foreach($cancelled as $invitation)
                                            <!-- Box1 -->
                                        <div class="tracking_box" id="invitaion-rec-81">
                                            <div class="row">
                                                <div class="col-md-1">
                                                <?php          
                                                        if(($invitation->user1->profilepic)!=null){ ?>
                                                                <img src="{{ url('uploads/profilepics/doctor/'.$invitation->user1->profilepic) }}" class="profile">
                                                        <?php
                                                        }else {
                                                        ?> <img src="{{ asset('img/user.jpg') }}" class="profile">
                                                        <?php  } ?> 
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="sub left_div">
                                                        <p><span class="name">{{ ucfirst($invitation->user1->name) }}  </span> </p>  
                                                        <p><span class="category">
                                                            <?php  $category = Helper::categorydetails();  ?>  
                                                            <?php  foreach ($category as $categories){ ?>
                                                                <?php if(($categories->id)==($invitation->user1->doctor->category_id)){echo ucfirst(strtolower($categories->category_name));} ?>
                                                            <?php } ?></span> 
                                                        </p>
                                                        <p><span class="category">
                                                            <?php   $city = Helper::cities();  ?>  
                                                            <?php  foreach ($city as $city1){ ?>
                                                                <?php if(($city1->id)==($invitation->user1->location)){echo ucfirst(strtolower($city1->city_name));} ?>
                                                            <?php } ?> </span></p> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="sub left_div">
                                                        <p><span class="category"><span>Appointment Date/Time: </span>  <img src="{{ url('img/calendar.png') }}" class="calendar_icon"> <span><?php echo date('d F Y', strtotime($invitation->invitation_date)); ?> |  {{ $invitation->timeslot }} </span></span>
                                                        <span class="virtualbold"><b><?php  if($invitation->appointment_type=='virtual') {echo " - Virtual";} ?></b></span>
                                                        </p>
                                                        <p><span style="">Client:</span> {{ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname)}}</p>
                                                        <p><span style="">Type:</span> @if($invitation->appointment_type == 'virtual') {{ucfirst($invitation->appointment_type)}} @else {{"In-Person"}} @endif </p>

                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <ul id="accordionrejected{{$invitation->id}}" class="accordion1">
                                                        <div class="">
                                                            <div class="card-header1 collapsed" data-toggle="collapse" href="#collapserejected{{$invitation->id}}" aria-expanded="false">
                                                                <a style="text-decoration:none;" class="card-title"><li><span>Address: </span>{{ $invitation->user1->business_address }}</li></a>
                                                            </div>                              
                                                        </div>
                                                        <div id="collapserejected{{$invitation->id}}" class="card-body collapse" data-parent="#accordionrejected{{$invitation->id}}" style="margin-top: -12px;width: 100%;">
                                                            <li><span>Designation: </span>{{ @$desgination[$invitation->user1->desgination] }}<br>
                                                            <span>License Number: </span>{{ $invitation->user1->doctor->licence_number }}<br>
                                                            <span>Introduction: </span>{{ $invitation->user1->doctor->introduction }}
                                                            </li>
                                                        </div>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Box1 -->
                                    @endforeach
                                @else
                                <div class="col-md-12">
                                    <h4 style="text-align:center;" class="mt-4">No cancelled invitations.</h4>
                                </div>
                                @endif
                            </div>
                         </div> 
                    </div>
			         <!-- =========== 16-07-2021 =========== -->
                </div>                                                                    
                <!---end tracking--->

                <!---start---->
					<div class="modal fade" id="scss_cancel_appointment" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
					
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content login_box">
                        <div class="modal-body">		
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        <div class="row">
                              <div class="col-md-3 text-center m-auto">
                                  <img id="modal-inv-succes-img" src="{{ asset('img/checkmark.svg') }}" class="w-100" alt="success" />				
                              </div>
                        </div>
                        <h5 class="modal-title"  style="color:green;text-align: center;" id="acceptscss">Appointment Cancelled Successfully</h5>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                  <!----end----->

                  <!---start---->
					<div class="modal fade" id="patient_resend_mail_modal" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
					
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content login_box">
                        <div class="modal-body">		
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        <div class="row">
                              <div class="col-md-3 text-center m-auto">
                                  <img id="modal-inv-succes-img" src="{{ asset('img/checkmark.svg') }}" class="w-100" alt="success" />				
                              </div>
                        </div>
                        <h5 class="modal-title"  style="color:green;text-align: center;" id="acceptscss">Resend Email to Client Successfully</h5>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                  <!----end----->
            </div>
        </div>
    </section>
@endsection

@section('footerscripts')
<script>

    display_ajax_doctors_list(null,{},0); // for doctor
    display_ajax_doctors_list(null,{},1);  // for clinic

    function display_ajax_doctors_list(Link=null,params={},types = 0)
    {
        if(Object.getOwnPropertyNames(params).length === 0){
           params ="types="+types;
        }
        else{
            params +="&types="+types;  
        }
    
        var redirectLink = Link ? Link :'{{ url("lawyer/ajax-search-doctors-listing") }}';
        $.ajax({
            url:redirectLink,
            type:'get',
            data:params,
            dataType:'json',
            success:function(res) {
                if(types == 0){
                    $('#search-doctors-loading-img').hide();
                }else{
                    $('#search-clinic-loading-img').hide();
                }
               
                if(res.status == 'success') {
                    if(types == 0){
                        $('#search-doctors-ajax-list').html(res.data);
                    }else{
                        $('#search-clinic-ajax-list').html(res.data);
                    }
                }
            }

        })     
    }
  
$('#search-category_id').change(function() {
        var params = $(this).serialize();
        //console.log($('#search-location').val());
        console.log(params);
        if($('#search-location').val()){
            params=params+'&location='+$('#search-location').val()
        }
        if($('#price_range').val()){
            params=params+'&price_range='+$('#price_range').val()
        }
        $('#search-doctors-loading-img').show();
        display_ajax_doctors_list(null,params,0);   
 });

$('#search-clinic-category_id').change(function() {
        var params = $(this).serialize();
        if($('#search-clinic-location').val()){
            params=params+'&location='+$('#search-clinic-location').val()
        }

        if($('#price_clinic_range').val()){
            params=params+'&price_range='+$('#price_clinic_range').val()
        }

        $('#search-clinic-loading-img').show();
        console.log(params)
        display_ajax_doctors_list(null,params,1);   
 });

 
$('#cliniclist').change(function() {
        var params = $(this).serialize();
        if($('#search-clinic-location').val()){
            params=params+'&location='+$('#search-clinic-location').val()
        }
        if($('#price_clinic_range').val()){
            params=params+'&price_range='+$('#price_clinic_range').val()
        }
        $('#search-clinic-loading-img').show();
        display_ajax_doctors_list(null,params,1);   
 });


$('#search-location').change(function() {
   // e.preventDefault();
    var params =$(this).serialize();
    if($('#search-category_id').val()){
            params=params+'&category_id='+$('#search-category_id').val()
        } 
     if($('#price_range').val()){
            params=params+'&price_range='+$('#price_range').val()
        } 

    $('#search-doctors-loading-img').show();
    display_ajax_doctors_list(null,params,0);
 });

$('#search-clinic-location').change(function() {
   // e.preventDefault();
    var params =$(this).serialize();
    if($('#search-clinic-category_id').val()){
            params=params+'&category_id='+$('#search-clinic-category_id').val()
        } 
     if($('#price_clinic_range').val()){
            params=params+'&price_range='+$('#price_clinic_range').val()
        } 

    $('#search-clinic-loading-img').show();
    display_ajax_doctors_list(null,params,1);
 });


$('#price_range').change(function() {
   // e.preventDefault();
    var params =$(this).serialize();
        
    if($('#search-category_id').val()){
        params=params+'&category_id='+$('#search-category_id').val()
    }

    if($('#search-location').val()){
            params=params+'&location='+$('#search-location').val()
    }    

    $('#search-doctors-loading-img').show();
    display_ajax_doctors_list(null,params);
 });

$('#price_clinic_range').change(function() {
    var params =$(this).serialize();
    if($('#search-clinic-category_id').val()){
        params=params+'&category_id='+$('#search-clinic-category_id').val()
    }

    if($('#search-clinic-location').val()){
            params=params+'&location='+$('#search-clinic-location').val()
    }    

    $('#search-clinic-loading-img').show();
    display_ajax_doctors_list(null,params,1);
 });



$('#search-doctors-form').submit(function(e){
    e.preventDefault();
    var params =$(this).serialize();      
    $('#search-doctors-loading-img').show();
    display_ajax_doctors_list(null,$(this).serialize());
})

    $('body').on('click','#search-doctors-view-all-btn', function(e){
        $('#search-doctors-form')[0].reset(); 
        display_ajax_doctors_list();
    })

    $('body').on('click','#search-doctors-list-pagination .pagination a', function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            display_ajax_doctors_list(url);
            return false;
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("id")
            var activeTab = target.replace('#','');
            var baseURL = '{{url("/lawyer-dashboard") }}'; 
            window.history.pushState({}, null, baseURL+'/'+activeTab);
        }); 

</script>   

@isset($selectedTab)
@if ($selectedTab > 0)
    <script>
         var item = $("#myTab_"+"{{$selectedTab}}");
         $(".mytbslnk").removeClass("active");
        // console.log(item)
         item.click();
        // item.addClass("active");
    </script>
@endif
@endisset

@endsection