@extends('layouts.doctor-profile-custom')

@section('content')	
<link href="{{ asset('css/mail-patient-appoint.css') }}" rel="stylesheet">


{{-- @if($type != 'rejected')
	 <!-- Banner Section -->
    <section class="intro_banner_sec inner_banner_sec" style="padding: 20px 0px;">

        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="welcome_text">

                    @if($type == 'expired')
                        <h1>Page Expired</h1>
                    @else
                         @if ($type == 'rejected')
                            <h2>Change Date Request</h2>
                         @else
                           <h1>Appointment {{ ucfirst($type) }}</h1>
                         @endif
                    @endif

                    </div>
                </div>
            </div>

</section>
@endif --}}


	<section class="dashboard_sec">

        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    @if($type== 'rejected')                 
                     <div class="content">
                        <h2 class="ribbon">Change Date Request</h2>

                        <form class="mt-4">

                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="rds" id="rds1" value="1" checked>
                            <label class="form-check-label radioLab" for="rds1">
                              Option 1
                            </label>
                          </div>

                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="rds" id="rds2" value="2">
                            <label class="form-check-label radioLab" for="rds2">
                              Option 2
                            </label>
                          </div>

                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="rds" id="rds3" value="3">
                            <label class="form-check-label radioLab" for="rds3">
                              Option 3
                            </label>
                          </div>

                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="rds" id="rds4" value="4">
                            <label class="form-check-label radioLab" for="rds4">
                              Other
                            </label>
                          </div>
                                                                   

                            <div class="form-group mt-3" id="desContainer" style="display: none">
                              <label for="" style="font-weight:bold;">Description</label>
                              <input id="invRegID" type="hidden" value="{{$invitation->id}}">
                              <textarea maxlength="5000" class="form-control" id="rejectDetails" rows="5"></textarea>
                            </div>

                            <div id="rej_ScMessage" style="display: none" class="alert alert-success" role="alert">
                                A simple success alert—check it out!
                              </div>
                            <div id="rej_ErMessage" style="display: none" class="alert alert-danger" role="alert">
                                A simple danger alert—check it out!
                            </div>
                              
                           
                            <div class="mt-3 mb-3" style="display: flex; align-items:center;">
                                <button id="rejectBtn" type="button" class="btn btn-primary" style="margin-bottom: 0px;">Submit</button>
                                <img id="rejectLoading" class="ml-3" style="display: none;" width="40" src="{{asset('img/loading.gif')}}" alt="">
                            </div>
                        </form>
                        
                      </div>

                    @endif

                    @if($type == 'accepted')
                      <div class="jumbotron text-success">
                        <h4 class="display-5 text-danger">Your appointment has been confirmed.</h4>
                      </div>
                    @elseif($type == 'submit')
                        <div class="jumbotron text-success">
                          <h4 class="display-5 text-success">Your have already submitted required to admin.</h4>
                        </div>
                    @elseif($type == 'expired')
                      <div class="jumbotron">
                        <h4 class="display-5 text-danger">Sorry! This page was expired.</h4>
                      </div>
                    @endif

                </div>
            </div>
        </div>		
    </section>
</div>
@endsection