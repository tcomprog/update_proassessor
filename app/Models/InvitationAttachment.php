<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvitationAttachment extends Model
{
    use HasFactory;
    protected $guarded = [];

    protected $table = 'invitation_attachments';

    public function invitation()
    {
        return $this->belongsTo(user_invitations::class);
    }
}
