<!-- Scripts -->


<script src="{{ asset('js/app.js') }}" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" ></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

<!--added-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/js/standalone/selectize.js"></script><!--select--> 


{{-- update private information --}}

<script>
	$.validator.addMethod('numericOnly', function (value) {
       return /^[0-9]+$/.test(value);
	}, 'Please enter only numeric values (0-9)');

    $("#update_profile_doctor22").validate({  
                        rules: {
                        business_address: {
                           required: true
                        },   
                        postcode: {
                           required: false,
                        },   
                        phone:{
                            required:true
                        },
                        licenence_number:{
                            required:true
                        },
                        password:{
                            required:false,
                            minlength: 8
                        },
                        confirm_password:{
                            required:false,
                            equalTo: "#password"
                        },				
                    },
                    messages: {
                     
                        business_address: {
                           required: "Please enter address"
                        },     
                        phone:{
                                required:"Please Enter  phone"
                        },
                        licenence_number:{
                            required:"Please Enter Licence Number"
                        },	
                        password:{
                            required:"Please Enter  password"
                        },
                        confirm_password:{
                            required:"Please Enter  Confirm password"
                        },
                    },
            submitHandler: function(form) {
                $("#update_profile_doctor_btn22"). attr("disabled", true);
               // $('#lawyeremailexists').text('');   
               $('#lawyerinvalidformat').text('');   

         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
           
            var formData = new FormData($('#update_profile_doctor22')[0]);
          //  return false;
            
            $.ajax
            ({
               url: "{{url('updateprivateinfo')}}",
               type: 'POST',              
               data: formData,
               dataType:'json',
               beforeSend: function() {
				 $("#sendInvoiceLoading22").show();
				 $('#update_profile_doctor_btn22').html('Please Wait');
			   },
               processData: false,
               contentType: false,
               success: function( response ) {
                    $('#update_profile_doctor_btn').html('Submit');
                     if(response.status == 'error'){                            
                            $("#update_profile_doctor_btn"). attr("disabled", false);
                           
							 if (typeof response.message.cvimage[0] === "string" ){ 
								var cvinvalidformats = "Allowed Formats Jpg, Jpeg, Png, doc, pdf & Max Size 1Mb";
								$('#cvlawyerinvalidformat').text(cvinvalidformats);
							}     
							 if (typeof response.message.image[0] === "string" ){                                
								var invalidformats = "Allowed Formats Jpg, Jpeg, Png & Max Size 1Mb";
								$('#lawyerinvalidformat').text(invalidformats);
							} 
                                                               
                     }
                   
                    if(response.status == 'success'){                       
                        document.getElementById('updatesuccess22').style.display = "block";
                        $('body,html').animate({scrollTop:0},800);
                        $("#update_profile_doctor_btn22"). attr("disabled", false);
                        $("#update_profile_doctor_btn22").text("Update");
                        // window.location.href=response.url;
                        // setTimeout(function () {
                        //     location.reload(true);
                        // }, 5000);
                    }

					$("#sendInvoiceLoading22").hide();
                }
            });
            }
        });
</script>


<!--------------update doctor profile------------------------>

<script>    
	$.validator.addMethod('numericOnly', function (value) {
       return /^[0-9]+$/.test(value);
	}, 'Please enter only numeric values (0-9)');

    $("#update_profile_doctor").validate({  
                        rules: {
                        name: {
                            required: true
                        },
                        assessmentOnlyCharges: {
                            required: true,
							number: true,
                        },
                        lateCancellationFee: {
                            required: true,
							number: true,
                        },
                        business_address: {
                           required: true
                        },  
                        location:{
                           required:true
                        },  
                        phone:{
                            required:true
                        },
                        licenence_number:{
                            required:true
                        },
                        
                        introduction:{
                            required:false
                        },
                        password:{
                            required:false,
                            minlength: 8
                        },
                        confirm_password:{
                            required:false,
                            equalTo: "#password"
                        },	
                        price: {
                        	required: true,
                        	number: true,
                        	numericOnly:true,
							min:50
                   		 },			
                    },
                    messages: {
                        name: {
                            required: "Please enter name"
                        },
						assessmentOnlyCharges: {
                            required: "Please enter assessment charges",
							number: "You can only enter numeric value",
                        	numericOnly:"Invalid valid enter valid number",
                        },
                        lateCancellationFee: {
                            required: "Please enter late cancellation",
							number: "You can only enter numeric value",
                        	numericOnly:"Invalid valid enter valid number",
                        },
                    
                        business_address: {
                             required: "Please enter address"
                        },   
                        location:{
                                    required:"Please Enter location"
                                },  
                        phone:{
                                required:"Please Enter  phone"
                            },
                        licenence_number:{
                            required:"Please Enter Licence Number"
                        },	
                        introduction:{
                            required:"Please Enter  Introduction"
                        },
                        password:{
                            required:"Please Enter  password"
                        },
                        confirm_password:{
                            required:"Please Enter  Confirm password"
                        },
                        price: {
                       		 required: "Please Enter Price",
                        	 number: "PLease Enter Number",
							 min:"Price should be greater then 0"
                   		 },
                    },
            submitHandler: function(form) {
                $("#update_profile_doctor_btn"). attr("disabled", true);
               // $('#lawyeremailexists').text('');   
               $('#lawyerinvalidformat').text('');   

                $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
           
            var formData = new FormData($('#update_profile_doctor')[0]);
          //  return false;
            
            $.ajax
            ({
                url: "{{url('updatedoctor')}}",
                type: 'POST',              
               data: formData,
               dataType:'json',
               beforeSend: function() {$('#update_profile_doctor_btn').html('Please Wait');},
               processData: false,
               contentType: false,
               success: function( response ) {
                    $('#update_profile_doctor_btn').html('Submit');
                     if(response.status == 'error'){                            
                            $("#update_profile_doctor_btn"). attr("disabled", false);
                            //alert('hii');
                            console.log(response.message); 
							
							 if (typeof response.message.cvimage[0] === "string" ){ 
								var cvinvalidformats = "Allowed Formats Jpg, Jpeg, Png, doc, pdf & Max Size 1Mb";
								//alert('cv');
								$('#cvlawyerinvalidformat').text(cvinvalidformats);
							}     
							 if (typeof response.message.image[0] === "string" ){                                
								var invalidformats = "Allowed Formats Jpg, Jpeg, Png & Max Size 1Mb";
								//alert('hii');
								$('#lawyerinvalidformat').text(invalidformats);
							} 
                           /* if (typeof response.message.email[0] === "string"){                               
                                var emailexists1 = "Email Already Exists";
                                $('#lawyeremailexists').text(emailexists1);
                             }   */                       
                                            
                         }
                    //console.log(response);
                    if(response.status == 'success'){                       
                        document.getElementById('updatesuccess').style.display = "block";
                        $('body,html').animate({scrollTop:0},800);
                        $("#update_profile_doctor_btn"). attr("disabled", false);
                       /// window.location.href=response.url;
                        setTimeout(function () {
                            location.reload(true);
                        }, 2000);
                    }
                }/*,
                error:function (response) {
                   console.log(response);
                }*/
            });
            }
        });
</script>

<!------delete pic------->
<script>	
	$("#removeprofilepic").click(function(){
			//alert("test");
            $('#deleteprofilepicdiv').modal('show');
			document.getElementById('successtask1').style.display = "none";
			$("#deleteprofilepic_form").validate({
				rules: {
					userid: {
									required: false
								},
				},
				messages: {
					userid: {
									required: "Please Date"
								},
				},
				submitHandler: function(form) {
				$("#deleteprofilepic_btn"). attr("disabled", true);
				$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
				$.ajax({
				url: "{{url('deleteprofilepic')}}",
				type: "POST",
				data: $('#deleteprofilepic_form').serialize(),
				dataType:'json',
				beforeSend: function() {$('#deleteprofilepic_btn').html('Please Wait');},
				success: function( response ) {
				$('#deleteprofilepic_btn').html('Submit');	
				   console.log(response.res);
					if(response.status == 'success'){
						console.log(response.res);
						$("#deleteprofilepic_btn"). attr("disabled", false);
						document.getElementById('successtask1').style.display = "block"; 
						document.getElementById("deleteprofilepic_form").reset();
						 window.location.href=response.url;
							//location.reload();
							setTimeout(function () {
								location.reload(true);
							  }, 2000); 
					}					
				}
				});
				}
				})
			/**/			
		  });  

</script>

<!-------upload reports-------->
<script type="text/javascript">
	$(document).ready(function (e) {
	$.ajaxSetup({
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
	});


function uploadMedicalReports(curret,total){
   let totalFiles = $('#files')[0].files.length; //Total files
   let files = $('#files')[0];
   var filenames = $('#files')[0].files;

	var progress = `<div  class="progress mt-2"><div id="fprog_l_${curret}" class="progress-bar" style='background:#323B62' role="progressbar" style="width: 5%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">0% </div> </div><p>${filenames[curret]["name"]}</p>`;
	$('#progress_DoctorReport').append(progress);
    
	var newintid = $("#upload-file-invitation-id").val();
    var patientid = $("#upload-file-patient-id").val();

    var formData = new FormData();
	formData.append('newintid', newintid);
	formData.append('patientid', patientid);
	formData.append('total', total);
	formData.append('current', curret);
	formData.append('file', files.files[curret]);

	$.ajax({
			xhr: function() {
			    var xhr = new window.XMLHttpRequest();
				xhr.onreadystatechange = function () {
                    var rdState = xhr.readyState
                    var progress = 25 * rdState;
                    $("#fprog_l_"+curret).css('width',progress+"%");
                    $("#fprog_l_"+curret).text(progress+"%");
                }
				return xhr;
			},
			type:'POST',
			url: "{{ url('/uploadreports')}}",
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: (data) => {
				if(curret+1 < total){
					uploadMedicalReports(curret+1,total)
				}
				else{
					$("#files").val(null);
					var url = "{{url()->full()}}";
					url = url.replace('&amp;','');
					if(url.includes('tab')){
					url = url.split("tab")[0];
					}
					url += "?tab=3";
					location.replace(url);
				}
			},
			error: function(data){
				$('#progress_DoctorReport').empty();
			    $("#error_reportUpload").show();
				$("#error_reportUpload").text('Error occured while uploading files, try again');
				$("#files").val(null);
				$("#submit_Questionair").show();

				setTimeout(() => {
					$("#error_reportUpload").hide();
				}, 5000);
			
			}
	});
}


	$("#reportSubmit").on('click',function(){
		$("#reportSubmit").hide();
		$("#error_reportUpload").hide();
        $("#progress_DoctorReport").empty();

		let totalFiles = $('#files')[0].files.length; //Total files
        let files = $('#files')[0];
        var filenames = $('#files')[0].files;

		if(totalFiles > 0){
			const allowed = [
				"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
				"application/msword",
				"application/pdf",
			];

			var error = false;

			// --- File type check start
			for(let i = 0; i < totalFiles; i++){
				if(!allowed.includes(files.files[i].type)){
					$("#error_reportUpload").show();
					$("#error_reportUpload").text('Invalid file type, You can only upload pdf, doc, docx');
					$("#files").val(null); // to clear all select files
					setTimeout(() => {
					$("#error_reportUpload").hide();
				}, 5000);
					error = true;
					break;
				}else{
					error = false;
				}
			}
			// --- File type check end

		   if(error == false){
			    uploadMedicalReports(0,totalFiles);
				$('#reportSubmit').hide();
			}else{
				$("#reportSubmit").show();
			}
		}

	});


	$('#upload_form').submit(function(e) {	
	e.preventDefault();
	$("#submit"). attr("disabled", true);
	var formData = new FormData(this);
	let TotalFiles = $('#files')[0].files.length; //Total files
	let files = $('#files')[0];
	for (let i = 0; i < TotalFiles; i++) {
	formData.append('files' + i, files.files[i]);
	}
	formData.append('TotalFiles', TotalFiles);
	$.ajax({
	type:'POST',
	url: "{{ url('/uploadreports')}}",
	data: formData,
	cache:false,
	contentType: false,
	processData: false,
	dataType: 'json',
	beforeSend: function() {$('#submit').html('Please Wait');},
	success: (data) => {
			$('#upload-error').text();
			$('#upload-scss').text();
			this.reset();
			//alert('Files has been uploaded using jQuery ajax');
			$('#submit').html('Submit');
			console.log(data);
			//console.log(data.success);
			console.log(data.url);
			var fscss=data.success;
			location.reload();
			$('#upload-scss').text(fscss);
			$('#reports-ul'+data.reportsid).append(data.reports);
			document.getElementById("upload_form").reset();
			$("#submit").attr("disabled", false);
	},
	error: function(data){
	if (typeof data.responseJSON.message === "string"){
		var ferror= "Files must be pdf, doc, docx and size not more than 1 MB";  
			$('#upload-error').text(ferror);
	}
	$("#submit"). attr("disabled", false);
	$('#submit').html('Submit');
	}
	});
	});
	});
</script>

<!----click id of delete docs--->
<script type="text/javascript">
   function reply_click1(clicked_id, dataname,activeli)
  {
	// console.log('clicked_id => ',clicked_id,"  dataname=> ",dataname," activeli => ",activeli);
	// return;
	//$('#dlte_scss').hide();
	$('#reports-btn-div').show();	
     //alert(activeli);
	  var intdid=clicked_id;	  
	  var intdname=dataname; 
	/// alert(intdname);	  
	  $('#DeleteDocsid').val(intdid);
	  $('#DeleteDocsname').val(intdname);
	  $('#DeleteActiveli').val(activeli);
	  $('#DeleteDocs').modal('show');
  }
</script> 


<!------Remove Report------->
<script>	
	$("#deletedoc_form #deletedoc_form_btn").click(function(e){
		//alert("test");
			$("#deletedoc_form_btn"). attr("disabled", false);
			//console.log();
			//document.getElementById('dlte_scss').style.display = "none";
			$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
				$.ajax({
				url: "{{url('deletereports')}}",
				type: "POST",
				data: $('#deletedoc_form').serialize(),
				dataType:'json',
				beforeSend: function() {$('#deletedoc_form_btn').html('Please Wait');},
				success: function( response ) {
				$('#deletedoc_form_btn').html('Submit');	
				
					if(response.status == 'success'){
						document.getElementById('dlte_scss'+response.int_id).style.display = "block";
						$('#DeleteDocs').modal('hide');
                        showAlertAfterDelete();
						$('#'+response.activeliid).hide();
							$('#reports-btn-div').hide();
							setTimeout(function () {								
								document.getElementById('dlte_scss'+response.int_id).style.display = "none";
							}, 2000);
						$("#deletedoc_form_btn"). attr("disabled", false);						 
						document.getElementById("deletedoc_form").reset();
						
					}					
				}
				});
				
			/**/	
					
		  });  

</script>

<!------delete cv------->
<script>	
	$("#removecv").click(function(){
			//alert("test");
        $('#removecvdiv').modal('show');
						
		  });  
	$("#deletecv_btn").click(function(e){
			e.preventDefault();
			document.getElementById('delscsstask').style.display = "none";			
			$("#deletecv_btn"). attr("disabled", false);
			$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
				$.ajax({
				url: "{{url('deletecv')}}",
				type: "POST",
				data: $('#deletecv_form').serialize(),
				dataType:'json',
				beforeSend: function() {$('#deletecv_btn').html('Please Wait');},
				success: function( response ) {
				$('#deletecv_btn').html('Submit');	
				   console.log(response);
					if(response.status == 'success'){
						//console.log(response.res);
						$("#deletecv_btn"). attr("disabled", false);
						document.getElementById('delscsstask').style.display = "block"; 
						document.getElementById("deletecv_form").reset();
						// window.location.href=response.url;
							//location.reload();
							setTimeout(function () {
								location.reload(true);
							  }, 2000); 
					}				
				}
				});
				});
	
</script>

<script type="text/javascript">
 /*active nav bar*/
  $(document).ready(function(){
	  var currentURL = $(location).attr('href');
	  if(currentURL.indexOf('#Reports') != -1){
			$('.nav-tabs a.active').removeClass('active')
			$('.tab-content div.active').removeClass('active')
			$('.tab-content div.show').removeClass('show')
			
			//$(this).addClass('active');
			$('#filesreports-tab').addClass('active')
			$('#nav-filesreports').addClass('active')
			$('#nav-filesreports').addClass('show')
			}				
	})
</script> 

<!------delete instruction form------->
<script>	
	$("#removeinsform").click(function(){
	    // alert("test");
	    $('#removeinsdiv').modal('show');				
	 }); 	
	 
	 $("#deleteins_btn").click(function(e){
		e.preventDefault();
		document.getElementById('delscssins').style.display = "none";			
		$("#deleteins_btn"). attr("disabled", false);
		$.ajaxSetup({
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
			});
			$.ajax({
			url: "{{url('deleteInsform')}}",
			type: "POST",
			data: $('#deleteins_form').serialize(),
			dataType:'json',
			beforeSend: function() {$('#deleteins_btn').html('Please Wait');},
			success: function( response ) {
			$('#deleteins_btn').html('Submit');	
			   console.log(response);
				if(response.status == 'success'){
					//console.log(response.res);
					$("#deleteins_btn"). attr("disabled", false);
					document.getElementById('delscssins').style.display = "block"; 
					document.getElementById("deleteins_form").reset();
					 window.location.href=response.url;
						//location.reload();
						setTimeout(function () {
							location.reload(true);
						  }, 2000); 
				}				
			}
			});
			});
</script>

<!-------upload invice-------->
<script type="text/javascript">
	$(document).ready(function (e) {
	$.ajaxSetup({
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
	});
	$('#upload_invoice_form').submit(function(e) {	
	e.preventDefault();
	$("#submit"). attr("disabled", true);
	var formData = new FormData(this);
	let TotalFiles = $('#invoicefiles')[0].files.length; //Total files
	let files = $('#invoicefiles')[0];
	for (let i = 0; i < TotalFiles; i++) {
	formData.append('files' + i, files.files[i]);
	}
	formData.append('TotalFiles', TotalFiles);
	$.ajax({
	type:'POST',
	url: "{{ url('/uploadinvoice')}}",
	data: formData,
	cache:false,
	contentType: false,
	processData: false,
	dataType: 'json',
	beforeSend: function() {$('#submit').html('Please Wait');},
	success: (data) => {
	$('#invoice-upload-error').text();
	$('#invoice-upload-scss').text();
	this.reset();
	//alert('Files has been uploaded using jQuery ajax');
	$('#submit').html('Submit');
	console.log(data);
	//console.log(data.success);
	console.log(data.url);
	var fscss=data.success;
	$('#invoice-upload-scss').text(fscss);
	setTimeout(function () {
		location.reload(true);
	  }, 1000);
	},
	error: function(data){
	if (typeof data.responseJSON.message === "string"){
		var ferror= "Files must be pdf, doc, docx and size not more than 1 MB";  
			$('#invoice-upload-error').text(ferror);
	}
	$("#submit"). attr("disabled", false);
	$('#submit').html('Submit');
	}
	});
	});
	});
</script>


<!----click id of delete docs--->
<script type="text/javascript">
  function reply_click2(clicked_id, dataname)
  {
    // alert(clicked_id);
	  var invoiceintdname=clicked_id;  
	  $('#invoiceintdname').val(invoiceintdname);
	  $('#DeleteInvoice').modal('show');
  }
</script> 
<script>	
	$("#deleteinvoice_form #deleteinvoice_form_btn").click(function(e){
		//alert("test");
			$("#deleteinvoice_form_btn"). attr("disabled", false);
			//console.log();
			document.getElementById('dlte_invoice_scss').style.display = "none";
			$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
				$.ajax({
				url: "{{url('deleteinvoice')}}", 
				type: "POST",
				data: $('#deleteinvoice_form').serialize(),
				dataType:'json',
				beforeSend: function() {$('#deleteinvoice_form_btn').html('Please Wait');},
				success: function( response ) {
				$('#deleteinvoice_form_btn').html('Submit');	
				   console.log(response);
					if(response.status == 'success'){
						//console.log(response.res);
						$("#deleteinvoice_form_btn"). attr("disabled", false);
						document.getElementById('dlte_invoice_scss').style.display = "block"; 
						document.getElementById("deleteinvoice_form").reset();
						 window.location.href=response.url;
							//location.reload();
							setTimeout(function () {
								location.reload(true);
							  }, 1000); 
					}					
				}
				});
				
			/**/	
					
		  });  

</script>

<script>	
	$("#custom_invoice_form_btn").click(function(){

			$("#custom_invoice_form").validate({
				rules: {
					billamt: {
						required: true,
						min:1,
						number: true		
						},
					serviceprovided: {
						required: true,
					 },
				},
				messages: {
					billamt: {
						required: "Enter Bill Amount",
						number:"Enter Valid Phone Number"
					},
					serviceprovided: {
						required: "Enter Service Provided",
					},
				},
				submitHandler: function(form) {
				var billamt = parseFloat($('#invoiceHidenAmount').val()).toFixed(2);
				var subtotal = (billamt* 1.13).toFixed(2);
				var hst = (subtotal-billamt).toFixed(2);
				var serviceprovided = $('#serviceprovided').val();				
				$('.new_bill_amt').text(billamt);
				$('.new_service_provided').text(serviceprovided);
				$('#subtotal').text(subtotal);
				$('#hst').text(hst);
				 $('#custom_invoice_modal').modal('show');				
				}
				})		
		  });  
		  
	$("#sendinvoice_btn").click(function(){  
	$('#sendinvoice_btn').prop('disabled', true);
	$("#sendInvoiceLoading").show();
	var newsubtotal = $('#subtotal').text();	
	if(newsubtotal){
		var billamt = newsubtotal;
	}else{
		var billamt = $('#billamt').val();
	}	
	var billintid = $('#bill_invitation_id').val();
	var docid = $('#docid').val();
	var serviceprovided = $('#serviceprovided').val();
		$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
				$.ajax({
				url: "{{url('custom_invoice')}}",
				type: "POST",
				data: {id:billintid,billamt:billamt,docid:docid,serviceprovided:serviceprovided},
				dataType:'json',
				success: function( response ) {
				$('#sendinvoice_btn').html('Submit');	

					if(response.status == 'success'){	
						$("#sendInvoiceLoading").hide();
						$('#custom_invoice_modal').modal('hide');
						$('#scss_invoice_sent').modal('show');
						document.getElementById("custom_invoice_form").reset();						 
							setTimeout(function () { 
								location.reload(true);
							  }, 2000); 
						
					}					
				} 
				});
	});
</script>
<script>
    $(document).ready(function() {
        $('#category_id').selectize();
    });
 </script>


<?php if(isset($_GET['tab'])) { ?>
	<script>
		var tab_id = "<?php echo $_GET['tab']; ?>";
		$("#filesreports-tab").click();
	</script>
<?php } ?>


<script>

$(document).ready(function(){
$('input:radio[name="rds"]').change(function(){
	var selectRadio = $(this).val();
    if(selectRadio == '4'){
        $("#desContainer").show();
    }
	else{
		$("#desContainer").hide();
	}
});
});

	$("#rejectBtn").click(function(){
		$("#rej_ScMessage").hide();
		$("#rej_ErMessage").hide();
        var option = $('input[name="rds"]:checked').val();
	
		let desc = $("#rejectDetails").val();
		let invID = $("#invRegID").val();

		if(option == '4' && isEmpty(desc)){
			$("#rej_ErMessage").show();
			$("#rej_ErMessage").text("Enter description first!");
			setTimeout(function(){
				$("#rej_ErMessage").hide();
			},3000);
			return;
		}

			$.ajaxSetup({
			headers: {
			  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
			});
			$.ajax({
				url: "{{url('/rejectReportRequest')}}",
				type: "POST",
				data: {id:invID,desc:desc,option:option,"_token":"{{ csrf_token() }}"},
				dataType:'json',
				beforeSend: function() {
					$("#rejectBtn").hide();
					$("#rejectLoading").show();
				},
				success: function( response ) {
					if(response.status == '1'){
						$("#rejectDetails").val('');
						$("#rej_ScMessage").show();
						$("#rej_ScMessage").html("Your request has been submitted to admin.")
						$("#rej_ErMessage").hide();
						setTimeout(function(){
							location.replace("{{url('/')}}");
						},3000);
					}
					else{
						$("#rej_ScMessage").hide();
						$("#rej_ErMessage").html("Error occured while submitting report.")
						$("#rej_ErMessage").show();
					}

					setTimeout(function(){
                        $("#rej_ScMessage").hide();
		                $("#rej_ErMessage").hide();
					},3000);

					$("#rejectBtn").show();
					$("#rejectLoading").hide();
				}
			  });
	});
</script>



<script>
	$("#changePriceRequestBtn").on('click',function(){
		$("#pr_success").hide();
		$("#pr_error").hide();
		$("#p_loadingPr").hide();
		$("#pr_description").val('');
		$("#p_drnewPrice").val('');
	    $("#changePriceRequest").modal("show");
	});


	$("#changeHstValue").on("click",function(){
        let doctorID = $("#hstdoctorID").val();
        let hst_number = $("#hst_number_inp").val();

		$("#hst_success").hide();
		$("#hst_error").hide();

		if(!isEmpty(hst_number)){
            $.ajaxSetup({
				headers: {
				   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				url: "{{url('/updatehstNumber')}}",
				type: "POST",
				data: {
					doctorID:doctorID,
					hst:hst_number,
				},
				dataType:'json',
				beforeSend: function() {
					$("#changeHstValue").hide();
					$("#hst_loadingPr").show();
				},
				success: function( response ) {
     
					$("#hst_number_inp").val('');
					$("#hst_loadingPr").hide();
					$("#hst_error").hide();
					$("#hst_success").hide();
					$("#changeHstValue").hide();

					if(response.status == 'success'){
						$("#hst_success").show();
						$("#hst_success").text(response.message);
						setTimeout(() => {
							$("#hst_success").hide();
							location.reload();
							$("#hstNumberModel").modal("hide");
						}, 3000);
					}					
				},
				error:function(error){
					$("#changeHstValue").show();
					$("#hst_loadingPr").hide();
					$("#hst_success").hide();
					$("#hst_error").show();
					$("#hst_error").text("Something went wrong try again");
					setTimeout(() => {
						$("#hst_error").hide();
					}, 3000);
				}
			  });
		}
		else{
		$("#hst_success").hide();
		   $("#hst_error").show();
		   $("#hst_error").text("Please enter HST number first");
		   setTimeout(() => {
			$("#hst_error").hide();
		   }, 3000);
		}

	});


	$("#p_changeRequestBtn").on('click',function(){
      
         let oldPrice = $("#p_oldPrice").val();
         let newPrice = $("#p_drnewPrice").val();
         let doctorID = $("#p_doctorID").val();
         let description = $("#pr_description").val();
        
		 $("#pr_success").hide();
		 $("#pr_error").hide();
		 
		 if(!isEmpty(newPrice)){
			let status = (Number(newPrice) != NaN && Number(newPrice) > 0) ? true : false;
			if(status){
				$.ajaxSetup({
					headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

                $.ajax({
				url: "{{url('/changePriceRequest')}}",
				type: "POST",
				data: {
					oldPrice:oldPrice,
					newPrice:newPrice,
					doctorID:doctorID,
					description:description,
				},
				dataType:'json',
				beforeSend: function() {
					$("#p_changeRequestBtn").hide();
					$("#p_loadingPr").show();
				},
				success: function( response ) {
     
					$("#pr_description").val('');
	            	$("#p_drnewPrice").val('');
					$("#p_loadingPr").hide();
					$("#pr_error").hide();
					$("#pr_success").hide();
					$("#changePriceRequestBtn").hide();

					if(response.status == 'success'){
						$("#pr_success").show();
						$("#pr_success").text(response.message);
						setTimeout(() => {
							$("#pr_success").hide();
							location.reload();
							$("#changePriceRequest").modal("hide");
						}, 3000);
					}
					else{
                        $("#pr_error").show();
						$("#pr_error").text(response.message);
						setTimeout(() => {
							$("#pr_error").hide();
							$("#changePriceRequest").modal("hide");
						}, 3000);
					}					
				},
				error:function(error){
					$("#changePriceRequestBtn").show();
					$("#p_loadingPr").hide();
					$("#pr_success").hide();
					$("#pr_error").show();
					$("#pr_error").text("Something went wrong try again");
					setTimeout(() => {
						$("#pr_error").hide();
					}, 3000);
				}
			  });

			}
			else{
				$("#pr_success").hide();
				$("#pr_error").show();
				$("#pr_error").text("New price must be number");
				setTimeout(() => {
					$("#pr_error").hide();
				}, 3000);
			}
		 }
		 else{
		   $("#pr_success").hide();
		   $("#pr_error").show();
		   $("#pr_error").text("Please enter new price first");
		   setTimeout(() => {
			$("#pr_error").hide();
		   }, 3000);
		 }

	});


	$("#viewChangePriceHistory").on('click',function(){
        $("#changePriceHostoryModel").modal("show");
	});

 </script>