@extends('layouts.admin_app')



@section('content')

<style>
	.addpatientdata p{
		color:#000000;
	}
	.addpatientdata b{
		font-weight:500;
	}
</style>


<div class="container-fluid">

    <div class="row">

	    <div class="col-md-12">

			 <nav aria-label="breadcrumb">

			  <ol class="breadcrumb">

				<li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>

				<li class="breadcrumb-item active"><a href="#">Terms and conditions</a></li>

			  </ol>

			</nav>

		</div>

		<div class="col-md-12">  

			<!----search-------------->
				<p style="    color: #141a45;font-weight: 500;font-size: 28px;text-align: center;">Terms and conditions</p>

				<form method="POST" action="{{ url('admin/terms_and_conditions_submit') }}" >
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10">
							<div class="form-group">

									<textarea id="editor1" name="editor1"></textarea>
									
									@error('terms_privacy_policy')
										<span class="invalid-feedback" role="alert">
											<strong>{{ $message }}</strong>
										</span>
									@enderror
							</div>   
							<div class="form-group">	

									<button type="submit" class="btn btn-primary">

										{{ __('Submit') }}

									</button>

							</div>
						</div>
						<div class="col-md-1"></div>
					</div>

				</form>

		</div>

	</div>   


</div>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>
<script>
 var editor = CKEDITOR.replace( 'editor1' );

// The "change" event is fired whenever a change is made in the editor.
editor.on( 'change', function( evt ) {
    // getData() returns CKEditor's HTML content.
  console.log( 'This is what you typed ' + evt.editor.getData() + typeof evt.editor.getData() );
    console.log( 'Total bytes: ' + evt.editor.getData().length );
  $('#hiddedn input').val(evt.editor.getData());
});
          </script>
@endsection

