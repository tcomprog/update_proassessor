<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<bod style="display: flex; flex-direction:column; height:100vh;"y>
    <header>
        <div class="container">
            <div class="row">
                @include('includes.header')
            </div>
        </div>
    </header>

    <div style="display: flex; flex-direction:column; height:100vh;">

    @yield('content')

    <footer style="margin-top:auto; padding-bottom:0px; padding-top:25px;">
        @include('includes.footer')
        @yield('footerscripts')
    </footer>
</div>
    
    @include('includes.doctor-profile-scripts')
</bod>
</html>