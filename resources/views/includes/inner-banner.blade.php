<!-- Banner Section -->
<section class="intro_banner_sec inner_banner_sec">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="welcome_text">
                    <h1><?php  if(isset($pageuser->page_title)){echo $pageuser->page_title;}else{echo "Dashboard";} ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section -->