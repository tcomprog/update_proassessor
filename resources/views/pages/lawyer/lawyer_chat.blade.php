@extends('layouts.lawyer-chat') 
@section('content')	
<style>
	.mesgs {width:100%;}
</style>
	 <!-- Banner Section -->
    <section class="intro_banner_sec inner_banner_sec chat page">
        <div class="container">
            <div class="row align-items-center"> 
                <div class="col-md-6 welcome_text">
					<h1>Chat Room</h1>					
                </div>
				<div class="col-md-6 text-right">
                    <button class="btn btn-primary" onClick="javascript:history.go(-1)" style="padding: 10px 30px;
					background: #5a8627;
					border: none;
					font-size: 18px;
					line-height: 28px;">Back</button>
                </div>               
            </div>
        </div>
    </section>
	<!---chat start-->
	<section class="dashboard_sec">
        <div class="container">
            <div class="patient-details">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6 m-auto pb-2">
                        <div class="card card-body">
                            <h5>Patient Information</h5>
                            <div class="row">
                                <div class="col-md-6">
                                <p class="mb-0">
                                <strong>Name: </strong>
                                <span>{{ $invitation->patient->name }}</span>
                                </p>
                                <p class="mb-0">
                                <strong>Email: </strong>
                                <span>{{ MyHelper::Decrypt($invitation->patient->email) }}</span>
								</p>
								<p class="mb-0">
                                <strong>Gender: </strong>
                                <span>{{ MyHelper::Decrypt($invitation->patient->gender) }}</span>
								</p>
								<p class="mb-0">
                                <strong>Phone Number: </strong>
                                <span>{{ MyHelper::Decrypt($invitation->patient->phonenumber) }}</span>
								</p>
								<p class="mb-0">
									<strong>Address: </strong>
									<span>{{ MyHelper::Decrypt($invitation->patient->address) }}</span>
								</p>
                                </div>
                                <div class="col-md-6">
                                <p class="mb-0">
                                <strong>Appointment Date: </strong>
                                <span>{{ date('d/M/Y',strtotime($invitation->invitation_date)) }}</span>
                                </p>
                                <p class="mb-0">
                                <strong>Doctor Name: </strong>
                                <span>{{ $invitation->user1->name }}</span>
                                </p>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
            </div>
			<div class="messaging">
                    <div class="inbox_msg">
                        <div class="mesgs">
						
						<div class="headind_srch">
                            <div class="recent_heading">
                            <h4>{{ $invitation->user1->name }}</h4>
                            </div>
                        </div>
						<br>
                        <div class="msg_history" id="messages">
                        </div>
                        <div class="type_msg">
                            <form action="#" id="chat-msg-form">
                            <div class="input_msg_write">
                            <input type="text" id="message" class="write_msg" autocomplete="off" placeholder="Type a message" />
                            <button class="msg_send_btn" type="submit"><img src="{{url('img/telegram.svg')}}"></button>
                            </form>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
					
					</div>
    </section>
	<!---chat end-->
@endsection
@section('footerscripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.1/socket.io.js"></script>

<script>   
    var start = 0;
    const messageContainer  = $( "#messages" );
   var socket = io.connect('{{ env("SOCKET_URL") }}');
   var userInfo = {
       email:'{{ Auth::user()->email }}', 
       sender_id:'{{ Auth::user()->unique_id }}', 
       invitation_id:'{{ $invitation->unique_id }}', 
       partner_id:'{{ $invitation->user1->unique_id }}', 
   };
$(document).ready(function(){

    load_messages();
    
    socket.on("user-join",function(socketID){
        userInfo.socket_id = socketID;
        socket.emit("user-join", userInfo);
    });

        socket.on('chat-message', function (data) {
            if(data && data.message && data.invitation_id == userInfo.invitation_id) {
                if(data.sender_id == userInfo.sender_id && data.invitation_id == userInfo.invitation_id) {
                const messageData = {
                    message : data.message,
                    partner_id:userInfo.partner_id,
                    invitation_id:userInfo.invitation_id,
                    created_on:"{{ date('h:i A | M y') }}",
                    sender_id:userInfo.sender_id,
                    socket_id:userInfo.socket_id,
                    is_read:data.is_read
                    }
                    // alert("partner Online status =  "+data.is_read)
                    store_mesage(messageData);
                }


                let msgData = {};
                if(data.sender_id == userInfo.sender_id && data.invitation_id == userInfo.invitation_id) {
                    msgData = `<div class="outgoing_msg">
                                <div class="incoming_msg_img"> <img src="{{ url('img/user.jpg')}}" alt="user"> </div>
                                <div class="sent_msg">
                                    <p>`+data.message+`</p>
                                    <span class="time_date">`+data.created_on+`</span> </div>
                                </div>`;
                }else if(data.partner_id == userInfo.sender_id && userInfo.invitation_id == data.invitation_id){
                    msgData =  `<div class="incoming_msg"><div class="incoming_msg_img"> <img src="{{ url('img/user.jpg')}}" alt="user"> </div>
                    <div class="received_msg">
                        <div class="received_withd_msg">
                        <p>`+data.message+`</p>
                        <span class="time_date">`+data.created_on+`</span></div>
                    </div>
                    </div>`;
                }
                messageContainer.append(msgData);
                messageContainer.scrollTop(messageContainer[0].scrollHeight);
            }
        });

$('#chat-msg-form').submit(function(e){
    e.preventDefault();
    var message = $('#message').val();
    if(message) {
        const messageData = {
            message : message,
            partner_id:userInfo.partner_id,
            invitation_id:userInfo.invitation_id,
            created_on:"{{ date('h:i A | M y') }}",
            sender_id:userInfo.sender_id,
            socket_id:userInfo.socket_id
        }
        console.log("sending message")
        socket.emit('chat-message',messageData);
        // store_mesage(messageData);

        $('#message').val('');
    }
})

});

function store_mesage(messageData)
{
    messageData._token = '{{ csrf_token() }}';
    $.ajax({
        url:"{{ url('chat/store-message') }}",
        data:messageData,
        type:'post',
        dataType:'json',
        success:function(res) {
            if(res.status != 'success') {
                alert("Error occured while storing message.")
            }
        }
    });
}

function load_messages() {
    $.ajax({
        url:"{{ url('chat/load-messages') }}",
        type:'get',
        data:{invitation_id:'{{ $invitation->unique_id}}',start:start},
        dataType:'json',
        success:function(res) {
            if(res.status == 'success') {
                messageContainer.append(res.data);
                messageContainer.scrollTop(messageContainer[0].scrollHeight);
            }else{
                alert("someting went wrong while loading Messages.")
            }
        }
    });
}
    </script>
@endsection
