<style>
	.bubble1 {
	@extend .bubble;&::before {
	content: '';
	position: absolute;
	left: 0;
	top: 50%;
	width: 0;
	height: 0;
	border: 13px solid transparent;
	border-right-color: #32557f;
	border-left: 0;
	margin-top: -13px;
	margin-left: -13px;
	}
	.removetsgs:hover{
		cursor: pointer !important;
		text-decoration: none !important;
	}
	}
	
	.showPassword:hover{
		cursor: pointer;
	}
	.forgrotPsass{
		height: auto !important;
		max-width: 300px !important;
		width: auto !important;
		padding:8px 25px !important;
		font-size: 17px !important;
		margin-top: 12px !important;
	}
	</style>
	
	{{-- ######## --}}
	{{-- |||||||||||||||||||||||||||||| START OF NEW SIGNUP ||||||||||||||||||||||||||||| --}}
	<!-- Signup Model -->
	<div class="modal fade" id="newsignup" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
		  <div id="signUpFormContainer" class="modal-content login_box login_box22">
	
			<div class="modal-header" style="border:none">
			  <button type="button" class="close close11btn" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>
			
			<div class="modal-body" style="padding: 0px;">
	
			   <div style="text-align: center">
				  <img class="img-fluid" src="{{asset('img/logo.png')}}" />
	
				  <div style="margin-top: 15px;">
					  <button id="doctorTab" class="activeTab" style=" border-radius: 30px 0px 0px 30px; margin-right:-3px;">Doctor</button>
					  <button id="clinicTab" class="inactiveTab" style="border-right:none;margin-right:-3px; margin-left:-3px;">Clinic</button>
					  <button id="lawyerTab" class="inactiveTab" style="border-radius: 0px 30px 30px 0px; margin-left:-3px;">Lawyer</button>
				  </div>	
	
			   </div>
	
			   <script>
				  $("#doctorTab").on('click',function(){
					$(this).addClass("activeTab");
					$(this).removeClass("inactiveTab");
	
					$("#lawyerTab").addClass("inactiveTab");
					$("#lawyerTab").removeClass("activeTab");
					$("#clinicTab").addClass("inactiveTab");
					$("#clinicTab").removeClass("activeTab");

					$("#designation").val("1")
					$("#descFieldContainer").show();
					$("#descField").attr("placeholder","License Number");

					$("#designationSection").show();
	
					$("#name1").val('');
					$("#email1").val('');
					$("#business_address1").val('');
					$("#password1").val('');
					$("#confirmpassword1").val('');
					$("#phone1").val('');
					$("#descField").val('');
					$("#agrrur").val('');
					
				});

				$("#clinicTab").on('click',function(){
					 $(this).addClass("activeTab");
					 $(this).removeClass("inactiveTab");

					$("#lawyerTab").addClass("inactiveTab");
					$("#lawyerTab").removeClass("activeTab");
					$("#doctorTab").addClass("inactiveTab");
					$("#doctorTab").removeClass("activeTab");

					$("#designation").val("4")
					$("#descFieldContainer").hide();
					$("#designationSection").hide();

				  });
	
				$("#lawyerTab").on('click',function(){
					$(this).addClass("activeTab");
					$(this).removeClass("inactiveTab");
	
					$("#doctorTab").addClass("inactiveTab");
					$("#doctorTab").removeClass("activeTab");
					$("#clinicTab").addClass("inactiveTab");
					$("#clinicTab").removeClass("activeTab");

					$("#designation").val("2");
					$("#descFieldContainer").show();
					$("#descField").attr("placeholder","Law Firm Name");

					$("#designationSection").hide();
	
					$("#name1").val('');
					$("#email1").val('');
					$("#business_address1").val('');
					$("#password1").val('');
					$("#confirmpassword1").val('');
					$("#phone1").val('');
					$("#descField").val('');
					$("#agrrur").val('');
					
				});
			   </script>
			  
	
			   <div class="container p-4 pt-2" style="padding-top: 18px !important;">
				  <form class="row" autocomplete="off">
	
					  <div id="successMSG" class="alert alert-success col-12 text-center" style="display: none;" role="alert">
						Registration Successful. Please check your mail for activation. <br>
						Please check your spam to ensure the receipt of your email. 
					  </div>
	
					<input type="hidden" name="designation" id="designation" value="1">
	
					<div class="col-md-6 col-sm-12">
						<div class="inputContainer">
							<span class="material-icons" style="color: #515C84; font-size:28px;">person</span>
							<input id="name1" name="name" placeholder="Name" type="text" class="inputStyle" autocomplete="off" />			  
						</div>
						<span id="name_error" class="errorMsg">Error message</span>
					</div>
	
					<div class="col-md-6 col-sm-12">
						<div class="inputContainer">
							<span class="material-icons" style="color: #515C84; font-size:28px;">drafts</span>
							<input id="email1" name="email" placeholder="Email" type="text" class="inputStyle" autocomplete="email"/>
						 </div>
						 <span id="email_error" class="errorMsg"></span>
					</div>
	
					<script>
						$("#email1").on('input',function(){
							if(validateEmail($(this).val()) == false){
							  $("#email_error").show();
							  $("#email_error").text("Invalid email address");
							}	
							else{
								$("#email_error").hide();
							}	
	
							if(isEmpty($(this).val()))	{
								$("#email_error").hide();
							}		
						})
					</script>
	
					<div class="col-12">
						<div class="inputContainer">
							<span class="material-icons" style="color: #515C84; font-size:28px;">add_location</span>
							<input autocomplete="off" id="business_address1" name="business_address" placeholder="Business Address" type="text" class="inputStyle" autocomplete="off" />
						 </div>
						 <span id="address_error" class="errorMsg">Error message</span>
					</div>
	
	
					<div class="col-md-6 col-sm-12">
						<div class="inputContainer">
							<span class="material-icons" style="color: #515C84; font-size:28px;">lock</span>
							<input id="password1" name="password" placeholder="Password" type="password" class="inputStyle" autocomplete="new-password" />
						 </div>
						 <span id="password_error" class="errorMsg">Error message</span>
					</div>
	
					<div class="col-md-6 col-sm-12">
						<div class="inputContainer">
							<span class="material-icons" style="color: #515C84; font-size:28px;">lock</span>
							<input id="confirmpassword1" name="confirmpassword" placeholder="Re-password" type="password" class="inputStyle" autocomplete="off" />
						 </div>
						 <span id="repassword_error" class="errorMsg">Error message</span>
					</div>
	
					<script>
						$("#password1").on('input',function(){
							let pw = $(this).val();
							let validate = checkPasswordValidity(pw);
			   
							if(!isEmpty(pw) && validate != null){
							  $("#password_error").show();
							  $("#password_error").text(validate);
							}
							else{
								$("#password_error").hide();
							}
	
							if(isEmpty(pw)){
								$("#password_error").hide();
							}
						});
	
						$("#confirmpassword1").on('input',function(){
							let pw = $(this).val();
							let pw2 = $("#password1").val();
	
							if(isEmpty(pw)){
								$("#repassword_error").hide();
							}
	
							if(pw != pw2){
							   $("#repassword_error").show();
							   $("#repassword_error").text("Passwords don`t match");
							}
							else{
								$("#repassword_error").hide();
							}
	
						});
					</script>
	
					<div class="col-md-6 col-sm-12">
						<div class="inputContainer">
							<span class="material-icons" style="color: #515C84; font-size:28px;">call</span>
							<input id="phone1" maxlength="10" name="phone" placeholder="Phone number" type="text" class="inputStyle" autocomplete="off" />
						 </div>
						 <span id="phone_error" class="errorMsg">Error message</span>
					</div>
	
					<script>
						 $("#phone1").on('input',function(){
							let pw = $(this).val();
							if(digits_only(pw) == false){
								$("#phone_error").show();
								$("#phone_error").text("Invalid phone number");
							}else{
								$("#phone_error").hide();
							}
						});
					</script>
	
					<div class="col-md-6 col-sm-12" id="descFieldContainer">
						<div class="inputContainer">
							<span class="material-icons" style="color: #515C84; font-size:28px;">edit_document</span>
							<input id="descField" placeholder="License Number" type="text" class="inputStyle" autocomplete="off">
						 </div>
						 <span id="descField_error" class="errorMsg">Error message</span>
					</div>

					<div class="col-12" id='designationSection'>
						<div class="form-group">
							{{-- <label for="license">Choose Designation</label> --}}
							<select class="form-control-file inputContainer" id='desginationDrop'>
									<option value="-1">Choose Designation</option>
									<?php  foreach (MyHelper::desginationList() as $designation){ ?>
									<option value="<?php echo $designation->id; ?>" ><?php echo $designation->designation; ?></option>
									<?php } ?>
							</select>
							<span id="designation_error" class="errorMsg">Error message</span>
						</div>
					</div>
	
					<div class="form-group form-check col-12" style="margin-left:5px; margin-top:12px;">						  
						<label style="color: #20215E" for="agrrur">
							<input id="agrrur" name="agrrur" type="checkbox">   I agree to the terms and conditions.</label>
						<span id="agrrur_error" class="errorMsg">Error message</span>
					</div>
	
					
					<div class="col-12 text-center">
						<img style="display: none" id="loadingIMG" width="50" src="{{asset('img/loading.gif')}}" alt="">
						<button onclick="submitSignUpForm()" type="button" class="signupBtn">Sign Up</button> <br>
					</div>
					
					   
					<div class="col-12" style="display: flex;justify-content: space-between;align-items: center;">
						<div>
							<p class='col-12' style="color:#475AA0;margin-left:16px; margin-top:12px;">Already have an account? <a class="removetsgs" href="#" id="signup-login-link" style="color:#20215E;">Login</a></p>
						</div>
						<div style="text-wrap: nowrap;">
							<button onclick="zoomFunc(1,1)" type="button" class="zoom_btn border-0 rounded mr-2">
								<span class="material-icons text-white align-middle" style="font-size:24px;">remove</span>
							</button>
							<button onclick="zoomFunc(0,1)" type="button" class="zoom_btn border-0 rounded">
								<span class="material-icons text-white align-middle" style="font-size:24px;">add</span>
							</button>
						</div>
					</div>
	
				  </form>
			   </div>
	  
			</div>
			</div>
			
		  </div>
		</div>
	  </div>
	{{-- |||||||||||||||||||||||||||||| END OF NEW SIGNUP ||||||||||||||||||||||||||||| --}}
	
	
	{{-- ######## --}}
	<!-- ********************** Login Model Start #### ****************************** -->
	<div class="modal fade" data-easein="swoopIn" data-backdrop="static" data-keyboard="false" tabindex="-1" id="login" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered d-flex justify-content-center align-items-center" role="document">
		<div id="loginModalPopup" class="modal-content login_box login_box22">
	
		  <div class="modal-header border-0 m-0 p-0">
	
			  <div class="modal-header" style="border:none">
				<button type="button" class="close close11btn" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
		 
		  </div>
	
		  <div style="text-align: center">
			<img class="img-fluid" src="{{asset('img/logo.png')}}" />		  
		 </div>
	
		  <div class="modal-body">
				  <form id="loginform" class="w-75 mr-auto ml-auto p-lg-2">
						@csrf
					   
						<div class="row">
							<div class="col-md-12">		
													
								<ul style="text-align:center">
																	
									<li>
										<div class="form-check">
											<input  class="form-check-input" type="radio" name="role_id" id="doctor2"  value="1" 
											<?php if(isset($_COOKIE['role_id'])){ if(MyHelper::Decrypt($_COOKIE['role_id'])==1){echo "checked";}} ?>>
											<label class="form-check-label font-weight-bold" for="doctor2">
												Doctor
											</label>
										</div>
									</li>
									<li>
										<div class="form-check">
											<input  class="form-check-input" type="radio" name="role_id" id="doctor3"  value="4" 
											<?php if(isset($_COOKIE['role_id'])){ if(MyHelper::Decrypt($_COOKIE['role_id'])==4){echo "checked";}} ?>>
											<label class="form-check-label font-weight-bold" for="doctor3">
												Clinic
											</label>
										</div>
									</li>
									<li>
										<div class="form-check">
											<input class="form-check-input" type="radio" name="role_id" id="lawyer2" value="2"
											<?php if(isset($_COOKIE['role_id'])){ if(MyHelper::Decrypt($_COOKIE['role_id'])==2){echo "checked";}} ?>>
											<label class="form-check-label font-weight-bold" for="lawyer2">
												Lawyer
											</label>
										</div>
									</li>
									<li>
										<div class="form-check">
											<input class="form-check-input" type="radio" name="role_id" id="patient2" value="5"
											<?php if(isset($_COOKIE['role_id'])){ if(MyHelper::Decrypt($_COOKIE['role_id'])==5){echo "checked";}} ?>>
											<label class="form-check-label font-weight-bold" for="lawyer2">
												Patient
											</label>
										</div>
									</li>
								</ul>
								<label class="error" generated="true" for="role_id" style="display:none;"></label>
							</div>
						</div>
						<div class="row">
	
							<div class="col-md-12 col-sm-12">
								<div class="inputContainer w-100">
									<span class="material-icons" style="color: #515C84; font-size:28px;">account_circle</span>
									<input class="inputStyle" type="email" placeholder="Email" id="email" name="email" aria-describedby="email" autocomplete="email" value="{{ old('email',isset($_COOKIE['email']) ? MyHelper::Decrypt($_COOKIE['email']) : '') }}" />
								 </div>
							</div>	
									
							<div class="col-md-12 col-sm-12">
								<div class="inputContainer w-100">
									<span class="material-icons" style="color: #515C84; font-size:28px;">lock_open</span>
									<input class="inputStyle" id="loginPasswordFi"  type="password" placeholder="Password" name="password" id="password" aria-describedby="password" autocomplete="new-password" value="{{ old('password',isset($_COOKIE['password']) ? MyHelper::Decrypt($_COOKIE['password']) : '') }}"/>
									<span class="material-icons showPassword" title="Show / hide password" id="loginShowPass" style="color: #515C84; font-size:28px;">visibility</span> 
								</div>
							</div>	
	
							<script>
								$("#loginShowPass").click(function(){
									let val = $(this).text();
									if(val == 'visibility'){
										$("#loginPasswordFi").attr('type','text');
										$("#loginShowPass").text("visibility_off");
									}else{
										$("#loginPasswordFi").attr('type','password');
										$("#loginShowPass").text("visibility");
									}
								})
							</script>
	
	
							<div class="col-md-12">
							   <p id="loginfail" style="color:red;"></p>
							   <p id="inactive" style="color:red;"></p> 
							   <script>
								   function goToContact() {
									$('.modal').modal('hide'); 
									  window.location.href='https://proassessors.com/#contact';
								   }
							   </script>
								<p id="admininactive" style="color:red;display:none;">Your account is currently Inactive. Please <a role="button" onClick="goToContact()" style="color: #00396B;"   aria-label="Contact Us" id="adminerrorcontact">contact</a> Admin for further details.</p>
							</div>
	
							<div class="col-md-12 mt-3">
								<div class="form-check">
									<input type="checkbox" class="form-check-input" id="terms" name="terms" checked>
									<label class="form-check-label checkboxsi" for="exampleCheck1">I agree to the <a class="checkboxsi" href="terms&conditions" target="_blank" style="text-decoration: none;">Terms and conditions.</a></label><br>
									<label class="error checkboxsi" generated="true" for="terms" style="display:none;"></label>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="form-check">
								<!--	<input type="checkbox" class="form-check-input" id="exampleCheck1">-->
									
									 <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember',isset($_COOKIE['rember']) ? true : false) ? 'checked' : '' }}>
									<label class="form-check-label checkboxsi" for="exampleCheck1">Remember me</label>
								</div>
							</div>
							<div class="col-md-6 text-right">
								<p class="checkboxsi"> <a href="#" onclick="openForgotPassword()">Forgot your password?</a></p>
							</div>
						</div>
						<div style="display:flex; align-items: center;">
						<img id="login_loadingIMG" style="width:50px; height:50px; display:none;" src="{{asset('img/loading.gif')}}" alt="">
	
						 <button type="submit" class="btn btn-primary submit_btn w-100 signupBtn" style="max-width:100%" id="loginsubmit">Login</button>
						 </div>

						<div style="display: flex;justify-content: space-between;align-items: center;">
						<p style="text-align:center">Don't have an account? <a href="#" id='login-signup-link'>Sign Up</a></p>
						<div style="text-wrap: nowrap;">
							<button onclick="zoomFunc(1)" type="button" class="zoom_btn border-0 rounded mr-2">
								<span class="material-icons text-white align-middle" style="font-size:24px;">remove</span>
							</button>
							<button onclick="zoomFunc(0)" type="button" class="zoom_btn border-0 rounded">
								<span class="material-icons text-white align-middle" style="font-size:24px;">add</span>
							</button>
						</div>
					    </div>
					</form>
		  </div>
		</div>
	  </div>
	</div>
	<!-- ********************** Login Model End ****************************** -->
	
	
	
	<!---------forgot---------->
	<div class="modal fade" id="forgot" tabindex="1" role="dialog" aria-labelledby="forgotTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content login_box">
			  <div class="modal-header">
				<h5 class="modal-title" id="loginTitle">Forgot Password</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
						<form id="resetpwdform">
								@csrf					
									<div class="row"> 
										
										 <div class="col-md-12">
												<p style="margin-bottom: 12px;">Enter your email address to reset your password.</p>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<input type="email" style="border-radius: 20px; padding-left:20px;" class="form-control" id="email" name="email" aria-describedby="email" placeholder="Email">
												</div>
												<div class="alert alert-success fade show" role="alert" id="emailsuccess" style="display:none;">
													Please Check Your Mail For Reset Password
												</div>
												<div class="alert alert-danger fade show" role="alert" id="emailfail" style="display:none;">
													Email Not Registered
												</div>
											</div>
											
									</div>
									<img id='forgotpasswordloading' width="34" style="vertical-align: middle;display:none;" src="{{asset('img/loading.gif')}}" />
									<button type="submit" class="btn btn-primary submit_btn forgrotPsass"  id="resetpwd">Reset Password</button>
									<p><a href="#" id="fwd-login-link">Login</a></p>
						</form>
			  </div>
		</div>
	  </div>
	</div>
	
	
	
	<link href="{{ asset('css/view_calenderar.css') }}" rel="stylesheet">
	<!--------- otp --------->
	<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" id="loginotp" tabindex="1" role="dialog" aria-labelledby="forgotTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content login_box">
			  
			 <div class="modal-header" style="border: none;">
				<h5 class="modal-title" id="loginTitle">Verification Code</h5>
				<button onclick="funcs_close()" type="button" class="close nclose" data-dismiss="modal" aria-label="Close">
	
					<span class="material-icons" style="color: #fff; font-size:23px;
					vertical-align: middle;">close</span>
	
				</button>
			  </div>
	
			  <div class="modal-body" style="padding-top:0px;">
						<form id="otploginform"  data-group-name="digits" data-autosubmit="false" autocomplete="off">
								@csrf					
									<div class="row"> 

										<div class="col-md-12">
											<div class="form-group">
												<label class="form-label" for="exampleCheck1">Please enter the verification code as received to your email address.</label>
												{{-- <input type="text" class="form-control" id="loginotp" name="loginotp"> --}}
												<input type="hidden" class="form-control" id="otpuniqueid" name="otpuniqueid">
											</div>
										</div>

										<div class="col-12 digit_groupss">
											<input class="optInputBox" type="text" id="digit-1" name="digit_1" data-next="digit-2" />
											<input class="optInputBox" type="text" id="digit-2" name="digit_2" data-next="digit-3" data-previous="digit-1" />
											<input class="optInputBox" type="text" id="digit-3" name="digit_3" data-next="digit-4" data-previous="digit-2" />
											<input class="optInputBox" type="text" id="digit-4" name="digit_4" data-next="digit-5" data-previous="digit-3" />
											<input class="optInputBox" type="text" id="digit-5" name="digit_5" data-next="digit-6" data-previous="digit-4" />
											<input class="optInputBox" type="text" id="digit-6" name="digit_6" data-previous="digit-5" />
										</div>
										
										<div class="col-12">
											<p id="invalidotp" style="color:red; text-align:center;margin-top:10px;"></p>
										</div>
											
											
									</div>
	
									<div style="display: flex;flex-direction:row;align-items:center; justify-content: center;">
										<img id="otp_loader" style="width:50px; height:50px;display:none; " src="{{asset('img/loading.gif')}}" alt="">
										<button type="submit" class="btn btn-primary submit_btn"  id="otploginsubmit">Verify</button>
										<h4 id="opt_counter" style="margin:0px;margin-left:40px;text-align: center;display:none;">00</h4>
									</div>
									
						</form>
					
						<p id="resendView" style='text-align:center;'>Didn't receive the verification code? <a id='ressendClick' href="javascript:void(0)">Resend Verification Code</a></p>
						<p id="resendotpsuccess" style="color:green;"></p>
			  </div>
		</div>
	  </div>
	</div>
	
	
	<script>
		$(".modal").each(function(l){
			$(this).on("show.bs.modal",function(l)
			{
			var o=$(this).attr("data-easein");
			"shake"==o ? $(".modal-dialog").velocity("callout."+o):"pulse"==o?$(".modal-dialog").velocity("callout."+o):"tada"==o?$(".modal-dialog").velocity("callout."+o):"flash"==o?$(".modal-dialog").velocity("callout."+o):"bounce"==o?$(".modal-dialog").velocity("callout."+o):"swing"==o?$(".modal-dialog").velocity("callout."+o):$(".modal-dialog").velocity("transition."+o)})});
	</script>


@include('includes.lawyer_model')



	