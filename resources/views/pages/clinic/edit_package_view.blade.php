@extends('layouts.admin_app')


@section('content')
<style>
    .deleteInput:hover{
        cursor: pointer;
    }
</style>

<div class="container-fluid bg-white rounded py-4 adminPageContainer">

    <div class="row">

        <h4 class="inv-content-header" style="margin-bottom:0px;">Edit Package</h4>
		 

        <form method="post" action="/updatePackage">
            @csrf
            <div class="row mt-3">
                <div class="form-group col">
                    <label for="exampleInputEmail1">Name</label>
                    <input name="name" type="text" readonly class="form-control" value="{{ucfirst($package->name)}}" aria-describedby="emailHelp">
                </div>

                <div class="form-group col">
                    <label for="exampleInputPassword1">Amount</label>
                    <input required name="amount" type="text" class="form-control" value="{{ucfirst($package->amount)}}">
                </div>
           </div>

           <input type="hidden" name="id" value="{{$package->id}}">

            <div class="row mt-1 mb-4">
                <p class="font-weight-bold ml-3">Features</p>
                <div id="featureContainer" class="d-flex" style="flex-wrap:wrap">

                    @php
                       $list = json_decode($package->features);
                    @endphp

                    @foreach ($list as $fr)
                      @php
                          $unique = MyHelper::randomString(50);
                      @endphp
                        <div id="feat_{{ $unique}}" class="d-flex align-items-center ml-3 mt-2">
                            <input type="text" value="{{$fr}}" name="features[]" style="width:300px" class="form-control" placeholder="Enter feature">
                            <span data-feat='{{ $unique}}' class="material-icons text-danger deleteInput" style="font-size:24px; text-align:center;">delete</span>
                        </div>
                    @endforeach

                </div>
           </div>
           
            <button type="submit" class="btn btn-primary">Update</button>
            <button onclick="addNewFeature()" type="button" class="btn btn-primary">Add Feature</button>
        </form>

    </div>
</div>


@endsection

<script>
    function addNewFeature(){
        var el = document.getElementById("featureContainer");
        var next_no = el.childElementCount+1;
        var id = next_no+"_"+Math.floor(Math.random() * 100);
        var feature = `<div id="feat_${id}" class="d-flex align-items-center ml-3 mt-2">
                        <input type="text" name="features[]" style="width:300px" class="form-control" placeholder="Enter feature">
                        <span data-feat='${id}' class="material-icons text-danger deleteInput" style="font-size:24px; text-align:center;">delete</span>
                    </div>`;
        el.insertAdjacentHTML('beforeend', feature);;
    }
</script>

