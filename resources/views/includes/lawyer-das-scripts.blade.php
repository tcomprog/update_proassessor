<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" ></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" ></script>
<!--added-->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<!----------------------------------update profile lawyer-------------------------> 
<script> 
    $("#update_profile").validate({  
            rules: {
                    name: {
                    required: true,
                    maxlength: 50
                    },
                    postcode: {
                      required: false,
                    },
                    email: {
                    required: true,
                    maxlength: 50,
                    email: true,
                    },  
                    password:{
                        required:false,
                        minlength: 8
                    },
                    confirm_password:{
                        required:false,
                        equalTo: "#password"
                    },
                    business_address:{
                                required:true
                            },  
                        location:{
                        required:true
                    },
                    phone: {
                            required: true
                        },
                    location: {
                        required: true
                    },
                       lawfirmname: {
                        required: true
                    },
                    websiteurl: {
                        required: false
                    },   
                },
                messages: {
                    name: {
                    required: "Please enter name",
                    maxlength: "Your name maxlength should be 50 characters long."
                    },
                    email: {
                    required: "Please enter valid email",
                    email: "Please enter valid email",
                    maxlength: "The email name should less than or equal to 50 characters",
                    },   
                    password:{
                            required:"Please Enter  password"
                        },
                        confirm_password:{
                            required:"Please Enter  Confirm password"
                        },
                    business_address:{
                            required:"Please Enter  Address"
                        },
                    location:{
                        required:"Please Enter  location"
                    },
                    phone: {
                            required: "Please Enter Phone Number"
                        },
                    location: {
                        required: "Please Select City"
                    },    
					lawfirmname: {
                        required: "Please Enter Law Firm Name"
                    }, 
                    websiteurl: {
                        required: "Please Enter Website Url"
                    }, 
                },
            submitHandler: function(form) {
                $("#update_btn"). attr("disabled", true);
               // $('#lawyeremailexists').text('');   
               $('#lawyerinvalidformat').text('');   
                $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
           
            var formData = new FormData($('#update_profile')[0]);
          //  return false;
            
            $.ajax
            ({
                url: "{{url('updatelawyer')}}",
                type: 'POST',              
               data: formData,
               dataType:'json',
               beforeSend: function() {$('#update_btn').html('Please Wait');},
               processData: false,
               contentType: false,
               success: function( response ) {
                    $('#update_btn').html('Submit');
                     if(response.status == 'error'){                            
                            $("#update_btn"). attr("disabled", false);
                            console.log(response.message.image[0]); 
                             if (typeof response.message.image[0] === "string" ){ 
                                var invalidformats = "Allowed Formats Jpg, Jpeg, Png & Max Size 1Mb";
                                $('#lawyerinvalidformat').text(invalidformats);
                            }        
                         }
                    if(response.status == 'success'){                       
                        document.getElementById('updatesuccess').style.display = "block";
                        $('body,html').animate({scrollTop:0},800);
                        $("#update_btn"). attr("disabled", false);
                        window.location.href=response.url;
                        setTimeout(function () {
                            location.reload(true);
                        }, 500);
                    }
                }
            });
            }
        });




		$("#updatePatientProfile").validate({  
            rules: {
				  fname: { required: true, maxlength: 50 },
				  lname: { required: true, maxlength: 50 },
				  filenumber: { required: true, maxlength: 50 },
				  address: { required: true},
				  phone: { required: true},
				  dob: { required: true},
				  doa: { required: true},  
                },
                messages: {
                    fname: { required: "Please enter first name", maxlength: "Your first name maxlength should be 50 characters long."},
                    lname: { required: "Please enter last name", maxlength: "Your last name maxlength should be 50 characters long."},
                    filenumber: { required: "Please enter file number", maxlength: "Your file number maxlength should be 50 characters long."},
                    address: { required: "Please enter address"},
					phone: {required: "Please Enter Phone Number"},
					dob: {required: "Please Enter DOB"},
					doa: {required: "Please Enter DOA"},
                },
            submitHandler: function(form) {
                $("#update_btn"). attr("disabled", true);
               // $('#lawyeremailexists').text('');   
               $('#lawyerinvalidformat').text('');   
                $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
           
            var formData = new FormData($('#updatePatientProfile')[0]);
          //  return false;
            
            $.ajax
            ({
                url: "{{url('updatePatientProfile')}}",
                type: 'POST',              
               data: formData,
               dataType:'json',
               beforeSend: function() {$('#update_btn').html('Please Wait');},
               processData: false,
               contentType: false,
               success: function( response ) {
                    $('#update_btn').html('Submit');
                     if(response.status == 'error'){                            
                            $("#update_btn"). attr("disabled", false);
                            console.log(response.message.image[0]); 
                             if (typeof response.message.image[0] === "string" ){ 
                                var invalidformats = "Allowed Formats Jpg, Jpeg, Png & Max Size 1Mb";
                                $('#lawyerinvalidformat').text(invalidformats);
                            }        
                         }
                    if(response.status == 'success'){                       
                        document.getElementById('updatesuccess').style.display = "block";
                        $('body,html').animate({scrollTop:0},800);
                        $("#update_btn"). attr("disabled", false);
                        window.location.href=response.url;
                        setTimeout(function () {
                            location.reload(true);
                        }, 500);
                    }
                }
            });
            }
        });



</script>
<!---------upload files---------->
<!----click id of upload--->
<script type="text/javascript">
  function reply_click(clicked_id)
  {
      //alert(clicked_id);
	  var intid=clicked_id; 
	  $('#newintid').val(intid);
	  $('#UploadDocs').modal('show');
  }
   
</script> 
<script type="text/javascript">
 /*active nav bar*/
  $(document).ready(function(){
	  var currentURL = $(location).attr('href');
	  if(currentURL.indexOf('#patientfile') != -1){
			$('.nav-tabs a.active').removeClass('active')
			$('.tab-content div.active').removeClass('active')
			$('.tab-content div.show').removeClass('show')
			
			//$(this).addClass('active');
			$('#nav-invitations-tab').addClass('active')
			$('#nav-invitations').addClass('active')
			$('#nav-invitations').addClass('show')
			}				
	})
</script>
<script type="text/javascript">
 /*active nav bar*/
  $(document).ready(function(){
	  var currentURL = $(location).attr('href');
	  if(currentURL.indexOf('#Appointments') != -1){
			$('.nav-tabs a.active').removeClass('active')
			$('.tab-content div.active').removeClass('active')
			$('.tab-content div.show').removeClass('show')
			
			//$(this).addClass('active');
			$('#nav-accepted-tab').addClass('active')
			$('#nav-accepted').addClass('active')
			$('#nav-accepted').addClass('show')
			}				
	})
</script> 
<script type="text/javascript">
 /*active nav bar*/
  $(document).ready(function(){
	  var currentURL = $(location).attr('href');
	  if(currentURL.indexOf('#Reports') != -1){
			$('.nav-tabs a.active').removeClass('active')
			$('.tab-content div.active').removeClass('active')
			$('.tab-content div.show').removeClass('show')
			
			//$(this).addClass('active');
			$('#nav-rejected-tab').addClass('active')
			$('#nav-rejected').addClass('active')
			$('#nav-rejected').addClass('show')
			}		
	})
</script> 



<!-------upload summary-------->
<script type="text/javascript">
	$(document).ready(function (e) {	
		
		$('.upload_doc_btn').click(function(){
			$('#upload-file-invitation-id').val($(this).attr('data-initation-id'));
			$('#upload-file-patient-id').val($(this).attr('data-patient-id'));
		});
	$.ajaxSetup({
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
	});


function makeSingleCall_summary(curret,total){
   let totalFiles = $('#files')[0].files.length; //Total files
   let files = $('#files')[0];
   var filenames = $('#files')[0].files;

	var progress = `<div  class="progress mt-2"><div id="fprog_sm_${curret}" class="progress-bar" style='background:#323B62' role="progressbar" style="width: 5%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">0% </div> </div><p>${filenames[curret]["name"]}</p>`;
	$('#progress_bar_summary').append(progress);
    
	var newintid = $("#upload-file-invitation-id").val();
    var patientid = $("#upload-file-patient-id").val();

    var formData = new FormData();
	formData.append('newintid', newintid);
	formData.append('patientid', patientid);
	formData.append('file', files.files[curret]);

	$.ajax({
			xhr: function() {
			    var xhr = new window.XMLHttpRequest();
				xhr.onreadystatechange = function () {
                    var rdState = xhr.readyState
                    var progress = 25 * rdState;
                    $("#fprog_sm_"+curret).css('width',progress+"%");
                    $("#fprog_sm_"+curret).text(progress+"%");
                }
				return xhr;
			},
			type:'POST',
			url: "{{ url('/uploaddocuments')}}",
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: (data) => {
				if(curret+1 < total){
					makeSingleCall_summary(curret+1,total)
				}
				else{
					$("#files").val(null);
					var url = "{{url()->full()}}";
					url = url.replace('&amp;','');
					if(url.includes('tab')){
					url = url.split("tab")[0];
					}
					url += "&tab=2";
					location.replace(url);
				}
			},
			error: function(data){
				$('#progress_bar_summary').empty();
			    $("#fileuploadError_summary").show();
				$("#fileuploadError_summary").text('Error occured while uploading files, try again');
				$("#files").val(null);
				$("#patient_submit").show();

				setTimeout(() => {
					$("#fileuploadError_summary").hide();
				}, 5000);
			
			}
	});
}

//aab
	// New summary uploading start
$("#patient_submit").on('click',function(){
		$("#patient_submit").hide();
		$("#fileuploadError_summary").hide();
        $("#progress_bar_summary").empty();
		let totalFiles = $('#files')[0].files.length; //Total files
        let files = $('#files')[0];
        var filenames = $('#files')[0].files;

		if(totalFiles > 0){
			const allowed = [
				"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
				"application/msword",
				"application/pdf",
			];

			var error = false;

			// --- File type check start
			for(let i = 0; i < totalFiles; i++){
				if(!allowed.includes(files.files[i].type)){
					$("#fileuploadError_summary").show();
					$("#fileuploadError_summary").text('Invalid file type, You can only upload pdf, doc, docx');
					$("#files").val(null); // to clear all select files
					setTimeout(() => {
					$("#fileuploadError_summary").hide();
			    	}, 5000);
					error = true;
					break;
				}else{
					error = false;
				}
			}
			// --- File type check end

			if(error == false){
				makeSingleCall_summary(0,totalFiles);
				$('#patient_submit').hide();
			}else{
				$("#patient_submit").show();
			}

		}

	 });
	// New summary uploading end



	$('.req-transport-btn').click(function(){
		const invitationID = $(this).attr('data-req-invit-id');
		if(invitationID) {
			$.confirm({
				title: 'Are you sure?',
				content: 'Are you sure want to send request to Admin?',
				buttons: {
				YES: function () {	
					$.ajax({
						type:'POST',
						url: "{{ url('request-transport-transalation')}}",
						data: {invitation_id:invitationID},
						dataType: 'json',
						success:function(res) {
							if(res.status == 'success') {
								$('#transport-option-no'+invitationID).hide();
								$('#transport-option-yes'+invitationID).show();
							}
						}
					});
				},
        		NO: function () {}
    			}
			});
		}else{
			alert('Invlid Input');
		}
	})
	
	//translation
	$('.req-translate-btn').click(function(){
		const invitationID = $(this).attr('data-req-invit-translate-id');
		if(invitationID) {
			$.confirm({
				title: 'Are you sure?',
				content: 'Are you sure want to send request to Admin?',
				buttons: {
				YES: function () {	
					$.ajax({
						type:'POST',
						url: "{{ url('request-transalation')}}",
						data: {invitation_id:invitationID},
						dataType: 'json',
						success:function(res) {
							if(res.status == 'success') {
								$('#translate-option-no'+invitationID).hide();
								$('#translate-option-yes'+invitationID).show();
							}
						}
					});
				},
        		NO: function () {}
    			}
			});
		}else{
			alert('Invlid Input');
		}
	})
	//amendment
	$('.req-amendment-btn').click(function(){
		const invitationID = $(this).attr('data-req-invit-amendment-id');
		if(invitationID) {
			$.confirm({
				title: 'Are you sure?',
				content: 'Are you sure want to send request to Admin?',
				buttons: {
				YES: function () {	
					$.ajax({
						type:'POST',
						url: "{{ url('request-amendment')}}",
						data: {invitation_id:invitationID},
						dataType: 'json',
						success:function(res) {
							if(res.status == 'success') {
								$('#amendment-option-no'+invitationID).hide();
								$('#amendment-option-yes'+invitationID).show();
							}
						}
					});
				},
        		NO: function () {}
    			}
			});
		}else{
			alert('Invlid Input');
		}
	})

	//cancelled appointment
	$('.req-cancel-btn').click(function(){		
		const invitationID = $(this).attr('data-appointment-cancel-id');
		
		if(invitationID) {
			$.confirm({
				title: 'Are you sure?',
				content: 'Are sure to cancel the appointment ?',
				buttons: {
				YES: function () {	
					$.ajax({
						type:'POST',
						url: "{{ url('cancel-appointment')}}",
						data: {invitation_id:invitationID},
						dataType: 'json',
						success:function(res) {
							if(res.status == 'success') {
								$('#scss_cancel_appointment').modal('show');
								//window.location.href=res.url;							
								setTimeout(function () {
										location.reload(true);
									}, 1000); 
							}
						}
					});
				},
        		NO: function () {}
    			}
			});
		}else{
			alert('Invlid Input');
		}
	})

	//resend mail to ptient
	$('.req-resend-mail-patient-btn').click(function(){		
		const invitationID = $(this).attr('data-resent-mail-patient-id');
		//alert(invitationID);
		
		if(invitationID) {
			$.confirm({
				title: 'Are you sure?',
				content: 'Are sure to Resend Email to Patient ?',
				buttons: {
				YES: function () {	
					$.ajax({
						type:'POST',
						url: "{{ url('resend-mail-to-patient')}}",
						data: {invitation_id:invitationID},
						dataType: 'json',
						success:function(res) {
							if(res.status == 'success') {
								$('#patient_resend_mail_modal').modal('show');
								//window.location.href=res.url;							
								setTimeout(function () {
										location.reload(true); 
									}, 1000); 
							}
						}
					});
				},
        		NO: function () {}
    			}
			});
		}else{
			alert('Invlid Input');
		}
	})
	
	});
</script>
<script>
$(document).ready(function() {

	//$('#dob').datepicker();
	
	$.validator.addMethod("validUsername", function (value, element) {
		return /^[0-9\s\+\-]+$/.test(value);
	}, "Please enter a valid number");
	
	$.validator.addMethod("maxDate", function(value, element) {
    var curDate = new Date();
    var inputDate = new Date(value);
    if (inputDate < curDate)
        return true;
    return false;
}, "Please select valid Date.");
$("#addpatient_form").validate({
rules: {
    fname: {
		required: true,
		maxlength:100,
	},
    lname: {
		required: true,
		maxlength:100,
	},
	email:{
		required:true,
		email:true,
		maxlength:80,
	},
	gender:{
		required:true,
	},
	dob:{
		required:true,
		maxDate:true,
	},
	doa:{
		required:true,
		maxDate:true,
	},
	address:{
		required:true,
		maxlength:300,
	},
	casetype:{
		required:false,
		maxlength:100,
	},
	phonenumber:{ 
		required:true,
		validUsername: true
		
	},
	terms:{
            required:true
        },
},
messages: {
    fname: {
    required: "Please enter first name"
    },
    lname: {
    required: "Please enter last name"
    },
  dob: {
    required: "Please select date of birth"
  },
  doa: {
    required: "Please select date of accident"
  },
  email: {
    required: "Please enter email",
	email: "Please enter valid email"
  },
   gender: {
    required: "Please choose gender",
  },
  address:{
            required:"Please enter address"
        },
	casetype:{
            required:"Please enter case type"
        },
	phonenumber:{
            required:"Please enter phone number",
            validUsername:"Please enter valid phone number",
        },
    terms:{
            required:"Please check and agree",
        },
},
submitHandler: function(form) {
$("#addpatient_form_button"). attr("disabled", true);	
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
$.ajax({
url: "<?=url('addpatient') ?>",
type: "POST",
data: $('#addpatient_form').serialize(),
beforeSend: function() {$('#addpatient_form_button').html('Please Wait');},
success: function( response ) {
$('#addpatient_form_button').html('Submit');
$("#addpatient_form_button"). attr("disabled", false);
if(response.status == 'success'){
        document.getElementById('successform').style.display = "block";
		//$("#successform").fadeOut(10000);
		//window.location.href=response.url;
		location.reload();
		setTimeout(function () {
			location.reload(true);
		  }, 10000); 
		document.getElementById("addpatient_form").reset();
		document.getElementById("addpatient_form_button").reset(); 
}
if(response.status == 'exist'){                              
	$('#emailexists').text(response.message);
}

}
});
}
})
});
</script>
<!------delete pic------->
<script>	
	$("#removeprofilepic").click(function(){
			//alert("test");
            $('#deleteprofilepicdiv').modal('show');
			document.getElementById('successtask1').style.display = "none";
			$("#deleteprofilepic_form").validate({
				rules: {
					userid: {
									required: false
								},
				},
				messages: {
					userid: {
									required: "Please Date"
								},
				},
				submitHandler: function(form) {
				$("#deleteprofilepic_btn"). attr("disabled", true);
				$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
				$.ajax({
				url: "{{url('deletelawprofilepic')}}",
				type: "POST",
				data: $('#deleteprofilepic_form').serialize(),
				dataType:'json',
				beforeSend: function() {$('#deleteprofilepic_btn').html('Please Wait');},
				success: function( response ) {
				$('#deleteprofilepic_btn').html('Submit');	
				   console.log(response.res);
					if(response.status == 'success'){
						console.log(response.res);
						$("#deleteprofilepic_btn"). attr("disabled", false);
						document.getElementById('successtask1').style.display = "block"; 
						document.getElementById("deleteprofilepic_form").reset();
						 window.location.href=response.url;
							//location.reload();
							setTimeout(function () {
								location.reload(true);
							  }, 2000); 
					}					
				}
				});
				}
				})
			/**/			
		  });  
</script>
<script type="text/javascript">
 $(document).ready(function() {	
		$.validator.addMethod("validUsername", function (value, element) {
			return /^[0-9\s\+\-]+$/.test(value);
		}, "Please enter a valid number");
		
		$(document).on('click', '.patient_edit_box #patient_edit', function () {
		
		$("#editPatient").modal('show');
		$("#editpatient_form").validate({
			rules: {
				fname: {
						required: true,
						maxlength:100,
					},
				lname: {
						required: true,
						maxlength:100,
					},
					email:{
						required:true,
						email:true,
						maxlength:80,
					},
					dob:{
						required:true,
						maxDate:true,
					},
					doa:{
						required:true,
						maxDate:true,
					},
					address:{
						required:true,
						maxlength:300,
					},
					casetype:{
						required:false,
						maxlength:100,
					},
					gender:{
						required:true,
					},
					phonenumber:{ 
							required:true,
							validUsername: true
							
						},
					filenumber:{ 
							required:false, 
						},
			},
			messages: {
				fname: {
				required: "Please enter first name"
			  },
				lname: {
				required: "Please enter first name"
			  },
			  dob: {
				required: "Please select data of birth"
			  },
			  doa: {
				required: "Please select data of accident"
			  },
			  email: {
				  email:'Please enter valid email',
				required: "Please enter email"
			  },
			  phone: {
				required: "Please enter phone"
			  },
			  address:{
						required:"Please enter address"
					},
			casetype:{
					required:"Please enter case type"
				},
			gender:{
					required:"Please choose gender"
				},
			phonenumber:{ 
				required:"Please enter phone number",
				validUsername: "Enter valid phone number",
				
			},
			},
			submitHandler: function(form) {
			$("#editpatient_form_button"). attr("disabled", true);	
			$.ajaxSetup({
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
			});
			$.ajax({
			url: "<?=url('editpatient') ?>",
			type: "POST",
			data: $('#editpatient_form').serialize(),
			beforeSend: function() {$('#editpatient_form_button').html('Please Wait');},
			success: function( response ) {
			$('#editpatient_form_button').html('Submit');
			$("#editpatient_form_button"). attr("disabled", false);
			if(response.status == 'success'){
				document.getElementById('successform').style.display = "block";
					//$("#successform").fadeOut(10000);
					    window.location.href=response.url;
						location.reload();
						setTimeout(function () {
							location.reload(true);
						  }, 10000); 
			}	
			
			document.getElementById("editpatient_form").reset();
			document.getElementById("editpatient_form_button").reset(); 
			
			}
			});
			}
			})
			
	});
});
</script> 
<script>
$(document).ready(function() {	
	$('.delete-patient-btn').click(function () {
     	var Button = $(this);
		
		$.confirm({
    title: 'Are you sure?',
    content: 'Are you sure you want to delete the client?',
    buttons: {
        confirm: function () {
			Button. attr("disabled", true);
		var patientId = Button.attr("data-id");			
		$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
		});
		
		$.ajax({
				type:'POST',
				url:"<?=url('/patientdelete') ?>",               
				data :{id: patientId},
				dataType:'json',
			   success:function(response) {
				   if(response.status== 'success'){ 
						
						Button. attr("disabled", false);
					    $('#scss_acc_pt').modal('show');
						$('#modal-inv-succes-img').css('filter','none');
					  	$('#acceptscss').css('display',"block");
					  	window.location.href=response.url;						
						
							setTimeout(function () {
								location.reload(true);
							  }, 2000); 
						
				   }
				   if(response.status== 'error'){ 
						
						Button. attr("disabled", false);
					    $('#scss_acc_pt_int').modal('show');
						
						$('#modal-inv-succes-img').css('filter','none');
						
					  	$('#acceptscss_int').css('display',"block");					  	
							setTimeout(function () {
								location.reload(true);
							  }, 2000); 
						
				   }
			   }
			});	
		 
        },
        cancel: function () {
            
        }
    }
});
	});
	});
</script>
<script type="text/javascript">
$(document).ready(function() {	
		$(document).on('change', '#invite_status', function () {			
			//alert( $('option:selected', this).val() );
			var status=$('option:selected', this).val();
			window.location.href="{{ url('/lawyer-dashboard/') }}/"+status;
			/*$.ajaxSetup({
					headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
						url: "{{url('/lawyer-dashboard')}}",
						type: "GET",
						data: {status:status},
						dataType:'json',
						success: function( response ) {
						console.log(response);
						//window.location.href=response.url/;
						}
				});	*/
			
			});
 });
</script> 
<!--------click invite doctor------->
<script>
function goToURL() {
  location.href = '{{ url('search-doctor') }}'; 
}
</script>
<!----click id of delete docs--->
<script type="text/javascript">
  function reply_click1(clicked_id, dataname,invitationID,patientID,activeliID)
  {
	//document.getElementById('dlte_scss').style.display = "none";
	$('#del-summary-btns-div').show();
      //alert(activeliID);
	  var intdid=clicked_id;	  
	  var intdname=dataname; 
	/// alert(intdname);	  
	  $('#DeleteDocsid').val(intdid);
	  $('#del-invitation-id').val(invitationID);
	  $('#DeleteDocsname').val(intdname);
	  $('#del-inv-patient_id').val(patientID);
	  $('#del-inv-activeli').val(activeliID);
	  $('#DeleteDocs').modal('show');
  }
  function reply_click_link(clicked_id)
  {
      var intdid=clicked_id;
	  $('#linkinvitationid').val(intdid);
	  $('#deltehyperlink').modal('show');
  }
  function reply_click_loe(clicked_id, dataname,invitationID,patientID,activeliID)
  {
	 // alert('hi');
	$('#del-loe-btns-div').show();
	//$('#loe_dlte_scss').hide();
     // alert(activeliID);
	  var intdid=clicked_id;	  
	  var intdname=dataname; 
	/// alert(intdname);	  
	  $('#loe-DeleteDocsid').val(intdid);
	  $('#loe-del-invitation-id').val(invitationID);
	  $('#loe-DeleteDocsname').val(intdname);
	  $('#loe-del-inv-patient_id').val(patientID);
	  $('#loe-activeli').val(activeliID);
	  $('#loeDelete').modal('show');
  }
  function reply_click_medicals(clicked_id, dataname,invitationID,patientID,activeliID)
  {
	  $('#del-medicals-btns-div').show();
	  var intdid=clicked_id;	  
	  var intdname=dataname; 	  
	  $('#medicals-DeleteDocsid').val(intdid);
	  $('#medicals-del-invitation-id').val(invitationID);
	  $('#medicals-DeleteDocsname').val(intdname);
	  $('#medicals-del-inv-patient_id').val(patientID);
	  $('#medicals-activeliid').val(activeliID);
	  $('#medicalsDelete').modal('show');
  }
  function reply_click_form53(clicked_id, dataname,invitationID,patientID,activeliID)
  {
	//alert(activeliID);
	//document.getElementById('form53_dlte_scss').style.display = "none";
	$('#del-form53-btns-div').show();     
	  var intdid=clicked_id;	  
	  var intdname=dataname; 
	/// alert(intdname);	  
	  $('#form53-DeleteDocsid').val(intdid);
	  $('#form53-del-invitation-id').val(invitationID);
	  $('#form53-DeleteDocsname').val(intdname);
	  $('#form53-del-inv-patient_id').val(patientID);
	  $('#form53-del-inv-activeli').val(activeliID);
	  $('#form53Delete').modal('show');
  }
   function reply_click_insform(clicked_id, dataname,invitationID,patientID,activeliID)
  {
    //  alert(activeliID);
	//document.getElementById('insform_dlte_scss').style.display = "none";
	$('#del-insform-btns-div').show();
	  var intdid=clicked_id;	  
	  var intdname=dataname; 
	/// alert(intdname);	  
	  $('#insform-DeleteDocsid').val(intdid);
	  $('#insform-del-invitation-id').val(invitationID);
	  $('#insform-DeleteDocsname').val(intdname);
	  $('#insform-del-inv-patient_id').val(patientID);
	  $('#insform-del-inv-activeli').val(activeliID);
	  $('#insformDelete').modal('show');
  }
</script> 
<!------Remove Doc------->
<script>	
    $(document).on('click',"#deletedoc_form_btn",function (e) {
		$("#deletedoc_form_btn"). attr("disabled", true);
			$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
				$.ajax({
				url: "{{url('deletedocument')}}",
				type: "POST",
				data: $('#deletedoc_form').serialize(),
				dataType:'json',
				success: function( response ) {
				$('#deletedoc_form_btn').html('Yes');	
				   console.log(response);
				   if(response.status == 'success'){
						document.getElementById('dlte_scss'+response.int_id).style.display = "block"; 
						$('#DeleteDocs').modal('hide');
						showAlertAfterDelete();
						$('#'+response.activeliid).hide();
							$('#del-summary-btns-div').hide();
							setTimeout(function () {								
								document.getElementById('dlte_scss'+response.int_id).style.display = "none"; 
							}, 2000);
						$("#deletedoc_form_btn"). attr("disabled", false);						
						document.getElementById("deletedoc_form").reset();
						
					}				
				}
				});
				
			/**/	
					
		  });  
		  /*delete hyperlink*/
		//   $("#deletehyperlink_form #deletehyperlink_form_btn").click(function(e){
		
		// 	$("#deletehyperlink_form_btn"). attr("disabled", false);
		// 	document.getElementById('hyper_dlte_scss').style.display = "none";
		// 	$.ajaxSetup({
		// 		headers: {
		// 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		// 		}
		// 		});
		// 		$.ajax({
		// 		url: "{{url('deletehyperlink')}}",
		// 		type: "POST",
		// 		data: $('#deletehyperlink_form').serialize(),
		// 		dataType:'json',
		// 		beforeSend: function() {$('#deletehyperlink_form_btn').html('Please Wait');},
		// 		success: function( response ) {
		// 		$('#deletehyperlink_form_btn').html('Submit');	
		// 		   console.log(response);
		// 			if(response.status == 'success'){
		// 				//console.log(response.res);
		// 				$("#deletehyperlink_form_btn"). attr("disabled", false);
		// 				document.getElementById('hyper_dlte_scss').style.display = "block"; 
		// 				document.getElementById("deletehyperlink_form").reset();
		// 				 window.location.href=response.url;
		// 					//location.reload();
		// 					setTimeout(function () {
		// 						location.reload(true);
		// 					  }, 1000); 
		// 			}					
		// 		}
		// 		});
				
		// 	/**/	
					
		//   });
		  /*end hyperlink*/

		  $(document).on('click',"#medicals_delete_form_btn",function (e) {
				$("#medicals_delete_form_btn"). attr("disabled", false);
				$.ajaxSetup({
					headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
					});
					$.ajax({
					url: "{{url('delete-medicals-file')}}",  
					type: "POST",
					data: $('#medicals_delete_form').serialize(),
					dataType:'json',
				
					success: function( response ) {
						if(response.status == 'success'){
							//console.log(response.activeliid);
							$('#'+response.activeliid).hide();
							showAlertAfterDelete();
							document.getElementById('medicals_dlte_scss'+response.int_id).style.display = "block"; 
							$('#medicalsDelete').modal('hide');
							//$('#del-medicals-btns-div').hide();
							setTimeout(function () {
								document.getElementById('medicals_dlte_scss'+response.int_id).style.display = "none"; 
							}, 2000);
							$("#medicals_delete_form_btn"). attr("disabled", false);							
							document.getElementById("medicals_delete_form").reset();	
						}					
					}
					});
	  		});
		  
		  /*delete loe*/
		  $("#loe_delete_form #loe_delete_form_btn").click(function(e){
		
			$("#loe_delete_form_btn"). attr("disabled", false);
		//	document.getElementById('loe_dlte_scss').style.display = "none";
			$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
				$.ajax({
				url: "{{url('deleteloe')}}",  
				type: "POST",
				data: $('#loe_delete_form').serialize(),
				dataType:'json',
				beforeSend: function() {$('#loe_delete_form_btn').html('Please Wait');},
				success: function( response ) {
				$('#loe_delete_form_btn').html('Yes');	
				   console.log(response);
					if(response.status == 'success'){
						document.getElementById('loe_dlte_scss'+response.int_id).style.display = "block"; 
						$('#loeDelete').modal('hide');
						showAlertAfterDelete();
						console.log(response.res);
						$('#'+response.activeliid).hide();
							//$('#del-loe-btns-div').hide();
							setTimeout(function () {								
								document.getElementById('loe_dlte_scss'+response.int_id).style.display = "none"; 
							}, 2000);
						$("#loe_delete_form_btn"). attr("disabled", false);
						
						document.getElementById("loe_delete_form").reset();
						
					}					
				}
				});
				
			/**/	
					
		  });
		  /*end loe*/
		  
		  /*delete form53*/
		  $("#form53_delete_form #form53_delete_form_btn").click(function(e){
		
			$("#form53_delete_form_btn"). attr("disabled", false);
			//document.getElementById('form53_dlte_scss').style.display = "none";
			$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
				$.ajax({
				url: "{{url('deleteform53')}}",  
				type: "POST",
				data: $('#form53_delete_form').serialize(),
				dataType:'json',
				beforeSend: function() {$('#form53_delete_form_btn').html('Please Wait');},
				success: function( response ) {
				$('#form53_delete_form_btn').html('Submit');	
				   console.log(response);
					if(response.status == 'success'){
						document.getElementById('form53_dlte_scss'+response.int_id).style.display = "block";
						$('#form53Delete').modal('hide'); 
						showAlertAfterDelete();
						$('#'+response.activeliid).hide();
							$('#del-form53-btns-div').hide();
							setTimeout(function () {								
								document.getElementById('form53_dlte_scss'+response.int_id).style.display = "none"; 
							}, 2000);

						$("#form53_delete_form_btn"). attr("disabled", false);						
						document.getElementById("form53_delete_form").reset();
						
					}					
				}
				});
				
			/**/	
					
		  });
		  /*end form53*/
		  
		  /*delete insform*/
		  $("#insform_delete_form #insform_delete_form_btn").click(function(e){
		
			$("#insform_delete_form_btn"). attr("disabled", false);
			//document.getElementById('insform_dlte_scss').style.display = "none";
			$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
				$.ajax({
				url: "{{url('deleteinsform')}}",  
				type: "POST",
				data: $('#insform_delete_form').serialize(),
				dataType:'json',
				beforeSend: function() {$('#insform_delete_form_btn').html('Please Wait');},
				success: function( response ) {
				$('#insform_delete_form_btn').html('Submit');	
				   console.log(response);
					if(response.status == 'success'){
						document.getElementById('insform_dlte_scss'+response.int_id).style.display = "block"; 
						$('#'+response.insformactiveliid).hide();
						   $('#insformDelete').modal('hide');
						   showAlertAfterDelete();
							//$('#del-insform-btns-div').hide();
							setTimeout(function () {								
								document.getElementById('insform_dlte_scss'+response.int_id).style.display = "none"; 
							}, 2000);
						$("#insform_delete_form_btn"). attr("disabled", false);						
						document.getElementById("insform_delete_form").reset();
						 
					}					
				}
				});
				
			/**/	
					
		  });
		  /*end form53*/
		   
</script>
<!------medicals linkss------->
<script>
	$(document).ready(function() {
		
		$(".medical-form").submit(function(e){
			e.preventDefault()
			var medical_url = $(this).find('.medical_url-input').val();
			var invitation_id = $(this).find('.invitation_id').val();
			var medicals_form='medicals_form_btn'+invitation_id;
			var medicals_form_btn='medicals_form_btn'+invitation_id;
			//alert(medicals_form_btn);	
				
			$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$("#medicals_form_btn"+invitation_id). attr("disabled", true);
			$.ajax({
					url: "{{url('/medicals')}}",
					type: "POST",
					data: {medicals_hyperlink:medical_url,invitation_id:invitation_id},
					dataType:'json',
					success: function( response ) {
					console.log(response);
					$("#medicals_form_btn"+invitation_id). attr("disabled", false);
					document.getElementById('hyper_dlte_scss').style.display = "block"; 
					window.location.href=response.url;
					setTimeout(function () {
						location.reload(true);
					  }, 1000);
					}
			});
			
		});
})
</script>
<!-----upload letter of enagagement----->
<script>
	$(document).ready(function (e) {	
		
		$('.upload_loe_btn').click(function(){
			$('#loe-upload-file-invitation-id').val($(this).attr('data-initation-id'));
			$('#loe-upload-file-patient-id').val($(this).attr('data-patient-id'));
		});
	});

	$.ajaxSetup({
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
	});


function makeSingleCall_Letter(curret,total){
   let totalFiles = $('#loefiles')[0].files.length; //Total files
   let files = $('#loefiles')[0];
   var filenames = $('#loefiles')[0].files;

	var progress = `<div  class="progress mt-2"><div id="fprog_l_${curret}" class="progress-bar" style='background:#323B62' role="progressbar" style="width: 5%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">0% </div> </div><p>${filenames[curret]["name"]}</p>`;
	$('#progress_bar_letter').append(progress);
    
	var loenewintid = $("#loe-upload-file-invitation-id").val();
    var loepatientid = $("#loe-upload-file-patient-id").val();

    var formData = new FormData();
	formData.append('loenewintid', loenewintid);
	formData.append('loepatientid', loepatientid);
	formData.append('file', files.files[curret]);

	$.ajax({
			xhr: function() {
			    var xhr = new window.XMLHttpRequest();
				xhr.onreadystatechange = function () {
                    var rdState = xhr.readyState
                    var progress = 25 * rdState;
                    $("#fprog_l_"+curret).css('width',progress+"%");
                    $("#fprog_l_"+curret).text(progress+"%");
                }
				return xhr;
			},
			type:'POST',
			url: "{{ url('/uploadLetterOfEngagementDocs')}}",
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: (data) => {
				if(curret+1 < total){
					makeSingleCall_Letter(curret+1,total)
				}
				else{
					$("#loefiles").val(null);
					var url = "{{url()->full()}}";
					url = url.replace('&amp;','');
					if(url.includes('tab')){
					url = url.split("tab")[0];
					}
					url += "&tab=3";
					location.replace(url);
				}
			},
			error: function(data){
				$('#fileuploadError_letter').empty();
			    $("#fileuploadError_letter").show();
				$("#fileuploadError_letter").text('Error occured while uploading files, try again');
				$("#loefiles").val(null);
				$("#submit").show();

				setTimeout(() => {
					$("#fileuploadError_letter").hide();
				}, 5000);
			
			}
	});
}


	$("#submit").on('click',function(){
         //abs
		$("#submit").hide();
		$("#fileuploadError_letter").hide();
        $("#progress_bar_letter").empty();

		let totalFiles = $('#loefiles')[0].files.length; //Total files
        let files = $('#loefiles')[0];
        var filenames = $('#loefiles')[0].files;

		if(totalFiles > 0){
			const allowed = [
				"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
				"application/msword",
				"application/pdf",
			];

			var error = false;

			// --- File type check start
			for(let i = 0; i < totalFiles; i++){
				if(!allowed.includes(files.files[i].type)){
					$("#fileuploadError_letter").show();
					$("#fileuploadError_letter").text('Invalid file type, You can only upload pdf, doc, docx');
					$("#loefiles").val(null); // to clear all select files
					setTimeout(() => {
					$("#fileuploadError_letter").hide();
				}, 5000);
					error = true;
					break;
				}else{
					error = false;
				}
			}
			// --- File type check end

		   if(error == false){
				makeSingleCall_Letter(0,totalFiles);
				$('#submit').hide();
			}else{
				$("#submit").show();
			}

		}

	});

</script>




<!-----upload letter of form53----->
<script>
	$(document).ready(function (e) {	
		
		$('.upload_form53_btn').click(function(){
			$('#form53-upload-file-invitation-id').val($(this).attr('data-initation-id'));
			$('#form53-upload-file-patient-id').val($(this).attr('data-patient-id'));
		});
	});
	$.ajaxSetup({
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
	});


function makeSingleCall_Form53(curret,total){
   let totalFiles = $('#form53files')[0].files.length; //Total files
   let files = $('#form53files')[0];
   var filenames = $('#form53files')[0].files;

	var progress = `<div  class="progress mt-2"><div id="fprog_f3_${curret}" class="progress-bar" style='background:#323B62' role="progressbar" style="width: 5%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">0% </div> </div><p>${filenames[curret]["name"]}</p>`;
	$('#progress_bar_form53').append(progress);
    
	var form53newintid = $("#form53-upload-file-invitation-id").val();
    var form53patientid = $("#form53-upload-file-patient-id").val();

    var formData = new FormData();
	formData.append('form53newintid', form53newintid);
	formData.append('form53patientid', form53patientid);
	formData.append('file', files.files[curret]);

	$.ajax({
			xhr: function() {
			    var xhr = new window.XMLHttpRequest();
				xhr.onreadystatechange = function () {
                    var rdState = xhr.readyState
                    var progress = 25 * rdState;
                    $("#fprog_f3_"+curret).css('width',progress+"%");
                    $("#fprog_f3_"+curret).text(progress+"%");
                }
				return xhr;
			},
			type:'POST',
			url: "{{ url('/uploadForm53Docs')}}",
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: (data) => {
				if(curret+1 < total){
					makeSingleCall_Form53(curret+1,total)
				}
				else{
					$("#submit_form53").val(null);
					var url = "{{url()->full()}}";
					url = url.replace('&amp;','');
					if(url.includes('tab')){
					url = url.split("tab")[0];
					}
					url += "&tab=4";
					location.replace(url);
				}
			},
			error: function(data){
				$('#fileuploadError_letter').empty();
			    $("#fileuploadError_form53").show();
				$("#fileuploadError_form53").text('Error occured while uploading files, try again');
				$("#form53files").val(null);
				$("#submit_form53").show();

				setTimeout(() => {
					$("#fileuploadError_form53").hide();
				}, 5000);
			
			}
	});
}

 
	$("#submit_form53").on('click',function(){
		$("#submit_form53").hide();
		$("#fileuploadError_form53").hide();
        $("#progress_bar_form53").empty();

		let totalFiles = $('#form53files')[0].files.length; //Total files
        let files = $('#form53files')[0];
        var filenames = $('#form53files')[0].files;

		if(totalFiles > 0){
			const allowed = [
				"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
				"application/msword",
				"application/pdf",
			];

			var error = false;

			// --- File type check start
			for(let i = 0; i < totalFiles; i++){
				if(!allowed.includes(files.files[i].type)){
					$("#fileuploadError_form53").show();
					$("#fileuploadError_form53").text('Invalid file type, You can only upload pdf, doc, docx');
					$("#form53files").val(null); // to clear all select files
					setTimeout(() => {
					$("#fileuploadError_form53").hide();
				}, 5000);
					error = true;
					break;
				}else{
					error = false;
				}
			}
			// --- File type check end

		   if(error == false){
				makeSingleCall_Form53(0,totalFiles);
				$('#submit_form53').hide();
			}else{
				$("#submit_form53").show();
			}

		}

	});

</script>





<!-----upload insform----->
<script>
	$(document).ready(function (e) {		
		$('.upload_insform_btn').click(function(){
			$('#insform-upload-scss').hide();
			$('#insform-upload-error').hide();
			$('#insform-upload-file-invitation-id').val($(this).attr('data-initation-id'));
			$('#insform-upload-file-patient-id').val($(this).attr('data-patient-id'));
		});
	});
	$.ajaxSetup({
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
	});

function makeSingleCall_Questionair(curret,total){
   let totalFiles = $('#insformfiles')[0].files.length; //Total files
   let files = $('#insformfiles')[0];
   var filenames = $('#insformfiles')[0].files;

	var progress = `<div  class="progress mt-2"><div id="fprog_l_${curret}" class="progress-bar" style='background:#323B62' role="progressbar" style="width: 5%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">0% </div> </div><p>${filenames[curret]["name"]}</p>`;
	$('#progress_bar_Questionair').append(progress);
    
	var insformnewintid = $("#insform-upload-file-invitation-id").val();
    var insformpatientid = $("#insform-upload-file-patient-id").val();

    var formData = new FormData();
	formData.append('insformnewintid', insformnewintid);
	formData.append('insformpatientid', insformpatientid);
	formData.append('file', files.files[curret]);

	$.ajax({
			xhr: function() {
			    var xhr = new window.XMLHttpRequest();
				xhr.onreadystatechange = function () {
                    var rdState = xhr.readyState
                    var progress = 25 * rdState;
                    $("#fprog_l_"+curret).css('width',progress+"%");
                    $("#fprog_l_"+curret).text(progress+"%");
                }
				return xhr;
			},
			type:'POST',
			url: "{{ url('/uploadinsform')}}",
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: (data) => {
				if(curret+1 < total){
					makeSingleCall_Questionair(curret+1,total)
				}
				else{
					$("#insformfiles").val(null);
					var url = "{{url()->full()}}";
					url = url.replace('&amp;','');
					if(url.includes('tab')){
					url = url.split("tab")[0];
					}
					url += "&tab=5";
					location.replace(url);
				}
			},
			error: function(data){
				$('#progress_bar_Questionair').empty();
			    $("#fileuploadError_Questionair").show();
				$("#fileuploadError_Questionair").text('Error occured while uploading files, try again');
				$("#insformfiles").val(null);
				$("#submit_Questionair").show();

				setTimeout(() => {
					$("#fileuploadError_Questionair").hide();
				}, 5000);
			
			}
	});
}


	$("#submit_Questionair").on("click",function(){
	
	    $("#submit_Questionair").hide();
		$("#fileuploadError_Questionair").hide();
        $("#progress_bar_Questionair").empty();

		let totalFiles = $('#insformfiles')[0].files.length; //Total files
        let files = $('#insformfiles')[0];
        var filenames = $('#insformfiles')[0].files;

		if(totalFiles > 0){
			const allowed = [
				"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
				"application/msword",
				"application/pdf",
			];

			var error = false;

			// --- File type check start
			for(let i = 0; i < totalFiles; i++){
				if(!allowed.includes(files.files[i].type)){
					$("#fileuploadError_Questionair").show();
					$("#fileuploadError_Questionair").text('Invalid file type, You can only upload pdf, doc, docx');
					$("#insformfiles").val(null); // to clear all select files
					setTimeout(() => {
					$("#fileuploadError_Questionair").hide();
				}, 5000);
					error = true;
					break;
				}else{
					error = false;
				}
			}
			// --- File type check end

		   if(error == false){
			    makeSingleCall_Questionair(0,totalFiles);
				$('#submit_Questionair').hide();
			}else{
				$("#submit_Questionair").show();
			}
		}

	});
	
	
	$( document ).ready(function() {
		$('.invitation-tab').addClass('active');
	});

	</script>
	<!-----upload letter of insform----->
	<script>
		$(document).ready(function (e) {		
			$('.upload_medical_btn').click(function(){
				$('#medicals-upload-file-invitation-id').val($(this).attr('data-initation-id'));
				$('#medicals-upload-file-patient-id').val($(this).attr('data-patient-id'));
		    });
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

function makeSingleCall(curret,total){
   let totalFiles = $('#medicalsfiles')[0].files.length; //Total files
   let files = $('#medicalsfiles')[0];
   var filenames = $('#medicalsfiles')[0].files;

	var progress = `<div  class="progress mt-2"><div id="fprog_${curret}" class="progress-bar" style='background:#323B62' role="progressbar" style="width: 5%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">0% </div> </div><p>${filenames[curret]["name"]}</p>`;
	$('#progress_bar').append(progress);
    
	var medicalsnewintid = $("#medicals-upload-file-invitation-id").val();
    var medicalspatientid = $("#medicals-upload-file-patient-id").val();
    var formData = new FormData();
	formData.append('medicalsnewintid', medicalsnewintid);
	formData.append('medicalspatientid', medicalspatientid);
	formData.append('file', files.files[curret]);

	$.ajax({
			xhr: function() {
			    var xhr = new window.XMLHttpRequest();
				xhr.onreadystatechange = function () {
                    var rdState = xhr.readyState
                    var progress = 25 * rdState;
                    $("#fprog_"+curret).css('width',progress+"%");
                    $("#fprog_"+curret).text(progress+"%");
                }
				return xhr;
			},
			type:'POST',
			url: "{{ url('/upload-medicals')}}",
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: (data) => {
				console.log(data)
				if(curret+1 < total){
					makeSingleCall(curret+1,total)
				}
				else{
					$("#medicalsfiles").val(null);
					var url = "{{url()->full()}}";
					url = url.replace('&amp;','');
					if(url.includes('tab')){
					url = url.split("tab")[0];
					}
					url += "&tab=1";
					location.replace(url);
				}
			},
			error: function(data){
				$('#progress_bar').empty();
			    $("#fileuploadError").show();
				$("#fileuploadError").text('Error occured while uploading files, try again');
				$("#medicalsfiles").val(null);
				$("#patient_submit").show();

				setTimeout(() => {
					$("#fileuploadError").hide();
				}, 5000);
			}
		});
}

	$( document ).ready(function() {
	    //$('.nav-tabs a[href="#new-invitations155"]').tab('show')
	});



//  ------------------------- New file uploading start ----------------------------
$("#submit_medical_upload").on('click',function(){
    $("#fileuploadError").hide();
    $("#progress_bar").empty();
    $('#medicals-upload-error').text('');
	$('#medicals-upload-scss').text('');
	$('#medicals-upload-error').text('');

   let totalFiles = $('#medicalsfiles')[0].files.length; //Total files
   let files = $('#medicalsfiles')[0];
   var filenames = $('#medicalsfiles')[0].files;

   if(totalFiles > 0){
    const allowed = [
     "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
	 "application/msword",
	 "application/pdf",
	];

	var error = false;

	// --- File type check start
	for(let i = 0; i < totalFiles; i++){
		if(!allowed.includes(files.files[i].type)){
            $("#fileuploadError").show();
            $("#fileuploadError").text('Invalid file type, You can only upload pdf, doc, docx');
			$("#medicalsfiles").val(null); // to clear all select files
		    error = true;
			break;
		}else{
			error = false;
		}
	}
	// --- File type check end
    if(error == false){
		makeSingleCall(0,totalFiles);
		$('#submit_medical_upload').hide();
	}
   }
});
//  ------------------------- New file uploading end   ----------------------------

</script>
<script>
    $('#hideshow').click(function(){
	   $('.invitation-tabs').toggle('slow');
	});
	$(document).ready(function(){
	    $("#hideshow").click(function(){
	        $(this).toggleClass("highlight");
	    });
	});
</script>

<script>
	function myFunction() {
	  document.getElementById("myDropdown").classList.toggle("show");
	}

	function myFunctionAppointments() {
	  document.getElementById("myDropdownAppointments").classList.toggle("show");
	}
	
	window.onclick = function(e) {
	  if (!e.target.matches('.dropbtn')) {
	  var myDropdown = document.getElementById("myDropdown");
	    if (myDropdown?.classList?.contains('show')) {
	      myDropdown.classList.remove('show');
	    }
	  }
	  if (!e.target.matches('.dropbtnapp')) {
	  var myDropdownAppointments = document.getElementById("myDropdownAppointments");
	    if (myDropdownAppointments?.classList.contains('show')) {
	      myDropdownAppointments?.classList.remove('show');
	    }
	  }
	}
</script>
<script>
	function openCity(evt, cityName) {
	  var i, tabcontent, tablinks;
	  tabcontent = document.getElementsByClassName("tabcontent");
	  for (i = 0; i < tabcontent.length; i++) {
	    tabcontent[i].style.display = "none";
	  }
	  tablinks = document.getElementsByClassName("tablinks");
	  for (i = 0; i < tablinks.length; i++) {
	    tablinks[i].className = tablinks[i].className.replace(" active", "");
	  }
	  document.getElementById(cityName).style.display = "block";
	  if(evt ==''){
		  $('#files-reports-menu').addClass('active');
		}
		if(evt ==1){
		  $('#files-reports-menu-app').addClass('active');
		}
		if((evt !='')&&(evt !=1)){
			 $('#client-information').addClass('active');
		}
	}
</script>
@if(request()->doctorid)
<script>
	openCity('', 'FilesReports');
</script>
@endif
@if(request()->appdate)
<script>
	openCity('1', 'Appointments');
</script>
@endif



<script>
$(function() {
    $( "#dob").datepicker({
		maxDate:'-36m',
		dateFormat: "yy-mm-dd",
		buttonImage: "{{asset('img/calendar_icon.png')}}"
	});
	
    $( "#doa").datepicker({dateFormat: "yy-mm-dd"});
});





$(document).on('click','.changedStatus',function(){
	let id = $(this).data('id');
	let status = $(this).data('status');

	if(!isEmpty(id) && !isEmpty(status)){
        $.confirm({
				title: 'Are you sure?',
				content: 'Do you want to '+status+' this appointment?',
				buttons: {
				YES: function () {	
					$("#loader_"+id).show();
					$("#approveBtn_"+id).addClass('hideBtn');
	                $("#rejectBtn_"+id).addClass('hideBtn');
					$.ajax({
						type:'POST',
						url: "{{ url('changeInvitationStatus')}}",
						data: {ID:id,status:status},
						dataType: 'json',
						success:function(res) {
							$("#loader_"+id).hide();
							$("#approveBtn_"+id).removeClass('hideBtn');
	                        $("#rejectBtn_"+id).removeClass('hideBtn');
							$("#approveBtn_"+id).addClass('showBtn');
	                        $("#rejectBtn_"+id).addClass('showBtn');

							$.confirm({
								title: 'Alert Message',
								content: res.message,
								buttons: {Ok: function () {
                                    if(res.status == 'success'){
										location.reload();
									}
								}}
							});
						},
						error:function(err){
							$("#loader_"+id).hide();
							$("#approveBtn_"+id).removeClass('hideBtn');
	                        $("#rejectBtn_"+id).removeClass('hideBtn');
							$("#approveBtn_"+id).addClass('showBtn');
	                        $("#rejectBtn_"+id).addClass('showBtn');
							alert("Something went try again");
						}
					});
				},
        		NO: function () {}
    			}
			});
	}
	else{
		alert("Invalid data provided!");
	}
});



</script>


<?php if(isset($_GET['tab'])) { ?>
	<script>
		var tab_id = "<?php echo $_GET['tab']; ?>";
		$("#patient_lawyer_"+tab_id).click();
	</script>
<?php } ?>