<script>
    $("#addNewLawyerType").on("click",function(){
   let name = $("#lawyerTypeName").val();
   let commission = $("#lawyerTypeCommission").val();
   let id = $("#lawyerTypeID").val();

   if(!isEmpty(name) && !isEmpty(commission)){
      if(isNum(commission)){
        $(".loadingGif").show();
        $("#addNewLawyerType").hide();
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });

            $.ajax({
				url: "{{url('admin/addLawyerType')}}",
				type: "POST",
				data: {name:name,commission:commission,id:id},
				dataType:'json',
				success: function( response ) {
                    $(".loadingGif").hide();
                    $("#addNewLawyerType").show();
					if(response.status == 'success'){						
					   msg_success(response.message);
                       setTimeout(() => {
                         location.reload();
                       }, 3000);
					}
                    else if(response.status == 'error'){
						msg_error(response.message);
					}	 				
				},
                error:function(){
                    $(".loadingGif").hide();
                    $("#addNewLawyerType").show();
                    msg_error("Something went wrong try again");
                }
			});

      }
      else{
        msg_error("Enter valid commission value");
      }
   }
   else{
      msg_error("Please enter name and commission");
   }
});



// ******************** Add or Update City ********************
$("#addNewCitySubmit").on("click",function(){
   let name = $("#addCityName").val();
   let id = $("#addCityID").val();

   if(!isEmpty(name)){
        $(".loadingGif").show();
        $("#addNewCitySubmit").hide();
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });

            $.ajax({
				url: "{{url('admin/addCity')}}",
				type: "POST",
				data: {name:name,id:id},
				dataType:'json',
				success: function( response ) {
                    $(".loadingGif").hide();
                    $("#addNewCitySubmit").show();
					if(response.status == 'success'){						
					   msg_success(response.message);
                       setTimeout(() => {
                         location.reload();
                       }, 3000);
					}
                    else if(response.status == 'error'){
						msg_error(response.message);
					}	 				
				},
                error:function(){
                    $(".loadingGif").hide();
                    $("#addNewCitySubmit").show();
                    msg_error("Something went wrong try again");
                }
			});
   }
   else{
      msg_error("Please enter city name first");
   }
});


// ******************** Add or Update Category ********************
$("#addNewCategorySub").on("click",function(){
   let name = $("#addCategoryName").val();
   let id = $("#addCategoryID").val();

   if(!isEmpty(name)){
        $(".loadingGif").show();
        $("#addNewCategorySub").hide();
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });

            $.ajax({
				url: "{{url('admin/addCategory')}}",
				type: "POST",
				data: {name:name,id:id},
				dataType:'json',
				success: function( response ) {
                    $(".loadingGif").hide();
                    $("#addNewCategorySub").show();
					if(response.status == 'success'){						
					   msg_success(response.message);
                       setTimeout(() => {
                         location.reload();
                       }, 3000);
					}
                    else if(response.status == 'error'){
						msg_error(response.message);
					}	 				
				},
                error:function(){
                    $(".loadingGif").hide();
                    $("#addNewCategorySub").show();
                    msg_error("Something went wrong try again");
                }
			});
   }
   else{
      msg_error("Please enter category name first");
   }
});



// ******************** Add or Update Desgination ********************
$("#addDesingationSubmit").on("click",function(){
   let name = $("#addDesingationName").val();
   let id = $("#addDesginationID").val();

   if(!isEmpty(name)){
        $(".loadingGif").show();
        $("#addDesingationSubmit").hide();
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });

            $.ajax({
				url: "{{url('admin/addDesgination')}}",
				type: "POST",
				data: {name:name,id:id},
				dataType:'json',
				success: function( response ) {
                    $(".loadingGif").hide();
                    $("#addDesingationSubmit").show();
					if(response.status == 'success'){						
					   msg_success(response.message);
                       setTimeout(() => {
                         location.reload();
                       }, 3000);
					}
                    else if(response.status == 'error'){
						msg_error(response.message);
					}	 				
				},
                error:function(){
                    $(".loadingGif").hide();
                    $("#addDesingationSubmit").show();
                    msg_error("Something went wrong try again");
                }
			});
   }
   else{
      msg_error("Please enter desgination name first");
   }
});


// #################### Approved Doctor & Lawyer Account ##########################
$(".changeLawyerStatusBtn").on("click",function(){
   let type = $(this).data('type');
   let id = $(this).data('id');

   if(type == 2){
      var lawyerType = $('#lawyerType_'+id+' option:selected').val();
      var selectedName = $('#lawyerType_'+id+' option:selected').text();
     if(isEmpty(lawyerType)){
        msg_toast("Please select lawyer type or define new lawyer types",1);
        return;
     }
   }
   else{
    var lawyerType = 0;
    var selectedName = '';
   }

   
  
   $("#loader_"+id).show();
   $("#changeLawyerStatusBtn_"+id).hide();
   
   $.ajaxSetup({
       headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
    });

         $.ajax({
				url: "{{url('admin/changesLawyerStatus')}}",
				type: "POST",
				data: {id:id,type:type,lawyerType:lawyerType},
				dataType:'json',
				success: function( response ) {
					if(response.status == 'success'){						
                        msg_toast(response.message);
                        $("#loader_"+id).hide();
                        $("#"+id).hide();
                        if(type == 2){
                            $("#typesRow_"+id).empty();
                            $("#typesRow_"+id).html(selectedName);
                        }
					}
                    else if(response.status == 'error'){
						msg_toast(response.message,1);
                        $("#loader_"+id).hide();
                        $("#changeLawyerStatusBtn_"+id).show();
					}	 				
				},
                error:function(){
                    $("#loader_"+id).hide();
                    $("#changeLawyerStatusBtn_"+id).show();
                    msg_toast("Something went wrong try again",1);
                }
			});

});


$(".changePriceRequesStatus").on('click',function(){
   var id = $(this).data("id");
   var status = $(this).data("status");
   var row = $("#prRow_"+id);
   var loader = $("#pr_loader_"+id);
   var accept_btn = $("#priceRequestSuccess_"+id);
   var reject_btn = $("#priceRequestReject_"+id);

               $.ajaxSetup({
					headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

                $.ajax({
				url: "{{url('/changePriceRequestStatus')}}",
				type: "POST",
				data: {
					id:id,
					status:status,
				},
				dataType:'json',
				beforeSend: function() {
                    accept_btn.hide();
                    reject_btn.hide();
                    loader.show();
				},
				success: function( response ) {
                    msg_toast(response.message);
                    loader.hide();	
                    row.remove();				
				},
				error:function(error){
					msg_toast("Something went wrong try again",1);
                    accept_btn.show();
                    reject_btn.show();
                    loader.hide();
				}
			  });

});


</script>