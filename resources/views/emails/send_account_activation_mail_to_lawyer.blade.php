@extends("includes.email_container")
@section("content")
                <div>

                        <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Dear {{ ucfirst($user->name) }},</h3>

                        <p style="font-size: 14px;line-height: 17px;">Thank you for registering with the ProAssessors. Please click the link below to activate your email and login with the website.</p>
                     
                        
               <div style="text-align: center; width:100%; margin:20px 0px;"> <a style="background: linear-gradient(254.35deg, #515C84 39.06%, rgba(81, 92, 132, 0.42) 60.46%);
                    border-radius: 20px;
                    padding: 9px 30px;
                    color: white;
                    text-decoration: none;
                    font-size: 17;
                    font-weight: bold;" 
                      href="{{url('activate_account')}}/{{$user->email_verify_token}}" target='_blank'>Click Here</a>
                </div> 

                        <br/>  

                        <br/>

                        <br/>

                        <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
                        <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 

                </div>  

@endsection