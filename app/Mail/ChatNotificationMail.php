<?php
namespace App\Mail;
use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
class ChatNotificationMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * The order instance.
     *
     * @var \App\Models\Order
     */
    public $user;
    public $sender;
    /**
     * Create a new message instance.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function __construct($user,$sender)
    {
        $this->user = $user;
        $this->sender = $sender;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $fromAddress = Config::get('constants.MAIL_FROM_ADDRESS');
        $site_title = Config::get('constants.site_title');
        $fronend_url = Config::get('constants.FRONTEND_URL');
        return $this->subject('You have received new Message from '.ucfirst($this->sender->name))->from($fromAddress,$site_title)->view('emails.send_unread_message_notification',['user'=>$this->user,'sender'=>$this->sender,'frontend_url'=>$fronend_url]);
    }
}