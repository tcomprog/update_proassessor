@extends('layouts.admin_app')

@section('content')

<div class="container-fluid">
    <div class="row">
	    <div class="col-md-12">
			 <nav aria-label="breadcrumb">
			  <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>
				<li class="breadcrumb-item active"><a href="{{url('/manage_appointments')}}">Manage Appointments</a></li>
			  </ol>
			</nav>
		</div>
		<div class="col-md-12">  
			<!----search-------------->
				<form method="GET" action="{{ route('manage_appointments') }}" >
					<div class="form-group row">	    
						
						<div class="col-md-5">
						   
							<input id="keyword" type="text" class="form-control @error('keyword') is-invalid @enderror" name="keyword" id="keyword" placeholder="Search Name" value="<?php if(isset($_GET['keyword'])){echo $_GET['keyword'];}?>"  autocomplete="keyword" autofocus>

							<input  type="hidden" class="form-control @error('keyword') is-invalid @enderror" name="id" id="id" value="<?php if(isset($_GET['id'])){echo $_GET['id'];}?>" >

							@error('keyword')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>          

						<div class="col-md-3">
							<button type="submit" class="btn btn-primary">
								{{ __('Search') }}
							</button>
							<?php  if(isset($_GET['keyword'])){  ?>
							<a href="{{url('/manage_appointments')}}" style="margin-left: 10px;color: #00528c;">
								Clear
							</a>
							<?php }  ?>
						</div>
					</div>
				</form>
		</div>
	</div>
	   <div class="row justify-content-center">
         <div class="container-fluid">
			<div class="card">
				 <div class="card-header">
					Manage Appointments
				  </div>
		  <div class="card-body">
				<div class="row justify-content-center">
					<div class="col-md-12">                   
						<table   id="myTable" class="table table-bordered table-striped" style="width:100%">
							<tr>
							<td>S.No</td>
							<td>Doctor Name</td>
							<td>Lawyer Name</td>
							<td>Appointment Date</td>                       
							<td>Patient Name</td>
							<td>Doctor's Invoice</td>
							<td>Lawers's Invoice</td>
							<td>PaymentStatus</td>
							<td>Appointment Status</td>
							</tr>
							@if($userint->isNotEmpty())
							@foreach ($userint as $userint1)
							<tr>						
							<td>{{ $number }}</td>
							 <?php $number++; ?>
							 <td>{{ @ucfirst($userint1->user1->name) }}</td>
							 <td>{{ @ucfirst($userint1->user->name) }} - {{$userint1->user->lawyer_type}}</td>
							<td><?php echo date('d F Y', strtotime($userint1->invitation_date)); ?><?php  if($userint1->appointment_type=='virtual'){ ?><br><b style="font-size: 0.9rem;color: #0e3358;font-weight: 500;"> - Virtual</b><?php  } ?></td>					
							<td>
								{{ ucfirst(@$userint1->patient->fname)." ".ucfirst(@$userint1->patient->lname)  }}
							</td>
							<td>
								<?php  if($userint1->status !='cancelled') { if(isset($userint1->invoices)&&($userint1->invoices->invoice_filename!=null)){  ?>
								<a href="{{url('/uploads/custom_invoice')}}/{{$userint1->invoices->invoice_filename}}" download="invoice_{{$userint1->invoices->invoice_filename}}"target="_blank" style="color: #00528c;font-weight: 500;">Download Invoice</a>	<?php }  } ?>	
							</td>	
							<td>
								<?php if($userint1->status !='cancelled') { if(isset($userint1->invoices)&&($userint1->invoices->invoice_filename!=null)){  ?>

									<?php  if($userint1->invoices->lawyer_invoice_filename!=null){ ?>
										
											<a href="{{url('/uploads/lawyer_custom_invoice')}}/{{$userint1->invoices->lawyer_invoice_filename}}" download="invoice_{{$userint1->invoices->lawyer_invoice_filename}}"target="_blank" style="color: #00528c;font-weight: 500;">Download Invoice</a>
											
										<?php  } ?>
									<form id="generateinvoiceform{{$userint1->id}}">
											@csrf
										<div class="row">
											<div class="col-md-12">
												<label>Bill Amount</label>
												<input type="number" name="billamt" id="billamt{{$userint1->id}}" class="form-control" placeholder="Enter Bill Amount" data-id="{{ $userint1->id }}"  value="{{ $userint1->invoices->lawyer_payment_amount }}">
												<label>Other Costs</label>
												<input type="number" name="othercosts" id="othercosts{{$userint1->id}}" class="form-control" placeholder="Enter Other Costs" data-id="{{ $userint1->id }}" value="{{ $userint1->invoices->othercosts }}">
												<input type="hidden" name="docid" id="docid{{$userint1->id}}" value="{{$userint1->user1->id}}" >
											</div>
											<div class="col-md-12">												
												<br>
												<?php  if(isset($userint1->invoices)&&($userint1->invoices->lawyer_invoice_filename==null)){ ?>
													<a href="javascript:void(0)" onClick="reply_click_loe(this)" id="{{ $userint1->id }}" style="color: #00528c;font-weight: 500;color: #ffffff; font-weight: 500;padding: 3px 0px; border-radius: 9px;  background: #00528c;" class="validate" data-id="{{ $userint1->id }}" >Generate Invoice</a>	 
												<?php } else{ ?>
													<a href="javascript:void(0)" onClick="reply_click_loe(this)" id="{{ $userint1->id }}" style="color: #00528c;font-weight: 500;color: #ffffff; font-weight: 500;padding: 3px 9px; border-radius: 9px;  background: #00528c;" class="validate" data-id="{{ $userint1->id }}" >Update Invoice</a>	
												<?php }  ?>
											<div>											
										</div>
									</form>
								<?php }  } ?>	
							</td>				
							<td><?php  if($userint1->status !='cancelled') { if($userint1->invoices!=null){ ?>
								<select class="form-control" id="paymentstatus">
									 <option value="pending" <?php if($userint1->invoices->payment_status=="pending"){echo "selected";} ?>>Payment Due</option>
									 <option value="success" <?php if($userint1->invoices->payment_status=="success"){echo "selected";} ?>>Payment Done</option>	
								</select>
								<input type="hidden" id="invitation_id" value="{{ $userint1->id  }}">
								<input type="hidden" id="doctor_user_id" value="{{ $userint1->invitee_user_id }}">
								<?php  }  } ?>
							</td>
							<td><?php 
								if($userint1->status=="pending" && strtotime($userint1->created_at) < $expire_time) {
									echo "Expired";
								} elseif($userint1->status=="pending"){
									echo "Sent/Received";
								}elseif($userint1->status=="rejected" && $userint1->rejected_by=="doctor"){
									echo "Rejected by Doctor";
								}elseif($userint1->status=="rejected" && $userint1->rejected_by=="patient"){
									echo "Rejected by Patient";
								}elseif($userint1->status=="active" && $userint1->is_patient_accepted=="no"){
									echo "Accepted";	
									echo "<p style='color: #00528c;font-size: 11px;font-weight: 500;'>(Patient Approval Pending)</p>";							
								}elseif($userint1->status=="active" && $userint1->is_patient_accepted=="yes"){
									echo "Confirmed";								
								}elseif($userint1->status=="cancelled"){ 
									echo "Cancelled";								
								}
							 ?>
							</td>
							</tr>
							 @endforeach
							@else 
								<div>
									<h2>No Records found</h2>
								</div>
							@endif
						</table> 
					</div>
				</div>
			</div>
			</div>
			  {{$userint->links("pagination::bootstrap-4")}}
        </div>
    </div>	
		<!-----send invoice start---->
		<div class="modal fade" id="invoicemodal" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 960px;">
				<div class="modal-content login_box">		
								@include('pages.doctor.admininvoicemodal') 
				</div>
			</div>
		</div>
		<!-----send invoice end---->

		<!---start---->
			<div class="modal fade" id="scss_invoice_sent" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
			
			<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content login_box">
				<div class="modal-body">		
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div class="row">
					<div class="col-md-3 text-center m-auto">
						<img id="modal-inv-succes-img" src="{{ asset('img/checkmark.svg') }}" class="w-100" alt="success" />				
					</div>
				</div>
				<h5 class="modal-title"  style="color:green;text-align: center;" id="acceptscss">Invoice Sent Successfully</h5>
				</div>
			</div>
			</div>
		</div>
		<!----end----->
</div>
<script>
		$('#paymentstatus').change(function(){ 
			var paymentstatus = $(this).val();
			var invitation_id=$('#invitation_id').val();
			var doctor_user_id=$('#doctor_user_id').val();
			var keyword=$('#keyword').val();
			$.ajaxSetup({
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
			});
			$.ajax({
					url: "{{url('/doctor_payment_status_update')}}",
					type: "POST",
					data: {status:paymentstatus,invitid:invitation_id,doctorid:doctor_user_id,keyword:keyword},
					dataType:'json',
					success: function( response ) {
					  window.location.href=response.url;
					}
				});
				///
			});
</script>
<script>
	 function reply_click_loe(input)
  {	 
	 var clickId = $(input).attr('data-id');	  
	 var billamt = $('#billamt'+clickId).val();
	 var othercosts = $('#othercosts'+clickId).val();
	 if(othercosts != ''){
	 	$('#othercoststr').show();
	 }else{
	 	var othercosts = 0;
	 }
	 var total=(parseInt(billamt) + parseInt(othercosts));	 

	if(billamt != ''){
		if(billamt == 0 ){
			alert('Please Enter Bill Amount greater than 0');
		}else{
       //alert('success');
	   $.ajax({
		   url: "{{url('/admin_invoice_modal_view')}}",
		   type: "POST",
		   headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
		   data: {id:clickId},
		   dataTYpe: 'json',
		   success:function(response){
			  //console.log(response);		
			  if(response.status=='success'){	
				  $('#doctorname').html(response.doctorname);
				  $('#categoryname').html(response.categoryname);	
				  $('#doctorlicencenumber').html(response.doctorlicencenumber);	
				  $('#doctorphone').html(response.doctorphone);	
				  $('#invitationdate').html(response.invitationdate);	
				  $('#patientname').html(response.patientname);
				  $('#patientdateofbirth').html(response.patientdateofbirth);
				  $('#patientdateofaccident').html(response.patientdateofaccident);
				  $('#new_service_provided').html(response.serviceprovided);
				  $('#todaydate').html(response.todaydate);
				  $('.new_bill_amt').html(billamt);
				  $('#total').html(total);
				  $('#invitationId').val(clickId);
				  $('#othercosts').html(othercosts);

				  $('#invoicemodal').modal('show');
			  }
		   },   
	   })
	  }
	}
	else{
		alert('Please Enter Bill Amount');
	}
}

$("#sendinvoice_btn").click(function(){  
	$('#sendinvoice_btn').prop('disabled', true);
	var billintid = $('#invitationId').val();	
	var billamt = $('#billamt'+billintid).val();
	var othercosts = $('#othercosts'+billintid).val();
	var docid = $('#docid'+billintid).val();
	    //alert(othercosts);		
		$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
				$.ajax({
				url: "{{url('admin_custom_invoice')}}",
				type: "POST",
				data: {id:billintid,billamt:billamt,docid:docid,othercosts:othercosts},
				dataType:'json',
				success: function( response ) {
				$('#sendinvoice_btn').html('Submit');				
					if(response.status == 'success'){	
						$('#invoicemodal').modal('hide');
						$('#scss_invoice_sent').modal('show');												 
							setTimeout(function () { 
								location.reload(true);
							  }, 2000); 
						
					}					
				} 
				}); 
	});
</script>
@endsection
