@extends('layouts.app')


<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
  $(document).ready(function(){
    $('input[type="radio"]').click(function(){
      if (  $(this).attr("value") == '1')
      {
        $("#doctordiv").show();
        document.getElementById("licencenumber").required = true;
        document.getElementById("introduction").required = true;
        document.getElementById('websiteurl').value = ''
      }
      else
      {
        $("#doctordiv").hide();
      }
      if (  $(this).attr("value") == '2')
      {
        $("#lawyerdiv").show();
        document.getElementById("websiteurl").required = true;
        document.getElementById('licencenumber').value = ''
        document.getElementById('introduction').value = ''
      }
      else
      {
        $("#lawyerdiv").hide();
      }
    });
});
</script>

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Designation') }}</label>
                            
                            <div class="col-md-6">    
                                <label class="radio-inline text-md-right">
                                        <input type="radio" name="designation"  value="1" {{ old('designation') == "1" ? 'checked' : '' }} required> Doctor
                                </label>
                                <label class="radio-inline text-md-right">
                                <input type="radio" name="designation"   value="2" {{ old('designation') == "2" ? 'checked' : '' }} required> Lawyer
                                </label>

                               <!-- <select class="form-control @error('designation') is-invalid @enderror" id='designation' name="designation"  required  autofocus>
                                    <option class="form-control" value="" selected='selected'>Please Select Designation</option>
                                    <option value="doctor" {{ old('designation') == "doctor" ? 'selected' : '' }}>doctor</option>
                                    <option value="lawyer" {{ old('designation') == "lawyer" ? 'selected' : '' }}>lawyer</option>
                                </select> -->
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div style="display:{{ old('designation') == 'doctor' ? 'block' : 'none' }};" id='doctordiv'>                             
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Licence Number') }}</label>

                                <div class="col-md-6">
                                    <input id="licencenumber" type="text" class="form-control @error('licencenumber') is-invalid @enderror" name="licencenumber" value="{{ old('licencenumber') }}"  autocomplete="licencenumber">

                                    @error('licencenumber')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Introduction') }}</label>

                                <div class="col-md-6">                                    
                                    <textarea class="form-control  @error('introduction') is-invalid @enderror"  rows="3" name="introduction" id="introduction"  autocomplete="introduction">{{ old('introduction') }}</textarea>
                                    @error('introduction')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>  

                        <div style="display:{{ old('designation') == 'lawyer' ? 'block' : 'none' }};" id='lawyerdiv'>                             
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Website Url') }}</label>

                                <div class="col-md-6">
                                    <input id="websiteurl" type="url" class="form-control @error('websiteurl') is-invalid @enderror" name="websiteurl" value="{{ old('websiteurl') }}"  autocomplete="websiteurl" autofocus>

                                    @error('websiteurl')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>  


                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Business Address') }}</label>

                                <div class="col-md-6">                                    
                                    <textarea class="form-control  @error('business_address') is-invalid @enderror"  rows="3" name="business_address" id="business_address" required autocomplete="business_address">{{ old('business_address') }}</textarea>
                                    @error('business_address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>



                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Location') }}</label>

                            <div class="col-md-6">
                                <input id="location" type="text" class="form-control @error('location') is-invalid @enderror" name="location" value="{{ old('location') }}" required autocomplete="location" autofocus>

                                @error('location')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
