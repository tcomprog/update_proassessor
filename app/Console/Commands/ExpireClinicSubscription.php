<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ClientSubscription;

class ExpireClinicSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expire_clinic_subscription:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expire past date subscription';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $subscription = ClientSubscription::where("status","1")->get();
        $today = date("Y-m-d");

        foreach($subscription as $row){
            $expire = $row->to_date; 
            $today_time = strtotime($today);
            $expire_time = strtotime($expire);

            if ($expire_time < $today_time) {
               $sb = ClientSubscription::find($row->id);
               $sb->status = 2;
               $sb->update();
            }
        } 
    }
}
