<nav id="sidebar">
    <div class="sidebar-header">
        <h3 class="mb-0"><a href="{{url('/dashboard')}}"><img src="{{ asset('img/logo.png') }}" alt=""></a></h3>
        <strong><a href="{{url('/dashboard')}}"><img src="{{ asset('img/logo.png') }}" alt=""></a></strong>
    </div>
    <ul class="list-unstyled components sidebar-mainmenu">
        <li class="nav-item {{ (request()->routeIs('dashboard')) ? 'active' : '' }}">
            <a href="{{url('/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a>
        </li>
        <!--<li class="nav-item {{ (request()->is('admin/categories*')) ? 'active' : '' }}">
            <a href="{{url('admin/categories')}}"><i class="fa fa-list"></i> Categories</a>
        </li>-->

        @php
           $manageUsers = (request()->is('doctors*') || request()->is('lawyers*')) ? 'active':''; 

           $doctorSection = (request()->is('doctors*') || request()->is('priceRequest')) ? 'active':'';
          
           $clinicSection = request()->is('clinic*')  ? 'active':'';

           $lawyerSection = (request()->is('lawyers*')) ? 'active':'';

           $assessmentSection = (request()->is('reject_req') || request()->is('manage_appointments')) ? 'active':'';

           $configSection = (request()->is('config*')) ? 'active':'';
           
           $adminSection = (request()->is('doctors1')) ? 'active':'';

           $Subscription = (request()->is('Subscription*')) ? 'active':'';
           
        @endphp


         {{-- ******* Clinic Section ******** --}}
        <li class="nav-item {{ $clinicSection }}">
            <a href="#clinicSection" data-toggle="collapse" aria-expanded="{{ $clinicSection=='active'?'true':'false'}}" class="dropdown-toggle">
                <i class="fa fa-hospital-o"></i> Clinic Section
            </a>
            <ul class="collapse list-unstyled {{ $clinicSection=='active'?'show':''}}" id="clinicSection" >
               
                <li class="nav-item ml-2 {{ (request()->is('clinic/all') or request()->is('clinic/active') or request()->is('clinic/inactive')) ? 'active' : '' }}">
                    <a class="nav-link " href="{{ url('/clinic/all') }}">
                        <i class="fa fa-chevron-circle-down"></i>Clinic List</a>
                </li>

                <li class="nav-item ml-2 {{ (request()->is('clinic/packages/view')) ? 'active' : '' }}">
                    <a class="nav-link " href="{{ route('packages') }}">
                        <i class="fa fa-chevron-circle-down"></i>Packages</a>
                </li>

            </ul>
        </li>

         {{-- ******* Doctor Section ******** --}}
        <li class="nav-item {{ $doctorSection }}">
            <a href="#doctorSection" data-toggle="collapse" aria-expanded="{{ $doctorSection=='active'?'true':'false'}}" class="dropdown-toggle">
                <i class="fa fa-user-md"></i> Doctor Section
            </a>
            <ul class="collapse list-unstyled {{ $doctorSection=='active'?'show':''}}" id="doctorSection" >
               
                <li class="nav-item ml-2 {{ (request()->is('doctors*')) ? 'active' : '' }}">
                    <a class="nav-link " href="{{ url('/doctors/all') }}">
                        <i class="fa fa-chevron-circle-down"></i>Doctor List</a>
                </li>

                <li class="nav-item ml-2 {{ (request()->is('priceRequest*')) ? 'active' : '' }}">
                    <a class="nav-link " href="{{ url('/priceRequest') }}">
                        <i class="fa fa-chevron-circle-down"></i>Price Request</a>
                </li>

            </ul>
        </li>


        {{-- ******* Lawyer Section ******** --}}
        <li class="nav-item {{ $lawyerSection }}">
            <a href="#lawyerSection" data-toggle="collapse" aria-expanded="{{ $lawyerSection=='active'?'true':'false'}}" class="dropdown-toggle">
                <i class="fa fa-user-secret"></i> Lawyer Section
            </a>
            <ul class="collapse list-unstyled {{ $lawyerSection=='active'?'show':''}}" id="lawyerSection" >
                <li class="nav-item ml-2 {{ (request()->is('lawyers*')) ? 'active' : '' }}">
                    <a class="nav-link " href="{{ url('/lawyers/all') }}">
                        <i class="fa fa-chevron-circle-down"></i>Lawyer List</a>
                </li>

            </ul>
        </li>


        {{-- ******* Assessment Section ******** --}}
        <li class="nav-item {{ $assessmentSection }}">
            <a href="#assessmentSection" data-toggle="collapse" aria-expanded="{{ $assessmentSection=='active'?'true':'false'}}" class="dropdown-toggle">
                <i class="fa fa-bars"></i> Assessment Section
            </a>
            <ul class="collapse list-unstyled {{ $assessmentSection=='active'?'show':''}}" id="assessmentSection" >
               
                <li class="nav-item ml-2 {{ (request()->is('manage_appointments*')) ? 'active' : '' }}">
                    <a class="nav-link " href="{{ url('/manage_appointments') }}">
                        <i class="fa fa-chevron-circle-down"></i>Appointments</a>
                </li>

                <li class="nav-item ml-2 {{ (request()->is('reject_req*')) ? 'active' : '' }}">
                    <a class="nav-link " href="{{ url('/reject_req') }}">
                        <i class="fa fa-chevron-circle-down"></i>Reject Request</a>
                </li>

            </ul>
        </li>


        
        {{-- ******* Config Section ******** --}}
        <li class="nav-item {{ $configSection }}">
            <a href="#configSection" data-toggle="collapse" aria-expanded="{{ $configSection=='active'?'true':'false'}}" class="dropdown-toggle">
                <i class="fa fa-cog"></i> Config Section
            </a>
            <ul class="collapse list-unstyled {{ $configSection=='active'?'show':''}}" id="configSection" >
                <li class="nav-item ml-2 {{ (request()->is('config/lawyer_types')) ? 'active' : '' }}">
                    <a class="nav-link " href="{{ url('/config/lawyer_types') }}">
                        <i class="fa fa-chevron-circle-down"></i>Lawyer Types</a>
                </li>

                <li class="nav-item ml-2 {{ (request()->is('config/city')) ? 'active' : '' }}">
                    <a class="nav-link " href="{{ url('/config/city') }}">
                        <i class="fa fa-chevron-circle-down"></i>City</a>
                </li>

                <li class="nav-item ml-2 {{ (request()->is('config/category')) ? 'active' : '' }}">
                    <a class="nav-link " href="{{ url('/config/category') }}">
                        <i class="fa fa-chevron-circle-down"></i>Category</a>
                </li>

                <li class="nav-item ml-2 {{ (request()->is('config/designation')) ? 'active' : '' }}">
                    <a class="nav-link " href="{{ url('/config/designation') }}">
                        <i class="fa fa-chevron-circle-down"></i>Designation</a>
                </li>

                <li class="nav-item ml-2 {{ (request()->is('config/cmp_profile')) ? 'active' : '' }}">
                    <a class="nav-link " href="{{ url('/config/cmp_profile') }}">
                        <i class="fa fa-chevron-circle-down"></i>Company Profile</a>
                </li>

            </ul>
        </li>
        


        <li class="nav-item {{ $Subscription }}">
            <a href="#subscription" data-toggle="collapse" aria-expanded="{{ $Subscription=='active'?'true':'false'}}" class="dropdown-toggle">
                <i class="fa fa-usd"></i> Subscription
            </a>

            <ul class="collapse list-unstyled {{ $configSection=='active'?'show':''}}" id="subscription" >
               
                <li class="nav-item ml-2 {{ (request()->is('/clinic/packages/view')) ? 'active' : '' }}">
                    <a class="nav-link " href="{{ url('/clinic/packages/view') }}">
                        <i class="fa fa-chevron-circle-down"></i>Subscription List</a>
                </li>

            </ul>
        </li>


        <li class="divider"></li>

        <li class="nav-item {{ (request()->is('admin/contact_us*')) ? 'active' : '' }}">
           <a class="dropdown-item" href="{{url('admin/contact_us')}}">
              <i class="fa fa-address-book-o"></i> Customer Emails                 
           </a>
        </li>

        <li class="nav-item {{ (request()->is('admin/send_email*')) ? 'active' : '' }}">
           <a class="dropdown-item" href="{{url('admin/send_email')}}">
              <i class="fa fa-envelope-o"></i> Send E-mail                 
           </a>
        </li>
        
            </ul>
        </li>

        
    </ul>
</nav>