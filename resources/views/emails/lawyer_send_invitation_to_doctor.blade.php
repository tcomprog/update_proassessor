@extends("includes.email_container")
@section("content")
            <div>

                    <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Dear  {{{ ucfirst($invitation->user1->name) }}},</h3> 
                    <p style="font-size: 14px;
                    line-height: 17px;">You have received an Invite from <b>{{ ucfirst($invitation->user->lawyer->lawfirmname) }}</b></p>
                    <p>Please <a href="{{url('/')}}" target="_blank">login</a> to ProAssessors for more details.</p> 
                    <br/>
                    <br/>

                    <br/>

                <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
                <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 

          </div>  
@endsection