<?php



namespace App\Console\Commands;

use Mail;

use App\Models\user_invitations;

use App\Models\Sitedetails;

use Illuminate\Console\Command;

class EmailNotificationCron extends Command

{

    /**

     * The name and signature of the console command.

     *

     * @var string

     */

    protected $signature = 'email-notification:cron';



    /**

     * The console command description.

     *

     * @var string

     */

    protected $description = 'This will run every  minutes';



    /**

     * Create a new command instance.

     *

     * @return void

     */

    public function __construct()

    {

        parent::__construct();

        $this->email_after_patient_inivtation_expire();

    }



    public function email_after_patient_inivtation_expire() 

    {
		$settings = Sitedetails::select(['invitation_remember_in_hours'])->first();

		$remember_to_time = date('Y-m-d H:i:s',strtotime('-'.$settings->invitation_remember_in_hours.' hours'));

		$remember_from_time = date('Y-m-d H:i:s',strtotime($remember_to_time. '-1 hour'));

        $invitations = user_invitations::where('status','active')->where('is_patient_accepted','no')->where('doctor_accepted_on','>',$remember_from_time)->where('doctor_accepted_on','<=',$remember_to_time)->get();

    }



    public function handle()

    {

        // Mail::send([],[], function ($message) {

        //     $message->subject('test patient cron mail');

        //     $message->to('showmacktest@gmail.com');

        //     $message->setBody('test message', 'text/html');

        // });

    }





}