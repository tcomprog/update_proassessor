<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Helper as MyHelper;
use Auth;

class fileEncryption extends Controller
{
    function downloadMedicalReport(Request $request){
     $path = $request->path;
     $name = $request->name;
     $enceypKey = $request->enceypKey;
     $directory = $request->directory;

     $extension =explode('.',$name)[1];
     $contents = Storage::get('uploads/'.$directory.'/'.$path);
     $image = MyHelper::Decrypt($contents,$enceypKey);
     $bin = base64_decode($image, true);

     $meme = 'application/pdf';
     if($extension == 'doc'){
       $meme = 'application/msword';
     }
     elseif($extension == 'docx'){
       $meme = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
     }
   
     return response($bin)->withHeaders([
        'Content-Type' => $meme,
        'Content-disposition: attachment;filename=' => str_replace(' ',"_",$name),
    ]);
     
    }
}
