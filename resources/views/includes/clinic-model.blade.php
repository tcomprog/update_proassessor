    
    {{-- *********************** Change Password Modal Start ************************* --}}
    <div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" id="lawyerchangePassword" tabindex="1" role="dialog" aria-labelledby="forgotTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content login_box">
            
           <div class="modal-header" style="border: none;">
            <h5 class="modal-title" id="loginTitle">Change Password</h5>
            <button onclick="funcs_close()" type="button" class="close nclose" data-dismiss="modal" aria-label="Close">
      
              <span class="material-icons" style="color: #fff; font-size:23px;
              vertical-align: middle;">close</span>
      
            </button>
            </div>
      
            <div class="modal-body" style="padding-top:0px; mt-3">

              <div id="lpas_error_ee" style="display:none;" class="alert alert-danger" role="alert">
                A simple success alert—check it out!
              </div>

              <div id="lpas_error_scuss" style="display:none;" class="alert alert-success" role="alert">
                A simple success alert—check it out!
              </div>
              
              <div class="col-md-12 col-sm-12 px-0">
								<div class="inputContainer w-100">
									<span class="material-icons" style="color: #515C84; font-size:28px;">key</span>
									<input class="inputStyle" id="l_old_password"  type="password" placeholder="Old password"/>
									<span class="material-icons showPassword passIcons" title="Show / hide password" id="loginShowPass1">visibility</span> 
								</div>
                <span id="l_old_password_error" class="lawyerchgPassError"></span>
							</div>

              <div class="col-md-12 col-sm-12 px-0 mt-4">
								<div class="inputContainer w-100">
									<span class="material-icons" style="color: #515C84; font-size:28px;">key</span>
									<input class="inputStyle" id="l_new_password"  type="password" placeholder="New password"/>
									<span class="material-icons showPassword passIcons" title="Show / hide password" id="loginShowPass2">visibility</span> 
								</div>
                <span id="l_new_password_error" class="lawyerchgPassError"></span>
							</div>

              <div class="col-md-12 col-sm-12 px-0 mt-4">
								<div class="inputContainer w-100">
									<span class="material-icons" style="color: #515C84; font-size:28px;">key</span>
									<input class="inputStyle" id="l_newcof_password"  type="password" placeholder="Confirm new password"/>
									<span class="material-icons showPassword passIcons" title="Show / hide password" id="loginShowPass3" >visibility</span> 
								</div>
                <span  id="l_newcof_password_error" class="lawyerchgPassError"></span>
							</div>
                            
									<div style="display: flex;flex-direction:row;align-items:center; justify-content: center; margin-top:30px">
										<img id="changeLayerPasswordLoader" style="width:50px; height:50px;display:none; " src="{{asset('img/loading.gif')}}" alt="">
										<button type="button" class="btn btn-primary submit_btn" style="margin:5px auto;"  id="changeLawyerPassword">Submit</button>
									</div>
              
            </div>
        </div>
        </div>
      </div>
      {{-- *********************** Change Password Modal End   ************************* --}}


<script>
  $("#changeLawyerPassword").click(function(){
     var old_password = $("#l_old_password").val();
     var new_password = $("#l_new_password").val();
     var confirm_new_password = $("#l_newcof_password").val();
     
     $("#l_old_password_error").hide();
     $("#l_new_password_error").hide();
     $("#l_newcof_password_error").hide();
     $("#lpas_error").hide();

     if(isEmpty(old_password)){ 
      $("#l_old_password_error").show().html("Enter value"); 
      setTimeout(() => {
        $("#l_old_password_error").hide();
      }, 3000);
      return;
     }

     if(isEmpty(new_password)){ 
       $("#l_new_password_error").show().html("Enter value"); 
       setTimeout(() => {
        $("#l_new_password_error").hide();
      }, 3000);
      return;
     }

     if(isEmpty(confirm_new_password)){ 
      $("#l_newcof_password_error").show().html("Enter value"); 
      setTimeout(() => {
        $("#l_newcof_password_error").hide();
      }, 3000);
      return;
     }

     if(old_password == new_password){
        $("#l_new_password_error").show().html("New password should be different from old password"); 
        setTimeout(() => {
          $("#l_new_password_error").hide();
        }, 3000);
        return;
     }

     let validate = checkPasswordValidity(new_password);
     if(validate == null && new_password == confirm_new_password){
       //make api call
       $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "<?=url('changeClinicPassword') ?>",
            type: "POST",
            data: {oldP:old_password,newP:new_password,newcP:confirm_new_password},
            beforeSend: function() {
               $("#changeLawyerPassword").hide();
               $("#l_old_password_error").hide();
               $("#l_new_password_error").hide();
               $("#l_newcof_password_error").hide();
               $("#lpas_error").hide();
               $("#changeLayerPasswordLoader").show();
            },
            success: function( response ) {
              if(response.status == 'success'){
                $("#l_old_password").val('');
                $("#l_new_password").val('');
                $("#l_newcof_password").val('');
                $("#changeLayerPasswordLoader").hide(); 
                $("#changeLawyerPassword").show();
                $("#lpas_error_ee").hide();
                $("#lpas_error_scuss").show().html(response.message);
              }
              else{
                $("#changeLayerPasswordLoader").hide(); 
                $("#changeLawyerPassword").show();
                $("#lpas_error_scuss").hide();
                $("#lpas_error_ee").show().html(response.message);
              }
            }
            });
       
     }
     else{
      $("#l_newcof_password_error").show().text("Passwords don`t match");
     }
          

  });

  $(".fdfdf").click(function(){
     $("#changeLawyerPassword").show();
     $("#changeLayerPasswordLoader").hide();
     $("#lpas_error_scuss").hide();
     $("#lpas_error_ee").hide();
     $("#l_old_password_error").hide();
     $("#l_new_password_error").hide();
     $("#l_newcof_password_error").hide();
     $("#l_old_password").val('');
     $("#l_new_password").val('');
     $("#l_newcof_password").val('');
  });



         $("#l_new_password").on('input',function(){
							let pw = $(this).val();
							let validate = checkPasswordValidity(pw);
			   
							if(!isEmpty(pw) && validate != null){
							  $("#l_new_password_error").show().text(validate);
							}
							else{
								$("#l_new_password_error").hide();
							}
	
							if(isEmpty(pw)){
								$("#l_new_password_error").hide();
							}
						});
	
						$("#l_newcof_password").on('input',function(){
							let pw = $(this).val();
							let pw2 = $("#l_new_password").val();
	
							if(isEmpty(pw)){
								$("#l_newcof_password_error").hide();
							}
	
							if(pw != pw2){
							   $("#l_newcof_password_error").show().text("Passwords don`t match");
							}
							else{
								$("#l_newcof_password_error").hide();
							}
	
						});
            
</script>


<script>
  $("#loginShowPass1").click(function(){
    let val = $(this).text();
    if(val == 'visibility'){
      $("#l_old_password").attr('type','text');
      $("#loginShowPass1").text("visibility_off");
    }else{
      $("#l_old_password").attr('type','password');
      $("#loginShowPass1").text("visibility");
    }
  });

  $("#loginShowPass2").click(function(){
    let val = $(this).text();
    if(val == 'visibility'){
      $("#l_new_password").attr('type','text');
      $("#loginShowPass2").text("visibility_off");
    }else{
      $("#l_new_password").attr('type','password');
      $("#loginShowPass2").text("visibility");
    }
  });

  $("#loginShowPass3").click(function(){
    let val = $(this).text();
    if(val == 'visibility'){
      $("#l_newcof_password").attr('type','text');
      $("#loginShowPass3").text("visibility_off");
    }else{
      $("#l_newcof_password").attr('type','password');
      $("#loginShowPass3").text("visibility");
    }
  });
</script>
