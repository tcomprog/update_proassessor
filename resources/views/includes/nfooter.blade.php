<style>
  .socialMedicIcon:hover{
    transform: scale(1.3);
  }
</style>
<div class="tp_footer container-fluid">
   <div class="container">
      <div class="row">
          <div class="col-md-4 col-sm-4 bfirst">
              <img src="{{asset('img/bottom_logo.png')}}" alt="">
              <p>Pro-assessors-web-based platform for your med-legal process</p>
          </div>

          <div class="col-md-4 col-sm-4 bsecond">
              <h5>Home</h5>
              <h5>About Us</h5>
              <h5>Contact Us</h5>
          </div>

          @php
              $sitedata1 = Helper::Sitedetails();
          @endphp

          <div class="col-md-4 col-sm-4 bthird">
             <p>Follow Us</p>
             <div>
                  <a class="socialMedicIcon" href="{{$sitedata1->facebook_link}}" target="_blank"><img width="24" src="{{asset('icon/facebook.png')}}" alt=""></a>
                  <a class="socialMedicIcon" href="{{$sitedata1->twitter_link}}" target="_blank"><img width="24" src="{{asset('icon/twitter.png')}}" alt=""></a>
                  <a class="socialMedicIcon" href="{{$sitedata1->instagram_link}}" target="_blank"><img width="24" src="{{asset('icon/instagram.png')}}" alt=""></a>
                  <a class="socialMedicIcon" href="{{$sitedata1->youtube}}" target="_blank"><img width="24" src="{{asset('icon/youtube.png')}}" alt=""></a>
             </div>
          </div>

      </div>
   </div>
</div>

<div class="n_footer">
    <div class="container">
       <div class="row">
            <p class="m-0 col-md-6 col-sm-12 text-light">© 2023 {{$sitedata1->site_title}} by Itek Global</p>
            <div class="nf_letfContainer col-md-6 col-sm-12">
                <a href="privacy_policy">Privacy Policy</a>
                <span>|</span>
                <a href="terms&conditions">Terms & Conditions</a>
            </div>
       </div>
    </div>
</div>


@include('includes.nfooter_action')


