<?php //$number = 1; ?>

								@if($users->isNotEmpty())

								@foreach ($users as $user)

								<tr>

								<td>{{ $number }}</td>

								<?php $number++; ?>

								<td>{{ ucfirst($user->name) }}</td>  

								<td>{{ $user->email }}</td>

								<td>{{ $user->phone }}</td>

								<td style="color:#00528c">{{ @$user->doctor->credentials }}</td>

								<!--<td></td>-->
								<td>
									<?php if($user->status=="active"){ echo "<p style='color:green;'><b>Active</b></p>"; } 

											if($user->status=="inactive"){ echo "<p style='color:orange;'><b>Pending</b></p>"; }

										?>
								</td>

								<td><?php echo date('d F Y', strtotime($user->created_at)); ?></td>

								<td style="text-align: center;">

								<!--<a href ='Edituser/{{ $user->id }}/{{ $user->role_id }}'><i class="fa fa-edit" ></i></a>&nbsp;
								<a  onclick="return confirm('Are you Sure Want to Delete?')"  href = 'deleteuser/{{ $user->id }}/{{ $user->role_id }}'><i style="color:red;" class="fa fa-trash"></i></a>-->

								<a href ="{{url('/doctor_file')}}/{{$user->id}}" title="View Profile"><i class="fa fa-eye" style="color:#5A8627;"> View Profile</i></a>&nbsp;

								<a style="font-size: 12px;color: #00528c;font-weight: 600;" href ='manage_appointments/?id={{ $user->id }}' style="font-size: 11px;" title="View Appointments"><i class="fa fa-calendar" > View Appointments</i></a>

								<select class="form-control" id="doctorstatus{{ $user->id  }}" onChange="submit_status(this)" data-id="{{ $user->id  }}" style="width: auto;margin-left: 25%;">
										<option value="active" {{ $user->authentication_status  == 'active' ? 'selected' : ''}} >approve</option>
										<option value="inactive" {{ $user->authentication_status  == 'inactive' ? 'selected' : ''}} >decline</option>
									</select>

								</td>

								</tr>                       

								@endforeach

								@else 

									<div>

										<h2>No Records found</h2>

									</div>

								@endif