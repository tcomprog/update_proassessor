@extends('layouts.admin_app')
    @section('content')
    
    <div class="container-fluid bg-white rounded py-4 adminPageContainer">
    
        <div class="row">
    
            <h4 class="inv-content-header" style="margin-bottom:0px;">Company Profile</h4>
                     
            <form class="col-12 mt-5" method="post" action="/updateCompanyProfile">
                @csrf
                <div class="row">
                  <div class="col">
                    <label for="">Name</label>
                    <input name="name" required type="text" class="form-control" placeholder="Name" value="{{$company->site_title}}">
                  </div>
                  <div class="col">
                    <label for="">Phone</label>
                    <input name="phone" required type="text" class="form-control" placeholder="Phone" value="{{$company->phone}}">
                  </div>
                  <div class="col">
                    <label for="">Email</label>
                    <input name="email" required type="text" class="form-control" placeholder="Email" value="{{$company->site_email}}">
                  </div>
                </div>

                <div class="row mt-3">
                  <div class="col">
                    <label for="">Address</label>
                    <input name="address" required type="text" class="form-control" placeholder="Address" value="{{$company->address}}">
                  </div>
                </div>

                <div class="row mt-3">
                    <div class="col">
                      <label for="">Facebook</label>
                      <input name="facebook" type="text" class="form-control" placeholder="Facebook" value="{{$company->facebook_link}}">
                    </div>
                    <div class="col">
                      <label for="">Instagram</label>
                      <input name="instagram" type="text" class="form-control" placeholder="Instagram" value="{{$company->instagram_link}}">
                    </div>
                    <div class="col">
                      <label for="">Twitter</label>
                      <input name="twitter" type="text" class="form-control" placeholder="Twitter" value="{{$company->twitter_link}}">
                    </div>

                  </div>

                <div class="row mt-3">
                   
                    <div class="col">
                      <label for="">Youtube</label>
                      <input name="youtube" type="text" class="form-control" placeholder="Youtube" value="{{$company->youtube}}">
                    </div>

                    <div class="col">
                      <label for="">Assessment expire time in minutes</label>
                      <input name="exp_time" type="text" class="form-control" placeholder="Assessment expire time in minutes" value="{{$company->invitation_expire_in_minutes}}">
                    </div>

                  </div>

                  <button class="btn btn-outline-primary mt-4 mb-4" type="submit">Update</button>

              </form>

        </div>
    </div>
    
    @endsection



   