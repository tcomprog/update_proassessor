<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;
use BadFunctionCallException;

use App\Http\Controllers\EmailController;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\AdminSettings;
use App\Models\CustomerEmails;
use App\Models\doctors;
use App\Helpers\Helper as MyHelper;
use App\Models\lawyers;
use App\Models\Sitedetails;
use App\Models\Pagecms;
use App\Models\Nofication;
use App\Models\user_invitations;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
	
	 public function about()
    {        
        $page_slug="about";
		
         $pageuser = Pagecms::where('page_slug',$page_slug)->first();     
		 $pagedetails = Sitedetails::find(1);
          $setting_about_image = AdminSettings::where('settings_name','aboutus_image')->first();

         return view('/pages/about', [
			'pageuser' => $pageuser,
			'pagedetails' => $pagedetails,
			]); 
    }

	public function unreadNotification(Request $request){
       $id = $request->id;
	   $not = Nofication::find($id);
	   $not->status = '1';
	   $not->save();
	   return response()->json(['status'=>"1",'message'=>"Notification unread successfull"]);
	}

	 public function notification()
    {    
		Nofication::where('to_id',Auth::id())->where("status","0")->update(['status'=>'1']);
		$notification = Nofication::where('to_id',Auth::id())->orderBy('created_at','desc')->get();
         return view('/pages.notification', ['notification' => $notification]); 
    }

	public function clear_notif(){
	   Nofication::where('to_id',Auth::id())->delete();
	   return response()->json(["status"=>1,"message"=>"All data deleted successfully"],200);
	}

    public function benefit()
    {        
        return view('/pages/benefit'); 
    }
    
    public function termsconditions()
    {        
        $page_slug="terms&conditions";
         $pageuser = Pagecms::where('page_slug',$page_slug)->first();  
        // dd($pageuser);
        return view('/pages/terms&conditions', ['pageuser' => $pageuser]); 
    }
    public function privacypolicy()
    {        
        $page_slug="privacy_policy";
         $pageuser = Pagecms::where('page_slug',$page_slug)->first();  
        // dd($pageuser);
        return view('/pages/privacy_policy', ['pageuser' => $pageuser]); 
    }
	
	public function assessment(Request  $request)
    {   
		$conts = new CustomerEmails();
		$rules=[
				'phone'=> 'required'
		];
		$response = [];
		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()) {
			$response['response'] = $validator->messages();
			//return $response;
			return response()->json(['status'=>'error', 'message'=>$validator->messages()]);					
		}else{
		
		$fname=$request->fname;
		$lastname=$request->lastname;
		$emailaddress=$request->emailaddress;
		$phone=$request->phone;
		$subject=$request->subject;
		$message=$request->message;

		$conts->f_name = $fname;
		$conts->l_name = $lastname;
		$conts->phone = $phone;
		$conts->email = $emailaddress;
		$conts->subject = $subject;
		$conts->message = $message;

		$conts->save();

		
		$body = view('emails.user_assessement_request',
	    [
			'fname' => $fname,
			'lname' => $lastname,
			'email' => $emailaddress,
			'phone' => $phone,
			'subject' => $subject,
			'message' => $message,
		]);
	
        Mail::send([],[], function ($message) use($body) {
		$message->subject(' New Contact Request');
		$message->to('showmacktest@gmail.com','Pro Assessors');
		$message->setBody($body, 'text/html');
		});	
		if( count(Mail::failures()) > 0 ) {
			return false;
		} else {
			return response()->json(['status'=>'success', 'message'=>" Thank You Request Sent Successfully"]);
		//	return true;
			
		}
	  }
    }
	public function activateaccount($id){
		$user = User::where('email_verify_token',$id)->first(); 
		if(!empty($user->email_verify_token)){
			$user->email_verify_token = '';
			$user->status = 'active';
			$user->save();
			if($user->role_id==2){
				$email_controller = new EmailController();					
				$email_controller->send_account_activation_after_mail_to_lawyer($user);
				return view('/pages/authenticate_message',['status'=>'0']);				
			}
			elseif($user->role_id==1){
				Auth::logout();
				return view('/pages/authenticate_message');
			}
			else{
				Auth::logout();
				return view('/pages/authenticate_message');
			}
		}else{
			return view('/pages/link_expired');
		}
	}
	public function resetpwd(Request $request){
		
		$user = User::where('email',$request->email)->first(); 
		if(isset($user)){
			$pwdreset_token=md5(time());
			$user->pwdreset_token = $pwdreset_token;
			$user->save();
			
			$email_token=$user->pwdreset_token;
			$regemail=$user->email;
			$regname=$user->name;
			$id=$user->id;
			
			$user = User::where('id', $id)->first(); 
			$email_controller = new EmailController();					
			$email_controller->send_reset_password_mail($user); 

			/*$body = "<h4>Please Click Below Link for Reset Password</h4>				
				<h3><a href='".url('reset_newpwd')."/".$email_token."' target='_blank'>Click Here</a></h3>";
		
			Mail::send([],[], function ($message) use($body,$regemail,$regname) {
			$message->subject('Reset Password');
			$message->from('showmacktest@gmail.com','Pro Assessors');
			$message->to($regemail,$regname);
			$message->setBody($body, 'text/html');
			});	
			if( count(Mail::failures()) > 0 ) {
				return false;
			} else {
				return response()->json(['status'=>'success']);
			//	return true;
				
			}*/			
			return response()->json(['status'=>'success']);
		}else{
			return response()->json(['status'=>"error"]);//pending
		}
		
	}
	public function resetnewpwd($id){				
		Session::flush();
        Auth::logout();
		$user = User::where('pwdreset_token',$id)->first(); 
		
		if(isset($user)){
			return view('/pages/pwd_reset', ['id' => $id]);
		}
		else{
			return view('/pages/link_expired');
		}
	}

	public function pwdset(Request $request){
		$pwdreset_token= $request->pwdreset_token;
		$pwd= $request->new_password;
		$cpwd= $request->confirm_password;		
		$upwd=Hash::make($pwd);

		$user = User::where('pwdreset_token',$pwdreset_token)->first(); 
		
		if(isset($user)){
			$pwdtoken=md5(time());
			$user->pwdreset_token = $pwdtoken;
			$user->password=$upwd;
			$user->save();
			return response()->json(['status'=>"success",'url'=>url('/')]);
		}
		else{
				return response()->json(['status'=>"error"]);
		}
	}
    
}
