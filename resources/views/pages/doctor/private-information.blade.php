@extends('layouts.doctor-profile-custom')

@section('content')	
<style>
.error{color:red !important;}
</style>
<style>
	.profile_update .form-group .demo-default {
   		 height: auto;
   		 border: none;
	}
		.pac-container:after{
		content:none !important;
	}
	
	.inputss,select{
	border-radius: 20px !important;
    border: 1px solid #545C84 !important;
    padding: 8px 10px !important;
    height: auto;
    max-height: 40px;
	}

	.fil_upload{
     padding:4.5px 10px !important;
	}
	@-moz-document url-prefix() {
		.fil_upload {
		padding:7px 10px !important;
	}
}
	

	label{
      font-weight: bold !important;
	  color: #545C84 !important;
	  font-size: 15px !important;
	  padding-left: 7px;
	}
	.disc{
		border-radius: 10px !important;
		box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px !important;
		padding:0px !important;
		border-bottom: 7px solid #545C84;
		padding-bottom: 20px !important;
	}
	.profle_div{
		font-size: 23px;
		font-weight: bold;
		color: white;
		background: #545C84;
		border-radius: 0px 0px 100px 100px;
		text-align: center;
		padding:3px 0px;
		overflow: hidden;
	}
	.selectize-input{
		border-radius: 20px !important;
		padding-left: 10px;
		height: 62px !important;
		border-color: #545C84 !important;
	}
	.newbtn{
		width: 280px !important;
		background: linear-gradient(241.07deg, #545C84 39.69%, rgba(84, 92, 132, 0.31) 71.84%) !important;
        border-radius: 30px !important;
		height: 46px !important;
		margin-left: auto !important;
		margin-right: auto !important;
		margin-bottom: 5px !important;
	}
	#removeprofilepic:hover,#removecv:hover,#removeinsform:hover{
		cursor: pointer !important;
	}

	.form-group{
		margin-bottom: 13px !important;
	}

	.pr_heading{
		background: #545C84;
	    color: white;
		width: 100%;
		padding: 5px 0px;
		text-align: center;
		border-radius: 100px 0px 100px 0px;
		margin: 0px;
		margin-bottom: 13px;
	}

     input{
		font-family: 'Montserrat', sans-serif !important; 
	 }
</style>
	 <!-- Banner Section -->

    <section class="intro_banner_sec inner_banner_sec" style="padding-top:25px; padding-bottom:25px;">
        <div class="container">

            <div class="row align-items-center">

                <div class="col-md-6">

                    <div class="welcome_text">

                        <h1>Private Setting</h1>

                    </div>

                </div>

            
            </div>
        </div>
    </section>
	
	<section class="dashboard_sec" style="padding:30px 0px;">
        <div class="container disc">
			
            <div class="row" style="padding: 15px 30px;">
				<div class="alert alert-success fade show" role="alert" id="updatesuccess22" style="display:none;">
					Private information updated successfully
				</div>
                 <div class="col-md-12">
						<form class="profile_update" id="update_profile_doctor22">
							<div class="row mt-3">	

							<div class="col-md-6">
								<div class="form-group">
									<label for="license">Licence Number</label>
									<input type="text" class="form-control-file inputss" name="licence_number" id="licence_number" aria-describedby="license" placeholder="Licence Number" value="{{ @$users->doctor->licence_number }}">
								</div>
							</div>
                            <div class="col-md-6">
								<div class="form-group">
									<label for="phone">Phone Number</label>
									<input type="text" class="form-control-file inputss" id="phone" name="phone" aria-describedby="phone" placeholder="Phone Number" value="{{ $users->phone }}">
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label for="address">Business Address</label>
									<input type="text" class="form-control-file inputss" id="address" name="business_address" aria-describedby="address" placeholder="Business Address" value="{{ $users->business_address }}">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label for="license">Change Password</label>
									<input type="password" class="form-control-file inputss" name="password" id="password" aria-describedby="license" placeholder="Password" >
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="license">Confirm Change Password</label>
									<input type="password" class="form-control-file inputss" name="confirm_password" id="confirm_password" aria-describedby="license" placeholder="Confirm Password" >
								</div>
							</div>
							 
								<input type="hidden" name="id" id="id" value="<?php  echo $users->id; ?>">
							</div>

							<div style="width: 100%; text-align:center">
								<img style="display: none" id="sendInvoiceLoading22" width="50" src="{{asset('img/loading.gif')}}" alt="">
								<button type="submit" class="btn btn-primary update_btn col-md-6 newbtn" id="update_profile_doctor_btn22">Update</button>
						   </div>
						</form>
					</div>
            </div>
        </div>
		
    </section>
	
@endsection