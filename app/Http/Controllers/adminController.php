<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\doctors;
use App\Models\Sitedetails;
use App\Models\lawyers;
use App\Models\ClinicPackages;
use App\Models\user_invitations;
use App\Models\Invites_Payments;
use App\Models\CustomerEmails;
use App\Models\Pagecms;
use App\Models\PriceChangeRequest;
use App\Models\Nofication;
use App\Models\AdminSettings;
use App\Http\Controllers\EmailController;
use App\Helper;
use App\Helpers\Helper as MyHelper;
use App\Models\Patient;
use App\Models\City;
use App\Models\Category;
use App\Models\RejectInvitation;
use App\Models\LawyerTypes;
use App\Models\Designation;
use App\Mail\SendEmail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Mail\RejectResponsePatient;
use App\Mail\UserAccountVerificationEmail;
use App\Models\ClientPayment;
use App\Models\ClientSubscription;
use App\Models\ClinicSpecialOffer;

use Mail;
use DB;

use Illuminate\Database\Eloquent\Model;
use Session;

class adminController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
   
    public function index()
    {
		$reject_req =  RejectInvitation::where('status','0')->count(); 
		$doctors=User::where('role_id', 1)->get();
		$doctorscount=count($doctors);
		$lawyers=User::where('role_id', 2)->get();
		$clinic=User::where('role_id', 4)->get();
		$cliniccount=count($clinic);
		$lawyerscount=count($lawyers);

		$changeRequest = PriceChangeRequest::where('status','0')->count();

        return view('/pages/admin/home', [
			'doctorscount' => $doctorscount,
			'lawyerscount' => $lawyerscount,
			'cliniccount' => $cliniccount,
			'reject_req' => $reject_req,
			'changeRequest' => $changeRequest,
		]);
    }

	public function companyProfile(){
		return view('pages.admin.companyProfile',[
			'company'=>Sitedetails::first()
		]);
	}

	public function customerEmails(){
		$customer_email = CustomerEmails::orderBy('id', 'desc')->get();
		return view("pages.admin.contact_us",[
			"customer_email" => $customer_email
		]);
	}

	public function deleteContactEmail($id){
		CustomerEmails::where("id",$id)->delete();
		return redirect()->route('customerEmails', ['deleted_contact_status' => 1]);
	}

	public function sendEmailView(){
		return view('pages.admin.send_email');
	}

	public function sendEmailToUser(Request $request){
		$to = $request->to;
		$subject = $request->subject;
		$message = $request->message;
	
		Mail::to($to)->send(new SendEmail(['to'=>$to,'subject'=>$subject,'message'=>$message]));
		return response()->json(["status"=>'success']);
	}

	public function updateCompanyProfile(Request $request){
		$name = $request->name;
		$phone = $request->phone;
		$email = $request->email;
		$address = $request->address;
		$facebook = $request->facebook;
		$instagram = $request->instagram;
		$twitter = $request->twitter;
		$youtube = $request->youtube;
		$exp_time = $request->exp_time;

		$company = Sitedetails::first();
        $company->site_title = $name;
        $company->phone = $phone;
        $company->site_email = $email;
        $company->address = $address;
        $company->facebook_link = $facebook;
        $company->instagram_link = $instagram;
        $company->twitter_link = $twitter;
        $company->youtube = $youtube;
        $company->invitation_expire_in_minutes = $exp_time;
        $company->update();

		return redirect()->route('loadProfilePage',[
			'company'=>Sitedetails::first(),
			'update_status' => 1
		]);
	}

	public function changePriceRequest(){

		$changeRequest = DB::table('doctor_price_change')
            ->join('users', 'doctor_price_change.doctorID', '=', 'users.id')
			->where("doctor_price_change.status",'0')
            ->select('doctor_price_change.*', 'users.phone', 'users.name','users.email')
            ->get();

		return view('pages.doctor.priceChangeRequest',
		  ['changeRequest' => $changeRequest]
	    );
	}

	public function changePriceRequestStatus(Request $request){
		$id = $request->id;
		$status = $request->status;
		$prq = PriceChangeRequest::find($id);
		$prq->status = $status;
		$prq->save();

		if($status == 1){
			DB::table('doctors')->where('user_id',$prq->doctorID)->update(['actual_price' => $prq->newPrice]);
		}

		$des = $status == 1 ? "Your price change request has been accepted by admin" : "Your price change request has been rejected by admin";

		$notif = new Nofication();
		$notif->url = "/doctor-profile";
		$notif->description = $des;
		$notif->to_id = $prq->doctorID;
		$notif->from_id = Auth::id();
		$notif->save();

		return response()->json(['status'=>'success','message'=>"Request status changed successfully"]);
	}

	function addLawyerType(Request $request){
       $id = $request->id;
       $name = $request->name;
       $commission = $request->commission;

	   if($id == 0){
		$sr = trim(strtolower($name));
		$check = DB::select("SELECT id FROM `tbl_lawyer_types` WHERE lower(name) = '$sr'");
		if(count($check) > 0){
		   return response()->json(['status'=>'error',"message"=>"Lawyer type already exist, try different one"],200);
		}

         // insert new one
		 $types = new LawyerTypes();
		 $types->name = $name;
		 $types->commission = $commission;
		 $types->save();
		 return response()->json(['status'=>'success',"message"=>"Lawyer type added successfully"],200);
	   }
	   else{
         // update existing one
		 $types = LawyerTypes::find($id);
		 $types->name = $name;
		 $types->commission = $commission;
		 $types->save();
		 return response()->json(['status'=>'success',"message"=>"Lawyer type updated successfully"],200);
	   }
	}


	function addCity(Request $request){
       $id = $request->id;
       $name = $request->name;

	   if($id == 0){
		$sr = trim(strtolower($name));
		$check = DB::select("SELECT id FROM `tbl_cities` WHERE lower(city_name) = '$sr'");
		if(count($check) > 0){
		   return response()->json(['status'=>'error',"message"=>"City name already exist, try different one"],200);
		}

         // insert new one
		 $types = new City();
		 $types->city_name = $name;
		 $types->save();
		 return response()->json(['status'=>'success',"message"=>"City added successfully"],200);
	   }
	   else{
         // update existing one
		 $types = City::find($id);
		 $types->city_name = $name;
		 $types->save();
		 return response()->json(['status'=>'success',"message"=>"City updated successfully"],200);
	   }
	}

	function addCategory(Request $request){
       $id = $request->id;
       $name = $request->name;

	   if($id == 0){

		$sr = trim(strtolower($name));
		$check = DB::select("SELECT id FROM `tbl_categories` WHERE lower(category_name) = '$sr'");
		if(count($check) > 0){
		   return response()->json(['status'=>'error',"message"=>"Category name already exist, try different one"],200);
		}

         // insert new one
		 $types = new Category();
		 $types->category_name = $name;
		 $types->save();
		 return response()->json(['status'=>'success',"message"=>"Category added successfully"],200);
	   }
	   else{
         // update existing one
		 $types = Category::find($id);
		 $types->category_name = $name;
		 $types->save();
		 return response()->json(['status'=>'success',"message"=>"Category updated successfully"],200);
	   }
	}


	function addDesgination(Request $request){
       $id = $request->id;
       $name = $request->name;

	   if($id == 0){

		$sr = trim(strtolower($name));
		$check = DB::select("SELECT id FROM `tbl_designations` WHERE lower(designation) = '$sr'");
		if(count($check) > 0){
		   return response()->json(['status'=>'error',"message"=>"Desgination name already exist, try different one"],200);
		}

         // insert new one
		 $types = new Designation();
		 $types->designation = $name;
		 $types->save();
		 return response()->json(['status'=>'success',"message"=>"Desgination added successfully"],200);
	   }
	   else{
         // update existing one
		 $types = Designation::find($id);
		 $types->designation = $name;
		 $types->save();
		 return response()->json(['status'=>'success',"message"=>"Desgination updated successfully"],200);
	   }
	}

	public function lawyer_types(){
		 $records =  LawyerTypes::orderBy('id','desc')->get();
		 return view('/pages/admin/lawyer_types',['records'=> $records]);
	}

	public function config_city(){
		 $records =  City::orderBy('id','desc')->get();
		 return view('pages.admin.config_city',['records'=> $records]);
	}

	public function config_desgination(){
		 $records =  Designation::orderBy('id','desc')->get();
		 return view('pages.admin.config_designation',['records'=> $records]);
	}

	public function config_category(){
		 $records =  Category::orderBy('id','desc')->get();
		 return view('pages.admin.config_category',['records'=> $records]);
	}

	public function reject_req(){
		$records =  RejectInvitation::orderBy('id','desc')->where('status','0')->get();
		return view('/pages/admin/rejectrequest',['records'=> $records]);
	}

	public function reject_details($id){
		$reject_details =  RejectInvitation::where('id',$id)->first();
		$user_invitations = user_invitations::where('id',$reject_details->invitation_id)->first();
		$patient = Patient::where('id',$reject_details->patient_id)->first();
		$lawyer = User::where('id',$user_invitations->inviter_user_id)->first();
		$doctor = User::where('id',$user_invitations->invitee_user_id)->first();

		return view("pages.admin.reject_details",
		[
			'reject_request' => $reject_details,
			'invitation' => $user_invitations,
			'patient' => $patient,
			'lawyer' => $lawyer,
			'doctor' => $doctor,
		]
	    );
	}

	public function changeDateRequest(Request $request){
		$id = $request->id;
		$date = $request->date;
		$reject_details =  RejectInvitation::where('id',$id)->first();
	
		$user_invitations = user_invitations::where('id',$reject_details->invitation_id)->first();
		$patient = Patient::where('id',$reject_details->patient_id)->first();

		$from = date("d-m-y",strtotime($user_invitations->invitation_date));
		$to = date("d-m-y",strtotime($date));
		$des1 = "The invitation date has been changed by admin from: ".$from
		.", to: ".$to." on request of patient: ".$patient->name;

		$reject_details->status = 1;
		$reject_details->save();

		$user_invitations->invitation_date = $date;
		$user_invitations->duedate = $date;
		$user_invitations->is_patient_accepted = 'yes';
		$user_invitations->save();
		
		Mail::to(MyHelper::Decrypt($patient->email),$patient->name)
		->send(new RejectResponsePatient($patient->name,$from,$to));

		$notif = new Nofication();
		$notif->url = "patient_details/".$reject_details->patient_id."?doctorid=".$user_invitations->invitee_category_id;
		$notif->description = $des1;
		$notif->to_id = $user_invitations->inviter_user_id; //lawyer
		$notif->from_id = Auth::id();
		$notif->save();

		$notif = new Nofication();
		$notif->url = "doctor-dashboard/my-invitations/#confirmed-invitations";
		$notif->description = $des1;
		$notif->to_id = $user_invitations->invitee_user_id; //doctor
		$notif->from_id = Auth::id();;
		$notif->save();

		return response()->json(["status"=>'1','message'=>"Date change successfully"],200);
	}

	/*Logout Admin*/
	 public function admin_logout()
    {
		if(Auth::check()){
			Session::flush();
			Auth::logout();
		}
		header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
		header("Pragma: no-cache"); // HTTP 1.0.
		header("Expires: 0"); // Proxies.
		session()->flash('logout-alert-success', 'Success logged out');
		return redirect('/admin');
    }
	/*All Doctors View*/
	 public function doctors($type)
    {
		$desg = Designation::get();
		$desAr = [];
		foreach($desg as $it){
			$desAr[$it->id] = $it->designation;
		}
		
		$user = User::where('role_id', '=',1);
		if(!empty($type) && $type != '' && $type != 'all'){
			$user->where('status', '=',$type);
		}
		$record = $user->orderBy('id','desc')->get();
		return view('/pages/admin/doctors', [
			'users' => $record,
			'desgination' => $desAr
		 ]);
			
			
    }
	public function get_doctors_paginate(Request $request)
	{
		if($request->ajax())
		{
			$search_keyword = $request->post('query');
			$sort_by = $request->post('sort_type');
			$sort_type = $request->post('sort_by');
			
			
			$users = User::where('role_id',1);
			if($search_keyword){
				$users->where('name', 'like', '%' .$search_keyword. '%')
				->where('email', 'like', '%' .$search_keyword. '%')
				->orWhere('phone', 'like', '%' .$search_keyword. '%')
				->orWhere('status', 'like', '%' .$search_keyword. '%')
				->orWhere('created_at', 'like', '%' .$search_keyword. '%')->paginate(1);
			}
			$users = $users->paginate(3);
			
			$paginate_number=3;
			$pagenumber=$request->page;
			if($pagenumber==null OR $pagenumber==1){
				$number=1;
			}
			elseif($pagenumber==2){
				$number=($paginate_number+1);
			}
			else{
				$number=((($pagenumber-1)*$paginate_number)+1);
			}
			
			 $view = View('pagination_views.doctors_paginate',['users'=>$users,'number'=>$number])->render();
            return ['status'=>'success','data'=>$view,'message'=>'Users fetched successfully'];
			
			
		}	
	}	
	/*Manage Appointments*/
	 public function manage_appointments(Request $request)
    {	
		$lawyer_invoice_number = Invites_Payments::orderBy('lawyer_invoice_number', 'DESC')->pluck('lawyer_invoice_number')->first();
		$settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
		$expireTime =strtotime('-'.$settings->invitation_expire_in_minutes.' minutes');
		$keyword=$request->keyword;	
		$id=$request->id;
			 $keyword=$request->keyword;	
			$userint = new user_invitations();
			if($id) {
				$userint = $userint->whereHas('user1',function($q) use($id) {
					$q->where('id',$id);
				});
			}
			if($request->keyword){ 			
				$userint = $userint->where(function($qry) use($keyword){				
					$qry->whereHas('user', function($q) use($keyword){
					  $q->where('name', 'like','%'.$keyword.'%');  
					})->orWhereHas('user1', function($y) use($keyword){
					  $y->where('name', 'like','%'.$keyword.'%');   
					 })->orWhereHas('patient', function($y) use($keyword){
					  $y->where('name', MyHelper::Encrypt($keyword));   
					 });
				});
			}

			$userint = $userint->orderBy('invitation_date', 'DESC')->paginate(100);
			$userint->appends(['keyword' => $keyword,'id'=>$id]);	
			$pagenumber=$request->page ? : 1;
			$number=($pagenumber-1)*100;
			$number = $number ? :1;
			
			return view('/pages/admin/manage_appointments', [
				'userint' => $userint,
				'number'=>$number,
				'lawyer_invoice_number'=>$lawyer_invoice_number,
				'expire_time'=>$expireTime
			]); 
	}

	/*All Lawyers list*/
	public function lawyers($type)
     {
		$types = LawyerTypes::orderBy("id","desc")->get();
		$user = User::where('role_id', '=',2 );
		if(!empty($type) && $type != '' && $type != 'all'){
			$user->where('status', '=',$type);
		}
		$record = $user->orderBy('id','desc')->get();
		$typeList = [];
		
        foreach($types as $item){
           $typeList[$item->id] = $item->name;
		}

		return view('/pages/admin/lawyers', [
			'users' => $record,
			'types' => $types,
			'typeList' => $typeList,
		]);      
    }


	/*All Lawyers list*/
	public function clinic($type)
     {
		$types = LawyerTypes::orderBy("id","desc")->get();
		$user = User::where('role_id', '=',4);
		if(!empty($type) && $type != '' && $type != 'all'){
			$user->where('status', '=',$type);
		}
		$record = $user->orderBy('id','desc')->get();
		$typeList = [];
        foreach($types as $item){
           $typeList[$item->id] = $item->name;
		}

		return view('/pages/admin/clinics', [
			'users' => $record,
			'types' => $types,
			'typeList' => $typeList,
		]);      
    }

	public function changesLawyerStatus(Request $request){
		// TODO: add verification email functinality
		$id = $request->id;
		$type = $request->type;
		$lawyerType = $request->lawyerType;
        $user = User::find($id);
        
		$email = $user->email;
		Mail::to($email,$user->name)
		->send(new UserAccountVerificationEmail($user->name));

        $user->authentication_status = 'active';
        $user->lawyer_type = $lawyerType;
        $user->save();
		return response()->json(['status'=>'success','message'=>"Account status changed successfully"],200);
	}

	/*Doctor Payment Status Update*/
	public function doctor_payment_status_update(Request $request){		
		
		$payment_status=$request->status;
		$invitation_id=$request->invitid;
		$doctor_user_id=$request->doctorid;
		$keyword=$request->keyword;
		
		$invoice=Invites_Payments::where('invitation_id', $invitation_id)->first();
		
		$invoice->payment_status=$payment_status;
		$invoice->save();
		 if($keyword!=null)   {       
		   return response()->json(['status'=>'success','url'=>url('manage_appointments')."?keyword=".$keyword]);   
         }	
         else{
			  return response()->json(['status'=>'success','url'=>url('manage_appointments')]);  
		 }		 
	}

    public function admin_assess_details($invitation_id){
        $invitation = user_invitations::where('unique_id',$invitation_id)->first();
		$patient = $invitation->patient;
		$user = $invitation->user;

        return view("pages.admin.assessmentDetailsAdmin",[
			'invitation'=>$invitation,
			'patient' => $patient, 
			'user' => $user,
        ]);
    }

	/*Doctor File*/
	public function doctor_file($id){
		$user = User::find($id);
		$designation = Designation::where('id',$user->desgination)->first();
		$ratings = Helper::doctorRating($user->id);
		$doctor = User::where("id",$id)->first();

		$new_ass = [];
		$accepted_ass = [];
		$confirmed_ass = [];
		$completed_ass = [];
		$expired_ass = [];
		$reject_ass = [];
		$cancelled_ass = [];
		$archived_ass = [];

		$settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
		$expireTime =date('Y-m-d H:i:s',strtotime('-'.$settings->invitation_expire_in_minutes.' minutes'));
 
		$assessment = user_invitations::where("invitee_user_id",$id)->get();
		foreach($assessment as $invitation){
            if($invitation->status == 'pending' && strtotime($invitation->created_at) > strtotime($expireTime)) {
              array_push($new_ass,$invitation);
            }
            else if($invitation->status == 'active' && $invitation->is_patient_accepted == 'yes') {
				array_push($confirmed_ass,$invitation);
			}
            elseif($invitation->status == 'rejected') {
                array_push($reject_ass,$invitation);
			}
			elseif($invitation->status == 'completed') {
                array_push($completed_ass,$invitation);
			}
			elseif($invitation->status == 'cancelled') {
                array_push($cancelled_ass,$invitation);
			}
            elseif($invitation->status == 'active' && $invitation->is_patient_accepted == 'no') {
                array_push($accepted_ass,$invitation);
			}
            elseif($invitation->status == 'pending' && $invitation->is_archive == 'no' && strtotime($invitation->created_at) <= strtotime($expireTime)) {
				array_push($expired_ass,$invitation);
			}
            elseif($invitation->is_archive == 'yes') {
				array_push($archived_ass,$invitation);
			}

       }

		return view('/pages/admin/doctor_file',[
			'user' => $user,
			'designation' => $designation->designation,
			'rating' => $ratings,
			'doctor' => $doctor,
			'new_ass' => $new_ass,
			'doctor_category_id'=>$doctor->doctor->category_id,
			'accepted_ass' => $accepted_ass,
			'confirmed_ass' => $confirmed_ass,
			'completed_ass' => $completed_ass,
			'expired_ass' => $expired_ass,
			'reject_ass' => $reject_ass,
			'cancelled_ass' => $cancelled_ass,
			'archived_ass' => $archived_ass,
		]); 
	}
	/*Lawyer File*/
	public function lawyer_file($id){
		$user = User::find($id);
		$patient = Patient::where('lawyer_id',$user->id)->get();
		return view('/pages/admin/lawyer_file',['user' => $user,'patient'=>$patient]); 
	}
	/*Lawyer Agreement*/
	public function lawyer_agreement($id){
		$user = User::find($id);
		$file = $user->terms_agreement;
        $name = basename($file);
        return response()->download($file, $name);
		//return view('/pages/admin/lawyer_file',['user' => $user]); 
	}
	/*change Password*/
	public function change_password(){
		return view('/pages/admin/change_password');
	}
	/*set password*/
	 public function set_new_password(Request $request)
    {
        $validator= Validator::make($request->all(),[
                'current_password'=>'required',
                'password'=>'required|confirmed|string|min:8'
                ]);
            $validator->after(function ($validator) use($request) {
                $current_db_password = Auth::User()->password;           
                if(!Hash::check($request->current_password, $current_db_password))
                {
                    $validator->errors()->add('current_password','You entered wrong current password.'); 
                } 
            });
        $validator->validate();

        $user =User::find(Auth::user()->id);
        $user->password = Hash::make($request->password);
        if($user->save()){
            Session::flash('message', 'Password changed successfully!'); 
            Session::flash('alert-class', 'alert-success');
            return redirect('admin/change-password');
        }
    }
	public function lawyerTransportationTranslation(Request $request)
	{		
			if($request->keyword){ 	
                $keyword=$request->keyword;	
				$userint = user_invitations::where('status','=','active')->where(function($q1){
					$q1->whereIn('transportation', ["yes"])
					->orwhereIn('translation', ["yes"])
					->orwhereIn('amendment', ["yes"]);
					})
					->where(function($qry) use($keyword){				
					$qry->whereHas('user', function($q) use($keyword){
					  $q->where('name', 'like','%'.$keyword.'%');  
					})->orWhereHas('user1', function($y) use($keyword){
					  $y->where('name', 'like','%'.$keyword.'%');   
					 })->orWhereHas('patient', function($y1) use($keyword){
					  $y1->where('name',MyHelper::Encrypt($keyword));   
					 });	  
			    })->orderBy('id', 'desc')->paginate(2);
				
				$paginate_number=2;
				$pagenumber=$request->page;
				if($pagenumber==null OR $pagenumber==1){
					$number=1;
				}
				elseif($pagenumber==2){
					$number=($paginate_number+1);
				}
				else{
					$number=((($pagenumber-1)*$paginate_number)+1);
				}	
               $userint->appends(['keyword' => $keyword]);					
			   return view('/pages/admin/lawyerTransportationTranslation', ['userint' => $userint,'number'=>$number]); 					  
		 }
		else{			
				$userint = user_invitations::where('status','=','active')->where(function($q1){
				$q1->whereIn('transportation', ["yes"])
				->orwhereIn('translation', ["yes"])
				->orwhereIn('amendment', ["yes"]);
				})->orderBy('id', 'desc')->paginate(2);
			$paginate_number=2;
			$pagenumber=$request->page;
			if($pagenumber==null OR $pagenumber==1){
				$number=1;
			}elseif($pagenumber==2){
				$number=($paginate_number+1);
			}
			else{
				$number=((($pagenumber-1)*$paginate_number)+1);
			}
			return view('/pages/admin/lawyerTransportationTranslation', ['userint' => $userint,'number'=>$number]); 
		}
		
		
	}

	public function admin_invoice_modal_view(Request $request){

		$invitation= user_invitations::where('id', $request->id)->first(); 
		
		$doctorname=ucfirst($invitation->user1->name);
		$doctorlicencenumber=ucfirst($invitation->user1->doctor->licence_number);
		$doctorphone=$invitation->user1->phone;
		$invitationdate=date('F d, Y', strtotime($invitation->invitation_date));
		$patientname=ucfirst($invitation->patient->name);
		$patientdateofbirth=date('F d, Y', strtotime($invitation->patient->dateofbirth));
		$patientdateofaccident=date('F d, Y', strtotime($invitation->patient->dateofaccident));
		$serviceprovided=ucfirst($invitation->invoices->serviceprovided);
		$date=date("Y-m-d"); 
		$todaydate=date('F d, Y', strtotime($date));
		
        $categoryname = "";
		$category = Helper::categorydetails();  
		foreach ($category as $categories){ 
		if(($categories->id)==($invitation->user1->doctor->category_id)){
			$categoryname=ucfirst($categories->category_name); }
		}
		return response()->json(['status'=>'success','doctorname'=>$doctorname,'categoryname'=>$categoryname,'doctorlicencenumber'=>$doctorlicencenumber, 'doctorphone'=>$doctorphone,'invitationdate'=>$invitationdate, 'patientname'=>$patientname, 'patientdateofbirth'=>$patientdateofbirth,'patientdateofaccident'=>$patientdateofaccident,'serviceprovided'=>$serviceprovided,'todaydate'=>$todaydate]);
	

	}
	//start
	public function custominvoice(Request $request){
		$date=date("Y-m-d");
		$invitation = user_invitations::where('id',$request->id)->first();
		$user = Invites_Payments::where('invitation_id', $request->id)->first(); 
		$lawyer_invoice_number = Invites_Payments::orderBy('lawyer_invoice_number', 'DESC')->pluck('lawyer_invoice_number')->first();
		$pdf = md5(time()).'.pdf';
		if($user->lawyer_invoice_filename != null){				
						$user->lawyer_payment_amount=$request->billamt;
							$user->othercosts=$request->othercosts;
						$user->lawyer_invoice_date=$date;
						
						$delimgfile='uploads/lawyer_custom_invoice/'.$user->lawyer_invoice_filename;
						if(file_exists($delimgfile))
						unlink($delimgfile);
						
						$user->lawyer_invoice_filename=$pdf;								
						$user->save();	

						if($user->id){						
							$upload_dir = public_path(); 
							$filename = $upload_dir.'/uploads/lawyer_custom_invoice/'.$pdf; 
							$mpdf = new \Mpdf\Mpdf();
							$mpdf->WriteHTML(\View::make('pages.doctor.admininvoicemail')->with('invitation',$invitation)->render());
							$mpdf->Output($filename,'F');
							
							$emailCtrl = new EmailController();
							$emailCtrl->send_invoice_admin_to_lawyer($invitation); 						
							
							return response()->json(['status'=>'success','success'=>'Invoice Sent Successfully','url'=>url('manage_appointments')]); 
						} 
				}
				else{
					$user->lawyer_payment_amount=$request->billamt;
					$user->lawyer_invoice_date=$date;					
					$user->othercosts=$request->othercosts;					
					if($lawyer_invoice_number==null){
						$user->lawyer_invoice_number=1;
					}
					else{
						$user->lawyer_invoice_number=$lawyer_invoice_number+1;
					}
					
					$user->lawyer_invoice_filename=$pdf;								
					$user->save();	
					if($user->id){						
						$upload_dir = public_path(); 
						$filename = $upload_dir.'/uploads/lawyer_custom_invoice/'.$pdf; 
						$mpdf = new \Mpdf\Mpdf();
						$mpdf->WriteHTML(\View::make('pages.doctor.admininvoicemail')->with('invitation',$invitation)->render());
						$mpdf->Output($filename,'F');
						
						$emailCtrl = new EmailController();
						$emailCtrl->send_invoice_admin_to_lawyer($invitation); 						
						
						return response()->json(['status'=>'success','success'=>'Invoice Sent Successfully','url'=>url('manage_appointments')]); 
					}
				}
			
	}
		//end

	/*Doctor Status Update*/
	public function doctor_status_update(Request $request){	

		$user=User::where('id', $request->userid)->first();		
		$user->authentication_status =$request->status;
		$user->save();
		if($user->id)   {       
		   return response()->json(['status'=>'success','url'=>url('doctors')]);   
         }	 
	}

	/*Lawyer Status Update*/
	public function lawyer_status_update(Request $request){	

		$user=User::where('id', $request->userid)->first();		
		$user->authentication_status=$request->status;
		$user->save();
		if($user->id)   {  
			$email_controller = new EmailController();	
			if($request->status=='active'){
				$email_controller->send_approve_notification_to_lawyer($user);
			}else{
				$email_controller->send_decline_notification_to_lawyer($user);
			}
		   return response()->json(['status'=>'success','url'=>url('lawyers')]);   
         }	 
	}

	public function cms(){
        $pages = Pagecms::where('is_active','yes')->get();
  
		return view('/pages/admin/cms', ['pages' => $pages]); 
	}
	public function editpage($slug){
        $page = Pagecms::where('page_slug',$slug)->first();

        $setting_about_image = AdminSettings::where('settings_name','aboutus_image')->first();

        $team_pic_1 = AdminSettings::where('settings_name','team_pic_1')->first();
        $team_pic_2 = AdminSettings::where('settings_name','team_pic_2')->first();
        $team_pic_3 = AdminSettings::where('settings_name','team_pic_3')->first();
        $team_pic_4 = AdminSettings::where('settings_name','team_pic_4')->first();

        return view('/pages/admin/editpage', ['page' => $page, 'setting_about_image'=>$setting_about_image, 'team_pic_1'=>$team_pic_1, 'team_pic_2'=>$team_pic_2,  'team_pic_3'=>$team_pic_3, 'team_pic_4'=>$team_pic_4]); 
		
	}

	public function edit_page(Request $request){
		
		$validatedData = $request->validate([
            'aboutus_image' => 'mimes:jpg,jpeg,png|max:2048',
            'team_pic_1' => 'mimes:jpg,jpeg,png|max:2048',
            'team_pic_2' => 'mimes:jpg,jpeg,png|max:2048',
            'team_pic_3' => 'mimes:jpg,jpeg,png|max:2048',
            'team_pic_4' => 'mimes:jpg,jpeg,png|max:2048',
        ]);  

        if($request->file('aboutus_image')){

             $file = $request->file('aboutus_image');
             $filename = time().'.'.$file->getClientOriginalExtension();
             $file->move('uploads/aboutus', $filename);

             $setting_about_image = AdminSettings::where('settings_name','aboutus_image')->first();

             if($setting_about_image->settings_value !=null){
             		$delimgfile='uploads/aboutus/'.$setting_about_image->settings_value;
            		@unlink($delimgfile);
             }
              $setting_about_image->settings_value=$filename;
              $setting_about_image->save(); 
        } 
        $team_pic_1 = AdminSettings::where('settings_name','team_pic_1')->first();
        if($request->file('team_pic_1')!=null){

             $file1 = $request->file('team_pic_1');
             $filename1 = time().'.'.$file1->getClientOriginalExtension();
             $file1->move('uploads/aboutus', $filename1);

             if($team_pic_1->settings_value !=null){
             		$delimgfile='uploads/aboutus/'.$team_pic_1->settings_value;
            		@unlink($delimgfile);
             }
              $team_pic_1->settings_display_name=$request->team_pic_1_name;
              $team_pic_1->settings_value=$filename1;
              $team_pic_1->save(); 
        }else{
        	$team_pic_1->settings_display_name=$request->team_pic_1_name;
            $team_pic_1->save();
        }
        $team_pic_2 = AdminSettings::where('settings_name','team_pic_2')->first();
        if($request->file('team_pic_2')!=null){

             $file2 = $request->file('team_pic_2');
             $filename2 = time().'.'.$file2->getClientOriginalExtension();
             $file2->move('uploads/aboutus', $filename2);

             if($team_pic_2->settings_value !=null){
             		$delimgfile='uploads/aboutus/'.$team_pic_2->settings_value;
            		@unlink($delimgfile);
             }
              $team_pic_2->settings_display_name=$request->team_pic_2_name;
              $team_pic_2->settings_value=$filename2;
              $team_pic_2->save(); 
        } else{
        	$team_pic_2->settings_display_name=$request->team_pic_2_name;
              $team_pic_2->save();
        }

         $team_pic_3 = AdminSettings::where('settings_name','team_pic_3')->first();

        if($request->file('team_pic_3')!=null){

             $file3 = $request->file('team_pic_3');
             $filename3 = time().'.'.$file3->getClientOriginalExtension();
             $file3->move('uploads/aboutus', $filename3);

             if(($team_pic_3 !=null)&& ($team_pic_3->settings_value !=null)){
             		$delimgfile='uploads/aboutus/'.$team_pic_3->settings_value;
            		@unlink($delimgfile);
             }
              $team_pic_3->settings_display_name=$request->team_pic_3_name;
              $team_pic_3->settings_value=$filename3;
              $team_pic_3->save(); 
        }else{
        	$team_pic_3->settings_display_name=$request->team_pic_3_name;
              $team_pic_3->save(); 
        } 

        $team_pic_4 = AdminSettings::where('settings_name','team_pic_4')->first();
        
        if($request->file('team_pic_4')!=null){

             $file4 = $request->file('team_pic_4');
             $filename4 = time().'.'.$file4->getClientOriginalExtension();
             $file4->move('uploads/aboutus', $filename4);             

             if(($team_pic_4 !=null)&& ($team_pic_4->settings_value !=null)){
             		$delimgfile='uploads/aboutus/'.$team_pic_4->settings_value;
            		@unlink($delimgfile);
             }
              $team_pic_4->settings_display_name=$request->team_pic_4_name;
              $team_pic_4->settings_value=$filename4;
              $team_pic_4->save(); 
        } else{
        	$team_pic_4->settings_display_name=$request->team_pic_4_name;
              $team_pic_4->save(); 
        }


        $page = Pagecms::where('page_slug',$request->slug)->first();

        $page->content=$request->content;

        $page->save();

        if($page->id){

       		return back()->with('success','Page Updated successfully!');
    	}
		
	}

	public function lawyer_email_update(Request $request){		

		$emailexists = User::where('email',$request->email)->first();		

		if($emailexists){
				
				 return response()->json(['status'=>'emailexists']);  
		}
		else{
			$user = User::where('id',$request->lawyerid)->first();
			$user->email=$request->email;
			$user->save();
			if($user->id){
				 return response()->json(['status'=>'success']);  
			}
		}

	}

	public function settings(){
		$settings = AdminSettings::where('is_active','yes')->get();
		if($settings!=null){
			return view('/pages/admin/admin_settings',['settings'=>$settings]); 
		}else{
			return view('/pages/admin/admin_settings'); 
		}
	}
	public function addsettings(){
		
			return view('/pages/admin/addsettings'); 
		
	}

	public function settings_update(Request $request){

			$settings = new AdminSettings();
			$settings->settings_name=$request->settingsname;
			$settings->settings_display_name=$request->settingsname;
			$settings->settings_value=$request->settingsvalue;
			$settings->save();
			return redirect()->route('admin/addsettings')->with('success','New Settings Added Successfully!');		
		
	}

	public function editsettings($id){

			$settings = AdminSettings::where('id',$id)->first();

			//dd($settings);
		
			return view('/pages/admin/editsettings',['settings'=>$settings]); 
		
	}

	public function edit_settings(Request $request){ 

			$settings = AdminSettings::where('id',$request->id)->first();
			$settings->settings_display_name=$request->settings_display_name;
			$settings->settings_value=$request->settings_value;
			$settings->save();
			return redirect()->route('admin/settings')->with('success','Settings Updated Successfully!');		
	}


	public function clinicProfile($id){
		$clinic = User::where("id",$id)->first();
		$doctor = User::where("clinic_id",$id)->get();

		$sub_tran = ClientPayment::where("user_id",$id)->get();
        $subscription = ClientSubscription::where('user_id',$id)->get();

		$monthly = 0;
		$yearly = 0;

		$offer = ClinicSpecialOffer::where('clinic_id',$id)->get();
		if(count($offer) > 0){
			$offer = $offer[0];
			$monthly = $offer["monthly"];
			$yearly = $offer["yearly"];
		}

		return view("pages.admin.clinicProfile",[
			"clinic" => $clinic,
			"doctor" => $doctor,
			'sub_tran' => $sub_tran,
			'monthly' => $monthly,
			'yearly'=>$yearly,
             'subscription'=> count($subscription) > 0 ? $subscription[0] : null
		]);
	}

	public function deleteSpecialOffer(Request $request){
		$clincID = $request->clincID;
		$offer = ClinicSpecialOffer::where('clinic_id',$clincID)->get();
		$offer[0]->delete();
		return response()->json(['status'=>'success']);
	}

	public function assignSpecailPrice(Request $request){
		$monthly = $request->monthly;
		$yearly = $request->yearly;
		$clincID = $request->clincID;

		$offer = ClinicSpecialOffer::where('clinic_id',$clincID)->get();
		if(count($offer) > 0){
           $n_offer = $offer[0];
		   $n_offer->monthly = $monthly;
		   $n_offer->yearly = $yearly;
		   $n_offer->clinic_id = $clincID;
		   $n_offer->update();
		}
		else{
			$n_offer = new ClinicSpecialOffer();
			$n_offer->monthly = $monthly;
			$n_offer->yearly = $yearly;
			$n_offer->clinic_id = $clincID;
			$n_offer->save();
		}
		return response()->json(['status'=>'success']);
	}

	public function packages(){
		$packages = ClinicPackages::get();
		return view('pages.clinic.packages_list',['packages'=>$packages]);
	}

	public function editPackages($id){
		$package = ClinicPackages::find($id);
		return view('pages.clinic.edit_package_view',['package'=>$package]);
	}

	public function updatePackage(Request $request){
		$package = ClinicPackages::find( $request->id);

        if($request->has("features")){
			$ar = $request->features;
	     	$ar = array_filter($ar);
			$package->features = json_encode($ar);
		}
		$package->amount = $request->amount;
		$package->save();
		return redirect()->route("packages",['packages_status'=>'1']);
	}
	
}
?>
