<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\doctors;
use App\Models\lawyers;
use App\Models\Opt_Handler;
use App\Helpers\Helper as MyHelper;
use App\Helper;
use App\Http\Controllers\EmailController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use Config;
use App\Models\RejectInvitation;
use Illuminate\Support\Facades\Crypt;
use App\Models\PriceChangeRequest;

class SigninController extends Controller
{
    
    public function index()
    {
        if(Auth::check() && Auth::user()->role_id == 3){
            $reject_req =  RejectInvitation::where('status','0')->count(); 
            $doctors=User::where('role_id', 1)->get();
            $doctorscount=count($doctors);
            $lawyers=User::where('role_id', 2)->get();
            $lawyerscount=count($lawyers);
            $changeRequest = PriceChangeRequest::where('status','0')->count();

            return view('/pages/admin/home', [
                'doctorscount' => $doctorscount,
                'lawyerscount' => $lawyerscount,
                'reject_req' => $reject_req,
                'changeRequest' => $changeRequest,
            ]);
        }
        else{
            return view('auth/login');
        }
    }
	
	/*Admin Login */
    public function login(Request $request)
    {  
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $remember = $request->has('remember') ? true : false;
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password'), 'status'=>'active', 'role_id'=>3], $remember)) {			
           // if ($request->input('remember') == 'yes') {	
		   if ($request->input('remember')!== null) {	
				//echo "hi";exit;
                setcookie('ad_email', MyHelper::Encrypt($request->input('email')), time() + (86400 * 3000), "/");
                setcookie('ad_password', MyHelper::Encrypt($request->input('password')), time() + (86400 * 3000), "/");
                setcookie('ad_remember', MyHelper::Encrypt($request->input('remember')), time() + (86400 * 3000), "/");
            } else 
                {
				//	echo "no";exit;
                unset($_COOKIE['ad_remember']);
                setcookie('ad_remember', null, -1, '/');
                setcookie('ad_email', null, -1, '/');
                setcookie('ad_password', null, -1, '/');
            }
          return redirect('dashboard');
        }   
        else {
			//echo "not working";exit;
            Session::flash('loginerror', 'These credentials do not match our records.');
            return redirect('admin');
            return back()->with('message', 'error');
        }
    }
	
    public function logout()
    {
		//alert('test');
     // Auth::logout();
    // return redirect('/');
	 Session::flush();
	 Auth::logout();
	 header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
	 header("Pragma: no-cache"); // HTTP 1.0.
	 header("Expires: 0"); // Proxies.
	 session()->flash('logout-alert-success', 'Success logged out');
	 return redirect('/');
    }


public function loginUser(Request $request)
{     	
        $remember = $request->has('remember') ? true : false;
        
// ******************* Lawyer and doctor login start *************************

            if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'status'=>'active','authentication_status'=>'active','role_id'=>$request->role_id],false,false)){
                Auth::logout();
                $user = User::where('email',$request->email)->first();
                
                $date = date('Y-m-d');
                $opt_hander = Opt_Handler::where('unique_id',$user->unique_id)->where('send_on',$date)->where('status','1')->get();
                
                if(count($opt_hander) > 0){
                   
                    if($user->id){
                        Auth::loginUsingId($user->id);
                        if ($remember == true) {				
                            setcookie('role_id', MyHelper::Encrypt($request->input('role_id')), time() + (86400 * 3000), "/");
                            setcookie('email', MyHelper::Encrypt($request->input('email')), time() + (86400 * 3000), "/");
                            setcookie('password', MyHelper::Encrypt($request->input('password')), time() + (86400 * 3000), "/");
                            setcookie('rember', true, time() + (86400 * 3000), "/");
                        } 
                        else 
                        {
                            unset($_COOKIE['rember']);
                            setcookie('rember', null, -1, '/');
                            setcookie('email', null, -1, '/');
                            setcookie('role_id', null, -1, '/');
                            setcookie('password', null, -1, '/');
                        }  
                    } 

                    $path = "";
                    switch($request->input('role_id')){
                        case 1:{
                            $path = "doctor-dashboard";
                            break;
                        }
                        case 2:{
                            $path = "lawyer-dashboard/search-doctors";
                            break;
                        }
                        case 4:{
                            $path = "clinic-dashboard";
                            break;
                        }
                    }
                   
                    return response()->json(['status'=>'login_success', 'message'=>'Login successfully','path'=>$path]);
                }
                else{
                    //basit
                    Opt_Handler::where('unique_id',$user->unique_id)->delete();

                    $email = Crypt::encryptString($request->email);
                    $password = Crypt::encryptString($request->password);
                    $otp = random_int(100000, 999999);

                    $email_controller = new EmailController();
                    $email_controller->sent_otp_to_lawyer($user->email,$user->name,$otp);    

                    $opt_h = new Opt_Handler();
                    $opt_h->send_on = $date;
                    $opt_h->unique_id = $user->unique_id;
                    $opt_h->email = $email;
                    $opt_h->password = $password;
                    $opt_h->otp = $otp;
                    $opt_h->save();

                    $unc = $opt_h->unique_id;

                    return response()->json(['status'=>'opt_send', 'message'=>'Otp sent successfully','unc'=>$unc]);
                }
            }
            elseif(Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'status'=>'inactive','authentication_status'=>'active','role_id'=>$request->role_id],false,false)){
                Auth::logout();
                return response()->json(['status'=>'inactive', 'message'=>'Acccount activation fail']);
            }
            elseif(Auth::attempt(['email'=>$request->email, 'password'=>$request->password, 'status'=>'active','authentication_status'=>'inactive','role_id'=>$request->role_id],false,false)){
                Auth::logout();
                return response()->json(['status'=>'admininactive', 'message'=>'Currently In active']);
            }
            else{
                return response()->json(['status'=>'error', 'message'=>'Invalid email or password']);
            }
// ******************* Lawyer and doctor login end   *************************
}


    public function loginUserWithOtp(Request $request)
    {  //abb
        $otp = $request->digit_1. $request->digit_2. $request->digit_3. $request->digit_4. $request->digit_5. $request->digit_6;
       // $otp = $request->loginotp;
        $unique = $request->otpuniqueid;

        $date = date("Y-m-d");
        $opt_hander = Opt_Handler::where('unique_id',$unique)->where('send_on',$date)->where('otp', $otp)->where('status','0')->get();
        
        // handle invalid otp senerio

        if(count($opt_hander) > 0){
            $email = Crypt::decryptString($opt_hander[0]->email);
            $password = Crypt::decryptString($opt_hander[0]->password);

            if(Auth::attempt(['email'=>$email, 'password'=>$password, 'status'=>'active','authentication_status'=>'active','unique_id'=>$unique],false,false)){
                Auth::logout();
                $user = User::where('email',$email)->where('unique_id',$unique)->first();

                $opt_hander[0]->status = 1;
                $opt_hander[0]->save();

                Auth::loginUsingId($user->id);

                $path = "";
                switch($user->role_id){
                    case 1:{
                        $path = "doctor-dashboard";
                        break;
                    }
                    case 2:{
                        $path = "lawyer-dashboard/search-doctors";
                        break;
                    }
                    case 4:{
                        $path = "clinic-dashboard";
                        break;
                    }
                }

                return response()->json(['status'=>'login_success', 'message'=>'Login successfully','path'=>$path]);
            }
            else{
                return response()->json(['status'=>'unkown', 'message'=>'Something went wrong']);
            }
        }
        else{
            return response()->json(['status'=>'invalid_otp', 'message'=>'Invalid otp']);
        }
        exit;


       print_r($request->all());
    }


    public function resendotp(Request $request){  
      //basit
      $otp = random_int(100000, 999999);
      $uniqueid = $request->uniqueid;
      $user = User::where('unique_id',$uniqueid)->first();
      $opt_hander = Opt_Handler::where('unique_id',$uniqueid)->first();
      $opt_hander->otp = $otp;
      $opt_hander->save();

      $email_controller = new EmailController();
      $email_controller->sent_otp_to_lawyer($user->email,$user->name,$opt_hander->otp);                  
      return response()->json(['status'=>'resendotpsuccess']);
    }

}

?>