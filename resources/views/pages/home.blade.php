@extends('layouts.default')
@section('content')

<link href="{{ asset('css/home.css')."?09000009" }}" rel="stylesheet">
<link href="{{ asset('css/home_section_new.css')."?0011111" }}" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/9.0.5/swiper-bundle.css" integrity="sha512-CTWIgc35lLPcCl1OP7MNcrrES+jyBBvMEz8Cqx/v0hifPNjIpPsd/jUYTJ/41CYCrQdfuw7LopKaqqjXVLqejg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<style>
.topHomeContainer{
    display: flex;
    flex-direction: column;
    background: url("{{asset('n_img/home-top-background.png')}}");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    height: 100vh;
}
</style>


<section class="topHomeContainer">
    @include('includes.nheader')

    <div class="superTop">

    <div class="top-banner">
  
       <div class="br-slide1">
           {{-- <h2 class="topIconanimation">Streamline Your Reporting Process with Pro Assessors</h2> --}}
           <h2 class="bounce3">Streamline Your Reporting Process with Pro Assessors</h2>
       </div>

       <div class="br-slide2">
          <div class="icons-container">
           <div class="hex-first-container">
              <div style="position: relative">
                 <img class="daimondImg" src="{{asset("n_img/hexogon.png")}}" alt="">
                 <img class="zoom-in-out-box centerImage" src="{{asset("icon/icon_1.png")}}" alt="">
              </div>
           </div>
           <div class="hex-second-container">

            <div style="position: relative">
                <img class="daimondImg" src="{{asset("n_img/hexogon.png")}}" alt="">
                <img class="bounce centerImage" src="{{asset("icon/icon_2.png")}}" alt="">
             </div>

            <div style="position: relative">
                <img class="daimondImg" src="{{asset("n_img/hexogon.png")}}" alt="">
                <img class="bounce centerImage" src="{{asset("icon/icon_3.png")}}" alt="">
             </div>

           </div>

           <div class="hex-third-container">
             <div style="position: relative">
                <img class="daimondImg" src="{{asset("n_img/hexogon.png")}}" alt="">
                <img class="zoom-in-out-box centerImage" src="{{asset("icon/icon_4.png")}}" alt="">
             </div>

             <div style="position: relative">
                <img class="daimondImg" src="{{asset("n_img/hexogon.png")}}" alt="">
                <img class="bounce2 centerImage" src="{{asset("icon/icon_5.png")}}" alt="">
             </div>

             <div style="position: relative">
                <img class="daimondImg" src="{{asset("n_img/hexogon.png")}}" alt="">
                <img class="zoom-in-out-box centerImage" src="{{asset("icon/icon_6.png")}}" alt="">
             </div>
           </div>
        </div>
       </div>

    </div>

    <div  class="st-line"></div>

   </div>
</section>



{{-- slider section --}}
<section class="container-fluid p-0 m-0" style="overflow: hidden">
    <div class="row new_slider_section">
        <div class="col-md-6 col-sm-12 first">
            <img class="slideImagess" src="assets/values.png" alt="">
        </div>
        <div class="col-md-6 col-sm-12 second">
            <div class="valueContainer">
             <p class="aboutfirst-heading" style="line-height: 1em; color:white;">Our Values</p>
            <div class="carouselBtn">
                <div class="swiper-button-prev swiper-navBtn"><a class="btn btn-primary" ><img src="assets/left.png" alt=""></a></div>
                <div class="swiper-button-next swiper-navBtn"><a class="btn btn-primary"><img src="assets/right.png" alt=""></a></div>
             </div>
           </div>

           <div class="slide-container swiper" style="margin-left: -200px;">
            <div class="slide-content">
                <div class="card-wrapper swiper-wrapper">

                <div class="card swiper-slide" style="border:solid #515C84">
                        <div class="thumb-wrapper" style="background-color: #515C84;height:180px;padding: 20px;">
                            <img src="assets/doctor.png" class="" width="40px" height="40px" alt="">
                            <p class="cardTitle" style="color:white;">Secure Transmission</p>
                            <p class="cardSubDesc" style="color:white;">Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</p>
                        </div>
                    </div>
                    <div class="card swiper-slide">
                        <div class="thumb-wrapper" style="height:180px;padding: 20px;">
                            <img src="assets/heart.png" class="" width="40px" height="40px" alt="">
                            <p class="cardTitle">Seamless Operation</p>
                            <p class="cardSubDesc">Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum </p>
                        </div>
                    </div>
                    <div class="card swiper-slide">
                        <div class="thumb-wrapper" style="height:180px;padding: 20px;">
                            <img src="assets/dentist.png" class="" width="40px" height="40px" alt="">
                            <p class="cardTitle">Accountability</p>
                            <p class="cardSubDesc">Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum </p>
                        </div>
                    </div>
                    
                    <div class="card swiper-slide">
                        <div class="thumb-wrapper" style="height:180px;padding: 20px;">
                            <img src="assets/heart.png" class="" width="40px" height="40px" alt="">
                            <p class="cardTitle">Mid-Legal Specialization</p>
                            <p class="cardSubDesc">Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum </p>
                        </div>
                    </div>
                    
                    <div class="card swiper-slide" style="border:solid #515C84">
                        <div class="thumb-wrapper" style="background-color: #515C84;height:180px;padding: 20px;">
                            <img src="assets/doctor.png" class="" width="40px" height="40px" alt="">
                            <p class="cardTitle" style="color:white;">Secure Transmission</p>
                            <p class="cardSubDesc" style="color:white;">Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</p>
                        </div>
                    </div>
                    <div class="card swiper-slide">
                        <div class="thumb-wrapper" style="height:180px;padding: 20px;">
                            <img src="assets/heart.png" class="" width="40px" height="40px" alt="">
                            <p class="cardTitle">Seamless Operation</p>
                            <p class="cardSubDesc">Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum </p>
                        </div>
                    </div>
                    <div class="card swiper-slide">
                        <div class="thumb-wrapper" style="height:180px;padding: 20px;">
                            <img src="assets/dentist.png" class="" width="40px" height="40px" alt="">
                            <p class="cardTitle">Accountability</p>
                            <p class="cardSubDesc">Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum </p>
                        </div>
                    </div>
                    
                    <div class="card swiper-slide">
                        <div class="thumb-wrapper" style="height:180px;padding: 20px;">
                            <img src="assets/heart.png" class="" width="40px" height="40px" alt="">
                            <p class="cardTitle">Mid-Legal Specialization</p>
                            <p class="cardSubDesc">Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum </p>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        </div>

        </div>
    </div>
</section>


{{-- New About Section --}}
<section class="container-fluid p-0 m-0" style="overflow: hidden">
  <div class="row p-0 m-0 new_about_section">
   <div class="col-md-7 col-sm-12 first">
    <p class="aboutfirst-heading " style="line-height: 1em;">About<br>
        Pro-Assessors</p>
        <div style="width: 80%;margin: 20px 0px;">
            <p class="aboutContent" >Pro Assessors is proud to be the first seamless, web-based platform of its kind that simplifies and streamlines the process of reporting by connecting clients with legal services and medical specialties of their choice to obtain professional opinions, literally at a glance.
                <br><br>
                This service is the first of its kind enabling users to interact with providers to request medical assessments and obtain med-legal reports in an efficient and effective manner by employing faster and simpler working methods.
                Book the required assessment and obtain a...... <a href="about">READ MORE</a> 
            </p>
        </div>
   </div>

   <div class="col-md-5 col-sm-12 second">
       <p>
           This service is the first of its kind enabling users to interact with providers to request medical assessments and obtain med-legal reports
       </p>
       <div>
           <img class="aboutsImage" src="assets/about.png" alt="">
       </div>
   </div>
</div>
</section>


{{-- Contact us section --}}
<section id="contact" class="contact_us_section container-fluid pb-5">

    <div class="wrapper11">
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
    </div>

    <div class="container-fluid-sm">
    <div class="row p-3 bottomRow px-0">
    <div class="col-md-5 col-sm-12 py-2" style="display: flex;justify-content: center;align-items: center;">
        <h1 class="contactDetails">
            Looking for an expert? <br> Make a Request for an Assessment
        </h1>
    </div>

    <div class="col-md-7 p-0 col-sm-12 topSecondContainer">
       <div class="formcontainer">
        <div class="alert alert-success fade show" role="alert" id="successform" style="display:none;">
            Thank you for reaching us, your form is submitted successfully.
        </div>
            <form id="assessment_form">
            @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label class="customerLabel">First Name</label>
                            <input maxlength="50"  type="text" class="form-control ndesigninput" name="fname" id="fcname" aria-describedby="fname" placeholder="First Name">
                            <span id="ctr_fname" class="contactFormError">Error here</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label class="customerLabel">Last Name</label>
                            <input maxlength="50"  type="text" class="form-control ndesigninput" id="lcastname" name="lastname" aria-describedby="lname" placeholder="Last Name">
                            <span id="ctr_lname" class="contactFormError">Error here</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label class="customerLabel">Email</label>
                            <input maxlength="70"  type="email" class="form-control ndesigninput" name="emailaddress" id="emailaddresscs" aria-describedby="email" placeholder="Email" value="@if(Auth::check()) {{trim(Auth::user()->email)}} @endif">
                            <span id="ctr_email" class="contactFormError">Error here</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label class="customerLabel">Phone Number</label>
                            <input maxlength="12"  type="text" class="form-control ndesigninput" name="phone" id="phonecs" aria-describedby="phone" placeholder="Phone Number" value="@if(Auth::check()) {{trim(Auth::user()->phone)}} @endif">
                            <span id="ctr_phone" class="contactFormError">Error here</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group mb-3">
                            <label class="customerLabel">Subject</label>
                            <input maxlength="254"  type="text" class="form-control ndesigninput" name="subject" id="cssubject" aria-describedby="subject" placeholder="Subject">
                            <span id="ctr_subject" class="contactFormError">Error here</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group ">
                            <label class="customerLabel">What can we help you with?</label>
                            <textarea maxlength="1000" class="form-control ndesigninputtextarea" id="csmessage" name="message" rows="3" placeholder="What can we help you with?"></textarea>
                            <span id="ctr_message" class="contactFormError">Error here</span>
                        </div>
                    </div>
                </div>

                <div style="display: flex;justify-content: center;">
                    <button  type="button" id="invitebtn1" class="btn btn-primary submit_btn222">Submit</button>
                </div>
            </form>
        </div>
    </div>
    </div>
</div>
</section>

@include('common.cookiePermissionPopup');

@endsection


