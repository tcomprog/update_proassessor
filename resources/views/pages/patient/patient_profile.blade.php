@extends('layouts.lawyer-das-custom') 

@section('content')	
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.error{color:red !important;}

.deisngCls{
    box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
    border-radius: 10px;
    border-left: 7px solid #515C84;
}

.profileTit{
    box-sizing: border-box;
    height: 52px;
    background: #515C84;
    border-radius: 40px;
    color:white;
    font-weight: 700;
    font-size: 30px;
    line-height: 52px;
    text-align: center;
    margin-bottom: 20px;
    width:55%;
    margin-left: auto;
    margin-right: auto;
}

.inputsss,select{
    border-radius:30px !important;
    height: 40px !important;
    border: 1px solid #515C84 !important;
}

label{
  color: #535D82 !important;
  font-size: 15px !important;
  font-weight: bold !important;
  padding-left: 8px !important;
  margin-bottom: 3px !important;
}
.form-group{
    margin-bottom: 15px !important;
}
.update_btn{
    margin-left: auto !important;
    margin-right: auto !important;
    background: linear-gradient(246.33deg, #20215E 34.76%, rgba(136, 144, 185, 0.59) 70.87%) !important;
    border-radius: 30px !important;
    height: 48px !important;
    margin-block: 0px !important;
    width: 330px !important;
    font-weight: bold !important;
}
</style>

	 <!-- Banner Section -->

    <section class="intro_banner_sec inner_banner_sec" style="padding:20px 0px;">
        <div class="container">

            <div class="row align-items-center"> 

                <div class="col-md-6">

                    <div class="welcome_text">

                        <h1>Profile</h1>

                    </div>

                </div>
               
            </div>
        </div>
    </section>
	<section class="dashboard_sec" style="padding-top:20px;">

        <div class="container deisngCls p-4">

            <div class="tab-content" id="nav-tabContent">
			
				<div class="tab-pane fade show active " id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

				<div class="row">

                    <div class="alert alert-success fade show" role="alert" id="updatesuccess" style="display:none;">
                        Profile updated successfully
                    </div>

                <div class="col-md-12">
                    <form class="profile_update" id="updatePatientProfile">
                        
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group" style="margin-bottom:0px;">
                                        <label style="font-weight: bold; margin-right:15px;" for="dob">Gender</label> 
                                        
                                        <input type="radio" id="male" name="gender" value="male" <?php if(MyHelper::Decrypt($patient->gender)=='male'){echo 'checked';} ?> >
  											<label for="male" >Male</label>&nbsp;&nbsp;&nbsp;
                                        
                                        <input type="radio" id="female" name="gender" value="female" 
                                        <?php if(MyHelper::Decrypt($patient->gender)=='female'){echo 'checked';} ?>>
  											<label for="female">Female</label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<label id="gender-error" class="error" for="gender"></label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">								
                                    <label for="fname">First name</label>
                                    <input maxlength="150" type="text" class="form-control inputsss" id="fname" name="fname" aria-describedby="fname" placeholder="Name" value="{{ ucfirst($patient->fname) }} ">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">								
                                    <label for="lname">Last name</label>
                                    <input maxlength="150" type="text" class="form-control inputsss" id="lname" name="lname" aria-describedby="lname" placeholder="Name" value="{{ ucfirst($patient->lname) }} ">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">								
                                    <label for="filenumber">File Number</label>
                                    <input maxlength="150" type="text" class="form-control inputsss" id="filenumber" name="filenumber" aria-describedby="filenumber" placeholder="Name" value="{{ ucfirst(MyHelper::Decrypt($patient->filenumber)) }} ">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control inputsss" id="email" name="email" aria-describedby="email" placeholder="Email" value="{{ $users->email }}" disabled>
                                </div>
                            </div>
                           
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <input type="text" class="form-control inputsss" id="address" name="address" aria-describedby="address" placeholder="Business Address" value="{{ $users->business_address }}">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="phone">Phone Number</label>
                                    <input type="text" class="form-control inputsss" id="phone" name="phone" aria-describedby="phone" placeholder="Phone Number" value="{{ $users->phone }}">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="location">Chose Case Type </label>
									<select class="form-control category_select pro_category_select @error('casetype') is-invalid @enderror" id='casetype' name="casetype" autofocus>
                                        <option selected="@if($patient->casetype == '1') {{"selected"}} @endif" value="1" selected="">MVA</option>
                                        <option selected="@if($patient->casetype == '2') {{"selected"}} @endif" value="2">WSIB</option>
                                        <option selected="@if($patient->casetype == '3') {{"selected"}} @endif" value="3">Slip &amp; Fall</option>
									</select>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="dob">Date Of Birth</label>
                                    <input type="text" class="form-control category_select date_select2 addP_inc" name="dob" id="dob" aria-describedby="lname" placeholder="Date of Birth" value="{{$patient->dateofbirth}}">
                                </div>
                            </div>

                           <div class="col-md-6">
                                <div class="form-group">
                                    <label for="doa">Date Of Accident</label>
                                    <input type="text" class="form-control category_select date_select2 addP_inc" name="doa" id="doa" aria-describedby="lname" placeholder="Date of Accident" value="{{$patient->dateofaccident}}">
                                </div>
                            </div>

                            


                            <div style="margin-bottom: 15px;" class="col-md-12">
                                <div id="imgSectionCrp" @if (empty($users->profilepic) || $users->profilepic == 'null') style="display:none" @endif>
                                    <img class="profileiamgecls" src="{{url('uploads/profilepics/patient/'.$users->profilepic)}}" alt="">
                                    <span class="material-icons deletecropimga" style="color: #FFB81C; font-size:24px; margin-left:10px">delete</span>
                                    <img id='deleteCropImageLoader' width="30" style="display: none;" src="{{asset('img/loading.gif')}}" alt="">
                                </div>

                                <button @if (!empty($users->profilepic) && $users->profilepic != 'null') style="display:none" @endif class="uploadbtn"  onclick="chooseImageModal()" type="button">Upload profile image</button>
                            </div>

                        </div>

        
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary update_btn" id="update_btn">Update Profile</button> 
                        </div>


                    </form>
                </div>
            </div>

        </div>
		
		<!---delete profile pic---->
		<div class="modal fade" id="deleteprofilepicdiv" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
				
			<div class="modal-dialog modal-dialog-centered" role="document">

				<div class="modal-content login_box">

				  <div class="modal-header" >

					<h5 class="modal-title" id="loginTitle" >Are Sure Want to Delete Profile Pic?</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

					  <span aria-hidden="true">&times;</span>

					</button>

				  </div>

				  <div class="modal-body">

						  <form id="deleteprofilepic_form"><!-------method="POST" action="{{ route('login') }}--------->
								@csrf

								<div class="row">

									<div class="col-md-12">

										<div class="form-group">
											<input type="hidden" id="userid" name="userid" value="{{ $users->id }}">
										</div>

									</div>
									<div class="col-md-12">
									   <p id="successtask1" style="color:green;display:none;">Profile Picture Deleted Successfully</p>
									</div>
								</div>

								<button type="submit" class="btn btn-primary submit_btn"  id="deleteprofilepic_btn">Yes</button>
								<button type="submit" class="btn btn-primary submit_btn"  id="deleteprofilepic_btn1" data-dismiss="modal" aria-label="Close">No</button>

							</form>

				  </div>

				</div>

			  </div>

		</div>

    </section>
	
    @include('includes.imageCroping');
	
@endsection

