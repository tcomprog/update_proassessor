@extends("includes.email_container")
@section("content")
            <div>
                <h3 style="font-weight: bold; font-size: 22px;line-height: 19px;color:#20215E;">New contact Request Added</h3>
                <h3 style="font-weight: bold; font-size: 17px;line-height: 19px;color:#20215E;">Below are contact form details</h3>

             <table style="margin-top: 20px">
                <tr style="line-height: 25px;">
                    <th width="100" style="text-align: left">First name</th>
                    <td>{{$fname}}</td>
                </tr>
                
                <tr style="line-height: 25px;">
                    <th width="100" style="text-align: left">Last name</th>
                    <td>{{$lname}}</td>
                </tr>

                <tr style="line-height: 25px;">
                    <th width="100" style="text-align: left">Email</th>
                    <td>{{$email}}</td>
                </tr>

                <tr style="line-height: 25px;">
                    <th width="100" style="text-align: left">Phone</th>
                    <td>{{$phone}}</td>
                </tr>

                <tr style="line-height: 25px;">
                    <th width="100" style="text-align: left">Subject</th>
                    <td>{{$subject}}</td>
                </tr>

                <tr style="line-height: 25px;">
                    <th width="100" style="text-align: left">Message</th>
                    <td>{{$message}}</td>
                </tr>

             </table>
                
                   
            </div>  
            </div>
            </div>
  @endsection