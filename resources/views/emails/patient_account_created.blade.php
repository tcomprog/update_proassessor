@extends("includes.email_container")
@section("content")
<div>

        <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Dear {{ucfirst($patient_name)}},</h3>
          <p style="font-size: 14px;line-height: 17px;">Your account has been created by <b>{{ucfirst($lawyer_name)}}</b>. Please visit the link to login into your account <a href="{{ url('/') }}" target="_blank">website</a></p> 
          <p style="font-size: 14px;line-height: 17px;"><b>Username</b>: {{$username}}</p> 
          <p style="font-size: 14px;line-height: 17px;"><b>Password</b>: {{$password}}</p> 
        <br/>    

        <br/>

        <br/>

        <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
        <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 

</div>  

@endsection