<?php 
namespace App\Helpers;
use App\Models\Category;
use App\Models\Message;
use App\Models\Nofication;
use Config;
use DB;
use Auth;
use App\Models\DoctorRating;

class Helper
{
	public static function categorydetails(){
	$categorydetails = Category::all();
	return $categorydetails;
	}

	public static function notification_messages($user_id)
	{
		$notification = Nofication::where('to_id',$user_id)->where('status','0')->get()->count();
		return $notification;
	} 

	public static function un_read_messages($user_id,$invitation_id)
	{
		$count = Message::where('receiver_user_id',$user_id)->where('is_read','0')->where('invitation_id',$invitation_id)->get()->count();
		return $count;
	} 

	public static function user_unread_messages($user_id)
	{
		$count = Message::where('receiver_user_id',$user_id)->where('is_read','0')->get()->count();
		return $count;
	} 

	public static function clear_un_read_messages($user_id,$invitation_id)
	{
		Message::where('receiver_user_id',$user_id)->where('is_read','0')->where('invitation_id',$invitation_id)->update(['is_read'=>'1']);
		return true;
	}
	/**
	 * encrypt patient fields
	 * $text - Plain text
	 * return base64_encode enctrypted string
	 */

	//******************* */ Custom encryption and decrtpyion *******************
	public static function Encrypt($simple_string,$key = null)
    {
        $ciphering = "AES-128-CTR"; 
        $iv_length = openssl_cipher_iv_length($ciphering); 
        $options = 0; 
        $encryption_iv = '1234567891011121'; 
        $encryption_key = $key == null ? env("ENCRYPTION_KEY") : $key; 
        $encryption = openssl_encrypt($simple_string, $ciphering, 
        $encryption_key, $options, $encryption_iv); 
        return $encryption;
    }

    public static function Decrypt($encryption,$key = null)
    {
        $decryption_iv = '1234567891011121'; 
        $decryption_key = $key == null ? env("ENCRYPTION_KEY") : $key;  
        $ciphering = "AES-128-CTR"; 
        $options = 0; 
        $decryption=openssl_decrypt ($encryption, $ciphering,  
        $decryption_key, $options, $decryption_iv); 
        return $decryption; 
    }
	//******************* */ Custom encryption and decrtpyion *******************


	public static function c_strings(){
		return [
		  "invit_to_doctor" => "You have received an invite from @@",
		  "invit_to_doctor_clinic" => "You have received an invite from @@ for ##",
		  "Report_due_date" => "Today is the last day for uploading the report for @patient",
		  "docotor_inv" => "Your invite to @doctor@ for @patient@ has been ##.",
		  "patient_accepted" => "@patient has accepted the appointment on @date",
		  "patient_rejected" => "@patient has rejected the appointment on @date",
		  "cancelled_app" => "@patient has cancelled the appointment on @date",
		  "medical_report_reminder" => "Please upload medical documents for the patient @patient has the appointment on @date",
		  "report_alert" => "@doctor has uploaded repots for @patient",
		];
	}

	public static function desginationList(){
		return DB::table("designations")->get();
	}

	public static function clearnName($string,$ext){
		$array = [".",":","-","(",")","{","}","[","]","#","`","@","#","<",">","?",",","&","$","!","'",'"'];
		$string = str_replace($array,'',$string);
		$string = str_replace($ext,'',$string);
		return $string.".".$ext;
	}	

	public static function randomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[random_int(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}
?>