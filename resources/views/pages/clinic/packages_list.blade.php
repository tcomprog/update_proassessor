@extends('layouts.admin_app')


@section('content')

<div class="container-fluid bg-white rounded py-4 adminPageContainer">

    <div class="row">

        <h4 class="inv-content-header" style="margin-bottom:0px;">Packages List</h4>
		

        <table id="datatbl11" class="row-border hover cell-border" style="width:100%">
            <thead>
                <tr>
					<th>Name</th>
					<th>Amount</th>
					<th>Updated on</th>
					<th>Features</th>
					<th>Action</th>
                </tr>
            </thead>

             <tbody>
               @foreach ($packages as $row)
                  <tr>
                    <td>{{ucfirst($row->name)}}</td>
                    <td>{{$row->amount}}</td>
                    <td>{{date("d-m-Y",strtotime($row->updated_at))}}</td>
                    <td>
                        @php
                            $list = json_decode($row->features);
                        @endphp
                        @foreach ($list as $fr)
                           <span class="rounded px-1 mt-1" style="background:#ffb6c1;font-size:13px;">{{$fr}}</span> <br>
                        @endforeach
                    </td>
                    <td class="text-center">
                        <a href="{{url('/clinic/edit_package/')."/".$row->id}}" class="btn btn-outline-danger btn-sm">Edit</a>
                    </td>
                  </tr> 
               @endforeach
            </tbody>

        </table>

    </div>
</div>


@endsection

