<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
    <header>
        <div class="container">
            <div class="row">
                @include('includes.header')
            </div>
        </div>
    </header>
        <!-- Banner Section -->
        <section class="intro_banner_sec inner_banner_sec">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="welcome_text">
                            <h1>Benefits</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Banner Section -->
    @yield('content')
    <footer>

        @include('includes.footer')
    </footer>
    @include('includes.model')
    @include('includes.scripts')
</body>
</html>