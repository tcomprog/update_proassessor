<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sitedetails extends Model
{
    use HasFactory;

    protected $table = 'site_details';

    protected $fillable = [
        'site_title',
        'phone',
        'address',
        'site_email',
        'facebook_link',
        'instagram_link',
        'twitter_link',
    ];
}
