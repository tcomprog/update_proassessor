<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Config;

class ExpirarySubscriptionWarning extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $option;
    public function __construct($option)
    {
        $this->option = $option;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $fromAddress = Config::get('constants.MAIL_FROM_ADDRESS');
        $site_title_old = Config::get('constants.site_title');
        $site_title=str_replace("-"," ",$site_title_old);
    
        return $this->subject($site_title.' - '.$this->option['subject'])
               ->from($fromAddress,$site_title)
               ->view('emails.exp_subscriptionn_war',[
                'name'=>$this->option['name'],
                'package'=>$this->option['package'],
                'from_date'=>$this->option['from_date'],
                'to_date'=>$this->option['to_date'],
                "site_title"=>$site_title
            ]);
    }
}
