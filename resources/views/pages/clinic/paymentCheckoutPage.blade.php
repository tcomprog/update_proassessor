@extends('layouts.default')
@section('content')

<link href="{{ asset('css/about.css') }}?99999999" rel="stylesheet">
<link href="{{ asset('css/subscription.css') }}?99999999" rel="stylesheet">
<link href="{{ asset('css/home.css') }}?99999999" rel="stylesheet">

<link href="{{ asset('css/checkout.css') }}?99999999" rel="stylesheet">

<script src="https://js.stripe.com/v3/"></script>
<script src="{{asset('js/checkout.js')}}"></script>

<style>
.topHomeContainerAbout{
    display: flex;
    flex-direction: column;
    background: url("{{asset('assets/about_banner.jpeg')}}");
}

.sub_card > .header{
    background: url("{{asset('n_img/offer_top.png')}}");
    height: 150px;
    width:100%;
    display: flex;
    justify-content: center;
    align-items: center;
    background-repeat: no-repeat;
    background-size: contain;
    background-position: center;
}

.mainBoxContainer{
    flex-direction: column;
    gap:0px;
}

.hhhhhh > h4, h3{
    font-family: DubaiBold !important;
}
.hhhhhh > h4{
    color: #707070;
}

</style>

<section class="topHomeContainerAbout">
<div class="subcontainer">
    @include('includes.nheader')

    <div class="top-banner container">
       <div class="br-slide1">
          <h2 style="font-family: 'DubaiBold' !important; margin-top:30px">Checkout</h2>
       </div>
    </div>
</div>
</section>


<section style="width:100%;margin:20px 0px 60px 0px;" >
<div class="container py-5">
    <div class="row">
   
        <div class="col-md-6 col-sm-12 bg-light rounded py-5 h-auto mb-3">
            <div class="d-flex align-items-center justify-content-between px-3 pb-1 border-bottom hhhhhh"> 
                <h3>Package</h3>
                <h4>{{ucfirst($package)}}</h4>
            </div>
            <div class="d-flex align-items-center justify-content-between px-3 pt-3 pb-1 border-bottom hhhhhh"> 
                <h3>Amount</h3>
                <h4>{{$amount}}</h4>
            </div>
        </div>

        <div class="col-md-6 col-sm-12">
            <form id="payment-form">
                <div id="link-authentication-element">
                  <!--Stripe.js injects the Link Authentication Element-->
                </div>
                <div id="payment-element">
                  <!--Stripe.js injects the Payment Element-->
                </div>
                <button id="submit">
                  <div class="spinner hidden" id="spinner"></div>
                  <span id="button-text">Pay now</span>
                </button>
                <div id="payment-message" class="hidden"></div>
            </form>
          
        </div>
        
    </div>
</div>
</section>

@endsection

