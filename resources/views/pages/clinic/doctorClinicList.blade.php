@extends('layouts.clinic-layout') 
@section('content')	
<link href="{{ asset('css/clinic_dashboard.css') }}" rel="stylesheet">


<div class="container dashboardContainer">
    <h3 class="inv-content-header">Doctor List</h3>

    <div class="row">

      <div class="col-12">
          <table id="tableSorter" class="row-border hover cell-border" style="width:100%">
            <thead>
                <tr>
                <th>Name</th>
                <th>Phone</th>
                <th>License Number</th>
                <th>Status</th>
                <th>Admin status</th>
                <th></th>
                </tr>
            </thead>

            <tbody>
                @if (count($doctor) > 0)
                  @foreach ($doctor as $row)
                      <tr>
                        <th>{{$row->name}}</th>
                       
                        <td>{{$row->phone}}</td>
                        <td>{{$row->doctor->licence_number}}</td>
                        <td>
                            @if ($row->status == 'active')
                               <span class="bg-success px-2 rounded">Active</span> 
                               @else
                               <span class="bg-warning px-2 rounded">Pending</span> 
                            @endif
                        </td>
                        <td>
                            @if ($row->authentication_status == 'active')
                            <span class="bg-success px-2 rounded">Active</span> 
                            @else
                            <span class="bg-warning px-2 rounded">Pending</span> 
                         @endif
                        </td>
                        <td>
                            <a href ="{{url('/cldoctorprofile/'.$row->id)}}" class="btn btn-info btn-sm" title="View profile">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </a>

                            <a href ="{{url('/viewClinicCalender/'.$row->id)}}" class="btn btn-warning btn-sm" title="View calendar">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </a>

                            @if ($row->status != 'active')
                                <a href ="{{url('/approveDoctor/'.$row->id)}}" class="btn btn-success btn-sm" title="Approve on behalf of doctor">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </a> 
                            @endif

                        </td>
                      </tr>
                  @endforeach                    
                @endif
            </tbody>

          </table>
      </div>

    </div>
</div>

@endsection