@extends('layouts.admin_app')


@section('content')

<div class="container-fluid bg-white rounded py-4 adminPageContainer">

    <div class="row">

        <h4 class="inv-content-header" style="margin-bottom:0px;">Doctor Price Change Requests</h4>		

        <table id="datatbl11" class="row-border hover cell-border" style="width:100%">
            <thead>
                <tr>
					<th width='10%'>Name</th>
					<th width='10%'>Email</th>
					<th width='10%'>Phone</th>
					<th width='10%'>Old Price</th>
					<th width='10%'>New Price</th>
					<th width='10%'>Date</th>
					<th width='30%'>Description</th>
					<th >Action</th>
                </tr>
            </thead>

             <tbody>
                    @foreach ($changeRequest as $row)
                        <tr id="prRow_{{$row->id}}">
                            <th>{{$row->name}}</th>
                            <td>{{$row->email}}</td>
                            <td>{{$row->phone}}</td>
                            <td>${{$row->oldPrice}}</td>
                            <th>${{$row->newPrice}}</th>
                            <td>{{date('d-m-Y',strtotime($row->created_at))}}</td>
                            <td>{{$row->des}}</td>
                            <td>
                                <button id="priceRequestSuccess_{{$row->id}}" data-status="1" data-id="{{$row->id}}"  class="btn btn-success btn-sm changePriceRequesStatus" title="Approve">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </button>

                                <button id="priceRequestReject_{{$row->id}}" data-status="2" data-id="{{$row->id}}"  class="btn btn-danger btn-sm changePriceRequesStatus" style="margin-top: 5px;" title="Approve">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>

                                <img id="pr_loader_{{$row->id}}" width="30" style="display: none;" src="{{asset("img/loading.gif")}}" alt="">
                            </td>
                        </tr>    
                    @endforeach       
            </tbody>

        </table>

    </div>
</div>


@endsection

