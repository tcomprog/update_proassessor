<script>
	let autocomplete;
	let autocomplete2;
	let autocomplete3;
	function initMap(){

	  autocomplete = new google.maps.places.Autocomplete(
	   document.getElementById("address"),
	   {
		 componentRestrictions: { country: ['ca']},
		 fields:["name"],
		 types: ["establishment"],
	   }
	  );

	//   autocomplete2 = new google.maps.places.Autocomplete(
	//    document.getElementById("business_address"),
	//    {
	// 	 componentRestrictions: { country: ["us",'uk','gr']},
	// 	 fields:["name"],
	// 	 types: ["establishment"],
	//    }
	//   );

	  autocomplete3 = new google.maps.places.Autocomplete(
	   document.getElementById("business_address1"),
	   {
		 componentRestrictions: { country: ['ca']},
		 fields:["name"],
		 types: ["establishment"],
	   }
	  );

	}

 </script>


@if(@$pageType != 'home')
   <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
@endif

 <script async
	 src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&libraries=places&callback=initMap">
 </script>
 <script src="{{asset('js/custom.js')}}"></script>
 <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.js"></script>
 <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.13.2/jquery-ui.min.js"></script>


 <script>

  $("#clear_all_notification").on('click',function(){
        $("#loader_ere").show();
        $(this).attr("disabled", true)
        $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
		});

        $.ajax({
				type:'POST',
				url:"<?= url('/clear_notif') ?>",                
				dataType:'json',
			   	success:function(response) {
                    $("#loader_ere").hide();
                    if(response.status == '1'){
                       $("#table_id > tbody").html("");
                       $(this).attr("disabled", false)
                     //  $("#nothing_to_show").show();
                       $("#clear_all_notification").hide();
                       $("#not_icons").hide();
                    }
				}
			});	
})

var route = "<?php echo Route::currentRouteName(); ?>";
if(route == 'notification'){
    $(document).ready(function () {
        $.noConflict();
        $('#table_id').DataTable({
            "paging": true,
             "ordering":false
        });
    });
}

$(document).ready(function() {  
   
   var check_cookie = getCookie('cookie_status');
   if(isEmpty(check_cookie)){
     openNav();
   }

});

function openNav() {
    // padding: 20px 30px;
  var x = window.matchMedia("(max-width: 500px)");
  var height = x.matches ? 220 : 120;
  $("#mySidenav").css({'width':'100%','height':height,'padding':'12px 30px'});
  $("#cookiecontent").show();
  $("#fullScreencontrol").addClass("fullScreencontrol");
  $("#fullScreencontrol").addClass("fullScreencontrol");
}

function closeCookie() {
   $("#cookiecontent").hide();
   $("#mySidenav").css({'width':0,'height':0,'padding':0});
   $("#fullScreencontrol").removeClass("fullScreencontrol");
}


function acceptCookie(){
    setCookie('cookie_status','true',30);
    closeCookie();
}


function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  let expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}


function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
</script>
@include('common.uploadDoctorQuestionair');


<script>
  $('#upload_questionnair').on('shown.bs.modal', function (event) {
     console.log('ddfdfdf')
  })

  $(".opensss").click(function(){
    $("#sideMenuContainer").css({'width':0,'height':0,'padding':0});
      $("#fullScreenSideMenu").removeClass("fullScreenSideMenu"); 

      $('html, body').css({
        overflow: 'auto',
        height: 'auto'
      });
  });


  $(document).ready(function(){
    var down = false;
    $('#bell').click(function(e){
        var color = $(this).text();
          if(down){    
            $('#box').css('display','none');
            $('#box').css('opacity','0');
            $('#box').css('opacity','0');
            down = false;
          }else{     
            $('#box').css('display','block');
            $('#box').css('height','auto');
            $('#box').css('opacity','1');
            down = true;
                    
          }
                
    });

    $("#closenotification").click(function(){
      if(down){    
            $('#box').css('height','0px');
            $('#box').css('opacity','0');
            down = false;
      }
    });
                
});


$(document).ready(function () {

$(".notifications-item").on('click', function () {
var id = $(this).data("id");
var status = $(this).data("status");
var link = $(this).data("link");
console.log(link);
if(status == '0'){
   $("#not_loading_"+id).show()
     $.ajaxSetup({
       headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
       });

      $.ajax({
         url: '{{url("unreadNotification")}}',
         type: "POST",
         data:{id:id},
         success: function (data) {
           if(data.status == '1'){
               let badge = $(".changebadgeNotification").html();
               $("#notification_row_"+id).css('background','white');
               badge = parseInt(badge);
               $("#not_loading_"+id).hide()
               if(badge == undefined || badge == 1){
                 $(".changebadgeNotification").hide();
               }
               else{
                 $(".changebadgeNotification").html(badge-1);
               }
               window.location.replace(link) 
           }
         }
   });
}
else{
 window.location.replace(link)
}

});
});

</script>



<script src="{{asset('js/croppie.js')}}"></script>
<script src="{{asset('js/ndesign.js')."?98988"}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/9.0.5/swiper-bundle.min.js" integrity="sha512-cEcJcdNCHLm3YSMAwsI/NeHFqfgNQvO0C27zkPuYZbYjhKlS9+kqO5hZ9YltQ4GaTDpePDQ2SrEk8gHUVaqxig==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<script>

function base64ToBlob(base64, mime) 
{
    mime = mime || '';
    var sliceSize = 1024;
    var byteChars = window.atob(base64);
    var byteArrays = [];

    for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
        var slice = byteChars.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, {type: mime});
}

$image_crop = $('#upload-image').croppie({
	enableExif: true,
	viewport: {
		width: 280,
		height: 280,
		type: 'square'
	},
	boundary: {
		width: '100%',
		height: 320
	}
});

$('#images').on('change', function () { 
	var reader = new FileReader();
  var type = this.files[0]?.type;
  console.log(type)
  if(type == "image/png" || type == "image/jpg" || type == "image/jpeg"){
      $('#cropImagAlert').hide();
        reader.onload = function (e) {
        $image_crop.croppie('bind', {
          url: e.target.result
        }).then(function(){
          $("#uploadcrpImg").show();
          console.log('File loaded');
        });			
      }
      reader.readAsDataURL(this.files[0]);
  }
  else{
    $('#cropImagAlert').show();
    setTimeout(() => {
      $('#cropImagAlert').hide();
    }, 5000);
  }

});

$.ajaxSetup({
    headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#uploadcrpImg').on('click', function (ev) {
  $("#uploadcrpImg").hide();
  $("#images").hide();
  $("#profileupload_prog").show();
	$image_crop.croppie('result', {
		type: 'canvas',
		size: 'viewport'
	}).then(function (response) {

    var base64ImageContent = response.replace(/^data:image\/(png|jpg);base64,/, "");
    var blob = base64ToBlob(base64ImageContent, 'image/png');                
    var formData = new FormData();
    formData.append('picture', blob);

		$.ajax({
			url: "{{url('uploadProfileImage')}}",
			type: "POST",
			data: formData,
      cache: false,
      contentType: false,
      processData: false,
			success: function (data) {
         if(data.status == '1'){
          $("#imageupload_modal").modal('hide');
          $("#imgSectionCrp").show();
          $(".profileiamgecls").show();
          $(".profileiamgecls").attr('src',data.lnk);
          $(".headerProfImag").attr('src',data.lnk);
          $(".deletecropimga").show();
          $("#uploadcrpImg").hide();
          $(".uploadbtn").hide();
         }
			}
		});
	});
});


$(document).on('click','.deletecropimga',function(){
  $.confirm({
    title: 'Are you sure?',
    content: 'Do you want to delete profile image?',
    buttons: {
      YES: function () {
        $(".profileiamgecls").hide();
        $(".deletecropimga").hide();
        $("#deleteCropImageLoader").show();

         $.ajax({
        		url: "{{url('deleteProfileImage')}}",
        		type: "GET",
        		success: function (data) {
              if(data.status == '1'){
                 $("#deleteCropImageLoader").hide();
                 $(".uploadbtn").show();
                 $(".headerProfImag").attr('src','{{url('img/user.jpg')}}');
              }
        		}
        	});
      },
      NO: function () {
              
      }
    }
});
});

</script>



<script>
  $(".mytbslnk")?.on('click',function(){
     var tabID = $(this).data("id");
     $("#trackingSelectedTab").val(tabID);
  });


  function showAlertAfterDelete(){
		$("#deleteanimModal").modal("show");
		setTimeout(function(){
			new Audio("{{asset("delete.mp3")}}").play();
		},300);

		setTimeout(() => {
			$("#deleteanimModal").modal("hide");
		}, 2500);
	}
	


  function deleteAllFiles(assessID,type){
  // type =>  1 - Medical, 2 - Summary, 3 - Letter en, 4 - form, 5 - Questionari
 
  $.confirm({
    title: 'Are you sure?',
    content: 'Do you want to delete all reports?',
    buttons: {
      YES: function () {
        $("#deleteLoad_"+type).show();
        $("#deleteBtn_"+type).attr("disabled", true);

          $.ajaxSetup({
          headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
          });

         $.ajax({
        		url: '{{url("deleteAllReports")}}',
        		type: "POST",
            data:{assessID:assessID,type:type},
        		success: function (data) {
              if(data.status == '1'){
                $("#deleteLoad_"+type).hide();
                $("#deleteBtn_"+type).remove();
                $(".reportList_"+type).remove();
                showAlertAfterDelete();
              }
        		}
        	});
      },
      NO: function () {
              
      }
    }
});
}


  function deleteAllFilesDoctor(assessID,type){
  // type =>  1 - Medical, 2 - Summary, 3 - Letter en, 4 - form, 5 - Questionari
 
  $.confirm({
    title: 'Are you sure?',
    content: 'Do you want to delete all reports?',
    buttons: {
      YES: function () {
        $("#deleteLoad_"+type).show();
        $("#deleteBtn_"+type).attr("disabled", true);

          $.ajaxSetup({
          headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
          });

         $.ajax({
        		url: '{{url("deleteMedicalReport")}}',
        		type: "POST",
            data:{assessID:assessID,type:type},
        		success: function (data) {
              if(data.status == '1'){
                $("#deleteLoad_"+type).hide();
                $("#deleteBtn_"+type).remove();
                $(".reportList_"+type).remove();
                showAlertAfterDelete();
              }
        		}
        	});
      },
      NO: function () {
              
      }
    }
});
}



</script>


@isset($_GET["login_pop"])
<script>
  $("#login").modal('show');
</script> 
@endisset




@if(request()->is('sub_checkout_page'))
<script>
     const stripe = Stripe("{{env('PUBLISHABLE_KEY')}}");
     const items = [{ id: "xl-tshirt" }];
     let elements;
     initialize();
     let emailAddress = '';

      async function handleSubmit(e) {
        e.preventDefault();
        setLoading(true);

        const { error } = await stripe.confirmPayment({
          elements,
          confirmParams: {
            // Make sure to change this to your payment completion page
            return_url: "{{env('STRIPE_RETURN_URL')}}",
            receipt_email: emailAddress,
          },
        });

        if (error.type === "card_error" || error.type === "validation_error") {
          showMessage(error.message);
        } else {
          showMessage("An unexpected error occurred.");
        }

        setLoading(false);
      }

      $(function name(params) {
        $("#payment-form").on('submit',handleSubmit)
      });

     async function initialize() {
         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
             }
         });
          
         $.ajax({
             type: "POST",
             url: "{{url('/init_ui')}}",
             data: {
                 'items': [{ id: "xl-tshirt" }],
                 'amount':'{{$amount}}',
                 'package':'{{$package}}'
             },
             dataType: 'json',
             success: function (data) {
                // console.log(data.clientSecret)
                 elements = stripe.elements({'clientSecret':data.clientSecret});
                 const linkAuthenticationElement = elements.create("linkAuthentication");
                 linkAuthenticationElement.mount("#link-authentication-element");
 
                 const paymentElementOptions = {
                     layout: "tabs",
                 };

                 const paymentElement = elements.create("payment", paymentElementOptions);
                 paymentElement.mount("#payment-element");

             },
             error: function (data) {
                 console.log(data);
             }
         });
     }
 </script>
@endif

<script>
  // ************************ SIGN UP SECTION START ******************************
{

function openTesting(type = null){

    $("#doctorTab").addClass("activeTab");
    $("#doctorTab").removeClass("inactiveTab");
    $("#lawyerTab").addClass("inactiveTab");
    $("#lawyerTab").removeClass("activeTab");

    $("#clinicTab").addClass("inactiveTab");
    $("#clinicTab").removeClass("activeTab");

    $("#designation").val("1")
    $("#descField").attr("placeholder","License Number");

   $("#name1").val('');
   $("#email1").val('');
   $("#business_address1").val('');
   $("#password1").val('');
   $("#confirmpassword1").val('');
   $("#phone1").val('');
   $("#descField").val('');
   $("#agrrur").val('');

   $("#sideMenuContainer").css({'width':0,'height':0,'padding':0});
      $("#fullScreenSideMenu").removeClass("fullScreenSideMenu"); 

      $('html, body').css({
        overflow: 'auto',
        height: 'auto'
      });
    
   if(type == null){
     $('#newsignup').modal()
     $("#errorMsgg").hide();
   }

   $("#designationSection").show();
   $("#agrrur_error").hide();
   $("#descField_error").hide();
   $("#phone_error").hide();
   $("#repassword_error").hide();
   $("#password_error").hide();
   $("#address_error").hide();
   $("#email_error").hide();
   $("#name_error").hide();
   $("#designation_error").hide();
}

function submitSignUpForm(){
   let name = $("#name1");
   let email = $("#email1");
   let business_address = $("#business_address1");
   let password = $("#password1");
   let confirmpassword = $("#confirmpassword1");
   let phone = $("#phone1");
   let descField = $("#descField");
   let agrrur = $("#agrrur");
   let designation = $("#designation");

   let doctorDesign = $('#desginationDrop :selected').val(); 

   let v_name = name.val();
   let v_email = email.val();
   let v_business_address = business_address.val();
   let v_password = password.val();
   let v_confirmpassword = confirmpassword.val();
   let v_phone = phone.val();
   let v_descField = descField.val();
   let v_designation = designation.val();
   let v_agrrur = (agrrur.is(':checked') == true) ? 1 : 0;

   let error = 0;

   if(isEmpty(v_password)){
      $("#password_error").show();
      $("#password_error").text("Password is required");
      error = 1;
      setTimeout(() => {
        $("#password_error").hide();
      }, 4000);
    }
    else{
       var check = checkPasswordValidity(v_password);
       if(check == null){
          $("#password_error").hide();
       }
       else{
        $("#password_error").show();
        $("#password_error").text(check);
        error = 1;
       }
    }


  if(isEmpty(v_email)){
    $("#email_error").show();
    $("#email_error").text("Email is required");
    setTimeout(() => {
      $("#email_error").hide();
     }, 5000);
    error = 1;
  }
  else{
      if(validateEmail(v_email) == false){
        $("#email_error").show();
        $("#email_error").text("Invalid email address");
        error = 1;
      }	
      else{
        $("#email_error").hide();
      }
  }

    if(agrrur.is(':checked') == false){
    $("#agrrur_error").show();
    $("#agrrur_error").text("Please accept the term and condition before submitting the form");
     error = 1;
     setTimeout(() => {
      $("#agrrur_error").hide();
     }, 4000);
    }
    else{
        $("#agrrur_error").hide();
    }
   

   if(isEmpty(v_descField) && v_designation != 4){
    $("#descField_error").show();
    $("#descField_error").text("This field is required");
     error = 1;
     setTimeout(() => {
      $("#descField_error").hide();
     }, 4000);
    }
    else{
        $("#descField_error").hide();
    }


   if(isEmpty(v_confirmpassword)){
      $("#repassword_error").show();
      $("#repassword_error").text("Re-password is required");
      error = 1;
      setTimeout(() => {
        $("#repassword_error").hide();
       }, 4000);
    }
    else{
      if(v_password != v_confirmpassword){
        $("#repassword_error").show();
        $("#repassword_error").text("Passwords don`t match");
        error = 1;
      }
      else{
          $("#repassword_error").hide();
      }
    }

    if(isEmpty(v_business_address)){
        $("#address_error").show();
        $("#address_error").text("Business address is required");
        error = 1;
        setTimeout(() => {
          $("#address_error").hide();
         }, 4000);
    }
    else{
        $("#address_error").hide();
    }

    if(isEmpty(v_name)){
        $("#name_error").show();
        $("#name_error").text("Name is required");
        error = 1;
        setTimeout(() => {
          $("#name_error").hide();
         }, 4000);
    }
    else{
        $("#name_error").hide();
    }


    if(isEmpty(v_phone)){
      $("#phone_error").show();
      $("#phone_error").text("Phone number is required");
      error = 1;
      setTimeout(() => {
        $("#phone_error").hide();
      }, 4000);
    }
    else{
      if(digits_only(v_phone) == false){
        $("#phone_error").show();
        $("#phone_error").text("Invalid phone number");
        error = 1;
      }else{
        $("#phone_error").hide();
      }
    }

    if(doctorDesign < 0 && v_designation == 1){
      $("#designation_error").show();
      $("#designation_error").text("Please select designation");
      error = 1;
      setTimeout(() => {
        $("#designation_error").hide();
      }, 4000);
    }


    if(error == 1){
        return;
    }else{
        $("#loadingIMG").show();
        $(".signupBtn").attr("disabled", true);
        $(".signupBtn").text("Loading...");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url : "{{url('signupcreatenew')}}",
            data : {
                name:v_name,
                email:v_email,
                address:v_business_address,
                password:v_password,
                re_password:v_confirmpassword,
                phone:v_phone,
                descField:v_descField,
                designation:v_designation,
                term_condition:v_agrrur,
                desginationDoctor:$('#desginationDrop :selected').text(),
                desginationDoctorID:doctorDesign
            },
            type : 'POST',
            dataType : 'json',
            success : function(response){
                $("#loadingIMG").hide();
                $(".signupBtn").attr("disabled", false);
                $(".signupBtn").text("Sign Up");

                if(response.status == 'error'){                            
                    if(response.message.email){
                        if (typeof response.message.email[0] === "string"){                               
                            var emailexists1 = "Email Already Exists";
                            $('#email_error').show();
                            $('#email_error').text(emailexists1);
                         }                          
                    }  
                    if(response.message.phone){
                        if (typeof response.message.phone[0] === "string"){                               
                            var doctorphoneerror1 = "Please Enter Phone Number";
                            $('#phone_error').show();
                            $('#phone_error').text(doctorphoneerror1);
                         }                          
                    } 
               }

               if(response.status == 'success'){
               // $('#newsignup').modal('hide');
                $('body,html').animate({scrollTop:0},800);
                openTesting(11);
                $("#successMSG").show();
                setTimeout(()=>{
                    $("#successMSG").hide();
                },5000)
              } 

            }
      });
    }
}

}

// ************************ SIGN UP SECTION END ******************************


</script>
