<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {        
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $remember = $request->has('remember') ? true : false;
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password'), 'status'=>'active', 'role_id'=>3], $remember)) {			
           // if ($request->input('remember') == 'yes') {	
		   if ($request->input('remember')!==null) {	
				//echo "hi";exit;
                setcookie('email', $request->input('email'), time() + (86400 * 3000), "/");
                setcookie('password', $request->input('password'), time() + (86400 * 3000), "/");
                setcookie('rember', $request->input('remember'), time() + (86400 * 3000), "/");
            } else 
                {
				//	echo "no";exit;
                unset($_COOKIE['rember']);
                setcookie('rember', null, -1, '/');
                setcookie('email', null, -1, '/');
                setcookie('password', null, -1, '/');
            }
          return redirect('dashboard');
        }   
        else {
			//echo "not working";exit;
            Session::flash('loginerror', 'These credentials do not match our records.');
            return redirect('signin');
            return back()->with('message', 'error');
        }
    }
}