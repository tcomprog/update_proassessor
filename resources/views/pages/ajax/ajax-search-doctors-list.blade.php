<div class="accordion" id="accordionExample">
    <div class="row">
        @if (!$users->isEmpty())
        @foreach($users as $user)
        <div class="col-md-12">
            <div class="card patient_tab box_div new_padding">
                <div class="card-head" id="heading{{ $user->id }}">
                    <h2 class="mb-0 docter_list" style="display: flex;justify-content: space-between;" >
                    <div class="main">
                            <div class="sub">
                                <?php           
                                if(($user->profilepic)!=null){ ?>
                                    <img src="{{ url('uploads/profilepics/doctor/'.$user->profilepic) }}" class="profile" style="border-radius: 10px;">
                                <?php
                                }else {
                                ?> <img src="{{ asset('img/no_doc_image.jpg') }}" class="profile" style="border-radius: 10px;">
                                <?php  } ?>
                            </div>
                            <div class="sub">
                            <span class="name">{{ ucfirst($user->name) }}</span>   
                            <span class="" style="margin-bottom: 6px;">
                                    <?php 
                                    $categoryids=explode(',',$user->doctor->category_id);    
                                    $categories_length = count($categoryids);                                
                                    $i=1;
                                    foreach ($categories as $category) { 
                                        if($input_category_id){
                                            if($category->id == $input_category_id){  ?>
                                               <span class="tags_span"> 
                                                <?php echo ucfirst($category->category_name); ?> </span> <?php
                                                //break;
                                            }
                                        }else{  
                                            if(in_array($category->id,$categoryids)) {
                                                 ?> <span class="tags_span"> <?php echo ucfirst($category->category_name); ?> </span> <?php
                                                echo $i < $categories_length ? ' ':''; 
                                                $i++; 
                                            }
                                        }
                                    }
                                    ?>
                            </span> 
                            
                            <span class="city">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i> <?php  $city = Helper::cities();  ?>  
                                    <?php  foreach ($city as $city1){ ?>
                                        <?php if(($city1->id)==($user->location)){echo $city1->city_name;} ?>
                                    <?php } ?>
                            </span>
                             <span class="city"> | </span>
                            <span class="city price">
                                <i class="fa fa-money" aria-hidden="true" style="margin-right: 2px;"></i>
                                {{$commission + $user->doctor->actual_price}}
                            </span>
                             <span class="city"> | </span>
                             <span class="city price">&#11088;  {{Helper::doctorRating($user->doctor->user_id,true)}} / 5</span>
                            </div>
                    </div>
                    <div class="sub">
                        <a href="{{ url('viewcalendar/'.$user->doctor->user_id.'/'.$input_category_id) }}" class="nav-link addpatient_btn addpatient_btn_new" style="position: unset;" >Invite</a>
                    </div>
                </h2>
                <a class="mb-0 collapsed read-more-block float-right" data-toggle="collapse" data-target="#collapse{{ $user->id }}" aria-expanded="false" aria-controls="collapse{{ $user->id }}">
                        <img src="{{ asset('images/up-chevron.svg') }}" class="arrow-up" width="20"  alt="up">
                        <img src="{{ asset('images/down-chevron.svg') }}" class="arrow-down" width="20" alt="down">
                </a>
                </div>
                <div id="collapse{{ $user->id }}" class="collapse" aria-labelledby="heading{{ $user->id }}" data-parent="#accordionExample">
                <div class="card-body">
                    <!-- <p><span class="img_bg"><img src="{{ asset('img/mail.svg') }}" class="mail"></span> <span>{{ $user->email }}</span></p>
                        <p><span class="img_bg"><img src="{{ asset('img/telephone.svg') }}" class="telephone"></span> <span>{{ $user->phone }}</span></p>-->
                        <p><span class="img_bg"><img src="{{ asset('img/address.svg') }}" class="address"></span> <span>{{ $user->business_address }}</span></p>
                        
                        <p style="margin: 0px 0px 5px;"><b>License Number:</b> 
                        
                        <?php if(isset($user->doctor->licence_number)){
                            $str1=strlen($user->doctor->licence_number);
                            $str2=$str1-4;											
                            ?>
                        <span><b  class="blur"><?php echo substr($user->doctor->licence_number, 0, $str2);  ?></b><?php echo substr($user->doctor->licence_number, $str2); ?></span>
                        <?php } ?>
                        </p>
                        
                        <p style="margin: 0px 0px 5px;"><b>Designation:</b> <span>{{ @$user->doctor->credentials }}</span></p>
                        <p class="last"><b>Introduction:</b> <span>{{ $user->doctor->introduction }}</span></p>
                </div>
                </div>
            </div>
        </div>
        @endforeach
        <div id="search-doctors-list-pagination">
            {{$users->links("pagination::bootstrap-4")}}
        </div>
        @else
        <div class="text-center col-md-12">
            <h4>No Records Found</h4>
            <p><button type="button" class="nav-link addpatient_btn border border-none" id="search-doctors-view-all-btn">View All</button></p>
        </div>
        @endif
    </div>
</div>