<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" ></script>

<script src="{{ asset('js/tavo-calendar.js') }}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" ></script>
<!--added-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="{{ asset('js/evo-calendar.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.multidatespicker.js') }}"></script>

<link rel="stylesheet" href="{{ asset('css/jquery-ui.multidatespicker.css') }}" />

<!-- =======================================calender===================== -->
<script>
	//For Auto refresh calendar once Invitation recieved.
	var invitatioCheckIntervalSeconds = 7;
	var activeTab
	setTimeout(checkDoctorNewInvitations,invitatioCheckIntervalSeconds*1000);
	
	
	function checkDoctorNewInvitations()
	{
		var segments = window.location.pathname.split( '/' );
		let current_page = segments[3] == 'update-calendar' ? 'update-calendar' : 'my-invitations';
		
		var doctorID = $("#selectedDoctorID").val();
		var doctorID = !isEmpty(doctorID) ? doctorID : "doctor";
		
		//alert(current_page);		
		$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
		});

		 $.ajax({
				type:'GET',
				url:"<?= url('/check-doctor-new-invitations') ?>",               
				data :{interval_duration: (invitatioCheckIntervalSeconds),doctorID:doctorID},
				dataType:'json',
			   	success:function(response) {
				   if(response.status== 'success'){					  
					   if(parseInt(response.data.count) > 0){
						   if(current_page == 'update-calendar'){
							   //window.location.href = '<? //=url('/doctor-dashboard/update-calendar?date=')?>'+response.data.invitation_date;
							   window.location.href = '<?=url('/doctor-dashboard/update-calendar')?>';
							}else{
							   window.location.href = '<?=url('/doctor-dashboard/my-invitations')?>';
						   }
					   }else{
						   setTimeout(checkDoctorNewInvitations,invitatioCheckIntervalSeconds*1000);
					   }
				   }
				}
			});	
	}



	/*check for confirmed invitations */
	//For Auto refresh calendar once confirmed Invitation recieved.
	
	/*var invitatioCheckIntervalSeconds1 = 7;
	var activeTab1
	setTimeout(checkDoctorNewInvitations1,invitatioCheckIntervalSeconds1*1000);	
	
	function checkDoctorNewInvitations1()
	{
		var segments = window.location.pathname.split( '/' );
		let current_page = segments[3] == 'update-calendar' ? 'update-calendar' : 'my-invitations';	
		$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
		});

		 $.ajax({
				type:'GET',
				url:"<? //=url('/check-doctor-confirmed-invitations') ?>",               
				data :{interval_duration: (invitatioCheckIntervalSeconds1)},
				dataType:'json',
			   	success:function(response) {
				   if(response.status== 'success'){	
					   console.log(response.data.count);				  
					   if(parseInt(response.data.count) > 0){
						   if(current_page == 'update-calendar'){
							//	window.location.href = '<? //=url('/doctor-dashboard/update-calendar')?>';
							}else{
							//	window.location.href = '<? //=url('/doctor-dashboard/my-invitations')?>'; 
						   }
					   }else{
						   setTimeout(checkDoctorNewInvitations1,invitatioCheckIntervalSeconds1*1000);
					   }
				   }
				}
			});	
	}*/  

   $("#calendar").evoCalendar({blockedDates:<?=$blockedDates?>,dueblockedDates:<?=$dueblockedDates?>,assessmentdays:<?=$assessment_days?>,duedays:<?=$due_days?>});
   $("#calendar").evoCalendar('addCalendarEvent',
   <?php echo $newList1; ?>    
   ); 
   
   //after blocked date move calendar to particular month start
  /* @if(request()->date)   
        var testdate=request()->date;  
   		$("#calendar").evoCalendar('selectDate','{{ request()->date}}' );
  @endif*/
 //end
 
</script>

<script>
$(document).ready(function() {	

	$('.icon-button').click(function(e){
		// e.preventDefault();
		// e.stopImmediatePropagation();
		// e.stopPropagation();
	});
	
	$('body').on('change','input[name=block_day]',function(){
		if($(this).val() == 'multiple'){
			$('#date-range-block').show();
		}else{
			$('#date-range-block').hide();
		}
	})


	$('.accept-invitation-btn').click(function () {
		var Button = $(this);
	$.confirm({
    title: 'Are you sure?',
    content: 'Are you sure want to Accept this Invitation?',
    buttons: {
        YES: function () {
			Button. attr("disabled", true);
		var invId = Button.attr("data-id");			

		$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
		});

		 $.ajax({
				type:'POST',
				url:"<?=url('/acceptint') ?>",               
				data :{id: invId},
				dataType:'json',
			   success:function(response) {
				   if(response.status== 'success'){					  
					  console.log(response.id);
					  Button. attr("disabled", false);
					    $('#scss_acc_pt').modal('show');
						$('#modal-inv-succes-img').css('filter','none');
					  	$('#acceptscss').css('display',"block");
					  	window.location.href="{{ url('doctor-dashboard/my-invitations#accepted-invitations') }}";
						window.location.reload();
				   }else{
					   alert('Sorry something went wrong')
				   }
			   }
			});	
        },
        NO: function () {
            
        }
    }
});

	});
	});
</script>
<script>
	
$(document).ready(function() {	
		document.getElementById('rjtscss').style.display = "none";	
		$(document).on('click', '.reject-inv-btn', function () {
			var Button = $(this);
			$.confirm({
				title: 'Are you sure?',
				content: 'Are you sure want to Reject this Invitation?',
				buttons: {
					YES: function () {
						Button.attr("disabled", true);
						var invId = Button.attr("data-id");		
					
						$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
						});

						$.ajax({
								type:'POST',
								url:"<?=url('/rejectint') ?>",               
								data :{id: invId},
								dataType:'json',
								//beforeSend: function() {$('#int_reject').html('Please Wait');},
							success:function(response) {
								if(response.status== 'success'){					  
									console.log(response.id);
									Button. attr("disabled", false);
									$('#scss_acc_pt').modal('show');
									$('#modal-inv-succes-img').css('filter','invert(175%) sepia(50%) saturate(242%) hue-rotate(80deg) brightness(82%) contrast(186%)');
										$('#rjtscss').css('display',"block");
										window.location.href="{{ url('doctor-dashboard/my-invitations#rejected-invitations') }}";
										window.location.reload();
								}else{
									alert('Sorry something went wrong')
								}
							}
							});	
						},
        			NO: function () {}
    			}
			});
		
		});
});
</script>


<script>
	$(document).ready(function() {	
		var today = new Date();
		var dd = String(today.getDate()).padStart(2, '0');
		var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = today.getFullYear();

		today = mm + '/' + dd + '/' + yyyy;
		timeslotdata(today);
		
		$('#calendar').evoCalendar({
			'format': 'dd MM, yyyy'
			//todayHighlight:true,
		})
		
		/*chking time slots doctor */
		function findValueInArray(value,arr){
		var result = "Doesn't exist";
		
		for(var i=0; i<arr.length; i++){
			var name = arr[i];
			if(name == value){
			result = 'Exist';
			break;
			}
		}

		return result;
		}
		
		function timeslotdata(testdate){
			$('.calendar-events #timeslotajax').html('Loading Timeslots...');
			var doctorID = $("#selectedDoctorID").val();
			var doctorID = !isEmpty(doctorID) ? doctorID : "doctor";
			$.ajaxSetup({
					headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
					});
					$.ajax({
							url: "{{url('/blockeddates')}}",
							type: "POST",
							data: {date:testdate,doctorID:doctorID},
							dataType:'json',
							success: function( response ) {
								console.log(response);
								$('.calendar-events #timeslotajax').html('<div class="col-md-12 text-center pb-1"><a class="btn btn-primary block-calendar-modal-btn">Block/Un Block Dates</a></div>');
							var timeslots = response.timeslots; 
							$.each(timeslots, function(i, timeslot) {
								var isChecked = typeof(timeslot.is_checked) != 'undefined' && timeslot.is_checked == true; 
								var isDisabled =  typeof(timeslot.is_disabled) != 'undefined' && timeslot.is_disabled == true;
								$('.calendar-events #timeslotajax').append('<label class="col-md-4"><input type="checkbox" '+(isChecked == true ? 'checked' : '')+' '+(isDisabled == true ? 'disabled' : '')+' name="timeslot[]" value="'+ timeslot.time_slot +'" class="checkBoxClass">' +  timeslot.time_slot + ' </label> ');
							});


							$("#blockdate-from").datepicker({
								minDate:'today',
								onSelect: function (selected) {
									var dt = new Date(selected);
									dt.setDate(dt.getDate() + 1);
									$("#blockdate-to").datepicker("option", "minDate", dt);
								}
							});
							$("#blockdate-to").datepicker({
								minDate:'today',
								onSelect: function (selected) {
									var dt = new Date(selected);
									dt.setDate(dt.getDate() - 1);
									$("#blockdate-from").datepicker("option", "maxDate", dt);
								}
							});
		}
				});
		}

		$('#calendar').on('selectDate', function(event, newDate, oldDate) {	
			var test=newDate;			
			$('#selecteddate').val(test);
			$('#delteddate').val(test);
			document.getElementById('errortask').style.display = "none";

			/*time slot blocking */
			var testdate=newDate;
			//alert(testdate);
			timeslotdata(testdate);
			console.log("-----------")
			
			/*time slot blocking */
			
			/*$("#ckbCheckAll").click(function () {
				$(".checkBoxClass").prop('checked', $(this).prop('checked'));
			});*/

			document.getElementById('successtask').style.display = "none";
			document.getElementById('successtask1').style.display = "none";
			document.getElementById('edittask').style.display = "none";

		});
		
		
	/*========add event===================*/	
		$("#addevent_btn").click(function(){
			//alert("test");
			document.getElementById('successtask').style.display = "none";
			document.getElementById('errortask').style.display = "none";
			$("#addevent_form").validate({
				rules: {
					notes: {
						required: true
					},
					timeslot: {
						required: true
					},
				},
				messages: {
					notes: {
									required: "Please Enter Notes"
								},
								timeslot: {
									required: "Please select timeslot"
								},
				},
				submitHandler: function(form) {
				$("#addevent_btn"). attr("disabled", true);
				$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});

				var doctorID = $("#selectedDoctorID").val();
	         	var doctorID = !isEmpty(doctorID) ? doctorID : "doctor";

				$.ajax({
				url: "{{url('doctortasks')}}",
				type: "POST",
				data: $('#addevent_form').serialize()+"&doctorID="+doctorID,
				dataType:'json',
				beforeSend: function() {$('#addevent_btn').html('Please Wait');},
				success: function( response ) {
				$('#addevent_btn').html('Submit');	
					if(response.status == 'success'){
						$("#addevent_btn"). attr("disabled", false);
						$.alert({
							title: 'Success',
							type: 'green',
							backgroundDismiss: true,
							content: 'Slots blocked successfully.',
						});
						// document.getElementById('successtask').style.display = "block"; 
						// setTimeout(function(){
						// 	document.getElementById('successtask').style.display = "block"; 
						// },8000);
					}
					if(response.status == 'error'){
						$("#addevent_btn"). attr("disabled", false);
						document.getElementById('errortask').style.display = "block"; 
					}				
				}
				});
				}
				})
			/**/			
		});  
		  
		  
		  /*active calendar*/
		  $(document).ready(function(){
			  var currentURL = $(location).attr('href');
			  if(currentURL.indexOf('#nav-calendar-tab') != -1){
					$('.nav-tabs a.active').removeClass('active')
					$('.tab-content div.active').removeClass('active')
					$('.tab-content div.show').removeClass('show')
					
					//$(this).addClass('active');
					$('#nav-calendar-tab').addClass('active')
					$('#nav-calendar').addClass('active')
					$('#nav-calendar').addClass('show')
					}				
			})	
		
			
$('#block_select_all').change(function(e){
	var month = $('#mdp-demo .ui-datepicker-month').text();
	var year = $('#mdp-demo .ui-datepicker-year').text();
	var monthStartDate = new Date('01/'+month+'/'+year);
			monthStartDate.setHours(0);
			monthStartDate.setMinutes(0);
			monthStartDate.setSeconds(0);
	var monthStartTimeStamp =  monthStartDate.getTime();
	var monthLastDate = new Date(year,monthStartDate.getMonth()+1,0);
			monthLastDate.setHours(23);
			monthLastDate.setMinutes(59);
			monthLastDate.setSeconds(59);
	var monthLastTimeStamp = monthLastDate.getTime();
	var blocked_dates = $('#mdp-demo').multiDatesPicker('getDates');
	var blockedDates = [];
		if(blocked_dates) {
			$.each(blocked_dates,function(index,date){
				blockedDates[index] = new Date(date).getTime();
			})
		}
	var blkCalendarConfig ={
		minDate: 0,
		onSelect:function(date){
			$('#selected_date').val(date);
		},
		defaultDate:monthStartDate.toLocaleDateString() 
	};
	var disabledDates = <?=$un_blockable_dates?>;
	if(disabledDates.length > 0) {
		blkCalendarConfig['addDisabledDates'] = disabledDates;
	}

	if(this.checked == true) {
		var currentMonthDates = [];
		for(var i=1; i<=31; i++){
			var cal_date = monthStartDate;
				cal_date.setDate(i);
				// if(new Date() > cal_date){
				// 	continue;	
				// }
				currentMonthDates.push(cal_date.getTime());
		}
		$('#mdp-demo').multiDatesPicker('addDates', currentMonthDates);
		$("label[for='block_select_all']").text('Unselect All');
	}else{
		$('#mdp-demo').multiDatesPicker('resetDates', 'picked');
		console.log(blockedDates)
		var currentMonthBlockedDates = [];
		for(var i=0; i < blockedDates.length; i++) {
			if(blockedDates[i] >= monthStartTimeStamp && blockedDates[i] <= monthLastTimeStamp ){
				currentMonthBlockedDates.push(blockedDates[i]);
			}
		}
		var updatedBlockedDates = blockedDates.filter((date)=> $.inArray(date,currentMonthBlockedDates) == -1 );
		blkCalendarConfig['addDates'] = updatedBlockedDates;
		$('#mdp-demo').multiDatesPicker('addDates',updatedBlockedDates);
		$("label[for='block_select_all']").text('Select All');
	}
})

$('body').on('click','.block-calendar-modal-btn',function(){
	var date = new Date();
	var blockedDates = <?=$blockedDates?>;
	var disabledDates = <?=$un_blockable_dates?>;
	var blkCalendarConfig ={
		minDate: 0,
		onSelect:function(date){
			$('#selected_date').val(date);
		}
	};
	if(disabledDates.length > 0) {	
		blkCalendarConfig['addDisabledDates'] = disabledDates;
	}
	if(blockedDates.length > 0){
		blkCalendarConfig['addDates'] = blockedDates;
	}

	$('#mdp-demo').multiDatesPicker(blkCalendarConfig);
	$('#block-calendar-modal').modal('show')
})

$('#block-date-calendar-form').submit(function(e){
	var dates = $('#mdp-demo').multiDatesPicker('getDates');
	$('#blocked_dates').val(dates.toString());
})


			
})


</script>

<script>	
	$("#addevent_btn1").click(function(){
			//alert("test");
			document.getElementById('successtask1').style.display = "none";
			$("#deleteevent_form").validate({
				rules: {
					rownotes1: {
									required: false
								},
				},
				messages: {
					rownotes1: {
									required: "Please Date"
								},
				},
				submitHandler: function(form) {
				$("#addevent_btn1"). attr("disabled", true);
				$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
				$.ajax({
				url: "{{url('deletetask')}}",
				type: "POST",
				data: $('#deleteevent_form').serialize(),
				dataType:'json',
				beforeSend: function() {$('#addevent_btn1').html('Please Wait');},
				success: function( response ) {
				$('#addevent_btn1').html('Submit');	
				   console.log(response.res);
					if(response.status == 'success'){
						console.log(response.res);
						$("#addevent_btn1"). attr("disabled", false);
						document.getElementById('successtask1').style.display = "block"; 
						document.getElementById("deleteevent_form").reset();
						 window.location.href=response.url;
							location.reload();
							setTimeout(function () {
								location.reload(true);
							  }, 1000); 
					}					
				}
				});
				}
				})
			/**/			
		  });  

</script>

			
<script>
	$("#block_unblock_dates").click(function(){
		$('#block_unblock_dates_modal').modal('show');
	});	
</script>

<script>
	function reply_click_doctorinvoice(id)
  {	 
	
	$("#hsttr").hide();
	$("#subtotaltr").hide();

	 var clickId = id;	 
	 //alert(clickId);
	
       //alert('success');
	   $.ajax({
		   url: "{{url('/doctor_invoice_modal_view')}}",
		   type: "POST",
		   headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
		   data: {id:clickId},
		   dataTYpe: 'json',
		   success:function(response){
			  //console.log(response);		
			  if(response.status=='success'){	
				  $('#doctorname').html(response.doctorname);
				  $('#categoryname').html(response.categoryname);	
				  $('#doctorlicencenumber').html(response.doctorlicencenumber);	
				  $('#doctorphone').html(response.doctorphone);	
				  $('#invitationdate').html(response.invitationdate);	
				  $('#patientname').html(response.patientname);
				  $('#patientdateofbirth').html(response.patientdateofbirth);
				  $('#patientdateofaccident').html(response.patientdateofaccident);
				  $('#new_service_provided').html(response.serviceprovided);
				  $('#todaydate').html(response.todaydate);
				  
				  $('#invitationId').val(clickId);
				  var doctor_invoice_number=String("00" + response.doctor_invoice_number).slice(-2); 
				  $('#doctor_invoice_number').html(doctor_invoice_number);

					if(response.doctorhst=='yes'){
						$('.new_bill_amt').html(response.billamt);
						$("#hsttr").show();
						$("#subtotaltr").show();
						$('#hstamount').html(response.hstamount);
						$('#subtotal').html(response.subtotal);
					}else{
						$('.new_bill_amt').html(response.subtotal);
					}

				  $('#invoicemodal').modal('show');
			  }
		   },   
	   })
	
}


</script>


