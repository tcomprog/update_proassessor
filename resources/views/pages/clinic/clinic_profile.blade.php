@extends('layouts.clinic-layout') 
@section('content')	

<style>
  .mainContainere{
    box-shadow: rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;
    padding: 20px 30px;
    border-radius: 15px;
  }

  .error{
    margin-bottom:0px;
  }

  .inputStyle{
    flex: 1;
  }
</style>

	 <!-- Banner Section -->
     <section class="intro_banner_sec inner_banner_sec" style="padding-top: 25px; padding-bottom: 25px;">
        <div class="container">
            <div class="row align-items-center"> 
                <div class="col-md-6">
                    <div class="welcome_text">
                        <h1>Profile <img src="{{ asset('n_img/clinic.png') }}" height="35" alt="Lawyer's Dashboard" class="dashboard_icon"></h1>
                    </div>
                </div>
                <div class="col-md-6 text-right">
                    <!-- <a href="<?= url('search-doctor'); ?>" class="search_btn">Search Doctors</a> -->
                </div>
            </div>
        </div>
    </section>

<div class="container py-3">
 
    <form id="update-clinic-profile" enctype="multipart/form-data" method="POST">
        
    <div class="container py-4">
       <div class="row mainContainere">

        <div class="col-12">
        <div class="alert alert-success succes-msg" style="display:none" role="alert">
           SUCCESS MESSAGE
          </div>
          <div class="alert alert-danger error-msg" style="display:none" role="alert">
            ERROR MESSAGE
          </div>
        </div>

        <div class="col-md-6 col-sm-12">
            <div class="inputContainer">
                <span class="material-icons" style="color: #515C84; font-size:28px;">person</span>
                <input id="name1" name="name" placeholder="Name" type="text" class="inputStyle" autocomplete="off" value="{{$user->name}}"/>			  
            </div>
        </div>

        <div class="col-md-6 col-sm-12">
            <div class="inputContainer" style="background: #f9f9f9;">
                <span class="material-icons" style="background: #f9f9f9;" style="color: #515C84 !important; font-size:28px;">drafts</span>
                <input disabled id="email1" name="email" placeholder="Email" type="text" class="inputStyle" style="background: #f9f9f9;" title="You can`t change email address" autocomplete="email" value="{{$user->email}}"/>
             </div>
        </div>

        <div class="col-12">
            <div class="inputContainer">
                <span class="material-icons" style="color: #515C84; font-size:28px;">add_location</span>
                <input autocomplete="off" id="business_address1" name="business_address" placeholder="Business Address" type="text" class="inputStyle" autocomplete="off" value="{{$user->business_address}}"/>
             </div>
        </div>

        <div class="col-md-6 col-sm-12">
            <div class="inputContainer">
                <span class="material-icons" style="color: #515C84; font-size:28px;">call</span>
                <input id="phone1" maxlength="10" name="phone" placeholder="Phone number" type="text" class="inputStyle" autocomplete="off" value="{{$user->phone}}" />
             </div>
        </div>

        <div class="col-md-12 col-sm-12 mt-4" style="display: flex;align-items: center;">
            <div>
               <input type="file" name="image" accept=".png,.jpg,.jpeg"> <br>
               <span class="text-danger">You can only upload png, jpg, jpeg</span>
            </div>
            @if (!empty($user->profilepic) && $user->profilepic != 'null')
                <img style="border-radius: 10px;" width="60" height="60" src="{{asset("uploads/profilepics/clinic/".$user->profilepic)}}" alt="">
            @endif
        </div>

        <div class="col-md-12 col-sm-12 mt-4 d-flex justify-content-center">
            <img id="loadingImg" width="35" height="35" style="display:none;" src="{{asset("img/loading.gif")}}" alt="">
            <button id="updateClinicProfile" class="btn btn-primary submit_btn submit_btn22" style="margin:0px !important;">Submit</button>
        </div>

      </div>
    </div>

    </form>
</div>

@endsection