<!doctype html>
<html>
<head>
    @include('includes.head')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.css">
</head>
<body style="display: flex; flex-direction:column; height:100vh;">

    <header>
        <div class="container">
            <div class="row">
                @include('includes.header')
            </div>
        </div>
    </header>

    @php
        $routeName = \Request::route()->getName(); 
    @endphp


  <div style="flex:1;">
     @yield('content')
  </div>

    <footer style="margin-top:auto; padding-bottom:0px; padding-top:25px;">
        @include('includes.footer')
    </footer>

    @include('includes.clinic-model')

    @if ($routeName == 'viewClinicCalender')
         @include('includes.doctor-scripts')
    @elseif ($routeName == 'addClinicDoctor')
        @include('includes.clinic-docotr-script')
    @else
         @include('includes.clinic-script')
    @endif
   
    @yield('footerScripts')

</body>
</html>