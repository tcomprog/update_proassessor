<?php
namespace App\Mail;
use App\Models\user_invitations;
use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
class LawyerSendInvitation extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * The order instance.
     *
     * @var \App\Models\Order
     */
    public $invitation;
    public $options = [];
    /**
     * Create a new message instance.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function __construct(user_invitations $invitation,$options)
    {
        $this->invitation = $invitation;
        $this->options = $options;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $fromAddress = Config::get('constants.MAIL_FROM_ADDRESS');
        $site_title_old = Config::get('constants.site_title');
        $site_title=str_replace("-"," ",$site_title_old);
        
        if(isset($this->options['type']) && $this->options['type'] == 'doctor') {
            return $this->subject($site_title. ' - New Invitation Notification.')->from($fromAddress,$site_title)->view('emails.lawyer_send_invitation_to_doctor',['invitation'=>$this->invitation, 'site_title'=>$site_title]);
        }	
    }
}