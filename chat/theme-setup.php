<?php

add_action('after_setup_theme', '_emogility_after_setup_theme');
function _emogility_after_setup_theme() {
    update_option('_emogility_company_name', COMPANY_NAME);

    // if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    // }

    register_nav_menus( array(
		// 'primary-menu' => __('Primary Menu', TEXT_DOMAIN),
		// 'primary-right-menu' => __('Primary Right Menu', TEXT_DOMAIN),
		'footer-menu' => __('Footer Menu', TEXT_DOMAIN),
		// 'sidebar-menu' => __('Sidebar Menu', TEXT_DOMAIN),
		// 'sidebar-footer-menu' => __('Sidebar Footer Menu', TEXT_DOMAIN)
	));

	// Add Featured Image Support.
	add_theme_support('post-thumbnails');
	add_theme_support('title-tag');
	add_theme_support('automatic-feed-links');


	/**
	 * Add Woocommerce Theme Supports.
	 */
	add_theme_support( 'woocommerce' );
	// add_theme_support( 'wc-product-gallery-zoom' );
	// add_theme_support( 'wc-product-gallery-lightbox' );
	// add_theme_support( 'wc-product-gallery-slider' );

	/**
	* Add Excerpt Feature to 'Page'.
	*/
	add_post_type_support( 'page', 'excerpt' );
}

add_action('widgets_init', 'emogility_widget_init');
function emogility_widget_init() {
	// Footer areas
	// register_sidebar(array(
	// 	'name' => 'Footer Area 1',
	// 	'before_widget' => '<div class="footer-widget">',
	// 	'after_widget' => '</div>',
	// 	'before_title' => '<h4 class="widget-title">',
	// 	'after_title' => '</h4>',
	// 	'id' => 'footer-1',
	// ));

	register_sidebar(array(
		'name' => 'Footer Area',
		'before_widget' => '<div class="footer-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
		'id' => 'footer-2'
	));
}

add_filter( 'rest_endpoints', function( $endpoints ){
    if ( isset( $endpoints['/wp/v2/users'] ) ) {
        unset( $endpoints['/wp/v2/users'] );
    }
    if ( isset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] ) ) {
        unset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] );
    }
    return $endpoints;
});