<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\AdminSettings;
use App\Models\Designation;
use App\Models\Pagecms;
use App\Models\doctors;
use App\Models\ClinicPackages;
use App\Models\Sitedetails;
use App\Models\Invites_Payments;
use App\Helper;
use App\Models\PriceChangeRequest;
use App\Models\Holiday;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\user_invitations;
use App\Models\ClientPayment;
use App\Models\ClientSubscription;
use App\Models\ClinicSpecialOffer;
use DB;
use Auth;

class ClinicController extends Controller
{
    public function dashboard(){
      $doctors = User::where("clinic_id",Auth::id())->get();
      $doctorIDS = [];
      $doctorList = [];
      foreach($doctors as $row){
        array_push($doctorIDS,$row->id);
        $doctorList[$row->id] = $row;
      }
      
      $settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
	  $expireTime =date('Y-m-d H:i:s',strtotime('-'.$settings->invitation_expire_in_minutes.' minutes'));

      $newList=$blocked_dates=array();
	  $invitations = user_invitations::whereIn('invitee_user_id',$doctorIDS)->orderBy('id','desc')->get();
      $archived_invitations=user_invitations::whereIn('invitee_user_id',$doctorIDS)->where('is_archive','yes')->orderBy('id','desc')->get();

      $completedAr = $new_invitations = $accepted_invitations =$confirmed_invitations= $expired_invitations = $rejected_invitations = $cancelled_invitations  = [];
      $dueDateArray = $invitationDateArray=[];
      $un_blockable_dates=[];
      $assessment_days_all=[];
      $due_days_all=[];
      $result='';

      if($invitations) {
		foreach($invitations as $invitation) {
          
			if($invitation->status == 'pending' && strtotime($invitation->created_at) > strtotime($expireTime)) {
				$new_invitations[] = $invitation;
				$un_blockable_dates[] = intval(strtotime($invitation->invitation_date).'000');
			}
            else if($invitation->status == 'active' && $invitation->is_patient_accepted == 'yes') {
				if(isset($request->lawyerkeyword)){					
					if((strripos($invitation->user->name, $request->lawyerkeyword)!==false)){						
						$confirmed_invitations[] = $invitation;
					}
                    elseif(strripos($invitation->patient->name, $request->lawyerkeyword)!==false){
						$confirmed_invitations[] = $invitation;
					}
					elseif(strripos($invitation->user->lawyer->lawfirmname, $request->lawyerkeyword)!==false){
						$confirmed_invitations[] = $invitation;
					}
				}
                else{ 
					$confirmed_invitations[] = $invitation;
				}
				$un_blockable_dates[] = intval(strtotime($invitation->invitation_date).'000');
				$invitationDateArray[] = $invitation->invitation_date.' 00:00:00';
				$dueDateArray[] = $invitation->duedate.' 00:00:00';
			}
			else if($invitation->status == 'rejected') {
				$rejected_invitations[] = $invitation;
			}
			else if($invitation->status == 'completed') {
				$completedAr[] = $invitation;
			}
			else if($invitation->status == 'cancelled') {
				$cancelled_invitations[] = $invitation;
			}
            else if($invitation->status == 'active' && $invitation->is_patient_accepted == 'no') {
				$accepted_invitations[] = $invitation;
				$dueDateArray[] = $invitation->duedate.' 00:00:00';
				$un_blockable_dates[] = intval(strtotime($invitation->invitation_date).'000');
			}
            else if($invitation->status == 'pending' && $invitation->is_archive == 'no' && strtotime($invitation->created_at) <= strtotime($expireTime)) {
				$expired_invitations[] = $invitation;
			}

			if(($invitation->status == 'pending' && strtotime($invitation->created_at) <= strtotime($expireTime)) || $invitation->status == 'rejected' || $invitation->status == 'cancelled') {
				continue;
			}	
					
			  $newList1[] = [				
				'id'   =>  $invitation['unique_id'],
				'name' =>  $invitation->status ==='pending'?'New Assessment':'Assessment  Accepted',				
				'date' => date('Y-m-d', strtotime($invitation['invitation_date'] .' +1 day')),
				'timeslot' => $invitation['timeslot'],						
				'patientname' => ucfirst($invitation->patient->name),	
				'docid' => $invitation->inviter_user_id,
				'type'  => "holiday", 
				];

			   $newList2[] = [				
				'id'   =>  $invitation['unique_id'],
				'name' =>    'Report Due Date',
				'date' => date('Y-m-d', strtotime($invitation['duedate'] .' +1 day')),
				'timeslot' => $invitation['timeslot'],  						
				'patientname' => ucfirst($invitation->patient->name),	
				'docid' => $invitation->inviter_user_id,
				'type'  => "event", 
				];	
                
				$assessment_days_all[]=date('Y-m-d',strtotime($invitation['invitation_date']));				
				$due_days_all[]=date('Y-m-d',strtotime($invitation['duedate']));				
				
				//$newList3=[]; 
				// $actual_blocked_dates = Holiday::where('doctor_user_id',$user->id)->where('date','>=',date('Y-m-d'))->get()->pluck('date')->toArray();
				// $actual_due_dates = user_invitations::where('invitee_user_id',$user->id)->where('duedate','>=',date('Y-m-d'))->get()->pluck('duedate')->toArray();
				// if($actual_due_dates){
				//   $result = array_intersect($actual_blocked_dates, $actual_due_dates);			
				// } 

				$newList = array_unique(array_merge($newList1, $newList2),SORT_REGULAR);	
			}	

            $invoices = Invites_Payments::whereIn('doctor_user_id', $doctorIDS)->orderBy('id', 'desc')->paginate(10);
            $suminvoices = Invites_Payments::query();
            $suminvoices->whereHas('invitations', function($q){
                    $q->where('status', '=', 'active');  });
            $suminvoices=$suminvoices->sum('payment_amount');
		}

      
      return view('pages.clinic.clinic-dasboard',[
              'newList' => $newList,
              'expire_time'=>$expireTime,
              'invoices' => $invoices, 
			  'suminvoices' => $suminvoices,
              'invitations' => $invitations,
              'new_invitations'=>$new_invitations,
              'accepted_invitations'=>$accepted_invitations, 
              'archived_invitations'=>$archived_invitations,
              'expired_invitations'=>$expired_invitations,
              'confirmed_invitations'=>$confirmed_invitations,
              'completedAr'=>$completedAr,
              'rejected_invitations'=>$rejected_invitations,
              'cancelled_invitations'=>$cancelled_invitations,
              'dueDateArr'=>json_encode($dueDateArray),
			  'invitationDateArr'=>json_encode($invitationDateArray),
			  'blockedDates'=>json_encode($blocked_dates),
			  'doctorList'=>$doctorList,
			  'dcotorLs'=>$doctors,
      ]);

    }

    public function profile(){
       $user = User::find(Auth::id());
       return view('pages.clinic.clinic_profile',['user'=>$user]);
    }

    public function updateProfile(Request $request){
       $name = $request->name;
       $business_address = $request->business_address;
       $phone = $request->phone;

       $rules=[
        'image' => 'mimes:png,jpg,jpeg|max:2500',
       ];

       $validator = Validator::make($request->all(), $rules);
       if ($validator->fails()) {
           $response['response'] = $validator->messages();
           return response()->json(['status'=>'invalid', 'message'=>$validator->messages()]);					
        }
        else{
            $user = User::find(Auth::id());
            $user->name = $name;
            $user->phone = $phone;
            $user->business_address = $business_address;

            if($request->has("image")){

                if(!empty($user->profilepic) && $user->profilepic != 'null'){
                   unlink(public_path('uploads/profilepics/clinic/'.$user->profilepic));
                }

                $imageName = $phone.'_'.time().'.'.$request->image->extension();
                $request->image->move(public_path('uploads/profilepics/clinic'), $imageName);
                $user->profilepic = $imageName;
            }
            $user->save();

            return response()->json(['status'=>'success','message'=>"Profile update successfully"]);
        }
    }

    public function changeClinicPassword(Request $request){
        $old_password = $request->oldP;
		$new_password = Hash::make($request->newP);
		$user = auth()->user();		   
		$users = User::find($user->id);	

		if (Hash::check($old_password, $user->password)) {
			$user->password = $new_password;
			$user->save();
			return response()->json(['status'=> "success","message"=>"Password changed successfully"]);
		}
		else{
			return response()->json(['status'=> "mismatch","message"=>"Old password doesn`t match"]);
		}
    }

    public function addClinicDoctor(){
       return view("pages.clinic.addDoctor");
    }

    public function addNewDoctorProfile(Request $request){

        $name = $request->input("name");
        $email = $request->input("email");
        $address = $request->input("address");
        $password = $request->input("password");
        $phone = Auth::user()->phone; //$request->input("phone");
        $city = $request->input("city");
        $licence_number = $request->input("licence_number");
        $category = $request->input("category");
        $term_condition = $request->input("term_condition");
        $desginationDoctor = $request->input("desginationDoctor");
        $assessmentOnly = $request->input("assessmentOnly");
        $lateCancellationFee = $request->input("lateCancellationFee");
        $introduction = $request->input("introduction");
        $virtualAppoiment = $request->input("virtualAppoiment");
        $hst = $request->input("hst");
        $hst_number = ($hst == "yes") ? $request->input("hst_number") : "";
        $price = $request->input("price");
      
        // $check = User::where('email',$email)->get();
        // if(count($check) > 0){
        //     return response()->json(['status'=>'exist', 'message'=>"Account exist"]);
        // }

        $user = AdminSettings::where('settings_name', 'comission')->first();
        if($user!=null){
            $comission=$user->settings_value;
        }else{
            $comission='';
        }

        $email_verify_token = md5(time());
        $uniqueid = md5(uniqid());				
        $data1 = new User();
        $data1->name=$name;
        $data1->email= Auth::id()."_".time();
        $data1->clinic_id= Auth::id();
        $data1->unique_id=$uniqueid;
        $data1->password="null";
        $data1->phone= $phone;
        $data1->location= $city;
        $data1->status= 'active';
        $data1->authentication_status= 'active';
        $data1->account_status= 1;
        $data1->role_id=1;
        $data1->business_address=$address;
        $data1->remember_token=$request->_token;
        $data1->email_verify_token=$email_verify_token;
        $data1->desgination=$request->desginationDoctorID;
        
        $data1->save();

        $last_insertid=$data1->id;
        $emailtoken=$data1->email_verify_token;
        $regemail=$data1->email;
        $regname=$data1->name;

        if(isset($last_insertid)){			
            $data2 = new doctors();
            $data2->user_id= $last_insertid;
            $data2->unique_id= $uniqueid;
            $data2->licence_number= $licence_number;
            $data2->hst_number= $hst_number;
            $data2->introduction= $introduction;
            $data2->virtual_appointment	= $virtualAppoiment;
            $data2->hst	= $hst;
            $data2->actual_price = $price;
            $data2->account_status = 1;
            $data2->assessmentOnlyCharges = $assessmentOnly;
            $data2->category_id = $category;
            $data2->lateCancellationFee = $lateCancellationFee;
           
            $data2->save();
            $last_insertid1=$data2->id;

            $user = User::where('id', $last_insertid)->first(); 

            //$email_controller = new EmailController();					
           // $email_controller->send_account_activation_mail_to_doctor($user);    
            
            return response()->json(['status'=>'success', 'message'=>" Thank You Request Sent Successfully"]);
        }
        
    }


    public function showClinicDoctorList(){
        $dcotor = User::where("clinic_id",Auth::id())->get();
        return view("pages.clinic.doctorClinicList",
        [
            "doctor"=>$dcotor
        ]
        );
    }

    public function clincDoctorProfile($id){

       $settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
	   $expireTime = date('Y-m-d H:i:s',strtotime('-'.$settings->invitation_expire_in_minutes.' minutes'));
      // $doctor = User::where("id",$id)->first();
       $doctor = User::where('id', $id)->where('clinic_id',Auth::id())->first();
       $designation = Designation::where('id',$doctor->desgination)->first();
       $ratings = Helper::doctorRating($doctor->id);

       $new_ass = [];
       $accepted_ass = [];
       $confirmed_ass = [];
       $completed_ass = [];
       $expired_ass = [];
       $reject_ass = [];
       $cancelled_ass = [];
       $archived_ass = [];

       $assessment = user_invitations::where("invitee_user_id",$id)->get();
       foreach($assessment as $invitation){
            if($invitation->status == 'pending' && strtotime($invitation->created_at) > strtotime($expireTime)) {
              array_push($new_ass,$invitation);
            }
            else if($invitation->status == 'active' && $invitation->is_patient_accepted == 'yes') {
				array_push($confirmed_ass,$invitation);
			}
            elseif($invitation->status == 'rejected') {
                array_push($reject_ass,$invitation);
			}
			elseif($invitation->status == 'completed') {
                array_push($completed_ass,$invitation);
			}
			elseif($invitation->status == 'cancelled') {
                array_push($cancelled_ass,$invitation);
			}
            elseif($invitation->status == 'active' && $invitation->is_patient_accepted == 'no') {
                array_push($accepted_ass,$invitation);
			}
            elseif($invitation->status == 'pending' && $invitation->is_archive == 'no' && strtotime($invitation->created_at) <= strtotime($expireTime)) {
				array_push($expired_ass,$invitation);
			}
            elseif($invitation->is_archive == 'yes') {
				array_push($archived_ass,$invitation);
			}

       }
       

       return view("pages.clinic.clinicDoctorProfile",[
        'doctor'=>$doctor,
        'designation'=>$designation->designation,
        'rating' => $ratings,
        'new_ass' => $new_ass,
        'accepted_ass' => $accepted_ass,
        'confirmed_ass' => $confirmed_ass,
        'completed_ass' => $completed_ass,
        'expired_ass' => $expired_ass,
        'doctor_category_id'=>$doctor->doctor->category_id,
        'reject_ass' => $reject_ass,
        'cancelled_ass' => $cancelled_ass,
        'archived_ass' => $archived_ass,
       ]);

    }

    public function approveDoctor($id){
        $user = User::where('id', $id)->where('clinic_id',Auth::id())->first();
        $user->status = 'active';
        $user->save();

        return back();
    }

public function assessmentDetails($invitation_id){
        $invitation = user_invitations::where('unique_id',$invitation_id)->first();
		$patient = $invitation->patient;
		$user = $invitation->user;

        return view("pages.clinic.assessmentDetailsClinic",[
			'invitation'=>$invitation,
			'patient' => $patient, 
			'user' => $user,
        ]);
}


 public function viewClinicCalender($id){
   $user = User::where("id",$id)->where("clinic_id",Auth::id())->get();
   if(count($user) <= 0){
      exit("Invalid access");
   }

   $user = $user[0];
   $blocked_dates = [];

    $doctors = User::where("clinic_id",Auth::id())->get();
    $doctorIDS = [];
    $doctorList = [];
    foreach($doctors as $row){
        array_push($doctorIDS,$row->id);
        $doctorList[$row->id] = $row;
    }
    
    $settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
    $expireTime =date('Y-m-d H:i:s',strtotime('-'.$settings->invitation_expire_in_minutes.' minutes'));

    $newList=$blocked_dates=array();
    $invitations = user_invitations::whereIn('invitee_user_id',$doctorIDS)->orderBy('id','desc')->get();

    $dueDateArray = $invitationDateArray=[];
    $un_blockable_dates=[];
    $assessment_days_all=[];
    $due_days_all=[];
    $result='';

   if($invitations) {
    foreach($invitations as $invitation) {
        $newList1[] = [				
            'id'   =>  $invitation['unique_id'],
            'name' =>  $invitation->status ==='pending'?'New Assessment':'Assessment  Accepted',				
            'date' => date('Y-m-d', strtotime($invitation['invitation_date'] .' +1 day')),
            'timeslot' => $invitation['timeslot'],						
            'patientname' => ucfirst($invitation->patient->name),	
            'docid' => $invitation->inviter_user_id,
            'type'  => "holiday", 
            ];
        $newList2[] = [				
            'id'   =>  $invitation['unique_id'],
            'name' =>    'Report Due Date',
            'date' => date('Y-m-d', strtotime($invitation['duedate'] .' +1 day')),
            'timeslot' => $invitation['timeslot'],  						
            'patientname' => ucfirst($invitation->patient->name),	
            'docid' => $invitation->inviter_user_id,
            'type'  => "event", 
            ];	
            $assessment_days_all[]=date('Y-m-d',strtotime($invitation['invitation_date']));				
            $due_days_all[]=date('Y-m-d',strtotime($invitation['duedate']));				
            
            //$newList3=[]; 
            $actual_blocked_dates = Holiday::where('doctor_user_id',$user->id)->where('date','>=',date('Y-m-d'))->get()->pluck('date')->toArray();
            $actual_due_dates = user_invitations::where('invitee_user_id',$user->id)->where('duedate','>=',date('Y-m-d'))->get()->pluck('duedate')->toArray();
            if($actual_due_dates){
            $result = array_intersect($actual_blocked_dates, $actual_due_dates);			
            } 

            $newList = array_unique(array_merge($newList1, $newList2),SORT_REGULAR);	
        }	
    }

    $blockedDates = Holiday::where('doctor_user_id',$user->id)->where('date','>=',date('Y-m-d'))->get()->pluck('date')->toArray();
    if($blockedDates) {
        foreach($blockedDates as $blockedDate) {
            $blocked_dates[] = intval(strtotime($blockedDate).'000'); 
        }
    }
    $assessment_days=[];
    if($assessment_days_all) {
        foreach($assessment_days_all as $assessment_day) {
            $assessment_days[] = intval(strtotime($assessment_day).'000'); 
        }
    }
    $due_days=[];
    if($due_days_all) {
        foreach($due_days_all as $due_day) {
            $due_days[] = intval(strtotime($due_day).'000'); 
        }
    }
        
    $due_block_dates=[];
    if($result){
        foreach($result  as $due_block_result) {
            $due_block_dates = intval(strtotime($due_block_result).'000'); 
        }
    }

   return view("pages.clinic.clinicDoctorCalender",[
		'newList' => $newList,
		'users' => $user,
		'dueDateArr'=>json_encode($dueDateArray),
		'invitationDateArr'=>json_encode($invitationDateArray),
		'blockedDates'=>json_encode($blocked_dates),
		'dueblockedDates'=>json_encode($due_block_dates),
		'un_blockable_dates'=>json_encode($un_blockable_dates),
		'assessment_days'=>json_encode($assessment_days),
		'due_days'=>json_encode($due_days)
   ]);
 }

 public function subscription(){
    $page_slug="subscritpion";
    $pageuser = Pagecms::where('page_slug',$page_slug)->first();     
    $pagedetails = Sitedetails::find(1);
    $setting_about_image = AdminSettings::where('settings_name','aboutus_image')->first();

    $monthly = ClinicPackages::where('name','monthly')->first();
    $yearly = ClinicPackages::where('name','yearly')->first();

    $offer = ClinicSpecialOffer::where('clinic_id',Auth::id())->get();

    if(count($offer) > 0){
        $offer = $offer[0];
        if($offer->monthly > 0){
            $monthly->amount = $offer->monthly;
        }

        if($offer->yearly > 0){
            $yearly->amount = $offer->yearly;
        }
    }

    return view('pages.clinic.subscription', [
       'pageuser' => $pageuser,
       'pagedetails' => $pagedetails,
       'monthly' => $monthly,
       'yearly' => $yearly,
       ]); 
 }

 public function clinicSubscriptionDetails(){

    $page_slug="subscritpion";
    $pageuser = Pagecms::where('page_slug',$page_slug)->first();     
    $pagedetails = Sitedetails::find(1);
    $setting_about_image = AdminSettings::where('settings_name','aboutus_image')->first();

    $sub_tran = ClientPayment::where("user_id",Auth::id())->get();
    $subscription = ClientSubscription::where('user_id',Auth::id())->get();

    

   return view('pages.clinic.viewClinicSubDetails',[
    'pageuser' => $pageuser,
    'pagedetails' => $pagedetails,
    'sub_tran' => $sub_tran,
    'subscription'=> $subscription[0]
   ]);
 } 

}
