@extends("includes.email_container")
@section("content")
            <div class="text-center">
                <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Dear {{{ ucfirst($invitation->patient->name) }}},</h3>
                <p style="font-size: 14px;line-height: 17px;">Your Appointment is scheduled, please check below detail and click on Accept button to confirm your Appointment.</p>
                    <table class="table"  style="width:100%;">
                        <tr>
                            <th style="text-align: left;border-bottom: 1px solid #f2f2f2;padding-bottom:5px;color:#20215E;">
                               Lawyer 
                            </th>
                            <td style="text-align: left;padding-left:12px;border-bottom: 1px solid #f2f2f2;padding-bottom:5px; font-size:15px;">{{ $invitation->user->name }}</td>
                        </tr>
                        <tr>
                            <th style="text-align: left;border-bottom: 1px solid #f2f2f2;padding-bottom:5px; color:#20215E;">
                                Appointment details
                            </th>
                            <td style="text-align: left;padding-left:12px;border-bottom: 1px solid #f2f2f2;padding-bottom:5px; font-size:15px;">
                                <b>Date:</b> {{ date('d/M/Y', strtotime($invitation->invitation_date)) }}
                                <br/>
                                <b>Time:</b> {{ $invitation->timeslot }}
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: left;border-bottom: 1px solid #f2f2f2;padding-bottom:5px;color:#20215E;">Doctor details</th>
                            <td style="text-align: left;padding-left:12px;border-bottom: 1px solid #f2f2f2;padding-bottom:5px; font-size:15px;">
                                <b>Name:</b> {{ $invitation->user1->name }}, <br/>
                                <b>Specialization:</b> {{ $invitation->user1->doctor->category->category_name }}, <br/>
                                <b>Address:</b> {{ $invitation->user1->business_address }},{{ $invitation->user1->city->city_name }}. <br/>
								
								<?php if($invitation->user1->doctor->instruction_form!=null){ ?><b>Questionnaire :</b> <a  href="{{url('uploads/instruction_form')}}/<?php echo $invitation->user1->doctor->instruction_form; ?>" target="_blank">Download								
								<?php } ?> 
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <div style="margin-bottom: 20px;
                    height: auto;
                    width: 100%;
                    display: flex;
                    flex-direction: unset;
                    gap: 15px;
                    flex-wrap: wrap;
                    justify-content: center;">	
                      <table style="width:100%">
                        <tr>
                           <td align="center">
                            <a href="{{ url('patient/accept-invitation/'.$invitation->unique_id) }}" style="text-decoration: none;color: #fff;padding: 10px 40px;border-radius: 20px;border: 1px solid #50C878;color: #50C878;" class="btn btn-success">Accept</a>  
                            <a href="{{ url('patient/reject-invitation/'.$invitation->unique_id) }}" class="btn btn-danger" style="text-decoration: none;color: #fff;padding: 10px 40px;border-radius: 20px;border: 1px solid #20215E;color: #20215E; margin-left:30px">Reject</a>  
                          </td>
                         </tr>
                       </table>					  							
                        
                    </div>  
               </div>   
        <br/><br/>
                <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
                <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 
     </div>

@endsection