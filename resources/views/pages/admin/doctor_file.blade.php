@extends('layouts.admin_app')



@section('content')
<link href="{{ asset('css/doctor_dashboard.css') }}" rel="stylesheet">
<link href="{{ asset('css/clinic_dashboard.css') }}" rel="stylesheet">

<style>
  .rountedBtn{
    width: 36px !important;
    height: 36px !important;
    background: #7BCC70 !important;
    border-radius: 30px;
    display: flex !important;
    justify-content: center;
    align-items: center !important;
    margin-right: 12px  !important;
}

.rountedBtn:hover{
    cursor: pointer;
}
</style>

<style>
	.catlable{
		background: lightblue;
		padding: 1px 5px;
		border-radius: 5px;
		font-size: 11px;
	}
</style>

<div class="container-fluid">

	<div class="container dashboardContainer mb-5 " style="margin-top:10px;">
		<h3 class="inv-content-header">Profile</h3>

		<div class="row">
			<div class="col-12">
				<h4>Personal Information</h4>
	 
				@if (!empty($user->profilepic) && $user->profilepic != 'null')
				<img src="{{url('uploads/profilepics/doctor/'.$user->profilepic)}}" class="img-thumbnail ml-1 mt-1" style="width:80px !important;" alt="">
				@endif
	 
				<table>
					<tr>
					 <th width="170">Name</th>
					 <td>{{$user->name}}</td>
					</tr>
	 
					<tr>
					 <th>Email</th>
					 <td><b>{{$user->email}}</b></td>
					</tr>
	 
					<tr>
					 <th>Phone Number</th>
					 <td>{{$user->phone}}</td>
					</tr>
	 
					<tr>
					 <th>License Number</th>
					 <td>{{$user->doctor->licence_number}}</td>
					</tr>
	 
					<tr>
					 <th>Price</th>
					 <td><b>$</b>{{$user->doctor->actual_price}}</td>
					</tr>
	 
					<tr>
					 <th>Assessment Price</th>
					 <td><b>$</b>{{$user->doctor->assessmentOnlyCharges}}</td>
					</tr>
	 
					<tr>
					 <th>Cancellation Fee</th>
					 <td><b>$</b>{{$user->doctor->lateCancellationFee}}</td>
					</tr>
	 
					 <tr>
					 <th>Designation</th>
					 <td>{{$designation}}</td>
					</tr>

					<tr>
						<th>HST</th>
						<td>{{ucfirst($user->doctor->hst)}}</td>
					</tr>
		
					<tr>
						<th>HST Number</th>
						<td>{{$user->doctor->hst_number}}</td>
					</tr>
					
					<tr>
						<th>Virtual Appointment</th>
						<td>{{ucfirst($user->doctor->virtual_appointment)}}</td>
					</tr>
	 
					<tr>
					 <th>Business Address</th>
					 <td>{{$user->business_address}}</td>
					</tr>

					<tr>
						<th>City</th>
						<td>
							<?php  $cities = Helper::cities(); 
							foreach ($cities as $cities1){
							if(($cities1->id)==($user->location)){
								echo $cities1->city_name;
							   }
							}
						?>  
						</td>
					</tr>

					<tr>
						<th>Introduction</th>
						<td>{{$user->doctor->introduction}}</td>
					</tr>

					<tr>
						<th>Category</th>
						@php
							 $category = Helper::categorydetails(); 
							 $categoryids=explode(',',$user->doctor->category_id);
						@endphp
						
						<td>
							@foreach ($category as $categories)
							   @if(in_array($categories->id,$categoryids))
							     <span class="catlable">{{ucfirst($categories->category_name)}}</span>
							   @endif
							@endforeach
						</td>
					</tr>

					<tr>
					 <th>CV</th>
					 <td>
						@if ($user->doctor->cvimage != null)
					    	<div><a href="{{url('uploads/cv')}}/<?php echo $user->doctor->cvimage; ?>" style="color:blue" download><br>Download CV</a>	</div>
						@else
						  Not Uploaded
						@endif

					</td>
					</tr>

				</table>
			</div>

			 {{-- Doctor Rating Section --}}
			 @if($user->account_status == 1)
			 <div class="container disc">
			  <div class="row">
			   <div class="col-12 mt-4">
				<hr>
				<h4>Doctor Rating</h4>
				<div class="row">
					<div class="col-md-6 col-sm-12" style="margin-top: 5px;">
						<div class="rating-block">
							<h5 style="color: #323B62">Average user rating</h5>
							<h2 class="bold padding-bottom-7">{{$rating["doc_avg"]}} <small>/ 5</small></h2>
							<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
							  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
							</button>
							<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
							  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
							</button>
							<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
							  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
							</button>
							<button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
							  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
							</button>
							<button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
							  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
							</button>
						</div>
					</div>
	  
					<div class="col-md-5 col-sm-12" style="margin-top: 20px;">
						<h5 style="color: #323B62">Rating breakdown</h5>
						<div class="pull-left singleProgressdesing">
							<div class="pull-left" style="width:35px; line-height:1;">
								<div style="height:9px; margin:5px 0;">5 <span class="glyphicon glyphicon-star"></span></div>
							</div>
							<div class="pull-left" style="width:180px; flex:1">
								<div class="progress" style="height:9px; margin:8px 0;">
								  <div class="progress-bar" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 1000%;background:#50C878;">
									<span class="sr-only">80% Complete (danger)</span>
								  </div>
								</div>
							</div>
							<div class="pull-right" style="margin-left:10px;">{{$rating["rating_5"]}}</div>
						</div>
	  
						<div class="pull-left singleProgressdesing">
							<div class="pull-left" style="width:35px; line-height:1;">
								<div style="height:9px; margin:5px 0;">4 <span class="glyphicon glyphicon-star"></span></div>
							</div>
							<div class="pull-left" style="width:180px; flex:1">
								<div class="progress" style="height:9px; margin:8px 0;">
								  <div class="progress-bar" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80%; background:#0096FF">
									<span class="sr-only">80% Complete (danger)</span>
								  </div>
								</div>
							</div>
							<div class="pull-right" style="margin-left:10px;">{{$rating["rating_4"]}}</div>
						</div>
	  
						<div class="pull-left singleProgressdesing">
							<div class="pull-left" style="width:35px; line-height:1;">
								<div style="height:9px; margin:5px 0;">3 <span class="glyphicon glyphicon-star"></span></div>
							</div>
							<div class="pull-left" style="width:180px; flex:1">
								<div class="progress" style="height:9px; margin:8px 0;">
								  <div class="progress-bar" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width: 60%;background:#6F8FAF">
									<span class="sr-only">80% Complete (danger)</span>
								  </div>
								</div>
							</div>
							<div class="pull-right" style="margin-left:10px;">{{$rating["rating_3"]}}</div>
						</div>
	  
						<div class="pull-left singleProgressdesing">
							<div class="pull-left" style="width:35px; line-height:1;">
								<div style="height:9px; margin:5px 0;">2 <span class="glyphicon glyphicon-star"></span></div>
							</div>
							<div class="pull-left" style="width:180px; flex:1">
								<div class="progress" style="height:9px; margin:8px 0;">
								  <div class="progress-bar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: 40%;background:#FFC300">
									<span class="sr-only">80% Complete (danger)</span>
								  </div>
								</div>
							</div>
							<div class="pull-right" style="margin-left:10px;">{{$rating["rating_2"]}}</div>
						</div>
	  
						<div class="pull-left singleProgressdesing">
							<div class="pull-left" style="width:35px; line-height:1;">
								<div style="height:9px; margin:5px 0;">1 <span class="glyphicon glyphicon-star"></span></div>
							</div>
							<div class="pull-left" style="width:180px; flex:1">
								<div class="progress" style="height:9px; margin:8px 0;">
								  <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: 20%;background: #c72a00">
									<span class="sr-only">80% Complete (danger)</span>
								  </div>
								</div>
							</div>
							<div class="pull-right" style="margin-left:10px;">{{$rating["rating_1"]}}</div>
						</div>
					</div>		
	  
				</div>
			 </div>
		   </div>
		  </div>
			 @endif


		</div>

	</div>



	    {{-- Assessment Section --}}
<div class="col-12 mt-3">
       
	<h3 class="inv-content-header">Doctor Assessment</h3>
			
			<!-- Demo header-->
	<section class="py-3 header">
		<div class="container py-3">
		  
			<div class="row">
				<div class="col-md-3">
					<!-- Tabs nav -->
					<div class="nav flex-column nav-pills nav-pills-custom" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					   
					  <a class="nav-link mb-2 px-3 tabsShawo active" id="tab-new" data-toggle="pill" href="#new_assessment" role="tab" aria-controls="v-pills-home" aria-selected="true">
							<i class="fa fa-plus-circle mr-2"></i>
							<span class="font-weight-bold small text-uppercase">New</span>
					  </a>
	
						<a class="nav-link mb-2 px-3  tabsShawo" id="accepted-tab" data-toggle="pill" href="#accepted_assessment" role="tab" aria-controls="v-pills-profile" aria-selected="false">
							<i class="fa fa-check mr-2"></i>
							<span class="font-weight-bold small text-uppercase">Accepted</span>
						</a>
	
						<a class="nav-link mb-2 px-3 tabsShawo" id="confirmed-tab" data-toggle="pill" href="#confirmed_assessment" role="tab" aria-controls="v-pills-messages" aria-selected="false">
							<i class="fa fa-check-square-o mr-2"></i>
							<span class="font-weight-bold small text-uppercase">Confirmed</span>
						</a>
	
						<a class="nav-link mb-2 px-3 tabsShawo" id="completed-tab" data-toggle="pill" href="#completed_assessment" role="tab" aria-controls="v-pills-settings" aria-selected="false">
							<i class="fa fa-check-circle mr-2"></i>
							<span class="font-weight-bold small text-uppercase">Completed</span>
						</a>
					
	
						<a class="nav-link mb-2 px-3 tabsShawo" id="expired-tab" data-toggle="pill" href="#expired_assessment" role="tab" aria-controls="v-pills-settings" aria-selected="false">
							<i class="fa fa-flask mr-2"></i>
							<span class="font-weight-bold small text-uppercase">Expired</span>
						</a>
	
						<a class="nav-link mb-2 px-3 tabsShawo" id="rejected-tab" data-toggle="pill" href="#rejected_assessment" role="tab" aria-controls="v-pills-settings" aria-selected="false">
							<i class="fa fa-compress mr-2"></i>
							<span class="font-weight-bold small text-uppercase">Rejected</span>
						</a>
	
						<a class="nav-link mb-2 px-3 tabsShawo" id="cancelled-tab" data-toggle="pill" href="#cancelled_assessment" role="tab" aria-controls="v-pills-settings" aria-selected="false">
							<i class="fa fa-times mr-2"></i>
							<span class="font-weight-bold small text-uppercase">Cancelled</span>
						</a>
	
						<a class="nav-link mb-2 px-3 tabsShawo" id="archived-tab" data-toggle="pill" href="#archived_assessment" role="tab" aria-controls="v-pills-settings" aria-selected="false">
							<i class="fa fa-archive mr-2"></i>
							<span class="font-weight-bold small text-uppercase">Archived</span>
						</a>
					</div>
				</div>
	
	
				<div class="col-md-9">
					<!-- Tabs content -->
					<div class="tab-content h-100" id="v-pills-tabContent">
	
					  {{-- New Assessment --}}
						<div class="tab-pane fade rounded show  active " id="new_assessment" role="tabpanel" aria-labelledby="v-pills-home-tab">
						   @if (count($new_ass) <= 0)
							  <h3 class="nothingTxt">Nothing to show</h3>
						   @else
							   @foreach ($new_ass as $invitation)
							   <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
								<div class="design_card_new">
								  <div class="d-flex">
									  @if (($invitation->user->profilepic)!=null)
										 <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
									  @else
										<img class="n_images" src="{{asset('img/no_image.jpg')}}" />
									   @endif
									<div class='n_c_container'>
									  <h5 class="d_name_text">{{ucfirst($invitation->user->name)}}</h5>
									  <p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
									</div>
								  </div>
	
								<table id="n_desing_tbl">
									@if($doctor_category_id !== $invitation->invitee_category_id)
									  <tr>
									  <th>Category:</th>
									  <td>{{ $invitation->category->category_name }}</td>
									  </tr>
									@endif
								   
									<tr>
									<th>Date:</th>
									<td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
									</tr>
									<tr>
									<th>Due date:</th>
									<td>{{date('d F Y', strtotime($invitation->duedate ))}}
									</td>
									</tr>
			  
									<tr>
									<th>Type:</th>
									<td>{{ucfirst($invitation->appointment_type)}}</td>
									</tr>
								   </table>
			  
								   {{-- <div class="button_containter">
									<img id="loading_{{$invitation->id}}" style="display:none;" width="40" src="{{asset("img/loading.gif")}}" alt="">
									
									<div id="accept_{{$invitation->id}}" class="accept-invitation-btn rountedBtn" data-id="{{ $invitation->id }}"  title="Accept">
									   <span class="material-icons" style="color: white; font-size:30px;">check</span>
									 </div>
			  
									<div id="reject_{{$invitation->id}}" class="reject-inv-btn rountedBtn"  style="background:#FA1D2F !important" title="Reject" data-id="{{ $invitation->id }}">
									   <span class="material-icons" style="color: white; font-size:30px;">close</span>
									 </div>
	
								   </div> --}}
	
								   <div class="button_containter">
							 
									<div class="rountedBtn" style="background:#546E7A !important;" data-id="{{ $invitation->id }}"  title="View more">
									  <a href="{{ url('/admin_assess_details/'.$invitation->unique_id)}}" style="line-height: 0;">
										<i class="fa fa-eye" aria-hidden="true" style="color: white; font-size:22px;"></i>
									  </a>
									</div>
		  
								   </div>
	
								</div>
							  </div>
	
							   @endforeach
						   @endif 
						</div>
						
						{{-- Accepted assessment section --}}
						<div class="tab-pane fade  rounded" id="accepted_assessment" role="tabpanel" aria-labelledby="v-pills-profile-tab">
						  @if (count($accepted_ass) <= 0)
						  <h3 class="nothingTxt">Nothing to show</h3>
						@else
						 @foreach ($accepted_ass as $invitation)
						 <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
						  <div class="design_card_new">
							<div class="d-flex">
								@if (($invitation->user->profilepic)!=null)
								   <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
								@else
								  <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
								 @endif
							  <div class='n_c_container'>
								<h5 class="d_name_text">{{ucfirst($invitation->user->name)}}</h5>
								<p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
							  </div>
							</div>
	
						  <table id="n_desing_tbl">
							  @if($doctor_category_id !== $invitation->invitee_category_id)
								<tr>
								<th>Category:</th>
								<td>{{ $invitation->category->category_name }}</td>
								</tr>
							  @endif
							 
							  <tr>
							  <th>Date:</th>
							  <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
							  </tr>
							  <tr>
							  <th>Due date:</th>
							  <td>{{date('d F Y', strtotime($invitation->duedate ))}}
							  </td>
							  </tr>
		
							  <tr>
							  <th>Type:</th>
							  <td>{{ucfirst($invitation->appointment_type)}}</td>
							  </tr>
							 </table>
		
							 <div class="button_containter">
							 
							  <div class="rountedBtn" style="background:#546E7A !important;" data-id="{{ $invitation->id }}"  title="View more">
								<a href="{{ url('/admin_assess_details/'.$invitation->unique_id)}}" style="line-height: 0;">
									<i class="fa fa-eye" aria-hidden="true" style="color: white; font-size:22px;"></i>
								</a>
							  </div>
	
							 </div>
						  </div>
						</div>
	
						 @endforeach
					 @endif 
						</div>
						
						 {{-- Confirmed assessment section --}}
						<div class="tab-pane fade  rounded" id="confirmed_assessment" role="tabpanel" aria-labelledby="v-pills-messages-tab">
						  @if (count($confirmed_ass) <= 0)
						  <h3 class="nothingTxt">Nothing to show</h3>
						@else
						 @foreach ($confirmed_ass as $invitation)
						 <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
						  <div class="design_card_new">
							<div class="d-flex">
								@if (($invitation->user->profilepic)!=null)
								   <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
								@else
								  <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
								 @endif
							  <div class='n_c_container'>
								<h5 class="d_name_text">{{ucfirst($invitation->user->name)}}</h5>
								<p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
							  </div>
							</div>
	
						  <table id="n_desing_tbl">
							  @if($doctor_category_id !== $invitation->invitee_category_id)
								<tr>
								<th>Category:</th>
								<td>{{ $invitation->category->category_name }}</td>
								</tr>
							  @endif
							 
							  <tr>
							  <th>Date:</th>
							  <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
							  </tr>
							  <tr>
							  <th>Due date:</th>
							  <td>{{date('d F Y', strtotime($invitation->duedate ))}}
							  </td>
							  </tr>
		
							  <tr>
							  <th>Type:</th>
							  <td>{{ucfirst($invitation->appointment_type)}}</td>
							  </tr>
							 </table>
		
							 <div class="button_containter">
							 
							  <div class="rountedBtn" style="background:#546E7A !important;" data-id="{{ $invitation->id }}"  title="View more">
								<a href="{{ url('/admin_assess_details/'.$invitation->unique_id)}}" style="line-height: 0;">
								   <span class="material-icons" style="color: white; font-size:22px;">visibility</span>
								</a>
							  </div>
	
							 </div>
						  </div>
						</div>
	
						 @endforeach
					 @endif 
						</div>
						
						 {{-- Completed assessment section --}}
						<div class="tab-pane fade  rounded " id="completed_assessment" role="tabpanel" aria-labelledby="v-pills-settings-tab">
						  @if (count($completed_ass) <= 0)
						  <h3 class="nothingTxt">Nothing to show</h3>
						@else
						 @foreach ($completed_ass as $invitation)
						 <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
						  <div class="design_card_new">
							<div class="d-flex">
								@if (($invitation->user->profilepic)!=null)
								   <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
								@else
								  <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
								 @endif
							  <div class='n_c_container'>
								<h5 class="d_name_text">{{ucfirst($invitation->user->name)}}</h5>
								<p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
							  </div>
							</div>
	
						  <table id="n_desing_tbl">
							  @if($doctor_category_id !== $invitation->invitee_category_id)
								<tr>
								<th>Category:</th>
								<td>{{ $invitation->category->category_name }}</td>
								</tr>
							  @endif
							 
							  <tr>
							  <th>Date:</th>
							  <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
							  </tr>
							  <tr>
							  <th>Due date:</th>
							  <td>{{date('d F Y', strtotime($invitation->duedate ))}}
							  </td>
							  </tr>
		
							  <tr>
							  <th>Type:</th>
							  <td>{{ucfirst($invitation->appointment_type)}}</td>
							  </tr>
							 </table>
		
							 <div class="button_containter">
							 
							  <div class="rountedBtn" style="background:#546E7A !important;" data-id="{{ $invitation->id }}"  title="View more">
								<a href="{{ url('/admin_assess_details/'.$invitation->unique_id)}}" style="line-height: 0;">
									<i class="fa fa-eye" aria-hidden="true" style="color: white; font-size:22px;"></i>
								</a>
							  </div>
	
							 </div>
						  </div>
						</div>
	
						 @endforeach
					 @endif 
						</div>
	
						 {{-- Expired assessment section --}}
						<div class="tab-pane fade  rounded" id="expired_assessment" role="tabpanel" aria-labelledby="v-pills-settings-tab">
						 
							@if (count($expired_ass) <= 0)
							  <h3 class="nothingTxt">Nothing to show</h3>
							@else
							 @foreach ($expired_ass as $invitation)
							 <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
							  <div class="design_card_new">
								<div class="d-flex">
									@if (($invitation->user->profilepic)!=null)
									   <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
									@else
									  <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
									 @endif
								  <div class='n_c_container'>
									<h5 class="d_name_text">{{ucfirst($invitation->user->name)}}</h5>
									<p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
								  </div>
								</div>
	
							  <table id="n_desing_tbl">
								  @if($doctor_category_id !== $invitation->invitee_category_id)
									<tr>
									<th>Category:</th>
									<td>{{ $invitation->category->category_name }}</td>
									</tr>
								  @endif
								 
								  <tr>
								  <th>Date:</th>
								  <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
								  </tr>
								  <tr>
								  <th>Due date:</th>
								  <td>{{date('d F Y', strtotime($invitation->duedate ))}}
								  </td>
								  </tr>
			
								  <tr>
								  <th>Type:</th>
								  <td>{{ucfirst($invitation->appointment_type)}}</td>
								  </tr>
								 </table>
			
								 <div class="button_containter">
								 
								  <div class="rountedBtn" style="background:#546E7A !important;" data-id="{{ $invitation->id }}"  title="View more">
									<a href="{{ url('/admin_assess_details/'.$invitation->unique_id)}}" style="line-height: 0;">
										<i class="fa fa-eye" aria-hidden="true" style="color: white; font-size:22px;"></i>
									</a>
								  </div>
	
								 </div>
							  </div>
							</div>
	
							 @endforeach
						 @endif 
						  
						</div>
	
						 {{-- Rejected assessment section --}}
						<div class="tab-pane fade  rounded" id="rejected_assessment" role="tabpanel" aria-labelledby="v-pills-settings-tab">
						  @if (count($reject_ass) <= 0)
						  <h3 class="nothingTxt">Nothing to show</h3>
						@else
						 @foreach ($reject_ass as $invitation)
						 <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
						  <div class="design_card_new">
							<div class="d-flex">
								@if (($invitation->user->profilepic)!=null)
								   <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
								@else
								  <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
								 @endif
							  <div class='n_c_container'>
								<h5 class="d_name_text">{{ucfirst($invitation->user->name)}}</h5>
								<p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
							  </div>
							</div>
	
						  <table id="n_desing_tbl">
							  @if($doctor_category_id !== $invitation->invitee_category_id)
								<tr>
								<th>Category:</th>
								<td>{{ $invitation->category->category_name }}</td>
								</tr>
							  @endif
							 
							  <tr>
							  <th>Date:</th>
							  <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
							  </tr>
							  <tr>
							  <th>Due date:</th>
							  <td>{{date('d F Y', strtotime($invitation->duedate ))}}
							  </td>
							  </tr>
		
							  <tr>
							  <th>Type:</th>
							  <td>{{ucfirst($invitation->appointment_type)}}</td>
							  </tr>
							 </table>
		
							 <div class="button_containter">
							 
							  <div class="rountedBtn" style="background:#546E7A !important;" data-id="{{ $invitation->id }}"  title="View more">
								<a href="{{ url('/admin_assess_details/'.$invitation->unique_id)}}" style="line-height: 0;">
									<i class="fa fa-eye" aria-hidden="true" style="color: white; font-size:22px;"></i>
								</a>
							  </div>
	
							 </div>
						  </div>
						</div>
	
						 @endforeach
					 @endif 
						</div>
	
						 {{-- Cancelled assessment section --}}
						<div class="tab-pane fade  rounded" id="cancelled_assessment" role="tabpanel" aria-labelledby="v-pills-settings-tab">
						  @if (count($cancelled_ass) <= 0)
						  <h3 class="nothingTxt">Nothing to show</h3>
						@else
						 @foreach ($cancelled_ass as $invitation)
						 <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
						  <div class="design_card_new">
							<div class="d-flex">
								@if (($invitation->user->profilepic)!=null)
								   <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
								@else
								  <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
								 @endif
							  <div class='n_c_container'>
								<h5 class="d_name_text">{{ucfirst($invitation->user->name)}}</h5>
								<p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
							  </div>
							</div>
	
						  <table id="n_desing_tbl">
							  @if($doctor_category_id !== $invitation->invitee_category_id)
								<tr>
								<th>Category:</th>
								<td>{{ $invitation->category->category_name }}</td>
								</tr>
							  @endif
							 
							  <tr>
							  <th>Date:</th>
							  <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
							  </tr>
							  <tr>
							  <th>Due date:</th>
							  <td>{{date('d F Y', strtotime($invitation->duedate ))}}
							  </td>
							  </tr>
		
							  <tr>
							  <th>Type:</th>
							  <td>{{ucfirst($invitation->appointment_type)}}</td>
							  </tr>
							 </table>
		
							 <div class="button_containter">
							 
							  <div class="rountedBtn" style="background:#546E7A !important;" data-id="{{ $invitation->id }}"  title="View more">
								<a href="{{ url('/admin_assess_details/'.$invitation->unique_id)}}" style="line-height: 0;">
									<i class="fa fa-eye" aria-hidden="true" style="color: white; font-size:22px;"></i>
								</a>
							  </div>
	
							 </div>
						  </div>
						</div>
	
						 @endforeach
					 @endif 
						</div>
	
						 {{-- Archived assessment section --}}
						<div class="tab-pane fade  rounded" id="archived_assessment" role="tabpanel" aria-labelledby="v-pills-settings-tab">
						  @if (count($archived_ass) <= 0)
						  <h3 class="nothingTxt">Nothing to show</h3>
						@else
						 @foreach ($archived_ass as $invitation)
						 <div class="col-md-12" style="padding: 0px; padding-left: 10px;">
						  <div class="design_card_new">
							<div class="d-flex">
								@if (($invitation->user->profilepic)!=null)
								   <img class="n_images" src="{{ url('uploads/profilepics/lawyer/'.$invitation->user->profilepic) }}" />
								@else
								  <img class="n_images" src="{{asset('img/no_image.jpg')}}" />
								 @endif
							  <div class='n_c_container'>
								<h5 class="d_name_text">{{ucfirst($invitation->user->name)}}</h5>
								<p class="firm_name">{{ucfirst($invitation->user->lawyer->lawfirmname)}}</p>
							  </div>
							</div>
	
						  <table id="n_desing_tbl">
							  @if($doctor_category_id !== $invitation->invitee_category_id)
								<tr>
								<th>Category:</th>
								<td>{{ $invitation->category->category_name }}</td>
								</tr>
							  @endif
							 
							  <tr>
							  <th>Date:</th>
							  <td>{{date('d F Y', strtotime($invitation->invitation_date ))}} |  {{ $invitation->timeslot }}</td>
							  </tr>
							  <tr>
							  <th>Due date:</th>
							  <td>{{date('d F Y', strtotime($invitation->duedate ))}}
							  </td>
							  </tr>
		
							  <tr>
							  <th>Type:</th>
							  <td>{{ucfirst($invitation->appointment_type)}}</td>
							  </tr>
							 </table>
		
							 <div class="button_containter">
							 
							  <div class="rountedBtn" style="background:#546E7A !important;" data-id="{{ $invitation->id }}"  title="View more">
								<a href="{{ url('/admin_assess_details/'.$invitation->unique_id)}}" style="line-height: 0;">
									<i class="fa fa-eye" aria-hidden="true" style="color: white; font-size:22px;"></i>
								</a>
							  </div>
	
							 </div>
						  </div>
						</div>
	
						 @endforeach
					 @endif 
						</div>
	
					</div>
				</div>
			</div>
		</div>
	</section>
	
	
		</div>

	

</div>

@endsection

