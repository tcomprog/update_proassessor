
@extends("includes.email_container")
@section("content")
            <div>

                    <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Hello {{{ ucfirst($user->name) }}},</h3>

                    <p style="font-size: 14px;line-height: 17px;">You have received a new message from <b>{{ ucfirst($sender->name) }}</b> from ProAssessors. Please login to your account to check the message.</p>

                    <p style="font-size: 14px;line-height: 17px;">Please click below link to login to ProAssessors</p>

                    <p><a href="{{ $frontend_url }}">{{$frontend_url}}</a></p>

                    <br/>

                    <br/>

                    <br/>

                    <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
                        <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 

                    </div>  

               </div>

            </div>

 @endsection
