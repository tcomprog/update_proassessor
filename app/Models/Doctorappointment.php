<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctorappointment extends Model
{
    use HasFactory;
	
	protected $table = 'doctorappointments';
}
