<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Models\user_invitations;
use App\Models\Patient;
use App\Mail\Report_Due_Alert;
use App\Models\User;
use App\Models\Nofication;
use App\Helpers\Helper;


class ReportDueDateAlert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'corm:daily_alert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify doctor about report due date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $subject = "Report deadline alert";
        $report = user_invitations::select('id','inviter_user_id','unique_id','patient_user_id','emila_not_status','invitee_user_id','invitee_user_id','invitee_category_id','duedate')
         ->where('status','active')
        ->where("emila_not_status",'<','2')
        ->where("is_patient_accepted",'yes')
        ->where("report_status",'0')
        ->whereRaw('Date(duedate) = CURDATE()')
        ->get();
    
        echo count($report);
        
        foreach($report as $row){
            $patient = Patient::find($row->patient_user_id);
            $doctor = User::find($row->invitee_user_id);
        
            $des = Helper::c_strings()["Report_due_date"];
			$des = str_replace("@patient",$patient->name,$des);

            $notif = new Nofication();
			$notif->url = "invitedetail/".$row->unique_id;
			$notif->description = $des;;
			$notif->from_id = $row->inviter_user_id;
			$notif->to_id = $row->invitee_user_id;
			$notif->save();
    
            $report = user_invitations::find($row->id);
            $report->emila_not_status = $row->emila_not_status + 1;
            $report->save();
    
            $data = ['doctor'=>$doctor->name,'patient'=>$patient->name,'subject'=>$subject];
           \Mail::to($doctor->email,$doctor->name)->send(new Report_Due_Alert($data));
        }
        return 0;
    }
}
