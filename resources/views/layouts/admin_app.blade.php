<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{url('uploads/admin/favicon.png')}}" rel="icon">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title><?php $sitedata1 = Helper::Sitedetails();   echo $sitedata1->site_title; ?> </title>
    <!-- Scripts -->
    <script src="{{ asset('js/admin_app.js') }}"  defer></script>	<!--defer-->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/admin_app.css') }}" rel="stylesheet">
    @yield('pageHeaderStyles')
    <link href="{{ asset('css/admin_main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin_new_script.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body>

    <div class="toast-container position-fixed top-20 end-0 p-3">
        <div id="liveToast" class="toast align-items-center text-bg-success border-0" role="alert" aria-live="assertive" aria-atomic="true" data-bs-delay='2000'>
            <div class="d-flex">
              <div id="toastContent" class="toast-body">
                Hello, world! This is a toast message.
              </div>
              <button type="button" class="btn-close btn-close-white me-2 m-auto" style="margin-right: 10px !important;" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
          </div>
    </div>

    @php 
        $action = request()->route()->getAction(); 
        $route = class_basename($action['controller']);
    @endphp
<script type="text/javascript">
        var base_url = "{{ url('/') }}/";
        var URLRoute = "{{$route}}";
    </script>
	<?php  
		$routeName = Route::currentRouteName();
		?>
    <div id="app">
    
    <div class="wrapper">
        <!-- Sidebar Holder -->
        @if(Auth::check())
           @include("layouts.admin_sliderbar")
        @endif


        <!-- Page Content Holder -->
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-tunedin">
                <div class="container-fluid">
                @if(Auth::check())
                    <button type="button" id="sidebarCollapse" class="navbar-btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fa fa-align-justify"></i>
                    </button>
                    @else
                    <h3><img src="{{url('uploads/admin/logo.svg')}}" alt="" width="150"></h3>
                    @endif
                    <div class="collapse navbar-collapse admindropdow" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto navbar-right">
                        @if(Auth::check())
                            <li>
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    
                                    <a class="dropdown-item" href="{{ url('admin/change-password') }}">
                                        {{ __('Change Password') }}
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        {{ __('Settings') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('admin_logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('admin_logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            <li>
                                <a href="#" class="header-power-off-link" onclick="event.preventDefault(); document.getElementById('poweroff-icon-form').submit();" class="nav-right-signout-icon">
                                                     <i class="fa fa-power-off"></i> </a>
                                    <form id="poweroff-icon-form" action="{{ route('admin_logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
        <main class="main">
            @yield('content')
        </main>
        
    </div>
   
    </div>
     <!-- Footer -->
 <footer id="footer">
  <div class="container-fluid cf">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="copyright">
          ©{{ date('Y')}} <u><span><?php $sitedata1 = Helper::Sitedetails();   echo $sitedata1->site_title; ?></span></u> | All Rights Reserved.
        </div>
      </div>
      
    </div>
  </div>
</footer>
<!-- Footer -->
    @yield('pageFooterScripts')
    @include("includes.admin_ajax_call");
    <script src="{{ asset('js/admin_scripts.js') }}"  defer></script><!----->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</body>
</html>


<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.js"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<script src="{{asset('js/custom.js')}}"></script>


<script>
      $(document).ready(function () {
        $.noConflict();
        $('#datatbl11').DataTable({
            "paging": true,
             "ordering":false
        });

        $( "#datepicker" ).datepicker(
            {
                dateFormat: "yy-mm-dd",
                minDate:1
            }
        );

      

        $("#changeDateRequestBtn").on("click",function(){
            $("#s_msg").hide();
            $("#r_msg").hide();
            $("#cng_loader").hide();

           var id = $(this).data("id");
           var date = $("#datepicker").val();

           if(isEmpty(date)){
             $("#s_msg").hide();
             $("#r_msg").show();
             $("#r_msg").text("Please select date first!");
           }
           else{
                var formData = new FormData();
                formData.append("id",id)
                formData.append("date",date);

                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                  $("#cng_loader").show();
                  $("#changeDateRequestBtn").attr("disabled", true);
                  $.ajax({
                    url: "{{url('changeDateRequest')}}",
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                      if(data.status == '1'){
                           $("#changeDateRequestBtn").attr("disabled", false);
                           $("#r_msg").hide();
                           $("#s_msg").show();
                           $("#s_msg").text("Date changed successfully!");
                           $("#cng_loader").hide();
                           $("#datechangeSection").hide();
                       }
                       else{
                           $("#s_msg").hide();
                           $("#cng_loader").hide();
                           $("#changeDateRequestBtn").attr("disabled", false);
                           $("#r_msg").show();
                           $("#r_msg").text("Something went try again later!");
                       }
                    }
                    });
           }
        });
    });
</script>

<script src="{{asset('js/admin_new_script.js')}}"></script>



@if (isset($_GET["update_status"]) && $_GET["update_status"] == "1")
<script>
      msg_toast("Compnay profile update successfully",0);
  </script> 
@endif

@if (isset($_GET["deleted_contact_status"]) && $_GET["deleted_contact_status"] == "1")
<script>
      msg_toast("Request deleted successfully",0);
  </script> 
@endif

<script>

    
    $(document).on("click","#send_email_btn",function(){
        var email = $("#send_email_to_email").val();
        var subject = $("#send_email_subject").val();
        var message = $("#mainMessage").val();
        let error = 0;
        
        if(isEmpty(email)){
           msg_toast("Please enter to email address",1);
           error = 1;
           return;
        }
        else if(validateEmail(email) == false){
           msg_toast("Invalid email address",1);
           error = 1;
           return;
        }

        if(isEmpty(subject)){
           msg_toast("Please enter to subject",1);
           error = 1;
           return;
        }

        if(isEmpty(message)){
           msg_toast("Please enter to message",1);
           error = 1;
           return;
        }

        if(error == 0){
            $("#loading_send_mail").show();
            $("#send_email_btn").hide();
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
            $.ajax({
				type:'POST',
				url:"<?= url('/admin/sendEmailToUser') ?>",                
				dataType:'json',
                data:{
                    to:email,
                    subject:subject,
                    message:message
                },
			   	success:function(response) {
                    if(response.status == 'success') {
                    $("#loading_send_mail").hide();
                    $("#send_email_btn").show();
                    $("#send_email_to_email").val('');
                    $("#send_email_subject").val('');
                    $("#mainMessage").val('')
                    msg_toast("Message sent successfully");
                  }
				},
                error:function(e){
                    msg_toast("Something went wrong try again",1);
                    $("#loading_send_mail").hide();
                    $("#send_email_btn").show();
                }
			});	
        }

    });


$(document).on("click",".deleteInput",function(){
    var id = $(this).data("feat");
    $("#feat_"+id).remove();
});
</script>


@if (isset($_GET["packages_status"]) && $_GET["packages_status"] == "1")
<script>
      msg_toast("Package updated successfully",0);
  </script> 
@endif


<script>
    $(document).on('click','#changeOfferBtn',function(){
       let loader = $("#changeOfferLoader");
       let monthly_price = $("#monthly_price");
       let yearly_price = $("#yearly_price");
       let submit_btn = $("#changeOfferBtn");
       let delet_btm = $("#deleteOffer");

       if(isEmpty(monthly_price.val()) || isEmpty(yearly_price.val())){
          msg_toast("Please fill the form correctly",2);
       }
       else{
        if(monthly_price.val() <= 0 || yearly_price <= 0){
            msg_toast("Values should be greate than zero",1);
        }
        else{
        loader.show();
        submit_btn.hide();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
            $.ajax({
				type:'POST',
				url:"<?= url('/assignSpecailPrice') ?>",                
				dataType:'json',
                data:{
                    monthly:monthly_price.val(),
                    yearly:yearly_price.val(),
                    clincID:$("#clinic_sel_id").val()
                },
			   	success:function(response) {
                 if(response.status == 'success') {
                    loader.hide();
                    submit_btn.show();
                    delet_btm.show();
                    msg_toast("Action performed successfully!");
                  }
				},
                error:function(e){
                    msg_toast("Something went wrong try again",1);
                    loader.hide();
                    submit_btn.show();
                }
			});
        }	
       }

    });

    $(document).on('click','#deleteOffer',function(){
        let loader = $("#changeOfferLoader");
        let monthly_price = $("#monthly_price");
        let yearly_price = $("#yearly_price");
        let submit_btn = $("#changeOfferBtn");
        let delet_btm = $("#deleteOffer");

        loader.show();
        delet_btm.hide();
        submit_btn.hide();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
        $.ajax({
				type:'POST',
				url:"<?= url('/deleteSpecialOffer') ?>",                
				dataType:'json',
                data:{
                    clincID:$("#clinic_sel_id").val()
                },
			   	success:function(response) {
                 if(response.status == 'success') {
                    loader.hide();
                    submit_btn.show();
                    monthly_price.val('0');
                    yearly_price.val('0');
                    msg_toast("Offer deleted successfully");
                  }
				},
                error:function(e){
                    msg_toast("Something went wrong try again",1);
                    loader.hide();
                    submit_btn.show();
                    delet_btm.show();
                }
			});	
    });
</script>


