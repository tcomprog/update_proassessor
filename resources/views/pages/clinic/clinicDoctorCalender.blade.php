@extends('layouts.clinic-layout') 
@section('content')	
<link href="{{ asset('css/clinic_dashboard.css') }}" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="{{ asset('css/doctor_dashboard.css') }}" rel="stylesheet">

<style>
	.ui-datepicker-trigger{
		background: transparent !important;
	}
	.n_links{
		font-weight: bold;
		color:#00396b;
	}
	.n_links:hover{
		text-decoration: none;
	}
	.calendar-sidebar{
		background-color: #141a45 !important;
	}
	.month.active-month{
		background-color: #526082 !important;
	}
	.calendar-sidebar>.month-list>.calendar-months>li:hover{
		background-color: #526082 !important;
	}
	#sidebarToggler,#eventListToggler{
		background-color: #141a45 !important;
	}
	th[colspan="7"]{
		color: #141a45 !important;
	}
	.evo-calendar{
		z-index: 10 !important;
	}
</style>

<div class="container dashboardContainer mb-5">
    <h3 class="inv-content-header">Calendar - {{$users->name}}</h3>

    <div class="row">
      <div class="col-12">
       
        @if(Session::has('message'))
           <div class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</div>
         @endif	

        <div class="search_sec">
            <div id="calendar"></div>
        </div>

      </div>
    </div>
</div>

<input id="selectedDoctorID" type="hidden" value="{{$users->id}}">

<div class="modal" id="block-calendar-modal" tabindex="-1">
	<div class="modal-dialog">
	  <div class="modal-content">
	  <form action="{{ url('doctor/block-dates') }}" method="post" id="block-date-calendar-form">
		<div class="modal-header">
		  <h5 class="modal-title">Block/Un Block Dates</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
			<p class="text-right"><input type="checkbox" name="select_all" id="block_select_all" value="1"/> <label for="block_select_all" class="cursor-pointer">Select All</label></p>
		<div id="mdp-demo"></div>
		@csrf
		<input type="hidden" name="dates" id="blocked_dates" />
		<input type="hidden" name="selected_date" id="selected_date" />
		<input name="doctorID" type="hidden" value="{{$users->id}}">
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		  <button type="submit" class="btn btn-primary">Block Dates</button>
		</div>
		</form>
	  </div>
	</div>
  </div>

@php
    $newList1=json_encode($newList);
@endphp

@endsection


@section('footerScripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
function addCanderNote(inp,inst){
	console.log(inp)
	console.log(inst)
	console.log($('.ui-datepicker-div'));
	$(`<div class="calender-note">
	<p><span class="yellow"></span> - Booked</p><p><span class="red"></span> - Report Due date</p>	
	</div>`).insertAfter('.ui-datepicker-calendar')
}

	$(document).ready(function() {

		var dueDateArr = <?=$dueDateArr?>;
		var invitationDateArr = <?=$invitationDateArr?>;

	var dueDates = {};
	var invitationDates = {};

		if(dueDateArr.length){
			for(var i =0; i<dueDateArr.length; i++){
				dueDates[new Date(dueDateArr[i])] = new Date(dueDateArr[i]).toString();
			}
		}
		
		if(invitationDateArr.length){
			for(var j =0; j<invitationDateArr.length; j++){
				invitationDates[new Date(invitationDateArr[j])] = new Date(invitationDateArr[j]).toString();
			}
		}

$('.my-calendar-input').datepicker({
  showOn: 'button',
  buttonText:`<div class="rountedBtn " style="background: #323B62 !important"  data-id="@isset($invitation) {{  $invitation->id }} @endisset"  title="Calendar">
			 <span class="material-icons" style="color: white; font-size:27px;">calendar_month </span>
			 </div>`,
  options: function(dateText, inst) { inst.show() },
  beforeShow:function(inp, inst){
	setTimeout(function () {
		addCanderNote(inp,inst);
	},500)
  },
  beforeShowDay: function(date) {
            var dueDate = dueDates[date];
            var inviteDate = invitationDates[date];
            if (dueDate) {
                return [true, "highlight-due-date", dueDate];
            }else if(inviteDate){
                return [true, "highlight-invite-date", inviteDate];
			}
            else {
                return [true, '', ''];
            }
        }
		
});

		$('#dashboard-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var target = $(e.target).attr("href")
			var activeTab = target.replace('#','');
    		var baseURL = '{{url("doctor-dashboard") }}';  
			window.history.pushState({}, null, baseURL+'/'+activeTab);
		});
		
		$('.invitation-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var activeTab = $(e.target).attr("href")
		var baseURL = '{{url("doctor-dashboard/my-invitations") }}'; 
			window.history.pushState({}, null, baseURL+'/'+activeTab);
		});


		var hash = window.location.hash;
		if(hash.length > 0) {
			$('.invitation-tabs .nav-item .nav-link').removeClass('active');
			$('.invitation-tabs #invitations-tab-content .tab-pane').removeClass('active');
			$('.invitation-tabs .nav-item .nav-link[href="'+hash+'"]').addClass('active');
			$('.invitation-tabs #invitations-tab-content '+hash).addClass('active');
		}
		
});

$("#choooseAllTask").on("change",function(){
   if($('#choooseAllTask:checkbox:checked').length > 0){
	  $('.expiredTaskCheckbox').prop('checked', true); 
   }else{
	$('.expiredTaskCheckbox').prop('checked', false); 
   }
});


$("#choooseAllConfirm").on("change",function(){
   if($('#choooseAllConfirm:checkbox:checked').length > 0){
	  $('.confirmedTaskCheckbox').prop('checked', true); 
   }else{
	$('.confirmedTaskCheckbox').prop('checked', false); 
   }
});

</script>

@endsection