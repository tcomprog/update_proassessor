<!DOCTYPE html>
<html lang="en">
<head>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Pro-assessors</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet"> 

    <style>
      *{
       font-family: 'Montserrat', sans-serif;
      }

     .abtopsider{
        background: #20215E;
        display: flex;
        margin-top: 30px;
        margin-bottom: 30px;
        align-items: center;
        padding: 20px;
        width: 45%;
        float: right;
        border-top-left-radius: 100px;
        border-bottom-left-radius: 100px;
        padding-left: 50px;
     }

     .abtopsider > p{
         color: white;
         margin: 0px;
         padding-left: 30px;
         padding-right: 15px;
     }
     
     .abtopcontainer{
         display: flex;
         justify-content: end;
     }

     .abbottomcontainer{
        margin-top: auto;
        justify-content: center;
        padding-block: 20px;
     }

     .abcontentContainer{
          display: flex;
          width: 90%;
          align-self: center;
          flex-direction: column;
          padding-bottom: 20px;
     }
     .abinsidecontainer{
       display: flex;
        width: auto;
        flex-direction: row;
        justify-content: center;
     }
     .abfirstslide{
       background: #20215E;
       padding: 20px 30px;
       border: 3.59777px solid #20215E;
       border-radius: 10.7933px;
       border-top-right-radius: 0px;
       border-bottom-right-radius: 0px;
       margin-right: -64px;
       padding-right: 55px;
     }
     .absecondslide{
       padding-left: 35px;
       padding-right: 35px;
       border: 3.59777px solid #20215E;
       border-radius: 10.7933px;
       border-top-left-radius: 0px;
       border-bottom-left-radius: 0px;
       border-left: 0px;
       margin-left: -75px;
       padding-left: 95px;
      justify-content: center;
      display: flex;
      flex-direction: column;
     }

     .abiconContainer > td > p{
         margin: 0px;
         padding-left: 10px;
         color:#20215E;
     }

     .abiconContainer > td > img{
         height: 18px;
     }

     .abbottombox{
        width: 120px;
        height: 120px;
        background: #FFFFFF;
        border: 3.59777px solid #20215E;
        border-radius: 10.7933px;
        align-self: center;
     }

     .abtopImg{
       height: 45px;
     }

     .abfirstslide > h2,h4{
       color: white;
       margin-bottom: 0px;
     }

     .abiconContainer{
        margin: 0px;
        height: auto;
        display: flex;
        align-items: center;
        margin-bottom: 10px;
     }

     @media only screen and (max-width: 500px) {
 
       .abtopsider{
           margin-top: 10px;
           margin-bottom: 20px;
           padding: 15px 20px 15px 30px;
           margin-left: 30px;
           width:80%;
      }

      .abtopImg{
          height: 30px;
      }

      .abinsidecontainer{
       display: flex;
        width: 80%;
        flex-direction: column;
        justify-content: center;
     }

     .abfirstslide{
       margin-right: 0px;
       padding-right: 0px;
       width: 100% !important;
       border: 3.59777px solid #20215E;
       border-radius: 10.7933px;
       padding: 20px;
       align-self: center;
       margin-bottom: -64px;
       padding-bottom: 65px;
       border-bottom-left-radius: 0px;
       border-bottom-right-radius: 0px;
     }

     .absecondslide{
       margin-left: 0px;
       padding-left: 0px;
       border: 3.59777px solid #20215E;
       border-radius: 10.7933px;
       width: 100%;
       align-self: center;
       padding-left: 10px !important;
       padding-bottom: 10px;
       padding-right: 0px;
       margin-top: -63px;
       padding-top: 80px;
       border-top: 0px;
       border-top-left-radius: 0px;
       border-top-right-radius: 0px;
       padding-right: 20px;
     }

  }

  table{
    width:100%; 
  }
</style>  

{{-- ============================================================================= --}}
{{-- New Design --}}
<style>
   body{
     margin:0px; padding:0px;
   }

   .topSectionContainer{
        background: #20215E;
        margin-top: 30px;
        margin-bottom: 30px;
        padding: 20px;
        width: 45%;
        float: right;
        border-top-left-radius: 100px;
        border-bottom-left-radius: 100px;
        padding-left: 50px;
        padding-top: 13px;
        padding-bottom: 13px;
   }

   .topSectionHeading{
       color: white;
       margin: 0px;
       padding-left: 30px;
       padding-right: 15px;
       font-size: 16px;
       font-weight: bold;
   }

   .bottomfirstSlide{
       background: #20215E;
       border: 3.59777px solid #20215E;
       border-radius: 10.7933px;
       border-top-right-radius: 0px;
       border-bottom-right-radius: 0px;
       padding-left: 30px;
       padding-right: 35px;
       height: 220px;
       overflow: hidden;
       width: 220px;
     }

  .bottomfirstSlide > h2,h4{
       color: white;
       margin-bottom: 0px;
  }
    
  .bottomMiddleBox{
    width: 120px;
    height: 120px;
    background: #FFFFFF;
    border: 3.59777px solid #20215E;
    border-radius: 10.7933px;
    align-self: center;
  }

  .bottomThirdBox{
       padding-left: 35px;
       padding-right: 35px;
       border: 3.59777px solid #20215E;
       border-radius: 10.7933px;
       border-top-left-radius: 0px;
       border-bottom-left-radius: 0px;
       border-left: 0px;
       height: 220px;
       width: 400px;
  }
  .bottom_images{
     width: auto;
     height: auto;
   }

   @media only screen and (max-width: 500px) {
   .bottom_images{
     width: 100% !important;
     height: auto !important;
   }
    .topSectionHeading{
       color: white;
       margin: 0px;
       padding-left: 30px;
       padding-right: 15px;
       font-size: 12px;
       font-weight: bold;
    } 

   .topSectionContainer{
           margin-top: 10px;
           margin-bottom: 20px;
           padding: 15px 20px 15px 30px;
           margin-left: 30px;
           width:80%;
      }
  }

</style>

</head>
<body style="">

  <table cellpadding="0" cellspacing="0" border="0" style="height:100vh;">
      {{-- Top Section --}}
       <tr align="right" style="height: 85px;">
          <td class="topSectionContainer">
              <table>
                <tr>
                  <td><img class="ab-topImg" height="45" src="https://i.ibb.co/0Z6xt8D/logo.png" alt=""></td>
                  <td class="topSectionHeading">Discovering Quality Medical Legal Solutions</td>
                </tr>
              </table>
          </td>
       </tr>

      {{-- Content Section --}}
      <tr align="center">
        <td style="width: 90%;display:block;padding-bottom: 20px;text-align: start;">
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
          <div style="    text-align: center;width: 100%;">
             <img class="bottom_images" src="{{asset("n_img/bottom_email.PNG")}}" alt="">
          </div>
        </td>
      </tr>

      {{-- Bottom Section --}}
      <tr align="center">
          <td>
            <img class="bottom_images" src="{{asset("n_img/bottom_email.PNG")}}" alt="">
          </td>
      </tr>

  </table>



  {{-- <table cellpadding="0" cellspacing="0" border="0" style="height:100vh;">

      <tr>
        <td style="width:100%;">

          <table cellpadding="0" cellspacing="0" border="0">
             <tr align="center">

              <td class="abbottomcontainer">

                <table cellpadding="0" cellspacing="0" border="0">
                  <tr align="center">
                   <td class="abinsidecontainer">

                    <table class="abfirstslide" cellpadding="0" cellspacing="0" border="0" style="width: auto;">
                      <tr>
                       <td class="abfirstslide">
                           
                              <img height="45" src="https://i.ibb.co/0Z6xt8D/logo.png" alt="">
                       
                          <h2>Asjatir Khan Asjir</h2>
                          <h4>CEO & Founder</h4>
                       </td>
                      </tr>
                    </table>

                    <table cellpadding="0" cellspacing="0" border="0" style="width: auto; height:120px; align-self: center !important;">
                      <td>
                        <td class="abbottombox"></td>
                      </td>
                    </table>

                    <table cellpadding="0" cellspacing="0" border="0" style="width: auto;" class="absecondslide">
                         <tr>
                          <td class="absecondslide" style="border: none">

                            <table class="abiconContainer" cellpadding="0" cellspacing="0" border="0" style="width: auto;">
                              
                              <tr class="abiconContainer">
                                <td> <img src="https://i.ibb.co/0Gm2mnN/call.png" alt=""></td>
                                <td><p>+78 8997 9875</p></td>
                              </tr>

                              <tr class="abiconContainer">
                                <td>  <img src="https://i.ibb.co/rmLHYqD/email.png" alt=""></td>
                                <td> <p>asjirkhan@gmail.com</p></td>
                              </tr>

                              <tr class="abiconContainer">
                                <td> <img src="https://i.ibb.co/B2bRKPd/internet.png" alt="">                                </td>
                                <td> <p>https://proassessors.com</p></td>
                              </tr>

                              <tr class="abiconContainer">
                                <td> <img src="https://i.ibb.co/LPwLJvw/maps-and-flags.png" alt=""></td>
                                <td> <p>330 highway 7 east richmond hill</p></td>
                              </tr>

                            </table>

                          </td>
                         </tr>
                    </table>

              </td>
            </tr>
          </table>

              </td>
             </tr>
          </table>
        </td>
    </tr>

  </table> --}}

</body>
</html>