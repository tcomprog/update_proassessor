@extends('layouts.admin_app')

<style>
    .cnt{
        box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
    }

    body {
  margin: 0;
  font-family: 'Ubuntu', sans-serif;
}
section {
  margin: 0 auto; 
  max-width: 660px;
  padding: 0 20px;
}

.ribbon {
  width: 48%;
  min-height: 50px;
  position: relative;
  float: left;
  margin-bottom: 20px;
  background: url(https://html5book.ru/wp-content/uploads/2015/10/snow-road.jpg);
  background-size: cover;
  text-transform: uppercase;
  color: white;
}
.ribbon:nth-child(even) {
  margin-right: 4%;
}
@media (max-width: 500px) {
  .ribbon {
    width: 100%;
  }
  .ribbon:nth-child(even) {
    margin-right: 0%;
  }
}
.ribbon1 {
  position: absolute;
  top: -6.1px;
  right: 10px;
}
.ribbon3 {
  min-width: 250px;
  height: 50px;
  line-height: 50px;
  padding-left: 15px;
  position: absolute;
  left: -8px;
  top: 20px;
  background: #526082;
}
.ribbon3:before, .ribbon3:after {
  content: "";
  position: absolute;
}
.ribbon3:before {
  height: 0;
  width: 0;
  top: -8.5px;
  left: 0.1px;
  border-bottom: 9px solid grey;
  border-left: 9px solid transparent;
}
.ribbon3:after {
  height: 0;
  width: 0;
  right: -14.5px;
  border-top: 25px solid transparent;
  border-bottom: 25px solid transparent;
  border-left: 15px solid #526082;
}

p{
  color: #000 !important;
}

#transtbl  th{
    padding: 10px 6px;
}
#transtbl  td{
    padding: 10px 6px;
}
#transtbl th{
    color:#20215E;
}

</style>

@php
$options = [
    "1" => "Option 1",
    "2" => "Option 2",
    "3" => "Option 3",
    "4" => "Other"
];
$casetype = [
    "1" => "MVA",
    "2" => " WSIB",
    "3" => "Slip and fall",
];
@endphp

@section('content')

<div class="modal fade" data-backdrop="static" id="assignSprice" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Define Special Offer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <div class="form-group">
          <label for="monthly_price">Monthly<sup class="text-danger">*</sup></label>
          <input type="number" class="form-control" id="monthly_price" aria-describedby="emailHelp" value="{{$monthly}}">
        </div>

        <div class="form-group">
          <label for="yearly_price">Yearly<sup class="text-danger">*</sup></label>
          <input type="number" class="form-control" id="yearly_price" aria-describedby="emailHelp" value="{{$yearly}}">
        </div>

        <input id="clinic_sel_id" type="hidden" value="{{$clinic->id}}">

      </div>
      <div class="modal-footer">
        @if ($monthly > 0)
           <button id="deleteOffer" type="button" class="btn btn-danger mr-2">Delete</button>
        @endif
        <button id="changeOfferBtn" type="button" class="btn btn-success">Save changes</button>
        <img style="display:none;" id="changeOfferLoader" width="30px" src="{{asset('img/loading.gif')}}" alt="">
      </div>
    </div>
  </div>
</div>

<div class="container-fluid bg-white rounded cnt pb-4 ml-2">

    <div class="row align-items-end">

        @if (!empty($clinic->profilepic) && $clinic->profilepic != 'null')
        <img src="{{asset("uploads/profilepics/clinic/".$clinic->profilepic)}}" alt="" class="img-thumbnail ml-1 mt-1" style="width:100px !important;">
        @endif

        <div class="col">
          <button class="mt-2 btn btn-warning" data-toggle="modal" data-target="#assignSprice" style="width:200px">Assign Special Price</button>
        </div>
       

        <div class="ribbon">
            <span class="ribbon3">Clinic Details</span>
        </div>

        <div class="container-fluid my-3">

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Name</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$clinic->name}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Phone</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$clinic->phone}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Email</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$clinic->email}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Address</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$clinic->business_address}}</p></div>
          </div>
       
        </div>


        @if($subscription != null)
        <div class="ribbon">
          <span class="ribbon3">Subscription</span>
        </div>

        <div class="container-fluid my-3">

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Package</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{ucfirst($subscription->package)}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Amout</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">$CAD {{$subscription->amount}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">From Date</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{date('d-m-Y',strtotime($subscription->from_date))}}</p></div>
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">To Date</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{date('d-m-Y',strtotime($subscription->to_date))}}</p></div>
          </div>
          
          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Status</p></div>
            @if($subscription->status == 1)
               <div class="col-md-10 col-sm-8"><p class="mb-0 text-success">Active</p></div>
            @else
               <div class="col-md-10 col-sm-8"><p class="mb-0 text-danger">Expired</p></div>
            @endif
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-4"><p class="mb-0 text-dark font-weight-bold">Transaction ID</p></div>
            <div class="col-md-10 col-sm-8"><p class="mb-0">{{$subscription->transaction_id}}</p></div>
          </div>
       
        </div>
      
        <div class="ribbon">
          <span class="ribbon3">Transactions</span>
        </div>

        <div class="container-fluid my-3">
          <div class="row">
            <div class="col-12">
              <table id="transtbl" class="row-border hover cell-border" style="width:100%">
                <thead>
                    <tr class="border-bottom border-top my-2">
                        <th>ID</th>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Payment ID</th>
                        <th>Package</th>
                        <th>From Date</th>
                        <th>To Date</th>
                    </tr>
                </thead>
        
                <tbody>
                    @foreach ($sub_tran as $row)
                        <tr class="border-bottom my-2">
                            <td>{{$row->id}}</td>
                            <td>{{date("d-m-Y",strtotime($row->date))}}</td>
                            <th>$CAD {{$row->amount}}</th>
                            <td>{{$row->payment_id}}</td>
                            <th>{{ucfirst($row->package)}}</th>
                            <td>{{date("d-m-Y",strtotime($row->from_date))}}</td>
                            <th>{{date("d-m-Y",strtotime($row->to_date))}}</th>
                        </tr> 
                    @endforeach
                </tbody>
            </table>
            </div>
          </div>
        </div>

        @endif


        <div class="row">
    
            <div class="ribbon">
                <span class="ribbon3">Clinic Doctor List</span>
            </div>
    
            <div class="container-fluid my-3">
                <div class="row">
                    <div class="col-12">

                <table id="datatbl11" class="row-border hover cell-border" style="width:100%">
                  <thead>
                      <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>License Number</th>
                      <th>Status</th>
                      <th>Admin status</th>
                      <th></th>
                      </tr>
                  </thead>
      
                  <tbody>
                      @if (count($doctor) > 0)
                        @foreach ($doctor as $row)
                            <tr>
                              <th>{{$row->name}}</th>
                              <td>{{$row->email}}</td>
                              <td>{{$row->phone}}</td>
                              <td>{{$row->doctor->licence_number}}</td>
                              <td>
                                  @if ($row->status == 'active')
                                     <span class="bg-success px-2 rounded">Active</span> 
                                     @else
                                     <span class="bg-warning px-2 rounded">Pending</span> 
                                  @endif
                              </td>
                              <td>
                                  @if ($row->authentication_status == 'active')
                                  <span class="bg-success px-2 rounded">Active</span> 
                                  @else
                                  <span class="bg-warning px-2 rounded">Pending</span> 
                               @endif
                              </td>
                              <td>
                                  <a href ="{{url('/doctor_file')}}/{{$row->id}}" class="btn btn-info btn-sm" title="View profile">
                                      <i class="fa fa-eye" aria-hidden="true"></i>
                                  </a>
                              </td>
                            </tr>
                        @endforeach                    
                      @endif
                  </tbody>
      
                </table>
            </div>
           </div>
            </div>


    </div>


@endsection

