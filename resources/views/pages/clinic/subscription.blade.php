@extends('layouts.default')
@section('content')

<link href="{{ asset('css/about.css') }}?99999999" rel="stylesheet">
<link href="{{ asset('css/subscription.css') }}?99999999" rel="stylesheet">
<link href="{{ asset('css/home.css') }}?99999999" rel="stylesheet">


<style>
.topHomeContainerAbout{
    display: flex;
    flex-direction: column;
    background: url("{{asset('assets/about_banner.jpeg')}}");
}

.sub_card > .header{
    background: url("{{asset('n_img/offer_top.png')}}");
    height: 150px;
    width:100%;
    display: flex;
    justify-content: center;
    align-items: center;
    background-repeat: no-repeat;
    background-size: contain;
    background-position: center;
}

</style>

<section class="topHomeContainerAbout">
<div class="subcontainer">
    @include('includes.nheader')

    <div class="top-banner container">
       <div class="br-slide1">
        <h2 style="font-family: 'DubaiBold' !important; margin-top:30px">Subscriptions</h2>
       </div>
    </div>
</div>
</section>


<div class="container mt-5">
    <div class="row">
        <div class="col-12 pt-4 pb-3 subsciption_heading">
            <h1>Choose Your Plan</h1>
            <h5 class="mt-4">Qorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
            @if ($exp_status == 2)
                <h6 class="mt-2 text-danger">Your {{$package_type}} package has been expired on {{$expire_date}}. In order use the system you need to resubscribe the package</h6>
            @endif
        </div>
    </div>
</div>


<section style="background:#20215E; width:100%;margin:20px 0px 60px 0px;" >
<div class="container-fluid py-5">
    <div class="mainBoxContainer">

         <form class="sub_card p-2" method="POST" action="{{ route('sub_checkout_page') }}">
            @csrf
            <div class="header">
                <h1 class="m-0 text-white pb-3">{{ucfirst($monthly->name)}}</h1>
            </div>

            <input type="hidden" name="amount" value="{{$monthly->amount}}">
            <input type="hidden" name="package" value="monthly">

            <div class="price_container">
                <h5>Team</h5>
                <h1>CA$ {{$monthly->amount}}</h1>
                {{-- <h6>/{{ucfirst($monthly->name)}}</h6> --}}
            </div>

            <div class="pg_detail_container px-5 mt-3 mb-5">
                @php
                    $m_list = json_decode($monthly->features);
                @endphp
                @foreach ($m_list as $li)
                    <div class="s_point">
                        <img width="15" src="{{asset("n_img/tick_n.png")}}" alt="">
                        <p class="m-0">{{$li}}</p>                    </div>  
                @endforeach
            </div>

            <div class="btn_container">
                <button type="submit">Subscriber Now</button>
            </div>
         </form>

         <form class="sub_card p-2" method="POST" action="{{ route('sub_checkout_page') }}">
            @csrf
            <div class="header">
                <h1 class="m-0 text-white pb-3">{{ucfirst($yearly->name)}}</h1>
            </div>

            <input type="hidden" name="amount" value="{{$yearly->amount}}">
            <input type="hidden" name="package" value="yearly">

            <div class="price_container">
                <h5>Team</h5>
                <h1>CA$ {{$yearly->amount}}</h1>
                {{-- <h6>/Monthly</h6> --}}
            </div>

            <div class="pg_detail_container px-5 mt-3 mb-5">
                @php
                    $y_list = json_decode($yearly->features);
                @endphp
                @foreach ($y_list as $li)
                    <div class="s_point">
                        <img width="15" src="{{asset("n_img/tick_n.png")}}" alt="">
                        <p class="m-0">{{$li}}</p>
                    </div>  
                @endforeach
            </div>

            <div class="btn_container">
                <button type="submitc">Subscriber Now</button>
            </div>
         </form>

    </div>
</div>
</section>


@include('common.cookiePermissionPopup')

@endsection
