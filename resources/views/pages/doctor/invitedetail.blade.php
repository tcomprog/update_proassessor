@extends('layouts.doctor-profile-custom')

@section('content')	
<style>
	  @font-face {
            font-family: Inter;
            src: url("{{asset('fonts/Inter-Regular.ttf')}}");
      }
</style>


<link rel="stylesheet" href="{{asset('css/invitedetail.css')}}?888888">

  
  <!-- Modal -->
  <div class="modal fade" id="deleteanimModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" style="display: flex;justify-content: center;align-items: center;">
		<div class="modal-content" style="height: 199px;width: 309px;">
		<div class="modal-body" style="padding:5px;overflow:hidden">
		  
			<img width="100%" style="height: 99%" src="{{asset("img/delete_anim.gif")}}" />

		</div>
	  </div>
	</div>
  </div>


    <section class="intro_banner_sec inner_banner_sec" style="padding:29px;height:">
        <div class="container">

            <div class="row align-items-center">

                <div class="col-md-6">

                    <div class="welcome_text">

                        <h1><?php echo ucfirst($invitation->patient->fname).' '.ucfirst($invitation->patient->lname); ?></h1>
                    </div>

                </div>

				
				<div class="col-md-6 text-right">
                    <a class="btn btn-primary"  style="padding: 5px 30px;
					background: #141a45;
					border: none;
					font-size: 16px;
					border:1px solid white;
					line-height: 28px;" href="@if(Auth::user()->role_id == '4') {{ url('clinic-dashboard') }} @else {{ url('doctor-dashboard')."/my-invitations/#confirmed-invitations" }}@endif">Back to Dashboard</a>
                </div>
            
            </div>
        </div>
    </section>
	
	<!-- Lawyer Dashboard Section -->

    
	<!---testing---->
	<section class="dashboard_sec">

        <div class="container">
            @if($invitation->is_patient_accepted == 'yes')
            <nav>
                <div class="nav nav-tabs mb-4" id="nav-tab" role="tablist">	

					<a class="nav-item nav-link {{ $activeTab == null || $activeTab=='files-reports' ? 'active' : '' }}" id="filesreports-tab" data-toggle="tab" href="#files-reports" role="tab" aria-controls="files-reports"    aria-selected="false">Files & Reports</a>
			
					<a class="nav-item nav-link {{$activeTab=='patient-information' ? 'active' : '' }}" id="patientinformation-tab" data-toggle="tab" href="#patient-information" role="tab" aria-controls="patient-information" aria-selected="false">Patient Information</a>

                    <a class="nav-item nav-link {{  $activeTab=='lawyer' ? 'active' : '' }}" id="lawyer-tab" data-toggle="tab" href="#lawyer" role="tab" aria-controls="lawyer"    aria-selected="false">Lawyer</a>


					@if ($invitation->status != 'completed' && $invitation->status != 'cancelled')
                    <a class="nav-item nav-link {{ $activeTab=='chatroom' ? 'active' : '' }}" id="chatroom-tab" data-toggle="tab" href="#chatroom" role="tab" aria-controls="chatroom" aria-selected="false">Chatroom 
						@if(Helper::un_read_messages(Auth::id(),$invitation->id))
						<span class="badge badge-warning">{{ Helper::un_read_messages(Auth::id(),$invitation->id) }}</span>
						@endif
			    	</a>
					@endif
                </div>
            </nav>

            <div class="tab-content" id="nav-tabContent">

                <div class="tab-pane fade {{  $activeTab=='lawyer' ? 'active show' : '' }}" id="lawyer" role="tabpanel" aria-labelledby="lawyer-tab">

					<div class="detailscontainer px-4">

						<div class="nameContainerSection d-flex align-items-center">
							<?php if($user->profilepic){?>
							 <img src="{{ url('uploads/profilepics/lawyer/'.$user->profilepic) }}" class="doctor_profile" style="margin-left: 12px !important;"> 
							<?php }else{?>
							 <img src="{{ url('img/user.jpg') }}" class="doctor_profile" style="margin-left: 12px !important;"> 
							<?php } ?>	
							
							<p>{{ ucfirst($user->name) }}</p>   
						</div>

						@php
						    $city = Helper::cities();
						    $city_name = "";
							 foreach ($city as $city1){ 
								 if(($city1->id)==($user->location)){
									$city_name = ucfirst(strtolower($city1->city_name));
								}
							 }
						@endphp

						<div class="row">
							@if(!empty($city_name))
							<div class="col-md-5 col-sm-12">
								<div class="singlep_container">
									<p>City</p>
									<p class="p-data">
										{{$city_name}}
									</p>
								</div>
							</div>
							@else
							<div class="col-md-5 col-sm-12">
								<div class="singlep_container">
									<p>Law Firm Name</p>
									<p class="p-data">{{ ucfirst($user->lawyer->lawfirmname) }}</p>
								</div>
							</div>
							@endif
							<div class="col-md-5 col-sm-12">
								<div class="singlep_container">
									<p>Business Address</p>
									<p class="p-data">{{ $user->business_address }}</p>
								</div>
							</div>
						</div>

						<div class="row">
							@if(!empty($city_name))
							<div class="col-md-5 col-sm-12">
								<div class="singlep_container">
									<p>Phone</p>
									<p class="p-data">{{ $user->phone }}</p>
								</div>
							</div>
							@endif
							<div class="col-md-5 col-sm-12">
								<div class="singlep_container">
									<p>Law Firm Name</p>
									<p class="p-data">{{ ucfirst($user->lawyer->lawfirmname) }}</p>
								</div>
							</div>
						</div>


						@if (!empty($user->lawyer->website_url))
							<div class="singlep_container">
								<p>Website Url</p>
								<p class="p-data">{{ $user->lawyer->website_url }}</p>
							</div>
						@endif

					</div>

                </div>

                <div class="tab-pane fade {{$activeTab=='patient-information' ? 'active show' : '' }}" id="patient-information" role="tabpanel" aria-labelledby="patientinformation-tab">


					<div class="detailscontainer px-4">

						<div class="row">
							<div class="col-md-5 col-sm-12">
								<div class="singlep_container">
									<p>First Name</p>
									<p class="p-data"><?php echo ucfirst(strtolower($patient->fname)); ?></p>
								</div>
							</div>
							<div class="col-md-5 col-sm-12">
								<div class="singlep_container">
									<p>Last Name</p>
									<p class="p-data"><?php echo ucfirst(strtolower($patient->lname)); ?></p>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-5 col-sm-12">
								<div class="singlep_container">
									<p>Email</p>
									<p class="p-data"><?php echo MyHelper::Decrypt($patient->email); ?></p>
								</div>
							</div>
							<div class="col-md-5 col-sm-12">
								<div class="singlep_container">
									<p>Gender</p>
									<p class="p-data"><?php echo ucfirst(MyHelper::Decrypt($patient->gender)); ?></p>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-5 col-sm-12">
								<div class="singlep_container">
									<p>Address</p>
									<p class="p-data"><?php echo ucfirst(strtolower(MyHelper::Decrypt($patient->address))); ?></p>
								</div>
							</div>
							<div class="col-md-5 col-sm-12">

								<div class="singlep_container">
									<p>Case type</p>
									<p class="p-data">
										@if ($patient->casetype == 1)
										MVA
										@elseif ($patient->casetype == 2)
										WSIB
										@elseif($patient->casetype == 3)
										Slip & Fall
										@endif
									</p>
								</div>

							</div>
						</div>

						<div class="row">
							<div class="col-md-5 col-sm-12">
								<div class="singlep_container">
									<p>Date of birth</p>
									<p class="p-data"><?php echo date('d F Y', strtotime($patient->dateofbirth)); ?></p>
								</div>
								
							</div>
							<div class="col-md-5 col-sm-12">
								<div class="singlep_container">
									<p>Date of accident</p>
									<p class="p-data"><?php  echo date('d F Y', strtotime($patient->dateofaccident)); ?></p>
								</div>
							</div>
						</div>

					</div>

                </div>

                <div class="tab-pane fade {{$activeTab == null ||  $activeTab=='files-reports' ? 'show active' : '' }}" id="files-reports" role="tabpanel" aria-labelledby="filesreports-tab">

					<!---start--->
					<div class="patient_details_box">
						<div style="display:flex; justify-content: space-between;">
							<span  style="margin-bottom: 15px;display: inline-block;font-size: 16px;"><span class="font-weight-bold" >Appointment Date/Time: </span>  <img src="{{ url('img/calendar.png') }}" class="calendar_icon"> <span style="font-size:16px;"><?php echo date('d F Y', strtotime($invitation->invitation_date)); ?> |  {{ $invitation->timeslot }} </span></span>
							<span  style="margin-bottom: 15px;display: inline-block;font-size: 16px;"><span class="font-weight-bold" style="color:red;" >Report Due Date: </span>  <img src="{{ url('img/calendar.png') }}" class="calendar_icon"> <span style="font-size:16px;"><?php echo date('d F Y', strtotime($invitation->duedate)); ?> </span>
								<?php if($invitation->appointment_type=='virtual') { ?><span class="virtualbold"><b> - Virtual</b></span><?php } ?>
							</span>
					   </div>
						
						<div class="invitation-tabs"> 
							<ul class="nav nav-tabs" id="myTab" role="tablist">

							@if ($invitation->status != 'cancelled')
								<li class="nav-item active">
									<a class="nav-link tabsss" style='font-family: "Titillium Web", sans-serif !important; margin-bottom: 3px;' data-toggle="tab" href="#status_sec{{$invitation->id}}" role="tab" aria-controls="Status section">Status</a>   
								</li>

								<li class="nav-item active">
									<a class="nav-link tabsss active" style='font-family: "Titillium Web", sans-serif !important; margin-bottom: 3px;' data-toggle="tab" href="#reports{{$invitation->id}}" role="tab" aria-controls="expired-invitations">Reports</a>   
								</li>
							@endif
							
							<li class="nav-item active">
								<a class="nav-link tabsss" data-toggle="tab" style='font-family: "Titillium Web", sans-serif !important; margin-bottom: 3px;' href="#invoice{{$invitation->id}}" role="tab" aria-controls="expired-invitations">Invoice</a>   
							</li>

							
							<li class="nav-item">
								<a class="nav-link tabsss  invitation-tab" data-toggle="tab" style='font-family: "Titillium Web", sans-serif !important; margin-bottom: 3px;' href="#new-invitations{{$invitation->id}}" role="tab" aria-controls="new-invitations">Medicals</a>
							</li>
							<li class="nav-item">
								<a class="nav-link tabsss" data-toggle="tab" href="#accepted-invitations{{$invitation->id}}" style='font-family: "Titillium Web", sans-serif !important; margin-bottom: 3px;' role="tab" aria-controls="accepted-invitations">Summary</a>
							</li>
							<li class="nav-item">
								<a class="nav-link tabsss" data-toggle="tab" href="#expired-invitations{{$invitation->id}}" style='font-family: "Titillium Web", sans-serif !important; margin-bottom: 3px;' role="tab" aria-controls="expired-invitations">Letter of Engagement</a>
							</li>							
							<li class="nav-item">
								<a class="nav-link tabsss" data-toggle="tab" href="#rejected-invitations{{$invitation->id}}" style='font-family: "Titillium Web", sans-serif !important; margin-bottom: 3px;' role="tab" aria-controls="expired-invitations">Form 53</a>
							</li>
							<li class="nav-item">
								<a class="nav-link tabsss" data-toggle="tab" href="#instruction-form{{$invitation->id}}" style='font-family: "Titillium Web", sans-serif !important; margin-bottom: 3px;' role="tab" aria-controls="instruction-form">Questionnaire</a>
							</li>
							
						</ul>

					
						<div class="tab-content " id="invitations-tab-content">

						  {{-- New status TODO: --}}
						  <div class="tab-pane" id="status_sec{{$invitation->id}}" role="tabpanel">
							<div class="statusSection">
								@if (isset($showUpload) && $showUpload == 'false' && $invitation->status == 'active')
								<div style="text-align: end;" class="btn-container11">
									<img style="display:none;" width="30" id="accept_report_loading_msg_{{$invitation->id}}" width="100" src="{{asset('img/loading.gif')}}" />
									<button id="acptButton_{{$invitation->id}}" onclick="acceptReportFunc({{$invitation->id}})" class="act_btn">Patient assessment completed</button>
									 <div id="successMsg_{{$invitation->id}}" class="alert alert-success mt-1" style="text-align: start;display:none;" role="alert">
										A simple success alert—check it out!
									  </div>
									  
								</div>
								@endif
							</div>	
						  </div>
		

							<div class="tab-pane active" id="reports{{$invitation->id}}" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										{{-- <h3 class="inv-content-header">Upload Reports </h3> --}}
										@if(@$invitation->attachments->reportfilename) 
											<div class="col d-flex mb-2" style="justify-content: end; padding:0px">
												<img id="deleteLoad_6" style="display: none" width="25" class="mr-3" src="{{asset('img/loading.gif')}}" />
												<button type="button" id="deleteBtn_6" onclick="deleteAllFilesDoctor('{{$invitation->id}}',6)" class="btn btn-outline-danger btn-sm ">Clear all</button>
											</div>
										@endif

										@php
											$st=0; 
										@endphp
										
										<h5 class="text-success" id="dlte_scss{{$invitation->id}}" style="color: #C01D1D !important;display:none;">Reports Deleted Successsfully</h5>
										<ul class="mt-2 reportList_6" id="reports-ul{{$invitation->id}}" >
											<?php if($invitation->attachments!=null) { 
												 
											 if(($invitation->attachments->reportfilename)!=null) {?>
											<?php  
													$a=json_decode($invitation->attachments->reportfilename);
													$b=json_decode($invitation->attachments->reportimagename);
													$b=json_decode( json_encode($b), true);
													$i=0;
													$ar = [
													'pdf' => 'picture_as_pdf',
													'doc' => 'upload_file',
													'docx' => 'description',
										         ];
													
												foreach($a as $key => $value) {		
													$st++;										
														?>
														<div style="background: #F3F2F2;border-radius: 5px; margin-bottom: 12px;padding: 5px 15px;display:flex;" id="reports{{ $invitation->id.$key }}">
															<span class="material-icons" style="color: #20215E; font-size:24px; margin-right:8px;">{{$ar[explode('.',$b[$i])[1]]}}</span>

															<form action="{{route('downloadEncFile')}}" style="margin-bottom: 0px; flex:1; display: inline-block" method="POST">
																@csrf
																<input name="path" type="hidden" value="{{$value}}">
																<input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
																<input name="name" type="hidden" value="{{$b[$i]}}">
																<input name="directory" type="hidden" value="medical_reports">
																<button style="background: transparent;
																border: none;
																font-weight: bold;
																color: #20215E"><?php echo $b[$i]; ?></button>
															</form>
															<a href="javascript:void(0)" onClick="reply_click1(this.id, '{{ $b[$i] }}','reports{{ $invitation->id.$key }}')" id="{{ $value }}" >
																<i class="fa fa-close" style="font-size: 20px;color: red;" ></i>
															</a>
														</div>
													<?php	$i++; } ?>																					
										</ul>
										<?php } }else{  ?>	
											<ul class="mt-2" id="reports-ul{{$invitation->id}}" ></ul>
										<?php  } ?>
										
										@if (isset($showUpload) && $showUpload == 'true' && $invitation->status != 'completed')
										<div @if($st > 0) {{'style=text-align:center'}} @else {{'style=text-align:center;padding-top:110px;'}} @endif>
											<a class="upload_doc_btn new_upload_btn" href="#" data-toggle="modal" data-target="#UploadDocs">
												<span class="material-icons upload_icon">backup</span>
												Upload
											</a>
									    	{{-- <a class="nav-link addpatient_btn upload_doc_btn" href="#" data-toggle="modal" data-target="#UploadDocs" > Upload</a> --}}
										</div>
										@endif
                                     
										@if (isset($showUpload) && $showUpload == 'false' && $invitation->status == 'active')
										{{-- <div style="text-align: end;" class="btn-container11">
											<img style="display:none;" width="30" id="accept_report_loading_msg_{{$invitation->id}}" width="100" src="{{asset('img/loading.gif')}}" />
											<button id="acptButton_{{$invitation->id}}" onclick="acceptReportFunc({{$invitation->id}})" class="act_btn">Patient visit</button>
											 <div id="successMsg_{{$invitation->id}}" class="alert alert-success mt-1" style="text-align: start;display:none;" role="alert">
												A simple success alert—check it out!
											  </div>
											  
										</div> --}}
										<p style="color: #eca606;font-size: 18px; font-weight: 600;">You will able to upload report once you changed patient visit status in the status section</p>
										@endif
									</div>
									
								</div>
							</div>
							<div class="tab-pane" id="invoice{{$invitation->id}}" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										<h3 class="inv-content-header">Upload Invoice</h3>
										<?php 
											$today = date("Y-m-d");
											//$today_time = strtotime($today);
											//$expire_time = strtotime($invitation->invitation_date);
										if ($invitation->invitation_date < $today || $invitation->status == "cancelled") { ?>
										<form id="custom_invoice_form">
											<br>
											<div class="row">
												<div class="col-md-12">	
													<?php  if(isset($invitation->invoices)&&($invitation->invoices->payment_amount!=null)){ ?>
														<ul class="mt-2">
															<li><span><a  href="{{url('/uploads/custom_invoice')}}/{{$invitation->invoices->invoice_filename}}" style="text-decoration:none;" target="_blank">Download Invoice</a></span>	
															<a href="{{url('/uploads/custom_invoice')}}/{{$invitation->invoices->invoice_filename}}" download="invoice_{{$invitation->invoices->invoice_filename}}"target="_blank"><i class="fa fa-download" style="font-size: 18px;float: right;" id="download_invoice"></i></a>
															</li>
																											
														</ul>
														<?php  } ?>
												</div>
												<div class="col-md-4 col-md-offset-4">
																										
													<div class="input-group mb-3">
														<div class="input-group-prepend">
															<span class="input-group-text" style="border-radius:0 !important" id="basic-addon1">$</span>
														  </div>
														<input type="hidden" id="invoiceHidenAmount" value="{{$doctor_invoice_price}}">
														<input type="text" class="form-control" name="billamt" id="billamt" value="{{$doctor_invoice_price}}" disabled  placeholder="Enter Bill amount"><br>
														<input type="hidden" class="form-control" name="bill_invitation_id" id="bill_invitation_id" value="{{$invitation->id}}">
														<input type="hidden" class="form-control" name="docid" id="docid" value="{{$invitation->user1->id}}">
													</div>
													<br>		
													<input type="text" class="form-control" name="serviceprovided" id="serviceprovided" Placeholder="Enter Service Provided" readonly value="{{$category}}"><br>											
													<?php  if(isset($invitation->invoices)&&($invitation->invoices->payment_amount!=null)){ ?>
													
													<button type="submit" class="nav-link addpatient_btn" id="custom_invoice_form_btn"> Update Invoice</button>
													<?php }else{ ?>
													<button type="submit" class="nav-link addpatient_btn" id="custom_invoice_form_btn"> Generate Invoice</button>
													<?php } ?>
												</div>  
											</div>
										</form>
										<?php  } 
											else { 
											echo "<br><br><p  style='color: #eca606;font-size: 18px; font-weight: 600;'>You will be able to generate an Invoice in this section once the Appointment Date is passed.</p>";
											 }  ?>
									</div>
									
								</div>
							</div>
								<div class="tab-pane invitation-tab " id="new-invitations{{$invitation->id}}" role="tabpanel">
									<div class="row">
										<div class="col-12">
											<h3 class="inv-content-header">Medicals</h3>
											
											<ul class="mt-4">
											<?php if($invitation->attachments && $invitation->attachments->medicalsfilename) {
												$a=json_decode($invitation->attachments->medicalsfilename);
												$b=json_decode($invitation->attachments->medicalsimagename);
												$b=json_decode( json_encode($b), true);
												$i=0;
												//print_r($b);exit;
												foreach($a as $key => $value) { ?>
												<div>
													<form action="{{route('downloadEncFile')}}" class="downloadForDes" method="POST">
														@csrf
														<input name="path" type="hidden" value="{{$value}}">
														<input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
														<input name="name" type="hidden" value="{{$b[$i]}}">
														<input name="directory" type="hidden" value="l_medical">
														<button class="downloadTitleDesign"><?php echo $b[$i]; ?></button>
														<button type="submit" style="background: transparent;border: none;">
															<i class="fa fa-download" title="Download report" style="color:#20215E !important; font-size:20px"></i>
														</button>
													</form>
													{{-- <span><a onclick="document.getElementById('medical-file-download-form{{ $invitation->id.$key }}').submit()" href="#" style="text-decoration:none;" ><?php echo $b[$i]; ?></a></span>
													<a onclick="document.getElementById('medical-file-download-form{{ $invitation->id.$key }}').submit()" href="#" style="text-decoration:none;" ><i class="fa fa-download" style="font-size: 18px;float: right;" ></i></a> --}}
												</div>
												<form id="medical-file-download-form{{ $invitation->id.$key }}" action="{{ url('download-medicals-file') }}" method="post">
													@csrf
													<input type="hidden" name="filename" value="{{ $value }}">
													<input type="hidden" name="user_id" value="{{ $invitation->invitee_user_id }}" />
													<input type="hidden" name="download_name" value="{{ $b[$i] }}">
												</form>
											<?php
											$i++; } ?>  														
												</ul> 
											<?php } else {
												echo "<p  style='color: #eca606;font-size: 18px; font-weight: 600;'>No Medical details uploaded by Lawyer.</p>";
											} 
										?>
										</div>
									</div>
								</div>
						<div class="tab-pane" id="accepted-invitations{{$invitation->id}}" role="tabpanel">
							<div class="row">
								<div class="col-md-12">
									<h3 class="inv-content-header">Summary</h3>
									
									<?php if(($invitation->attachments)!=null && $invitation->attachments->filename) { ?>
											<ul class="mt-4">
											<?php  //print_r(json_decode($invitation->attachments->filename)); 
													$a=json_decode($invitation->attachments->filename);
													$b=json_decode($invitation->attachments->imagename);
													$b=json_decode( json_encode($b), true);
													$i=0;
													//print_r($b);exit;
													foreach($a as $key => $value) { ?>
													<div>
														<form action="{{route('downloadEncFile')}}" class="downloadForDes" method="POST">
															@csrf
															<input name="path" type="hidden" value="{{$value}}">
															<input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
															<input name="name" type="hidden" value="{{$b[$i]}}">
															<input name="directory" type="hidden" value="l_summary">
															<button class="downloadTitleDesign"><?php echo $b[$i]; ?></button>
															<button type="submit" style="background: transparent;border: none;">
																<i class="fa fa-download" title="Download report" style="color:#20215E !important; font-size:20px"></i>
															</button>
														</form>
														{{-- <span><a target="_blank" href="{{ url('uploads/docs/'.$value) }}" style="text-decoration:none;" ><?php echo $b[$i]; ?></a></span>	 --}}
														{{-- <a href="{{ url('uploads/docs/'.$value) }}" download="{{ $b[$i] }}"  id="{{ $value }}" ><i class="fa fa-download" style="font-size: 18px;float: right;" ></i></a> --}}
													</div>
												<?php
												$i++; } ?>  														
												</ul> 
											<?php } else {
												echo "<p  style='color: #eca606;font-size: 18px; font-weight: 600;'>No Summary Documents uploaded by Lawyer.</p>";
											} 
										?>
								</div>								
							</div>
						</div>
							<div class="tab-pane" id="expired-invitations{{$invitation->id}}" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										<h3 class="inv-content-header">Letter of Engagement</h3>
										
										<?php if(($invitation->attachments)!=null && $invitation->attachments->loefilename) { ?>
											<ul class="mt-4">
											<?php  //print_r(json_decode($invitation->attachments->filename)); 
													$a=json_decode($invitation->attachments->loefilename);
													$b=json_decode($invitation->attachments->loeimagename);
													$b=json_decode( json_encode($b), true);
													$i=0;
													//print_r($b);exit;
													foreach($a as $key => $value) { ?>
													<div>
														{{-- <span><a target="_blank" href="{{ url('uploads/loe/'.$value) }}" style="text-decoration:none;" ><?php echo $b[$i]; ?><i class="fa fa-download" style="font-size: 18px;float: right;" ></i></a></span>	 --}}
														<form action="{{route('downloadEncFile')}}"  class="downloadForDes" method="POST">
															@csrf
															<input name="path" type="hidden" value="{{$value}}">
															<input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
															<input name="name" type="hidden" value="{{$b[$i]}}">
															<input name="directory" type="hidden" value="l_LetterOfEngagement">
															<button class="downloadTitleDesign"><?php echo $b[$i]; ?></button>
															<button type="submit" style="background: transparent;border: none;">
																<i class="fa fa-download" title="Download report" style="color:#20215E !important; font-size:20px"></i>
															</button>
														</form>
													</div>
												<?php
												$i++; } ?>  														
												</ul> 
											<?php } else {
												echo "<p  style='color: #eca606;font-size: 18px; font-weight: 600;'>No Letter of Engagement Documents uploaded by Lawyer.</p>";
											} 
										?>
									</div>
									
								</div>
							</div>
							
							<div class="tab-pane" id="rejected-invitations{{$invitation->id}}" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										<h3 class="inv-content-header">Form 53</h3>
										
										<?php if(($invitation->attachments)!=null && $invitation->attachments->form53filename) { ?>
											<ul class="mt-4">
											<?php  //print_r(json_decode($invitation->attachments->filename)); 
													$a=json_decode($invitation->attachments->form53filename);
													$b=json_decode($invitation->attachments->form53imagename);
													$b=json_decode( json_encode($b), true);
													$i=0;
													//print_r($b);exit;
													foreach($a as $key => $value) { ?>
													<div>
														{{-- <span><a target="_blank" href="{{ url('uploads/form53/'.$value) }}" style="text-decoration:none;" ><?php echo $b[$i]; ?><i class="fa fa-download" style="font-size: 18px;float: right;" ></i></a></span>	 --}}
														<form action="{{route('downloadEncFile')}}"  class="downloadForDes" method="POST">
															@csrf
															<input name="path" type="hidden" value="{{$value}}">
															<input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
															<input name="name" type="hidden" value="{{$b[$i]}}">
															<input name="directory" type="hidden" value="l_form_53">
															<button class="downloadTitleDesign"><?php echo $b[$i]; ?></button>
															<button type="submit" style="background: transparent;border: none;">
																<i class="fa fa-download" title="Download report" style="color:#20215E !important; font-size:20px"></i>
															</button>
														</form>
													</div>
												<?php
												$i++; } ?>  														
												</ul> 
											<?php } else {
												echo "<p  style='color: #eca606;font-size: 18px; font-weight: 600;'>No Form 53 Documents uploaded by Lawyer.</p>";
											} 
										?>
									</div>
									
								</div>
							</div>
							<div class="tab-pane" id="instruction-form{{$invitation->id}}" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										<h3 class="inv-content-header">Questionnaire</h3>
										
										<?php if(($invitation->attachments)!=null && $invitation->attachments->insformfilename) { ?>
											<ul class="mt-4">
											<?php  //print_r(json_decode($invitation->attachments->filename)); 
													$a=json_decode($invitation->attachments->insformfilename);
													$b=json_decode($invitation->attachments->insformimagename);
													$b=json_decode( json_encode($b), true);
													$i=0;
													//print_r($b);exit;
													foreach($a as $key => $value) { ?>
													<div>
														{{-- <span><a target="_blank" href="{{ url('uploads/insform/'.$value) }}" style="text-decoration:none;" ><?php echo $b[$i]; ?><i class="fa fa-download" style="font-size: 18px;float: right;" ></i></a></span>	 --}}
														<form action="{{route('downloadEncFile')}}"  class="downloadForDes" method="POST">
															@csrf
															<input name="path" type="hidden" value="{{$value}}">
															<input name="enceypKey" type="hidden" value="{{@$invitation->enceypKey}}">
															<input name="name" type="hidden" value="{{$b[$i]}}">
															<input name="directory" type="hidden" value="l_questionair">
															<button class="downloadTitleDesign"><?php echo $b[$i]; ?></button>
															<button type="submit" style="background: transparent;border: none;">
																<i class="fa fa-download" title="Download report" style="color:#20215E !important; font-size:20px"></i>
															</button>
														</form>
													</div>
												<?php
												$i++; } ?>  														
												</ul> 
											<?php } else {
												echo "<p  style='color: #eca606;font-size: 18px; font-weight: 600;'>No Questionnaire Documents uploaded by Lawyer.</p>";
											} 
										?>
									</div>
									
								</div>
							</div>
							
							</div>
							<br><br><br><br><br>
						</div>	
					</div>	
					<!---end----->

                </div>

                <div class="tab-pane fade {{ $activeTab=='chatroom' ? 'show active' : '' }}" id="chatroom" role="tabpanel" aria-labelledby="chatroom-tab">
                <div class="patient-details">
					
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6 m-auto pb-2">
                        <div class="card card-body">
                            <h5>Patient Information</h5>
                            <div class="row">
							@if($doctor->category_id !== $invitation->invitee_category_id)
								<div class="col-md-12">
									<p class="mb-0"><strong>Category:</strong> {{ $invitation->category->category_name }}</p>
								</div>
							@endif
                                <div class="col-md-6">
                                <p class="mb-0">
                                <strong>Name: </strong>
                                <span>{{ ucfirst($invitation->patient->name) }}</span>
                                </p>
                                <p class="mb-0">
									<strong>Email: </strong>
									<span>{{ MyHelper::Decrypt($invitation->patient->email) }}</span>
								</p>
								<p class="mb-0">
									<strong>Gender: </strong>
									<span>{{ MyHelper::Decrypt($invitation->patient->gender) }}</span>
								</p>
                            <p class="mb-0">
                                <strong>Address: </strong>
                                <span>{{ MyHelper::Decrypt($invitation->patient->address) }}</span>
                            </p>
                                </div>
                                <div class="col-md-6">
                                <p class="mb-0">
                                <strong>Appointment Date: </strong>
                                <span>{{ date('d/M/Y',strtotime($invitation->invitation_date)) }}</span>
                                </p>
                                <p class="mb-0">
                                <strong>Lawyer Name: </strong>
                                <span><?php echo ucfirst($invitation->user->name);?> <br><?php if($invitation->user->lawyer->lawfirmname!=null){?>(<?php echo ucfirst($invitation->user->lawyer->lawfirmname);?>)<?php } ?></span>
                                </p>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
            </div>
                <div class="messaging">

                    <div class="inbox_msg">

                        <div class="inbox_people1">

                        <div class="headind_srch">
                            <div class="recent_heading">
                            <h4>{{ ucfirst($invitation->user->name) }}</h4>
                            </div>
                            <!-- <div class="srch_bar">
                            <div class="stylish-input-group">
                                <input type="text" class="search-bar"  placeholder="Search" >
                                <span class="input-group-addon">
                                <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                                </span> </div>
                            </div> -->
                        </div>
						<br>
                        <div class="msg_history" id="messages">
                        </div>

                        <div class="type_msg">
                        <form action="#" id="chat-msg-form">
                            <div class="input_msg_write pl-2">

                            <input type="text" id="message" class="write_msg" autocomplete="off" placeholder="Type a message" />

                            <button class="msg_send_btn mr-2" type="submit"><img src="{{url('img/telegram.svg')}}"></button>

                            </div>
                        </form>

                        </div>

                        </div>

                    </div>

                    </div>

                </div>

            </div>
			@else 
            <h4>Patient not yet accepted this Invitation.</h4>
			@endif
		</div>

    </section>


<div class="modal fade" id="UploadDocs" tabindex="1" role="dialog" aria-labelledby="calendarTitle" data-backdrop="static" data-keyboard="false" tabindex="-1" >

<div class="modal-dialog modal-dialog-centered" role="document">

  <div class="modal-content login_box">

	<div class="modal-header">

	  <h5 class="modal-title" id="calendarTitle">Please upload patients documents</h5>

	  <button type="button" class="close" data-dismiss="modal" aria-label="Close">

		<span aria-hidden="true">&times;</span>

	  </button>

	</div>

	<div class="modal-body">
	  <form id="upload_form"  enctype="multipart/form-data">

		  <div class="form-group files">

			<input type="file" class="form-control" name="files[]" id="files" multiple="" required>
			<span class="text-danger" id="upload-error" style="color:red;"></span>
			<h5 class="text-success" id="upload-scss" style="color:green;"></h5>
		  </div>
		  <div class="form-group">
			<input type="hidden" name="newintid" id="upload-file-invitation-id" value="{{ $invitation->id }}" > 
			<input type="hidden" name="patientid" id="upload-file-patient-id" value="{{ $invitation->patient_user_id }}"> 
		  </div>

		  <div id="error_reportUpload" class="alert alert-danger" role="alert" style="display: none">
			Invalid file type, You can only upload pdf, doc, docx
		  </div>

		  <div id="progress_DoctorReport">
			  
		  </div>

		  <button  type="button" class="invite_btn upload_btn" id="reportSubmit">Submit</button> 
	  </form>

	</div>

  </div>
</div>

</div>	
<!---upload invoice start-->
<div class="modal fade" id="UploadInvoice" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">

<div class="modal-dialog modal-dialog-centered" role="document">

  <div class="modal-content login_box">

	<div class="modal-header">

	  <h5 class="modal-title" id="calendarTitle">Please upload patient invoice</h5>

	  <button type="button" class="close" data-dismiss="modal" aria-label="Close">

		<span aria-hidden="true">&times;</span>

	  </button>

	</div>

	<div class="modal-body">
	  <form id="upload_invoice_form"  enctype="multipart/form-data">
		  <div class="form-group files">
			<input type="file" class="form-control" name="invoicefiles[]" id="invoicefiles"  required>
			<span class="text-danger" id="invoice-upload-error" style="color:red;"></span>
			<h5 class="text-success" id="invoice-upload-scss" style="color:green;"></h5>
		  </div>
		  <div class="form-group">
			<input type="hidden" name="invoicenewintid" id="upload-file-invitation-id1" value="{{ $invitation->id }}" > 
			<input type="hidden" name="invoicedoctorid" id="upload-file-doctor-id" value="{{ $invitation->invitee_user_id }}"> 
		  </div>
		  <button  type="submit" class="invite_btn upload_invoice_btn" id="submit">Submit</button> 
	  </form>

	</div>

  </div>
</div>

</div>	
<!---upload invoice end-->


<div class="modal fade" id="DeleteDocs" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">

<div class="modal-dialog modal-dialog-centered" role="document" style="display: flex;justify-content: center;align-items: center;">

  <div class="modal-content p-0" style="max-width: 400px">

	{{-- <div class="modal-header">

	  <h5 class="modal-title" id="calendarTitle">Are You Sure Want to Delete ?</h5>

	  <button type="button" class="close" data-dismiss="modal" aria-label="Close">

		<span aria-hidden="true">&times;</span>

	  </button>

	</div> --}}
		
	<div class="modal-body">
	  <form id="deletedoc_form" >
		  
		  <div class="form-group">
			  <input type="hidden" name="newintid" id="del-invitation-id" value="{{ $invitation->id }}"> 
			<input type="hidden" name="patientid" id="del-inv-patient_id" value="{{ $invitation->patient_user_id }}"> 
			<input type="hidden" name="DeleteDocsid" id="DeleteDocsid"> 
			<input type="hidden" name="DeleteDocsname" id="DeleteDocsname"> 
			<input type="hidden" name="reportsactiveliid" id="DeleteActiveli"> 
		   </div>
		   <div class="row">
			<div class="col-md-12">
				<div class="manualboxdesi mx-auto">
				  <button type="button" id="deletedoc_form_btn">Yes</button> 
				   <button  type="button"  data-dismiss="modal" aria-label="Close" style="background: #CC0A11;">No</button> 
				</div>
			  </div>
		   {{-- <div class="col-md-2"></div>
		   <div class="col-md-8" id="reports-btn-div">
			  <button type="button" class="invite_btn" id="deletedoc_form_btn">Yes</button> 
			  <button  type="button"  class="invite_btn" data-dismiss="modal" aria-label="Close">No</button> 
		  </div>
		  <div class="col-md-2"></div> --}}
		
	  </form>

	</div>

  </div>
</div>

</div>		  
<!-- end delete Documents Model -->
</div>

<!-----delete invoice start---->
<div class="modal fade" id="DeleteInvoice" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">

	  <div class="modal-content login_box">

		<div class="modal-header">

		  <h5 class="modal-title" id="calendarTitle">Are You Sure Want to Delete ?</h5>

		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">

			<span aria-hidden="true">&times;</span>

		  </button>

		</div>
			<h5 class="text-success" id="dlte_invoice_scss" style="color:green;display:none;">Invoice Deleted Successsfully</h5>
		<div class="modal-body">
		  <form id="deleteinvoice_form" >
			  
			  <div class="form-group">				 
				<input type="hidden" name="invoiceintdid" id="invoiceintdid" value="{{ $invitation->id }}"> 
				<input type="hidden" name="invoiceintdname" id="invoiceintdname"> 
			   </div>
			   <div class="row">
			   <div class="col-md-2"></div>
			   <div class="col-md-8">
				  <button type="button" class="invite_btn" id="deleteinvoice_form_btn">Yes</button> 
				  <button  type="button"  class="invite_btn" data-dismiss="modal" aria-label="Close">No</button> 
			  </div>
			  <div class="col-md-2"></div>
		  </form>

		</div>

	  </div>
	</div>
	</div>
</div>
<!-----delete invoice end---->

<!-----send invoice start---->
<div class="modal fade" id="custom_invoice_modal" tabindex="1" role="dialog" aria-labelledby="calendarTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 960px;">
		<div class="modal-content login_box">		
						@include('pages.doctor.invoicemodal') 
		</div>
	</div>
</div>
<!-----send invoice end---->
<!---start---->
	<div class="modal fade" id="scss_invoice_sent" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
	
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content login_box">
		  <div class="modal-body">		
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  <div class="row">
				<div class="col-md-3 text-center m-auto">
					<img id="modal-inv-succes-img" src="{{ asset('img/checkmark.svg') }}" class="w-100" alt="success" />				
				</div>
		  </div>
			<h5 class="modal-title"  style="color:green;text-align: center;" id="acceptscss">Invoice Sent Successfully</h5>
			</div>
		</div>
	  </div>
	</div>
	<!----end----->
 <!-- Lawyer Dashboard Section -->
	
@endsection


@section('footerscripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.1/socket.io.js"></script>

<script>   
    var start = 0;
    const messageContainer  = $( "#messages" );
   var socket = io.connect('{{ env("SOCKET_URL") }}');

   var userInfo = {
       email:'{{ Auth::user()->email }}', 
       sender_id:'{{ Auth::user()->unique_id }}', 
       invitation_id:'{{ $invitation->unique_id }}', 
       partner_id:'{{ $invitation->user->unique_id }}', 
   };
$(document).ready(function(){
    load_messages();

    socket.on("user-join",function(socketID){
        userInfo.socket_id = socketID;
        socket.emit("user-join", userInfo);
    });

    socket.on('chat-message', function (data) {
            if(data && data.message && data.invitation_id == userInfo.invitation_id) {
				if(data.sender_id == userInfo.sender_id && data.invitation_id == userInfo.invitation_id) {
                const messageData = {
                    message : data.message,
                    partner_id:userInfo.partner_id,
                    invitation_id:userInfo.invitation_id,
                    created_on:"{{ date('h:i A | M y') }}",
                    sender_id:userInfo.sender_id,
                    socket_id:userInfo.socket_id,
                    is_read:data.is_read
                    }
                    store_mesage(messageData);
                }

                let msgData = {};
                if(data.sender_id == userInfo.sender_id && data.invitation_id == userInfo.invitation_id) {
                    msgData = `<div class="outgoing_msg">
                                <div class="incoming_msg_img"> <img src="{{ url('img/user.jpg')}}" alt="user"> </div>
                                <div class="sent_msg">
                                    <p>`+data.message+`</p>
                                    <span class="time_date">`+data.created_on+`</span> </div>
                                </div>`;
                }else if(data.partner_id == userInfo.sender_id){
                    msgData =  `<div class="incoming_msg"><div class="incoming_msg_img"> <img src="{{ url('img/user.jpg')}}" alt="user"> </div>
                    <div class="received_msg">
                        <div class="received_withd_msg">
                        <p>`+data.message+`</p>
                        <span class="time_date">`+data.created_on+`</span></div>
                    </div>
                    </div>`;
                }
                const messageContainer  = $( "#messages" );
                messageContainer.append(msgData);
                messageContainer.scrollTop(messageContainer[0].scrollHeight);
            }
        });

$('#chat-msg-form').submit(function(e){
    e.preventDefault();
    var message = $('#message').val();
    if(message) {
        const messageData = {
            message : message,
            partner_id:userInfo.partner_id,
            invitation_id:userInfo.invitation_id,
            created_on:"{{ date('h:i A | M y') }}",
            sender_id:userInfo.sender_id,
			socket_id:userInfo.socket_id
        }
        console.log("sending message")
        socket.emit('chat-message',messageData);
        // store_mesage(messageData)
        $('#message').val('');
    }
})

});


function store_mesage(messageData)
{
    messageData._token = '{{ csrf_token() }}';
    $.ajax({
        url:"{{ url('chat/store-message') }}",
        data:messageData,
        type:'post',
        dataType:'json',
        success:function(res) {
            if(res.status != 'success') {
                alert("Error occured while storing message.")
            }
        }
    });
}

function load_messages() {
    $.ajax({
        url:"{{ url('chat/load-messages') }}",
        type:'get',
        data:{invitation_id:'{{ $invitation->unique_id}}',start:start},
        dataType:'json',
        success:function(res) {
            if(res.status == 'success') {
                messageContainer.append(res.data);
                messageContainer.scrollTop(messageContainer[0].scrollHeight);
            }else{
                alert("someting went wrong while loading Messages.")
            }
        }
    });
}
    </script>
@endsection

<script>
	 function acceptReportFunc(id){
		$.ajax({
        url:"{{ url('patient_visit') }}",
        data:{'id':id,'_token':'{{ csrf_token() }}'},
        type:'post',
        dataType:'json',
		beforeSend:function(){
			$("#accept_report_loading_msg_"+id).show();
			$("#acptButton_"+id).hide();
			$("#successMsg_"+id).hide();
		},
        success:function(res) {
		    $("#accept_report_loading_msg_"+id).hide();
			$("#acptButton_"+id).hide();
			$(".btn-container11").hide();
			$("#successMsg_"+id).show();
			$("#successMsg_"+id).text(res.message);

			setTimeout(() => {
				$("#successMsg_"+id).hide();
				location.reload();
			}, 3000);
        },
		error:function(xhr,status,error){
			$("#accept_report_loading_msg_"+id).hide();
			$("#successMsg_"+id).hide();
			$("#acptButton_"+id).show();
			console.log('xhr => ',xhr," status => ",status," error => ",error)
		}
    });
	}
</script>