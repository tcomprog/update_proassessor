@extends("includes.email_container")
@section("content")
<div>

        <h3 style="font-weight: bold; font-size: 18px;line-height: 19px;color:#20215E;">Dear {{ ucfirst($user->name) }},</h3>
        <?php if($auth_status=='approve') { ?>
            <p style="font-size: 14px;line-height: 17px;">Your account has been approved by the Admin and you can now login to the <a href="{{ url('/') }}" target="_blank">website</a></p> 
        <?php } else{ ?>
            <p style="font-size: 14px;line-height: 17px;">Your account has been declined by the Admin.</p> 
        <?php } ?>
        <br/>    

        <br/>

        <br/>

        <p style="font-weight: bold; font-size: 16px;line-height: 19px;color:#20215E;">Regards</p>
                        <p style="font-size: 14px;line-height: 17px;">{{ucfirst($site_title)}} Team.</p> 

</div>  

@endsection