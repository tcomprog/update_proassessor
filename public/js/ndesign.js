$(".openmnuebtn").on('click',function(){
    var type = $(this).data('status');

    if(type == 'open'){
        $("#sideMenuContainer").css({'width':'80%','height':"100vh",'padding':'35px 25px'});
        $("#fullScreenSideMenu").addClass("fullScreenSideMenu");

        $('html, body').css({
          overflow: 'hidden',
          height: '100%'
        });
    }
    else{
      $("#sideMenuContainer").css({'width':0,'height':0,'padding':0});
      $("#fullScreenSideMenu").removeClass("fullScreenSideMenu"); 

      $('html, body').css({
        overflow: 'auto',
        height: 'auto'
      });
    }

});

$(".openLoginPopup").on("click",function(){
   $("#sideMenuContainer").css({'width':0,'height':0,'padding':0});
      $("#fullScreenSideMenu").removeClass("fullScreenSideMenu"); 

      $('html, body').css({
        overflow: 'auto',
        height: 'auto'
      });
   $("#login").modal("show");
});

$(".inforp").on("click",function(){
   var type = $(this).data("type");
   var value = $(this).data("value");

   if(type == '1'){
      window.location.href = "mailto:"+value+"?subject=&body=";
   }
   else if(type == 0){
     window.location.href = "tel:"+value;
   }

});

$(".menumobclick").on("click",function(){
  $("#sideMenuContainer").css({'width':0,'height':0,'padding':0});
  $("#fullScreenSideMenu").removeClass("fullScreenSideMenu"); 

  $('html, body').css({
    overflow: 'auto',
    height: 'auto'
  });
});


$(document).ready(function()
{
    var swiper = new Swiper(".slide-content", {
    slidesPerView: 3,
    spaceBetween: 25,
    loop: true,
    centerSlide: 'true',
    fade: 'true',
    grabCursor: 'true',
    allowTouchMove:'true',
    loopPreventsSliding:'true',
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      dynamicBullets: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    scrollbar: {
      el: '.swiper-scrollbar',
    },
  });
 });


 function zoomFunc(zoomtype=0,formType = 0){
   // zoomtype => 0 - pluse, 1- minus
   // formType => 0 - login, 1- signup

   let loginModalPopup;
   if(formType == 0){
     loginModalPopup = $("#loginModalPopup");
   }
   else{
     loginModalPopup = $("#signUpFormContainer");
   }

   let zoom = localStorage.getItem('zoomLevel');

   if(zoom == null){
     localStorage.setItem('zoomLevel',1.05)
     zoom = 1.05;
   }else{
     zoom = parseFloat(zoom) + 0.03;
     console.log("---- ",zoom)
     localStorage.setItem('zoomLevel',zoom)
   }

   if(zoom > 1.3){
      zoom = 1.05;
      localStorage.setItem('zoomLevel',zoom)
   }

   if(zoomtype == 0){
        loginModalPopup.animate({ 'zoom': zoom }, 400);
   }
   else{
     loginModalPopup.animate({ 'zoom': 1 }, 400);
   }

 }
