<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Patient extends Model
{
    use HasFactory;
    protected $table = 'patients';
	public function patientinvitation(){
        return $this->hasOne(user_invitations::class, 'patient_user_id','id');
    }
    public function invitations()
    {
        return $this->hasMany(user_invitations::class, 'patient_user_id','id')->orderBy('id','desc');
    }
    
    public function withoutcanerlinvitations()
    {
        return $this->hasMany(user_invitations::class, 'patient_user_id','id')->where('status','!=','cancelled')->orderBy('id','desc');
    }

    public function doctorinvitations()
    {
      //  return $this->hasMany(user_invitations::class, 'patient_user_id','id')->groupBy('invitee_user_id');
        return $this->hasMany(user_invitations::class, 'patient_user_id','id');
    }

}
