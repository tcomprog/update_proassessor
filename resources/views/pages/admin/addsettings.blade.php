@extends('layouts.admin_app')



@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">

             <nav aria-label="breadcrumb">

              <ol class="breadcrumb">

                <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>

                <li class="breadcrumb-item"><a href="{{url('admin/settings')}}">Settings</a></li>

                <li class="breadcrumb-item active"><a href="#">Add Settings</a></li>

              </ol>

            </nav>

        </div>
        

        <div class="col-md-4"></div>
        <div class="col-md-4">
            @if (Session::has('success'))
                <div class="mt-2 alert alert-success">{{ Session::get('success') }}</div>
            @endif
            <form method="post" action="{{url('admin/settings-update')}}">
                @csrf
                  <div class="form-group">                   
                    <label for="exampleInputEmail1" style="font-size:18px;">Settings Name</label>
                    <div class="input-group">                         
                        <input type="text" class="form-control" placeholder="Enter Settings Name" required name="settingsname" required>
                   </div>
                 </div>

                 <div class="form-group">                   
                    <label for="exampleInputEmail1" style="font-size:18px;">Settings Value</label>
                    <div class="input-group">                         
                        <input type="text" class="form-control" placeholder="Enter Settings Value" required name="settingsvalue" required>
                   </div>
                 </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col-md-4"></div>

    </div>

</div>

@endsection

