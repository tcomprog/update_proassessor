<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Config;


class DeletePatientAccountMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data = [];
    public function __construct($data)
    {
      $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $fromAddress = Config::get('constants.MAIL_FROM_ADDRESS');
        $site_title_old = Config::get('constants.site_title');
        $site_title=str_replace("-"," ",$site_title_old);
        $suject = "Account deleted!";

        return $this->subject($suject)->from($fromAddress,$site_title)->view('emails.patient_deleted_email',[
            'name' => $this->data['name'],
            'site_title'=>$site_title,
        ]);
    }
}
