@extends('layouts.lawyer-das-custom') 

@section('content')	
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.error{color:red !important;}

.deisngCls{
    box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
    border-radius: 10px;
    border-left: 7px solid #515C84;
}

.profileTit{
    box-sizing: border-box;
    height: 52px;
    background: #515C84;
    border-radius: 40px;
    color:white;
    font-weight: 700;
    font-size: 30px;
    line-height: 52px;
    text-align: center;
    margin-bottom: 20px;
    width:55%;
    margin-left: auto;
    margin-right: auto;
}

.inputsss,select{
    border-radius:30px !important;
    height: 40px !important;
    border: 1px solid #515C84 !important;
}

label{
  color: #535D82 !important;
  font-size: 15px !important;
  font-weight: bold !important;
  padding-left: 8px !important;
  margin-bottom: 3px !important;
}
.form-group{
    margin-bottom: 15px !important;
}
.update_btn{
    margin-left: auto !important;
    margin-right: auto !important;
    background: linear-gradient(246.33deg, #20215E 34.76%, rgba(136, 144, 185, 0.59) 70.87%) !important;
    border-radius: 30px !important;
    height: 48px !important;
    margin-block: 0px !important;
    width: 330px !important;
    font-weight: bold !important;
}
</style>

	 <!-- Banner Section -->

    <section class="intro_banner_sec inner_banner_sec" style="padding:20px 0px;">
        <div class="container">

            <div class="row align-items-center"> 

                <div class="col-md-6">

                    <div class="welcome_text">

                        <h1>Profile</h1>

                    </div>

                </div>
               
            </div>
        </div>
    </section>
	<section class="dashboard_sec" style="padding-top:20px;">

        <div class="container deisngCls p-4">

            <div class="tab-content" id="nav-tabContent">
			
				<div class="tab-pane fade show active " id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

				<div class="row">

                    {{-- <div class="col-12">
                        <div class="profileTit">Profile</div>
                    </div> --}}

                    <div class="alert alert-success fade show" role="alert" id="updatesuccess" style="display:none;">
                        Profile updated successfully
                    </div>

                <div class="col-md-12">
                    <form class="profile_update" id="update_profile">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">								
                                    <label for="name">Name</label>
                                    <input maxlength="150" type="text" class="form-control inputsss" id="name" name="name" aria-describedby="name" placeholder="Name" value="{{ ucfirst($users->name) }} ">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control inputsss" id="email" name="email" aria-describedby="email" placeholder="Email" value="{{ $users->email }}" disabled>
                                </div>
                            </div>
                            {{-- <div class="col-md-6">
                                <div class="form-group">
                                    <label for="license">Change Password</label>
                                    <input type="password" class="form-control inputsss" name="password" id="password" aria-describedby="license" placeholder="Password" >
                                </div>
                            </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label for="license">Confirm Change Password</label>
                                    <input type="password" class="form-control inputsss" name="confirm_password" id="confirm_password" aria-describedby="license" placeholder="Confirm Password" >
                                </div>
                            </div> --}}
                            {{-- <?php  if(($users->profilepic)!=null){ ?>
							<div class="col-md-6">
                                <div class="form-group">								
                                    <label for="name">Update Profile Picture</label>
                                    <input type="file" class="form-control inputsss" name="image">
                                    <span class="text-danger" id="lawyerinvalidformat" style="color:red;"></span>
                                </div>								
                            </div>
							<div class="col-md-6">
                                <div class="form-group">
									<label for="name">Present Profile Picture</label>	<br>
                                      <img  src="uploads/profilepics/<?php echo $users->profilepic; ?>" class="profile_update_img">									  
                                </div>	
								<a class="" id="removeprofilepic" style="color: red;font-weight: 600;font-size: 15px;">Remove Profile Picture</a>
                            </div>
							<?php  }else { ?>
							<div class="col-md-12">
                                <div class="form-group">								
                                    <label for="name">Upload Profile Picture</label>
                                    <input type="file" class="form-control inputsss" name="image">
                                    <span class="text-danger" id="lawyerinvalidformat" style="color:red;"></span>
                                </div>
                            </div>
							<?php  } ?>   --}}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="address">Business Address</label>
                                    <input type="text" class="form-control inputsss" id="address" name="business_address" aria-describedby="address" placeholder="Business Address" value="{{ $users->business_address }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="location">City</label>
                                   <?php  $cities = Helper::cities();  ?>  
									<select class="form-control category_select pro_category_select @error('location') is-invalid @enderror" id='location' name="location" autofocus>
										<option value="">Choose City</option>
										<?php  foreach ($cities as $cities1){ ?>
										<option value="<?php echo $cities1->id; ?>" <?php if(($cities1->id)==($users->location)){echo "selected";} ?> ><?php echo $cities1->city_name; ?></option>
										<?php } ?>
									</select>
									<p class="city_txt" style="margin: 0px; font-weight:bold;padding-left:8px;">Ontario, Canada</p>								
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="phone">Postal Code </label>
                                    <input maxlength="6" type="text" class="form-control inputsss" id="postcode" name="postcode" aria-describedby="phone" placeholder="Postal Code" value="{{ $users->lawyer->postal_code }}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="phone">Phone Number</label>
                                    <input type="text" class="form-control inputsss" id="phone" name="phone" aria-describedby="phone" placeholder="Phone Number" value="{{ $users->phone }}">
                                </div>
                            </div>
                            														<div class="col-md-6">                                <div class="form-group">                                    <label for="license">Law Firm Name</label>                                    <input type="text" class="form-control inputsss" name="lawfirmname" id="lawfirmname" aria-describedby="license" placeholder="Law Firm Name" value="{{isset($users->lawyer->lawfirmname) ? $users->lawyer->lawfirmname : '' }}">                                </div>                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="license">Website ur</label>
                                    <input type="url" class="form-control inputsss" name="website_url" id="website_url" aria-describedby="license" placeholder="Website ur" value="{{isset($users->lawyer->website_url) ? $users->lawyer->website_url : '' }}">
                                </div>
                            </div>
							<input type="hidden" name="id" id="id" value="<?php  echo $users->id; ?>">
                        </div>


                        @if($users->terms_agreement)
                            <div class="col-md-12">
                                <div class="form-group">
                                    <a href="{{url('/download_agreement')}}"><i class="fa fa-download" > Download Terms & Conditions Agreement</i></a>
                                </div>
                            </div>
                        @endif
							<input type="hidden" name="id" id="id" value="<?php  echo $users->id; ?>">


                            <div style="margin-bottom: 15px;">
                                <div id="imgSectionCrp" @if (empty($users->profilepic) || $users->profilepic == 'null') style="display:none" @endif>
                                    <img class="profileiamgecls" src="{{url('uploads/profilepics/lawyer/'.$users->profilepic)}}" alt="">
                                    <span class="material-icons deletecropimga" style="color: #FFB81C; font-size:24px; margin-left:10px">delete</span>
                                    <img id='deleteCropImageLoader' width="30" style="display: none;" src="{{asset('img/loading.gif')}}" alt="">
                                </div>

                                <button @if (!empty($users->profilepic) && $users->profilepic != 'null') style="display:none" @endif class="uploadbtn"  onclick="chooseImageModal()" type="button">Upload profile image</button>
                            </div>

                        </div>

        

                        <button type="submit" class="btn btn-primary update_btn" id="update_btn">Update Information</button> 
                    </form>
                </div>
            </div>

        </div>
		
		<!---delete profile pic---->
		<div class="modal fade" id="deleteprofilepicdiv" tabindex="1" role="dialog" aria-labelledby="loginTitle" aria-hidden="true">
				
			<div class="modal-dialog modal-dialog-centered" role="document">

				<div class="modal-content login_box">

				  <div class="modal-header" >

					<h5 class="modal-title" id="loginTitle" >Are Sure Want to Delete Profile Pic?</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

					  <span aria-hidden="true">&times;</span>

					</button>

				  </div>

				  <div class="modal-body">

						  <form id="deleteprofilepic_form"><!-------method="POST" action="{{ route('login') }}--------->
								@csrf

								<div class="row">

									<div class="col-md-12">

										<div class="form-group">
											<input type="hidden" id="userid" name="userid" value="{{ $users->id }}">
										</div>

									</div>
									<div class="col-md-12">
									   <p id="successtask1" style="color:green;display:none;">Profile Picture Deleted Successfully</p>
									</div>
								</div>

								<button type="submit" class="btn btn-primary submit_btn"  id="deleteprofilepic_btn">Yes</button>
								<button type="submit" class="btn btn-primary submit_btn"  id="deleteprofilepic_btn1" data-dismiss="modal" aria-label="Close">No</button>

							</form>

				  </div>

				</div>

			  </div>

		</div>

    </section>
	
    @include('includes.imageCroping');
	
@endsection

