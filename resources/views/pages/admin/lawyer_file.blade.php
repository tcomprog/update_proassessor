@extends('layouts.admin_app')
<link href="{{ asset('css/clinic_dashboard.css') }}" rel="stylesheet">


@section('content')

<div class="container-fluid">


	<div class="container dashboardContainer mb-5 bg-white" style="margin-top:10px;">
		<h3 class="inv-content-header">Personal Information</h3>

		<div class="row">
			<div class="col-12">
			
				@if (!empty($user->profilepic) && $user->profilepic != 'null')
				<img src="{{url('uploads/profilepics/lawyer')}}/<?php echo $user->profilepic; ?>" class="img-thumbnail ml-1 mt-1" style="width:80px !important;" alt="">
				@endif
	 
				<table class="mt-2">

					<tr>
					  <th width="170">Name</th>
					  <td>{{$user->name}}</td>
					</tr>
					
					<tr>
					  <th width="170">Email</th>
					  <td>{{$user->email}}</td>
					</tr>

					<tr>
					  <th width="170">Phone Number</th>
					  <td>{{$user->phone}}</td>
					</tr>

					<tr>
					  <th width="170">Business Address</th>
					  <td>{{$user->business_address}}</td>
					</tr>

					<tr>
					  <th width="170">City</th>
					  <td>
						@php
							$cities = Helper::cities(); 
						@endphp
						@foreach ($cities as $cities1)
						   @if (($cities1->id)==($user->location))
							  {{$cities1->city_name}}
						   @endif
						@endforeach
					  </td>
					</tr>

					<tr>
					  <th width="170">Website Url</th>
					  <td>{{$user->lawyer->website_url}}</td>
					</tr>

				</table>

			</div>
		</div>

		<h3 class="inv-content-header mt-5">Patients</h3>

		<div class="row">
			<div class="col-12">
				<table id="datatbl11" class="row-border hover cell-border" style="width:100%">
					<thead>
						<tr>
							<th>S.No</th>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>DOB</th>
							<th>File Number</th>
							<th>Case Type</th>
							<th >Action</th>
						</tr>
					</thead>
		
					 <tbody>
					   @if(count($patient) > 0)
						 @foreach($patient as $key => $row)
							 <tr>
								<td>{{$key+1}}</td>
								<td>{{$row->name}}</td>
								<td><?php echo MyHelper::Decrypt($row->email); ?></td>
								<td><?php echo MyHelper::Decrypt($row->phonenumber); ?></td>
								<td>{{date('d/m/Y',strtotime($row->dateofbirth))}}</td>
								<td><?php echo MyHelper::Decrypt($row->filenumber); ?></td>
								<td>
									@if ($row->casetype == '1')
										MVA
									@elseif ($row->casetype == '2')
                                        WSIB
									@elseif ($row->casetype == '3')
									    Slip & Fall
									@endif
								</td>
								<td>
									<a href ="#" class="btn btn-success btn-sm" title="View profile">
										<i class="fa fa-eye" aria-hidden="true"></i>
									</a>
								</td>
							 </tr>
						 @endforeach
					   @endif
					</tbody>
		
				</table>
			</div>
		</div>

	</div>



   

</div>

@endsection

