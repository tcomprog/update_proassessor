<?php

namespace App\Mail;

use App\Models\user_invitations;

use Config;

use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Crypt;

use Illuminate\Queue\SerializesModels;

class PatientInvitation extends Mailable

{

    use Queueable, SerializesModels;

    /**

     * The order instance.

     *

     * @var \App\Models\Order

     */

    public $invitation;

    public $options = [];

    /**

     * Create a new message instance.

     *

     * @param  \App\Models\Order  $order

     * @return void

     */

    public function __construct(user_invitations $invitation,$options)

    {

        $this->invitation = $invitation;

        $this->options = $options;

    }

    /**

     * Build the message.

     *

     * @return $this

     */

    public function build()

    {

        $fromAddress = Config::get('constants.MAIL_FROM_ADDRESS');

        $site_title_old = Config::get('constants.site_title');
        $site_title=str_replace("-"," ",$site_title_old);

        if(isset($this->options['type']) && $this->options['type'] == 'lawyer') {

            $suject = 'HCMG - Your Invitation is Expired.';

            if($this->invitation->status == 'active'){

                $suject = $site_title.' - Accepted Invitation Notification.';

            }else if($this->invitation->status == 'rejected') {

                $suject = $site_title.' - Rejected Invitation Notification.';

            }
    
            return $this->subject($suject)->from($fromAddress,$site_title)->view('emails.send_invitation_to_lawyer',[
                'invitation'=>$this->invitation,
                'site_title'=>$site_title,
            ]);

        }else{

            return $this->subject($site_title.' - Appointment confirmation mail')->from($fromAddress,$site_title)->view('emails.send_invitation_to_patient',['invitation'=>$this->invitation,'site_title'=>$site_title]);

        }

    }

}