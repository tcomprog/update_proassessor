@extends('layouts.admin_app')



@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">

             <nav aria-label="breadcrumb">

              <ol class="breadcrumb">

                <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a></li>

                <li class="breadcrumb-item"><a href="{{url('admin/settings')}}">Settings</a></li>

                <li class="breadcrumb-item active"><a href="#">Edit Settings</a></li>

              </ol>

            </nav>

        </div>
        

        <div class="col-md-4"></div>
        <div class="col-md-4">
            @if (Session::has('success'))
                <div class="mt-2 alert alert-success">{{ Session::get('success') }}</div>
            @endif
            <form method="post" action="{{url('admin/edit_settings')}}">
                @csrf
                  <div class="form-group">                   
                    <label for="exampleInputEmail1" style="font-size:18px;">Settings Name</label>
                    <div class="input-group">                         
                        <input type="text" class="form-control" placeholder="Enter Settings Name" required name="settings_display_name"  value="{{$settings->settings_display_name}}">
                   </div>
                 </div>

                 <div class="form-group">                   
                    <label for="exampleInputEmail1" style="font-size:18px;">Settings Value</label>
                    <div class="input-group">                         
                        <input type="text" class="form-control" placeholder="Enter Settings"  required name="settings_value" required value="{{$settings->settings_value}}">
                        <input type="hidden" name="id" value="{{$settings->id}}">
                   </div>
                 </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col-md-4"></div>

    </div>

</div>

@endsection

