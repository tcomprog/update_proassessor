<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\doctors;
use App\Models\DoctorRating;
use App\Models\lawyers;
use App\Models\RejectInvitation;
use App\Models\user_invitations;
use App\Models\InvitationAttachment;
use App\Http\Controllers\EmailController;
use App\Models\Tracability;
use App\Models\Nofication;
use App\Models\Patient;
use App\Helpers\Helper as MyHelper;
use App\Models\Designation;
use Illuminate\Support\Collection;
use App\Models\Sitedetails;
use App\Models\Category;

use Session;

class PatientController extends Controller
{
    public function accept_invitation(Request $request, $invitation_id)
    {
        Session::flush();
        Auth::logout();
        $invitation = user_invitations::where('unique_id', $invitation_id)->first();
        // $invitationattacments = InvitationAttachment::where('invitation_id', $invitation->id)->first(); 
     
        $type = 'expired';
        if ($invitation) {
            if ($invitation->status == 'active' && $invitation->rejected_by == null && $invitation->is_patient_accepted != 'yes') {
                $invitation->is_patient_accepted = 'yes';
                $invitation->patient_accepted_on = date('Y-m-d H:i:s');
                $type = 'accepted';
                $invitation->save();

                $tracability = new Tracability();
				$tracability->date = date('Y-m-d H:i:s');
				$tracability->invitation_id	 = $invitation->id;
				$tracability->status = '2';
				$tracability->save();

                $data = Patient::find($invitation->patient_user_id);
                $des = MyHelper::c_strings()["patient_accepted"];
                $des = str_replace("@patient",$data->name,$des);
                $des = str_replace("@date",date("d-m-Y H:i",strtotime($invitation->created_at)),$des);
                
                $notif = new Nofication();
                $notif->url = "doctor-dashboard/my-invitations/#confirmed-invitations";
                $notif->description = $des;;
                $notif->from_id = $invitation->inviter_user_id;
                $notif->to_id = $invitation->invitee_user_id;
                $notif->save();

                $des = MyHelper::c_strings()["medical_report_reminder"];
                $des = str_replace("@patient",$data->name,$des);
                $des = str_replace("@date",date("d-m-Y H:i",strtotime($invitation->created_at)),$des);
                
                $notif = new Nofication();
                $notif->url = "patient_details/".$invitation->patient_user_id."?doctorid=".$invitation->invitee_category_id;
                $notif->description = $des;; 
                $notif->from_id = $invitation->invitee_user_id; 
                $notif->to_id =  $invitation->inviter_user_id;
                $notif->save();

                if ($invitation->id) {
                    $name = $email = "";
                    $doctor = $invitation->user1;
                    if($doctor->clinic_id > 0){
                        $clinic = User::find($doctor->clinic_id);
                        $name = $clinic->name;
                        $email = $clinic->email;
                    }
                    else{
                        $name = $doctor->name;
                        $email = $doctor->email;
                    }
                    
                    $emailCtrl = new EmailController();
                    $emailCtrl->send_notification_email_to_lawyer_after_patient_accepts($invitation,$name,$email);
                }


            }
        }

        return view('pages/mail-patient-appointment', ['invitation' => $invitation, 'type' => $type]);
    }

    public function reject_invitation(Request $request, $invitation_id)
    {
        if(Auth::check()){
            $user = Auth::user();
            if($user->role_id != '5'){
                Session::flush();
                Auth::logout();
            }
        }else{
            Session::flush();
            Auth::logout();
        }

        $invitation = user_invitations::where('unique_id', $invitation_id)->first();
      
       if($invitation->reject_status == 0){
           $type = 'rejected';
       }
       elseif($invitation->reject_status == 1){
           $type = 'submit';
       }
       else{
           $type = 'expired';
       }
        // if ($invitation) {
        //     if ($invitation->status == 'active' && $invitation->rejected_by == null) {
        //         $invitation->status = 'rejected';
        //         $invitation->rejected_by = 'patient';
        //         $type = 'rejected';
        //         $invitation->save();

        //         $data = Patient::find($invitation->patient_user_id);
        //         $des = MyHelper::c_strings()["patient_rejected"];
        //         $des = str_replace("@patient",$data->name,$des);
        //         $des = str_replace("@date",date("d-m-Y H:i",strtotime($invitation->created_at)),$des);
                
        //         $notif = new Nofication();
        //         $notif->url = "doctor-dashboard/my-invitations/#rejected-invitations";
        //         $notif->description = $des;; 
        //         $notif->from_id = $invitation->inviter_user_id;
        //         $notif->to_id = $invitation->invitee_user_id;
        //         $notif->save();

        //         if ($invitation->id) {
        //             $emailCtrl = new EmailController();
        //             $emailCtrl->send_notification_email_to_lawyer_after_patient_rejects($invitation);
        //         }
        //     }
        // }
        return view('pages/mail-patient-appointment', ['invitation' => $invitation, 'type' => $type]);
    }

    public function rejectReportRequest(Request $request){
        $id = $request->id;
        $desc = $request->desc;
        $option = $request->option;
        $invitation = user_invitations::where('id', $id)->first();

        $desc = !empty($desc) ? $desc : '';

        if($invitation->reject_status == 0){
            $reject = new RejectInvitation();
            $reject->invitation_id = $id;
            $reject->patient_id = $invitation->patient_user_id; 
            $reject->desc = $desc;
            $reject->option = $option;
            $reject->status = 0;
            $reject->save();

            $data = Patient::find($invitation->patient_user_id);

            $notif = new Nofication();
            $notif->url = "patient_details/".$data->id."?appdate=".$invitation->invitee_category_id;
            $notif->description = "The assessment reject request has been submitted by ".$data->name." and will futher process by Admin";
            $notif->from_id = 0;
            $notif->to_id = $invitation->inviter_user_id;
            $notif->save();

            $invitation->reject_status = 1;
            $invitation->save();

        }else{
            return response()->json(['status'=>'0','message'=>"Request already submitted"],200);
        }

        return response()->json(['status'=>'1','message'=>"Successfully"],200);
    }


    public function invitation_tracking(Request $request){
        $invitation_id = $request->id;
        $data = Tracability::where('invitation_id',$invitation_id)->orderBy('status','asc')->get();

        return response()->json(['sttus'=>'1','data'=>$data],200);
    }

    public function accept_reports(Request $request){

        $sendint = user_invitations::find($request->id);

        $doctorRating = new DoctorRating();
        $doctorRating->rating = $request->rating;
        $doctorRating->assessment_id = $request->id;
        $doctorRating->doctor_id = $sendint->invitee_user_id;
        $doctorRating->save();

        $invitation_id = $request->id;
        $tracability = new Tracability();
        $tracability->date = date('Y-m-d H:i:s');
        $tracability->invitation_id	 = $invitation_id;
        $tracability->status = '7';
        $tracability->save();

        $sendint->status='completed';
        $sendint->save();

        return response()->json(['status'=>'1','message'=>'Report accepted successfully'],200);
    }

    public function patient_visit(Request $request){
        $invitation_id = $request->id;
        $tracability = new Tracability();
        $tracability->date = date('Y-m-d H:i:s');
        $tracability->invitation_id	 = $invitation_id;
        $tracability->status = '3';
        $tracability->save();

        $tracability = new Tracability();
        $tracability->date = date('Y-m-d H:i:s');
        $tracability->invitation_id	 = $invitation_id;
        $tracability->status = '4';
        $tracability->save();

        return response()->json(['status'=>'1','message'=>'Patient visit confirmed successfully'],200);

    }


    public function openPatientProfile(Request $request){
        $user = auth()->user();
		$users = User::find($user->id);
		$patient = Patient::find($users->patient_id);

		return view('/pages/patient/patient_profile',[
            'users' => $users,
            'patient' => $patient
        ]);
    }

    public function updatePatientProfile(Request $request){
        $user = auth()->user();
        $users = User::where('id', $user->id)->first(); 
		$patient = Patient::find($users->patient_id);
        $helper = new MyHelper();	

        $gender = $request->gender;
        $fname = $request->fname;
        $lname = $request->lname;
        $filenumber = $request->filenumber;
        $address = $request->address;
        $phone = $request->phone;
        $casetype = $request->casetype;
        $dob = $request->dob;
        $doa = $request->doa;

		$patient->fname=$request->fname;
		$patient->lname=$request->lname;
		$patient->name=$request->fname.' '.$request->lname;
		$patient->dateofaccident = $request->doa;
		$patient->dateofbirth = $request->dob;
		$patient->address = $helper->Encrypt($request->address);	
		$patient->casetype = $request->casetype;	
		$patient->gender = $helper->Encrypt($request->gender);	
		$patient->phonenumber = $helper->Encrypt($request->phone);
		$patient->filenumber = MyHelper::Encrypt($request->filenumber);
		$patient->save();	

	    $users->name = $request->fname.' '.$request->lname;
	    $users->phone = $request->phone;
	    $users->business_address = $request->address;
	    $users->save();
		
        return response()->json(['status'=> "success", 'url'=>url('updatePatientProfile')]);
    }


    public function patientDashboard(){
        $user = Auth()->user();
        
        $pending = user_invitations::where('patient_user_id',$user->patient_id)
            ->where(function ($query) {
                $query->orWhere(function ($query) {
                    $query->where('status', 'active')->where('is_patient_accepted','no')->where('reject_status','0');
                })
                ->orWhere(function ($query) {
                    $query->orWhere('status','pending');
                });

            })->get();


        $active = user_invitations::where('patient_user_id',$user->patient_id)->where('is_patient_accepted','yes')->where('status','active')->get();
        $completed = user_invitations::where('patient_user_id',$user->patient_id)->where('status','completed')->get();
        $cancelled = user_invitations::where('patient_user_id',$user->patient_id)->where('status','cancelled')->get();

        $patient = Patient::find($user->patient_id);
        $lawyer = User::select('email','phone','business_address','name','profilepic')->find($patient->lawyer_id);

        $desg = Designation::get();
        $desAr = [];
        foreach($desg as $it){
            $desAr[$it->id] = $it->designation;
        }
        
        return view('pages/patient/patient-dashboard',[
            'pending' => $pending,
            'active' => $active,
            'completed' => $completed,
            'cancelled' => $cancelled,
            'lawyer' => $lawyer,
            'desgination' => $desAr,
        ]);
    }


    public function changeInvitationStatus(Request $request){
        $ID = $request->ID;
        $status = $request->status;
        $user = Auth::user();
        $invitation = user_invitations::where('id', $ID)->where('patient_user_id',$user->patient_id)->get();


       if(count($invitation) > 0){
        $invitation = $invitation[0];

        if($invitation->status == 'active' && $invitation->rejected_by == null && $invitation->is_patient_accepted != 'yes'){

                $invitation->is_patient_accepted = 'yes';
                $invitation->patient_accepted_on = date('Y-m-d H:i:s');
                $type = 'accepted';
                $invitation->save();

                $tracability = new Tracability();
				$tracability->date = date('Y-m-d H:i:s');
				$tracability->invitation_id	 = $invitation->id;
				$tracability->status = '2';
				$tracability->save();

                $data = Patient::find($invitation->patient_user_id);
                $des = MyHelper::c_strings()["patient_accepted"];
                $des = str_replace("@patient",$data->name,$des);
                $des = str_replace("@date",date("d-m-Y H:i",strtotime($invitation->created_at)),$des);

                $notif = new Nofication();
                $notif->url = "doctor-dashboard/my-invitations/#confirmed-invitations";
                $notif->description = $des;;
                $notif->from_id = $invitation->inviter_user_id;
                $notif->to_id = $invitation->invitee_user_id;
                $notif->save();

                $des = MyHelper::c_strings()["medical_report_reminder"];
                $des = str_replace("@patient",$data->name,$des);
                $des = str_replace("@date",date("d-m-Y H:i",strtotime($invitation->created_at)),$des);
                
                $notif = new Nofication();
                $notif->url = "patient_details/".$invitation->patient_user_id."?doctorid=".$invitation->invitee_category_id;
                $notif->description = $des;; 
                $notif->from_id = $invitation->invitee_user_id; 
                $notif->to_id =  $invitation->inviter_user_id;
                $notif->save();

                if ($invitation->id) {
                    $name = $email = "";
                    $doctor = $invitation->user1;
                    if($doctor->clinic_id > 0){
                        $clinic = User::find($doctor->clinic_id);
                        $name = $clinic->name;
                        $email = $clinic->email;
                    }
                    else{
                        $name = $doctor->name;
                        $email = $doctor->email;
                    }
                    
                    $emailCtrl = new EmailController();
                    $emailCtrl->send_notification_email_to_lawyer_after_patient_accepts($invitation,$name,$email);
                }

                return response()->json(['status'=>'success','message'=>'Invitation approved successfully!']);
       }
       else{
        return response()->json(['status'=>'error','message'=>'Your are not allowed to performed this action!']);
       }
       }
       else{
         return response()->json(['status'=>'error','message'=>'Invalid access!']);
       }
    }
    

    public function viewInvitationDetails($id){
        $user = Auth::user();
        $invitation = user_invitations::where('id', $id)->where('patient_user_id',$user->patient_id)->get();

        if(count($invitation) > 0){
            $invitation = $invitation[0];
            $patientid = $invitation->patient_user_id;
            $desg = Designation::get();
            $desAr = [];
            foreach($desg as $it){
                $desAr[$it->id] = $it->designation;
            }
            $patient = patient::find($patientid);
            $settings = Sitedetails::select(['invitation_expire_in_minutes'])->first();
            $expireTime = date('Y-m-d H:i:s',strtotime('-'.$settings->invitation_expire_in_minutes.' minutes'));
            $categories = Category::select(['id','category_name'])->get();

            return view('pages.patient.invitation-details',
            [
            'patient' => $patient, 
            'expire_time'=>$expireTime,
            'patientid'=>$patientid, 
            'categories'=>$categories,
            'desgination' => $desAr,
            'invitation' => $invitation
            ]
            );
        }
        else{
            exit("You are not allowed to performed this action");
        }
    }

}


